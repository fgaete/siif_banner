<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Boletas de Honorario</title>
<%
	int rutUsuario = session.getAttribute("rutUsuario")!= null?Integer.parseInt(session.getAttribute("rutUsuario")+""):0;


	String numeroBoleta = request.getParameter("numeroBoleta") != null &&
						!request.getParameter("numeroBoleta").trim().equals("")?
						request.getParameter("numeroBoleta"):"0";
	String rutnum = request.getParameter("rutnum") != null &&
						!request.getParameter("rutnum").trim().equals("")?
						request.getParameter("rutnum"):"0";
	String diaFecha = request.getParameter("diaFecha") != null &&
						!request.getParameter("diaFecha").trim().equals("")?
						request.getParameter("diaFecha"):"0";
	String mesFecha = request.getParameter("mesFecha") != null &&
						!request.getParameter("mesFecha").trim().equals("")?
						request.getParameter("mesFecha"):"0";
	String annoFecha = request.getParameter("annoFecha") != null &&
						!request.getParameter("annoFecha").trim().equals("")?
						request.getParameter("annoFecha"):"0";
	String url = request.getParameter("url") != null &&
						!request.getParameter("url").trim().equals("")?
						request.getParameter("url"):"";


	String imgPDF = "../sgf/img/32x16_pdf.gif";

	String img = "";
	//directorio de boletas
	//String directorio = "/usr/local/siif_doc/boleta/" + annoFecha + "/"+ mesFecha+"/"+ diaFecha ;

%>
<script language="javascript">

function downloadArch(file, dir, action){
	document.formdir.file.value = file; 
	document.formdir.dir.value = dir; 
	document.formdir.action = action;
	document.formdir.submit();	
}
</script>
</head>
<body  onload="downloadArch('<%="bhe_" + rutnum+"-"+numeroBoleta+".pdf"%>','<%=diaFecha%>','<%=url%>');">
<form name="formdir" id="formdir" action="" METHOD="POST" target="ventana" >
	 <input type="hidden" name="file" value="">
	 <input type="hidden" name="dir" value="">
	 <input type="hidden" name="admin" value="">
	 <input type="hidden" name="annoFecha" value="<%=annoFecha %>">
	 <input type="hidden" name="mesFecha" value="<%=mesFecha %>">
</form>
</body>
</html>