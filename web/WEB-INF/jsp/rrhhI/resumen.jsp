<%@page language="java" import="descad.presupuesto.*,
                                 java.util.*,
                                 java.io.*,
                                 descad.cliente.PreswBean,
                                 java.text.NumberFormat,
                                 org.apache.poi.ss.usermodel.DataFormat,
                                 org.apache.poi.poifs.filesystem.POIFSFileSystem,
                                 org.apache.poi.hssf.usermodel.*,
                                 org.apache.poi.ss.usermodel.CellStyle"
                                 contentType="application/vnd.ms-excel"%><%                                 
	// Creado por           : Yonis vergara.
	// Fecha                : 29/08/2018
	// �ltima Actualizaci�n :		
			
			//Vector vecHead = (Vector)session.getAttribute("head");
			Vector contenido = (Vector)session.getAttribute("contenido");
			String fecha = (String)session.getAttribute("fecha");
			
			String archivo = "Resumen de traspaso "+fecha;
			String titulo = "Resumen";

			//fecha
			java.util.Calendar f = new java.util.GregorianCalendar();
			String diaActual = String.valueOf(f.get(java.util.Calendar.DAY_OF_MONTH));
			String mesActual = String.valueOf(f.get(java.util.Calendar.MONTH) + 1);
			String a�oActual = String.valueOf(f.get(java.util.Calendar.YEAR));
			String hora = String.valueOf(f.get(java.util.Calendar.HOUR_OF_DAY));
			String min = String.valueOf(f.get(java.util.Calendar.MINUTE));
			if (hora.length() == 1)
				hora = "0" + hora;
			if (min.length() == 1)
				min = "0" + min;
			try {
				int fila = 0;

				ServletOutputStream sos = response.getOutputStream();
				ByteArrayOutputStream OUT = new ByteArrayOutputStream();

				HSSFWorkbook wb = new HSSFWorkbook();
				HSSFSheet sheet1 = wb.createSheet(titulo);

				// Estilo Arial-BOLD
				HSSFFont font1 = wb.createFont();
				font1.setFontHeightInPoints((short) 10);
				font1.setFontName("Arial");
				font1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				HSSFCellStyle style_bold = wb.createCellStyle();
				style_bold.setFont(font1);

				// Estilo Arial-ITALICA
				HSSFFont font2 = wb.createFont();
				font2.setFontHeightInPoints((short) 10);
				font2.setFontName("Arial");
				font2.setItalic(true);
				HSSFCellStyle style_italica = wb.createCellStyle();
				style_italica.setFont(font2);

				// Estilo FECHA
				HSSFCellStyle style_fecha = wb.createCellStyle();
				style_fecha.setDataFormat(HSSFDataFormat.getBuiltinFormat("d/m/yy h:mm"));

				// Estilo BORDE BOLD
				HSSFCellStyle style_borde_bold = wb.createCellStyle();
				style_borde_bold.setBorderBottom((short) 1);
				style_borde_bold.setBorderLeft((short) 1);
				style_borde_bold.setBorderRight((short) 1);
				style_borde_bold.setBorderTop((short) 1);
				style_borde_bold.setAlignment((short) 1);
				style_borde_bold.setFont(font1);

				// Estilo BORDE
				HSSFCellStyle style_borde = wb.createCellStyle();
				style_borde.setBorderBottom((short) 1);
				style_borde.setBorderLeft((short) 1);
				style_borde.setBorderRight((short) 1);
				style_borde.setBorderTop((short) 1);
				style_borde.setAlignment((short) 1);

				// Estilo BORDE
				HSSFCellStyle style_borde_centro = wb.createCellStyle();
				style_borde_centro.setBorderBottom((short) 1);
				style_borde_centro.setBorderLeft((short) 1);
				style_borde_centro.setBorderRight((short) 1);
				style_borde_centro.setBorderTop((short) 1);
				style_borde_centro.setAlignment((short) 2);

				// Estilo numerico
				DataFormat format = wb.createDataFormat();
				CellStyle style_numerico = wb.createCellStyle();
				style_numerico = wb.createCellStyle();
				style_numerico.setDataFormat(format.getFormat("##,###,###,###,##0"));
				style_numerico.setBorderBottom((short) 1);
				style_numerico.setBorderLeft((short) 1);
				style_numerico.setBorderRight((short) 1);
				style_numerico.setBorderTop((short) 1);
				style_numerico.setAlignment((short) 3);

				// cabesera
				
					

				//Cabezera de la tabla
				int fila_enc = fila++;
				HSSFRow row0 = sheet1.createRow((short) fila_enc);
				int col = 0;
				
				//for (int i = 0; i < vecHead.size(); i++) {
					
					//HSSFCell cell0 = row0.createCell((short) col);
					//String columname =  "Gurfeed_System_Id";
					//row0.createCell((short) col);
					//cell0.setCellValue(vecHead.get(i)+"");
					//cell0.setCellStyle(style_borde_bold);
					//col++;
				//}
				Vector datos ;
				for (int i = 0; i < contenido.size(); i++) {
				    row0 = sheet1.createRow((short) fila);
					datos = (Vector)contenido.get(i);
					for (int x = 0; x < datos.size(); x++) {
						HSSFCell cell0 = row0.createCell((short) x);
						//String columname =  "Gurfeed_System_Id";
						//row0.createCell((short) col);
						cell0.setCellValue(datos.get(x)+"");
						//cell0.setCellStyle(style_borde_bold);
						col++;
					}
					fila++;
				}	

				wb.write(OUT); // se escribe el xls en un ByteArrayOutputStream

				response.setContentLength(OUT.size());
				response.setContentType("application/x-download");
				response.setHeader("Content-Disposition","attachment;filename=" + archivo + ".xls;");

				//Se pone el fichero en la salida
				byte[] bufferExcel = OUT.toByteArray();
				sos.write(bufferExcel, 0, bufferExcel.length);

				//Se muestra la salida.
				sos.flush();
				sos.close();
				OUT.close();
				if (request.getSession().getAttribute("informeMovEjecucionExcel") != null)
					session.removeAttribute("informeMovEjecucionExcel");
				return;

			} catch (IOException ex) {
				ex.printStackTrace();
			}
		
			%>
