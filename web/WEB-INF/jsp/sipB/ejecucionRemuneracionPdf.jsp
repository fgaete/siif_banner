<%@
page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%

response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 25, 20);/*.rotate() es para dejar la hoja horizontal*/
int rutUsuario = session.getAttribute("rutUsuario") != null?
                 Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
String nomUnidad = request.getParameter("nomUnidad") != null &&
                 !request.getParameter("nomUnidad").trim().equals("")?
                  request.getParameter("nomUnidad"):"";  
String estamento = request.getParameter("estamento") != null &&
                  !request.getParameter("estamento").trim().equals("")?
                   request.getParameter("estamento"):"";   
int anno = request.getParameter("anno") != null &&
                   !request.getParameter("anno").trim().equals("")?
                    Integer.parseInt(request.getParameter("anno")):0;                   
String nomEstamento = "";
               	if(estamento.trim().equals("T"))
               		nomEstamento = "Todos";
               	if(estamento.trim().equals("A"))
               		nomEstamento = "Acad�mico";
               	if(estamento.trim().equals("N"))
               		nomEstamento = "No Planta";
               	if(estamento.trim().equals("M"))
               		nomEstamento = "Administrativo";
               	if(estamento.trim().equals("O"))
               		nomEstamento = "Operarios";
               	if(estamento.trim().equals("H"))
               		nomEstamento = "Honorarios";
               	if(estamento.trim().equals("Y"))
               		nomEstamento = "Ayudantes";
               	if(estamento.trim().equals("D"))
               		nomEstamento = "Docentes";		   
try{
	long presupuesto = 0;
	long gastos = 0 ;
	long presupuestoacum = 0;
	long gastosacum = 0 ;
	
	Vector vec_encabezado2 = new Vector();
    Vector vec_datos = new Vector();
    vec_datos = request.getSession().getAttribute("ejecucionRemuneracionExportar") != null?
    		    (Vector) request.getSession().getAttribute("ejecucionRemuneracionExportar"): new Vector();
    
    NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);		    
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 

	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(new Phrase("P�gina ", helv), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);

    String textoOpcion = "";
    textoOpcion = "EJECUCI�N DE REMUNERACIONES " ;
      
    HeaderFooter header = new HeaderFooter(new Phrase(textoOpcion, new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);      
    //document.open();
	
    // datos
    Vector vec_encabezado = new Vector();   
    float[] colsWidth = null;
    int largocols = 0;
    vec_encabezado.addElement("MES");
    vec_encabezado.addElement("PRESUPUESTO MES $");
    vec_encabezado.addElement("GASTO DEL MES $");
    vec_encabezado.addElement("%");
    vec_encabezado.addElement("SALDO DEL MES $");
    vec_encabezado.addElement("PRESUPUESTO ACUMULADO $");
    vec_encabezado.addElement("GASTO ACUMULADO $");
    vec_encabezado.addElement("%");
    vec_encabezado.addElement("SALDO DISPONIBLE MES $");
        colsWidth = new float[] {1.2f, 1f, 1f, 0.5f, 1f, 1f, 1f, 0.5f, 1f}; // Code 1
        largocols = 9;
  
    

    
        // add a table to the document
        document.open();
		
		PdfPTable table = new PdfPTable(colsWidth);
		table.setWidthPercentage(100); // Code 2
		table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
		
		PdfPCell cell = null;
		cell = new PdfPCell(new Paragraph("UNIDAD: " + nomUnidad , new Font(bf_helvb)));
		cell.setBorder(0);
		cell.setPaddingBottom(10);
	    cell.setColspan(largocols);
		table.addCell(cell);
		
		 cell = null;
		cell = new PdfPCell(new Paragraph("A�O: " + anno + " - " + nomEstamento, new Font(bf_helvb)));
		cell.setBorder(0);
		cell.setPaddingBottom(10);
	    cell.setColspan(largocols);
		table.addCell(cell);
		
		for (int m = 0 ; m < vec_encabezado.size(); m++){
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table.addCell(cell);
	      	
	    }
		
			for (int j = 0 ; j < vec_datos.size(); j++){  
				for (int d = 0 ; d < largocols; d++){
					
					if (d ==1) //presupuesto
						presupuesto = presupuesto + Long.parseLong(((Vector) vec_datos.get(j)).get(d).toString());
					
					if ( d ==2) //gastos
						gastos = gastos + Long.parseLong(((Vector) vec_datos.get(j)).get(d).toString());
					
					if (d ==5) //preacum
						presupuestoacum =  Long.parseLong(((Vector) vec_datos.get(j)).get(d).toString());
				
					if (d ==6) //preacum
						gastosacum =  Long.parseLong(((Vector) vec_datos.get(j)).get(d).toString());
					
					if (d == 0) {
						if(j == (vec_datos.size() - 1)){
							cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(d).toString(), helvb));
							cell.setBackgroundColor(Color.LIGHT_GRAY);	
							
						} else {
						cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(d).toString(), helv));
						cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						}
					}	
					else {
						cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector) vec_datos.get(j)).get(d).toString())), helv));
						
						if(j == (vec_datos.size() - 1)){
							cell.setBackgroundColor(Color.LIGHT_GRAY);
						}
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					}
		   			table.addCell(cell);      	
	    		}   
				
			}
		document.add(table);
	
		 colsWidth = new float[] {1.2f, 1f, 1f, 0.5f, 1f, 1f, 1f, 0.5f, 1f}; // Code 1
		 table = new PdfPTable(colsWidth);
	table.setWidthPercentage(100); // Code 2
	table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
        
		
	
		 /*totales*/
        vec_encabezado = new Vector(); 
        vec_encabezado.addElement("Total ");       
        vec_encabezado.addElement(presupuesto+"");
        vec_encabezado.addElement(gastos+"");
        vec_encabezado.addElement("");
        vec_encabezado.addElement("");
        vec_encabezado.addElement(presupuestoacum+"");
        vec_encabezado.addElement(gastosacum+"");
        vec_encabezado.addElement("");
        vec_encabezado.addElement("");
	
        for (int m = 0 ; m < vec_encabezado.size(); m++){
        	//System.out.println("encabezado " + vec_encabezado.get(m).toString());
        	if (m == 0 || m ==3 || m==4 || m == 7 || m==8) {
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
	     
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        	}
        	else {
        	//	cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(vec_encabezado.get(m).toString())), helvb));
       	     cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong((vec_encabezado.get(m)).toString())), new Font(bf_helv)));
    	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
    	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        	}
	       	table.addCell(cell);
	       
	      	
	    }
        document.add(table);
        document.close();
        
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	session.removeAttribute("ejecucionRemuneracionExportar");
}catch(DocumentException e){
	e.printStackTrace();
}

%>