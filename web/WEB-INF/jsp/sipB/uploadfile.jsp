<%@ page language="java" import = "java.io.*,java.util.*,org.apache.commons.fileupload.FileItem,org.apache.commons.fileupload.FileUploadException,org.apache.commons.fileupload.servlet.ServletFileUpload,org.apache.commons.fileupload.disk.DiskFileItemFactory,org.apache.commons.fileupload.FileItemFactory,org.apache.poi.openxml4j.opc.OPCPackage,org.apache.poi.openxml4j.opc.PackageAccess,org.apache.poi.openxml4j.exceptions.*,org.xml.sax.*,javax.xml.parsers.*,upxls.*,org.apache.xmlbeans.*"
%>
 
<%
	System.out.println("UPLOAD: Inicia IMPORTACION"); 
	int anno = session.getAttribute("anno")!= null?Integer.parseInt(session.getAttribute("anno")+""):0;
 	int rutUsuario = session.getAttribute("rutUsuario")!= null?Integer.parseInt(session.getAttribute("rutUsuario")+""):0;

 	String carpeta = session.getAttribute("carpeta") != null &&
	!session.getAttribute("carpeta").toString().trim().equals("")?
	session.getAttribute("carpeta").toString():"";
 	
	if (rutUsuario > 0) {

    boolean exito = false;
    String mensaje = "";
 	String fieldName = "";
 	String fileName = "";
 	String contentType = "";
 	String extension = "";
 	
	
 	String dir = "/usr/local/siga_doc/sip/DOCUMENTO/"+carpeta+"/";

 	// Create a factory for disk-based file items
 	FileItemFactory factory = new DiskFileItemFactory();

 	// Create a new file upload handler
 	ServletFileUpload upload = new ServletFileUpload(factory);

 	// Parse the request
 	try {

 		List items = null;
 		/* FileItem */items = upload.parseRequest(request);

 		// Process the uploaded items
 		Iterator iter = items.iterator();
 		while (iter.hasNext()) {
 			FileItem item = (FileItem) iter.next();
 			if (!item.isFormField()) {
 				fieldName = item.getFieldName();
 				fileName = item.getName();
 				contentType = item.getContentType();
 				boolean isInMemory = item.isInMemory();
 				long sizeInBytes = item.getSize();
				System.out.println("UPLOAD: fieldName: " + fieldName);
				System.out.println("UPLOAD: fileName: " + fileName);
 				if (fileName.indexOf(".") > 0) {
 					extension = fileName.substring(fileName.indexOf(".") + 1, fileName.length());
 				}	

 				if (!(new File(dir).exists()))
 					(new File(dir)).mkdirs();

 				File uploadedFile = new File(dir + fileName);

	
 				try {
 					item.write(uploadedFile);
 					mensaje = "El archivo *" + fileName + "* ha sido importado.";
					exito = true;

 				} catch (Exception ex1) {
 					System.out.println("UPLOAD: error escribir " + ex1.toString());
 					mensaje = "ERROR 1. Existen problemas al cargar el archivo.";
 				}
 			}
 		}

 	} catch (FileUploadException ex) {
 		System.out.println("UPLOAD: error " + ex.toString());
 		mensaje = "ERROR 2. Existen problemas al cargar el archivo.";
 	}

 	
 	System.out.println("UPLOAD: Fin IMPORTACION");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="cache-control" VALUE="no-cache, no-store, must-revalidate">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Pragma" content="no-cache">
<meta name="keywords" content="" />
<meta name="description" content="" />

<title>SIIF - Sistema Integrado de Informaci&oacute;n Financiera</title>
<script>
function CargaLista(){
	opener.parent.cargarListaArch('<%=carpeta%>');
}
</script>
<style>
* {
	padding: 0px;
	margin: 0px;
}

body {
	font-size: 13px;
	font-family: "trebuchet ms", helvetica, sans-serif;
	color: #8C8C73;
	line-height: 18px;
}
#outer {
	position: relative;
	width: 99%;
	margin: 0 auto;
	border: solid 1px #E4E4E4;
	background-color: #ffffff;
}
#inner {
	position: relative;
	padding: 13px 30px 13px 30px;
	z-index: 3;
}
#footer {
	position: relative;
	clear: both;
	height: 35px;
	text-align: center;
	line-height: 35px;
	color: #A8A88D;
}
.buttonGris {
	text-align: center;
	COLOR: #000; text-shadow: 0 1px 0 #3e5a88;
	PADDING: 3px;
	MARGIN: 0px; 
}
</style> 
</head>
<body <%if (exito){%>onload="CargaLista()"<%}%>>
<div id="outer"><!--outer-->                               		
	<div id="inner"><!--inner--> 	

 			<div align="center">
 			<h3>Servicio de Importaci&oacute;n de Archivos</h3>
 			</div>
			<div style="text-align:center; margin:10px; padding:10px; border:dotted 1px #CCCCCC;">
				<font color='#990000'><%=mensaje%></font>	
			</div>	
			<center>
			<INPUT TYPE="BUTTON" NAME="Cerrar" VALUE="Cerrar" class="buttonGris" onClick="window.close();">
			</center>
			<br>
		<div id="footer"><!--footer-->
		&copy; Universidad T&eacute;cnica Federico Santa Mar&iacute;a
		</div><!--footer-->
    </div><!--inner--> 
</div><!--outer--> 
</body>
</html>
<%}%>	
	                     