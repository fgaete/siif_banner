<%@
page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%

response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 25, 20);/*.rotate() es para dejar la hoja horizontal*/
int rutUsuario = session.getAttribute("rutUsuario") != null?
        Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
String nomUnidad = request.getParameter("nomUnidad") != null &&
        !request.getParameter("nomUnidad").trim().equals("")?
         request.getParameter("nomUnidad"):"";  
String estamento = request.getParameter("estamento") != null &&
         !request.getParameter("estamento").trim().equals("")?
          request.getParameter("estamento"):"";   
int anno = request.getParameter("anno") != null &&
          !request.getParameter("anno").trim().equals("")?
           Integer.parseInt(request.getParameter("anno")):0;    
String mes = request.getParameter("mes") != null &&
           !request.getParameter("mes").trim().equals("")?
            request.getParameter("mes"):"";             
String nomitem = request.getParameter("item") != null &&
            !request.getParameter("item").trim().equals("")?
             request.getParameter("item"):"";                       
try{
	long total = 0;

	Vector vec_encabezado2 = new Vector();
    Vector vec_datos = new Vector();
    vec_datos = request.getSession().getAttribute("informeMovDisponibleExcel") != null?
    		    (Vector) request.getSession().getAttribute("informeMovDisponibleExcel"): new Vector();
    
    NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);		    
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 

	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(new Phrase("P�gina ", helv), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);

    String textoOpcion = "";
    textoOpcion = "DETALLE DE MOVIMIENTOS " ;
      
    HeaderFooter header = new HeaderFooter(new Phrase(textoOpcion, new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    //document.open();
    
    String nommes = "";
    int nummes = 0 ;
    if (mes.length() == 1 || mes.length() == 2) {
    	nummes	= Integer.parseInt(mes);
   
    switch(nummes){
	case 1 :nommes = "Enero";
	break;
	case 2: nommes = "Febrero";
	break;
	case 3:	nommes = "Marzo";
	break;
	case 4: nommes = "Abril";
	break;
	case 5: nommes = "Mayo";
	break;
	case 6:	nommes = "Junio";
	break;
	case 7:	nommes = "Julio";
	break;
	case 8:	nommes = "Agosto";
	break;
	case 9:	nommes = "Septiembre";
	break;
	case 10: nommes = "Octubre";
	break;
	case 11: nommes = "Noviembre";
	break;
	case 12: nommes = "Diciembre";
		}
    }
	
    // datos
    Vector vec_encabezado = new Vector();   
    float[] colsWidth = null;
    int largocols = 0;
    if(nomitem.equals("-1")) 
    vec_encabezado.addElement("CUENTA");
    vec_encabezado.addElement("MOVIMIENTO");
    vec_encabezado.addElement("N� DOC BANNER");
    vec_encabezado.addElement("TIPO DOCUMENTO");
    vec_encabezado.addElement("RUT");
    vec_encabezado.addElement("N�MERO DOCUMENTO");
    vec_encabezado.addElement("FECHA");
    vec_encabezado.addElement("IDENTIFICADOR");
    vec_encabezado.addElement("VALOR");
    vec_encabezado.addElement("ESTADO");
        colsWidth = new float[] {1.2f, 1f, 1f, 0.5f, 1f, 1f, 1f, 1.2f, 1f, 1f}; // Code 1
        largocols = 10;
  
    

    
        // add a table to the document
        document.open();
		
		PdfPTable table = new PdfPTable(colsWidth);
		table.setWidthPercentage(100); // Code 2
		table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
		
		PdfPCell cell = null;
		cell = new PdfPCell(new Paragraph("UNIDAD: " + nomUnidad , new Font(bf_helvb)));
		cell.setBorder(0);
		cell.setPaddingBottom(10);
	    cell.setColspan(largocols);
		table.addCell(cell);
		
		 cell = null;
		 cell = new PdfPCell(new Paragraph("Periodo: " + nommes + " " + anno , new Font(bf_helvb)));
		cell.setBorder(0);
		cell.setPaddingBottom(10);
	    cell.setColspan(largocols);
		table.addCell(cell);
		
		if(!nomitem.equals("-1")) {
		 cell = null;
		 cell = new PdfPCell(new Paragraph("Cuenta: " + nomitem  , new Font(bf_helvb)));
		cell.setBorder(0);
		cell.setPaddingBottom(10);
	    cell.setColspan(largocols);
		table.addCell(cell);
		}
		
		for (int m = 0 ; m < vec_encabezado.size(); m++){
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table.addCell(cell);
	      	
	    }
		Boolean continua = true;
		for (int j = 0 ; j < vec_datos.size(); j++){
		if(continua) {	
			for (int d = 0 ; d < largocols; d++){
				if (((Vector) vec_datos.get(j)).get(d).toString().equals("TOTAL") ) {
					continua = false;
					total = Long.parseLong (((Vector) vec_datos.get(j)).get(d + 1).toString());
					break;
				}
					//System.out.println("valor J "+ j + " valor D " + d +  " resultado  " + ((Vector) vec_datos.get(j)).get(d).toString() );					
				else 
				{
							
				if (d == 8) {
					cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector) vec_datos.get(j)).get(d).toString())), helv));
					cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				} else {
					cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(d).toString(), helv));
					cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				}
				
				}
			
	   			table.addCell(cell);      	
			} // for D
		} 
		else 
			break;// if continua
		} //for J
			
		
		document.add(table);
	
		 colsWidth = new float[]   {1.2f, 1f}; // Code 1
		 table = new PdfPTable(colsWidth);
	table.setWidthPercentage(100); // Code 2
	table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
        
		
	
	 /*totales*/
	
    	
 
	      	cell = new PdfPCell(new Paragraph("TOTAL", helvb));	     
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	       	table.addCell(cell);
    	     cell = new PdfPCell(new Paragraph(formatoNum.format(total), new Font(helvb)));
 	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
 	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
     	
	       	table.addCell(cell);
	       
	      	
	    
        document.add(table);
        document.close();
        
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	session.removeAttribute("listaDisponibilidadExcel");
}catch(DocumentException e){
	e.printStackTrace();
}

%>