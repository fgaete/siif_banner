<%@page import="com.sun.org.apache.bcel.internal.generic.NEWARRAY"%>
<%@page import="java.io.*,
        java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		java.util.List,
		java.text.NumberFormat"
%><%
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER, 20, 20, 25, 20);
int rutUsuario = session.getAttribute("rutUsuario") != null?
                 Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
try{
  
    
    Vector vecHistorial = request.getSession().getAttribute("vecHistorial") != null?
    	    	    	  (Vector) request.getSession().getAttribute("vecHistorial"): new Vector();
    String numSol = request.getSession().getAttribute("numSol") != null ?(String) request.getSession().getAttribute("numSol"):"";
    String rutnum = request.getSession().getAttribute("rutSolicitud") != null ?(String) request.getSession().getAttribute("rutSolicitud"):"";
    String codOrg = request.getSession().getAttribute("codOrg") != null ?(String) request.getSession().getAttribute("codOrg"):"";
    String nomOrg = request.getSession().getAttribute("nomOrg") != null ?(String) request.getSession().getAttribute("nomOrg"):"";
    String valorIncentivo = request.getSession().getAttribute("valorIncentivo") != null ?(String) request.getSession().getAttribute("valorIncentivo"):"";
    String numCuotas = request.getSession().getAttribute("numCuotas") != null ?(String) request.getSession().getAttribute("numCuotas"):"";
    String nombre = request.getSession().getAttribute("nombre") != null ?(String) request.getSession().getAttribute("nombre"):"";
    String mesPago = request.getSession().getAttribute("mesPago") != null ?(String) request.getSession().getAttribute("mesPago"):"";
    String a�oPago = request.getSession().getAttribute("annioPago") != null ?(String) request.getSession().getAttribute("annioPago"):"";
    String codHaber = request.getSession().getAttribute("codHaber") != null ?(String) request.getSession().getAttribute("codHaber"):"";
    String descHaber = request.getSession().getAttribute("descHaber") != null ?(String) request.getSession().getAttribute("descHaber"):"";
    String motivo = request.getSession().getAttribute("motivo") != null ?(String) request.getSession().getAttribute("motivo"):"";
    String detalle = request.getSession().getAttribute("detalle") != null ?(String) request.getSession().getAttribute("detalle"):"";
    String estado = request.getSession().getAttribute("estado") != null ?(String) request.getSession().getAttribute("estado"):"";
    String saldoCuenta = request.getSession().getAttribute("saldoCuenta") != null ?(String) request.getSession().getAttribute("saldoCuenta"):"";
    
  	NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);

    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    PdfWriter writer = PdfWriter.getInstance(document, buffer); 

    // various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
    BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    document.open();
    
    //Ejemplos de IMAGE
 	PdfPTable table = new PdfPTable(4);
 	table.setWidthPercentage(100); // Code 2
	table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3

    //a�adimos texto con formato a la primera celda
	PdfPCell celda = new PdfPCell(new Paragraph("Universidad T�cnica Federico Santa Mar�a  ",new Font(bf_helvb)));
	//unimos esta celda con otras 2
	celda.setColspan(4);
	//alineamos el contenido al centro
	celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	// a�adimos un espaciado
	celda.setPadding (12.0f);
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);

	//fila 2
	celda = new PdfPCell(new Paragraph("Direcci�n General de Finanzas ",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(4);
	celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	table.addCell(celda);
	
	
	// fila 6
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(4);
	table.addCell(celda);
	
	// fila 7
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(4);
	table.addCell(celda);
	
	if(!rutnum.trim().equals("")){
		
	celda = new PdfPCell(new Paragraph("N� de Solicitud ",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph (numSol));
	celda.setColspan(3);
	celda.setBorder(0);
	table.addCell(celda);	
		
    // fila 8
 	celda = new PdfPCell(new Paragraph("RUT Funcionario ",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph (rutnum));
	celda.setBorder(0);
	celda.setColspan(3);
	table.addCell(celda);
	}
	
    // fila 9	
	celda = new PdfPCell(new Paragraph("Nombre ",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph (nombre));
	celda.setColspan(3);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 10
	celda = new PdfPCell(new Paragraph("Organizaci�n ",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(codOrg + " - " + nomOrg));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph("Disponibilidad ",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(saldoCuenta));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	
	// fila 11
	celda = new PdfPCell(new Paragraph("Valor Incentivo ",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(valorIncentivo));
	celda.setColspan(3);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph("Cuotas ",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(numCuotas));
	celda.setColspan(3);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph("Inicio de Pago (Mes/A�o) ",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(mesPago + " - " + a�oPago));
	celda.setColspan(3);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph("Descripci�n ",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(motivo));
	celda.setColspan(3);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph("Detalle trabajo ",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(detalle));
	celda.setColspan(3);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph("Motivo del Pago ",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(codHaber + " - " + descHaber));
	celda.setColspan(3);
	celda.setBorder(0);
	table.addCell(celda);
	
			
	
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	table.addCell(celda);
			
	// historial
				
	celda = new PdfPCell(new Paragraph("Historial " , new Font(bf_helvb)));
	celda.setColspan(4);
	celda.setHorizontalAlignment(Element.ALIGN_LEFT);
	//celda.setPadding (12.0f);
	//celda.setBorder(0);
	//celda.setPaddingBottom(10);
	table.addCell(celda);
	
	float[] widths2 = {1f, 2f}; /*largo de las columnas, en este caso son dos*/
	PdfPTable table2 = new PdfPTable(widths2);
	table2.setWidthPercentage(100);
	    
	PdfPCell cell = null;
	//System.out.println("vecHistorial " + vecHistorial);
	if(vecHistorial!= null && vecHistorial.size() > 0){
		for(int i=0;i<vecHistorial.size();i++){
				Vector vec = (Vector) vecHistorial.get(i);
			 	cell = new PdfPCell(new Paragraph(vec.get(0).toString(), helv));
			 	table2.addCell(cell);
		       	cell = new PdfPCell(new Paragraph(vec.get(1).toString(),helvb));
			   	table2.addCell(cell);
			
		}
	}
	celda = new PdfPCell(table2);
	celda.setColspan(4);
	celda.setBorder(0);
	table.addCell(celda);
	// fin historial
			
			
	document.add(table);
	document.close();

	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	
	response.getOutputStream().flush();
	response.getOutputStream().close();

	session.removeAttribute("vecHistorial");
	session.removeAttribute("numSol");
	session.removeAttribute("rutSolicitud");
	session.removeAttribute("codOrg");
	session.removeAttribute("nomOrg");
	session.removeAttribute("valorIncentivo");
	session.removeAttribute("numCuotas");
	session.removeAttribute("nombre");
	session.removeAttribute("mesPago");
	session.removeAttribute("annioPago");
	session.removeAttribute("codHaber");
	session.removeAttribute("descHaber");
	session.removeAttribute("motivo");
	session.removeAttribute("detalle");
	session.removeAttribute("estado");
	session.removeAttribute("saldoCuenta");
}catch(DocumentException e){
	e.printStackTrace();
}

%>