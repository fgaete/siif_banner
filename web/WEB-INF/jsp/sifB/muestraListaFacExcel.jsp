<%@ page language="java" import="descad.presupuesto.*,
                                 java.util.*,
                                 java.io.*,
                                 descad.cliente.PreswBean,
                                 java.text.NumberFormat,
                                 org.apache.poi.ss.usermodel.DataFormat,
                                 org.apache.poi.poifs.filesystem.POIFSFileSystem,
                                 org.apache.poi.hssf.usermodel.*,                               
                                 org.apache.poi.ss.util.*,
                                 org.apache.poi.ss.usermodel.CellStyle"
                                 contentType="application/vnd.ms-excel"%><%

  // Creado por           : M�nica Barrera F.
  // Fecha                : 20/08/2012
  // �ltima Actualizaci�n :

  int rutUsuario = session.getAttribute("rutUsuario") != null?
                    Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
   
  if(rutUsuario > 0) {

	String organizacion = session.getAttribute("nomOrganizacion") != null ?
                session.getAttribute("nomOrganizacion")+"":"";
                                                  
   	String estado = session.getAttribute("nomEstado") != null ?
   			        session.getAttribute("nomEstado")+"":"";                        
   	String fechaIni = request.getParameter("fechaIni") != null &&
                            !request.getParameter("fechaIni").trim().equals("")?
                             request.getParameter("fechaIni"):"";  
  	String fechaTer = request.getParameter("fechaTer") != null &&
                             !request.getParameter("fechaTer").trim().equals("")?
                              request.getParameter("fechaTer"):""; 
                              
                      	

    Vector vec_datos = new Vector();
    vec_datos = request.getSession().getAttribute("listadoFacturacion") != null ?
    		   (Vector) request.getSession().getAttribute("listadoFacturacion"):new Vector();
    		   
    NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);
	//System.out.println("vec_datos: "+vec_datos);   
    if (vec_datos.size() > 0 ) {
      
      String archivo = "Consulta General";
      String titulo = "Consulta General de Solicitudes de Facturaci�n";
      String subtitulo2 = "Estado: " + estado  ; 
      String subtitulo3 = "Organizacion: " + ((organizacion.trim().equals("-"))?"Todas":organizacion); 
      String subtitulo4 = (!fechaIni.trim().equals(""))?("Entre fechas: " + fechaIni + " - " + fechaTer):"";
      String subtitulo5 = "";

          
         //fecha
      java.util.Calendar f = new java.util.GregorianCalendar();
      String diaActual = String.valueOf(f.get(java.util.Calendar.DAY_OF_MONTH));
      String mesActual = String.valueOf(f.get(java.util.Calendar.MONTH) + 1);
      String a�oActual = String.valueOf(f.get(java.util.Calendar.YEAR));
      String hora      = String.valueOf(f.get(java.util.Calendar.HOUR_OF_DAY));
      String min       = String.valueOf(f.get(java.util.Calendar.MINUTE));
      if (hora.length() == 1) hora = "0"+hora;
      if (min.length()  == 1) min  = "0"+min;

      try {
        int fila = 0;

        ServletOutputStream sos = response.getOutputStream();
        ByteArrayOutputStream OUT = new ByteArrayOutputStream();

        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet1 = wb.createSheet(titulo);

        // Estilo Arial-BOLD
        HSSFFont font1 = wb.createFont();
        font1.setFontHeightInPoints((short)10);
        font1.setFontName("Arial");
        font1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        HSSFCellStyle style_bold = wb.createCellStyle();
        style_bold.setFont(font1);

        // Estilo Arial-ITALICA
        HSSFFont font2 = wb.createFont();
        font2.setFontHeightInPoints((short)10);
        font2.setFontName("Arial");
        font2.setItalic(true);
        HSSFCellStyle style_italica = wb.createCellStyle();
        style_italica.setFont(font2);

        // Estilo FECHA
        HSSFCellStyle style_fecha = wb.createCellStyle();
        style_fecha.setDataFormat(HSSFDataFormat.getBuiltinFormat("d/m/yy h:mm"));

        // Estilo BORDE BOLD
        HSSFCellStyle style_borde_bold= wb.createCellStyle();
        style_borde_bold.setBorderBottom((short)1);
        style_borde_bold.setBorderLeft((short)1);
        style_borde_bold.setBorderRight((short)1);
        style_borde_bold.setBorderTop((short)1);
        style_borde_bold.setAlignment((short)2);
        style_borde_bold.setFont(font1);

        // Estilo BORDE
        HSSFCellStyle style_borde= wb.createCellStyle();
        style_borde.setBorderBottom((short)1);
        style_borde.setBorderLeft((short)1);
        style_borde.setBorderRight((short)1);
        style_borde.setBorderTop((short)1);
        style_borde.setAlignment((short)1);

        // Estilo BORDE
        HSSFCellStyle style_borde_centro= wb.createCellStyle();
        style_borde_centro.setBorderBottom((short)1);
        style_borde_centro.setBorderLeft((short)1);
        style_borde_centro.setBorderRight((short)1);
        style_borde_centro.setBorderTop((short)1);
        style_borde_centro.setAlignment((short)2);

        // Estilo BORDE derecho
        HSSFCellStyle style_borde_derecho= wb.createCellStyle();
        style_borde_derecho.setBorderBottom((short)1);
        style_borde_derecho.setBorderLeft((short)1);
        style_borde_derecho.setBorderRight((short)1);
        style_borde_derecho.setBorderTop((short)1);
        style_borde_derecho.setAlignment((short)3);
        
        // Estilo numerico
        DataFormat format = wb.createDataFormat();
        CellStyle style_numerico= wb.createCellStyle();
        style_numerico = wb.createCellStyle();
        style_numerico.setDataFormat(format.getFormat("##,###,###,###,##0"));        
        style_numerico.setBorderBottom((short)1);
        style_numerico.setBorderLeft((short)1);
        style_numerico.setBorderRight((short)1);
        style_numerico.setBorderTop((short)1);
        style_numerico.setAlignment((short)3);
        
        
        // T�tulo
        HSSFRow row_tit = sheet1.createRow((short)fila);
        HSSFCell cell_tit = row_tit.createCell((short)0);
        row_tit.createCell((short)0);
        cell_tit.setCellValue(titulo);
        cell_tit.setCellStyle(style_bold);

        fila++;
        // subT�tulo
        HSSFRow row_tit2 = sheet1.createRow((short)fila);
        HSSFCell cell_tit2 = row_tit2.createCell((short)0);
        row_tit2.createCell((short)0);
        cell_tit2.setCellValue(subtitulo2);
        cell_tit2.setCellStyle(style_bold);

        fila++;
        // subT�tulo3
        HSSFRow row_tit3 = sheet1.createRow((short)fila);
        HSSFCell cell_tit3 = row_tit3.createCell((short)0);
        row_tit3.createCell((short)0);
        cell_tit3.setCellValue(subtitulo3);
        cell_tit3.setCellStyle(style_bold);

        fila++;
        // subT�tulo4
        HSSFRow row_tit5 = sheet1.createRow((short)fila);
        HSSFCell cell_tit5 = row_tit5.createCell((short)0);
        row_tit5.createCell((short)0);
        cell_tit5.setCellValue(subtitulo4);
        cell_tit5.setCellStyle(style_bold);
        
        fila++;
        // subT�tulo5
        HSSFRow row_tit6 = sheet1.createRow((short)fila);
        HSSFCell cell_tit6 = row_tit6.createCell((short)0);
        row_tit6.createCell((short)0);
        cell_tit6.setCellValue(subtitulo5);
        cell_tit6.setCellStyle(style_bold);
        
        fila++;
        HSSFRow row_tit4 = sheet1.createRow((short)fila);
        HSSFCell cell_tit4 = row_tit4.createCell((short)0);
        row_tit4.createCell((short)0);
        cell_tit4.setCellValue("Generado el " + diaActual + "/" + mesActual + "/" + a�oActual +
        " " + hora +":" + min);
        cell_tit4.setCellStyle(style_italica);

        fila = fila + 2;

        Vector vec_lista = new Vector();
        Vector vec_encabezado = new Vector();

        
	    vec_encabezado.addElement("N� Interno");	    
	    vec_encabezado.addElement("Fecha");	   
	    vec_encabezado.addElement("Cliente");	    
	    vec_encabezado.addElement("Glosa");
	    vec_encabezado.addElement("Valor");
	    vec_encabezado.addElement("Estado Documento");
	    vec_encabezado.addElement("Tipo Factura");
	    vec_encabezado.addElement("Factura");
	    vec_encabezado.addElement("Estado Pago");
	    vec_encabezado.addElement("Saldo");
	    vec_encabezado.addElement("Valor Pagado");
	    vec_encabezado.addElement("Monto pagado con Cheques");
	    vec_encabezado.addElement("Cheques pendiente por Depositar");
	

	    
	    //Cabecera1 de la tabla
	    int fila_enc = fila++;
	   
        

        
        //Cabecera2 de la tabla
        fila_enc = fila++;
        HSSFRow row_e1 = sheet1.createRow((short)fila_enc);

       

        
        for (int m = 0; m < vec_encabezado.size(); m++){
          
          HSSFCell cell_e1 = row_e1.createCell((short)m);
          String columname = vec_encabezado.get(m)+"";
          row_e1.createCell((short)m);
          cell_e1.setCellValue(columname);
          cell_e1.setCellStyle(style_borde_bold);
        }
        
       
        
        for (int i = 0; i < vec_datos.size(); i++){  
          vec_lista = new Vector();
         
       	    vec_lista.addElement(((Vector)vec_datos.get(i)).get(0)+"");/*N� Interno*/
           	vec_lista.addElement(((Vector)vec_datos.get(i)).get(1)+"");/*Fecha*/
          	vec_lista.addElement(((Vector)vec_datos.get(i)).get(2)+"");/*Cliente*/
           	vec_lista.addElement(((Vector)vec_datos.get(i)).get(3)+"");/*Glosa*/
          	vec_lista.addElement(((Vector)vec_datos.get(i)).get(4)+"");/*Valor*/
        	vec_lista.addElement(((Vector)vec_datos.get(i)).get(5)+"");/*Estado Documento*/
          	vec_lista.addElement(((Vector)vec_datos.get(i)).get(6)+"");/*Tipo Factura*/
          	vec_lista.addElement(((Vector)vec_datos.get(i)).get(7)+"");/*Factura*/
          	vec_lista.addElement(((Vector)vec_datos.get(i)).get(8)+"");/*Estado Pago*/
          	vec_lista.addElement(((Vector)vec_datos.get(i)).get(9)+"");/*Saldo*/
         	vec_lista.addElement(((Vector)vec_datos.get(i)).get(10)+"");/*Valor Pagado*/
         	vec_lista.addElement(((Vector)vec_datos.get(i)).get(11)+"");/*Monto pagado con Cheques*/
         	vec_lista.addElement(((Vector)vec_datos.get(i)).get(12)+"");/*Cheques pendiente por Depositar*/
         	
          	//System.out.println("vec_lista: "+vec_lista);
         	// Contenido de la tabla
         	HSSFRow row = sheet1.createRow((short)fila++);
         	int columna = 0;
          
         	for (int v = 0 ; v < vec_lista.size(); v++){
        		columna ++;     
        		//System.out.println("vec_lista.get(v): "+vec_lista.get(v));
            	HSSFCell cel00 = row.createCell((short)v);
            	
                if (v == 4 || v > 8){
	              	cel00.setCellValue(Long.parseLong(vec_lista.get(v)+""));
	        		cel00.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
	            	cel00.setCellStyle(style_numerico);
	                }
                else
                	cel00.setCellStyle(style_borde);
            	cel00.setCellValue(vec_lista.get(v)+"");
         
                 
          	}
      
        }
        //System.out.println("f: "+fila);
        CellRangeAddress region3 = new CellRangeAddress(fila-1 /*firstRow*/, fila-1 /*lastRow*/, 0 /*firstCol*/, 1 /*lastCol*/) ;
        sheet1.addMergedRegion(region3);

        
            	
    	// autosize each column
        for (int m = 0 ; m < vec_encabezado.size(); m++){	
          	sheet1.autoSizeColumn(m, true);  
          if (m > 1 )
        	  if(m > 1)
        		  if(m == 2 || m == 3 || m == 5)
            		  sheet1.setColumnWidth((short)m,(short)(8000)); 
        		  else
        		  	  sheet1.setColumnWidth((short)m,(short)(4000)); 
         
        	  
        }


    	
               
        wb.write(OUT); // se escribe el xls en un ByteArrayOutputStream
        
        response.setContentLength(OUT.size());
        response.setContentType ("application/x-download");
    	response.setHeader("Content-Disposition", "attachment;filename="+archivo+".xls;");

        //Se pone el fichero en la salida
        byte[] bufferExcel = OUT.toByteArray();
        sos.write(bufferExcel,0,bufferExcel.length);

        //Se muestra la salida.
        sos.flush();
        sos.close();
        OUT.close();
        if(request.getSession().getAttribute("listadoFacturacion") != null)
        	session.removeAttribute("listadoFacturacion");
        session.removeAttribute("nomOrganizacion");
        session.removeAttribute("nomEstado");
    	return;    

        }
      catch ( IOException ex ) {
        ex.printStackTrace();
      }
    }
  }
%>