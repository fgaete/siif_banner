<%@ page language="java" contentType='text/html;charset=iso-8859-1'%>
<%@ page import = "descad.presupuesto.Presw25DTO"%>
<%@ page import = "java.io.*" %>
<%@ page import = "java.util.*"%>
<%@ page import = "java.util.List"%>
<%@ page import = "java.util.Vector"%>
<%@ page import = "org.apache.commons.fileupload.FileItem"%>
<%@ page import = "org.apache.commons.fileupload.FileUploadException"%>
<%@ page import = "org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@ page import = "org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@ page import = "org.apache.commons.fileupload.FileItemFactory"%>
<%@ page import = "org.xml.sax.*"%>
<%@ page import = "javax.xml.parsers.*"%>
<%@ page import = "upxls.ProcessTesoreria_xls"%><%

System.out.println("entra en uploadxls"); 
	System.out.println("UPLOAD: Inicia IMPORTACION"); 
 	boolean exito = false;
    String mensaje = "";
 	String fieldName = "";
 	String fileName = "";
 	String contentType = "";
 	String extension = "";
 	String dir = "";
 	
 	// para icalma
	String dirb = "/usr/local/siga_doc/saf/";
	String dirp = "/usr/local/siga_doc/saf/";
 	// para prueba local
	//String dirb = "";
	//String dirp = "C:/usr/local/siga_doc/saf/"; 	
%>
<%
    String documento = session.getAttribute("documento")!= null?session.getAttribute("documento")+"":"";
 	int anno = session.getAttribute("anno")!= null?Integer.parseInt(session.getAttribute("anno")+""):0;
	int rutUsuario = session.getAttribute("rutUsuario")!= null?Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
 	String dv = session.getAttribute("dv") != null? session.getAttribute("dv")+"":"";
 	int tippro = session.getAttribute("tippro") != null? Integer.parseInt(session.getAttribute("tippro")+""):1;
	Vector salida_source = null;
	Vector vec1= new Vector();
	Vector vec2= new Vector();
 	Vector resultadoImport;
 	
	if(session.getAttribute("resultadoImport") != null) {
    	session.setAttribute("resultadoImport", null);
    	session.removeAttribute("resultadoImport");
    }
 
	Calendar fecha = Calendar.getInstance();
	String mes = "";
	String dia = "";
	String a�o = String.valueOf(fecha.get(Calendar.YEAR));
	if (fecha.get(Calendar.MONTH) < 8)
		mes = "0"+ String.valueOf(fecha.get(Calendar.MONTH) + 1);
	else
		mes = String.valueOf(fecha.get(Calendar.MONTH) + 1);
	if (fecha.get(Calendar.DAY_OF_MONTH) < 10)
		dia = "0"+ String.valueOf(fecha.get(Calendar.DAY_OF_MONTH));
	else
		dia = String.valueOf(fecha.get(Calendar.DAY_OF_MONTH));
	
	dir = dirb + a�o + "/" + mes + "/" + dia + "/";
			
	String hora = String.valueOf(fecha.get(Calendar.HOUR_OF_DAY));
	String minutos = String.valueOf(fecha.get(Calendar.MINUTE));
	String segundos = String.valueOf(fecha.get(Calendar.SECOND));

	String archivo = documento + "_" + rutUsuario + "_" + 	hora + "" + minutos + "" + segundos; 
    if(documento.equals(""))	
		archivo = documento + "_" + rutUsuario + "_" + 	hora + "" + minutos + "" + segundos; 

	System.out.println("UPLOAD: archivo a subir: " + archivo);	
	
 
 	// Create a factory for disk-based file items
 	FileItemFactory factory = new DiskFileItemFactory();

 	// Create a new file upload handler
 	ServletFileUpload upload = new ServletFileUpload(factory);

 	// Parse the request
 	try {

 		List items = null;
 		/* FileItem */items = upload.parseRequest(request);

 		// Process the uploaded items
 		Iterator iter = items.iterator();
 		while (iter.hasNext()) {
 			FileItem item = (FileItem) iter.next();
 			if (!item.isFormField()) {
 				fieldName = item.getFieldName();
 				fileName = item.getName();
 				contentType = item.getContentType();
 				boolean isInMemory = item.isInMemory();
 				long sizeInBytes = item.getSize();
				System.out.println("UPLOAD: fieldName: " + fieldName);
				System.out.println("UPLOAD: fileName: " + fileName);
 				if (fileName.indexOf(".") > 0) {
 					extension = fileName.substring(fileName.indexOf(".") + 1, fileName.length());
 					//System.out.println("extension "+ extension);
 				}	

 				if (!(new File(dir).exists()))
 					(new File(dir)).mkdirs();

 				File uploadedFile = null;
 				if (extension .equals("xlsx")) {
 					uploadedFile = new File(dir + archivo + ".xlsx");
 				} else {
 					uploadedFile = new File(dir + archivo + ".xls");
 				}
	
 				try {
 					item.write(uploadedFile);

 				} catch (Exception ex1) {
 					System.out.println("UPLOAD: error escribir " + ex1.toString());
 					mensaje = "ERROR 1. Existen problemas al cargar el archivo.";
 				}
 			}
 		}

 	} catch (FileUploadException ex) {
 		System.out.println("UPLOAD: error " + ex.toString());
 		mensaje = "ERROR 2. Existen problemas al cargar el archivo.";
 	}
  
 	try {
 		System.out.println("index: "+fileName.indexOf(".") );
 		if (fileName.indexOf(".") > 0) {
 		
 		    // PROCESA XLSX
 			if (extension .equals("xlsx")) {
 				ProcessTesoreria_xls procesarxls = null;
 				if(tippro == 1) { // liberacion
		 				System.out.println("UPLOAD: Inicia PROCESA XLSX: " +
								dir + archivo + ".xlsx  " ); 
		 				
		 				procesarxls = new ProcessTesoreria_xls(dir + archivo + ".xlsx",
		 						dir + archivo + ".xlsx", 1, 1);
		 		
 				}  
 			
 				System.out.println("UPLOAD: Fin PROCESA XLSX");
 				//salida_source = procesarxls.Source();
 				vec1= new Vector(); 
 				vec2= new Vector(); 
 			//	boolean iguales = procesarxls.Equals();
				
 			//	System.out.println("salida_source: "+salida_source.size());
 				//if (iguales)
 					try {						
 						vec1 = procesarxls.Source();
 						System.out.println("vec1: "+vec1);
 						
 						session.setAttribute("resultadoImport", vec1);
 						mensaje = "El archivo XLSX ha sido importado.<br><br><b>Por favor, REVISE la informaci&oacute;n luego de presionar boton 'Registrar en Banner', para que se haga efectivo.</b>";
 	 	 	 			exito = true;
 							
					System.out.println("UPLOAD: Fin PROCESA XLSX"+vec1.size());
 					}
					catch (NumberFormatException e){
						mensaje = "ERROR. El archivo XLSX presenta problemas de conversi&oacute;n de datos.";
						System.out.println(e.toString());	
					}
					
					// PROCESA XLS
 			} else {
 				ProcessTesoreria_xls procesarxls = null;
 				if(tippro == 1) { // liberacion
 			 		
				System.out.println("UPLOAD: Inicia PROCESA XLS: " +
						dir + archivo + ".xls  " ); 				
 		 		//System.out.println("comienza xls 1: "+extension);
 		 		
 		    	procesarxls = new ProcessTesoreria_xls(dir + archivo + ".xls",	
 						dir + archivo + ".xls", 1, 1);
 		    	
 		       }
 				//boolean iguales = procesarxls.Equals();
				
 				//if (iguales)
 					try {						
 						vec1 = procesarxls.Source();
 						
  						session.setAttribute("resultadoImport", vec1);
  						System.out.println("vec1: "+vec2);
 						mensaje = "El archivo XLSX ha sido importado.<br><br><b>Por favor, REVISE la informaci&oacute;n para que se registre en Banner.</b>";
 			 	 	 	exito = true;
 							
					System.out.println("UPLOAD: Fin PROCESA XLSX");
 					}
					catch (NumberFormatException e){
						mensaje = "ERROR. El archivo XLS presenta problemas de conversi&oacute;n de datos.";
						System.out.println(e.toString());	
					}
 		
 			
 		}
 		} else
 		     System.out.println("sali8da: ");
 	} catch (Exception ex1) {
 		System.out.println("UPLOAD: error " + ex1.toString());
 		mensaje = "ERROR. El archivo no pudo ser importado.";

 	}
 	
 	System.out.println("UPLOAD: Fin IMPORTACION");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="cache-control" VALUE="no-cache, no-store, must-revalidate">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Pragma" content="no-cache">
<meta name="keywords" content="" />
<meta name="description" content="" />

<title>SIP - Sistema de Informe de Presupuestos</title>
<script>

function CargaPagina(){

	opener.parent.cargaDocumento();
}

</script>
<style>
* {
	padding: 0px;
	margin: 0px;
}

body {
	font-size: 13px;
	font-family: "trebuchet ms", helvetica, sans-serif;
	color: #8C8C73;
	line-height: 18px;
}
#outer {
	position: relative;
	width: 99%;
	margin: 0 auto;
	border: solid 1px #E4E4E4;
	background-color: #ffffff;
}
#inner {
	position: relative;
	padding: 13px 30px 13px 30px;
	z-index: 3;
}
#footer {
	position: relative;
	clear: both;
	height: 35px;
	text-align: center;
	line-height: 35px;
	color: #A8A88D;
}
.buttonGris {
	text-align: center;
	COLOR: #000; text-shadow: 0 1px 0 #3e5a88;
	PADDING: 3px;
	MARGIN: 0px; 
}
</style> 
</head>
<body <%if (exito){%>onload="CargaPagina()"<%}%>>
<div id="outer"><!--outer-->                               		
	<div id="inner"><!--inner--> 	

 			<div align="center">
 			<h3>Servicio de Importaci&oacute;n de Archivos</h3>
 			</div>
			<div style="text-align:center; margin:10px; padding:10px; border:dotted 1px #CCCCCC;">
				<font color='#990000'><%=mensaje%></font>	
			</div>	
			<center>
			<INPUT TYPE="BUTTON" NAME="Cerrar" VALUE="Cerrar" class="buttonGris" onClick="window.close();">
			</center>
			<br>
		<div id="footer"><!--footer-->
		&copy; Universidad T&eacute;cnica Federico Santa Mar&iacute;a
		</div><!--footer-->
    </div><!--inner--> 
</div><!--outer--> 
</body>
</html>              