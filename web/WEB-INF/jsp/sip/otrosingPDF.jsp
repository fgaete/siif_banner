<%@page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%

int rutUsuario = session.getAttribute("rutUsuario") != null?
        Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
int anno     = request.getParameter("anno") != null &&
                !request.getParameter("anno").trim().equals("")?
                Integer.parseInt(request.getParameter("anno")):0;
String funcionario  = request.getParameter("funcionario") != null &&
                !request.getParameter("funcionario").trim().equals("")?
                request.getParameter("funcionario"):"";
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER/*.rotate()*/, 20, 20, 25, 20);/*.rotate() es para dejar la hoja horizontal*/

try{
    // datos
    Vector vec_encabezado = new Vector();    
    vec_encabezado.addElement("Concepto");
    vec_encabezado.addElement("Valor Venta $");
    vec_encabezado.addElement("Observaciones");    

    long total = 0;
    Vector vec_datos = new Vector();
    vec_datos = request.getSession().getAttribute("listaOtrosIngPDF") != null?
    		    (Vector) request.getSession().getAttribute("listaOtrosIngPDF"): new Vector();
    
    NumberFormat nf2 = 	NumberFormat.getInstance(Locale.GERMAN);

	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 

	document.addTitle("PRESUPUESTACIÓN OTROS NEGOCIOS "+anno);
	document.addAuthor(funcionario.trim());
	document.addSubject("");	
	
	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(
                new Phrase("Página ", new Font(bf_helv)), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);

    HeaderFooter header = new HeaderFooter(
                new Phrase("PRESUPUESTACIÓN OTROS NEGOCIOS " + anno, new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    document.open();
	

 // add a table to the document
 	float[] widths = {0.1f, 0.035f, 0.1f}; /*largo de las columnas, en este caso son tres*/
	PdfPTable table = new PdfPTable(widths);
    table.setWidthPercentage(100);
    table.setHeaderRows(1);/* cantidad de filas cabeceras para que se repitan en cada hoja*/
    
    PdfPCell cell = null;
    int columna = 0;
    
	//Cabecera de la tabla    
    for (int m = 0 ; m < vec_encabezado.size(); m++){
    	columna ++;
      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), new Font(bf_helvb)));
      	cell.setBackgroundColor(Color.LIGHT_GRAY);
      	if (columna == 2) {
      		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
      	}
      	table.addCell(cell);
      	if (columna == 3) columna = 0;
    }

    //Contenido de la tabla
    columna = 0;

    for (int d = 0 ; d < vec_datos.size(); d++){   
    	if (d == vec_datos.size()-1) {   		
    		total = Long.parseLong(vec_datos.get(d).toString());
    	}
    	else {
	    	columna ++;      	
	    	cell = new PdfPCell(new Paragraph(vec_datos.get(d).toString(), new Font(bf_helv)));
	      	if (columna == 2) {
	      		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	      	}
	      	table.addCell(cell);      	
	      	if (columna == 3) columna = 0;
    	}
    }    

    // Totales
    cell = new PdfPCell(new Paragraph("TOTAL", new Font(bf_helvb)));
    table.addCell(cell);
    
    cell = new PdfPCell(new Paragraph(nf2.format(total), new Font(bf_helvb)));
    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    table.addCell(cell);


    cell = new PdfPCell(new Paragraph(""));
    cell.setBorder(0);
    table.addCell(cell);   
    
    /*document.newPage();*/

    document.add(table);  
    document.close();
    session.removeAttribute("listaOtrosIngPDF");
	

	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	
}catch(DocumentException e){
	e.printStackTrace();
}
%>