<%@ page language="java" import = "java.io.*,java.util.*, java.text.SimpleDateFormat, org.apache.commons.fileupload.FileItem,org.apache.commons.fileupload.FileUploadException,org.apache.commons.fileupload.servlet.ServletFileUpload,org.apache.commons.fileupload.disk.DiskFileItemFactory,org.apache.commons.fileupload.FileItemFactory,org.apache.poi.openxml4j.opc.OPCPackage,org.apache.poi.openxml4j.opc.PackageAccess,org.apache.poi.openxml4j.exceptions.*,org.xml.sax.*,javax.xml.parsers.*,upxls.*,org.apache.xmlbeans.*"
%><%
   int rutUsuario = session.getAttribute("rutUsuario")!= null?Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
   if (rutUsuario > 0) {

	String annoDIR = request.getParameter("dir") != null &&
	!request.getParameter("dir").trim().equals("")?
	request.getParameter("dir"):"0";
	String admin = request.getParameter("admin") != null &&
	!request.getParameter("admin").trim().equals("")?
			request.getParameter("admin"):"";

	String imgXLS = "../sgf/img/32x16_xls.gif";
	String imgDOC = "../sgf/img/32x16_doc.gif";
	String imgPDF = "../sgf/img/32x16_pdf.gif";
	String imgTXT = "../sgf/img/32x16_txt.gif";
	String imgPPT = "../sgf/img/32x16_ppt.gif";
	String imgZIP = "../sgf/img/32x16_zip.gif";
	String imgRAR = "../sgf/img/32x16_rar.gif";
	String imgFILE = "../sgf/img/32x16_file.gif";
	String imgDIR = "../sgf/img/32x16_dir.gif";
	String img = "";
	
	String directorio = "/usr/local/siga_doc/sip/DOCUMENTO/" + annoDIR;
	
	File dir = new File(directorio);
	
	String[] children = dir.list();
	if (children == null) {
		// Either dir does not exist or is not a directory
	} else {
		for (int i=0; i<children.length; i++) {
		// Get filename of file or directory
			String filename = children[i];
		}
	}

	// It is also possible to filter the list of returned files.
	// This example does not return any files that start with `.'.
	FilenameFilter filter = new FilenameFilter() {
		public boolean accept(File dir, String name) {
			return !name.startsWith(".");
		}
	};
	children = dir.list(filter);


	// The list of files can also be retrieved as File objects
	File[] files = dir.listFiles();

	// This filter only returns directories
	FileFilter fileFilter = new FileFilter() {
		public boolean accept(File file) {
			return file.isDirectory();
		}
	};
	files = dir.listFiles(fileFilter);


	String[] listaArchivos=children;
	String extension = "";
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy',&nbsp;' HH:mm");

	       

if (listaArchivos != null && listaArchivos.length > 0) { %>
<div style="padding-top:10px; padding-bottom:5px">Documentos de la carpeta <%=annoDIR%></div>
<table cellpadding="0" cellspacing="8">
	<tbody>
<!-- folders -->
<!-- /folders -->
<!-- files -->
			<% for(int i=0; i<listaArchivos.length; i++){
	        	File nombre = new File(dir + "/" + listaArchivos[i]);
	        	long filesize = nombre.length(); 
	        	String size = filesize +  " B";
	        	
	        	if (filesize  >= 1024) { 
	        		size = (filesize / 1024) + " KB";
	        		if ((filesize / 1024)  >= 1024)
	        			size = ((filesize / 1024)/1024) + " MB";
	        	}
	        		
	        	
	        	Date fecha = new Date(nombre.lastModified());

	        	if(!nombre.isDirectory())  {
	        		if (listaArchivos[i].indexOf(".") > 0) {
	 					extension = listaArchivos[i].substring(listaArchivos[i].indexOf(".") + 1, listaArchivos[i].length());
	 					extension = extension.toUpperCase();
	 					if (extension.equals("XLS") || extension.equals("XLSX")) img = imgXLS;
	 					if (extension.equals("DOC") || extension.equals("DOCX")) img = imgDOC;
	 					if (extension.equals("PDF")) img = imgPDF;
	 					if (extension.equals("TXT")) img = imgTXT;
	 					if (extension.equals("PPT")) img = imgPPT;
	 					if (extension.equals("ZIP")) img = imgZIP;
	 					if (extension.equals("RAR")) img = imgRAR;
	 					if (img.equals("")) img = imgFILE;
	 				}	

	        	}%>
		<tr>
			<td nowrap="nowrap" width="200"><img src="<%=img%>" align="ABSMIDDLE" /> <a href="#" onclick="downloadArch('<%=listaArchivos[i]%>','<%=annoDIR%>'); return false;"><%=listaArchivos[i]%></a></td>
			<td nowrap="nowrap" width="100" align="rigth"><%=size%></td>
			<td><%=sdf.format(fecha)%></td>
		    <% if(admin.trim().equals("1")) {%>
			<td><a href="#" onclick="deleteArch('<%=listaArchivos[i]%>','<%=annoDIR%>'); return false;"><img alt="Eliminar" src="../sgf/img/eliminar.gif" align="ABSMIDDLE" border="0" /></a></td>
			<%} %>
			<td><a href="#" onclick="downloadArch('<%=listaArchivos[i]%>','<%=annoDIR%>'); return false;"><img alt="Download" src="../sgf/img/down2.gif" align="ABSMIDDLE" border="0" /></a></td>
			
        </tr>
<%}%>	    
<!-- /files -->
	</tbody>                          
</table>

<%} else {%>
<div style="padding-top:10px; padding-bottom:5px">NO EXISTEN DOCUMENTOS EN lA CARPETA <b><%=annoDIR%></b>.</div>
<%}}%>

