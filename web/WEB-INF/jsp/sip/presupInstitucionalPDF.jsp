<%@
page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%
// Creado por           : M�nica Barrera F.
// Fecha                : 02/05/2011
// �ltima Actualizaci�n :
	
	
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 25, 20);/*.rotate() es para dejar la hoja horizontal*/
int rutUsuario = session.getAttribute("rutUsuario") != null?
                 Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
                        
Vector vec_datos = new Vector();
vec_datos = request.getSession().getAttribute("listadoPresupSerie") != null ?
	   (Vector) request.getSession().getAttribute("listadoPresupSerie"):new Vector();
String tipo = request.getParameter("tipo")	;
int anno = request.getParameter("anno") != null &&
!request.getParameter("anno").trim().equals("")?
	Integer.parseInt(request.getParameter("anno")):0;
int expresado = request.getParameter("expresado") != null &&
	!request.getParameter("expresado").trim().equals("")?
		Integer.parseInt(request.getParameter("expresado")):0;

try{
  
    
    NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);		    
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 

	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(new Phrase("P�gina ", helv), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);
    String subtitulo2 = "" ; 
    String subtitulo3 = "" ;
    String textoOpcion = "";
    String subtitulo4 = "" ;
    textoOpcion = "Series de Tiempo e Informaci�n Estad�stica ";
    if(tipo.trim().equals("PIF"))
  	  subtitulo2 = "Presupuesto Institucional de Fondos Centrales " + anno;
    if(tipo.trim().equals("FGB"))
  	  subtitulo2 = "Presupuesto Institucional Global " + anno ;
    if(tipo.trim().equals("STP")){
    	subtitulo2 = "Ingresos de Pregrado" ;
    	String nomSede = request.getParameter("nomSede") != null &&
    			!request.getParameter("nomSede").trim().equals("")?
     			request.getParameter("nomSede"):"";
  		String nomDepartamento = request.getParameter("nomDepartamento") != null &&
    			!request.getParameter("nomDepartamento").trim().equals("")?
     			request.getParameter("nomDepartamento"):"";
  		String nomCarrera = request.getParameter("nomCarrera") != null &&
    			!request.getParameter("nomCarrera").trim().equals("")?
     			request.getParameter("nomCarrera"):"";
  		String nomMencion = request.getParameter("nomMencion") != null &&
    			!request.getParameter("nomMencion").trim().equals("")?
     			request.getParameter("nomMencion"):"";
      	subtitulo3 = "Sede " + nomSede.trim() + ", Departamento " + nomDepartamento.trim() + ", Carrera " + nomCarrera.trim() +", Menci�n " + nomMencion.trim();
        subtitulo4 =(expresado == 1)?"Expresado en Pesos":"Expresado en Porcentaje " ;
      }
      
    HeaderFooter header = new HeaderFooter(new Phrase(textoOpcion, new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    //document.open();
	
    // datos
    Vector vec_encabezado = new Vector();   
    float[] colsWidth = null;
    int largocols = 0;
  
    if(!tipo.trim().equals("STP")) {
    vec_encabezado.addElement("Ingresos"); 
    for (int i = (anno - 5); i <= anno; i++){
   		vec_encabezado.addElement(i+"");/*A�O*/ 
    } 
    colsWidth = new float[] {2f,1f, 1f, 1f, 1f, 1f, 1f}; // Code 1
    largocols = 7;
    } else {
        vec_encabezado.addElement("Financiamiento del Arancel"); 
        for (int i = (anno - 6); i <= anno; i++){
       		vec_encabezado.addElement(i+"");/*A�O*/ 
        } 
        colsWidth = new float[] {2f,1f, 1f, 1f, 1f, 1f, 1f, 1f}; // Code 1
        largocols = 8;
    }

  
    
    
        // add a table to the document
        document.open();
		
		PdfPTable table = new PdfPTable(colsWidth);
		table.setWidthPercentage(100); // Code 2
		table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
		
		PdfPCell cell = null;
		cell = new PdfPCell(new Paragraph(subtitulo2, new Font(bf_helvb)));
		cell.setBorder(0);
		cell.setPaddingBottom(10);
	    cell.setColspan(largocols);
		table.addCell(cell);	
		
		PdfPCell cell2 = null;
		cell2 = new PdfPCell(new Paragraph(subtitulo3, new Font(bf_helvb)));
		cell2.setBorder(0);
		cell2.setPaddingBottom(10);
	    cell2.setColspan(largocols);
		table.addCell(cell2);
		
		PdfPCell cell3 = null;
		cell3 = new PdfPCell(new Paragraph(subtitulo4, new Font(bf_helvb)));
		cell3.setBorder(0);
		cell3.setPaddingBottom(10);
	    cell3.setColspan(largocols);
		table.addCell(cell3);
		
		
		for (int m = 0 ; m < vec_encabezado.size(); m++){
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table.addCell(cell);
	      	
	    }		
		 
		 if(tipo.trim().equals("STP")) {
				for (int j = 0 ; j < vec_datos.size(); j++){
					
						for (int d = 0 ; d <= 7; d++){	
							if(d==0 || (((Vector) vec_datos.get(j)).get(d).toString()).indexOf(".") >= 0){
								  	cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(d).toString(), helv));	
							  		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							  		if((((Vector) vec_datos.get(j)).get(d).toString()).indexOf(".") >= 0)
							  			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							  		table.addCell(cell); 
							} else {
								if(d>0){
								cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector) vec_datos.get(j)).get(d).toString())), helv));
								cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
								table.addCell(cell);
								}
								 
							}
					   	     	
			    		} 
					
					}
		 } else {
		/*ingresos*/
		for (int j = 0 ; j < vec_datos.size(); j++){
			 if((((Vector)vec_datos.get(j)).get(8)+"").trim().equals("I")){
				for (int d = 9 ; d > 0; d--){	
					if(d==9){
						  	cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(d).toString(), helv));	
					  		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					  		table.addCell(cell); 
					} else {
						if(d!=8 && d!=7){
						cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector) vec_datos.get(j)).get(d).toString())), helv));
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						table.addCell(cell);
						}
						 
					}
			   	     	
	    		} 
			 }
			}	
		
		
		vec_encabezado = new Vector();   
		  vec_encabezado.addElement("Egresos"); 
		    for (int i = (anno - 5); i <= anno; i++){
		   		vec_encabezado.addElement(i+"");/*A�O*/ 
		    }
		for (int m = 0 ; m < vec_encabezado.size(); m++){
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table.addCell(cell);
	      	
	    }		
		        
		for (int j = 0 ; j < vec_datos.size(); j++){
			 if((((Vector)vec_datos.get(j)).get(8)+"").trim().equals("E")){
					for (int d = 9 ; d > 0; d--){	
							if(d==9){
						  	cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(d).toString(), helv));	
					  		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					  		table.addCell(cell); 
					} else {
						if(d!=8 && d!=7){							
						cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector) vec_datos.get(j)).get(d).toString())), helv));
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						table.addCell(cell); 
						}
					}
			   		    	
	    		} 
			 }
			}
		 }
		document.add(table);
		
	/*	PdfPTable table3 = new PdfPTable(colsWidth);
		table3.setWidthPercentage(100); // Code 2
		table3.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
		
		PdfPCell cell_b = null;
		cell_b = new PdfPCell(new Paragraph("Informaci�n en millones de pesos.", helvb));
		cell_b.setBorder(0);
		cell_b.setColspan(largocols);
		table3.addCell(cell_b);
		
		document.add(table3); */
		
		
		document.close();


	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	session.removeAttribute("listadoPresupSerie");
}catch(DocumentException e){
	e.printStackTrace();
}

%>