<%@page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 25, 20);

int rutUsuario = session.getAttribute("rutUsuario") != null?
        Integer.parseInt(session.getAttribute("rutUsuario")+""):0;



//System.out.println(vec_datos)	; 
try{

	Vector vec_datos = new Vector();

	vec_datos = request.getSession().getAttribute("planDesarrolloExcel") != null ?
	    (Vector) request.getSession().getAttribute("planDesarrolloExcel"):new Vector();
	String nomMacro = request.getParameter("nomMacro");		 
	String codUnidadMacro = request.getParameter("codUnidadMacro");
	int codCuenta = (request.getParameter("codCuenta") != null)?Integer.parseInt(request.getParameter("codCuenta")+""):0;
	int anno = (request.getParameter("anno") != null)?Integer.parseInt(request.getParameter("anno")+""):0;
	String nomCuenta = request.getParameter("nomCuenta");  
	int codItem = (request.getParameter("codItem") != null)?Integer.parseInt(request.getParameter("codItem")+""):0;
    String nomItem = request.getParameter("nomItem");
	            
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 
	NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);		    


	document.addTitle("CONTROL PLAN DE DESARROLLO" );
	//document.addAuthor(funcionario.trim());
	document.addSubject("ControlPlanDesarrollo");	
		
	
	
	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(
                new Phrase("P�gina ", helv), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);

    HeaderFooter header = new HeaderFooter(
                new Phrase("CONTROL PLAN DE DESARROLLO " , new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    document.open();
	
  
    Vector vec_encabezado = new Vector();
    vec_encabezado.addElement("MOVIMIENTO");
    vec_encabezado.addElement("DOCUMENTO");
    vec_encabezado.addElement("FECHA ");
    vec_encabezado.addElement("IDENTIFICADOR");
    vec_encabezado.addElement("VALOR");

   
    
 // add a table to the document
 	float[] widths = {
		 0.2f, 0.1f, 0.1f,
		 0.1f, 0.1f}; /*largo de las columnas, en este caso son 5*/
	PdfPTable table = new PdfPTable(widths);
    table.setWidthPercentage(100);
  //  table.setHeaderRows(2);
    

    
    
    
	 //Cabecera de la tabla
	PdfPCell cell = null;
    
    cell = new PdfPCell(new Paragraph("MACROUNIDAD: " + codUnidadMacro + "-" + nomMacro, new Font(bf_helvb)));
    cell.setBorder(0);
    cell.setPaddingBottom(10);
    cell.setColspan(16);
    table.addCell(cell);
    
    
    cell = new PdfPCell(new Paragraph("CUENTA ACTIVIDAD: " + codCuenta + "-" + nomCuenta, new Font(bf_helvb)));
    cell.setBorder(0);
    cell.setPaddingBottom(10);
    cell.setColspan(16);
    table.addCell(cell);
    
    cell = new PdfPCell(new Paragraph("ITEM: " + codItem + "-" + nomItem, new Font(bf_helvb)));
    cell.setBorder(0);
    cell.setPaddingBottom(10);
    cell.setColspan(16);
    table.addCell(cell);
    
    cell = new PdfPCell(new Paragraph("A�O: " + anno, new Font(bf_helvb)));
    cell.setBorder(0);
    cell.setPaddingBottom(10);
    cell.setColspan(16);
    table.addCell(cell);
    
//    cell = new PdfPCell(new Paragraph("UNIDAD: " + nomUnidad, new Font(bf_helvb)));
//    cell.setBorder(0);
//    cell.setPaddingBottom(10);
//    cell.setColspan(16);
//    table.addCell(cell);
    
    for (int m = 0 ; m < vec_encabezado.size(); m++){
      cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), new Font(Font.HELVETICA, 8, Font.BOLD)));
      cell.setBackgroundColor(Color.LIGHT_GRAY);   
      table.addCell(cell);
    }


	


    Vector vec_lista = new Vector();
    for (int i = 0; i < vec_datos.size(); i++){
    	vec_lista = new Vector();         
        vec_lista.addElement(((Vector)vec_datos.get(i)).get(0));/*MOVIMIENTO*/
        vec_lista.addElement(((Vector)vec_datos.get(i)).get(1));/*DOCUMENTO*/
        vec_lista.addElement(((Vector)vec_datos.get(i)).get(2));/*FECHA*/
        vec_lista.addElement(((Vector)vec_datos.get(i)).get(3));/*IDENTIFICADOR*/
        vec_lista.addElement(((Vector)vec_datos.get(i)).get(4));/*VALOR*/
    
    	  
    //Contenido de la tabla
    int columna = 0;
    for (int d = 0 ; d < vec_lista.size(); d++){
      columna ++;
      cell = new PdfPCell(new Paragraph(vec_lista.get(d).toString(), new Font(Font.HELVETICA, 8)));
      if (columna == 5) {
    	  cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong((vec_lista.get(d).toString()))), new Font(Font.HELVETICA, 8)));
    	  cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    	  cell.setNoWrap(true);
 
      }
      table.addCell(cell);
      if (columna == 5) columna = 0;      
    }   
    }
    document.add(table);
    document.newPage();  
    document.close();
    

	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	session.removeAttribute("planDesarrolloExcel");
}catch(DocumentException e){
	e.printStackTrace();
}

%>