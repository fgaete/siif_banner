<%@
page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%

response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 25, 20);/*.rotate() es para dejar la hoja horizontal*/
int rutUsuario = session.getAttribute("rutUsuario") != null?
                 Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
String nomUnidad = request.getParameter("nomUnidad") != null &&
                 !request.getParameter("nomUnidad").trim().equals("")?
                  request.getParameter("nomUnidad"):"";  
String opcion    = request.getParameter("opcion") != null &&
                  !request.getParameter("opcion").trim().equals("")?
                   request.getParameter("opcion"):"";
try{
    Vector vec_datos = new Vector();
    String observacion = "";
    vec_datos = request.getSession().getAttribute("consultaDocPDF") != null?
    		    (Vector) request.getSession().getAttribute("consultaDocPDF"): new Vector();
    
    NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);		    
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 

	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(new Phrase("P�gina ", helv), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);

    String textoOpcion = "";
    if (opcion.equals("FAC")) textoOpcion = "CONSULTA FACTURAS POR UNIDAD ";
    if (opcion.equals("IMP")) textoOpcion = "CONSULTA IMPORTACIONES POR UNIDAD ";
    if (opcion.equals("CHQ")) textoOpcion = "CONSULTA DOCUMENTOS PENDIENTES POR UNIDAD ";
    
    HeaderFooter header = new HeaderFooter(new Phrase(textoOpcion, new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    //document.open();
	
    // datos
    Vector vec_encabezado = new Vector();   
    float[] colsWidth = null;
    int largocols = 0;
    if (opcion.equals("FAC")) {
        vec_encabezado.addElement("N� DOCUMENTO");
        vec_encabezado.addElement("TIPO DOCUMENTO");
        vec_encabezado.addElement("NOMBRE CLIENTE");
        vec_encabezado.addElement("FECHA EMISI�N");
        vec_encabezado.addElement("VALOR $");
        vec_encabezado.addElement("SALDO PAGO $");
        vec_encabezado.addElement("LIQUIDABLE $");
        colsWidth = new float[] {1f, 1f, 2f, 1f, 1f, 1f, 1f}; // Code 1
        largocols = 7;
    }
    
    if (opcion.equals("IMP")) {
    	 vec_encabezado.addElement("SUCURSAL");
         vec_encabezado.addElement("PER");
         vec_encabezado.addElement("FECHA");
         vec_encabezado.addElement("PROVEEDOR");
         vec_encabezado.addElement("ASIGNADO $");
         vec_encabezado.addElement("GASTADO $");
         vec_encabezado.addElement("ESTADO");
         colsWidth = new float[] {1f, 1f, 1f, 1f, 1f, 1f, 1f}; // Code 1
         largocols = 7;
    }
    
    if (opcion.equals("CHQ")) {
    	vec_encabezado.addElement("CUENTA");
        vec_encabezado.addElement("TOTAL $");
        vec_encabezado.addElement("POR DISTRIBUIR $");
        colsWidth = new float[] {1f, 1f, 1f}; // Code 1
        largocols = 3;
    } 
    
        // add a table to the document
        document.open();
		
		PdfPTable table = new PdfPTable(colsWidth);
		table.setWidthPercentage(100); // Code 2
		table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
		
		PdfPCell cell = null;
		cell = new PdfPCell(new Paragraph("UNIDAD: " + nomUnidad, new Font(bf_helvb)));
		cell.setBorder(0);
		cell.setPaddingBottom(10);
	    cell.setColspan(largocols);
		table.addCell(cell);
		
		for (int m = 0 ; m < vec_encabezado.size(); m++){
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table.addCell(cell);
	      	
	    }
		
		if (opcion.equals("FAC")) {
			long sumTotalValor  = 0;
        	long sumTotalSaldo  = 0;
        	long sumTotalLiquid = 0;
			for (int j = 0 ; j < vec_datos.size(); j++){  
				for (int d = 0 ; d < largocols; d++){
					if (d < 4) {
						cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(d).toString(), helv));
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					}	
					else {
						cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector) vec_datos.get(j)).get(d).toString())), helv));
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					}
		   			table.addCell(cell);      	
	    		}   
				sumTotalValor  += Long.parseLong(((Vector)vec_datos.get(j)).get(4)+"");
	   			sumTotalSaldo  += Long.parseLong(((Vector)vec_datos.get(j)).get(5)+"");
	   			sumTotalLiquid += Long.parseLong(((Vector)vec_datos.get(j)).get(6)+"");
			
			}
		
			Vector vec_totales = new Vector();
			vec_totales.add(sumTotalValor+"");
			vec_totales.add(sumTotalSaldo+"");
			vec_totales.add(sumTotalLiquid+"");
			
			int k = 0;
			for (int x = 0 ; x < largocols; x++){ 
				if (x < 3) {
			      cell = new PdfPCell(new Paragraph(""));
			      cell.setBorder(0);	
				}
				if (x == 3) {
					cell = new PdfPCell(new Paragraph("TOTAL", helvb));
					cell.setBackgroundColor(Color.LIGHT_GRAY);
				}	
				if (x > 3) {
				   cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(vec_totales.get(k).toString())), helvb));
				   cell.setBackgroundColor(Color.LIGHT_GRAY);
				   k++;
			   }
			   cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			   
			   table.addCell(cell); 	
				
			}
		}
		
		 if (opcion.equals("IMP")) {
	    	 for (int j = 0 ; j < vec_datos.size(); j++){  
			    Vector vec_lista = new Vector();
			    if (Integer.parseInt(((Vector)vec_datos.get(j)).get(7)+"") == 1)
			    	vec_lista.addElement(((Vector)vec_datos.get(j)).get(7) + " CASA CENTRAL"); /* Sucursal */
      	 	    else if(Integer.parseInt(((Vector)vec_datos.get(j)).get(7)+"") == 2)
      	 	    	vec_lista.addElement(((Vector)vec_datos.get(j)).get(7) + " VI�A DEL MAR"); /* Sucursal */
      	 	     else if (Integer.parseInt(((Vector)vec_datos.get(j)).get(7)+"") == 3)
      	 	    	vec_lista.addElement(((Vector)vec_datos.get(j)).get(7) + " TALCAHUANO"); /* Sucursal */
      	 	     else if (Integer.parseInt(((Vector)vec_datos.get(j)).get(7)+"") == 4)
      	 	    	vec_lista.addElement(((Vector)vec_datos.get(j)).get(7) + " SANTIAGO"); /* Sucursal */
      	 	     else if (Integer.parseInt(((Vector)vec_datos.get(j)).get(7)+"") == 5)
      	 	    	vec_lista.addElement(((Vector)vec_datos.get(j)).get(7) + " RANCAGUA"); /* Sucursal */  
      	 	     else if (Integer.parseInt(((Vector)vec_datos.get(j)).get(7)+"") == 6)
      	 	    	vec_lista.addElement(((Vector)vec_datos.get(j)).get(7) + " A.C.A."); /* Sucursal */ 
        	    vec_lista.addElement(((Vector)vec_datos.get(j)).get(0));/*PER (numDoc)*/
                vec_lista.addElement(((Vector)vec_datos.get(j)).get(3));/*fecha doc */
                vec_lista.addElement(((Vector)vec_datos.get(j)).get(2));/*proveedor */
                vec_lista.addElement(((Vector)vec_datos.get(j)).get(4));/*asignado */
                vec_lista.addElement(((Vector)vec_datos.get(j)).get(5));/*gastado */
                vec_lista.addElement(((Vector)vec_datos.get(j)).get(8));/*Estado */
                 
                for (int d = 0 ; d < largocols; d++){   
                	if (d == 4 || d == 5)
                		cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(vec_lista.get(d).toString())), helv));
                	else cell = new PdfPCell(new Paragraph(vec_lista.get(d).toString(), helv));
					if (d < 4)
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					else cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		   			table.addCell(cell);      	
	    		}   
                         
             } 
          }
		 
	       if (opcion.equals("CHQ")) {
	     	  for (int j = 0 ; j < vec_datos.size(); j++){  
	    	   Vector vec_lista = new Vector();
	           vec_lista.addElement(((Vector)vec_datos.get(j)).get(2));/*cuenta */
	           vec_lista.addElement(((Vector)vec_datos.get(j)).get(4));/*total */
	           vec_lista.addElement(((Vector)vec_datos.get(j)).get(5));/*por distribuir */
	           
	           for (int d = 0 ; d < largocols; d++){   
				if (d < 1) {
				  cell = new PdfPCell(new Paragraph(vec_lista.get(d).toString(), helv));	
				  cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				}  
			    else {
			    	cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(vec_lista.get(d).toString())), helv));
			    	cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			    }
		   		table.addCell(cell);      	
	    	  }   
	                             
	    	  }
	       }	 
		
		document.add(table);
		
		
		document.close();


	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	session.removeAttribute("consultaDocPDF");
}catch(DocumentException e){
	e.printStackTrace();
}

%>