<%@page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%

// 05 y 06/10/2016 KP - Se cambian nombres de variables y etiquetas 'unidad(es)' por 'organizacion(es)'
//19/10/2016 y 20/10/2016 KP - Se agregan tabas a visualizar
response.setContentType("application/pdf");
Document document = new Document(PageSize.A4, 20, 20, 25, 20);/*.rotate() es para dejar la hoja horizontal*/
int rutUsuario    = session.getAttribute("rutUsuario") != null?
                    Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
/*int unidad   = request.getParameter("unidad") != null?
	              Integer.parseInt(request.getParameter("unidad")):0;*/

String organizacion  = request.getParameter("organizacion") != null &&
	                   !request.getParameter("organizacion").trim().equals("")?
	                   request.getParameter("organizacion"):"";	  	               		                 
int anno   			 = request.getParameter("anno") != null &&
                	   !request.getParameter("anno").trim().equals("")?
                	   Integer.parseInt(request.getParameter("anno")):0;
String funcionario   = request.getParameter("funcionario") != null &&
               		   !request.getParameter("funcionario").trim().equals("")?
                       request.getParameter("funcionario"):"";
try{
	
    Vector vec_datos = new Vector();
    //String nomUnidad = "";
    String nomOrganizacion = "";
    vec_datos = request.getSession().getAttribute("listaRemuneracionPDF") != null?
    		    (Vector) request.getSession().getAttribute("listaRemuneracionPDF"): new Vector();
	    
    if(vec_datos.size() > 0){
    	//nomUnidad = vec_datos.get(vec_datos.size()-1).toString();
    	nomOrganizacion = organizacion.trim()+" "+vec_datos.get(vec_datos.size()-1).toString().trim();    	
    }    
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 

	document.addTitle("PRESUPUESTACIÓN DE REMUNERACIONES VARIABLES " + anno);
	document.addAuthor(funcionario.trim());
	//document.addSubject(nomUnidad);
	document.addSubject(nomOrganizacion);	
	
	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(new Phrase("Página ", new Font(bf_helv)), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);

    HeaderFooter header = new HeaderFooter(new Phrase("PRESUPUESTACIÓN DE REMUNERACIONES VARIABLES " + anno, new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    document.open();
	
    // datos
 /*   Vector vec_encabezado = new Vector(); 
    vec_encabezado.addElement("Departamento");
    vec_encabezado.addElement("SEM 1");
    vec_encabezado.addElement("SEM 2");
    vec_encabezado.addElement("SEM 1");  
    vec_encabezado.addElement("SEM 2");
    vec_encabezado.addElement("SEM 1");
    vec_encabezado.addElement("SEM 2");
    vec_encabezado.addElement("SEM 1");
    vec_encabezado.addElement("SEM 2");
    vec_encabezado.addElement("SEM 1");
    vec_encabezado.addElement("SEM 2");
*/


    Vector vec_encabezado2 = new Vector(); 
  
  //  vec_encabezado2.addElement((anno - 4)+"");
    vec_encabezado2.addElement((anno - 3)+"");
    vec_encabezado2.addElement((anno - 2)+"");  
    vec_encabezado2.addElement((anno - 1)+"");
    vec_encabezado2.addElement(anno+"");
  
    
 // add a table to the document
 	float[] widths = {0.09f, 0.05f, 0.05f, 0.05f, 0.05f/*, 0.05f, 0.05f, 0.05f, 0.05f, 0.05f, 0.05f*/}; /*largo de las columnas, en este caso son tres*/
	PdfPTable table = new PdfPTable(widths);
    table.setWidthPercentage(100);
    table.setHeaderRows(2);/* cantidad de filas cabeceras para que se repitan en cada hoja*/
    
    PdfPCell cell = null;
    
   // cell = new PdfPCell(new Paragraph("UNIDAD: " + nomUnidad, new Font(bf_helvb)));
    cell = new PdfPCell(new Paragraph("ORGANIZACIÓN: " + nomOrganizacion.trim(), new Font(bf_helvb)));
    cell.setBorder(0);
    cell.setPaddingBottom(10);
    //cell.setColspan(11);
    cell.setColspan(5);
    table.addCell(cell);
    
    cell = new PdfPCell(new Paragraph(""));
    cell.setBorder(0);
    table.addCell(cell);
    
    for (int m = 0 ; m < vec_encabezado2.size(); m++){
      	cell = new PdfPCell(new Paragraph(vec_encabezado2.get(m).toString(), new Font(bf_helvb)));
      	cell.setBackgroundColor(Color.LIGHT_GRAY);
      	//cell.setColspan(2);
      	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
      	table.addCell(cell);
      	
    }
    
	//Cabecera de la tabla    
  /*  for (int m = 0 ; m < vec_encabezado.size(); m++){
      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), new Font(bf_helvb)));
      	cell.setBackgroundColor(Color.LIGHT_GRAY);
      	if (m == 0) cell.setHorizontalAlignment(Element.ALIGN_LEFT);
      	else cell.setHorizontalAlignment(Element.ALIGN_CENTER);
      	table.addCell(cell);
    }*/

    //Contenido de la tabla
    int col = 1;
    
    for (int d = 0 ; d < (/*vec_datos.size()-1*/ 5); d++){   
    	if (col == 1){
  		    cell = new PdfPCell(new Paragraph("Ejecución Real")); 
  		    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
  		    cell.setBorder(0);
    	}
    	else{
    		cell = new PdfPCell(new Paragraph(vec_datos.get(d).toString(), new Font(bf_helv)));
    		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    	}    	
    	col ++;
    	//if (col > 11) col = 1;
      	table.addCell(cell);      	
    }    
   
    //Inicio - PARA LA FILA EN BLANCO, se agregan 3 filas
    PdfPCell cellb = null;
    cellb = new PdfPCell(new Paragraph(" ")); 
    //cellb.setColspan(11); 
    cellb.setColspan(5);
    cellb.setBorder(0); 
    table.addCell(cellb); 
    table.addCell(cellb);
  //Fin - PARA LA FILA EN BLANCO

/*    
   cell = new PdfPCell(new Paragraph("La información corresponde a valores semanales.")); 
    cell.setColspan(11);
    cell.setBorder(1); 
    table.addCell(cell);*/
 
        
 //   document.add(table); 
    
    
    PdfPCell cellC = null;
    cellC = new PdfPCell(new Paragraph(" "));
    cellC.setBorder(0);
    cellC.setColspan(1);
    table.addCell(cellC);    
    cellC = new PdfPCell(new Paragraph("AÑO " + anno, new Font(bf_helvb)));    
    cellC.setHorizontalAlignment(Element.ALIGN_CENTER);
    cellC.setBorder(0);
    cellC.setColspan(2);
    table.addCell(cellC);
    cellC = new PdfPCell(new Paragraph(" "));
    cellC.setBorder(0);
    cellC.setColspan(2);
    table.addCell(cellC);    
    
   cellC = new PdfPCell(new Paragraph(" "));
   cellC.setBorder(0);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph("1er Semestre"));
   cellC.setHorizontalAlignment(Element.ALIGN_CENTER);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph("2do Semestre"));
   cellC.setHorizontalAlignment(Element.ALIGN_CENTER);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph(" "));
   cellC.setBorder(0);
   cellC.setColspan(2);
   table.addCell(cellC); 
   
   cellC = new PdfPCell(new Paragraph("Cantidad de Paralelos"));
   cellC.setBorder(0);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph(vec_datos.get(5).toString(), new Font(bf_helv)));
   cellC.setHorizontalAlignment(Element.ALIGN_RIGHT);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph(vec_datos.get(6).toString(), new Font(bf_helv)));
   cellC.setHorizontalAlignment(Element.ALIGN_RIGHT);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph(" "));
   cellC.setBorder(0);
   cellC.setColspan(2);
   table.addCell(cellC);  
   
   cellC = new PdfPCell(new Paragraph("Alumnos a atender"));
   cellC.setBorder(0);
   cellC.setColspan(1);
   table.addCell(cellC); 
   cellC = new PdfPCell(new Paragraph(vec_datos.get(7).toString(), new Font(bf_helv)));
   cellC.setHorizontalAlignment(Element.ALIGN_RIGHT);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph(vec_datos.get(8).toString(), new Font(bf_helv)));
   cellC.setHorizontalAlignment(Element.ALIGN_RIGHT);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph(" "));
   cellC.setBorder(0);
   cellC.setColspan(2);
   table.addCell(cellC);     
   
   cellC = new PdfPCell(new Paragraph("Cantidad de horas Part Time"));
   cellC.setBorder(0);
   cellC.setColspan(1);
   table.addCell(cellC);  
   cellC = new PdfPCell(new Paragraph(vec_datos.get(9).toString(), new Font(bf_helv)));
   cellC.setHorizontalAlignment(Element.ALIGN_RIGHT);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph(vec_datos.get(10).toString(), new Font(bf_helv)));
   cellC.setHorizontalAlignment(Element.ALIGN_RIGHT);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph(" "));
   cellC.setBorder(0);
   cellC.setColspan(2);
   table.addCell(cellC);     
  
   cellC = new PdfPCell(new Paragraph("Cantidad de horas Honorarios"));
   cellC.setBorder(0);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph(vec_datos.get(11).toString(), new Font(bf_helv)));
   cellC.setHorizontalAlignment(Element.ALIGN_RIGHT);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph(vec_datos.get(12).toString(), new Font(bf_helv)));
   cellC.setHorizontalAlignment(Element.ALIGN_RIGHT);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph(" "));
   cellC.setBorder(0);
   cellC.setColspan(2);
   table.addCell(cellC);  
   
   cellC = new PdfPCell(new Paragraph("Cantidad de horas Full Time"));
   cellC.setBorder(0);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph(vec_datos.get(13).toString(), new Font(bf_helv)));
   cellC.setHorizontalAlignment(Element.ALIGN_RIGHT);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph(vec_datos.get(14).toString(), new Font(bf_helv)));
   cellC.setHorizontalAlignment(Element.ALIGN_RIGHT);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph(" "));
   cellC.setBorder(0);
   cellC.setColspan(2);
   table.addCell(cellC); 
   
   cellC = new PdfPCell(new Paragraph("Cantidad de horas Ayudantias"));
   cellC.setBorder(0);
   cellC.setColspan(1);
   table.addCell(cellC); 
   cellC = new PdfPCell(new Paragraph(vec_datos.get(15).toString(), new Font(bf_helv)));
   cellC.setHorizontalAlignment(Element.ALIGN_RIGHT);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph(vec_datos.get(16).toString(), new Font(bf_helv)));
   cellC.setHorizontalAlignment(Element.ALIGN_RIGHT);
   cellC.setColspan(1);
   table.addCell(cellC);
   cellC = new PdfPCell(new Paragraph(" "));
   cellC.setBorder(0);
   cellC.setColspan(2);
   table.addCell(cellC); 

    document.add(table); 

    document.close();

	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	session.removeAttribute("listaRemuneracionPDF");
}
catch(DocumentException e){
	e.printStackTrace();
}
%>