<%@page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 25, 20);

int rutUsuario = session.getAttribute("rutUsuario") != null?
        Integer.parseInt(session.getAttribute("rutUsuario")+""):0;



//System.out.println(vec_datos)	; 
try{

	Vector vec_datos = new Vector();

	vec_datos = request.getSession().getAttribute("planDesarrolloExcel") != null ?
	    (Vector) request.getSession().getAttribute("planDesarrolloExcel"):new Vector();
	int annoControl = request.getSession().getAttribute("annoControl")	!= null?
				Integer.parseInt(request.getSession().getAttribute("annoControl")+""):0;            
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 
	NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);		    


	document.addTitle("CONTROL PLAN DE DESARROLLO" );
	//document.addAuthor(funcionario.trim());
	document.addSubject("ControlPlanDesarrollo");	
	

	
	
	
	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(
                new Phrase("P�gina ", helv), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);

    HeaderFooter header = new HeaderFooter(
                new Phrase("CONTROL PLAN DE DESARROLLO - "+annoControl , new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    document.open();
	
    Vector vec_encabezado = new Vector();
    vec_encabezado.addElement("MACROUNIDAD");
    vec_encabezado.addElement("ASIGNACI�N");
    vec_encabezado.addElement("EJECUTADO");
    vec_encabezado.addElement("% EJECUTADO");
    vec_encabezado.addElement("% �PTIMO");
    vec_encabezado.addElement("% PRECAUCI�N");
    vec_encabezado.addElement("% ALERTA");   

   
    
 // add a table to the document
 	float[] widths = {
		 0.2f, 0.1f, 0.1f,
		 0.1f, 0.1f, 0.1f, 0.1f}; /*largo de las columnas, en este caso son 7*/
	PdfPTable table = new PdfPTable(widths);
    table.setWidthPercentage(100);
  //  table.setHeaderRows(2);
    
    //Cabecera de la tabla
	PdfPCell cell = null;
    
//    cell = new PdfPCell(new Paragraph("UNIDAD: " + nomUnidad, new Font(bf_helvb)));
//    cell.setBorder(0);
//    cell.setPaddingBottom(10);
//    cell.setColspan(16);
//    table.addCell(cell);
    
    for (int m = 0 ; m < vec_encabezado.size(); m++){
      cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), new Font(Font.HELVETICA, 8, Font.BOLD)));
      cell.setBackgroundColor(Color.LIGHT_GRAY);   
      table.addCell(cell);
    }


	


    Vector vec_lista = new Vector();
    for (int i = 0; i < vec_datos.size(); i++){
    	vec_lista = new Vector();         
        vec_lista.addElement(((Vector)vec_datos.get(i)).get(0));/*MACROUNIDAD*/
        vec_lista.addElement(((Vector)vec_datos.get(i)).get(1));/*ASIGNACI�N*/
        vec_lista.addElement(((Vector)vec_datos.get(i)).get(2));/*EJECUTADO*/
        vec_lista.addElement(((Vector)vec_datos.get(i)).get(3));/*% EJECUTADO*/
        vec_lista.addElement(((Vector)vec_datos.get(i)).get(4));/*% �PTIMO*/
        vec_lista.addElement(((Vector)vec_datos.get(i)).get(5));/*% PREC.*/
        vec_lista.addElement(((Vector)vec_datos.get(i)).get(6));/*% ALERTA*/
    
    	  
    //Contenido de la tabla
    int columna = 0;
    for (int d = 0 ; d < vec_lista.size(); d++){
      columna ++;
      cell = new PdfPCell(new Paragraph(vec_lista.get(d).toString(), new Font(Font.HELVETICA, 8)));
      if (columna > 1 && columna < 8) {
    	  cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong((vec_lista.get(d).toString()))), new Font(Font.HELVETICA, 8)));
    	  cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    	  cell.setNoWrap(true); 
      }
      table.addCell(cell);
      if (columna == 7) columna = 0;      
    }   
    }
    document.add(table); 
	
    document.newPage();
    


  
    document.close();
    
   

	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	session.removeAttribute("planDesarrolloExcel");
    if(request.getSession().getAttribute("annoControl") != null)
    	session.removeAttribute("annoControl");
}catch(DocumentException e){
	e.printStackTrace();
}

%>