<%@ page language="java" contentType='text/html;charset=iso-8859-1'%>
<%@ page import = "descad.presupuesto.Presw25DTO"%>
<%@ page import = "java.io.*" %>
<%@ page import = "java.util.*"%>
<%@ page import = "java.util.List"%>
<%@ page import = "java.util.Vector"%>
<%@ page import = "org.apache.commons.fileupload.FileItem"%>
<%@ page import = "org.apache.commons.fileupload.FileUploadException"%>
<%@ page import = "org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@ page import = "org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@ page import = "org.apache.commons.fileupload.FileItemFactory"%>
<%@ page import = "org.xml.sax.*"%>
<%@ page import = "javax.xml.parsers.*"%>
<%@ page import = "upxls.Process_xls"%><%

System.out.println("entra en uploadxls"); 
	System.out.println("UPLOAD: Inicia IMPORTACION"); 
 	boolean exito = false;
    String mensaje = "";
 	String fieldName = "";
 	String fileName = "";
 	String contentType = "";
 	String extension = "";
 	String dir = "";
 	
 	// para icalma
	String dirb = "/usr/local/siga_doc/sip/";
	String dirp = "/usr/local/siga_doc/sip/";
 	// para prueba local
	//String dirb = "";
	// String dirp = "C:/usr/local/siga_doc/sip/"; 	
%>
<%
 //	int unidad = session.getAttribute("codigoUnidad")!= null?Integer.parseInt(session.getAttribute("codigoUnidad")+""):0;
    String organizacion = session.getAttribute("codigoOrganizacion")!= null?session.getAttribute("codigoOrganizacion")+"":"";
 	int anno = session.getAttribute("anno")!= null?Integer.parseInt(session.getAttribute("anno")+""):0;
	int codActividad = session.getAttribute("codActividad")!= null?Integer.parseInt(session.getAttribute("codActividad")+""):0;
 	int rutUsuario = session.getAttribute("rutUsuario")!= null?Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
 	String dv = session.getAttribute("dv") != null? session.getAttribute("dv")+"":"";
 	String tippro = session.getAttribute("tippro") != null? session.getAttribute("tippro")+"":"";
 	Vector resultadoPresupImport;
 	
	System.out.println("organizacion "+ organizacion);
	//System.out.println("ss anno "+ session.getAttribute("anno")); 	
    if(session.getAttribute("resultadoPresupImport") != null) {
    	session.setAttribute("resultadoPresupImport", null);
    	session.removeAttribute("resultadoPresupImport");
    }
 
	Calendar fecha = Calendar.getInstance();
	String mes = "";
	String dia = "";
	String a�o = String.valueOf(fecha.get(Calendar.YEAR));
	if (fecha.get(Calendar.MONTH) < 8)
		mes = "0"+ String.valueOf(fecha.get(Calendar.MONTH) + 1);
	else
		mes = String.valueOf(fecha.get(Calendar.MONTH) + 1);
	if (fecha.get(Calendar.DAY_OF_MONTH) < 10)
		dia = "0"+ String.valueOf(fecha.get(Calendar.DAY_OF_MONTH));
	else
		dia = String.valueOf(fecha.get(Calendar.DAY_OF_MONTH));
	
	dir = dirb + a�o + "/" + mes + "/" + dia + "/";
			
	String hora = String.valueOf(fecha.get(Calendar.HOUR_OF_DAY));
	String minutos = String.valueOf(fecha.get(Calendar.MINUTE));
	String segundos = String.valueOf(fecha.get(Calendar.SECOND));

	//String archivo = unidad + "_" + rutUsuario + "_" + 	hora + "" + minutos + "" + segundos; 
	String archivo = organizacion + "_" + rutUsuario + "_" + 	hora + "" + minutos + "" + segundos; 
	// if(unidad == 0)
	 if(organizacion.equals(""))	
		archivo = codActividad + "_" + rutUsuario + "_" + 	hora + "" + minutos + "" + segundos; 

	System.out.println("UPLOAD: archivo a subir: " + archivo);	
	
 	//Vector salida = new Vector();

 	// Create a factory for disk-based file items
 	FileItemFactory factory = new DiskFileItemFactory();

 	// Create a new file upload handler
 	ServletFileUpload upload = new ServletFileUpload(factory);

 	// Parse the request
 	try {

 		List items = null;
 		/* FileItem */items = upload.parseRequest(request);

 		// Process the uploaded items
 		Iterator iter = items.iterator();
 		while (iter.hasNext()) {
 			FileItem item = (FileItem) iter.next();
 			if (!item.isFormField()) {
 				fieldName = item.getFieldName();
 				fileName = item.getName();
 				contentType = item.getContentType();
 				boolean isInMemory = item.isInMemory();
 				long sizeInBytes = item.getSize();
				System.out.println("UPLOAD: fieldName: " + fieldName);
				System.out.println("UPLOAD: fileName: " + fileName);
 				if (fileName.indexOf(".") > 0) {
 					extension = fileName.substring(fileName.indexOf(".") + 1, fileName.length());
 					//System.out.println("extension "+ extension);
 				}	

 				if (!(new File(dir).exists()))
 					(new File(dir)).mkdirs();

 				File uploadedFile = null;
 				if (extension .equals("xlsx")) {
 					uploadedFile = new File(dir + archivo + ".xlsx");
 				} else {
 					uploadedFile = new File(dir + archivo + ".xls");
 				}
	
 				try {
 					item.write(uploadedFile);

 				} catch (Exception ex1) {
 					System.out.println("UPLOAD: error escribir " + ex1.toString());
 					mensaje = "ERROR 1. Existen problemas al cargar el archivo.";
 				}
 			}
 		}

 	} catch (FileUploadException ex) {
 		System.out.println("UPLOAD: error " + ex.toString());
 		mensaje = "ERROR 2. Existen problemas al cargar el archivo.";
 	}
//	System.out.println("inicia comparacion");    
 	try {
 		//System.out.println("index: "+fileName.indexOf("."));
 		if (fileName.indexOf(".") > 0) {
 		    // PROCESA XLSX
 			if (extension .equals("xlsx")) {
 				System.out.println("UPLOAD: Inicia PROCESA XLSX: " +
						dir + archivo + ".xlsx   PLANTILLA: " +
						dirp + "planillaPresupuestoCorriente.xlsx"); 
 				
 				Process_xls procesarxls = new Process_xls(dir + archivo + ".xlsx",
 						dirp + "planillaPresupuestoCorriente.xlsx", 2, 1);
 				
 				System.out.println("UPLOAD: Fin PROCESA XLSX");
 				// salida = procesarxls.Source();
 				boolean iguales = procesarxls.Equals();
				System.out.println("UPLOAD: compara archivos "+ iguales);

 				if (iguales)
 					try {			
 						//System.out.println("obtenemos resultados BD ");
 						
 	 				//	session.setAttribute("resultadoPresupImport", procesarxls.getPresw25DTO(tippro, unidad, anno, codActividad, rutUsuario, dv));
 	 				    resultadoPresupImport = procesarxls.getPresw25DTO(tippro, organizacion, anno, codActividad, rutUsuario, dv);
// 	 				 	List<Presw25DTO> resultadoPresupImport = procesarxls.getPresw25DTO(tippro, organizacion, anno, codActividad, rutUsuario, dv);
 	 				    
 	 					session.setAttribute("resultadoPresupImport", resultadoPresupImport);
	 					
 	 					
 	 	    			// MB 21/10/2015 comenta segun instrucciones de presupuesto
 	 					//mensaje = "El archivo XLSX ha sido importado.<br><b>EL ITEM 41 DEBE SER INGRESADO DE FORMA MANUAL</b>.<br><b>Por favor, REGISTRE la informaci&oacute;n para que sus cambios sean guardados.</b>";
 	 					mensaje = "El archivo XLSX ha sido importado.<br><br><b>Por favor, REGISTRE la informaci&oacute;n para que sus cambios sean guardados.</b>";
 	 	
 	 					exito = true;
 						}
 						catch (NumberFormatException e){
 							mensaje = "ERROR. El archivo XLSX presenta problemas de conversi&oacute;n de datos.";
 							System.out.println(e.toString());	
 						}
 				else {
					mensaje = "ERROR. El archivo XLSX no coincide con la plantilla oficial.";
 				}
 				
			// PROCESA XLS
 			} else {
				System.out.println("UPLOAD: Inicia PROCESA XLS: " +
						dir + archivo + ".xls   PLANTILLA: " +
						dirp + "planillaPresupuestoCorriente.xls"); 				
 		 		//System.out.println("comienza xls 1: "+extension); 		    
 				Process_xls procesarxls = new Process_xls(dir + archivo + ".xls",	
 						dirp + "planillaPresupuestoCorriente.xls", 2, 1);
 				// salida = procesarxls.Source();
 				System.out.println("UPLOAD: Fin PROCESA XLS");
 				
 				boolean iguales = procesarxls.Equals();
				System.out.println("UPLOAD: compara archivos "+ iguales);

				if (iguales){
					try { 						
 	 					//session.setAttribute("resultadoPresupImport", procesarxls.getPresw25DTO(tippro, unidad, anno, codActividad, rutUsuario, dv));
 	 					resultadoPresupImport = procesarxls.getPresw25DTO(tippro, organizacion, anno, codActividad, rutUsuario, dv);
 	 					session.setAttribute("resultadoPresupImport", resultadoPresupImport);
 	 					System.out.println("UPLOAD: sube a sesion plantilla");
 	 					// MB 21/10/2015 comenta segun instrucciones de presupuesto
 	 	 				//	mensaje = "El archivo XLS ha sido importado.<br><b>EL ITEM 41 DEBE SER INGRESADO DE FORMA MANUAL</b>.<br><b>Por favor, REGISTRE la informaci&oacute;n para que sus cambios sean guardados.</b>";
 			    		mensaje = "El archivo XLS ha sido importado.<br><br><b>Por favor, REGISTRE la informaci&oacute;n para que sus cambios sean guardados.</b>";
 						exito = true;
					}
					catch (NumberFormatException e){
						mensaje = "ERROR. El archivo XLS presenta problemas de conversi&oacute;n de datos.";
						System.out.println("UPLOAD: error " + e.toString());	
					}
				}
				else {
					mensaje = "ERROR. El archivo XLS no coincide con la plantilla oficial.";
				}

 			}
 		}
 		//System.out.println("sali8da: "+ salida);
 	} catch (Exception ex1) {
 		System.out.println("UPLOAD: error " + ex1.toString());
 		mensaje = "ERROR. El archivo no pudo ser importado.";

 	}
 	
 	System.out.println("UPLOAD: Fin IMPORTACION");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="cache-control" VALUE="no-cache, no-store, must-revalidate">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Pragma" content="no-cache">
<meta name="keywords" content="" />
<meta name="description" content="" />

<title>SIP - Sistema de Informe de Presupuestos</title>
<script>
<% if(codActividad == 0){%>
function CargaPresup(){
	opener.parent.document.presup.volver.value='1';
	opener.parent.cargar();
}
<%} else {%>
function CargaPresup(){
	opener.parent.document.formPresup.volver.value='1';
	opener.parent.cargar();
}
<%}%>
</script>
<style>
* {
	padding: 0px;
	margin: 0px;
}

body {
	font-size: 13px;
	font-family: "trebuchet ms", helvetica, sans-serif;
	color: #8C8C73;
	line-height: 18px;
}
#outer {
	position: relative;
	width: 99%;
	margin: 0 auto;
	border: solid 1px #E4E4E4;
	background-color: #ffffff;
}
#inner {
	position: relative;
	padding: 13px 30px 13px 30px;
	z-index: 3;
}
#footer {
	position: relative;
	clear: both;
	height: 35px;
	text-align: center;
	line-height: 35px;
	color: #A8A88D;
}
.buttonGris {
	text-align: center;
	COLOR: #000; text-shadow: 0 1px 0 #3e5a88;
	PADDING: 3px;
	MARGIN: 0px; 
}
</style> 
</head>
<body <%if (exito){%>onload="CargaPresup()"<%}%>>
<div id="outer"><!--outer-->                               		
	<div id="inner"><!--inner--> 	

 			<div align="center">
 			<h3>Servicio de Importaci&oacute;n de Archivos</h3>
 			</div>
			<div style="text-align:center; margin:10px; padding:10px; border:dotted 1px #CCCCCC;">
				<font color='#990000'><%=mensaje%></font>	
			</div>	
			<center>
			<INPUT TYPE="BUTTON" NAME="Cerrar" VALUE="Cerrar" class="buttonGris" onClick="window.close();">
			</center>
			<br>
		<div id="footer"><!--footer-->
		&copy; Universidad T&eacute;cnica Federico Santa Mar&iacute;a
		</div><!--footer-->
    </div><!--inner--> 
</div><!--outer--> 
</body>
</html>              