<%@ 
page language="java" import="descad.presupuesto.*,
                                 java.util.*,
                                 java.io.*,
                                 descad.cliente.PreswBean,
                                 java.text.NumberFormat,
                                 org.apache.poi.ss.usermodel.DataFormat,
                                 org.apache.poi.poifs.filesystem.POIFSFileSystem,
                                 org.apache.poi.hssf.usermodel.*,
                                 org.apache.poi.ss.usermodel.CellStyle"
                                 contentType="application/vnd.ms-excel"%><%                                 
	// Creado por           : M�nica Barrera F.
	// Fecha                : 26/01/2015
	// �ltima Actualizaci�n :

	int rutUsuario = session.getAttribute("rutUsuario") != null ? 
			Integer.parseInt(session.getAttribute("rutUsuario") + ""): 0;

	if (rutUsuario > 0) {
		String nomUnidad = request.getParameter("nomUnidad") != null &&
					       !request.getParameter("nomUnidad").trim().equals("")?
					        request.getParameter("nomUnidad"):"";   
		int anno 		 = request.getParameter("anno") != null &&
		          			!request.getParameter("anno").trim().equals("")?
		           			Integer.parseInt(request.getParameter("anno")):0; 
		int mesInicio 	 = request.getParameter("mesInicio") != null &&
		           			!request.getParameter("mesInicio").trim().equals("")?
		            		Integer.parseInt(request.getParameter("mesInicio")):0; 
		int mesTermino 	 = request.getParameter("mesTermino") != null &&
		            		!request.getParameter("mesTermino").trim().equals("")?
		             		Integer.parseInt(request.getParameter("mesTermino")):0;                    


		Vector vec_datos = new Vector();
		vec_datos = request.getSession().getAttribute("informeMovEjecucionExcel") != null?
		   (Vector) request.getSession().getAttribute("informeMovEjecucionExcel"): new Vector();
		String nomMesInicio = "";
		String nomMesTermino = "";
		switch(mesInicio){
		case 1: nomMesInicio = "ENERO";
		break;
		case 2: nomMesInicio = "FEBRERO";
		break;
		case 3: nomMesInicio = "MARZO";
		break;
		case 4: nomMesInicio = "ABRIL";
		break;
		case 5: nomMesInicio = "MAYO";
		break;
		case 6: nomMesInicio = "JUNIO";
		break;
		case 7: nomMesInicio = "JULIO";
		break;
		case 8: nomMesInicio = "AGOSTO";
		break;
		case 9: nomMesInicio = "SEPTIEMBRE";
		break;
		case 10: nomMesInicio = "OCTUBRE";
		break;
		case 11: nomMesInicio = "NOVIEMBRE";
		break;
		case 12: nomMesInicio = "DICIEMBRE";
		break;
		}
		switch(mesTermino){
		case 1: nomMesTermino = "ENERO";
		break;
		case 2: nomMesTermino = "FEBRERO";
		break;
		case 3: nomMesTermino = "MARZO";
		break;
		case 4: nomMesTermino = "ABRIL";
		break;
		case 5: nomMesTermino = "MAYO";
		break;
		case 6: nomMesTermino = "JUNIO";
		break;
		case 7: nomMesTermino = "JULIO";
		break;
		case 8: nomMesTermino = "AGOSTO";
		break;
		case 9: nomMesTermino = "SEPTIEMBRE";
		break;
		case 10: nomMesTermino = "OCTUBRE";
		break;
		case 11: nomMesTermino = "NOVIEMBRE";
		break;
		case 12: nomMesTermino = "DICIEMBRE";
		break;
		}
		if (vec_datos.size() > 0) {
			
			String archivo = "EjecPresupRemuneracion_" + anno;
			String titulo = "";
			String subtitulo2 = "UNIDAD: " + nomUnidad;
			String subtitulo3 = "MESES DE " +nomMesInicio + " A " + nomMesTermino + " DE " + anno;

			titulo = "LISTADO DE MOVIMIENTOS PRESUPUESTARIOS  ";

			//fecha
			java.util.Calendar f = new java.util.GregorianCalendar();
			String diaActual = String.valueOf(f.get(java.util.Calendar.DAY_OF_MONTH));
			String mesActual = String.valueOf(f.get(java.util.Calendar.MONTH) + 1);
			String a�oActual = String.valueOf(f.get(java.util.Calendar.YEAR));
			String hora = String.valueOf(f.get(java.util.Calendar.HOUR_OF_DAY));
			String min = String.valueOf(f.get(java.util.Calendar.MINUTE));
			if (hora.length() == 1)
				hora = "0" + hora;
			if (min.length() == 1)
				min = "0" + min;

			try {
				int fila = 0;

				ServletOutputStream sos = response.getOutputStream();
				ByteArrayOutputStream OUT = new ByteArrayOutputStream();

				HSSFWorkbook wb = new HSSFWorkbook();
				HSSFSheet sheet1 = wb.createSheet(titulo);

				// Estilo Arial-BOLD
				HSSFFont font1 = wb.createFont();
				font1.setFontHeightInPoints((short) 10);
				font1.setFontName("Arial");
				font1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				HSSFCellStyle style_bold = wb.createCellStyle();
				style_bold.setFont(font1);

				// Estilo Arial-ITALICA
				HSSFFont font2 = wb.createFont();
				font2.setFontHeightInPoints((short) 10);
				font2.setFontName("Arial");
				font2.setItalic(true);
				HSSFCellStyle style_italica = wb.createCellStyle();
				style_italica.setFont(font2);

				// Estilo FECHA
				HSSFCellStyle style_fecha = wb.createCellStyle();
				style_fecha.setDataFormat(HSSFDataFormat.getBuiltinFormat("d/m/yy h:mm"));

				// Estilo BORDE BOLD
				HSSFCellStyle style_borde_bold = wb.createCellStyle();
				style_borde_bold.setBorderBottom((short) 1);
				style_borde_bold.setBorderLeft((short) 1);
				style_borde_bold.setBorderRight((short) 1);
				style_borde_bold.setBorderTop((short) 1);
				style_borde_bold.setAlignment((short) 1);
				style_borde_bold.setFont(font1);

				// Estilo BORDE
				HSSFCellStyle style_borde = wb.createCellStyle();
				style_borde.setBorderBottom((short) 1);
				style_borde.setBorderLeft((short) 1);
				style_borde.setBorderRight((short) 1);
				style_borde.setBorderTop((short) 1);
				style_borde.setAlignment((short) 1);

				// Estilo BORDE
				HSSFCellStyle style_borde_centro = wb.createCellStyle();
				style_borde_centro.setBorderBottom((short) 1);
				style_borde_centro.setBorderLeft((short) 1);
				style_borde_centro.setBorderRight((short) 1);
				style_borde_centro.setBorderTop((short) 1);
				style_borde_centro.setAlignment((short) 2);

				// Estilo numerico
				DataFormat format = wb.createDataFormat();
				CellStyle style_numerico = wb.createCellStyle();
				style_numerico = wb.createCellStyle();
				style_numerico.setDataFormat(format.getFormat("##,###,###,###,##0"));
				style_numerico.setBorderBottom((short) 1);
				style_numerico.setBorderLeft((short) 1);
				style_numerico.setBorderRight((short) 1);
				style_numerico.setBorderTop((short) 1);
				style_numerico.setAlignment((short) 3);

				// T�tulo
				HSSFRow row_tit = sheet1.createRow((short) fila);
				HSSFCell cell_tit = row_tit.createCell((short) 0);
				row_tit.createCell((short) 0);
				cell_tit.setCellValue(titulo);
				cell_tit.setCellStyle(style_bold);

				fila++;
				// subT�tulo
				HSSFRow row_tit2 = sheet1.createRow((short) fila);
				HSSFCell cell_tit2 = row_tit2.createCell((short) 0);
				row_tit2.createCell((short) 0);
				cell_tit2.setCellValue(subtitulo2);
				cell_tit2.setCellStyle(style_bold);

				fila++;
				// subT�tulo 2 
				HSSFRow row_tit3 = sheet1.createRow((short) fila);
				HSSFCell cell_tit3 = row_tit3.createCell((short) 0);
				row_tit3.createCell((short) 0);
				cell_tit3.setCellValue(subtitulo3);
				cell_tit3.setCellStyle(style_bold);

				fila++;
				HSSFRow row_tit4 = sheet1.createRow((short) fila);
				HSSFCell cell_tit4 = row_tit4.createCell((short) 0);
				row_tit4.createCell((short) 0);
				cell_tit4.setCellValue("Generado el " + diaActual + "/"
						+ mesActual + "/" + a�oActual + " " + hora
						+ ":" + min);
				cell_tit4.setCellStyle(style_italica);

				fila = fila + 2;

				Vector vec_lista = new Vector();
				Vector vec_encabezado = new Vector();

			    vec_encabezado.addElement("TIPO DE DOCUMENTO");
			    vec_encabezado.addElement("NUMERO DOCUMENTO");
			    vec_encabezado.addElement("FECHA");
			    vec_encabezado.addElement("CONTRA CUENTA");
			    vec_encabezado.addElement("ITEM"); 
			    vec_encabezado.addElement("IDENTIFICADOR/GLOSA");
			    vec_encabezado.addElement("PR/NU");
			    vec_encabezado.addElement("CARGO");
			    vec_encabezado.addElement("ABONO");
			    vec_encabezado.addElement("DATOS ADICIONALES");
			    vec_encabezado.addElement("DIGIT.");

				//Cabezera de la tabla
				int fila_enc = fila++;
				HSSFRow row0 = sheet1.createRow((short) fila_enc);
				for (int m = 0; m < vec_encabezado.size(); m++) {

					HSSFCell cell0 = row0.createCell((short) m);
					String columname = vec_encabezado.get(m) + "";
					row0.createCell((short) m);
					cell0.setCellValue(columname);
					cell0.setCellStyle(style_borde_bold);
				}

				for (int i = 0; i < vec_datos.size(); i++) {
					vec_lista = new Vector();

					vec_lista.addElement(((Vector) vec_datos.get(i)).get(0));/*TIPO DE DOCUEMENTO*/
					vec_lista.addElement(((Vector) vec_datos.get(i)).get(1));/*NUMERO DOCUMENTO*/
					vec_lista.addElement(((Vector) vec_datos.get(i)).get(2));/*FECHA*/
					vec_lista.addElement(((Vector) vec_datos.get(i)).get(3));/*CON. CUENTA*/
					vec_lista.addElement(((Vector) vec_datos.get(i)).get(4));/*ITEM*/
					vec_lista.addElement(((Vector) vec_datos.get(i)).get(5));/*IDENTIFICADOR/GLOSA*/
					vec_lista.addElement(((Vector) vec_datos.get(i)).get(6));/*PR/NU*/
					vec_lista.addElement(((Vector) vec_datos.get(i)).get(7));/*CARGO*/
					vec_lista.addElement(((Vector) vec_datos.get(i)).get(8));/*ABONO*/
					vec_lista.addElement(((Vector) vec_datos.get(i)).get(9));/*DIGIT.*/		
					vec_lista.addElement(((Vector) vec_datos.get(i)).get(10));/*descripcion adicional.*/	
				    

					// Contenido de la tabla
					HSSFRow row = sheet1.createRow((short) fila++);
					int columna = 0;

					for (int v = 0; v < vec_lista.size(); v++) {
						columna++;
						HSSFCell cel00 = row.createCell((short) v);
						
						if (columna == 8 || columna == 9) {
							cel00.setCellValue(Long.parseLong(vec_lista.get(v)+ ""));
							cel00.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
							cel00.setCellStyle(style_numerico);
						} else
							cel00.setCellValue(vec_lista.get(v) + "".trim());

						if (columna == 10)
							columna = 0;
						cel00.setCellStyle(style_borde);
					}
				}

				// autosize each column
				for (int m = 0; m < vec_encabezado.size(); m++) {
					sheet1.autoSizeColumn(m, true);
					if (m > 0)
						if (m == 0 || m == 5 || m == 9)
							sheet1.setColumnWidth((short) m,(short) (6500));
						else
							sheet1.setColumnWidth((short) m,(short) (3000));

				}

				wb.write(OUT); // se escribe el xls en un ByteArrayOutputStream

				response.setContentLength(OUT.size());
				response.setContentType("application/x-download");
				response.setHeader("Content-Disposition",
						"attachment;filename=" + archivo + ".xls;");

				//Se pone el fichero en la salida
				byte[] bufferExcel = OUT.toByteArray();
				sos.write(bufferExcel, 0, bufferExcel.length);

				//Se muestra la salida.
				sos.flush();
				sos.close();
				OUT.close();
				if (request.getSession().getAttribute(
						"informeMovEjecucionExcel") != null)
					session
							.removeAttribute("informeMovEjecucionExcel");
				return;

			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
%>