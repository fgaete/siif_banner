<%@page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 25, 20);


// MAA cambia unidad por organizacion
int rutUsuario = session.getAttribute("rutUsuario") != null?
        Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
//int unidad   = request.getParameter("unidad") != null?
	//              Integer.parseInt(request.getParameter("unidad")):0;
String organizacion  = request.getParameter("organizacion") != null?
	    	           request.getParameter("organizacion").trim():"";
int anno     = request.getParameter("anno") != null &&
                !request.getParameter("anno").trim().equals("")?
                Integer.parseInt(request.getParameter("anno")):0;
String funcionario  = request.getParameter("funcionario") != null &&
                !request.getParameter("funcionario").trim().equals("")?
                request.getParameter("funcionario"):"";
try{
	    Vector vec_datos = new Vector();
	    Vector vec_datos2 = new Vector();
	    Vector vec_datos3 = new Vector();
	    //String nomUnidad = "";
	    String nomOrganizacion = "";
	    vec_datos = request.getSession().getAttribute("listaCorrientePDF") != null ?
	    		   (Vector) request.getSession().getAttribute("listaCorrientePDF"): new Vector();
	    vec_datos2 = request.getSession().getAttribute("listaReqResumenPDF") != null?
	    				    (Vector) request.getSession().getAttribute("listaReqResumenPDF"): new Vector();
	    vec_datos3 = request.getSession().getAttribute("listaReqDetResumenPDF") != null?
	    				    (Vector) request.getSession().getAttribute("listaReqDetResumenPDF"):new Vector();		   
	    if (vec_datos.size() > 0)	{
	    	//nomUnidad = vec_datos.get(vec_datos.size()-1).toString();
	    	nomOrganizacion = vec_datos.get(vec_datos.size()-1).toString();
	    	vec_datos.setElementAt("",vec_datos.size()-1);
	    }
	            
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 
	NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);		    

   // System.out.println("vec_datos corriente: " + vec_datos.size() + " - "+vec_datos);
	document.addTitle("PRESUPUESTACIÓN CORRIENTE " + anno);
	document.addAuthor(funcionario.trim());
	//document.addSubject(nomUnidad);
	document.addSubject(nomOrganizacion);	
	
	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(
                new Phrase("Página ", helv), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);

    HeaderFooter header = new HeaderFooter(
                new Phrase("PRESUPUESTACIÓN CORRIENTE " + anno, new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    document.open();
	
    Vector vec_encabezado = new Vector();
   // vec_encabezado.addElement("ITEM");
    vec_encabezado.addElement("CUENTA");
    vec_encabezado.addElement("TOTAL ANUAL $");
    vec_encabezado.addElement("ENE $");
    vec_encabezado.addElement("FEB $");
    vec_encabezado.addElement("MAR $");
    vec_encabezado.addElement("ABR $");
    vec_encabezado.addElement("MAY $");
    vec_encabezado.addElement("JUN $");
    vec_encabezado.addElement("JUL $");
    vec_encabezado.addElement("AGO $");
    vec_encabezado.addElement("SEP $");
    vec_encabezado.addElement("OCT $");
    vec_encabezado.addElement("NOV $");
    vec_encabezado.addElement("DIC $");
    vec_encabezado.addElement("OBSERVACIONES");    

   
    
 // add a table to the document
 	float[] widths = {
		 0.05f, 0.2f, 0.1f, 0.1f,
		 0.1f, 0.1f, 0.1f, 0.1f,
		 0.1f, 0.1f, 0.1f, 0.1f,
		 0.1f, 0.1f, 0.1f, 0.2f}; /*largo de las columnas, en este caso son 16*/
	PdfPTable table = new PdfPTable(widths);
    table.setWidthPercentage(100);
    table.setHeaderRows(2);
    
    //Cabecera de la tabla
	PdfPCell cell = null;
    
   // cell = new PdfPCell(new Paragraph("UNIDAD: " + nomUnidad, new Font(bf_helvb)));
   cell = new PdfPCell(new Paragraph("ORGANIZACIÓN: " + nomOrganizacion, new Font(bf_helvb)));
    cell.setBorder(0);
    cell.setPaddingBottom(10);
    cell.setColspan(16);
    table.addCell(cell);
    
    for (int m = 0 ; m < vec_encabezado.size(); m++){
      cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), new Font(Font.HELVETICA, 8, Font.BOLD)));
      cell.setBackgroundColor(Color.LIGHT_GRAY);
      if (m == 0) cell.setColspan(2);
      table.addCell(cell);
    }

    //Contenido de la tabla
    int columna = 0;
    boolean color = false;
    int x = 1;
    for (int d = 0 ; d < vec_datos.size(); d++){
    
     if (d >= vec_datos.size()-16)
    	  cell = new PdfPCell(new Paragraph(vec_datos.get(d).toString(), new Font(Font.HELVETICA, 8, Font.BOLD)));
      else 
    	  cell = new PdfPCell(new Paragraph(vec_datos.get(d).toString(), new Font(Font.HELVETICA, 8)));
      
      if (columna == 0) {
      	  if (vec_datos.get(d).toString().trim().equals("")) color = true;
      }
  
      if (color) {
    	  cell.setBackgroundColor(Color.LIGHT_GRAY);
    	  x++;
      }
      
      if (x == 17) {
    	  color = false;
    	  x = 1;
      }
      
      
      columna ++;
      if (columna > 2 && columna < 16) {
    	  cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    	  cell.setNoWrap(true);
      }
      
      table.addCell(cell);
      if (columna == 16) columna = 0;
      
    }   

    document.add(table); 
    document.close();
    
   

	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	session.removeAttribute("listaCorrientePDF");
}catch(DocumentException e){
	e.printStackTrace();
}

%>