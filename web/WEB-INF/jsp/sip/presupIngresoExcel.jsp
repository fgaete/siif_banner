<%@ page language="java" import="descad.presupuesto.*,
                                 java.util.*,
                                 java.io.*,
                                 descad.cliente.PreswBean,
                                 java.text.NumberFormat,
                                 org.apache.poi.ss.usermodel.DataFormat,
                                 org.apache.poi.poifs.filesystem.POIFSFileSystem,
                                 org.apache.poi.hssf.usermodel.*,                               
                                 org.apache.poi.ss.util.*,
                                 org.apache.poi.ss.usermodel.CellStyle"
                                 contentType="application/vnd.ms-excel"%><%

  // Creado por           : M�nica Barrera F.
  // Fecha                : 29/04/2011
  // �ltima Actualizaci�n :

  int rutUsuario = session.getAttribute("rutUsuario") != null?
                    Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
   
  if(rutUsuario > 0) {
    int anno = request.getParameter("anno") != null &&
                          !request.getParameter("anno").trim().equals("")?
                           Integer.parseInt(request.getParameter("anno")):0; 
    int cargaIngreso = request.getParameter("cargaIngreso") != null &&
                           !request.getParameter("cargaIngreso").trim().equals("")?
                            Integer.parseInt(request.getParameter("cargaIngreso")):0;
                                                  
   String nomSede = request.getParameter("nomSede") != null &&
                           !request.getParameter("nomSede").trim().equals("")?
                            request.getParameter("nomSede"):"";                        
   String nomUni = request.getParameter("nomUni") != null &&
                            !request.getParameter("nomUni").trim().equals("")?
                             request.getParameter("nomUni"):"";  
  String nomDoc = request.getParameter("nomDoc") != null &&
                             !request.getParameter("nomDoc").trim().equals("")?
                              request.getParameter("nomDoc"):""; 
  //System.out.println("cargaIngreso: "+cargaIngreso) ;                          
    Vector vec_datos = new Vector();
    vec_datos = request.getSession().getAttribute("listadoPresupIngreso") != null ?
    		   (Vector) request.getSession().getAttribute("listadoPresupIngreso"):new Vector();
    		   
    NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);
	   
    if (vec_datos.size() > 0 ) {
      
      String archivo = "PresupIngreso" + anno;
      String titulo = "";
      String subtitulo2 = "A�O: " + anno  ; 
      String subtitulo3 = ""; 
      String subtitulo4 = "";
      String subtitulo5 = "";

      if(!nomSede.trim().equals("") && cargaIngreso > 1)
    	  subtitulo3 = "Campus "+ nomSede;
      if(!nomUni.trim().equals("") && cargaIngreso > 2)
    	  subtitulo4 = "Departamento " + nomUni;
      if(!nomDoc.trim().equals("") && cargaIngreso > 3)
    	  subtitulo5 = "Carrera " + nomDoc;
      titulo = "Ingresos Pregrado Diurno USM ";
          
         //fecha
      java.util.Calendar f = new java.util.GregorianCalendar();
      String diaActual = String.valueOf(f.get(java.util.Calendar.DAY_OF_MONTH));
      String mesActual = String.valueOf(f.get(java.util.Calendar.MONTH) + 1);
      String a�oActual = String.valueOf(f.get(java.util.Calendar.YEAR));
      String hora      = String.valueOf(f.get(java.util.Calendar.HOUR_OF_DAY));
      String min       = String.valueOf(f.get(java.util.Calendar.MINUTE));
      if (hora.length() == 1) hora = "0"+hora;
      if (min.length()  == 1) min  = "0"+min;

      try {
        int fila = 0;

        ServletOutputStream sos = response.getOutputStream();
        ByteArrayOutputStream OUT = new ByteArrayOutputStream();

        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet1 = wb.createSheet(titulo);

        // Estilo Arial-BOLD
        HSSFFont font1 = wb.createFont();
        font1.setFontHeightInPoints((short)10);
        font1.setFontName("Arial");
        font1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        HSSFCellStyle style_bold = wb.createCellStyle();
        style_bold.setFont(font1);

        // Estilo Arial-ITALICA
        HSSFFont font2 = wb.createFont();
        font2.setFontHeightInPoints((short)10);
        font2.setFontName("Arial");
        font2.setItalic(true);
        HSSFCellStyle style_italica = wb.createCellStyle();
        style_italica.setFont(font2);

        // Estilo FECHA
        HSSFCellStyle style_fecha = wb.createCellStyle();
        style_fecha.setDataFormat(HSSFDataFormat.getBuiltinFormat("d/m/yy h:mm"));

        // Estilo BORDE BOLD
        HSSFCellStyle style_borde_bold= wb.createCellStyle();
        style_borde_bold.setBorderBottom((short)1);
        style_borde_bold.setBorderLeft((short)1);
        style_borde_bold.setBorderRight((short)1);
        style_borde_bold.setBorderTop((short)1);
        style_borde_bold.setAlignment((short)2);
        style_borde_bold.setFont(font1);

        // Estilo BORDE
        HSSFCellStyle style_borde= wb.createCellStyle();
        style_borde.setBorderBottom((short)1);
        style_borde.setBorderLeft((short)1);
        style_borde.setBorderRight((short)1);
        style_borde.setBorderTop((short)1);
        style_borde.setAlignment((short)1);

        // Estilo BORDE
        HSSFCellStyle style_borde_centro= wb.createCellStyle();
        style_borde_centro.setBorderBottom((short)1);
        style_borde_centro.setBorderLeft((short)1);
        style_borde_centro.setBorderRight((short)1);
        style_borde_centro.setBorderTop((short)1);
        style_borde_centro.setAlignment((short)2);

     	// Estilo numerico
        DataFormat format = wb.createDataFormat();
        CellStyle style_numerico= wb.createCellStyle();
        style_numerico = wb.createCellStyle();
        style_numerico.setDataFormat(format.getFormat("##,###,###,###,##0"));        
        style_numerico.setBorderBottom((short)1);
        style_numerico.setBorderLeft((short)1);
        style_numerico.setBorderRight((short)1);
        style_numerico.setBorderTop((short)1);
        style_numerico.setAlignment((short)3);
        
        
        // T�tulo
        HSSFRow row_tit = sheet1.createRow((short)fila);
        HSSFCell cell_tit = row_tit.createCell((short)0);
        row_tit.createCell((short)0);
        cell_tit.setCellValue(titulo);
        cell_tit.setCellStyle(style_bold);

        fila++;
        // subT�tulo
        HSSFRow row_tit2 = sheet1.createRow((short)fila);
        HSSFCell cell_tit2 = row_tit2.createCell((short)0);
        row_tit2.createCell((short)0);
        cell_tit2.setCellValue(subtitulo2);
        cell_tit2.setCellStyle(style_bold);

        fila++;
        // subT�tulo3
        HSSFRow row_tit3 = sheet1.createRow((short)fila);
        HSSFCell cell_tit3 = row_tit3.createCell((short)0);
        row_tit3.createCell((short)0);
        cell_tit3.setCellValue(subtitulo3);
        cell_tit3.setCellStyle(style_bold);

        fila++;
        // subT�tulo4
        HSSFRow row_tit5 = sheet1.createRow((short)fila);
        HSSFCell cell_tit5 = row_tit5.createCell((short)0);
        row_tit5.createCell((short)0);
        cell_tit5.setCellValue(subtitulo4);
        cell_tit5.setCellStyle(style_bold);
        
        fila++;
        // subT�tulo5
        HSSFRow row_tit6 = sheet1.createRow((short)fila);
        HSSFCell cell_tit6 = row_tit6.createCell((short)0);
        row_tit6.createCell((short)0);
        cell_tit6.setCellValue(subtitulo5);
        cell_tit6.setCellStyle(style_bold);
        
        fila++;
        HSSFRow row_tit4 = sheet1.createRow((short)fila);
        HSSFCell cell_tit4 = row_tit4.createCell((short)0);
        row_tit4.createCell((short)0);
        cell_tit4.setCellValue("Generado el " + diaActual + "/" + mesActual + "/" + a�oActual +
        " " + hora +":" + min);
        cell_tit4.setCellStyle(style_italica);

        fila = fila + 2;

        Vector vec_lista = new Vector();
        Vector vec_encabezado = new Vector();

        
	    vec_encabezado.addElement("");
	    switch (cargaIngreso){
	        case 1:	vec_encabezado.addElement("Sede/Unidad");
	        break;
	        case 2:	vec_encabezado.addElement("Departamento");
	        break;
	        case 3:	vec_encabezado.addElement("Carrera");
	        break;
	        case 4:	vec_encabezado.addElement("Menci�n");
	        break;
	    }
	    vec_encabezado.addElement("Total Aranceles");
	    vec_encabezado.addElement("Pago por Caja");
	    vec_encabezado.addElement("Cred. Univ.");
	    vec_encabezado.addElement("Becas Gobierno");
	    vec_encabezado.addElement("Becas Privadas");
	    vec_encabezado.addElement("Becas USM");
	    vec_encabezado.addElement("CAE");
	    vec_encabezado.addElement("Gratuidad Estado");
	    vec_encabezado.addElement("Dif. Gratuidad");
	    vec_encabezado.addElement("Total Ingresos");
	    vec_encabezado.addElement("Saldo del A�o");
	    vec_encabezado.addElement("% Morosidad");
	    vec_encabezado.addElement("Pagos Posteriores");
	    vec_encabezado.addElement("Saldo Actual");
	    vec_encabezado.addElement("% Morosidad");
	        
	   

	    //Cabecera1 de la tabla
	    int fila_enc = fila++;
	    HSSFRow row_e0 = sheet1.createRow((short)fila_enc);
	    CellRangeAddress region1 = new CellRangeAddress(7 /*firstRow*/, 7 /*lastRow*/, 3 /*firstCol*/, 10 /*lastCol*/) ;
        sheet1.addMergedRegion(region1);
        HSSFCell cell_e0 = row_e0.createCell((short)3);
        cell_e0.setCellValue("Ingresos generados en el periodo");
        cell_e0.setCellStyle(style_borde_bold);
        cell_e0 = row_e0.createCell((short)4);
        cell_e0.setCellStyle(style_borde_bold);
        cell_e0 = row_e0.createCell((short)5);
        cell_e0.setCellStyle(style_borde_bold);
        cell_e0 = row_e0.createCell((short)6);
        cell_e0.setCellStyle(style_borde_bold);
        cell_e0 = row_e0.createCell((short)7);
        cell_e0.setCellStyle(style_borde_bold);
        cell_e0 = row_e0.createCell((short)8);
        cell_e0.setCellStyle(style_borde_bold);
        cell_e0 = row_e0.createCell((short)9);
        cell_e0.setCellStyle(style_borde_bold);
        cell_e0 = row_e0.createCell((short)10);
        cell_e0.setCellStyle(style_borde_bold);

        

        
        //Cabecera2 de la tabla
        fila_enc = fila++;
        HSSFRow row_e1 = sheet1.createRow((short)fila_enc);

        CellRangeAddress region2 = new CellRangeAddress(8 /*firstRow*/, 8 /*lastRow*/, 0 /*firstCol*/, 1 /*lastCol*/) ;
        sheet1.addMergedRegion(region2);
        HSSFCell cell_e00 = row_e1.createCell((short)0);
        cell_e00.setCellValue(vec_encabezado.get(1)+"");
        cell_e00.setCellStyle(style_borde_bold);
        cell_e00 = row_e1.createCell((short)1);
        cell_e00.setCellStyle(style_borde_bold);

        
        for (int m = 2 ; m < vec_encabezado.size(); m++){
          
          HSSFCell cell_e1 = row_e1.createCell((short)m);
          String columname = vec_encabezado.get(m)+"";
          row_e1.createCell((short)m);
          cell_e1.setCellValue(columname);
          cell_e1.setCellStyle(style_borde_bold);
        }
        
    	
        
        for (int i = 0; i < vec_datos.size(); i++){  
          vec_lista = new Vector();
         
       	    vec_lista.addElement(((Vector)vec_datos.get(i)).get(0)+"");/*SEDE/UNIDAD*/
           	vec_lista.addElement(((Vector)vec_datos.get(i)).get(1)+"");/*SEDE/UNIDAD*/
          	vec_lista.addElement(((Vector)vec_datos.get(i)).get(2)+"");/*TOTAL ARANCELES*/
          	vec_lista.addElement(((Vector)vec_datos.get(i)).get(3)+"");/*PAGO POR CAJA*/
          	vec_lista.addElement(((Vector)vec_datos.get(i)).get(4)+"");/*CRED. UNIV.*/
          	vec_lista.addElement(((Vector)vec_datos.get(i)).get(5)+"");/*BECAS GOBIERNO*/
          	vec_lista.addElement(((Vector)vec_datos.get(i)).get(6)+"");/*BECAS PRIVADAS*/
          	vec_lista.addElement(((Vector)vec_datos.get(i)).get(7)+"");/*BECAS USM*/
          	vec_lista.addElement(((Vector)vec_datos.get(i)).get(8)+"");/*CAE*/
        	vec_lista.addElement(((Vector)vec_datos.get(i)).get(9)+"");/*Gratuidad Estado*/
        	vec_lista.addElement(((Vector)vec_datos.get(i)).get(10)+"");/*Dif. Gratuidad*/
            vec_lista.addElement(((Vector)vec_datos.get(i)).get(11)+"");/*TOTAL INGRESOS*/
         	vec_lista.addElement(((Vector)vec_datos.get(i)).get(12)+"");/*SALDO DEL A�O*/
         	vec_lista.addElement(((Vector)vec_datos.get(i)).get(13)+"");/*% MOROSIDAD*/
         	vec_lista.addElement(((Vector)vec_datos.get(i)).get(14)+"");/*PAGOS POSTERIORES*/
         	vec_lista.addElement(((Vector)vec_datos.get(i)).get(15)+"");/* SALDO ACTUAL */
         	vec_lista.addElement(((Vector)vec_datos.get(i)).get(16)+"");/* MOROSIDAD */
                     
          	// Contenido de la tabla
         	HSSFRow row = sheet1.createRow((short)fila++);
         	int columna = 0;
          
         	for (int v = 0 ; v < vec_lista.size(); v++){
        		columna ++;     
        		
            	HSSFCell cel00 = row.createCell((short)v);
            	cel00.setCellStyle(style_borde);
            	if (columna > 2 && !(vec_lista.get(v)+"").trim().equals("") && ((vec_lista.get(v)+"").indexOf(".") < 0)) {                
            		cel00.setCellValue(Long.parseLong(vec_lista.get(v)+""));
            	   	cel00.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
                	cel00.setCellStyle(style_numerico);
            	} else {
                	cel00.setCellValue(vec_lista.get(v)+"");     
                	if (vec_lista.get(v).equals("Total")) 
                		cel00.setCellStyle(style_borde_centro);
            	}
            
            	if (columna == 15) columna = 0;    
            
          	}
      
        }
        System.out.println("f: "+fila);
        CellRangeAddress region3 = new CellRangeAddress(fila-1 /*firstRow*/, fila-1 /*lastRow*/, 0 /*firstCol*/, 1 /*lastCol*/) ;
        sheet1.addMergedRegion(region3);

        
            	
    	// autosize each column
        for (int m = 0 ; m < vec_encabezado.size(); m++){	
          	sheet1.autoSizeColumn(m, true);  
          if (m > 0 )
        	  if(m > 1)
        		  sheet1.setColumnWidth((short)m,(short)(4000)); 
        	  else
        	      sheet1.setColumnWidth((short)m,(short)(6500));

        }

        /*fila++;

    	HSSFRow row_b = sheet1.createRow((short)fila++); 
    	HSSFCell cel_b = row_b.createCell((short)0);
    	cel_b.setCellValue("Informaci�n en millones de pesos"); */
    	
               
        wb.write(OUT); // se escribe el xls en un ByteArrayOutputStream
        
        response.setContentLength(OUT.size());
        response.setContentType ("application/x-download");
    	response.setHeader("Content-Disposition", "attachment;filename="+archivo+".xls;");

        //Se pone el fichero en la salida
        byte[] bufferExcel = OUT.toByteArray();
        sos.write(bufferExcel,0,bufferExcel.length);

        //Se muestra la salida.
        sos.flush();
        sos.close();
        OUT.close();
        if(request.getSession().getAttribute("listadoPresupIngreso") != null)
        	session.removeAttribute("listadoPresupIngreso");
    	return;    

        }
      catch ( IOException ex ) {
        ex.printStackTrace();
      }
    }
  }
%>