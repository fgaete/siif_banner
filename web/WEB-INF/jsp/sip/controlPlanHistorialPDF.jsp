<%@page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 25, 20);

int rutUsuario = session.getAttribute("rutUsuario") != null?
        Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
    	String   nomMacro =  request.getParameter("nomMacro")!=null &&
        !request.getParameter("nomMacro").trim().equals("null") &&
        !request.getParameter("nomMacro").trim().equals("NULL") &&
        !request.getParameter("nomMacro").trim().equals("")?request.getParameter("nomMacro")+"":"";
String   codUnidadMacro =  request.getParameter("codUnidadMacro")!=null &&
        !request.getParameter("codUnidadMacro").trim().equals("null") &&
        !request.getParameter("codUnidadMacro").trim().equals("NULL") &&
        !request.getParameter("codUnidadMacro").trim().equals("")?request.getParameter("codUnidadMacro")+"":"";
String   nomCuenta =  request.getParameter("nomCuenta")!=null &&
        !request.getParameter("nomCuenta").trim().equals("null") &&
        !request.getParameter("nomCuenta").trim().equals("NULL") &&
        !request.getParameter("nomCuenta").trim().equals("")?request.getParameter("nomCuenta")+"":"";
int codCuenta = (request.getParameter("codCuenta") != null)?Integer.parseInt(request.getParameter("codCuenta")+""):0;



//System.out.println(vec_datos)	; 
try{

	Vector vec_datos = new Vector();

	vec_datos = request.getSession().getAttribute("planDesarrolloExcel") != null ?
	    (Vector) request.getSession().getAttribute("planDesarrolloExcel"):new Vector();
    
	    
	            
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 
	NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);		    
 

	document.addTitle("CONTROL PLAN DE DESARROLLO POR ACTIVIDAD, HISTORIAL" );
	//document.addAuthor(funcionario.trim());
	document.addSubject("ControlPlanDesarrollo");	
	

	
	
	
	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(
                new Phrase("P�gina ", helv), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);

    HeaderFooter header = new HeaderFooter(
                new Phrase("CONTROL PLAN DE DESARROLLO POR ACTIVIDAD, HISTORIAL" , new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    document.open();
	
    Vector vec_encabezado = new Vector();
    vec_encabezado.addElement("TIPO");
    vec_encabezado.addElement("FECHA");
    vec_encabezado.addElement("INGRESADO");
    vec_encabezado.addElement("DOCUMENTO");
    vec_encabezado.addElement("IDENTIFICADOR");
    vec_encabezado.addElement("VALOR");
    vec_encabezado.addElement("PROV.");
      

   
    
 // add a table to the document
 	float[] widths = {
		 0.2f, 0.1f, 0.1f,
		 0.1f, 0.1f,
		 0.1f, 0.1f}; /*largo de las columnas, en este caso son 6*/
	PdfPTable table = new PdfPTable(widths);
    table.setWidthPercentage(100);
  //  table.setHeaderRows(2);
    
    //Cabecera de la tabla
	PdfPCell cell = null;
    
    cell = new PdfPCell(new Paragraph("MACROUNIDAD: " + codUnidadMacro + "-" + nomMacro, new Font(bf_helvb)));
    cell.setBorder(0);
    cell.setPaddingBottom(10);
    cell.setColspan(16);
    table.addCell(cell);
    
    cell = new PdfPCell(new Paragraph("CUENTA ACTIVIDAD: " + codCuenta + "-" + nomCuenta, new Font(bf_helvb)));
    cell.setBorder(0);
    cell.setPaddingBottom(10);
    cell.setColspan(16);
    table.addCell(cell);
       
    
    
    
    for (int m = 0 ; m < vec_encabezado.size(); m++){
      cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), new Font(Font.HELVETICA, 8, Font.BOLD)));
      cell.setBackgroundColor(Color.LIGHT_GRAY);   
      table.addCell(cell);
    }


	


    Vector vec_lista = new Vector();
    for (int i = 0; i < vec_datos.size(); i++){
    	vec_lista = new Vector();         
    	  vec_lista.addElement(((Vector)vec_datos.get(i)).get(0));/*TIPO*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(1));/*FECHA*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(2));/*INGRESADO*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(3));/*% DOCUMENTO*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(4));/*IDENTIFICADOR*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(5));/*% VALOR*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(6));/*% PROV*/
       
    
    	  
    //Contenido de la tabla
    int columna = 0;
    for (int d = 0 ; d < vec_lista.size(); d++){
      columna ++;
      cell = new PdfPCell(new Paragraph(vec_lista.get(d).toString(), new Font(Font.HELVETICA, 8)));
      if ((columna == 6) || (columna == 4)) {
    	  cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong((vec_lista.get(d).toString()))), new Font(Font.HELVETICA, 8)));
    	  cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    	  cell.setNoWrap(true);
 
      }
      table.addCell(cell);
      if (columna == 6) columna = 0;      
    }   
    }
    cell = new PdfPCell(new Paragraph("* Indica que hay documentos en proceso ", new Font(bf_helvb)));
    cell.setBorder(0);
    cell.setPaddingBottom(10);
    cell.setColspan(16);
    table.addCell(cell);
    document.add(table); 
	
    document.newPage();
    


  
    document.close();
    
   

	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	session.removeAttribute("planDesarrolloExcel");
}catch(DocumentException e){
	e.printStackTrace();
}

%>