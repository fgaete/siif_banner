<%@
page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat,
		java.text.DecimalFormat"
%><%
// Creado por           : M�nica Barrera F.
// Fecha                : 25/08/2011
// �ltima Actualizaci�n :
	
	
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 25, 20);/*.rotate() es para dejar la hoja horizontal*/
int rutUsuario = session.getAttribute("rutUsuario") != null?
                 Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
                        
Vector vec_datos = new Vector();
vec_datos = request.getSession().getAttribute("listadoIndicadores") != null ?
            (Vector) request.getSession().getAttribute("listadoIndicadores"):new Vector();
               // System.out.println(vec_datos);
String titulo =  session.getAttribute("titulo") != null?
                 session.getAttribute("titulo")+"":"";	
                      //   System.out.println("titulo: "+titulo);
Vector   vec_annos = request.getSession().getAttribute("annoIndicadores") != null ?
              	(Vector) request.getSession().getAttribute("annoIndicadores"):new Vector();        
              		//  System.out.println("vec_annos: "+vec_annos);
String tipo =  session.getAttribute("tipo") != null?
              	session.getAttribute("tipo")+"":"";	
              		       //     System.out.println("tipo: "+titulo);
              		        //   System.out.println("tipo: "+tipo);   

try{
  
    
    NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);	
    Locale l = new Locale("es","CL");
    DecimalFormat formatter2 = (DecimalFormat)DecimalFormat.getInstance(l);
    formatter2.applyPattern("###,###.###");
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 

	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(new Phrase("P�gina ", helv), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);
   
      
    HeaderFooter header = new HeaderFooter(new Phrase(titulo, new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    //document.open();
	
    // datos
    Vector vec_encabezado = new Vector();   
    float[] colsWidth = null;
    int largocols = 0;
  
   
    vec_encabezado.addElement("Indicador");
    for(int i=0;i<vec_annos.size();i++){
        vec_encabezado.addElement(vec_annos.get(i)); 
      
    }
    colsWidth = new float[] {2f,1f, 1f, 1f, 1f, 1f, 1f, 1f}; // Code 1
    largocols = 8;


  
    
    
        // add a table to the document
        document.open();
		
		PdfPTable table = new PdfPTable(colsWidth);
		table.setWidthPercentage(100); // Code 2
		table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
		
		PdfPCell cell = null;
		
		
		for (int m = 0 ; m < vec_encabezado.size(); m++){
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table.addCell(cell);
	      	
	    }		
		 
			for (int j = 0 ; j < vec_datos.size(); j++){
					
						for (int d = 0 ; d <= 7; d++){	
							if(d==0 ){
								  	cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(d).toString(), helv));	
							  		cell.setHorizontalAlignment(Element.ALIGN_LEFT);							  		
							  		table.addCell(cell); 
							} else {
								if(d>0){
									if(tipo.trim().equals("IIP")){
										if(j == 0)
											cell = new PdfPCell(new Paragraph(formatter2.format(Double.parseDouble(((Vector) vec_datos.get(j)).get(d).toString())) , helv));
										else
										    cell = new PdfPCell(new Paragraph(formatter2.format(Double.parseDouble(((Vector) vec_datos.get(j)).get(d).toString())/100) + "%", helv));
											
									}else 
										cell = new PdfPCell(new Paragraph(formatoNum.format(Double.parseDouble(((Vector) vec_datos.get(j)).get(d).toString())/100), helv));
								cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
								table.addCell(cell);
								}
								 
							}
					   	     	
			    		} 
					
					}

		document.add(table);
		
		PdfPTable table3 = new PdfPTable(colsWidth);
		table3.setWidthPercentage(100); // Code 2
		table3.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
		
		PdfPCell cell_b = null;
		cell_b = new PdfPCell(new Paragraph(" * Informaci�n en base a persupuesto del A�o.", helvb));
		cell_b.setBorder(0);
		cell_b.setColspan(largocols);
		table3.addCell(cell_b);
		
		document.add(table3); 
		
		
		document.close();


	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
    if(request.getSession().getAttribute("listadoIndicadores") != null)
    	session.removeAttribute("listadoIndicadores");
    if(request.getSession().getAttribute("titulo") != null)
    	session.removeAttribute("titulo");
    if(request.getSession().getAttribute("annoIndicadores") != null)
    	session.removeAttribute("annoIndicadores");
    if(request.getSession().getAttribute("tipo") != null)
    	session.removeAttribute("tipo");
}catch(DocumentException e){
	e.printStackTrace();
}

%>