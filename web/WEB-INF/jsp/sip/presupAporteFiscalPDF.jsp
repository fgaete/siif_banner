<%@
page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%
// Creado por           : M�nica Barrera F.
// Fecha                : 02/05/2011
// �ltima Actualizaci�n :
	
	
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 25, 20);/*.rotate() es para dejar la hoja horizontal*/
int rutUsuario = session.getAttribute("rutUsuario") != null?
                 Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
                        
Vector vec_datos = new Vector();
vec_datos = request.getSession().getAttribute("listadoPresupSerie") != null ?
	   (Vector) request.getSession().getAttribute("listadoPresupSerie"):new Vector();
String tipo = request.getParameter("tipo")	;
Vector vec_sede = request.getSession().getAttribute("listaSede") != null ?
		   (Vector) request.getSession().getAttribute("listaSede"):new Vector();

try{
  
    
    NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);		    
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 

	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(new Phrase("P�gina ", helv), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);
    String subtitulo2 = "Aporte Fiscal Directo (AFD)" ; 
    String textoOpcion = "";
    textoOpcion = "Series de Tiempo e Informaci�n Estad�stica ";
    if(tipo.trim().equals("AFI"))
  	  subtitulo2 = "Aporte Fiscal Indirecto (AFI)" ; 
    if(tipo.trim().equals("FSC"))
  	  subtitulo2 = "Fondo Solidario de Cr�dito Universitario" ; 
    if(tipo.trim().equals("FGB"))
  	  subtitulo2 = "Fondos Gubernamentales para Becas de Arancel" ; 
 
    //fecha
    java.util.Calendar f = new java.util.GregorianCalendar();
    String diaActual = String.valueOf(f.get(java.util.Calendar.DAY_OF_MONTH));
    String mesActual = String.valueOf(f.get(java.util.Calendar.MONTH) + 1);
    String a�oActual = String.valueOf(f.get(java.util.Calendar.YEAR));
    String hora      = String.valueOf(f.get(java.util.Calendar.HOUR_OF_DAY));
    String min       = String.valueOf(f.get(java.util.Calendar.MINUTE));
    if (hora.length() == 1) hora = "0"+hora;
    if (min.length()  == 1) min  = "0"+min;  
    
    
    HeaderFooter header = new HeaderFooter(new Phrase(textoOpcion, new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    //document.open();
    
    int porc = 100;
	
    // datos
    Vector vec_encabezado = new Vector();   
    float[] colsWidth = null;
    int largocols = 0;
  
    if(tipo.trim().equals("AFD")){
        vec_encabezado.addElement("A�o");	     
        vec_encabezado.addElement("95% de Fondo");
        vec_encabezado.addElement("5% de Fondo");
        vec_encabezado.addElement("Total USM");
        vec_encabezado.addElement("Total Fondo Nacional");
        vec_encabezado.addElement("Participaci�n USM ");
        colsWidth = new float[] {1f, 1f, 1f, 1f, 1f, 1f}; // Code 1
        largocols = 6;
        porc = 50;
    }
     if(tipo.trim().equals("FSC") ){
    	     vec_encabezado.addElement("A�o");	 
    	     vec_encabezado.addElement("Total USM");
    	     vec_encabezado.addElement("Total Fondo Nacional");
    	     vec_encabezado.addElement("Participaci�n USM ");
	         colsWidth = new float[] {1f, 1f, 1f, 1f}; // Code 1
	         largocols = 4;
	         porc = 50;
     }
     if(tipo.trim().equals("FGB")){
	     vec_encabezado.addElement("A�o");	 
	     vec_encabezado.addElement("Total USM");
	     colsWidth = new float[] {1f, 1f}; // Code 1
         largocols = 2;
         porc = 30;
 }
     if(tipo.trim().equals("AFI")){
    	 vec_encabezado.addElement("A�o");
    	for (int i = 0 ; i < vec_sede.size(); i++){           	     
    	  	  
 	    vec_encabezado.addElement(((Vector)vec_sede.get(i)).get(1)+"");	     
     	}
        vec_encabezado.addElement("Total USM");
        vec_encabezado.addElement("Total Fondo Nacional");
        vec_encabezado.addElement("Participaci�n USM ");
    	 colsWidth = new float[] {1f, 1f, 1f, 1f,1f, 1f, 1f, 1f, 1f, 1f}; // Code 1
    	 largocols = vec_sede.size() +4;
    	 
  }
    
    
        // add a table to the document
        document.open();
		
		PdfPTable table = new PdfPTable(colsWidth);
		table.setWidthPercentage(porc); // Code 2
		table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
		
		PdfPCell cell = null;
		cell = new PdfPCell(new Paragraph(subtitulo2, new Font(bf_helvb)));
		cell.setBorder(0);
		cell.setPaddingBottom(10);
	    cell.setColspan(largocols);
		table.addCell(cell);		
		
		
		for (int m = 0 ; m < vec_encabezado.size(); m++){
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	       	cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	      	table.addCell(cell);
	      	
	    }		
		        
		for (int j = 0 ; j < vec_datos.size(); j++){ 
			  long total = 0;
				for (int d = 0 ; d < largocols; d++){
				
						
					if(d==0 || ((Vector) vec_datos.get(j)).get(d).toString().trim().equals("") || (((Vector) vec_datos.get(j)).get(d).toString().indexOf(".") > 0)){
						    if(/*tipo.trim().equals("AFD") && */(((Vector)vec_datos.get(j)).get(d)+"").trim().equals(a�oActual.trim()))
						 		cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(d).toString().concat("*"), helv));	
					  		else
						 	  	cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(d).toString(), helv));	
						    if((((Vector) vec_datos.get(j)).get(d).toString().indexOf(".") > 0)){
						    	cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(d).toString().concat("%"), helv));
						    	cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						    } else cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					} else {
						 total = total + Long.parseLong(((Vector)vec_datos.get(j)).get(d)+"");
						cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector) vec_datos.get(j)).get(d).toString())), helv));
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					}
					 if(tipo.trim().equals("AFI") && d == 9 ){
							cell = new PdfPCell(new Paragraph(formatoNum.format(total), helv));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					 }
			   		table.addCell(cell);   
			   		if(tipo.trim().equals("AFI")  && d == 0) d = 2;
	    		}
				if(tipo.trim().equals("AFI") ){
					cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(1).toString(), helv));	
					cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector) vec_datos.get(j)).get(1).toString())), helv));
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table.addCell(cell);

					cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(2).toString().concat("%"), helv));	
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table.addCell(cell);
			}
			}
		
 
		
	
		document.add(table);
		
		/*if(tipo.trim().equals("AFD")) {*/
			PdfPTable table2 = new PdfPTable(colsWidth);
			table2.setWidthPercentage(porc); // Code 2
			table2.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
			
			PdfPCell cell_a = null;
			cell_a = new PdfPCell(new Paragraph("* Presupuesto del a�o", helvb));
			cell_a.setBorder(0);
			cell_a.setPaddingTop(10);
			cell_a.setColspan(largocols);
			table2.addCell(cell_a);
			
			document.add(table2);
			
			PdfPTable table3 = new PdfPTable(colsWidth);
			table3.setWidthPercentage(porc); // Code 2
			table3.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
			
			PdfPCell cell_b = null;
			cell_b = new PdfPCell(new Paragraph("Informaci�n en millones de pesos.", helvb));
			cell_b.setBorder(0);
			cell_b.setColspan(largocols);
			table3.addCell(cell_b);
			
			document.add(table3);
			
		/*}*/
		
	
		document.close();


	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	session.removeAttribute("listadoPresupSerie");
}catch(DocumentException e){
	e.printStackTrace();
}

%>