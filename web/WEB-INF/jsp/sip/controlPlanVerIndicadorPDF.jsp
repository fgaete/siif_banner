<%@page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 25, 20);

int rutUsuario = session.getAttribute("rutUsuario") != null?
        Integer.parseInt(session.getAttribute("rutUsuario")+""):0;



//System.out.println(vec_datos)	; 
try{

	Vector vec_datos = new Vector();

	vec_datos = request.getSession().getAttribute("planDesarrolloExcel") != null ?
	    (Vector) request.getSession().getAttribute("planDesarrolloExcel"):new Vector();
	    String   nomMacro =  request.getParameter("nomMacro")!=null &&
        !request.getParameter("nomMacro").trim().equals("null") &&
        !request.getParameter("nomMacro").trim().equals("NULL") &&
        !request.getParameter("nomMacro").trim().equals("")?request.getParameter("nomMacro")+"":"";
String   codUnidadMacro =  request.getParameter("codUnidadMacro")!=null &&
        !request.getParameter("codUnidadMacro").trim().equals("null") &&
        !request.getParameter("codUnidadMacro").trim().equals("NULL") &&
        !request.getParameter("codUnidadMacro").trim().equals("")?request.getParameter("codUnidadMacro")+"":"";
String   nomCuenta =  request.getParameter("nomCuenta")!=null &&
        !request.getParameter("nomCuenta").trim().equals("null") &&
        !request.getParameter("nomCuenta").trim().equals("NULL") &&
        !request.getParameter("nomCuenta").trim().equals("")?request.getParameter("nomCuenta")+"":"";
String   comen1 =  request.getParameter("comen1")!=null &&
        !request.getParameter("comen1").trim().equals("null") &&
        !request.getParameter("comen1").trim().equals("NULL") &&
        !request.getParameter("comen1").trim().equals("")?request.getParameter("comen1")+"":"";
String   comen2 =  request.getParameter("comen2")!=null &&
        !request.getParameter("comen2").trim().equals("null") &&
        !request.getParameter("comen2").trim().equals("NULL") &&
        !request.getParameter("comen2").trim().equals("")?request.getParameter("comen2")+"":"";
String   comen3 =  request.getParameter("comen3")!=null &&
        !request.getParameter("comen3").trim().equals("null") &&
        !request.getParameter("comen3").trim().equals("NULL") &&
        !request.getParameter("comen3").trim().equals("")?request.getParameter("comen3")+"":"";
String   comen4 =  request.getParameter("comen4")!=null &&
        !request.getParameter("comen4").trim().equals("null") &&
        !request.getParameter("comen4").trim().equals("NULL") &&
        !request.getParameter("comen4").trim().equals("")?request.getParameter("comen4")+"":"";
String   comen5 =  request.getParameter("comen5")!=null &&
        !request.getParameter("comen5").trim().equals("null") &&
        !request.getParameter("comen5").trim().equals("NULL") &&
        !request.getParameter("comen5").trim().equals("")?request.getParameter("comen5")+"":"";
String   indicador =  request.getParameter("indicador")!=null &&
        !request.getParameter("indicador").trim().equals("null") &&
        !request.getParameter("indicador").trim().equals("NULL") &&
        !request.getParameter("indicador").trim().equals("")?request.getParameter("indicador")+"":"";

int codCuenta = (request.getParameter("codCuenta") != null)?Integer.parseInt(request.getParameter("codCuenta")+""):0;
            
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 
	NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);		    


	document.addTitle("CONTROL PLAN DE DESARROLLO " );
	//document.addAuthor(funcionario.trim());
	document.addSubject("Valores de Indicadores");		
	
	
	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(
                new Phrase("P�gina ", helv), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);

    HeaderFooter header = new HeaderFooter(
                new Phrase("CONTROL PLAN DE DESARROLLO POR ACTIVIDADES, VALOR INDICADORES" , new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    document.open();
	
    Vector vec_encabezado = new Vector();
    vec_encabezado.addElement("FECHA");
    vec_encabezado.addElement("INGRESADO POR");
    vec_encabezado.addElement("VALOR INDICADOR"); 

   
    
 // add a table to the document
 	float[] widths = {
		 0.2f, 0.1f, 0.2f}; /*largo de las columnas, en este caso son 3*/
	PdfPTable table = new PdfPTable(widths);
    table.setWidthPercentage(100);
  //  table.setHeaderRows(2);
    
    //Cabecera de la tabla
	PdfPCell cell = null;
    
	 cell = new PdfPCell(new Paragraph("MACROUNIDAD: " + codUnidadMacro + "-" + nomMacro, new Font(bf_helvb)));
	    cell.setBorder(0);
	    cell.setPaddingBottom(10);
	    cell.setColspan(3);
	    table.addCell(cell);
	    
	    
	    cell = new PdfPCell(new Paragraph("CUENTA ACTIVIDAD: " + codCuenta + "-" + nomCuenta, new Font(bf_helvb)));
	    cell.setBorder(0);
	    cell.setPaddingBottom(10);
	    cell.setColspan(3);
	    table.addCell(cell);
	    
	    cell = new PdfPCell(new Paragraph("INDICADOR: " + indicador , new Font(bf_helvb)));
	    cell.setBorder(0);
	    cell.setPaddingBottom(10);
	    cell.setColspan(3);
	    table.addCell(cell);
	    
	    cell = new PdfPCell(new Paragraph("DESCRIPCI�N: " + comen1.trim() + " " + comen2.trim() + " " + comen3.trim() +" " + comen4.trim() , new Font(bf_helvb)));
	    cell.setBorder(0);
	    cell.setPaddingBottom(10);
	    cell.setColspan(3);
	    table.addCell(cell);
	    
	    cell = new PdfPCell(new Paragraph("METAS: " + comen5.trim() , new Font(bf_helvb)));
	    cell.setBorder(0);
	    cell.setPaddingBottom(10);
	    cell.setColspan(3);
	    table.addCell(cell);
	    
	  
	    
	    cell = new PdfPCell(new Paragraph(" " , new Font(bf_helvb)));
	    cell.setBorder(0);
	    cell.setPaddingBottom(10);
	    cell.setColspan(3);
	    table.addCell(cell);
    
    for (int m = 0 ; m < vec_encabezado.size(); m++){
      cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), new Font(Font.HELVETICA, 8, Font.BOLD)));
      cell.setBackgroundColor(Color.LIGHT_GRAY);   
      table.addCell(cell);
    }


    Vector vec_lista = new Vector();
    for (int i = 0; i < vec_datos.size(); i++){
    	vec_lista = new Vector();         
        vec_lista.addElement(((Vector)vec_datos.get(i)).get(0));/*FECHA*/
        vec_lista.addElement(((Vector)vec_datos.get(i)).get(1));/*INGRESADO POR*/
        vec_lista.addElement(((Vector)vec_datos.get(i)).get(2));/*VALOR INDICADOR*/
        
    	  
    //Contenido de la tabla
    for (int d = 0 ; d < vec_lista.size(); d++){
      cell = new PdfPCell(new Paragraph(vec_lista.get(d).toString(), new Font(Font.HELVETICA, 8)));
      table.addCell(cell);
    }   
    }
    document.add(table);
    document.newPage();  
    document.close(); 
   

	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	session.removeAttribute("planDesarrolloExcel");
}catch(DocumentException e){
	e.printStackTrace();
}

%>