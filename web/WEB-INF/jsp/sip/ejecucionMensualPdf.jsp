<%@page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%
	
	response.setContentType("application/pdf");
	Document document = new Document(PageSize.A4.rotate(), 20, 20, 25, 20);/*.rotate() es para dejar la hoja horizontal*/
	int rutUsuario = session.getAttribute("rutUsuario") != null?
	        Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
	        String nomUnidad = request.getParameter("nomUnidad") != null &&
	        !request.getParameter("nomUnidad").trim().equals("")?
	         request.getParameter("nomUnidad"):"";  
	int mes = request.getParameter("mes") != null &&
	          !request.getParameter("mes").trim().equals("")?
	        		  Integer.parseInt(request.getParameter("mes")):0;   
	int anno = request.getParameter("anno") != null &&
	           !request.getParameter("anno").trim().equals("")?
	            Integer.parseInt(request.getParameter("anno")):0; 
	Vector vec_datos1 =  request.getSession().getAttribute("ejecucionMensualExportarET1") != null ?
	          (Vector) request.getSession().getAttribute("ejecucionMensualExportarET1"):new Vector();
	Vector vec_datos2 =  request.getSession().getAttribute("ejecucionMensualExportarET2") != null ?
			     (Vector) request.getSession().getAttribute("ejecucionMensualExportarET2"):new Vector();
	Vector vec_datos3 =  request.getSession().getAttribute("ejecucionMensualExportarET3") != null ?
		    	(Vector) request.getSession().getAttribute("ejecucionMensualExportarET3"):new Vector();

	try{
	
	
	   String nomMes = "";
	   switch(mes){
	   case 1 : nomMes = "ENERO";
	   break;
	   case 2 : nomMes = "FEBRERO";
	   break;
	   case 3 : nomMes = "MARZO";
	   break;
	   case 4 : nomMes = "ABRIL";
	   break;
	   case 5 : nomMes = "MAYO";
	   break;
	   case 6 : nomMes = "JUNIO";
	   break;
	   case 7 : nomMes = "JULIO";
	   break;
	   case 8 : nomMes = "AGOSTO";
	   break;
	   case 9 : nomMes = "SEPTIEMBRE";
	   break;
	   case 10 : nomMes = "OCTUBRE";
	   break;
	   case 11 : nomMes = "NOVIEMBRE";
	   break;
	   case 12 : nomMes = "DICIEMBRE";
	   break;
	   }
 
  
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 
	NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);		    
	

	document.addTitle("RESUMEN DE EJECUCI�N PRESUPUESTARIA " + anno);
	document.addSubject(nomUnidad);	
	
	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(
                new Phrase("P�gina ", new Font(bf_helv)), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);

    HeaderFooter header = new HeaderFooter(
                new Phrase("RESUMEN DE EJECUCI�N PRESUPUESTARIA  " + anno + " - " + nomMes, new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    document.open();
	
    // datos
    Vector vec_encabezado = new Vector(); 
    Vector vec_encabezado2 = new Vector();
    if(vec_datos1.size() > 0){
        vec_encabezado.addElement("ITEM");
        vec_encabezado.addElement("PRESUPUESTO (+) $");
        vec_encabezado.addElement("TRASPASOS (+) $");
        vec_encabezado.addElement("GASTO REAL (-) $");
        vec_encabezado.addElement("DIFERENCIA $");
   

  
    
 // add a table to the document
 	float[] widths = {2f, 1f, 1f, 1f, 1f}; /*largo de las columnas, en este caso son cinco*/
	PdfPTable table = new PdfPTable(widths);
    table.setWidthPercentage(100);
    table.setHeaderRows(2);/* cantidad de filas cabeceras para que se repitan en cada hoja*/
    
    PdfPCell cell = null;
    
    cell = new PdfPCell(new Paragraph("UNIDAD: " + nomUnidad, new Font(bf_helvb)));
    cell.setBorder(0);
    cell.setPaddingBottom(10);
    cell.setColspan(11);
    table.addCell(cell);
    
  
    
	//Cabecera de la tabla    
    for (int m = 0 ; m < vec_encabezado.size(); m++){
      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), new Font(bf_helvb)));
      	cell.setBackgroundColor(Color.LIGHT_GRAY);
      	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
      	table.addCell(cell);
    }
//System.out.println("vec_datos1: "+vec_datos1);
    //Contenido de la tabla
    for (int d = 0 ; d < (vec_datos1.size()); d++){   
    	cell = new PdfPCell(new Paragraph(((Vector)vec_datos1.get(d)).get(0).toString(), new Font(bf_helv)));
    	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
      	table.addCell(cell);  
      	cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector)vec_datos1.get(d)).get(1).toString())), new Font(bf_helv)));
    	cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
      	table.addCell(cell);
      	cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector)vec_datos1.get(d)).get(2).toString())), new Font(bf_helv)));
    	cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
      	table.addCell(cell);  
      	cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector)vec_datos1.get(d)).get(3).toString())), new Font(bf_helv)));
    	cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
      	table.addCell(cell); 
      	cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector)vec_datos1.get(d)).get(4).toString())), new Font(bf_helv)));
    	cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
      	table.addCell(cell);  
    }    
    //Inicio - PARA LA FILA EN BLANCO, se agregan 3 filas
    cell = new PdfPCell(new Paragraph("")); 
    cell.setColspan(5); 
    cell.setBorder(0); 
    table.addCell(cell); 
    table.addCell(cell);
    table.addCell(cell); 
    //Fin - PARA LA FILA EN BLANCO

    document.add(table); 
    
    }
    if(vec_datos2.size() > 0){
        
        // add a table to the document
     	float[] widths = {2f,1f}; /*largo de las columnas, en este caso son tres*/
     	PdfPCell cell = null;
     	
    	PdfPTable table = new PdfPTable(widths);
        table.setWidthPercentage(80);
        table.setHeaderRows(2);/* cantidad de filas cabeceras para que se repitan en cada hoja*/
        
        
     
    
        
      //Cabecera de la tabla    
           	cell = new PdfPCell(new Paragraph("DISPONIBILIDAD", new Font(bf_helvb)));
            cell.setColspan(2);
          	cell.setBackgroundColor(Color.LIGHT_GRAY);
          	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
          	table.addCell(cell);
     
    	
        
      
        
  
        //Contenido de la tabla
        for (int d = 0 ; d < (vec_datos2.size()); d++){      
        
            	cell = new PdfPCell(new Paragraph(((Vector)vec_datos2.get(d)).get(0).toString(), new Font(bf_helv)));
            	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              	table.addCell(cell);  
              	
              	cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector)vec_datos2.get(d)).get(1).toString())), new Font(bf_helv)));
            	cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
              	table.addCell(cell);               
        }    
       
        document.add(table); 
    }
    if(vec_datos3.size() > 0){
    	vec_encabezado = new Vector(); 
        vec_encabezado.addElement("CONCEPTO");
        vec_encabezado.addElement("PRESUPUESTO $");
        vec_encabezado.addElement("GASTO REAL $");
        vec_encabezado.addElement("DESVIACI�N");
        // add a table to the document
     	float[] widths = {2f, 1f, 1f, 1f}; /*largo de las columnas, en este caso son tres*/
    	PdfPTable table = new PdfPTable(widths);
        table.setWidthPercentage(100);
        table.setHeaderRows(2);/* cantidad de filas cabeceras para que se repitan en cada hoja*/
        
        PdfPCell cell = null;
        
        cell = new PdfPCell(new Paragraph("UNIDAD: " + nomUnidad, new Font(bf_helvb)));
        cell.setBorder(0);
        cell.setPaddingBottom(10);
        cell.setColspan(11);
        table.addCell(cell);
        
              
    	//Cabecera de la tabla    
        for (int m = 0 ; m < vec_encabezado.size(); m++){
          	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), new Font(bf_helvb)));
          	cell.setBackgroundColor(Color.LIGHT_GRAY);
          	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
          	table.addCell(cell);
        }
        cell = null;
        //Contenido de la tabla
        for (int d = 0 ; d < (vec_datos3.size()); d++){      
        	 	cell = new PdfPCell(new Paragraph(((Vector)vec_datos3.get(d)).get(0).toString(), new Font(bf_helv)));
            	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              	table.addCell(cell);  
        	 	cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector)vec_datos3.get(d)).get(1).toString())), new Font(bf_helv)));
            	cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
              	table.addCell(cell); 
        	 	cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector)vec_datos3.get(d)).get(2).toString())), new Font(bf_helv)));
            	cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
              	table.addCell(cell);  
        	 	cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector)vec_datos3.get(d)).get(3).toString())), new Font(bf_helv)));
            	cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
              	table.addCell(cell);  
     
     
        	}    
       
        document.add(table); 
    }

 // Comentario final
 //   document.add(new Paragraph(""));
 //   document.add(new Paragraph("Observaci�n y comentario general:", new Font(bf_helvb)));
 //  document.add(new Paragraph(observacion, new Font(bf_helv)));

 
    document.close();
	

	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
    if(request.getSession().getAttribute("ejecucionMensualExportarET1") != null)
    	session.removeAttribute("ejecucionMensualExportarET1");
    if(request.getSession().getAttribute("ejecucionMensualExportarET2") != null)
    	session.removeAttribute("ejecucionMensualExportarET2");
    if(request.getSession().getAttribute("ejecucionMensualExportarET3") != null)
    	session.removeAttribute("ejecucionMensualExportarET3");
}catch(DocumentException e){
	e.printStackTrace();
}

%>