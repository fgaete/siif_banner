<%@ page language="java" import="descad.presupuesto.*,
                                 java.util.*,
                                 java.io.*,
                                 descad.cliente.PreswBean,
                                 java.text.NumberFormat,
                                 org.apache.poi.ss.usermodel.DataFormat,
                                 org.apache.poi.poifs.filesystem.POIFSFileSystem,
                                 org.apache.poi.hssf.usermodel.*,
                                 org.apache.poi.ss.usermodel.CellStyle"
                                 contentType="application/vnd.ms-excel"%><%
                                 
	// Creado por           : M�nica Barrera F.
	// Fecha                : 16/08/2010
	// �ltima Actualizaci�n :

	int rutUsuario = session.getAttribute("rutUsuario") != null ? 
			Integer.parseInt(session.getAttribute("rutUsuario") + ""): 0;

	if (rutUsuario > 0) {
		String nomUnidad = request.getParameter("nomUnidad") != null
				&& !request.getParameter("nomUnidad").trim().equals("") ? 
				request.getParameter("nomUnidad"): "";
		int mes = request.getParameter("mes") != null
				&& !request.getParameter("mes").trim().equals("") ? 
				Integer.parseInt(request.getParameter("mes")): 0;
		int anno = request.getParameter("anno") != null
				&& !request.getParameter("anno").trim().equals("") ? 
				Integer.parseInt(request.getParameter("anno")): 0;
		Vector vec_datos1 = request.getSession().getAttribute("ejecucionMensualExportarET1") != null ? 
				(Vector) request.getSession().getAttribute("ejecucionMensualExportarET1"): new Vector();
		Vector vec_datos2 = request.getSession().getAttribute("ejecucionMensualExportarET2") != null ? 
				(Vector) request.getSession().getAttribute("ejecucionMensualExportarET2"): new Vector();
		Vector vec_datos3 = request.getSession().getAttribute("ejecucionMensualExportarET3") != null ? 
				(Vector) request.getSession().getAttribute("ejecucionMensualExportarET3"): new Vector();

		NumberFormat formatoNum = NumberFormat.getInstance(Locale.GERMAN);

		String nomMes = "";
		switch (mes) {
		case 1:
			nomMes = "ENERO";
			break;
		case 2:
			nomMes = "FEBRERO";
			break;
		case 3:
			nomMes = "MARZO";
			break;
		case 4:
			nomMes = "ABRIL";
			break;
		case 5:
			nomMes = "MAYO";
			break;
		case 6:
			nomMes = "JUNIO";
			break;
		case 7:
			nomMes = "JULIO";
			break;
		case 8:
			nomMes = "AGOSTO";
			break;
		case 9:
			nomMes = "SEPTIEMBRE";
			break;
		case 10:
			nomMes = "OCTUBRE";
			break;
		case 11:
			nomMes = "NOVIEMBRE";
			break;
		case 12:
			nomMes = "DICIEMBRE";
			break;
		}

		String archivo = "ResumenEjecPresup_" + anno;
		String titulo = "";
		String subtitulo2 = "UNIDAD: " + nomUnidad;
		String subtitulo3 = " " + anno + " - " + nomMes;

		titulo = "RESUMEN DE EJECUCI�N PRESUPUESTARIA";
		//fecha
		java.util.Calendar f = new java.util.GregorianCalendar();
		String diaActual = String.valueOf(f.get(java.util.Calendar.DAY_OF_MONTH));
		String mesActual = String.valueOf(f.get(java.util.Calendar.MONTH) + 1);
		String a�oActual = String.valueOf(f.get(java.util.Calendar.YEAR));
		String hora = String.valueOf(f.get(java.util.Calendar.HOUR_OF_DAY));
		String min = String.valueOf(f.get(java.util.Calendar.MINUTE));
		if (hora.length() == 1)
			hora = "0" + hora;
		if (min.length() == 1)
			min = "0" + min;

		try {
			int fila = 0;

			ServletOutputStream sos = response.getOutputStream();
			ByteArrayOutputStream OUT = new ByteArrayOutputStream();

			HSSFWorkbook wb = new HSSFWorkbook();
			HSSFSheet sheet1 = wb.createSheet(titulo);

			// Estilo Arial-BOLD
			HSSFFont font1 = wb.createFont();
			font1.setFontHeightInPoints((short) 10);
			font1.setFontName("Arial");
			font1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			HSSFCellStyle style_bold = wb.createCellStyle();
			style_bold.setFont(font1);

			// Estilo Arial-ITALICA
			HSSFFont font2 = wb.createFont();
			font2.setFontHeightInPoints((short) 10);
			font2.setFontName("Arial");
			font2.setItalic(true);
			HSSFCellStyle style_italica = wb.createCellStyle();
			style_italica.setFont(font2);

			// Estilo FECHA
			HSSFCellStyle style_fecha = wb.createCellStyle();
			style_fecha.setDataFormat(HSSFDataFormat
					.getBuiltinFormat("d/m/yy h:mm"));

			// Estilo BORDE BOLD
			HSSFCellStyle style_borde_bold = wb.createCellStyle();
			style_borde_bold.setBorderBottom((short) 1);
			style_borde_bold.setBorderLeft((short) 1);
			style_borde_bold.setBorderRight((short) 1);
			style_borde_bold.setBorderTop((short) 1);
			style_borde_bold.setAlignment((short) 1);
			style_borde_bold.setFont(font1);

			// Estilo BORDE
			HSSFCellStyle style_borde = wb.createCellStyle();
			style_borde.setBorderBottom((short) 1);
			style_borde.setBorderLeft((short) 1);
			style_borde.setBorderRight((short) 1);
			style_borde.setBorderTop((short) 1);
			style_borde.setAlignment((short) 1);

			// Estilo BORDE
			HSSFCellStyle style_borde_centro = wb.createCellStyle();
			style_borde_centro.setBorderBottom((short) 1);
			style_borde_centro.setBorderLeft((short) 1);
			style_borde_centro.setBorderRight((short) 1);
			style_borde_centro.setBorderTop((short) 1);
			style_borde_centro.setAlignment((short) 2);

			// Estilo numerico
			DataFormat format = wb.createDataFormat();
			CellStyle style_numerico = wb.createCellStyle();
			style_numerico = wb.createCellStyle();
			style_numerico.setDataFormat(format
					.getFormat("##,###,###,###,##0"));
			style_numerico.setBorderBottom((short) 1);
			style_numerico.setBorderLeft((short) 1);
			style_numerico.setBorderRight((short) 1);
			style_numerico.setBorderTop((short) 1);
			style_numerico.setAlignment((short) 3);

			// T�tulo
			HSSFRow row_tit = sheet1.createRow((short) fila);
			HSSFCell cell_tit = row_tit.createCell((short) 0);
			row_tit.createCell((short) 0);
			cell_tit.setCellValue(titulo);
			cell_tit.setCellStyle(style_bold);

			fila++;
			// subT�tulo
			HSSFRow row_tit2 = sheet1.createRow((short) fila);
			HSSFCell cell_tit2 = row_tit2.createCell((short) 0);
			row_tit2.createCell((short) 0);
			cell_tit2.setCellValue(subtitulo2);
			cell_tit2.setCellStyle(style_bold);

			fila++;
			// subT�tulo 2 
			HSSFRow row_tit3 = sheet1.createRow((short) fila);
			HSSFCell cell_tit3 = row_tit3.createCell((short) 0);
			row_tit3.createCell((short) 0);
			cell_tit3.setCellValue(subtitulo3);
			cell_tit3.setCellStyle(style_bold);

			fila++;
			HSSFRow row_tit4 = sheet1.createRow((short) fila);
			HSSFCell cell_tit4 = row_tit4.createCell((short) 0);
			row_tit4.createCell((short) 0);
			cell_tit4.setCellValue("Generado el " + diaActual + "/"
					+ mesActual + "/" + a�oActual + " " + hora + ":"
					+ min);
			cell_tit4.setCellStyle(style_italica);

			fila = fila + 2;
			Vector vec_lista = new Vector();
			Vector vec_encabezado = new Vector();
			Vector vec_encabezado2 = new Vector();

			if (vec_datos1.size() > 0) {
				vec_encabezado.addElement("ITEM");
				vec_encabezado.addElement("PRESUPUESTO (+) $");
				vec_encabezado.addElement("TRASPASOS (+) $");
				vec_encabezado.addElement("GASTO REAL (-) $");
				vec_encabezado.addElement("DIFERENCIA $");
			}
			if (vec_datos2.size() > 0) {
				vec_encabezado2.addElement("DISPONIBILIDAD");
			}
			if (vec_datos3.size() > 0) {
				vec_encabezado.addElement("CONCEPTO");
				vec_encabezado.addElement("PRESUPUESTO $");
				vec_encabezado.addElement("GASTO REAL $");
				vec_encabezado.addElement("DESVIACI�N");
			}

			if (vec_datos1.size() > 0) {
				//Cabezera de la tabla
				int fila_enc = fila++;
				HSSFRow row0 = sheet1.createRow((short) fila_enc);
				for (int m = 0; m < vec_encabezado.size(); m++) {

					HSSFCell cell0 = row0.createCell((short) m);
					String columname = vec_encabezado.get(m) + "";
					row0.createCell((short) m);
					cell0.setCellValue(columname);
					cell0.setCellStyle(style_borde_bold);
				}

				for (int i = 0; i < vec_datos1.size(); i++) {
					vec_lista = new Vector();

					vec_lista.addElement(((Vector) vec_datos1.get(i)).get(0));/*ITEM*/
					vec_lista.addElement(((Vector) vec_datos1.get(i)).get(1));/*PRESUPUESTO (+) $*/
					vec_lista.addElement(((Vector) vec_datos1.get(i)).get(2));/*TRASPASOS $*/
					vec_lista.addElement(((Vector) vec_datos1.get(i)).get(3));/*GASTO REAL*/
					vec_lista.addElement(((Vector) vec_datos1.get(i)).get(4));/*DIFERENCIA $*/

					// Contenido de la tabla
					HSSFRow row = sheet1.createRow((short) fila++);
					int columna = 0;

					for (int v = 0; v < vec_lista.size(); v++) {
						columna++;
						HSSFCell cel00 = row.createCell((short) v);
						cel00.setCellStyle(style_borde);
						if (columna > 1) {
							cel00.setCellValue(Long.parseLong(vec_lista.get(v)+ ""));
							cel00.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
							cel00.setCellStyle(style_numerico);
						} else
							cel00.setCellValue(vec_lista.get(v) + "");

						if (columna == 5)
							columna = 0;

					}

					// autosize each column
					for (int m = 0; m < vec_encabezado.size(); m++) {
						sheet1.autoSizeColumn(m, true);
						if (m == 0)
							sheet1.setColumnWidth((short) m,(short) (8000));
						else
							sheet1.setColumnWidth((short) m,(short) (4000));

					}
				}
			}

			if (vec_datos2.size() > 0) {
				//Cabezera de la tabla
				fila = fila + 2;
				int fila_enc = fila++;
				HSSFRow row0 = sheet1.createRow((short) fila_enc);
				for (int m = 0; m < vec_encabezado2.size(); m++) {

					HSSFCell cell0 = row0.createCell((short) m);
					String columname = vec_encabezado2.get(m) + "";
					row0.createCell((short) m);
					cell0.setCellValue(columname);
					cell0.setCellStyle(style_borde_bold);
				}
				for (int i = 0; i < vec_datos2.size(); i++) {
					vec_lista = new Vector();

					vec_lista.addElement(((Vector) vec_datos2.get(i)).get(0));/**/
					vec_lista.addElement(((Vector) vec_datos2.get(i)).get(1));/**/

					// Contenido de la tabla
					HSSFRow row = sheet1.createRow((short) fila++);
					int columna = 0;

					for (int v = 0; v < vec_lista.size(); v++) {
						columna++;
						HSSFCell cel00 = row.createCell((short) v);
						cel00.setCellStyle(style_borde);
						if (columna > 1) {
							cel00.setCellValue(Long.parseLong(vec_lista.get(v)+ ""));
							cel00.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
							cel00.setCellStyle(style_numerico);
						} else
							cel00.setCellValue(vec_lista.get(v) + "");

						if (columna == 2)
							columna = 0;

					}

					// autosize each column
					for (int m = 0; m < vec_encabezado.size(); m++) {
						sheet1.autoSizeColumn(m, true);
						if (m == 0)
							sheet1.setColumnWidth((short) m,(short) (8000));
						else
							sheet1.setColumnWidth((short) m,(short) (4000));

					}
				}
			}
			if (vec_datos3.size() > 0) {
				//Cabezera de la tabla
				int fila_enc = fila++;
				HSSFRow row0 = sheet1.createRow((short) fila_enc);
				for (int m = 0; m < vec_encabezado.size(); m++) {

					HSSFCell cell0 = row0.createCell((short) m);
					String columname = vec_encabezado.get(m) + "";
					row0.createCell((short) m);
					cell0.setCellValue(columname);
					cell0.setCellStyle(style_borde_bold);
				}
				for (int i = 0; i < vec_datos3.size(); i++) {
					vec_lista = new Vector();

					vec_lista.addElement(((Vector) vec_datos3.get(i)).get(0));/**/
					vec_lista.addElement(((Vector) vec_datos3.get(i)).get(1));/**/
					vec_lista.addElement(((Vector) vec_datos3.get(i)).get(2));/**/
					vec_lista.addElement(((Vector) vec_datos3.get(i)).get(3));/**/

					// Contenido de la tabla
					HSSFRow row = sheet1.createRow((short) fila++);
					int columna = 0;

					for (int v = 0; v < vec_lista.size(); v++) {
						columna++;
						HSSFCell cel00 = row.createCell((short) v);
						cel00.setCellStyle(style_borde);
						if (columna > 1) {
							cel00.setCellValue(Long.parseLong(vec_lista.get(v)+ ""));
							cel00
									.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
							cel00.setCellStyle(style_numerico);
						} else
							cel00.setCellValue(vec_lista.get(v) + "");

						if (columna == 4)
							columna = 0;

					}

					// autosize each column
					for (int m = 0; m < vec_encabezado.size(); m++) {
						sheet1.autoSizeColumn(m, true);
						if (m == 0)
							sheet1.setColumnWidth((short) m,(short) (8000));
						else
							sheet1.setColumnWidth((short) m,(short) (4000));

					}
				}
			}

			wb.write(OUT); // se escribe el xls en un ByteArrayOutputStream

			response.setContentLength(OUT.size());
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition",
					"attachment;filename=" + archivo + ".xls;");

			//Se pone el fichero en la salida
			byte[] bufferExcel = OUT.toByteArray();
			sos.write(bufferExcel, 0, bufferExcel.length);

			//Se muestra la salida.
			sos.flush();
			sos.close();
			OUT.close();
			if (request.getSession().getAttribute(
					"ejecucionMensualExportarET1") != null)
				session.removeAttribute("ejecucionMensualExportarET1");
			if (request.getSession().getAttribute(
					"ejecucionMensualExportarET2") != null)
				session.removeAttribute("ejecucionMensualExportarET2");
			if (request.getSession().getAttribute(
					"ejecucionMensualExportarET3") != null)
				session.removeAttribute("ejecucionMensualExportarET3");
			return;

		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}
%>