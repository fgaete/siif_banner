<%@
page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%
// Creado por           : M�nica Barrera F.
// Fecha                : 02/05/2011
// �ltima Actualizaci�n :
	
	
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 25, 20);/*.rotate() es para dejar la hoja horizontal*/
int rutUsuario = session.getAttribute("rutUsuario") != null?
                 Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
int anno = request.getParameter("anno") != null &&
                 !request.getParameter("anno").trim().equals("")?
                  Integer.parseInt(request.getParameter("anno")):0; 
int cargaIngreso = request.getParameter("cargaIngreso") != null &&
                  !request.getParameter("cargaIngreso").trim().equals("")?
                   Integer.parseInt(request.getParameter("cargaIngreso")):0;
                                         
String nomSede = request.getParameter("nomSede") != null &&
                  !request.getParameter("nomSede").trim().equals("")?
                   request.getParameter("nomSede"):"";                        
String nomUni = request.getParameter("nomUni") != null &&
                   !request.getParameter("nomUni").trim().equals("")?
                    request.getParameter("nomUni"):"";  
String nomDoc = request.getParameter("nomDoc") != null &&
                    !request.getParameter("nomDoc").trim().equals("")?
                     request.getParameter("nomDoc"):""; 
//System.out.println("cargaIngreso: "+cargaIngreso) ;                          
Vector vec_datos = new Vector();
vec_datos = request.getSession().getAttribute("listadoPresupIngreso") != null ?
	   (Vector) request.getSession().getAttribute("listadoPresupIngreso"):new Vector();
try{
  
    
    NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);		    
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 

	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv7 = new Font(bf_helv);
    helv7.setSize(7);
    helv7.setColor(Color.BLACK);
    
    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(new Phrase("P�gina ", helv), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);
    String subtitulo2 = "A�O: " + anno  ; 
    String subtitulo3 = ""; 
    String subtitulo4 = "";
    String subtitulo5 = "";

    String textoOpcion = "";
    textoOpcion = "Ingresos Pregrado Diurno USM ";
    if(!nomSede.trim().equals("") && cargaIngreso > 1)
  	  subtitulo3 = "Campus "+ nomSede;
    if(!nomUni.trim().equals("") && cargaIngreso > 2)
  	  subtitulo4 = "Departamento " + nomUni;
    if(!nomDoc.trim().equals("") && cargaIngreso > 3)
  	  subtitulo5 = "Carrera " + nomDoc;
 
      
    HeaderFooter header = new HeaderFooter(new Phrase(textoOpcion, new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    //document.open();
	
    // datos
    Vector vec_encabezado = new Vector();   
    float[] colsWidth = null;
    int largocols = 0;
  
    vec_encabezado.addElement("");
    switch (cargaIngreso){
    case 1:	vec_encabezado.addElement("Sede/Unidad");
    break;
    case 2:	vec_encabezado.addElement("Departamento");
    break;
    case 3:	vec_encabezado.addElement("Carrera");
    break;
    case 4:	vec_encabezado.addElement("Menci�n");
    break;
    }
    vec_encabezado.addElement("Total Aranceles");
    vec_encabezado.addElement("Ingresos generados en el periodo");
    vec_encabezado.addElement("Total Ingresos");
    vec_encabezado.addElement("Saldo del A�o");
    vec_encabezado.addElement("% Morosidad");
    vec_encabezado.addElement("Pagos Posteriores");
    vec_encabezado.addElement("Saldo Actual");
    vec_encabezado.addElement("% Morosidad");
    colsWidth = new float[] {0.9f, 1.5f, 2f, 1.8f, 1.8f, 1.8f, 1.8f, 1.8f, 1.8f, 1.8f, 1.8f, 2f, 2f, 0.8f, 1.7f, 2f, 0.8f}; // Code 1
    largocols = 17;

    

    

    
        // add a table to the document
        document.open();
		
		PdfPTable table = new PdfPTable(colsWidth);
		table.setWidthPercentage(100); // Code 2
		table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
		
		PdfPCell cell = null;
		cell = new PdfPCell(new Paragraph(subtitulo2, helvb));
		cell.setBorder(0);
		cell.setPaddingBottom(10);
	    cell.setColspan(largocols);
		table.addCell(cell);
		
		
		PdfPCell cel2 = null;
		cel2 = new PdfPCell(new Paragraph(subtitulo3, helvb));
		cel2.setBorder(0);
		cel2.setPaddingBottom(10);
	    cel2.setColspan(largocols);
		table.addCell(cel2);
		
		PdfPCell cel3 = null;
		cel3 = new PdfPCell(new Paragraph(subtitulo4, helvb));
		cel3.setBorder(0);
		cel3.setPaddingBottom(10);
	    cel3.setColspan(largocols);
		table.addCell(cel3);
		
		PdfPCell cel4 = null;
		cel4 = new PdfPCell(new Paragraph(subtitulo5, helvb));
		cel4.setBorder(0);
		cel4.setPaddingBottom(10);
	    cel4.setColspan(largocols);
		table.addCell(cel4);
		
		
		for (int m = 0 ; m < vec_encabezado.size(); m++){
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));	      	
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	       	cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	       	cell.setRowspan(2);
	       	/*if(m== 1){
	       		cell.setColspan(2);
	       	}*/
	       	if(m==3) {
	       		cell.setColspan(8);
	       		cell.setRowspan(1);
	       	}
	       		
	      	table.addCell(cell);
	      	
	    }	
		vec_encabezado = new Vector(); 
		//vec_encabezado.addElement("");
	    vec_encabezado.addElement("Pago por Caja");
	    vec_encabezado.addElement("Cred. Univ.");
	    vec_encabezado.addElement("Becas Gobierno");
	    vec_encabezado.addElement("Becas Privadas");
	    vec_encabezado.addElement("Becas USM");
	    vec_encabezado.addElement("CAE");
	    vec_encabezado.addElement("Gratuidad Estado");
	    vec_encabezado.addElement("Dif. por Gratuidad");
	    
		for (int m = 0 ; m < vec_encabezado.size(); m++){
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));	      	
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table.addCell(cell);
	      	
	    }	
		        
		for (int j = 0 ; j < vec_datos.size(); j++){  
				for (int d = 0 ; d < largocols; d++){
					if (d < 2 || ((Vector) vec_datos.get(j)).get(d).toString().trim().equals("") || (((Vector) vec_datos.get(j)).get(d).toString().indexOf(".") > 0)) {
						cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(d).toString(), helv7));
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					}	
					else {
						cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector) vec_datos.get(j)).get(d).toString())), helv));
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					}
		   			table.addCell(cell);      	
	    		} 
			}		 
 
		/*PdfPCell cell_b = null;
		cell_b = new PdfPCell(new Paragraph("Informaci�n en millones de pesos.", helvb));
		cell_b.setBorder(0);
		cell_b.setColspan(largocols);
		table.addCell(cell_b);*/
		
		
		document.add(table);
		
		
		document.close();


	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	session.removeAttribute("listadoPresupIngreso");
}catch(DocumentException e){
	e.printStackTrace();
}

%>