<%@
page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%
//07/10/2016 KP - Se cambian nombres de variables y etiquetas 'unidad(es)' por 'organizacion(es)'
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 25, 20);/*.rotate() es para dejar la hoja horizontal*/
int rutUsuario = session.getAttribute("rutUsuario") != null?
        Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
/*int unidad   = request.getParameter("unidad") != null?
	              Integer.parseInt(request.getParameter("unidad")):0;*/
String organizacion  = request.getParameter("organizacion") != null &&
	             !request.getParameter("organizacion").trim().equals("")?
	             request.getParameter("organizacion"):"";		              
int anno     = request.getParameter("anno") != null &&
                !request.getParameter("anno").trim().equals("")?
                Integer.parseInt(request.getParameter("anno")):0;
String funcionario  = request.getParameter("funcionario") != null &&
                !request.getParameter("funcionario").trim().equals("")?
                request.getParameter("funcionario"):"";
                               
try{
    Vector vec_datos = new Vector();
//    Vector vec_vacantes = new Vector();
    
    String nomUnidad = "";
    String observacion = "";
    vec_datos = request.getSession().getAttribute("listaNominaPDF") != null?
    		    (Vector) request.getSession().getAttribute("listaNominaPDF"): new Vector();
//    vec_vacantes = request.getSession().getAttribute("listaVacantePDF") != null?
//    	    		    (Vector) request.getSession().getAttribute("listaVacantePDF"): new Vector();
    boolean muestra = true;
    if (vec_datos.size() > 0)	{ 	
    	if(vec_datos.get(1).toString().trim().equals("11.111.111 - 1"))	
    			muestra = false;
    	//nomUnidad = vec_datos.get(vec_datos.size()-2).toString();
    	nomUnidad = vec_datos.get(vec_datos.size()-2).toString();
    	observacion = vec_datos.get(vec_datos.size()-1).toString();
    }
	
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 

	document.addTitle("PRESUPUESTACI�N VALIDACI�N DE N�MINA " + anno);
	document.addAuthor(funcionario.trim());
	document.addSubject(nomUnidad);	
	
	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(
                new Phrase("P�gina ", new Font(bf_helv)), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);

    HeaderFooter header = new HeaderFooter(
                new Phrase("PRESUPUESTACI�N VALIDACI�N DE N�MINA " + anno, new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    document.open();
	
    // datos
    Vector vec_encabezado = new Vector();    
    vec_encabezado.addElement("Planta");
    vec_encabezado.addElement("RUT");
    vec_encabezado.addElement("Nombre");  
    vec_encabezado.addElement("Validaci�n");
    vec_encabezado.addElement("Observaciones");



    
 // add a table to the document
 	float[] widths = {0.05f, 0.05f, 0.1f, 0.035f, 0.1f}; /*largo de las columnas, en este caso son tres*/
	PdfPTable table = new PdfPTable(widths);
    table.setWidthPercentage(100);
    if(muestra)
    	table.setHeaderRows(2);/* cantidad de filas cabeceras para que se repitan en cada hoja*/
    
    PdfPCell cell = null;
    
   // cell = new PdfPCell(new Paragraph("UNIDAD: " + nomUnidad, new Font(bf_helvb)));
    cell = new PdfPCell(new Paragraph("ORGANIZACI�N: " + nomUnidad, new Font(bf_helvb)));
    cell.setBorder(0);
    cell.setPaddingBottom(10);
    cell.setColspan(5);
    table.addCell(cell);
    
    if(muestra){
	//Cabecera de la tabla    
    for (int m = 0 ; m < vec_encabezado.size(); m++){
      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), new Font(bf_helvb)));
      	cell.setBackgroundColor(Color.LIGHT_GRAY);
      	table.addCell(cell);
    }

    //Contenido de la tabla
    for (int d = 0 ; d < vec_datos.size(); d++){      
    	cell = new PdfPCell(new Paragraph(vec_datos.get(d).toString(), new Font(bf_helv)));
      	table.addCell(cell);      	
    }    
    }
    
    
    document.add(table);
   
    
 // Comentario final
    document.add(new Paragraph(""));
    document.add(new Paragraph("Observaci�n y comentario general:", new Font(bf_helvb)));
    document.add(new Paragraph(observacion, new Font(bf_helv)));
    document.add(new Paragraph(""));
    
   //document.close();
	
 //   vec_encabezado = new Vector();    
 //   vec_encabezado.addElement("Cargo");
 //   vec_encabezado.addElement("N� de puestos");
 //   vec_encabezado.addElement("Motivo"); 


   
 // add a table to the document
// 	float[] widths2 = {0.05f, 0.05f, 0.1f}; /*largo de las columnas, en este caso son tres*/
//	PdfPTable table2 = new PdfPTable(widths2);
//    table2.setWidthPercentage(100);
 //   table2.setHeaderRows(2);/* cantidad de filas cabeceras para que se repitan en cada hoja*/
    
 //   PdfPCell cell2 = null;
    
  //Inicio - PARA LA FILA EN BLANCO, se agregan 3 filas
 //   cell2 = new PdfPCell(new Paragraph("")); 
 //   cell2.setColspan(5); 
 //   cell2.setBorder(0); 
 //   table2.addCell(cell2); 
 //   table2.addCell(cell2);
 //   table2.addCell(cell2); 
    //Fin - PARA LA FILA EN BLANCO
    
//    cell2 = new PdfPCell(new Paragraph("Vacantes Solicitadas", new Font(bf_helvb)));
//    cell2.setBorder(0);
//    cell2.setPaddingBottom(10);
//    cell2.setColspan(5);
//    table2.addCell(cell2);
    
    
	//Cabecera de la tabla    
//    for (int m = 0 ; m < vec_encabezado.size(); m++){
//      	cell2 = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), new Font(bf_helvb)));
//      	cell2.setBackgroundColor(Color.LIGHT_GRAY);
//      	table2.addCell(cell2);
//    }

    //Contenido de la tabla
//    for (int d = 0 ; d < vec_vacantes.size(); d++){      
//    	cell2 = new PdfPCell(new Paragraph(vec_vacantes.get(d).toString(), new Font(bf_helv)));
//      	table2.addCell(cell2);      	
//    }    
   
//    document.add(table2); 
    
    document.close();
	

	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	session.removeAttribute("listaNominaPDF");
	session.removeAttribute("listaVacantePDF");
}catch(DocumentException e){
	e.printStackTrace();
}

%>