<%@
page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%
// Creado por           : M�nica Barrera F.
// Fecha                : 26/01/2015
// �ltima Actualizaci�n :

response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 25, 20);/*.rotate() es para dejar la hoja horizontal*/
int rutUsuario = session.getAttribute("rutUsuario") != null?
                 Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
if (rutUsuario > 0) {
	String nomUnidad = request.getParameter("nomUnidad") != null &&
	                 !request.getParameter("nomUnidad").trim().equals("")?
	                  request.getParameter("nomUnidad"):"";   
	int anno = request.getParameter("anno") != null &&
	                   !request.getParameter("anno").trim().equals("")?
	                    Integer.parseInt(request.getParameter("anno")):0; 
	int mesInicio = request.getParameter("mesInicio") != null &&
	                    !request.getParameter("mesInicio").trim().equals("")?
	                     Integer.parseInt(request.getParameter("mesInicio")):0; 
	int mesTermino = request.getParameter("mesTermino") != null &&
	                     !request.getParameter("mesTermino").trim().equals("")?
	                      Integer.parseInt(request.getParameter("mesTermino")):0;                    
	   
try{
    Vector vec_datos = new Vector();
    vec_datos = request.getSession().getAttribute("informeMovEjecucionExcel") != null?
    		    (Vector) request.getSession().getAttribute("informeMovEjecucionExcel"): new Vector();
    String nomMesInicio = "";
    String nomMesTermino = "";
    switch(mesInicio){
    case 1: nomMesInicio = "ENERO";
    break;
    case 2: nomMesInicio = "FEBRERO";
    break;
    case 3: nomMesInicio = "MARZO";
    break;
    case 4: nomMesInicio = "ABRIL";
    break;
    case 5: nomMesInicio = "MAYO";
    break;
    case 6: nomMesInicio = "JUNIO";
    break;
    case 7: nomMesInicio = "JULIO";
    break;
    case 8: nomMesInicio = "AGOSTO";
    break;
    case 9: nomMesInicio = "SEPTIEMBRE";
    break;
    case 10: nomMesInicio = "OCTUBRE";
    break;
    case 11: nomMesInicio = "NOVIEMBRE";
    break;
    case 12: nomMesInicio = "DICIEMBRE";
    break;
    }
    switch(mesTermino){
    case 1: nomMesTermino = "ENERO";
    break;
    case 2: nomMesTermino = "FEBRERO";
    break;
    case 3: nomMesTermino = "MARZO";
    break;
    case 4: nomMesTermino = "ABRIL";
    break;
    case 5: nomMesTermino = "MAYO";
    break;
    case 6: nomMesTermino = "JUNIO";
    break;
    case 7: nomMesTermino = "JULIO";
    break;
    case 8: nomMesTermino = "AGOSTO";
    break;
    case 9: nomMesTermino = "SEPTIEMBRE";
    break;
    case 10: nomMesTermino = "OCTUBRE";
    break;
    case 11: nomMesTermino = "NOVIEMBRE";
    break;
    case 12: nomMesTermino = "DICIEMBRE";
    break;
    }
    
    NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);		    
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 

	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(new Phrase("P�gina ", helv), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);

    String textoOpcion = "";
    textoOpcion = "LISTADO DE MOVIMIENTOS PRESUPUESTARIOS " ;
      
    HeaderFooter header = new HeaderFooter(new Phrase(textoOpcion, new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    //document.open();
	
    // datos
    Vector vec_encabezado = new Vector();   
    float[] colsWidth = null;
    int largocols = 0;
    vec_encabezado.addElement("TIPO DE DOCUMENTO");
    vec_encabezado.addElement("NUMERO DOCUMENTO");
    vec_encabezado.addElement("FECHA");
    vec_encabezado.addElement("CONTRA CUENTA");
    vec_encabezado.addElement("ITEM"); 
    vec_encabezado.addElement("IDENTIFICADOR/GLOSA");
    vec_encabezado.addElement("PR/NU");
    vec_encabezado.addElement("CARGO");
    vec_encabezado.addElement("ABONO");
    vec_encabezado.addElement("DATOS ADICIONALES");
    vec_encabezado.addElement("DIGIT.");
        colsWidth = new float[] {1.5f, 0.5f, 0.5f, 0.5f, 0.5f, 1.5f, 0.5f, 0.5f, 0.5f, 1f, 0.5f}; // Code 1
        largocols = 11; 
    

    
        // add a table to the document
        document.open();
		
		PdfPTable table = new PdfPTable(colsWidth);
		table.setWidthPercentage(100); // Code 2
		table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
		
		PdfPCell cell = null;
		cell = new PdfPCell(new Paragraph(" UNIDAD: " + nomUnidad , new Font(bf_helvb)));
		cell.setBorder(0);
		cell.setPaddingBottom(10);
	    cell.setColspan(largocols);
		table.addCell(cell);
		
		 cell = null;
		cell = new PdfPCell(new Paragraph(" MESES DE " +nomMesInicio + " A " + nomMesTermino + " DE " + anno, new Font(bf_helvb)));
		cell.setBorder(0);
		cell.setPaddingBottom(10);
	    cell.setColspan(largocols);
		table.addCell(cell);
		
		for (int m = 0 ; m < vec_encabezado.size(); m++){
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table.addCell(cell);
	      	
	    }
		
			for (int j = 0 ; j < vec_datos.size(); j++){  
				for (int d = 0 ; d < largocols; d++){
					if (d != 1 && d != 7 && d != 8 || ((((Vector) vec_datos.get(j)).get(d).toString().trim().equals("")))) {
						cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(d).toString(), helv));
						cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					} else {
						cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector) vec_datos.get(j)).get(d).toString())), helv));						
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					}
					if(j == (vec_datos.size() - 1)){
						cell.setBackgroundColor(Color.LIGHT_GRAY);
					}
		   			table.addCell(cell);      	
	    		}   
				
			}
		document.add(table);	
		document.close();


	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	session.removeAttribute("informeMovEjecucionExcel");
}catch(DocumentException e){
	e.printStackTrace();
}
}
%>