<%@ page language="java" import="descad.presupuesto.*,
                                 java.util.*,
                                 java.io.*,
                                 descad.cliente.PreswBean,
                                 java.text.NumberFormat,
                                 org.apache.poi.ss.usermodel.DataFormat,
                                 org.apache.poi.poifs.filesystem.POIFSFileSystem,
                                 org.apache.poi.hssf.usermodel.*,
                                 org.apache.poi.ss.usermodel.CellStyle"
                                 contentType="application/vnd.ms-excel"%><%

  // Creado por           : M�nica Barrera Frez
  // Fecha                : 30-09-09
  // �ltima Actualizaci�n :

  int rutUsuario = session.getAttribute("rutUsuario") != null?
                    Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
    

   if(rutUsuario > 0)
    {
    //int unidad          = request.getParameter("unidad") != null?
    		              //Integer.parseInt(request.getParameter("unidad")):0;
    String organizacion = request.getParameter("organizacion") != null?
    		              request.getParameter("organizacion"):"";    		              
    int anno            = request.getParameter("anno") != null &&
                          !request.getParameter("anno").trim().equals("")?
                          Integer.parseInt(request.getParameter("anno")):0;
   // String nomUnidad    = request.getParameter("nomUnidad") != null &&
   //                       !request.getParameter("nomUnidad").trim().equals("")?
   //                       request.getParameter("nomUnidad"):""; 
    String nomOrganizacion = request.getParameter("nomOrganizacion") != null &&
                             !request.getParameter("nomOrganizacion").trim().equals("")?
                             request.getParameter("nomOrganizacion"):"";     
   
    Vector vec_datos = new Vector();
	Vector vec_datos2 = new Vector();
	Vector vec_datos3 = new Vector();
	vec_datos = request.getSession().getAttribute("listaCorrienteExcel") != null ?
			    (Vector) request.getSession().getAttribute("listaCorrienteExcel"):new Vector();
	                    			
    if (vec_datos.size() > 0 ) {
     
      String archivo = "PresupCorriente_" + anno;
      String titulo = "SOLICITUD DE PRESUPUESTACI�N A�O " + anno;
     // String subtitulo2 = "UNIDAD: " + nomUnidad;
      String subtitulo2 = "ORGANIZACI�N: " + nomOrganizacion;
      String hoja = "CORRIENTE "+anno;
      
      //fecha
      java.util.Calendar f = new java.util.GregorianCalendar();
      String diaActual = String.valueOf(f.get(java.util.Calendar.DAY_OF_MONTH));
      String mesActual = String.valueOf(f.get(java.util.Calendar.MONTH) + 1);
      String a�oActual = String.valueOf(f.get(java.util.Calendar.YEAR));
      String hora      = String.valueOf(f.get(java.util.Calendar.HOUR_OF_DAY));
      String min       = String.valueOf(f.get(java.util.Calendar.MINUTE));
      if (hora.length() == 1) hora = "0"+hora;
      if (min.length()  == 1) min  = "0"+min;

      try {
        int fila = 0;

        ServletOutputStream sos = response.getOutputStream();
        ByteArrayOutputStream OUT = new ByteArrayOutputStream();
        NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);

        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet1 = wb.createSheet(hoja);
        
        // Estilo Arial-BOLD
        HSSFFont font1 = wb.createFont();
        font1.setFontHeightInPoints((short)10);
        font1.setFontName("Arial");
        font1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        HSSFCellStyle style_bold = wb.createCellStyle();
        style_bold.setFont(font1);

        // Estilo Arial-ITALICA
        HSSFFont font2 = wb.createFont();
        font2.setFontHeightInPoints((short)10);
        font2.setFontName("Arial");
        font2.setItalic(true);
        HSSFCellStyle style_italica = wb.createCellStyle();
         style_italica.setFont(font2);

        // Estilo FECHA
        HSSFCellStyle style_fecha = wb.createCellStyle();
        style_fecha.setDataFormat(HSSFDataFormat.getBuiltinFormat("d/m/yy h:mm"));

        // Estilo BORDE BOLD
        HSSFCellStyle style_borde_bold= wb.createCellStyle();
        style_borde_bold.setBorderBottom((short)1);
        style_borde_bold.setBorderLeft((short)1);
        style_borde_bold.setBorderRight((short)1);
        style_borde_bold.setBorderTop((short)1);
        style_borde_bold.setAlignment((short)1);
        style_borde_bold.setFont(font1);


        // Estilo BORDE
        HSSFCellStyle style_borde= wb.createCellStyle();
        style_borde.setBorderBottom((short)1);
        style_borde.setBorderLeft((short)1);
        style_borde.setBorderRight((short)1);
        style_borde.setBorderTop((short)1);
        style_borde.setAlignment((short)1);

        // Estilo BORDE
        HSSFCellStyle style_borde_centro= wb.createCellStyle();
        style_borde_centro.setBorderBottom((short)1);
        style_borde_centro.setBorderLeft((short)1);
        style_borde_centro.setBorderRight((short)1);
        style_borde_centro.setBorderTop((short)1);
        style_borde_centro.setAlignment((short)2);
        
     // Estilo numerico
        DataFormat format = wb.createDataFormat();
        CellStyle style_numerico= wb.createCellStyle();
        style_numerico = wb.createCellStyle();
        style_numerico.setDataFormat(format.getFormat("##,###,###,###,##0"));        
        style_numerico.setBorderBottom((short)1);
        style_numerico.setBorderLeft((short)1);
        style_numerico.setBorderRight((short)1);
        style_numerico.setBorderTop((short)1);
        style_numerico.setAlignment((short)3);
        
     // Estilo numerico negrita
        format = wb.createDataFormat();
        CellStyle style_numerico_negrita = wb.createCellStyle();
        style_numerico_negrita = wb.createCellStyle();
        style_numerico_negrita.setDataFormat(format.getFormat("##,###,###,###,##0"));        
        style_numerico_negrita.setBorderBottom((short)1);
        style_numerico_negrita.setBorderLeft((short)1);
        style_numerico_negrita.setBorderRight((short)1);
        style_numerico_negrita.setBorderTop((short)1);
        style_numerico_negrita.setAlignment((short)3);
        style_numerico_negrita.setFont(font1);

        // T�tulo
        HSSFRow row_tit = sheet1.createRow((short)fila);
        HSSFCell cell_tit = row_tit.createCell((short)0);
        row_tit.createCell((short)0);
        cell_tit.setCellValue(titulo);
        cell_tit.setCellStyle(style_bold);

        fila++;
        // subT�tulo
        HSSFRow row_tit2 = sheet1.createRow((short)fila);
        HSSFCell cell_tit2 = row_tit2.createCell((short)0);
        row_tit2.createCell((short)0);
        cell_tit2.setCellValue(subtitulo2);
        cell_tit2.setCellStyle(style_bold);

        fila++;
        HSSFRow row_tit4 = sheet1.createRow((short)fila);
        HSSFCell cell_tit4 = row_tit4.createCell((short)0);
        row_tit4.createCell((short)0);
        cell_tit4.setCellValue("Generado el " + diaActual + "/" + mesActual + "/" + a�oActual +
        " " + hora +":" + min);
        cell_tit4.setCellStyle(style_italica);

        fila = fila + 2;
      
        Vector vec_lista = new Vector();
        Vector vec_encabezado = new Vector();
      //  vec_encabezado.addElement("COD");
       // vec_encabezado.addElement("ITEM");
        vec_encabezado.addElement("");
        vec_encabezado.addElement("CUENTA");
        vec_encabezado.addElement("TOTAL ANUAL");
        vec_encabezado.addElement("ENERO");
        vec_encabezado.addElement("FEBRERO");
        vec_encabezado.addElement("MARZO");
        vec_encabezado.addElement("ABRIL");
        vec_encabezado.addElement("MAYO");
        vec_encabezado.addElement("JUNIO");
        vec_encabezado.addElement("JULIO");
        vec_encabezado.addElement("AGOSTO");
        vec_encabezado.addElement("SEPTIEMBRE");
        vec_encabezado.addElement("OCTUBRE");
        vec_encabezado.addElement("NOVIEMBRE");
        vec_encabezado.addElement("DICIEMBRE");
        vec_encabezado.addElement("OBSERVACION");
      
      	//Cabecera de la tabla
        int fila_enc = fila++;
        HSSFRow row0 = sheet1.createRow((short)fila_enc);
        for (int m = 0 ; m < vec_encabezado.size(); m++){  
          HSSFCell cell0 = row0.createCell((short)m);
          String columname = vec_encabezado.get(m)+"";
          row0.createCell((short)m);
          cell0.setCellValue(columname);
          cell0.setCellStyle(style_borde_bold);
        }

        //System.out.println(vec_datos);
        for (int i = 0; i < vec_datos.size(); i++){
          vec_lista = new Vector();
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(0));/*cod*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(1));/*item*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(2));/*total*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(3));/*enero*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(4));/*febrero*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(5));/*marzo*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(6));/*abril*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(7));/*mayo*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(8));/*junio*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(9));/*julio*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(10));/*agosto*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(11));/*sept*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(12));/*oct*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(13));/*nov*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(14));/*dic*/
          vec_lista.addElement(((Vector)vec_datos.get(i)).get(15));/*observacion*/
          
          // Contenido de la tabla
         HSSFRow row = sheet1.createRow((short)fila++);
         int columna = 0;
         
         boolean negrita = false;
         for (int v = 0 ; v < vec_lista.size(); v++){
        	columna ++;        	     
            HSSFCell cel00 = row.createCell((short)v);
            cel00.setCellStyle(style_borde);
            
            if (vec_lista.get(v).toString().trim().equals(""))
                negrita = true;
                        
            if (columna > 2 && columna < 16) {                       		
        		cel00.setCellValue(Long.parseLong(vec_lista.get(v)+""));
        		cel00.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
            	cel00.setCellStyle(style_numerico);
            	if (negrita)
                	cel00.setCellStyle(style_numerico_negrita);
             } else {
                cel00.setCellValue(vec_lista.get(v)+"");
                if (negrita)
                	 cel00.setCellStyle(style_borde_bold);
             }
            
            if (columna == 16) columna = 0;    
            
          }
        }  // fin de vec
        
     	// autosize each column
        for (int m = 0 ; m < vec_encabezado.size(); m++){	
          sheet1.autoSizeColumn(m, true);  
          if (m > 1 && m < vec_encabezado.size()-1) 
        	  sheet1.setColumnWidth((short)m,(short)(6000)); 

        }
        
        

           
        
           

               wb.write(OUT); // se escribe el xls en un ByteArrayOutputStream
        
        response.setContentLength(OUT.size());        
        response.setContentType ("application/x-download");
    	response.setHeader("Content-Disposition", "attachment;filename="+archivo+".xls;");
        

        //Se pone el fichero en la salida
        byte[] bufferExcel = OUT.toByteArray();
        sos.write(bufferExcel,0,bufferExcel.length);

        //Se muestra la salida.
        sos.flush();
        sos.close();
        OUT.close();
        if(request.getSession().getAttribute("listaCorrienteExcel") != null)
        	session.removeAttribute("listaCorrienteExcel");
    	if(request.getSession().getAttribute("listaReqResumenExcel") != null)
    		session.removeAttribute("listaReqResumenExcel");
    	if(request.getSession().getAttribute("listaReqDetResumenExcel") != null )
    		session.removeAttribute("listaReqDetResumenExcel");
    	   return;    

        }
      catch ( IOException ex ) {
        ex.printStackTrace();
      }
    }
  }
%>