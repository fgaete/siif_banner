<%@ page language="java" import = "java.io.*,java.util.*,org.apache.commons.fileupload.FileItem,org.apache.commons.fileupload.FileUploadException,org.apache.commons.fileupload.servlet.ServletFileUpload,org.apache.commons.fileupload.disk.DiskFileItemFactory,org.apache.commons.fileupload.FileItemFactory,org.apache.poi.openxml4j.opc.OPCPackage,org.apache.poi.openxml4j.opc.PackageAccess,org.apache.poi.openxml4j.exceptions.*,org.xml.sax.*,javax.xml.parsers.*,upxls.*,org.apache.xmlbeans.*"
%><%
	System.out.println("DELETE: Inicia"); 
 	int rutUsuario = session.getAttribute("rutUsuario")!= null?Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
 	
 	if (rutUsuario > 0) {
 	
	String fileName = request.getParameter("file") != null &&
	!request.getParameter("file").trim().equals("")?
	request.getParameter("file"):"";
	String annoDIR = request.getParameter("dir") != null &&
	!request.getParameter("dir").trim().equals("")?
	request.getParameter("dir"):"0";

    boolean exito = false;
    String mensaje = "";
 	String fieldName = "";
 	String contentType = "";
 	String extension = "";
	
 	String dir = "/usr/local/siga_doc/sip/DOCUMENTO/"+annoDIR+"/";

 	// Create a factory for disk-based file items
 	FileItemFactory factory = new DiskFileItemFactory();

 	// Create a new file upload handler
 	ServletFileUpload upload = new ServletFileUpload(factory);

 	// Parse the request
 	try {

 	    // A File object to represent the filename
 	    File f = new File(dir + fileName);

 	    // Make sure the file or directory exists and isn't write protected
 	    if (!f.exists()){
 	      	/*throw new IllegalArgumentException(
 	          "Delete: no such file or directory: " + fileName);*/
 	     	mensaje = "ERROR 1. No existe el archivo a borrar.";
 	    }

 	    if (!f.canWrite()) {
 	      	/*throw new IllegalArgumentException("Delete: write protected: "
 	          + fileName);*/
 	     	mensaje = "ERROR 2. Archivo protegido contra escritura.";
 	    }

 	    // If it is a directory, make sure it is empty
 	    if (f.isDirectory()) {
 	      String[] files = f.list();
 	      if (files.length > 0) {
 	      	  /*throw new IllegalArgumentException(
 	            "Delete: directory not empty: " + fileName);*/
 	       	  mensaje = "ERROR 3. El directorio est� vac�o.";
 	      }
 	    }

 	    // Attempt to delete it
 	    boolean success = f.delete();

 	    if (!success) {
 	      /*throw new IllegalArgumentException("Delete: deletion failed");*/
 	      mensaje = "ERROR 4. Existen problemas al borrar el archivo.";
 	    }
 	    else {
 	    	mensaje = "El archivo *" + fileName + "* ha sido eliminado.";
			exito = true;
 	    }


 	} catch (Exception ex) {
 		System.out.println("DELETE: error " + ex.toString());
 		mensaje = "ERROR. Existen problemas al borrar el archivo.";
 	}

 	
 	System.out.println("DELETE: Fin");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="cache-control" VALUE="no-cache, no-store, must-revalidate">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Pragma" content="no-cache">
<meta name="keywords" content="" />
<meta name="description" content="" />

<title>SIIF - Sistema Integrado de Informaci&oacute;n Financiera</title>
<script>
function CargaLista(){
	opener.parent.cargarListaArch('<%=annoDIR%>');
}
</script>
<style>
* {
	padding: 0px;
	margin: 0px;
}

body {
	font-size: 13px;
	font-family: "trebuchet ms", helvetica, sans-serif;
	color: #8C8C73;
	line-height: 18px;
}
#outer {
	position: relative;
	width: 99%;
	margin: 0 auto;
	border: solid 1px #E4E4E4;
	background-color: #ffffff;
}
#inner {
	position: relative;
	padding: 13px 30px 13px 30px;
	z-index: 3;
}
#footer {
	position: relative;
	clear: both;
	height: 35px;
	text-align: center;
	line-height: 35px;
	color: #A8A88D;
}
.buttonGris {
	text-align: center;
	COLOR: #000; text-shadow: 0 1px 0 #3e5a88;
	PADDING: 3px;
	MARGIN: 0px; 
}
</style> 
</head>
<body <%if (exito){%>onload="CargaLista()"<%}%>>
<div id="outer"><!--outer-->                               		
	<div id="inner"><!--inner--> 	

 			<div align="center">
 			<h3>Servicio de Eliminaci&oacute;n de Archivos</h3>
 			</div>
			<div style="text-align:center; margin:10px; padding:10px; border:dotted 1px #CCCCCC;">
				<font color='#990000'><%=mensaje%></font>	
			</div>	
			<center>
			<INPUT TYPE="BUTTON" NAME="Cerrar" VALUE="Cerrar" class="buttonGris" onClick="window.close();">
			</center>
			<br>
		<div id="footer"><!--footer-->
		&copy; Universidad T&eacute;cnica Federico Santa Mar&iacute;a
		</div><!--footer-->
    </div><!--inner--> 
</div><!--outer--> 
</body>
</html>
<%}%>       