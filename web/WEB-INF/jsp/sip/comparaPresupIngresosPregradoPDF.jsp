<%@
page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%
// Creado por           : M�nica Barrera F.
// Fecha                : 02/05/2011
// �ltima Actualizaci�n :
	
	
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 25, 20);/*.rotate() es para dejar la hoja horizontal*/
int rutUsuario = session.getAttribute("rutUsuario") != null?
                 Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
                        
Vector vec_datos = new Vector();
vec_datos = request.getSession().getAttribute("listaIngresosPregradoAnt") != null ?
            (Vector) request.getSession().getAttribute("listaIngresosPregradoAnt"):new Vector();
Vector vec_datos2 = new Vector();
            vec_datos2 = request.getSession().getAttribute("listaComparaPresupIngresosPregrado") != null ?
(Vector) request.getSession().getAttribute("listaComparaPresupIngresosPregrado"):new Vector();
            Vector vec_datos3 = new Vector();
vec_datos3 = request.getSession().getAttribute("listaResultadoPresupIngresosPregrado") != null ?
             (Vector) request.getSession().getAttribute("listaResultadoPresupIngresosPregrado"):new Vector();
String tipo = request.getParameter("tipo")	;
int anno = request.getParameter("anno") != null &&
!request.getParameter("anno").trim().equals("")?
	Integer.parseInt(request.getParameter("anno")):0;
int expresado = request.getParameter("expresado") != null &&
	!request.getParameter("expresado").trim().equals("")?
		Integer.parseInt(request.getParameter("expresado")):0;

try{
  
    
    NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);		    
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 

	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(new Phrase("P�gina ", helv), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);
    String subtitulo2 = "" ; 
    String subtitulo3 = "" ;
    String subtitulo4 = "" ;
    String subtitulo5 = "" ;
    String textoOpcion = "";
	subtitulo2 = "Ingresos de Pregrado" ;
	String nomSede = request.getParameter("nomSede") != null &&
			!request.getParameter("nomSede").trim().equals("")?
 			request.getParameter("nomSede"):"";
	String nomDepartamento = request.getParameter("nomDepartamento") != null &&
			!request.getParameter("nomDepartamento").trim().equals("")?
 			request.getParameter("nomDepartamento"):"";
	String nomCarrera = request.getParameter("nomCarrera") != null &&
			!request.getParameter("nomCarrera").trim().equals("")?
 			request.getParameter("nomCarrera"):"";
	String nomMencion = request.getParameter("nomMencion") != null &&
			!request.getParameter("nomMencion").trim().equals("")?
 			request.getParameter("nomMencion"):"";
 	String nomSedeAnt = request.getParameter("nomSedeAnt") != null &&
			!request.getParameter("nomSedeAnt").trim().equals("")?
 			request.getParameter("nomSedeAnt"):"";
	String nomDepartamentoAnt = request.getParameter("nomDepartamentoAnt") != null &&
			!request.getParameter("nomDepartamentoAnt").trim().equals("")?
 			request.getParameter("nomDepartamentoAnt"):"";
	String nomCarreraAnt = request.getParameter("nomCarreraAnt") != null &&
			!request.getParameter("nomCarreraAnt").trim().equals("")?
 			request.getParameter("nomCarreraAnt"):"";
	String nomMencionAnt = request.getParameter("nomMencionAnt") != null &&
			!request.getParameter("nomMencionAnt").trim().equals("")?
 			request.getParameter("nomMencionAnt"):"";
  	subtitulo3 = "Sede " + nomSedeAnt.trim() + ", Departamento " + nomDepartamentoAnt.trim() + ", Carrera " + nomCarreraAnt.trim() +", Menci�n " + nomMencionAnt.trim();

	subtitulo4 = "Sede " + nomSede.trim() + ", Departamento " + nomDepartamento.trim() + ", Carrera " + nomCarrera.trim() +", Menci�n " + nomMencion.trim();

	textoOpcion = "Comparaci�n de Ingresos de Pregrado " + anno;
	subtitulo5 = (expresado == 1)?"Expresado en Pesos":"Expresado en Porcentaje";
      
    HeaderFooter header = new HeaderFooter(new Phrase(textoOpcion, new Font(bf_helvb)), false);
    header.setAlignment(Element.ALIGN_CENTER);
    document.setHeader(header);  
    
    //document.open();
	
    // datos
    Vector vec_encabezado = new Vector();   
    float[] colsWidth = null;
    int largocols = 0;
  
    vec_encabezado.addElement("Financiamiento del Arancel"); 
    for (int i = (anno - 6); i <= anno; i++){
       		vec_encabezado.addElement(i+"");/*A�O*/ 
    } 
    colsWidth = new float[] {2f,1f, 1f, 1f, 1f, 1f, 1f, 1f}; // Code 1
   largocols = 8;
   

  
    
    
        // add a table to the document
        document.open();
		
		PdfPTable table = new PdfPTable(colsWidth);
		table.setWidthPercentage(100); // Code 2
		table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
		
		PdfPCell cell = null;
		cell = new PdfPCell(new Paragraph(subtitulo2, new Font(bf_helvb)));
		cell.setBorder(0);
		cell.setPaddingBottom(10);
	    cell.setColspan(largocols);
		table.addCell(cell);	
		
		PdfPCell cell2 = null;
		cell2 = new PdfPCell(new Paragraph(subtitulo3, new Font(bf_helvb)));
		cell2.setBorder(0);
		cell2.setPaddingBottom(10);
	    cell2.setColspan(largocols);
		table.addCell(cell2);
		
		PdfPCell cell6 = null;
		cell6 = new PdfPCell(new Paragraph(subtitulo5, new Font(bf_helvb)));
		cell6.setBorder(0);
		cell6.setPaddingBottom(10);
	    cell6.setColspan(largocols);
		table.addCell(cell6);
		
		
		for (int m = 0 ; m < vec_encabezado.size(); m++){
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table.addCell(cell);
	      	
	    }		
		 
		
		for (int j = 0 ; j < vec_datos.size(); j++){
					
						for (int d = 0 ; d < largocols; d++){	
							if(d==0 || (((Vector) vec_datos.get(j)).get(d).toString()).indexOf(".") >= 0){
								  	cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(d).toString(), helv));	
							  		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							  		if((((Vector) vec_datos.get(j)).get(d).toString()).indexOf(".") >= 0)
							  			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							  		table.addCell(cell); 
							} else {
								if(d>0){
								cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector) vec_datos.get(j)).get(d).toString())), helv));
								cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
								table.addCell(cell);
								}
								 
							}
					   	     	
			    		} 
					
		}
		PdfPCell cell3 = null;
		cell3 = new PdfPCell(new Paragraph("Comparar con:", new Font(bf_helvb)));
		cell3.setBorder(0);
		cell3.setPaddingBottom(10);
	    cell3.setColspan(largocols);
		table.addCell(cell3);
		
		PdfPCell cell4 = null;
		cell4 = new PdfPCell(new Paragraph(subtitulo4, new Font(bf_helvb)));
		cell4.setBorder(0);
		cell4.setPaddingBottom(10);
	    cell4.setColspan(largocols);
		table.addCell(cell4);
		
		
		for (int m = 0 ; m < vec_encabezado.size(); m++){
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table.addCell(cell);
	      	
	    }	
		
		for (int j = 0 ; j < vec_datos2.size(); j++){
			
			for (int d = 0 ; d < largocols; d++){	
				if(d==0 || (((Vector) vec_datos2.get(j)).get(d).toString()).indexOf(".") >= 0){
					  	cell = new PdfPCell(new Paragraph(((Vector) vec_datos2.get(j)).get(d).toString(), helv));	
				  		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				  		if((((Vector) vec_datos2.get(j)).get(d).toString()).indexOf(".") >= 0)
				  			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				  		table.addCell(cell); 
				} else {
					if(d>0){
					cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector) vec_datos2.get(j)).get(d).toString())), helv));
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table.addCell(cell);
					}
					 
				}
		   	     	
    		} 
		
		}
		if(expresado == 1){
		PdfPCell cell5 = null;
		cell5 = new PdfPCell(new Paragraph("Resultado:", new Font(bf_helvb)));
		cell5.setBorder(0);
		cell5.setPaddingBottom(10);
	    cell5.setColspan(largocols);
		table.addCell(cell5);
		
		vec_encabezado = new Vector();
	    vec_encabezado.addElement("% de Participaci�n"); 
	    for (int i = (anno - 6); i <= anno; i++){
	         vec_encabezado.addElement(i+"");/*A�O*/ 
	    } 
		
		for (int m = 0 ; m < vec_encabezado.size(); m++){
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table.addCell(cell);
	      	
	    }	
		for (int j = 0 ; j < vec_datos3.size(); j++){
			
			for (int d = 0 ; d < largocols; d++){	
				if(d==0 || (((Vector) vec_datos3.get(j)).get(d).toString()).indexOf(".") >= 0){
					  	cell = new PdfPCell(new Paragraph(((Vector) vec_datos3.get(j)).get(d).toString()+"%", helv));	
				  		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				  		if((((Vector) vec_datos3.get(j)).get(d).toString()).indexOf(".") >= 0)
				  			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				  		table.addCell(cell); 
				} else {
					if(d>0){
					cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(((Vector) vec_datos3.get(j)).get(d).toString())), helv));
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table.addCell(cell);
					}
					 
				}
		   	     	
    		} 
		
		}
		
		}
		document.add(table);
		
		PdfPTable table3 = new PdfPTable(colsWidth);
		table3.setWidthPercentage(100); // Code 2
		table3.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
		
		/*PdfPCell cell_b = null;
		cell_b = new PdfPCell(new Paragraph("Informaci�n en millones de pesos.", helvb));
		cell_b.setBorder(0);
		cell_b.setColspan(largocols);
		table3.addCell(cell_b);*/
		
		document.add(table3);
		
		
		document.close();


	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	session.removeAttribute("listadoPresupSerie");
}catch(DocumentException e){
	e.printStackTrace();
}

%>