<%@ page language="java" import="descad.presupuesto.*,
                                 java.util.*,
                                 java.io.*,
                                 descad.cliente.PreswBean,
                                 java.text.NumberFormat,
                                 org.apache.poi.ss.usermodel.DataFormat,
                                 org.apache.poi.poifs.filesystem.POIFSFileSystem,
                                 org.apache.poi.hssf.usermodel.*,
                                 org.apache.poi.ss.usermodel.CellStyle"
                                 contentType="application/vnd.ms-excel"%><%

  // Creado por           : M�nica Barrera F.
  // Fecha                : 02/05/2011
  // �ltima Actualizaci�n :

  int rutUsuario = session.getAttribute("rutUsuario") != null?
                    Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
   
  if(rutUsuario > 0) {
    
    Vector vec_datos = new Vector();
    vec_datos = request.getSession().getAttribute("listadoPresupSerie") != null ?
    		   (Vector) request.getSession().getAttribute("listadoPresupSerie"):new Vector();
    Vector vec_sede = request.getSession().getAttribute("listaSede") != null ?
    	    		   (Vector) request.getSession().getAttribute("listaSede"):new Vector();
    String tipo = request.getParameter("tipo")	;	   
    		   
    NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);
	   
    if (vec_datos.size() > 0 ) {
      
      String archivo = "SerieDeTiempo";
      String titulo = "";
      String subtitulo2 = "Aporte Fiscal Directo (AFD)" ; 
      if(tipo.trim().equals("AFI"))
    	  subtitulo2 = "Aporte Fiscal Indirecto (AFI)" ; 
      if(tipo.trim().equals("FSC"))
    	  subtitulo2 = "Fondo Solidario de Cr�dito Universitario" ; 
      if(tipo.trim().equals("FGB"))
    	  subtitulo2 = "Fondos Gubernamentales para Becas de Arancel" ;
      titulo = "Series de Tiempo e Informaci�n Estad�stica ";
          
         //fecha
      java.util.Calendar f = new java.util.GregorianCalendar();
      String diaActual = String.valueOf(f.get(java.util.Calendar.DAY_OF_MONTH));
      String mesActual = String.valueOf(f.get(java.util.Calendar.MONTH) + 1);
      String a�oActual = String.valueOf(f.get(java.util.Calendar.YEAR));
      String hora      = String.valueOf(f.get(java.util.Calendar.HOUR_OF_DAY));
      String min       = String.valueOf(f.get(java.util.Calendar.MINUTE));
      if (hora.length() == 1) hora = "0"+hora;
      if (min.length()  == 1) min  = "0"+min;

      try {
        int fila = 0;

        ServletOutputStream sos = response.getOutputStream();
        ByteArrayOutputStream OUT = new ByteArrayOutputStream();

        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet1 = wb.createSheet(titulo);

        // Estilo Arial-BOLD
        HSSFFont font1 = wb.createFont();
        font1.setFontHeightInPoints((short)8);
        font1.setFontName("Arial");
        font1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        HSSFCellStyle style_bold = wb.createCellStyle();
        style_bold.setFont(font1);

        // Estilo Arial-ITALICA
        HSSFFont font2 = wb.createFont();
        font2.setFontHeightInPoints((short)8);
        font2.setFontName("Arial");
        font2.setItalic(true);
        HSSFCellStyle style_italica = wb.createCellStyle();
        style_italica.setFont(font2);

        // Estilo FECHA
        HSSFCellStyle style_fecha = wb.createCellStyle();
        style_fecha.setDataFormat(HSSFDataFormat.getBuiltinFormat("d/m/yy h:mm"));

        // Estilo BORDE BOLD
        HSSFCellStyle style_borde_bold= wb.createCellStyle();
        style_borde_bold.setBorderBottom((short)1);
        style_borde_bold.setBorderLeft((short)1);
        style_borde_bold.setBorderRight((short)1);
        style_borde_bold.setBorderTop((short)1);
        style_borde_bold.setAlignment((short)2);
        style_borde_bold.setFont(font1);

        // Estilo BORDE
        HSSFCellStyle style_borde= wb.createCellStyle();
        style_borde.setBorderBottom((short)1);
        style_borde.setBorderLeft((short)1);
        style_borde.setBorderRight((short)1);
        style_borde.setBorderTop((short)1);
        style_borde.setAlignment((short)1);

        // Estilo BORDE
        HSSFCellStyle style_borde_centro= wb.createCellStyle();
        style_borde_centro.setBorderBottom((short)1);
        style_borde_centro.setBorderLeft((short)1);
        style_borde_centro.setBorderRight((short)1);
        style_borde_centro.setBorderTop((short)1);
        style_borde_centro.setAlignment((short)2);

     	// Estilo numerico
        DataFormat format = wb.createDataFormat();
        CellStyle style_numerico= wb.createCellStyle();
        style_numerico = wb.createCellStyle();
        style_numerico.setDataFormat(format.getFormat("##,###,###,###,##0"));        
        style_numerico.setBorderBottom((short)1);
        style_numerico.setBorderLeft((short)1);
        style_numerico.setBorderRight((short)1);
        style_numerico.setBorderTop((short)1);
        style_numerico.setAlignment((short)3);
        
        
        // T�tulo
        HSSFRow row_tit = sheet1.createRow((short)fila);
        HSSFCell cell_tit = row_tit.createCell((short)0);
        row_tit.createCell((short)0);
        cell_tit.setCellValue(titulo);
        cell_tit.setCellStyle(style_bold);

        fila++;
        // subT�tulo
        HSSFRow row_tit2 = sheet1.createRow((short)fila);
        HSSFCell cell_tit2 = row_tit2.createCell((short)0);
        row_tit2.createCell((short)0);
        cell_tit2.setCellValue(subtitulo2);
        cell_tit2.setCellStyle(style_bold);
     
 
        
        fila++;
        HSSFRow row_tit4 = sheet1.createRow((short)fila);
        HSSFCell cell_tit4 = row_tit4.createCell((short)0);
        row_tit4.createCell((short)0);
        cell_tit4.setCellValue("Generado el " + diaActual + "/" + mesActual + "/" + a�oActual +
        " " + hora +":" + min);
        cell_tit4.setCellStyle(style_italica);

        fila = fila + 2;

        Vector vec_lista = new Vector();
        Vector vec_encabezado = new Vector();
        if(tipo.trim().equals("AFD")){
            vec_encabezado.addElement("A�O");	     
	        vec_encabezado.addElement("95% de Fondo");
	        vec_encabezado.addElement("5% de Fondo");
	        vec_encabezado.addElement("Total USM");
	        vec_encabezado.addElement("Total Fondo Nacional");
	        vec_encabezado.addElement("Participaci�n USM (%)");
        }
         if(tipo.trim().equals("FSC") ){
        	    vec_encabezado.addElement("A�O");	     
        		 vec_encabezado.addElement("TOTAL USM");
            	 vec_encabezado.addElement("Total Fondo Nacional");
            	 vec_encabezado.addElement("Participacion USM (%)");
         }
         if( tipo.trim().equals("FGB")){
     	    vec_encabezado.addElement("A�O");	     
     		 vec_encabezado.addElement("TOTAL USM");
        }
         if(tipo.trim().equals("AFI")){
        	 vec_encabezado.addElement("A�O");
        	for (int i = 0 ; i < vec_sede.size(); i++){           	     
        	  	  
     	    vec_encabezado.addElement(((Vector)vec_sede.get(i)).get(1)+"");	     
         	}
        	 vec_encabezado.addElement("TOTAL USM");
        	 vec_encabezado.addElement("Total Fondo Nacional");
        	 vec_encabezado.addElement("Participacion USM (%)");
      }
	    
	        
	   
        //Cabezera de la tabla
        int fila_enc = fila++;
        HSSFRow row0 = sheet1.createRow((short)fila_enc);
        for (int m = 0 ; m < vec_encabezado.size(); m++){
          
          HSSFCell cell0 = row0.createCell((short)m);
          String columname = vec_encabezado.get(m)+"";
          row0.createCell((short)m);
          cell0.setCellValue(columname);
          cell0.setCellStyle(style_borde_bold);
        }


         for (int i = 0; i < vec_datos.size(); i++){  
           vec_lista = new Vector();
           if(tipo.trim().equals("AFI")){
        	   long total = 0;
        	   if((((Vector)vec_datos.get(i)).get(0)+"").trim().equals(a�oActual.trim()))
       			vec_lista.addElement(((Vector)vec_datos.get(i)).get(0)+"*");/*A�O*/
       			else
        	   vec_lista.addElement(((Vector)vec_datos.get(i)).get(0)+"");/*A�O*/ 
        		for (int j = 3 ; j < (vec_sede.size()+3); j++){           	     
        			 vec_lista.addElement(((Vector)vec_datos.get(i)).get(j)+"");/*sedes*/ 
        			 total = total + Long.parseLong(((Vector)vec_datos.get(i)).get(j)+"");
        		}
        		 vec_lista.addElement(String.valueOf(total));/*total*/ 
        		  vec_lista.addElement(((Vector)vec_datos.get(i)).get(1)+"");/*total fondo*/ 
        		  vec_lista.addElement(((Vector)vec_datos.get(i)).get(2)+"");/*participacion*/ 
              	
           }else {
        		if(/*tipo.trim().equals("AFD") && */(((Vector)vec_datos.get(i)).get(0)+"").trim().equals(a�oActual.trim()))
        			vec_lista.addElement(((Vector)vec_datos.get(i)).get(0)+"*");/*A�O*/
        			else
       	    		vec_lista.addElement(((Vector)vec_datos.get(i)).get(0)+"");/*A�O*/
           	vec_lista.addElement(((Vector)vec_datos.get(i)).get(1)+"");/*95%*/
           	if(tipo.trim().equals("FSC")) {
              	vec_lista.addElement(((Vector)vec_datos.get(i)).get(2)+"");/*5%*/
              	vec_lista.addElement(((Vector)vec_datos.get(i)).get(3)+"");/*TOTAL*/
           	}
           	if(tipo.trim().equals("AFD")) {
          	vec_lista.addElement(((Vector)vec_datos.get(i)).get(2)+"");/*5%*/
          	vec_lista.addElement(((Vector)vec_datos.get(i)).get(3)+"");/*TOTAL*/
        	vec_lista.addElement(((Vector)vec_datos.get(i)).get(4)+"");/*TOTAL*/
        	vec_lista.addElement(((Vector)vec_datos.get(i)).get(5)+"");/*TOTAL*/
           	}
           }        
          	// Contenido de la tabla
         	HSSFRow row = sheet1.createRow((short)fila++);     
          
         	for (int v = 0 ; v < vec_lista.size(); v++){  
                  	HSSFCell cel00 = row.createCell((short)v);
	            	cel00.setCellStyle(style_borde);  
	            	
	            	if(v > 0 && !(vec_lista.get(v)+"").trim().equals("") && ((vec_lista.get(v)+"").indexOf(".") < 0)){
	            	cel00.setCellValue(Long.parseLong(vec_lista.get(v)+""));
            	   	cel00.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
                	cel00.setCellStyle(style_numerico);
	            	}else {
	            		cel00.setCellValue(vec_lista.get(v)+"");
	            	}
          	}
        }

        	
            	
    	// autosize each column
        for (int m = 0 ; m < vec_encabezado.size(); m++){	
          	sheet1.autoSizeColumn(m, true);  
          if (m > 0 )
        	  if(m > 2)
        		  sheet1.setColumnWidth((short)m,(short)(3000)); 
        	  else
        	      sheet1.setColumnWidth((short)m,(short)(6500));

        }
    	
        /*if(tipo.trim().equals("AFD")){*/
        	fila++;
        	HSSFRow row_a = sheet1.createRow((short) fila++); 
        	HSSFCell cel_a = row_a.createCell((short)0);
        	//cel00.setCellStyle(style_borde); 
        	cel_a.setCellValue("* Presupuesto del a�o"); 

        	HSSFRow row_b = sheet1.createRow((short)fila++); 
        	HSSFCell cel_b = row_b.createCell((short)0);
        	//cel00.setCellStyle(style_borde); 
        	cel_b.setCellValue("Informaci�n en millones de pesos"); 
    
        /*}  */  
        wb.write(OUT); // se escribe el xls en un ByteArrayOutputStream
        
        response.setContentLength(OUT.size());
        response.setContentType ("application/x-download");
    	response.setHeader("Content-Disposition", "attachment;filename="+archivo+".xls");

        //Se pone el fichero en la salida
        byte[] bufferExcel = OUT.toByteArray();
        sos.write(bufferExcel,0,bufferExcel.length);

        //Se muestra la salida.
        sos.flush();
        sos.close();
        OUT.close();
        if(request.getSession().getAttribute("listadoPresupSerie") != null)
        	session.removeAttribute("listadoPresupSerie");
    	return;    

        }
      catch ( IOException ex ) {
        ex.printStackTrace();
      }
    }
  }
%>