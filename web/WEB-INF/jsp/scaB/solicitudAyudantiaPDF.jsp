<%@page import="com.sun.org.apache.bcel.internal.generic.NEWARRAY"%>
<%@page import="java.io.*,
        java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		java.util.List,
		java.text.NumberFormat"
%><%
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER, 20, 20, 25, 20);
int rutUsuario = session.getAttribute("rutUsuario") != null?
                 Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
try{
  
    Vector vecSolicitudes = request.getSession().getAttribute("vecSolicitudes") != null?
    		    (Vector) request.getSession().getAttribute("vecSolicitudes"): new Vector();
    Vector vecTipoAyudantia = request.getSession().getAttribute("vecTipoAyudantia") != null?
    	    		       (Vector) request.getSession().getAttribute("vecTipoAyudantia"): new Vector();	
    NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);

    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    PdfWriter writer = PdfWriter.getInstance(document, buffer); 

    // various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
    BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    document.open();
   		
	PdfPTable table = new PdfPTable(6);
 	table.setWidthPercentage(100); // Code 2
	table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3

    //a�adimos texto con formato a la primera celda
	PdfPCell celda = new PdfPCell(new Paragraph("Universidad T�cnica Federico Santa Mar�a  ",new Font(bf_helvb)));
	//unimos esta celda con otras 2
	celda.setColspan(6);
	//alineamos el contenido al centro
	celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	// a�adimos un espaciado
	celda.setPadding (12.0f);
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);

	//fila 2
	celda = new PdfPCell(new Paragraph("Direcci�n General de Finanzas ",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(6);
	celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	table.addCell(celda);
	

	
	// fila 3
	celda = new PdfPCell(new Paragraph("SOLICITUD DE AYUDANTIA N�: " + vecSolicitudes.get(0), new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(6);
	celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	table.addCell(celda);
	
	// fila 4
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(6);
	table.addCell(celda);
	
	// fila 5
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(6);
	table.addCell(celda);
	
	// fila 6
	celda = new PdfPCell(new Paragraph("DATOS AYUDANTE ", new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(6);
	celda.setHorizontalAlignment(Element.ALIGN_LEFT);
	table.addCell(celda);
		
	// fila 7
	
 	celda = new PdfPCell(new Paragraph("RUT ayudante",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph (": " + vecSolicitudes.get(1)+""));
	celda.setBorder(0);
	celda.setColspan(5);
	table.addCell(celda);
	
	
    // fila 8	
	celda = new PdfPCell(new Paragraph("Nombre",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph (": " + vecSolicitudes.get(4) + " " + vecSolicitudes.get(2) + " " + vecSolicitudes.get(3)));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 9
	 
	celda = new PdfPCell(new Paragraph("Sexo",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(5)+""));
	celda.setBorder(0);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph("Estado Civil",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(6)+""));
	celda.setColspan(3);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 10
	celda = new PdfPCell(new Paragraph("Domicilio",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(7)+""));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 11
	celda = new PdfPCell(new Paragraph("Comuna",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(8)+""));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph("Ciudad",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(9)+""));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph("Regi�n",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(10)+""));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph("Banco dep�sito",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(11)+""));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph("Sucursal",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(12)+""));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph("Cuenta",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(13)+""));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	
	
	// fila 13
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(6);
	table.addCell(celda);
	
	// fila 14
	celda = new PdfPCell(new Paragraph("DATOS SOLICITUD ", new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(6);
	celda.setHorizontalAlignment(Element.ALIGN_LEFT);
	table.addCell(celda);
	
	// fila 15
	celda = new PdfPCell(new Paragraph("Sede",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(14)+" - " + vecSolicitudes.get(15)));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 16
	celda = new PdfPCell(new Paragraph("Organizaci�n",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(16)+" - " + vecSolicitudes.get(17)));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 17
 	celda = new PdfPCell(new Paragraph("Trabajo a efectuar",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(18)+""));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 18
	celda = new PdfPCell(new Paragraph("Curso Ayudante",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(19)+""));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 19
	celda = new PdfPCell(new Paragraph("Fecha Solicitud",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(20)+""));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 20
	celda = new PdfPCell(new Paragraph("Semestre",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(21)+" - " + vecSolicitudes.get(22)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph("Inicio",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(23)+""));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph("T�rmino",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(24)+""));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	
	// fila 21
	celda = new PdfPCell(new Paragraph("Per�odo efectivo",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(25)+ " - " +vecSolicitudes.get(26)));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 22
	celda = new PdfPCell(new Paragraph("Tipo ayudant�a",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(27)+" - " + vecSolicitudes.get(28)));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 23
	celda = new PdfPCell(new Paragraph("Tipo contrato",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(29)+""));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	if (!vecSolicitudes.get(29).toString().trim().equals("")) {
			
	   celda = new PdfPCell(new Paragraph(" "));
	   celda.setBorder(0);
	   celda.setPaddingBottom(10);
	   celda.setColspan(6);
	   table.addCell(celda);
		
	   celda = new PdfPCell(new Paragraph("Solicitudes docente y acad�micas ", new Font(bf_helvb)));
	   celda.setBorder(0);
  	   celda.setPaddingBottom(10);
	   celda.setColspan(6);
	   celda.setHorizontalAlignment(Element.ALIGN_LEFT);
	   table.addCell(celda);
	   
	   celda = new PdfPCell(new Paragraph("Nivel", new Font(bf_helvb)));
	   celda.setBorder(0);
	   celda.setPaddingBottom(10);
	   table.addCell(celda);
	   celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(30)+""));
	   celda.setColspan(5);
	   celda.setBorder(0);
	   table.addCell(celda);
	   
	   Vector vec_encabezado = new Vector();    
	    vec_encabezado.addElement("Profesor");
	    vec_encabezado.addElement("Asignatura");
	    vec_encabezado.addElement("Paralelo");
	    
	    float[] widths = {0.6f, 0.6f, 0.2f}; //largo de las columnas, en este caso son seis
	    PdfPTable table2 = new PdfPTable(widths); 
	    
		table2.setWidthPercentage(100);
				
		PdfPCell cell = null;
	   	    
	  //Cabecera de la tabla    
	    for (int m = 0 ; m < vec_encabezado.size(); m++){
	       	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	      	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table2.addCell(cell);
	    }
	   
	    if (vecSolicitudes.get(31).toString().trim().length() > 2) {
	    	cell = new PdfPCell(new Paragraph("Profesor asig 1: " + vecSolicitudes.get(31)+""));
	      	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	      	table2.addCell(cell);
	      	cell = new PdfPCell(new Paragraph("Asignatura 1: " +vecSolicitudes.get(32)+""));
	      	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	      	table2.addCell(cell);
	      	cell = new PdfPCell(new Paragraph("Paralelo asig. 1: " +vecSolicitudes.get(33)+""));
	      	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table2.addCell(cell);
	    }
	    
	    if (vecSolicitudes.get(34).toString().trim().length() > 2) {
	    	cell = new PdfPCell(new Paragraph("Profesor asig 2: " + vecSolicitudes.get(34)+""));
	      	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	      	table2.addCell(cell);
	      	cell = new PdfPCell(new Paragraph("Asignatura 2: " +vecSolicitudes.get(35)+""));
	      	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	      	table2.addCell(cell);
	      	cell = new PdfPCell(new Paragraph("Paralelo asig. 2: " + vecSolicitudes.get(36)+""));
	      	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table2.addCell(cell);
	    }
	    
	    if (vecSolicitudes.get(37).toString().trim().length() > 2) {
	    	cell = new PdfPCell(new Paragraph("Profesor asig 3: " +vecSolicitudes.get(37)+""));
	      	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	      	table2.addCell(cell);
	      	cell = new PdfPCell(new Paragraph("Asignatura 3: " +vecSolicitudes.get(38)+""));
	      	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	      	table2.addCell(cell);
	      	cell = new PdfPCell(new Paragraph("Paralelo asig. 3: " +vecSolicitudes.get(39)+""));
	      	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table2.addCell(cell);
	    }
	   	  
	   celda = new PdfPCell(table2);
	   celda.setColspan(6);
	   celda.setBorder(0);
	   table.addCell(celda);
	   
	   celda = new PdfPCell(new Paragraph(" "));
	   celda.setBorder(0);
	   celda.setPaddingBottom(10);
	   celda.setColspan(6);
	   table.addCell(celda);
	   
	// Lista asignaturas
	    Vector vec_encabezado3 = new Vector();    
	    vec_encabezado3.addElement("Subtipo Ayudant�a");
	    vec_encabezado3.addElement("Valor Hora $");
	    vec_encabezado3.addElement("HH Asi.1"); 
	    vec_encabezado3.addElement("HH Asi.2");
	    vec_encabezado3.addElement("HH Asi.3");
	    vec_encabezado3.addElement("Valor Total $");
	   	 // add a table to the document
	   
	   float[] widths2 = {0.6f, 0.6f, 0.3f, 0.3f, 0.3f, 0.3f}; //largo de las columnas, en este caso son seis
	   PdfPTable table3 = new PdfPTable(widths2); 
	    
	   table3.setWidthPercentage(100);
				
	   PdfPCell cell3 = null;
	   int columna = 0;
	    
	  //Cabecera de la tabla
	 if (vecTipoAyudantia != null && vecTipoAyudantia.size() > 0) {
	   for (int m = 0 ; m < vec_encabezado3.size(); m++){
	    	cell3 = new PdfPCell(new Paragraph(vec_encabezado3.get(m).toString(), helvb));
	      	cell3.setBackgroundColor(Color.LIGHT_GRAY);
	      	cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table3.addCell(cell3);
	    }

	    String tipoAyu = "";
	    Vector vecTipoAyud = new Vector();
	    Vector v = null;
	    long  total = 0;
	    int cont = 0;
	    for (int x = 0 ; x < vecTipoAyudantia.size(); x++){
	    	Vector lista = (Vector) vecTipoAyudantia.get(x);
	       	if (!tipoAyu.equals(lista.get(0)+"")) {
	    		if (!tipoAyu.equals("")) {
		    		  if (cont == 1) {v.add(0);v.add(0);}
			   	      if (cont == 2) v.add(0);
			   	      v.add(total+"");
			    	  vecTipoAyud.add(v);
			   	  }	
	    	  v = new Vector();	
	    	  v.add(lista.get(0)+"");
	    	  v.add(lista.get(1)+"");
	    	  v.add(lista.get(2)+"");
	    	  
	    	  total = (Long.parseLong(v.get(1)+"")* Long.parseLong(v.get(2)+"")) ;
	    	  cont = 1;
	    	}
	    	else {
	    	  cont++;	
	    	  v.add(lista.get(2)+"");
	       	  total = total + (Long.parseLong(v.get(1)+"")*Long.parseLong(lista.get(2)+"")); // total
	      	}
	    	tipoAyu = lista.get(0)+"";
	    }
	    
	     
	    if (vecTipoAyud != null && vecTipoAyud.size() > 0) {
			for (int x = 0 ; x < vecTipoAyud.size(); x++){
		    	Vector lista = (Vector) vecTipoAyud.get(x);
		     	columna = 0;
		    	for (int y = 0 ; y < lista.size(); y++){
			    	columna ++;
			       	cell3 = new PdfPCell(new Paragraph(lista.get(y)+""));
			      	if (columna == 1) cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
			      	else if (columna == 2 || columna == 6) cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
			      		 else cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			      	table3.addCell(cell3);
		    	}
		    }	
		}	
		}	
	   
	 celda = new PdfPCell(table3);
	 celda.setColspan(6);
	 celda.setBorder(0);
	 table.addCell(celda);  
    }   
	
	// Totales
	celda = new PdfPCell(new Paragraph("TOTAL MENSUAL",new Font(bf_helvb)));
	celda.setColspan(2);
    celda.setBorder(0);
    celda.setPaddingBottom(10);
    table.addCell(celda);
    celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(38)+""));
    celda.setColspan(4);
    celda.setBorder(0);
    table.addCell(celda);
	    
	 // Totales
	 celda = new PdfPCell(new Paragraph("TOTAL CONTRATO", new Font(bf_helvb)));
	 celda.setColspan(2);
	 celda.setBorder(0);
	 celda.setPaddingBottom(10);
	 table.addCell(celda);
	 celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(39)+""));
	 celda.setColspan(4);
	 celda.setBorder(0);
	 table.addCell(celda);

	
    document.add(table);
	    

	document.close();

	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	
	response.getOutputStream().flush();
	response.getOutputStream().close();
	
	session.removeAttribute("vecSolicitudes");
	session.removeAttribute("vecListaAsign");
	

}catch(DocumentException e){
	e.printStackTrace();
}

%>