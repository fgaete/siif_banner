<%@page import="java.io.*,
        java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		java.util.List,
		java.text.NumberFormat"
%><%
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER, 20, 20, 25, 20);
int rutUsuario = session.getAttribute("rutUsuario") != null?
                 Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
try{
  
    Vector vecSolicitudes = request.getSession().getAttribute("vecSolicitudes") != null?
    		    (Vector) request.getSession().getAttribute("vecSolicitudes"): new Vector();
    Vector vecDatos = request.getSession().getAttribute("vecDatos") != null?
    	    		    (Vector) request.getSession().getAttribute("vecDatos"): new Vector();	
    Vector vecHistorial = request.getSession().getAttribute("vecHistorial") != null?
    	    	    		    (Vector) request.getSession().getAttribute("vecHistorial"): new Vector();  
    String tipoDocumento = request.getParameter("tipoDocumento") != null &&
    						!request.getParameter("tipoDocumento").trim().equals("")?
            				request.getParameter("tipoDocumento"):"";     
            				
    long	valorIVA = 0;	 
    String	tipoServicio = "";
    String	descripcion = "";
    String	nomSede = "";
    String	rutide = "";
    String	digide = "";
    String	nombreCliente = "";
    String	direccion = "";
    String	comuna = "";
    String	ciudad = "";
    String	giro = "";
    String	referencia = "";
    String	condicion = "";
    String	unidad = "";
    String	nomUnidad = "";
    String	vencimiento = "";
    long	valorneto = 0;
    long	valorExento = 0;
    long	valorImpuesto =0;
    long	valorTotal =0;
    String	numReferencia = "";
    String	tipoReferencia = "";
    String	estado = ""; 
    String  numDoc = "";
    String numFactura = "";
    String titulo = "";
    String tipoNota = "";
    long numNota = 0;
  

   if(vecDatos.size()>0){
	    valorIVA = Long.parseLong(vecDatos.get(0)+"");	 
	   // tipoDocumento = vecDatos.get(1)+"";
	    tipoServicio = vecDatos.get(2)+"";
	    descripcion = vecDatos.get(3)+"";
	    nomSede = vecDatos.get(4)+"";
	    rutide = vecDatos.get(5)+"";
	    digide = vecDatos.get(6)+"";
	    nombreCliente = vecDatos.get(7)+"";
	    direccion = vecDatos.get(8)+"";
	    comuna = vecDatos.get(9)+"";
	    ciudad = vecDatos.get(10)+"";
	    giro = vecDatos.get(11)+"";
	    referencia = vecDatos.get(12)+"";
	    condicion = vecDatos.get(13)+"";
	    unidad = vecDatos.get(14)+"";
	    nomUnidad = vecDatos.get(15)+"";
	    vencimiento = vecDatos.get(16)+"";
	    valorneto = Long.parseLong(vecDatos.get(17)+"");
	    valorExento = Long.parseLong(vecDatos.get(18)+"");
	    valorImpuesto = Long.parseLong(vecDatos.get(19)+"");
	    valorTotal = Long.parseLong(vecDatos.get(20)+"");
	    numReferencia = vecDatos.get(21)+"";
	    tipoReferencia = vecDatos.get(22)+"";
	    estado = vecDatos.get(23)+"";
	    numDoc = vecDatos.get(24)+"";
	    numFactura  = vecDatos.get(25)+"";
	    tipoNota = vecDatos.get(26)+"";
	    numNota =  Long.parseLong(vecDatos.get(27)+""); 
   }
   if(tipoNota.trim().equals("NCE"))
	   titulo = "Nota de Cr�dito ";
   else
	   titulo = "Nota de D�bito ";
   if(numNota > 0)
       titulo = titulo + " N� " + numNota + "-" + tipoNota;
   else
	   titulo = titulo + " " + tipoNota;
   
 //  System.out.println("dentro del pdf"+valorImpuesto+"exento "+valorExento);
  	NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);

    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    PdfWriter writer = PdfWriter.getInstance(document, buffer); 

    // various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
    BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    document.open();
    
    //Ejemplos de IMAGE
 /*   Image foto = Image.getInstance("web/sgf/img/logos/logo_usm_bn70x51.jpg");
    foto.scaleToFit(100, 100);
    foto.setAlignment(Chunk.ALIGN_MIDDLE);
    document.add(foto);*/
  			
	PdfPTable table = new PdfPTable(3);
 	table.setWidthPercentage(70); // Code 2
	table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3

    //a�adimos texto con formato a la primera celda
	PdfPCell celda = new PdfPCell(new Paragraph("Universidad T�cnica Federico Santa Mar�a  ",new Font(bf_helvb)));
	//unimos esta celda con otras 2
	celda.setColspan(3);
	//alineamos el contenido al centro
	celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	// a�adimos un espaciado
	celda.setPadding (12.0f);
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);

	//fila 2
	celda = new PdfPCell(new Paragraph("Direcci�n General de Finanzas ",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	table.addCell(celda);
	
	//fila 3        
/*	
    celda = new PdfPCell(new Paragraph("Fecha:" + vec_datos.get(6)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
	table.addCell(celda);
	
	//fila 4        
	celda = new PdfPCell(new Paragraph("Comit� de Coordinaci�n y Desarrollo Docente", new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	table.addCell(celda);*/
	
	// fila 5
	celda = new PdfPCell(new Paragraph(titulo , new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	table.addCell(celda);
	

	
	// fila 6
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	table.addCell(celda);
	
	
	// fila 7
	celda = new PdfPCell(new Paragraph("Datos del Cliente ", new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	table.addCell(celda);
	
	if(!rutide.trim().equals("")){
	if(Integer.parseInt(numFactura) > 0 ){	
	celda = new PdfPCell(new Paragraph("N� de Factura "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph (numFactura+"-"+tipoDocumento));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);	
	}	
    // fila 8
 	celda = new PdfPCell(new Paragraph("RUT Cliente "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph (String.valueOf(rutide) + "-"+ digide));
	celda.setBorder(0);
	celda.setColspan(2);
	table.addCell(celda);
	}
	
    // fila 9	
	celda = new PdfPCell(new Paragraph("Nombre Cliente "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph (nombreCliente));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 10
	celda = new PdfPCell(new Paragraph("Direcci�n " ));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(direccion));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 11
	celda = new PdfPCell(new Paragraph("Comuna "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(comuna));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph("Ciudad "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(ciudad));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph("Giro "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(giro));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph("Datos  Venta ", new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph("Referencia Atenci�n "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(referencia));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph("Condici�n de Venta "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(condicion));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph("D�as Vencimiento "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(vencimiento));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph("Tipo Servicio "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(tipoServicio.trim() + " "+descripcion.trim()));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	

	
	
	// fila 12
	celda = new PdfPCell(new Paragraph("Cuenta de Ingreso "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(unidad + "-" + nomUnidad));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	if(tipoReferencia.trim().equals("O"))
		tipoReferencia = "Orden de Compra";
	else
		tipoReferencia = "HES";
	// fila 12
	celda = new PdfPCell(new Paragraph("OC � HES "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(tipoReferencia + "-" + numReferencia));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	
	
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	table.addCell(celda);
	
	
	// fila 13
	celda = new PdfPCell(new Paragraph("Bienes y Servicios "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);


	
	//document.add(table);
	
	// distribuci�n de gastos
    Vector vec_encabezado = new Vector();    
    vec_encabezado.addElement("Descripci�n");
    vec_encabezado.addElement("Valor $");
   
	
	
	 // add a table to the document
	 	float[] widths = {0.4f, 0.1f}; /*largo de las columnas, en este caso son dos*/
		PdfPTable table2 = new PdfPTable(widths);
	    table2.setWidthPercentage(100);
	    table2.setHeaderRows(1);/* cantidad de filas cabeceras para que se repitan en cada hoja*/
	    
	    PdfPCell cell = null;
	    int columna = 0;
	    
		//Cabecera de la tabla    
	    for (int m = 0 ; m < vec_encabezado.size(); m++){
	    	columna ++;
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	      	if (columna == 2) {
	      		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	      	}
	      	table2.addCell(cell);
	      	if (columna == 3) columna = 0;
	    }

	    //Contenido de la tabla
	    columna = 0;
        long total = 0;
    //    System.out.println("vecSolicitudes.size(): "+vecSolicitudes);
	    for (int d = 0 ; d < vecSolicitudes.size(); d++){   
	    	//if (d == vecSolicitudes.size()) {   	
	    		Vector vec = new Vector();
	    		vec =(Vector) vecSolicitudes.get(d);
	    		
		    	for(int col = 0;col < 2;col++){      	
		    	cell = new PdfPCell(new Paragraph(vec.get(col).toString(), helv));
		      	if (col == 1) {
		      		cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(vec.get(col).toString())),helvb));
		      		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			      	
		      	}
		      	columna ++;
		      	table2.addCell(cell);      	
		    	}
	    //	}
	    }    

	    // valor Neto
	    cell = new PdfPCell(new Paragraph("Valor Neto", helvb));
	    table2.addCell(cell);
	    
	    cell = new PdfPCell(new Paragraph(formatoNum.format(valorneto),helvb));
	    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	    table2.addCell(cell);
	    
	    // valoe IVA
	    cell = new PdfPCell(new Paragraph("I.V.A.", helvb));
	    table2.addCell(cell);
	    
	    cell = new PdfPCell(new Paragraph(formatoNum.format(valorImpuesto),helvb));
	    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	    table2.addCell(cell);
	    
	    // Totales
	    cell = new PdfPCell(new Paragraph("Valor Total", helvb));
	    table2.addCell(cell);
	    
	    cell = new PdfPCell(new Paragraph(formatoNum.format(valorTotal),helvb));
	    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	    table2.addCell(cell);


	    cell = new PdfPCell(new Paragraph(""));
	    cell.setBorder(0);
	    table2.addCell(cell); 
	
	    
		celda = new PdfPCell(table2);
		celda.setColspan(2);
		celda.setBorder(0);
		table.addCell(celda);
	    
		celda = new PdfPCell(new Paragraph(" "));
		celda.setBorder(0);
		celda.setPaddingBottom(10);
		celda.setColspan(3);
		table.addCell(celda);
		
			// historial
				
		celda = new PdfPCell(new Paragraph("Historial "));
		celda.setBorder(0);
		celda.setPaddingBottom(10);
		table.addCell(celda);
		float[] widths2 = {1f, 2f}; /*largo de las columnas, en este caso son dos*/
		table2 = new PdfPTable(widths2);
	    table2.setWidthPercentage(100);
	    

		if(vecHistorial!= null && vecHistorial.size() > 0){
			for(int i=0;i<vecHistorial.size();i++){
				Vector vec = (Vector) vecHistorial.get(i);
			 	cell = new PdfPCell(new Paragraph(vec.get(0).toString(), helv));
			 //	cell.setBorder(0);
			 	table2.addCell(cell);
		       	cell = new PdfPCell(new Paragraph(vec.get(1).toString(),helvb));
		         //	cell.setBorder(0);
			   	table2.addCell(cell);
			
			}
		}
		celda = new PdfPCell(table2);
		celda.setColspan(2);
		celda.setBorder(0);
		table.addCell(celda);
		// fin distribucion del pago
			// fin historial
			
			
	    document.add(table);
	    

	document.close();

	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	response.getOutputStream().flush();
	response.getOutputStream().close();
	
	session.removeAttribute("vecHistorial");
	session.removeAttribute("vecSolicitudes");
}catch(DocumentException e){
	e.printStackTrace();
}

%>