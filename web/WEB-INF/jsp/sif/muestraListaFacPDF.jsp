<%@
page import="java.io.*,java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		descad.presupuesto.Presw25DTO,
		descad.cliente.PreswBean,
		java.util.List,
		java.text.NumberFormat"
%><%
// Creado por           : M�nica Barrera F.
// Fecha                : 06/09/2012
// �ltima Actualizaci�n :
	
	
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 25, 20);/*.rotate() es para dejar la hoja horizontal*/
int rutUsuario = session.getAttribute("rutUsuario") != null?
                 Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
String unidad = session.getAttribute("nomUnidad") != null ?
                session.getAttribute("nomUnidad")+"":"";
                                                          
String estado = session.getAttribute("nomEstado") != null ?
           	    session.getAttribute("nomEstado")+"":"";                        
String fechaIni = request.getParameter("fechaIni") != null &&
                  !request.getParameter("fechaIni").trim().equals("")?
                  request.getParameter("fechaIni"):"";  
String fechaTer = request.getParameter("fechaTer") != null &&
                  !request.getParameter("fechaTer").trim().equals("")?
                  request.getParameter("fechaTer"):"";
                     
             	

Vector vec_datos = new Vector();
vec_datos = request.getSession().getAttribute("listadoFacturacion") != null ?
	   (Vector) request.getSession().getAttribute("listadoFacturacion"):new Vector();
try{
  
    
    NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);		    
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer); 

	// various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
	BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv7 = new Font(bf_helv);
    helv7.setSize(7);
    helv7.setColor(Color.BLACK);
    
    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    // headers and footers must be added before the document is opened
    HeaderFooter footer = new HeaderFooter(new Phrase("P�gina ", helv), true);
    footer.setBorder(Rectangle.NO_BORDER);
    footer.setAlignment(Element.ALIGN_CENTER);
    document.setFooter(footer);
    String titulo = "Consulta General de Solicitudes de Facturaci�n";
    String subtitulo2 = "Estado: " + estado  ; 
    String subtitulo3 = "Unidad: " + ((unidad.trim().equals("0"))?"Todas":unidad); 
    String subtitulo4 = (!fechaIni.trim().equals(""))?("Entre fechas: " + fechaIni + " - " + fechaTer):"";
    String subtitulo5 = "";

    String textoOpcion = "";
  
      
     
    //document.open();
	
    // datos
    Vector vec_encabezado = new Vector();   
    float[] colsWidth = null;
    int largocols = 0;
  
    vec_encabezado.addElement("N� Interno");	    
    vec_encabezado.addElement("Fecha");	   
    vec_encabezado.addElement("Cliente");	    
    vec_encabezado.addElement("Glosa");
    vec_encabezado.addElement("Valor");
    vec_encabezado.addElement("Estado Documento");
    vec_encabezado.addElement("Tipo Factura");
    vec_encabezado.addElement("Factura");
    vec_encabezado.addElement("Estado Pago");
    vec_encabezado.addElement("Saldo");
    vec_encabezado.addElement("Valor Pagado");
    vec_encabezado.addElement("Monto pagado con Cheques");
    vec_encabezado.addElement("Cheques pendiente por Depositar");
    
    colsWidth = new float[] {0.9f, 1.5f, 2f, 1.8f, 1.8f, 1.8f, 1.8f, 1.8f, 1.8f, 2f, 2f, 0.8f, 1.7f}; // Code 1
    largocols = 13;

    

    

    
        // add a table to the document
        document.open();
        PdfPTable table = new PdfPTable(colsWidth);
        
   
     
        
		PdfPCell cel0 = null;
		cel0 = new PdfPCell(new Paragraph(titulo, helvb));
		cel0.setBorder(0);
		cel0.setPaddingBottom(10);
	    cel0.setColspan(largocols);
	    cel0.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cel0);
        
	      HeaderFooter header = new HeaderFooter(new Phrase(textoOpcion, new Font(bf_helvb)), false);
	        header.setAlignment(Element.ALIGN_CENTER);
	        document.setHeader(header); 
		
		
		table.setWidthPercentage(100); // Code 2
		table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
		

		
		
		PdfPCell cell = null;
		cell = new PdfPCell(new Paragraph(subtitulo2, helvb));
		cell.setBorder(0);
		cell.setPaddingBottom(10);
	    cell.setColspan(largocols);
		table.addCell(cell);
		
		
		PdfPCell cel2 = null;
		cel2 = new PdfPCell(new Paragraph(subtitulo3, helvb));
		cel2.setBorder(0);
		cel2.setPaddingBottom(10);
	    cel2.setColspan(largocols);
		table.addCell(cel2);
		
		PdfPCell cel3 = null;
		cel3 = new PdfPCell(new Paragraph(subtitulo4, helvb));
		cel3.setBorder(0);
		cel3.setPaddingBottom(10);
	    cel3.setColspan(largocols);
		table.addCell(cel3);
		
		PdfPCell cel4 = null;
		cel4 = new PdfPCell(new Paragraph(subtitulo5, helvb));
		cel4.setBorder(0);
		cel4.setPaddingBottom(10);
	    cel4.setColspan(largocols);
		table.addCell(cel4);
		

	    
		for (int m = 0 ; m < vec_encabezado.size(); m++){
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));	      	
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	       	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table.addCell(cell);
	      	
	    }	
		        
		for (int j = 0 ; j < vec_datos.size(); j++){  
				for (int d = 0 ; d < largocols; d++){
					 cell = new PdfPCell(new Paragraph(((Vector) vec_datos.get(j)).get(d).toString(), helv7));
					  
					  if (d == 4 || d > 8)
							 cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					  else 
			               cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					
						
		   			table.addCell(cell);      	
	    		} 
			}		 
 
	
		
		
		document.add(table);
		
		
		document.close();


	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	
	response.getOutputStream().flush();
	response.getOutputStream().close();
	session.removeAttribute("listadoFacturacion");
    session.removeAttribute("nomUnidad");
    session.removeAttribute("nomEstado");
}catch(DocumentException e){
	e.printStackTrace();
}

%>