<%@page import="com.sun.org.apache.bcel.internal.generic.NEWARRAY"%>
<%@page import="java.io.*,
        java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		java.util.List,
		java.text.NumberFormat"
%><%
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER, 20, 20, 25, 20);
int rutUsuario = session.getAttribute("rutUsuario") != null?
                 Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
try{
  
    Vector vecSolicitudes = request.getSession().getAttribute("vecSolicitudes") != null?
    		    (Vector) request.getSession().getAttribute("vecSolicitudes"): new Vector();
    Vector vecListaAsign = request.getSession().getAttribute("vecListaAsign") != null?
    	    		       (Vector) request.getSession().getAttribute("vecListaAsign"): new Vector();	
    NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);

    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    PdfWriter writer = PdfWriter.getInstance(document, buffer); 

    // various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
    BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    document.open();
   		
	PdfPTable table = new PdfPTable(6);
 	table.setWidthPercentage(100); // Code 2
	table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3

    //a�adimos texto con formato a la primera celda
	PdfPCell celda = new PdfPCell(new Paragraph("Universidad T�cnica Federico Santa Mar�a  ",new Font(bf_helvb)));
	//unimos esta celda con otras 2
	celda.setColspan(6);
	//alineamos el contenido al centro
	celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	// a�adimos un espaciado
	celda.setPadding (12.0f);
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);

	//fila 2
	celda = new PdfPCell(new Paragraph("Direcci�n General de Finanzas ",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(6);
	celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	table.addCell(celda);
	

	
	// fila 3
	celda = new PdfPCell(new Paragraph("SOLICITUD DE DOCENCIA N�: " + vecSolicitudes.get(0), new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(6);
	celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	table.addCell(celda);
	
	// fila 4
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(6);
	table.addCell(celda);
	
	// fila 5
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(6);
	table.addCell(celda);
	
	// fila 6
	celda = new PdfPCell(new Paragraph("DATOS FUNCIONARIO ", new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(6);
	celda.setHorizontalAlignment(Element.ALIGN_LEFT);
	table.addCell(celda);
		
	// fila 7
	System.out.println(vecSolicitudes.size() + "-" + vecSolicitudes);
 	celda = new PdfPCell(new Paragraph("RUT funcionario",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph (": " + vecSolicitudes.get(1)+""));
	celda.setBorder(0);
	celda.setColspan(5);
	table.addCell(celda);
	
	
    // fila 8	
	celda = new PdfPCell(new Paragraph("Nombre",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph (": " + vecSolicitudes.get(4) + " " + vecSolicitudes.get(2) + " " + vecSolicitudes.get(3)));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 9
	 
	celda = new PdfPCell(new Paragraph("Sexo",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(5)+""));
	celda.setBorder(0);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph("Estado Civil",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(6)+""));
	celda.setColspan(3);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 10
	celda = new PdfPCell(new Paragraph("Domicilio",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(7)+""));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 11
	celda = new PdfPCell(new Paragraph("Comuna",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(8)+""));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph("Ciudad",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(9)+""));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph("Regi�n",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(10)+""));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph("Fecha Nacimiento",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(11)+""));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph("Nacionalidad",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(12)+""));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph("Otra",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(13)+""));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	
	// fila 13
	celda = new PdfPCell(new Paragraph("Grado Acad�mico",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(14)+""));
	celda.setColspan(5);
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	
	// fila 14
	celda = new PdfPCell(new Paragraph("Titulo",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(15)+""));
	celda.setColspan(5);
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	
	// fila 14
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(6);
	table.addCell(celda);
	
	// fila 15
	celda = new PdfPCell(new Paragraph("DATOS SOLICITUD ", new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(6);
	celda.setHorizontalAlignment(Element.ALIGN_LEFT);
	table.addCell(celda);
	
	// fila 16
	celda = new PdfPCell(new Paragraph("Planta",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(16)+" - " + vecSolicitudes.get(17)));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph("Cargo",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(18)+" - " + vecSolicitudes.get(19)));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 17
	celda = new PdfPCell(new Paragraph("Sede",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(21)+" - " + vecSolicitudes.get(22)));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 18
	celda = new PdfPCell(new Paragraph("Organizaci�n",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(23)+" - " + vecSolicitudes.get(24)));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 19
	celda = new PdfPCell(new Paragraph("Trabajo a efectuar",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(25)+""));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 20
	celda = new PdfPCell(new Paragraph("Fecha Solicitud",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(26)+""));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 21
	celda = new PdfPCell(new Paragraph("Semestre",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(27)+" - " + vecSolicitudes.get(28)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph("Inicio",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(29)+""));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph("T�rmino",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(30)+""));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	
	// fila 22
	celda = new PdfPCell(new Paragraph("Tipo trabajo",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(31)+""));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 23
	celda = new PdfPCell(new Paragraph("Tipo contrato",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(32)+""));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 24
	celda = new PdfPCell(new Paragraph("Suma fija",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(33)));
	celda.setBorder(0);
	table.addCell(celda);
	if (vecSolicitudes.get(34).toString().equals("OTR")) {
		celda = new PdfPCell(new Paragraph("Horas semestre",new Font(bf_helvb)));
		celda.setColspan(2);
		celda.setBorder(0);
		celda.setPaddingBottom(10);
		table.addCell(celda);
		celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(36)));
	}
	else {
		celda = new PdfPCell(new Paragraph("Semanas c�lculo",new Font(bf_helvb)));
		celda.setColspan(2);
		celda.setBorder(0);
		celda.setPaddingBottom(10);
		table.addCell(celda);
		celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(35)));
	}
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);

    // fila 25
	celda = new PdfPCell(new Paragraph("Nivel horas",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(37)+""));
	celda.setColspan(5);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 26
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(6);
	table.addCell(celda);
	
	if (!vecSolicitudes.get(34).toString().equals("OTR")) {
		// fila 27
		celda = new PdfPCell(new Paragraph("Solicitudes docente y acad�micas ", new Font(bf_helvb)));
		celda.setBorder(0);
		celda.setPaddingBottom(10);
		celda.setColspan(6);
		celda.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(celda);
		
		// Lista asignaturas
	    Vector vec_encabezado = new Vector();    
	    vec_encabezado.addElement("Asignatura/Paralelo");
	    vec_encabezado.addElement("Valor hrs. t�oricas $");
	    vec_encabezado.addElement("Cantidad hrs. t�oricas"); 
	    vec_encabezado.addElement("Valor hrs. pr�cticas $");
	    vec_encabezado.addElement("Cantidad hrs. pr�cticas");
	    vec_encabezado.addElement("Total $");
	   	 // add a table to the document
	   	 
	    float[] widths = {0.6f, 0.6f, 0.6f, 0.6f, 0.6f, 0.3f}; //largo de las columnas, en este caso son seis
	    PdfPTable table2 = new PdfPTable(widths); 
	    
		table2.setWidthPercentage(100);
				
		PdfPCell cell = null;
	    int columna = 0;
	    
		//Cabecera de la tabla    
	    for (int m = 0 ; m < vec_encabezado.size(); m++){
	       	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	      	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	table2.addCell(cell);
	    }
		
		
		if (vecListaAsign != null && vecListaAsign.size() > 0) {
			for (int x = 0 ; x < vecListaAsign.size(); x++){
		    	Vector lista = (Vector) vecListaAsign.get(x);
		    	columna = 0;
		    	for (int y = 0 ; y < lista.size(); y++){
			    	columna ++;
			       	cell = new PdfPCell(new Paragraph(lista.get(y)+""));
			      	if (columna == 1) {
			      		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			      	}
			      	else if (columna == 2 || columna == 4 || columna == 6) cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			      	else  cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			      	table2.addCell(cell);
		    	}
		    }	
		}	
	    
		celda = new PdfPCell(table2);
		celda.setColspan(6);
		celda.setBorder(0);
		table.addCell(celda);
		
		
		
	}	 
	
	// Totales
	celda = new PdfPCell(new Paragraph("TOTAL MENSUAL",new Font(bf_helvb)));
	celda.setColspan(2);
    celda.setBorder(0);
    celda.setPaddingBottom(10);
    table.addCell(celda);
    celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(38)+""));
    celda.setColspan(4);
    celda.setBorder(0);
    table.addCell(celda);
	    
	 // Totales
	 celda = new PdfPCell(new Paragraph("TOTAL CONTRATO", new Font(bf_helvb)));
	 celda.setColspan(2);
	 celda.setBorder(0);
	 celda.setPaddingBottom(10);
	 table.addCell(celda);
	 celda = new PdfPCell(new Paragraph(": " + vecSolicitudes.get(39)+""));
	 celda.setColspan(4);
	 celda.setBorder(0);
	 table.addCell(celda);

	    
	
    document.add(table);
	    

	document.close();

	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	
	response.getOutputStream().flush();
	response.getOutputStream().close();
	
	session.removeAttribute("vecSolicitudes");
	session.removeAttribute("vecListaAsign");
	

}catch(DocumentException e){
	e.printStackTrace();
}

%>