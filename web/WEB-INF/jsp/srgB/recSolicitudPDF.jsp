<%@page import="java.io.*,
        java.awt.Color,
		java.util.*,
		com.lowagie.text.pdf.*,
		com.lowagie.text.*,
		java.util.List,
		java.text.NumberFormat"
%><%
response.setContentType("application/pdf");
Document document = new Document(PageSize.LETTER, 20, 20, 25, 20);
int rutUsuario = session.getAttribute("rutUsuario") != null?
                 Integer.parseInt(session.getAttribute("rutUsuario")+""):0;
try{
  
    Vector vecSolicitudDocumento = request.getSession().getAttribute("vecSolicitudDocumento") != null?
    		    (Vector) request.getSession().getAttribute("vecSolicitudDocumento"): new Vector();
    Vector vecSolicitudDistribucion = request.getSession().getAttribute("vecSolicitudDistribucion") != null?
    	    		    (Vector) request.getSession().getAttribute("vecSolicitudDistribucion"): new Vector();	
    Vector vecHistorial = request.getSession().getAttribute("vecHistorial") != null?
    	    	    		    (Vector) request.getSession().getAttribute("vecHistorial"): new Vector();
   	String nombrePago = request.getSession().getAttribute("nombrePago") != null ?(String) request.getSession().getAttribute("nombrePago"):"";
    String glosa1 = request.getSession().getAttribute("glosa1") != null ?(String) request.getSession().getAttribute("glosa1"):"";
    String glosa2 = request.getSession().getAttribute("glosa2") != null ?(String) request.getSession().getAttribute("glosa2"):"";
    String glosa3 = request.getSession().getAttribute("glosa3") != null ?(String) request.getSession().getAttribute("glosa3"):"";
    String glosa4 = request.getSession().getAttribute("glosa4") != null ?(String) request.getSession().getAttribute("glosa4"):"";
    String glosa5 = request.getSession().getAttribute("glosa5") != null ?(String) request.getSession().getAttribute("glosa5"):"";
    String glosa6 = request.getSession().getAttribute("glosa6") != null ?(String) request.getSession().getAttribute("glosa6"):"";
    String rutnum = request.getSession().getAttribute("rutnum") != null ?(String) request.getSession().getAttribute("rutnum"):"";
    //String nomSede = request.getSession().getAttribute("nomSede") != null ?(String) request.getSession().getAttribute("nomSede"):""; VS.04.01.2017
    String nomOrganizacion = request.getSession().getAttribute("nomOrganizacion") != null ?(String) request.getSession().getAttribute("nomOrganizacion"):""; 
    String valorAPagar = request.getSession().getAttribute("valorAPagar") != null ?(String) request.getSession().getAttribute("valorAPagar"):"";
    String identificadorInterno = request.getSession().getAttribute("identificadorInterno") != null ?(String) request.getSession().getAttribute("identificadorInterno"):"";
    String numDoc = request.getSession().getAttribute("numDoc") != null ?(String) request.getSession().getAttribute("numDoc"):"";
    String estadoFinal = request.getSession().getAttribute("estadoFinal") != null ?(String) request.getSession().getAttribute("estadoFinal"):"";
  
    NumberFormat formatoNum = 	NumberFormat.getInstance(Locale.GERMAN);

    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    PdfWriter writer = PdfWriter.getInstance(document, buffer); 

    // various fonts
    BaseFont bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
    BaseFont bf_helvb = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", false);
    BaseFont bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
    BaseFont bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
    
    Font helvb = new Font(bf_helvb);
    helvb.setSize(8);
    helvb.setColor(Color.BLACK);

    Font helv = new Font(bf_helv);
    helv.setSize(8);
    helv.setColor(Color.BLACK);

    document.open();
    
    //Ejemplos de IMAGE
  /*  Image foto = Image.getInstance("web/sgf/img/logos/logo_usm_bn70x51.jpg");
    foto.scaleToFit(100, 100);
    foto.setAlignment(Chunk.ALIGN_MIDDLE);
    document.add(foto); */
  			
	PdfPTable table = new PdfPTable(3);
 	table.setWidthPercentage(70); // Code 2
	table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3
		
	PdfPTable table1 = new PdfPTable(3);
 	table.setWidthPercentage(100); // Code 2
	table.setHorizontalAlignment(Element.ALIGN_CENTER);//Code 3

    //a�adimos texto con formato a la primera celda
	PdfPCell celda = new PdfPCell(new Paragraph("Universidad T�cnica Federico Santa Mar�a  ",new Font(bf_helvb)));
	//unimos esta celda con otras 2
	celda.setColspan(3);
	//alineamos el contenido al centro
	celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	// a�adimos un espaciado
	celda.setPadding (12.0f);
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);

	//fila 2
	celda = new PdfPCell(new Paragraph("Direcci�n General de Finanzas ",new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	table.addCell(celda);
	
	//fila 3        
/*	
    celda = new PdfPCell(new Paragraph("Fecha:" + vec_datos.get(6)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
	table.addCell(celda);
	
	//fila 4        
	celda = new PdfPCell(new Paragraph("Comit� de Coordinaci�n y Desarrollo Docente", new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	table.addCell(celda);*/
	
	// fila 5
	celda = new PdfPCell(new Paragraph("Solicitud de Recuperaci�n de Gastos  " + identificadorInterno, new Font(bf_helvb)));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	table.addCell(celda);
	
	// fila 6
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	table.addCell(celda);
	
	// fila 7
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	table.addCell(celda);
	
	if(!rutnum.trim().equals("")){
		
	celda = new PdfPCell(new Paragraph("N� de Solicitud "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph (numDoc));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);	
		
    // fila 8
 	celda = new PdfPCell(new Paragraph("RUT a Pago "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph (rutnum));
	celda.setBorder(0);
	celda.setColspan(2);
	table.addCell(celda);
	}
	
    // fila 9	
	celda = new PdfPCell(new Paragraph("Pagar a "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph (nombrePago));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	// VS.04.01.2017. fila 10 no va.
	// fila 10
	//celda = new PdfPCell(new Paragraph("Sede " ));
	//celda.setBorder(0);
	//celda.setPaddingBottom(10);
	//table.addCell(celda);
	//celda = new PdfPCell(new Paragraph(nomSede));
	//celda.setColspan(2);
	//celda.setBorder(0);
	//table.addCell(celda);
	
	// fila 11
	celda = new PdfPCell(new Paragraph("Valor a Pagar "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(valorAPagar));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
//	celda = new PdfPCell(new Paragraph("Tipo de Pago "));
//	celda.setBorder(0);
	//celda.setPaddingBottom(10);
	//table.addCell(celda);
	//celda = new PdfPCell(new Paragraph(nomTipoPago));
	//celda.setColspan(2);
	//celda.setBorder(0);
	//table.addCell(celda);*/
	
	// fila 1s
	celda = new PdfPCell(new Paragraph("Organizaci�n " ));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(nomOrganizacion));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph("Descripci�n "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(glosa1));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(glosa2));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(glosa3));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(glosa4));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(glosa5));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	// fila 12
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);
	celda = new PdfPCell(new Paragraph(glosa6));
	celda.setColspan(2);
	celda.setBorder(0);
	table.addCell(celda);
	
	celda = new PdfPCell(new Paragraph(" "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	celda.setColspan(3);
	table.addCell(celda);
	
	
	celda = new PdfPCell(new Paragraph("Documentos Asociados "));
	celda.setBorder(0);
	celda.setPaddingBottom(10);
	table.addCell(celda);

	
	// distribuci�n de gastos
    Vector vec_encabezado = new Vector();    
    vec_encabezado.addElement("N�");
    vec_encabezado.addElement("Tipo");
    vec_encabezado.addElement("RUT"); 
    vec_encabezado.addElement("I.V.A."); 
    vec_encabezado.addElement("Total"); 
    vec_encabezado.addElement("Rubro Gasto"); 
    vec_encabezado.addElement("Glosa"); 
   	
	 // add a table to the document
	 	float[] widths = {0.2f, 0.2f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f}; /*largo de las columnas, en este caso son siete*/
		PdfPTable table2 = new PdfPTable(widths);
	    table2.setWidthPercentage(100);
	    table2.setHeaderRows(1);/* cantidad de filas cabeceras para que se repitan en cada hoja*/
	    
	    PdfPCell cell = null;
	    int columna = 0;
	    
		//Cabecera de la tabla    
	    for (int m = 0 ; m < vec_encabezado.size(); m++){
	    	columna ++;
	      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
	      	cell.setBackgroundColor(Color.LIGHT_GRAY);
	      	if (columna == 2) {
	      		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	      	}
	      	
	      	table2.addCell(cell);
	      //	if (columna == 3) columna = 0;
	    }

	    //Contenido de la tabla
	    columna = 0;
        long total = 0;
      //  System.out.println("vecSolicitudDocumento.size(): "+vecSolicitudDocumento);
	    for (int d = 0 ; d < vecSolicitudDocumento.size(); d++){   
	    	//if (d == vecSolicitudes.size()) {   	
	    		Vector vec = new Vector();
	    		vec =(Vector) vecSolicitudDocumento.get(d);
	    		
		    	for(int col = 0;col < 7;col++){      	
		    	cell = new PdfPCell(new Paragraph(vec.get(col).toString(), helv));
		      	if ( col == 3 || col == 4) {
		      		if(col == 4)
		      			total += Long.parseLong(vec.get(col).toString());
		      		cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(vec.get(col).toString())),helvb));
		      		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			      	
		      	}
		      	columna ++;
		      	table2.addCell(cell);      	
		    	}
	    //	}
	    }    
	
	 
	    cell = new PdfPCell(new Paragraph("TOTAL", helvb));
	    table2.addCell(cell);
	    
	    cell = new PdfPCell(new Paragraph(" "));
	    cell.setBorder(0);
	    table2.addCell(cell);
	    
	    cell = new PdfPCell(new Paragraph(" "));
	    cell.setBorder(0);
	    table2.addCell(cell);
	    
	    cell = new PdfPCell(new Paragraph(" "));
	    cell.setBorder(0);
	    table2.addCell(cell);
	    
	    cell = new PdfPCell(new Paragraph(formatoNum.format(total),helvb));
	    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	    table2.addCell(cell);
	    
	    cell = new PdfPCell(new Paragraph(" "));
	    cell.setBorder(0);
	    table2.addCell(cell);
	    
	    cell = new PdfPCell(new Paragraph(" "));
	    cell.setBorder(0);
	    table2.addCell(cell);


	    cell = new PdfPCell(new Paragraph(" "));
	    cell.setBorder(0);
	    table2.addCell(cell); 
	
	    
		celda = new PdfPCell(table2);
		celda.setColspan(2);
		celda.setBorder(0);
		table.addCell(celda);

		celda = new PdfPCell(new Paragraph(" "));
		celda.setBorder(0);
		celda.setPaddingBottom(10);
		celda.setColspan(7);
		table.addCell(celda);
		
		// distribuci�n del pago
//	if(vecSolicitudDistribucion!= null && vecSolicitudDistribucion.size() > 0){
//	
//			// fila 14
//		celda = new PdfPCell(new Paragraph("Distribuci�n de Gastos "));
//		celda.setBorder(0);
//		celda.setPaddingBottom(10);
//		table.addCell(celda);
//		
//		if(vecSolicitudDistribucion!= null && vecSolicitudDistribucion.size() > 0){
//		    vec_encabezado = new Vector();    
//		    vec_encabezado.addElement("Organizaci�n"); // VS.04-01-2017 cuenta - organizacion
//		    vec_encabezado.addElement("Valor $");
//				
//		
//		 // add a table to the document
//		 	float[] widths2 = {0.4f, 0.1f}; /*largo de las columnas, en este caso son dos*/
//			table2 = new PdfPTable(widths2);
//		    table2.setWidthPercentage(100);
//		    table2.setHeaderRows(1);/* cantidad de filas cabeceras para que se repitan en cada hoja*/
//		    
//		    cell = null;
//		    columna = 0;
//		    
//			//Cabecera de la tabla    
//		    for (int m = 0 ; m < vec_encabezado.size(); m++){
//		    	columna ++;
//		      	cell = new PdfPCell(new Paragraph(vec_encabezado.get(m).toString(), helvb));
//		      	cell.setBackgroundColor(Color.LIGHT_GRAY);
//		      	if (columna == 1) {
//		      		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		      	}
//		      	table2.addCell(cell);
//		      	if (columna == 2) columna = 0;
//		    }
//
//		    //Contenido de la tabla
//		    columna = 0;
//	        total = 0;
//	     //   System.out.println("vecSolicitudes.size(): "+vecSolicitudes.size());
//		    for (int d = 0 ; d < vecSolicitudDistribucion.size(); d++){   
//		    	//if (d == vecSolicitudes.size()) {   	
//		    		Vector vec = new Vector();
//		    		vec =(Vector) vecSolicitudDistribucion.get(d);
//		    		
//			    	for(int col = 0;col < 2;col++){      	
//			    	cell = new PdfPCell(new Paragraph(vec.get(col).toString(), helv));
//			      	if (col == 1) {
//			      		total += Long.parseLong(vec.get(col).toString());
//			      		cell = new PdfPCell(new Paragraph(formatoNum.format(Long.parseLong(vec.get(col).toString())),helvb));
//			      		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//				      	
//			      	}
//			      	columna ++;
//			      	table2.addCell(cell);      	
//			    	}
//		    //	}
//		    }    
//
//		    // Totales
//		    cell = new PdfPCell(new Paragraph("TOTAL", helvb));
//		    table2.addCell(cell);
//		    
//		    cell = new PdfPCell(new Paragraph(formatoNum.format(total),helvb));
//		    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		    table2.addCell(cell);
//
//
//		    cell = new PdfPCell(new Paragraph(""));
//		    cell.setBorder(0);
//		    table2.addCell(cell); 
//		
//		    
//			celda = new PdfPCell(table2);
//			celda.setColspan(2);
//			celda.setBorder(0);  
//			table.addCell(celda);
//		}
//		
//		// fin distribucion del pago
//		
//			celda = new PdfPCell(new Paragraph(" "));
//			celda.setBorder(0);
//			celda.setPaddingBottom(10);
//			celda.setColspan(3);
//			table.addCell(celda);
//	}			
			// historial
				
		celda = new PdfPCell(new Paragraph("Historial "));
		celda.setBorder(0);
		celda.setPaddingBottom(10);
		table.addCell(celda);
		float[] widths2 = {1f, 2f}; /*largo de las columnas, en este caso son tres*/
		table2 = new PdfPTable(widths2);
	    table2.setWidthPercentage(100);
	    

		if(vecHistorial!= null && vecHistorial.size() > 0){
			for(int i=0;i<vecHistorial.size();i++){
				Vector vec = (Vector) vecHistorial.get(i);
			 	cell = new PdfPCell(new Paragraph(vec.get(0).toString(), helv));
			 //	cell.setBorder(0);
			 	table2.addCell(cell);
		       	cell = new PdfPCell(new Paragraph(vec.get(1).toString(),helvb));
		         //	cell.setBorder(0);
			   	table2.addCell(cell);
			
			}
		}
		celda = new PdfPCell(table2);
		celda.setColspan(2);
		celda.setBorder(0);
		table.addCell(celda);
		// fin distribucion del pago
			// fin historial
			
			
	    document.add(table);
	    

	document.close();

	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	for(int i = 0; i < bytes.length; i++)
	{
		dataOutput.writeByte(bytes[i]);
	}
	
	response.getOutputStream().flush();
	response.getOutputStream().close();
	
	session.removeAttribute("vecSolicitudDocumento");
	session.removeAttribute("vecSolicitudDistribucion");
	session.removeAttribute("vecHistorial");
	session.removeAttribute("nombrePago");
	session.removeAttribute("glosa1");
	session.removeAttribute("glosa2");
	session.removeAttribute("glosa3");
	session.removeAttribute("glosa4");
	session.removeAttribute("glosa5");
	session.removeAttribute("glosa6");
	session.removeAttribute("rutnum");
	session.removeAttribute("numDoc");
	session.removeAttribute("nomIdentificador");
	//session.removeAttribute("nomSede"); VS.04-01-2017
	session.removeAttribute("nomOrganizacion");
	session.removeAttribute("valorAPagar");
	session.removeAttribute("identificadorInterno");
	session.removeAttribute("estadoFinal");

}catch(DocumentException e){
	e.printStackTrace();
}

%>