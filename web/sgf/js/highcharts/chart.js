var chart;
$(document).ready(function() {
	chart = new Highcharts.Chart({
	chart: {
		renderTo: 'container',
		defaultSeriesType: 'bar',
		marginLeft:margen
	},
	title: {	
		text: titulo
	},
	subtitle: {
		text: subtitulo
	},
	xAxis: {
		categories: conceptos,	
		title: {
			text: null
		},
		labels: {
			align: 'right',
			style: {
				font: 'normal 9px Verdana, sans-serif'
			}
		}
		
	},
	yAxis: {
		min: null,
		tickPixelInterval: 100,
		
		title: {
			text: null
		},
		labels: {
			/*rotation: -45,
			align: 'right',*/
			style: {
				font: 'normal 9px Verdana, sans-serif'
			},
			formatter: function() {
				return Highcharts.numberFormat(this.value, 0, ',', '.');
			}
		},
		lineWidth: 1
	},

	tooltip: {
		formatter: function() {
			return ''+ this.series.name +': $ '+ Highcharts.numberFormat(this.y, 0, ',', '.');
		}
	},
	plotOptions: {
		bar: {
			dataLabels: {
				enabled: false
			}
		}
	},
	legend: {
		backgroundColor: '#FFFFFF',
		reversed: true
	},
	credits: {
		enabled: false
	},
	series: [{
		name: label_line1,
		data: line1
	}, {
		name: label_line2,
		data: line2
	}]
	});
				
				
	});


function cerrar(){
	window.close();
}
