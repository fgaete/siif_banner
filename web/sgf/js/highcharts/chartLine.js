var chart;
$(document).ready(function() {
   chart = new Highcharts.Chart({
      chart: {
         renderTo: 'container',
         defaultSeriesType: 'line',
         marginRight: margen,
         marginBottom: 25
      },
      title: {
         text: titulo,
         x: -20 //center
      },
      subtitle: {
         text: subtitulo,
         x: -20
      },
      xAxis: {
         categories: conceptos
      },
      yAxis: {
         title: {
            text: ''
         },
         plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
         }]
      },
      tooltip: {
         formatter: function() {
                   return '<b>'+ this.series.name +'</b> '+
               this.x +': $ '+ Highcharts.numberFormat(this.y, 0, ',', '.');
         }
      },
      legend: {
         layout: 'vertical',
         align: 'right',
         verticalAlign: 'top',
         x: -10,
         y: 100,
         borderWidth: 0
      },
	  credits: {
		enabled: false
	  },
      series: serie
   });
   
   
});