var chart;
$(document).ready(function() {
	chart = new Highcharts.Chart({
      chart: {
         renderTo: 'container',
         defaultSeriesType: 'column',
         height: 480 ,
         marginRight: margen
      },
      title: {
         text: titulo
      },
  	  subtitle: {
		 text: subtitulo
	  },
      xAxis: {
         categories: conceptos,
         labels: {
            rotation: 45,
            align: 'left'
         }
      },
      yAxis: {
         min: 0,		
         title: {
            text: texto_ejeY
         }
      },
      tooltip: {
         formatter: function() {
            return ''+
                this.series.name +': $ '+ Highcharts.numberFormat(this.y, 0, ',', '.') +' ('+ Math.round(this.percentage) +'%)';
         }
      },
      legend: {
         layout: 'vertical',
         align: 'right',
         verticalAlign: 'top',
         x: 0,
         y: 100,
         borderWidth: 0
      },
	  credits: {
		enabled: false
	  },
      plotOptions: {
         column: {
            stacking: 'percent'
         }
      },
      
      series: serie
   });
   
   
}); 