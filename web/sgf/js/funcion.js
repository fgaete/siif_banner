function Item(v){
  if (v.value == 1) window.location.href = "resumen_item.html";
}
function Todos(v){
  if (v.value == 1) window.location.href = "resumen.html";
}

/* llamar a prototype de este JS */
Number.prototype.decimal = function (num) { 
    pot = Math.pow(10,num); 
    return parseInt(this * pot) / pot; 
} 
function truncDecimal(valor,cantdec) { 
    var n = eval(valor); 
    return n.decimal(cantdec); //deja esa cantidad de decimales 
} 

function format(input){

	var num = input.value.replace(/\./g,"");
	if(!isNaN(num)){
		num = num.toString().split("").reverse().join("").replace(/(?=\d*\.?)(\d{3})/g,"$1.");
		num = num.split("").reverse().join("").replace(/^[\.]/,"");
		input.value = num;
	}
	else{ 
		alert("Solo se permiten numeros");
		input.value = input.value.replace(/[^\d\.]*/g,"");
	}
}
/* <input type="text" onkeyup="format(this)" onchange="format(this)"> */

function formatNumber(num,prefix){
	prefix = prefix || '';
	num += '';
	var splitStr = num.split(',');
	var splitLeft = splitStr[0];
	var splitRight = splitStr.length > 1 ? ',' + splitStr[1] : '';
	var regx = /(\d+)(\d{3})/;
	while (regx.test(splitLeft)) {
		splitLeft = splitLeft.replace(regx, '$1' + '.' + '$2');
	}
	return prefix + splitLeft + splitRight;
}

function unformatNumber(num) {
    num = num.replace(/\./g,"");
    num = num.replace("$","");
	return num;
} 
/*
var test1 = formatNumber('5123456789,25'); // devuelve 5.123.456.789,25
var test2 = formatNumber(1234,'$'); // devuelve $1.234
var test3 = unformatNumber('$1.234,15'); // devuelve 1234,15 
*/


function acceptNum(evt){
 if(window.event) key = evt.keyCode;
  else key = evt.which;  
  return (key < 13 || (key >= 48 && key <= 57));
}

function acceptText(evt){
 if(window.event) key = evt.keyCode;
 else key = evt.which;  
 return (key != 13 && key != 34 && key != 39);
}

function cuentaLetras(valor, largo){
largo = largo - 1; /****por onkeypress, si es onkeyup quitar el -1****/
var obs = document.getElementById(valor).value;
if(obs.length > largo)
 document.getElementById(valor).value = obs.substr(0,largo);
}


function verificaCambios(t,h){
  if (h > 0) {
	  document.getElementById('mensajeCambio').innerHTML = "Por favor, recuerde REGISTRAR la informaci&oacute;n para que sus cambios sean guardados.";
	  t.style.color="#3366CC";
  }
  /*t.style.fontWeight = "bold"; */
}

function realizaCambios(t,h){
  if (h > 0) {
	  document.getElementById('mensajeCambio').innerHTML = "Se han detectado cambios en Datos del Cliente,\n recuerde que toda informaci&oacute;n modificada est&aacute; sujeta a revisi&oacute;n en Finanzas.";
	  t.style.color="#3366CC";
  }
  /*t.style.fontWeight = "bold"; */
}

function asigna(valor){
if(valor == 1) {/*area*/
   if(document.getElementById('area').value.length > 40){
   		document.getElementById('area1').value = document.getElementById('area').value.substr(0,40);
   		document.getElementById('area2').value = "";
   		if(document.getElementById('area').value.length <= 80)
   			document.getElementById('area2').value = document.getElementById('area').value.substr(40);   
   		else
   			document.getElementById('area2').value = document.getElementById('area').value.substr(40,40); 	
   	} else
   	   document.getElementById('area1').value = document.getElementById('area').value;   
   }

 if(valor == 2) {/*comen*/

   if(document.getElementById('comen').value.length <= 40){
      document.getElementById('comen1').value = document.getElementById('comen').value;
      document.getElementById('comen2').value = "";
      document.getElementById('comen3').value = "";
      document.getElementById('comen4').value = "";
      document.getElementById('comen5').value = "";
      document.getElementById('comen6').value = "";
   } else {
    		document.getElementById('comen1').value = document.getElementById('comen').value.substr(0,40);
   		 	if(document.getElementById('comen').value.length <= 80){
   		     	document.getElementById('comen2').value = document.getElementById('comen').value.substr(40);
   		     	document.getElementById('comen3').value = "";
      			document.getElementById('comen4').value = "";
      			document.getElementById('comen5').value = "";
      			document.getElementById('comen6').value = "";
   		 	} else {    
   					document.getElementById('comen2').value = document.getElementById('comen').value.substr(40,40);
            		if(document.getElementById('comen').value.length <= 120){
               			document.getElementById('comen3').value = document.getElementById('comen').value.substr(80);
               			document.getElementById('comen4').value = "";
      					document.getElementById('comen5').value = "";
      					document.getElementById('comen6').value = "";
            		} else {   
   			      			document.getElementById('comen3').value = document.getElementById('comen').value.substr(80,40);
   			      			if(document.getElementById('comen').value.length <= 160){
               					document.getElementById('comen4').value = document.getElementById('comen').value.substr(120);
               					document.getElementById('comen5').value = "";
      							document.getElementById('comen6').value = "";
            	  	        } else {
            	  	        		document.getElementById('comen4').value = document.getElementById('comen').value.substr(120, 40);
            	  	        		if(document.getElementById('comen').value.length <= 200){
               							document.getElementById('comen5').value = document.getElementById('comen').value.substr(160);
               							document.getElementById('comen6').value = "";
            	  	        		} else {
            	  	        				document.getElementById('comen5').value = document.getElementById('comen').value.substr(160, 40);
            	  	        		        if(document.getElementById('comen').value.length <= 240)
            	  	        		           document.getElementById('comen6').value = document.getElementById('comen').value.substr(200);
            	  	        		           else
            	  	        		           document.getElementById('comen6').value = document.getElementById('comen').value.substr(200,40);
            	  	        				}
            	  	        		}
   			      			}
   					}      
   			}      
   	}		      		
 if(valor == 3) {/*motiv*/
    if(document.getElementById('motiv').value.length <= 40){
      document.getElementById('motiv1').value = document.getElementById('motiv').value;
      document.getElementById('motiv2').value = "";
      document.getElementById('motiv3').value = "";
      document.getElementById('motiv4').value = "";
      document.getElementById('motiv5').value = "";
      document.getElementById('motiv6').value = "";
      
   } else {
    		document.getElementById('motiv1').value = document.getElementById('motiv').value.substr(0,40);
   		 	if(document.getElementById('motiv').value.length <= 80){
   		     	document.getElementById('motiv2').value = document.getElementById('motiv').value.substr(40);
   		     	document.getElementById('motiv3').value = "";
     			document.getElementById('motiv4').value = "";
      			document.getElementById('motiv5').value = "";
      			document.getElementById('motiv6').value = "";
   		 	} else {    
   					document.getElementById('motiv2').value = document.getElementById('motiv').value.substr(40,40);
            		if(document.getElementById('motiv').value.length <= 120){
               			document.getElementById('motiv3').value = document.getElementById('motiv').value.substr(80);
               			document.getElementById('motiv4').value = "";
      					document.getElementById('motiv5').value = "";
      					document.getElementById('motiv6').value = "";
            		} else {   
   			      			document.getElementById('motiv3').value = document.getElementById('motiv').value.substr(80,40);
   			      			if(document.getElementById('motiv').value.length <= 160){
               					document.getElementById('motiv4').value = document.getElementById('motiv').value.substr(120);
            	  	            document.getElementById('motiv5').value = "";
      							document.getElementById('motiv6').value = "";
      							} else {
            	  	        		document.getElementById('motiv4').value = document.getElementById('motiv').value.substr(120, 40);
            	  	        		if(document.getElementById('motiv').value.length <= 200){
               							document.getElementById('motiv5').value = document.getElementById('motiv').value.substr(160);
               							document.getElementById('motiv6').value = "";
            	  	        		} else {
            	  	        				document.getElementById('motiv5').value = document.getElementById('motiv').value.substr(160, 40);
            	  	        				if(document.getElementById('motiv').value.length <= 240)
            	  	        		        	document.getElementById('motiv6').value = document.getElementById('motiv').value.substr(200);
            	  	        		        else
            	  	        		            document.getElementById('motiv6').value = document.getElementById('motiv').value.substr(200,40);
            	  	        				}
            	  	        		}
   			      			}
   					}      
   			}      
   	}
   	
   	

if(valor == 4){
    glosa = document.getElementById('glosa').value;
    
    glosa1 = "";
    glosa2 = "";
    glosa3 = "";
    glosa4 = "";
    glosa5 = "";
    glosa6 = "";
    indiceIni = 0;
    indiceFin = 41;

    if(glosa.length <= 40){
      glosa1 = glosa;     
      
   } else {    
   				
                indiceFin = acomodaTexto(glosa, indiceFin);
   		        glosa1 = glosa.substr(indiceIni,indiceFin);   		       	
   		     	glosa2 = glosa.substr(indiceFin);
   		     	if(glosa2.length > 40){
   		     	    indiceIni = indiceFin;
   		     	    indiceFin = (glosa.length < indiceIni + 40)?glosa.length:indiceIni + 40;
   		     	    indiceFin = acomodaTexto(glosa, indiceFin);
   		     	    glosa2 = '';
   		     	    glosa2 = glosa.substr(indiceIni,(indiceFin - indiceIni ));  
   		     	    glosa3 = glosa.substr(indiceFin); 
   		    
	     			if(glosa3.length > 40){
   		     	    	indiceIni = indiceFin;
   		     	    	indiceFin = (glosa.length < (indiceIni + 40))?glosa.length:(indiceIni + 40);
   		     	     	indiceFin = acomodaTexto(glosa, indiceFin);
   		     	     	glosa3 = glosa.substr(indiceIni,(indiceFin - indiceIni )); 
   		     	    	glosa4 = glosa.substr(indiceFin); 
   		    
   		     			if(glosa4.length > 40){
   		     	    		indiceIni = indiceFin;
   		     	    		indiceFin = (glosa.length < (indiceIni + 40))?glosa.length:(indiceIni + 40);
   		     	    		indiceFin = acomodaTexto(glosa, indiceFin);
   		     	    		glosa4 = glosa.substr(indiceIni,(indiceFin - indiceIni ));  
   		     	    		glosa5 = glosa.substr(indiceFin); 
   		     			
            			if(glosa5.length > 40){
   		     	    		indiceIni = indiceFin;
   		     	    		indiceFin = (glosa.length < (indiceIni + 40))?glosa.length:(indiceIni + 40);
   		     	    		indiceFin = acomodaTexto(glosa, indiceFin);
   		     	    		glosa5 = glosa.substr(indiceIni,(indiceFin - indiceIni));
   		     	    		glosa6 = glosa.substr(indiceFin); 
   		     			
            	  	    if(glosa6.length > 40){
            	  	        indiceIni = indiceFin;
            	  	        indiceFin = (glosa.length < (indiceIni + 40))?glosa.length:(indiceIni + 40);
            	  	  		indiceFin = acomodaTexto(glosa, indiceFin);
            	  			glosa6 = glosa.substr(indiceIni,(indiceFin - indiceIni )); 
   		        		 }			
   		     			}
            	  	    }
   			      	    }
   					} 
   					}
   					
   			document.getElementById('glosa1').value = glosa1;
      		document.getElementById('glosa2').value = glosa2;
      		document.getElementById('glosa3').value = glosa3;
      		document.getElementById('glosa4').value = glosa4;
      		document.getElementById('glosa5').value = glosa5;
      		document.getElementById('glosa6').value = glosa6;	
      		document.getElementById('glosa').value = document.getElementById('glosa1').value + document.getElementById('glosa2').value+document.getElementById('glosa3').value + document.getElementById('glosa4').value + document.getElementById('glosa5').value +	document.getElementById('glosa6').value;	     

 }   
 if(valor == 5){ // para boletas de honorario y facturaci�n
    glosa = document.getElementById('glosa').value;
    
    glosa1 = "";
    glosa2 = "";
    glosa3 = "";

    indiceIni = 0;
    indiceFin = 41;

    if(glosa.length <= 40){
      glosa1 = glosa;     
      
   } else {    
   				
                indiceFin = acomodaTexto(glosa, indiceFin);
   		        glosa1 = glosa.substr(indiceIni,indiceFin);   		       	
   		     	glosa2 = glosa.substr(indiceFin);
   		     	if(glosa2.length > 40){
   		     	    indiceIni = indiceFin;
   		     	    indiceFin = (glosa.length < indiceIni + 40)?glosa.length:indiceIni + 40;
   		     	    indiceFin = acomodaTexto(glosa, indiceFin);
   		     	    glosa2 = '';
   		     	    glosa2 = glosa.substr(indiceIni,(indiceFin - indiceIni ));  
   		     	    glosa3 = glosa.substr(indiceFin); 
   		    
	     			if(glosa3.length > 40){
   		     	    	indiceIni = indiceFin;
   		     	    	indiceFin = (glosa.length < (indiceIni + 40))?glosa.length:(indiceIni + 40);
   		     	     	indiceFin = acomodaTexto(glosa, indiceFin);
   		     	     	glosa3 = glosa.substr(indiceIni,(indiceFin - indiceIni )); 
   		     	    }
   					} 
   					}
   					
   			document.getElementById('glosa1').value = glosa1;
      		document.getElementById('glosa2').value = glosa2;
      		document.getElementById('glosa3').value = glosa3;
       		document.getElementById('glosa').value = document.getElementById('glosa1').value + document.getElementById('glosa2').value+document.getElementById('glosa3').value;	     

}  

if(valor == 6){
    motiv = document.getElementById('motiv').value;
    
    motiv1 = "";
    motiv2 = "";
    motiv3 = "";
    motiv4 = "";
    motiv5 = "";
    motiv6 = "";
    indiceIni = 0;
    indiceFin = 41;

    if(motiv.length <= 40){
      motiv1 = motiv;     
      
   } else {    
   				
                indiceFin = acomodaTexto(motiv, indiceFin);
   		        motiv1 = motiv.substr(indiceIni,indiceFin);   		       	
   		     	motiv2 = motiv.substr(indiceFin);
   		     	if(motiv2.length > 40){
   		     	    indiceIni = indiceFin;
   		     	    indiceFin = (motiv.length < indiceIni + 40)?motiv.length:indiceIni + 40;
   		     	    indiceFin = acomodaTexto(motiv, indiceFin);
   		     	    motiv2 = '';
   		     	    motiv2 = motiv.substr(indiceIni,(indiceFin - indiceIni ));  
   		     	    motiv3 = motiv.substr(indiceFin); 
   		    
	     			if(motiv3.length > 40){
   		     	    	indiceIni = indiceFin;
   		     	    	indiceFin = (motiv.length < (indiceIni + 40))?motiv.length:(indiceIni + 40);
   		     	     	indiceFin = acomodaTexto(motiv, indiceFin);
   		     	     	motiv3 = motiv.substr(indiceIni,(indiceFin - indiceIni )); 
   		     	    	motiv4 = motiv.substr(indiceFin); 
   		    
   		     			if(motiv4.length > 40){
   		     	    		indiceIni = indiceFin;
   		     	    		indiceFin = (motiv.length < (indiceIni + 40))?motiv.length:(indiceIni + 40);
   		     	    		indiceFin = acomodaTexto(motiv, indiceFin);
   		     	    		motiv4 = motiv.substr(indiceIni,(indiceFin - indiceIni ));  
   		     	    		motiv5 = motiv.substr(indiceFin); 
   		     			
            			if(motiv5.length > 40){
   		     	    		indiceIni = indiceFin;
   		     	    		indiceFin = (motiv.length < (indiceIni + 40))?motiv.length:(indiceIni + 40);
   		     	    		indiceFin = acomodaTexto(motiv, indiceFin);
   		     	    		motiv5 = motiv.substr(indiceIni,(indiceFin - indiceIni));
   		     	    		motiv6 = motiv.substr(indiceFin); 
   		     			
            	  	    if(motiv6.length > 40){
            	  	        indiceIni = indiceFin;
            	  	        indiceFin = (motiv.length < (indiceIni + 40))?motiv.length:(indiceIni + 40);
            	  	  		indiceFin = acomodaTexto(motiv, indiceFin);
            	  			motiv6 = motiv.substr(indiceIni,(indiceFin - indiceIni )); 
   		        		 }			
   		     			}
            	  	    }
   			      	    }
   					} 
   					}
   					
   			document.getElementById('motiv1').value = motiv1;
      		document.getElementById('motiv2').value = motiv2;
      		document.getElementById('motiv3').value = motiv3;
      		document.getElementById('motiv4').value = motiv4;
      		document.getElementById('motiv5').value = motiv5;
      		document.getElementById('motiv6').value = motiv6;	
      		document.getElementById('motiv').value = document.getElementById('motiv1').value + document.getElementById('motiv2').value+document.getElementById('motiv3').value + document.getElementById('motiv4').value + document.getElementById('motiv5').value +	document.getElementById('motiv6').value;	     

}	
}

function acomodaTexto(texto, indice){
 salir = 1;
 while((salir == 1) && (indice >= 0)){
  if(texto.substr(indice - 1,1) != ' ' && texto.substr(indice,1) == ' ')
       salir = 0;
     else
       indice = indice - 1;
   
       
  }
 return indice;           
}


//C�digo para colocar 
//los indicadores de miles mientras se escribe
//script por tunait!
function puntitos(donde,caracter){
	pat = /[\*,\+,\(,\),\?,\,$,\[,\],\^]/;
	valor = donde.value;
	largo = valor.length;
	crtr = true;
	if(isNaN(caracter) || pat.test(caracter) == true){
		if (pat.test(caracter)==true){ 
			caracter = "\\" + caracter;
		}
		carcter = new RegExp(caracter,"g");
		valor = valor.replace(carcter,"");
		donde.value = valor;
		crtr = false;
	}
	else{
		var nums = new Array();
		cont = 0;
		for(m=0;m<largo;m++){
			if(valor.charAt(m) == "." || valor.charAt(m) == " ")
				{continue;}
			else{
				nums[cont] = valor.charAt(m);
				cont++;
			}
		}
	}
	var cad1="",cad2="",tres=0
	if(largo > 3 && crtr == true){
		for (k=nums.length-1;k>=0;k--){
			cad1 = nums[k];
			cad2 = cad1 + cad2;
			tres++;
			if((tres%3) == 0){
				if(k!=0){
					cad2 = "." + cad2;
				}
			}
		}
		donde.value = cad2;
	}
}

/* PRESUP */

function cargarEscogeItem(valor){
	/*  MB modifica segun instrucciones de presupuesto  2015
	 if(document.detallePresup.item.value == 41)
		cargarReq(valor);
	else*/
	cargarItem(valor,document.detallePresup.item.value);
	return;
}
function cargarReq(home){
    document.detallePresup.itedoc.value = "41";
    document.detallePresup.accionweb.value = 'presupuestoInterceptorB.cargaPresupRequerimiento';		
	document.detallePresup.action = home+'/sipB/inc/presupItemreq.html';
	document.detallePresup.submit();	
}

function cargarItem(home,item){
	document.detallePresup.accionweb.value = 'presupuestoInterceptorB.cargaPresupItem';
	document.detallePresup.itedoc.value = item;
	document.detallePresup.action = home+'/sipB/presupItem.html';
	document.detallePresup.submit();
}

function cargarItemEsp(home,item){
	document.detallePresup.accionweb.value = 'presupuestoInterceptorB.cargaPresupItemEsp';
	document.detallePresup.itedoc.value = item;
	document.detallePresup.action = home+'/sipB/presupItemEsp.html';
	document.detallePresup.submit();
}

function registrarPresup(home, indexi){
//alert(document.getElementById('topeUnidad').value+' < '+document.getElementById('totalItems').value);


 if(document.getElementById('botonRegistrar')!= null)
		           		 	document.getElementById('botonRegistrar').style.visibility="hidden";
					
if(document.getElementById('topeUnidad') != null && document.getElementById('topeUnidad') != null &&
   document.getElementById('topeUnidad').value > 0 &&
   document.getElementById('totalItems').value/1 > document.getElementById('topeUnidad').value/1){
   	var cad1="",cad2="",tres=0
   	var nums = document.getElementById('topeUnidad').value;
 
		for (k=nums.length-1;k>=0;k--){
			cad1 = nums[k];
			cad2 = cad1 + cad2;
			tres++;
			if((tres%3) == 0){
				if(k!=0){
					cad2 = "." + cad2;
				}
			}
		}
		tope = cad2;

	alert('NO PUEDE REGISTRAR LA PRESUPUESTACION CORRIENTE.\nHa sobrepasado el tope a presupuestar para su unidad.\nEsta unidad tiene un tope de $'+ tope+'.');
	return true;
}
   document.detallePresup.indexi.value = indexi;
   document.detallePresup.accionweb.value = 'presupuestoInterceptorB.RegistrarMatriz';
   document.detallePresup.action = home + '/sipB/presup.html';  
   document.detallePresup.submit();
}	
function registrarPresupEsp(home, indexi){
   document.detallePresup.indexi.value = indexi;
   document.detallePresup.accionweb.value = 'presupuestoInterceptorB.RegistrarMatrizEsp';
   document.detallePresup.action = home + '/sipB/presupEsp.html';  
   document.detallePresup.submit();
}	
function manual(valor, largo){

if(valor == 1){
    document.getElementById('texto1').style.display='';
    document.getElementById('texto2').style.display='none';
     } 
if (valor == 2){
	document.getElementById('texto1').style.display='none';
	document.getElementById('texto2').style.display='';
   } 
 
} 
function importarPlantilla(home){
	alert("EN CONSTRUCCION...");

	document.detallePresup.action = home + '/sipB/uploadxls.html';
	document.detallePresup.target = '_top';
	document.formUploads.submit();
}
/*item 41*/

function cargaItemDet(home, mes, sucur, total){

document.detalleItemPresup.action = home+'/sipB/presupItemreqdet.html';
document.detalleItemPresup.mes.value = mes;
document.detalleItemPresup.sucur.value = sucur;
document.detalleItemPresup.total.value = total;
document.detalleItemPresup.accionweb.value = 'presupuestoInterceptorB.cargaDetalleItemReq';
document.detalleItemPresup.target = '_top';
document.detalleItemPresup.submit();
}
function volverMatriz(home){

document.presupItemReq.action = home+'/sipB/presup.html';
document.presupItemReq.accionweb.value = 'presupuestoInterceptorB.actualizaMatriz';
document.presupItemReq.target = '_top';
document.presupItemReq.submit();
}




/*  presupESP*/
function cargarEscogeItemEsp(valor){
	
	cargarItemEsp(valor,document.detallePresup.item.value);
	return;
}
function cargarItemEsp(home,item){
	document.detallePresup.accionweb.value = 'presupuestoInterceptorB.cargaPresupItemEsp';
	document.detallePresup.itedoc.value = item;
	document.detallePresup.action = home+'/sipB/presupItemEsp.html';
	document.detallePresup.submit();
}
/**/



/* OTROS INGRESOS */


function asignaObservacion(tope){
    if(tope > 0){
    for (i=1; i<=tope; i++){
     if(document.getElementById('observacion'+i).value.length <= 40){
      document.getElementById('comen1'+i).value = document.getElementById('observacion'+i).value;
      document.getElementById('comen2'+i).value = "";
      document.getElementById('comen3'+i).value = "";
      document.getElementById('comen4'+i).value = "";
      document.getElementById('comen5'+i).value = "";
    } else {
    		document.getElementById('comen1'+i).value = document.getElementById('observacion'+i).value.substr(0,40);
   		 	if(document.getElementById('observacion'+i).value.length <= 80){
   		     	document.getElementById('comen2'+i).value = document.getElementById('observacion'+i).value.substr(40);
   		     	document.getElementById('comen3'+i).value = "";
      			document.getElementById('comen4'+i).value = "";
      			document.getElementById('comen5'+i).value = "";
      			
   		 	} else {    
   					document.getElementById('comen2'+i).value = document.getElementById('observacion'+i).value.substr(40,40);
            		if(document.getElementById('observacion'+i).value.length <= 120){
               			document.getElementById('comen3'+i).value = document.getElementById('observacion'+i).value.substr(80);
               			document.getElementById('comen4'+i).value = "";
      					document.getElementById('comen5'+i).value = "";
      					
            		} else {   
   			      			document.getElementById('comen3'+i).value = document.getElementById('observacion'+i).value.substr(80,40);
   			      			if(document.getElementById('observacion'+i).value.length <= 160){
               					document.getElementById('comen4'+i).value = document.getElementById('observacion'+i).value.substr(120);
               					document.getElementById('comen5'+i).value = "";
      						
            	  	        } else {
            	  	        		document.getElementById('comen4'+i).value = document.getElementById('observacion'+i).value.substr(120, 40);
            	  	        		if(document.getElementById('observacion'+i).value.length <= 200){
               							document.getElementById('comen5'+i).value = document.getElementById('observacion'+i).value.substr(160);
               						
            	  	        		} else {
            	  	        				document.getElementById('comen5'+i).value = document.getElementById('observacion'+i).value.substr(160, 40);
            	  	        		       }
            	  	        		}
   			      			}
   					}      
   			}   
	}
	}
}
function asignaObservacionNomina(tope){
    if(tope > 0){
    for (i=1; i<=tope; i++){
     if(document.getElementById('observacion'+i).value.length <= 40){
      document.getElementById('comen1'+i).value = document.getElementById('observacion'+i).value;
      document.getElementById('comen2'+i).value = "";
      document.getElementById('comen3'+i).value = "";
      document.getElementById('comen4'+i).value = "";
      document.getElementById('comen5'+i).value = "";
      document.getElementById('comen6'+i).value = "";
   } else {
    		document.getElementById('comen1'+i).value = document.getElementById('observacion'+i).value.substr(0,40);
   		 	if(document.getElementById('observacion'+i).value.length <= 80){
   		     	document.getElementById('comen2'+i).value = document.getElementById('observacion'+i).value.substr(40);
   		     	document.getElementById('comen3'+i).value = "";
      			document.getElementById('comen4'+i).value = "";
      			document.getElementById('comen5'+i).value = "";
      			document.getElementById('comen6'+i).value = "";
      			
   		 	} else {    
   					document.getElementById('comen2'+i).value = document.getElementById('observacion'+i).value.substr(40,40);
            		if(document.getElementById('observacion'+i).value.length <= 120){
               			document.getElementById('comen3'+i).value = document.getElementById('observacion'+i).value.substr(80);
               			document.getElementById('comen4'+i).value = "";
      					document.getElementById('comen5'+i).value = "";
      					document.getElementById('comen6'+i).value = "";
            		} else {   
   			      			document.getElementById('comen3'+i).value = document.getElementById('observacion'+i).value.substr(80,40);
   			      			if(document.getElementById('observacion'+i).value.length <= 160){
               					document.getElementById('comen4'+i).value = document.getElementById('observacion'+i).value.substr(120);
               					document.getElementById('comen5'+i).value = "";
      						    document.getElementById('comen6'+i).value = "";
            	  	        } else {
            	  	        		document.getElementById('comen4'+i).value = document.getElementById('observacion'+i).value.substr(120, 40);
            	  	        		if(document.getElementById('observacion'+i).value.length <= 200){
               							document.getElementById('comen5'+i).value = document.getElementById('observacion'+i).value.substr(160);
               						    document.getElementById('comen6'+i).value = "";
            	  	        		} else {
            	  	        				document.getElementById('comen5'+i).value = document.getElementById('observacion'+i).value.substr(160, 40);
            	  	        		        if(document.getElementById('observacion'+i).value.length <= 240)
            	  	        		           document.getElementById('comen6'+i).value = document.getElementById('observacion'+i).value.substr(200);
            	  	        		           else
            	  	        		           document.getElementById('comen6'+i).value = document.getElementById('observacion'+i).value.substr(200,40);
            	  	        
            	  	        		       
            	  	        		       }
            	  	        		}
   			      			}
   					}      
   			}   
	}
	asigna(3); 

	if(document.getElementById('motiv').value.length > 240){
	      if(document.getElementById('motiv').value.length <= 280){
          	document.getElementById('tipcon').value = document.getElementById('motiv').value.substr(240);
   			document.getElementById('area1').value = "";
			document.getElementById('area2').value = "";
			document.getElementById('desite').value = "";
          }else {
                document.getElementById('tipcon').value = document.getElementById('motiv').value.substr(240,40);
            	if(document.getElementById('motiv').value.length <= 320){
            	  	document.getElementById('area1').value = document.getElementById('motiv').value.substr(280);
  					document.getElementById('area2').value = "";
					document.getElementById('desite').value = "";
            	} else {
            	  	document.getElementById('area1').value = document.getElementById('motiv').value.substr(280,40);
            	  	if(document.getElementById('motiv').value.length <= 360){
            	  	   	document.getElementById('area2').value = document.getElementById('motiv').value.substr(320);
        				document.getElementById('desite').value = "";
            	  	}else {
            	  	   document.getElementById('area2').value = document.getElementById('motiv').value.substr(320,40);
            	  	   if(document.getElementById('motiv').value.length <= 400)
            	  	      document.getElementById('desite').value = document.getElementById('motiv').value.substr(360);
            	  	    else
            	  	      document.getElementById('desite').value = document.getElementById('motiv').value.substr(360,40);
            	  	        		         		
            	  	    }
            	  	  }		
           }
    } else {
    	document.getElementById('tipcon').value = "";
		document.getElementById('area1').value = "";
		document.getElementById('area2').value = "";
		document.getElementById('desite').value = "";
    } 
	/*    if(document.getElementById('motiv').value.length <= 40){
      document.getElementById('motiv1').value = document.getElementById('motiv').value;
      document.getElementById('motiv2').value = "";
      document.getElementById('motiv3').value = "";
      document.getElementById('motiv4').value = "";
      document.getElementById('motiv5').value = "";
      document.getElementById('motiv6').value = "";
      
   } else {
    		document.getElementById('motiv1').value = document.getElementById('motiv').value.substr(0,40);
   		 	if(document.getElementById('motiv').value.length <= 80){
   		     	document.getElementById('motiv2').value = document.getElementById('motiv').value.substr(40);
   		     	document.getElementById('motiv3').value = "";
     			document.getElementById('motiv4').value = "";
      			document.getElementById('motiv5').value = "";
      			document.getElementById('motiv6').value = "";
   		 	} else {    
   					document.getElementById('motiv2').value = document.getElementById('motiv').value.substr(40,40);
            		if(document.getElementById('motiv').value.length <= 120){
               			document.getElementById('motiv3').value = document.getElementById('motiv').value.substr(80);
               			document.getElementById('motiv4').value = "";
      					document.getElementById('motiv5').value = "";
      					document.getElementById('motiv6').value = "";
            		} else {   
   			      			document.getElementById('motiv3').value = document.getElementById('motiv').value.substr(80,40);
   			      			if(document.getElementById('motiv').value.length <= 160){
               					document.getElementById('motiv4').value = document.getElementById('motiv').value.substr(120);
            	  	            document.getElementById('motiv5').value = "";
      							document.getElementById('motiv6').value = "";
      							} else {
            	  	        		document.getElementById('motiv4').value = document.getElementById('motiv').value.substr(120, 40);
            	  	        		if(document.getElementById('motiv').value.length <= 200){
               							document.getElementById('motiv5').value = document.getElementById('motiv').value.substr(160);
               							document.getElementById('motiv6').value = "";
            	  	        		} else {
            	  	        				document.getElementById('motiv5').value = document.getElementById('motiv').value.substr(160, 40);
            	  	        				if(document.getElementById('motiv').value.length <= 240)
            	  	        		        	document.getElementById('motiv6').value = document.getElementById('motiv').value.substr(200);
            	  	        		        else {
            	  	        		            document.getElementById('motiv6').value = document.getElementById('motiv').value.substr(200,40);
            	  	        		            if(document.getElementById('motiv').value.length <= 280)
            	  	        		        		document.getElementById('tipcon').value = document.getElementById('motiv').value.substr(240);
            	  	        		            else {
            	  	        		          		document.getElementById('tipcon').value = document.getElementById('motiv').value.substr(240,40);
            	  	        		                if(document.getElementById('motiv').value.length <= 320)
            	  	        		        			document.getElementById('area1').value = document.getElementById('motiv').value.substr(280);
            	  	        		                else {
            	  	        		                	document.getElementById('area1').value = document.getElementById('motiv').value.substr(280,40);
            	  	        		                	if(document.getElementById('motiv').value.length <= 360)
            	  	        		        				document.getElementById('area2').value = document.getElementById('motiv').value.substr(320);
            	  	        		         			else {
            	  	        		         				document.getElementById('area2').value = document.getElementById('motiv').value.substr(320,40);
            	  	        		                		if(document.getElementById('motiv').value.length <= 400)
            	  	        		        				   document.getElementById('desite').value = document.getElementById('motiv').value.substr(360);
            	  	        		        				else
            	  	        		        				   document.getElementById('desite').value = document.getElementById('motiv').value.substr(360,40);
            	  	        		         		
            	  	        		         			}
            	  	        		         		}		
            	  	        		            }
            	  	        				}
            	  	        			}
            	  	        		}
   			      			}
   					}      
   			}  */
	
	}

}
function registrarOtrosIng( tope){
    asignaObservacion(tope);
	document.presupOtrosIng.submit();
}

function suma(largo){
 var total = 0;
  for(i=1;i<=largo;i++){
      valor = unformatNumber(document.getElementById('valuni'+i).value);    
      if(valor == "")
         valorTot = 0;
      else valorTot = parseInt(valor);   
      total = total +  valorTot;
   }
  
  document.getElementById('totalIngreso').innerHTML = formatNumber(total,'$');
}

function imprimirOtrosIng(){
	document.impOtrosIng.submit();	
}



/* REMUNERACION */

function registrarRemuneracion(control, indexi) {
//if (validaRemuneracionVariable()) { jordan solicita sacarlo 5/11/2018
	document.presupRemuneracion.accionweb.value = "presupuestoInterceptorB.registraPresupRemuneracion";
    document.presupRemuneracion.indexi.value = indexi;
	document.presupRemuneracion.control.value = control;
	document.presupRemuneracion.submit();
// }	
 //else alert("Todos los valores deben ser ingresados y deben ser mayores que cero.");
}

function validaRemuneracionVariable(){
    if (document.presupRemuneracion.p_pres05.value <= 0 ||
        document.presupRemuneracion.p_pres06.value <= 0 ||
        document.presupRemuneracion.p_pres07.value <= 0 ||
        document.presupRemuneracion.p_pres08.value <= 0 ||
        document.presupRemuneracion.p_pres09.value <= 0 ||
        document.presupRemuneracion.p_pres10.value <= 0 ||
        document.presupRemuneracion.p_pres11.value <= 0 ||
        document.presupRemuneracion.p_pres12.value <= 0 ||
        document.presupRemuneracion.p_valr11.value <= 0 ||
        document.presupRemuneracion.p_valr12.value <= 0 ||
        document.presupRemuneracion.p_valr21.value <= 0 ||
        document.presupRemuneracion.p_valr22.value <= 0 ||
        document.presupRemuneracion.p_valr31.value <= 0 ) {
       return false;
	}
	return true;
}


/* NOMINA */

function registrarPresupNomina(tope, indexi){
	if (validaNomina()) {
	    asignaObservacionNomina(tope);
	    document.detallePresupNom.indexi.value = indexi;
		document.detallePresupNom.accionweb.value = 'presupuestoInterceptorB.registraPresupNomina';
		document.detallePresupNom.submit();
	}
}
function generaInforme(accion, accionweb){
   
    document.exportarPresup.accionweb.value = accionweb;
    document.exportarPresup.action = accion;
	document.exportarPresup.submit();
}

function generaInformePDF(accion, accionweb){
   	document.exportarPresup.target = "ventana";
    var par = 'toolbar=no,status=no,menubar=no,scrollbars=yes,resizable=yes';
    var w = Centrar03(document.exportarPresup.target,750,950,par);
   
    document.exportarPresup.accionweb.value = accionweb;
    document.exportarPresup.action = accion;
	document.exportarPresup.submit();
}

function validaNomina(){
    if (document.detallePresupNom != null) {
		var el = document.detallePresupNom.elements; 
		for (var i = 0 ; i < el.length ; ++i) { 
			if (el[i].type == "radio") { 
				var radiogroup = el[el[i].name]; 
				var itemchecked = false; 
				for (var j = 0 ; j < radiogroup.length ; ++j) { 
					if (radiogroup[j].checked) { 
						itemchecked = true; 
						break; 
					} 
				} 
				if (!itemchecked) {  
					i=0; 
					break; 
				} 
			} 
		} 
		if (i>0) { 
			return true; 
		} else { 
			if(confirm(" NO HA REALIZADO LA VALIDACION\nCOMPLETA DE LA NOMINA.\n\n �Desea igualmente registrar?")) 
				return true;
			else
				return false; 
		}
	}
	else {
		return true;
	}
}
/* mensajes informativos a jefes de carrera y encargado de Sede*/

function mensajeRegDefinitivo(){
		if(confirm(" ESTA OPCI�N REGISTRAR� LOS DATOS INGRESADOS EN PANTALLA, luego de Aceptar no podr� modifcarlos.\n\nADVERTENCIA:\n El REGISTRAR DEFINITIVAMENTE significa que se registrar� y finalizar� el ingreso para la Unidad\nde la Presupuestaci�n Corriente, Remuneraciones Variables, Otros Negocios y de Validar N�mina.\nPor Favor ANTES DE REGISTRAR, VERIFIQUE si ha realizado un REGISTRO TEMPORAL para esta\nUnidad en todas las opciones.\n\n �Desea igualmente registrar?")) 
				return true;
			else
				return false; 

}
function mensajeRegAprobado(){
		if(confirm(" ESTA OPCI�N REGISTRAR� LOS DATOS INGRESADOS EN PANTALLA.\n\nADVERTENCIA:\n\n El APROBAR significa que se registrar� y finalizar� el proceso de presupuestaci�n para la Unidad\nde la Presupuestaci�n Corriente, Remuneraciones Variables, Otros Negocios y de Validar N�mina.\nUna vez que est�n todas las Unidades Aprobadas, se aprueba autom�ticamente Otros Negocios.\n\nPor Favor ANTES DE REGISTRAR, VERIFIQUE si ha registrado el ingreso para esta Unidad en las otras opciones.\n\n �Desea igualmente registrar?")) 
				return true;
			else
				return false; 

}
function mensajeRegRechazado(){
		if(confirm(" ESTA OPCI�N REGISTRAR� LOS DATOS INGRESADOS EN PANTALLA.\n\nADVERTENCIA:\n El RECHAZAR significa que se registrar� y permitir� al Jefe de Carrera volver a modificar la presupuestaci�n para la Unidad\nde la Presupuestaci�n Corriente, Remuneraciones Variables, Otros Negocios y de Validar N�mina.\nPor Favor ANTES DE REGISTRAR, VERIFIQUE si ha registrado el ingreso para esta\nUnidad en las otras opciones.\n\n �Desea igualmente registrar?")) 
				return true;
			else
				return false; 

}

/*para validaci�n de rut*/
function checkRutField(rut, f)
{
	var tmpstr = "";

	for ( i=0; i < rut.length ; i++ )
		if ( rut.charAt(i) != ' ' && rut.charAt(i) != '.' && rut.charAt(i) != '-' )
			tmpstr = tmpstr + rut.charAt(i);
	rut = tmpstr;
	largo = rut.length;

	tmpstr = "";
	for ( i=0; rut.charAt(i) == '0' ; i++ );
		for (; i < rut.length ; i++ )
			tmpstr = tmpstr + rut.charAt(i);
	rut = tmpstr;
	largo = rut.length;

	if ( largo < 2 )
	{
		alert("Debe ingresar el RUT completo.");
		f.rut_aux.focus();
		f.rut_aux.select();
		return false;
	}

	for (i=0; i < largo ; i++ )
	{
		if ( rut.charAt(i) != "0" && rut.charAt(i) != "1" && rut.charAt(i) !="2" && rut.charAt(i) != "3" && rut.charAt(i) != "4" && rut.charAt(i) !="5" && rut.charAt(i) != "6" && rut.charAt(i) != "7" && rut.charAt(i) !="8" && rut.charAt(i) != "9" && rut.charAt(i) !="k" && rut.charAt(i) != "K" )
		{
			alert("El RUT no es v�lido.");
                        error = 1;
            f.rut_aux.value = "";            
			f.rut_aux.focus();
			f.rut_aux.select();
			return false;
		}
	}
	var invertido = "";
	for ( i=(largo-1),j=0; i>=0; i--,j++ )
		invertido = invertido + rut.charAt(i);
	var drut = "";
	drut = drut + invertido.charAt(0);
	drut = drut + '-';
	cnt = 0;
	for ( i=1,j=2; i<largo; i++,j++ )
	{
		if ( cnt == 3 )
		{
			drut = drut + '.';
			j++;
			drut = drut + invertido.charAt(i);
			cnt = 1;
		}
		else
		{
			drut = drut + invertido.charAt(i);
			cnt++;
		}
	}
	invertido = "";
	for ( i=(drut.length-1),j=0; i>=0; i--,j++ )
		invertido = invertido + drut.charAt(i);
	if(f.rut_aux)	
		f.rut_aux.value = invertido;


        if ( checkDV(rut, f) )
	 return true;
	return false;

}




function checkRutFieldAut(rut, f)
{
	var tmpstr = "";

	for ( i=0; i < rut.length ; i++ )
		if ( rut.charAt(i) != ' ' && rut.charAt(i) != '.' && rut.charAt(i) != '-' )
			tmpstr = tmpstr + rut.charAt(i);
	rut = tmpstr;
	largo = rut.length;

	tmpstr = "";
	for ( i=0; rut.charAt(i) == '0' ; i++ );
		for (; i < rut.length ; i++ )
			tmpstr = tmpstr + rut.charAt(i);
	rut = tmpstr;
	largo = rut.length;

	if ( largo < 2 )
	{
		alert("Debe ingresar el RUT completo.");
		f.rut_aut.focus();
		f.rut_aut.select();
		return false;
	}

	for (i=0; i < largo ; i++ )
	{
		if ( rut.charAt(i) != "0" && rut.charAt(i) != "1" && rut.charAt(i) !="2" && rut.charAt(i) != "3" && rut.charAt(i) != "4" && rut.charAt(i) !="5" && rut.charAt(i) != "6" && rut.charAt(i) != "7" && rut.charAt(i) !="8" && rut.charAt(i) != "9" && rut.charAt(i) !="k" && rut.charAt(i) != "K" )
		{
			alert("El RUT no es v�lido.");
                        error = 1;
            f.rut_aut.value = "";            
			f.rut_aut.focus();
			f.rut_aut.select();
			return false;
		}
	}
	var invertido = "";
	for ( i=(largo-1),j=0; i>=0; i--,j++ )
		invertido = invertido + rut.charAt(i);
	var drut = "";
	drut = drut + invertido.charAt(0);
	drut = drut + '-';
	cnt = 0;
	for ( i=1,j=2; i<largo; i++,j++ )
	{
		if ( cnt == 3 )
		{
			drut = drut + '.';
			j++;
			drut = drut + invertido.charAt(i);
			cnt = 1;
		}
		else
		{
			drut = drut + invertido.charAt(i);
			cnt++;
		}
	}
	invertido = "";
	for ( i=(drut.length-1),j=0; i>=0; i--,j++ )
		invertido = invertido + drut.charAt(i);
	if(f.rut_aut)	
		f.rut_aut.value = invertido;


        if ( checkDVAut(rut, f) )
	 return true;
	return false;

}

//

function checkDVAut( crut, f )
{
	largo = crut.length;
	if ( largo < 2 )
	{
		alert("Debe ingresar el RUT completo.");
		f.rut_aut.focus();
		f.rut_aut.select();
		return false;
	}
	if ( largo > 2 )
		rut = crut.substring(0, largo - 1);
	else
		rut = crut.charAt(0);
	dv = crut.charAt(largo-1);
	checkCDVAut( dv, f );
	if ( rut == null || dv == null )
		return 0;
	var dvr = '0';
	suma = 0;
	mul = 2;
	for (i= rut.length -1 ; i >= 0; i--)
	{
		suma = suma + rut.charAt(i) * mul;
		if (mul == 7)
			mul = 2;
		else
			mul++;
	}
	res = suma % 11;
	if (res==1)
		dvr = 'k';
	else if (res==0)
		dvr = '0';
	else
	{
		dvi = 11-res;
		dvr = dvi + "";
	}
	if ( dvr != dv.toLowerCase() )
	{
		alert("EL RUT es incorrecto.");
		f.rut_aux.focus();
		// f.rut_aux.value = "";
		return false;
	}
	return true;
}


///////////////////////////////////////////////////////////////

function checkDV( crut, f )
{
	largo = crut.length;
	if ( largo < 2 )
	{
		alert("Debe ingresar el RUT completo.");
		f.rut_aux.focus();
		f.rut_aux.select();
		return false;
	}
	if ( largo > 2 )
		rut = crut.substring(0, largo - 1);
	else
		rut = crut.charAt(0);
	dv = crut.charAt(largo-1);
	checkCDV( dv, f );
	if ( rut == null || dv == null )
		return 0;
	var dvr = '0';
	suma = 0;
	mul = 2;
	for (i= rut.length -1 ; i >= 0; i--)
	{
		suma = suma + rut.charAt(i) * mul;
		if (mul == 7)
			mul = 2;
		else
			mul++;
	}
	res = suma % 11;
	if (res==1)
		dvr = 'k';
	else if (res==0)
		dvr = '0';
	else
	{
		dvi = 11-res;
		dvr = dvi + "";
	}
	if ( dvr != dv.toLowerCase() )
	{
		alert("EL RUT es incorrecto.");
		f.rut_aux.focus();
		// f.rut_aux.value = "";
		return false;
	}
	return true;
}
function checkR(r, texto)
{
	rut = r.value;
	var tmpstr = "";
	for ( i=0; i < rut.length ; i++ )
		if ( rut.charAt(i) != ' ' && rut.charAt(i) != '.' && rut.charAt(i) != '-' )
			tmpstr = tmpstr + rut.charAt(i);
	rut = tmpstr;

	largo = rut.length;

	tmpstr = "";
	for ( i=0; rut.charAt(i) == '0' ; i++ );
		for (; i < rut.length ; i++ )
			tmpstr = tmpstr + rut.charAt(i);
	rut = tmpstr;
	largo = rut.length;

	if ( largo < 2 )
	{
          alert("Debe ingresar el " + texto + " completo.");
          r.focus();
          r.value = "";
          return false;
        }
        for (i=0; i < largo ; i++ )
        {
          if ( rut.charAt(i) != "0" && rut.charAt(i) != "1" && rut.charAt(i) !="2" && rut.charAt(i) != "3" && rut.charAt(i) != "4" && rut.charAt(i) !="5" && rut.charAt(i) != "6" && rut.charAt(i) != "7" && rut.charAt(i) !="8" && rut.charAt(i) != "9" && rut.charAt(i) !="k" && rut.charAt(i) != "K" )
          {
          alert("El " + texto + " no es v�lido.");
          r.focus();
          r.value = "";
          return false;
        }
        }
        var invertido = "";
        for ( i=(largo-1),j=0; i>=0; i--,j++ )
          invertido = invertido + rut.charAt(i);
        var drut = "";
        drut = drut + invertido.charAt(0);
        drut = drut + '-';
        cnt = 0;

        if ( texto.substring(0,3) == 'RUT') {
          for ( i=1,j=2; i<largo; i++,j++ )
          {
            if ( cnt == 3 )
            {
            drut = drut + '.';
            j++;
            drut = drut + invertido.charAt(i);
            cnt = 1;
          }
          else
          {
            drut = drut + invertido.charAt(i);
            cnt++;
          }
          }
        }

        if ( texto == 'ROL') {
          k = 1;
          for ( i=1,j=2; i<largo; i++,j++ )
          {
            if ( (k == 1 && cnt == 3) || (k == 2 && cnt == 2) )
            {
            drut = drut + '.';
            j++;
            drut = drut + invertido.charAt(i);
            cnt = 1;
            k++;
          }
          else
          {
            drut = drut + invertido.charAt(i);
            cnt++;
          }
          }
        }
	invertido = "";
	for ( i=(drut.length-1),j=0; i>=0; i--,j++ )
		invertido = invertido + drut.charAt(i);
        // alert(invertido);
	r.value = invertido;
	if ( checkDVR(rut, r, texto) )
		return true;
	return false;
}

///////////////////////////////////////////////////////////////

function checkDVR( v, r, texto )
{
	crut = v;
	largo = crut.length;
	if ( largo < 2 )
	{
		alert("Debe ingresar el " + texto + " completo.");
		r.focus();
		r.value = "";
		return false;
	}
	if ( largo > 2 )
		rut = crut.substring(0, largo - 1);
	else
		rut = crut.charAt(0);
	dv = crut.charAt(largo-1);
	checkCDVR( dv, r );
	if ( rut == null || dv == null )
		return 0;
	var dvr = '0';
	suma = 0;
	mul = 2;
	for (i= rut.length -1 ; i >= 0; i--)
	{
		suma = suma + rut.charAt(i) * mul;
		if (mul == 7)
			mul = 2;
		else
			mul++;
	}
	res = suma % 11;
	if (res==1)
		dvr = 'k';
	else if (res==0)
		dvr = '0';
	else
	{
		dvi = 11-res;
		dvr = dvi + "";
	}

	if ( dvr != dv.toLowerCase() )
	{
		alert("El " + texto + " es incorrecto.");
		r.focus();
		r.value = "";
		return false;
	}
	return true;
}

///////////////////////////////////////////////////////////////

function checkCDVR( dvr, r )
{
	dv = dvr + "";
	if ( dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4' && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9' && dv != 'k'  && dv != 'K')
	{
		alert("Debe ingresar un d�gito verificador v�lido.");
		r.focus();
		r.value = "";
		return false;
	}
	return true;
}
function checkCDV( dvr, f )
{
	dv = dvr + "";
	if ( dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4' && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9' && dv != 'k'  && dv != 'K')
	{
		alert("Debe ingresar un d�gito verificador v�lido.");
		f.rut_aux.focus();
		f.rut_aux.select();
		return false;
	}
	return true;
}
function checkCDVAut( dvr, f )
{
	dv = dvr + "";
	if ( dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4' && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9' && dv != 'k'  && dv != 'K')
	{
		alert("Debe ingresar un d�gito verificador v�lido.");
		f.rut_aut.focus();
		f.rut_aut.select();
		return false;
	}
	return true;
}
function noenter() {
  return !(window.event && window.event.keyCode == 13);
}
function enter(e){

  if (window.event) {
    if (window.event.keyCode == 13)
      validar(document.login_form);
  }
  else {
    if (e.which == 13)
      validar(document.login_form);
  }
}
function enterApertura(e){

  if (window.event) {
    if (window.event.keyCode == 13)
      cargarIngresoCuenta();
  }
  else {
    if (e.which == 13)
      cargarIngresoCuenta();
  }
}
function enterAutoriza(e){

  if (window.event) {
    if (window.event.keyCode == 13)
      validarAutoriza(document.login_form);
  }
  else {
    if (e.which == 13)
      validarAutoriza(document.login_form);
  }
}

function validarAutoriza(form){

  if(trim(form.rut_aut.value)==""){
    alert("Debe ingresar el RUT del Autorizador.");
    form.rut_aut.focus();
    return false;
  }
 
  if (!valida_rut2(form.rut_aut, form.rutnumAut, form.dvrutAut, 'RUT')) {
    form.rut_aut.focus();
    return false;
  }

 cargaLoginAut();

return;
}

function enterRutDoc(e, f){

  if (window.event) {
    if (window.event.keyCode == 13)
      validarRut(f);
  }
  else {
    if (e.which == 13)
      validarRut(f);
  }
}

function validarRut(form){

  if(trim(form.rut.value)!=""){
   if (!valida_rut2(form.rut, form.rutnumDoc, form.dvrutDoc, 'RUT')) {
    form.rut.focus();
    return false;
  }
  }

 cargaRut();

return;
}



function validar(form){

  if(trim(form.rut_aux.value)==""){
    alert("Debe ingresar el RUT.");
    form.rut_aux.focus();
    return false;
  }
 
  if (!valida_rut2(form.rut_aux, form.rutnum, form.dvrut, 'RUT')) {
    form.rut_aux.focus();
    return false;
  }

 cargaLogin();

  //form.submit();
}

function validarBeneficiario(form){

  if(trim(form.rut_aux.value)==""){
    alert("Debe ingresar el RUT.");
    form.rut_aux.focus();
    return false;
  }
 
  if (!valida_rut2(form.rut_aux, form.rutnum, form.dvrut, 'RUT')) {
    form.rut_aux.focus();
    return false;
  }

cargarPago();

  //form.submit();
}
function enterSolicitud(e){

  if (window.event) {
    if (window.event.keyCode == 13)
      validarSolicitud(document.formSolicitudCuenta);
  }
  else {
    if (e.which == 13)
      validarSolicitud(document.formSolicitudCuenta);
  }
}

function validarSolicitud(form){

  if(trim(form.rut_aux.value)==""){
    alert("Debe ingresar el RUT.");
    form.rut_aux.focus();
    return false;
  }
 
  if (!valida_rut2(form.rut_aux, form.rutnum, form.dvrut, 'RUT')) {
    form.rut_aux.focus();
    return false;
  }

 cargaSolicitud();

  //form.submit();
}

function valida_rut2(r, r2, d2, texto) {
        rut_val = r.value;

        if ( !checkR(r, texto) )
        {
                return false;
        }

        var tmpstr = "";
        for ( i=0; i < rut_val.length ; i++ )
                if ( rut_val.charAt(i) != ' ' && rut_val.charAt(i) != '.' && rut_val.charAt(i) != '-' )
                        tmpstr = tmpstr + rut_val.charAt(i);
        rut_val = tmpstr;
        rut_valor = rut_val.substring(0,rut.length);
        if ( rut_valor >= 100000000)
        {
                alert( "El " + texto + " no es v�lido.");
                r.value = "";
                r.focus();
                return false;
        }
        r2.value = rut_val.substring(0,rut.length);
        d2.value = rut_val.substring(rut.length,rut.length+1);
        if (d2.value == "k") d2.value = "K";
        return true;
}

///////////////////////////////////////////////////////////////

function cerrar(){
	window.close();
}

function printweb(){
	window.print();
}  

function volver(){
	window.history.go(-1);
} 

function cargarPantalla(pag, home,fecMov, opcion){    
    if (pag == "") alert("EN CONSTRUCCION ...");
    else {
        document.form_detalle.fecmov.value =  fecMov;
        document.form_detalle.opcion.value = opcion;
        document.form_detalle.accionweb.value = "presupuestoInterceptorB.cargaDocumentoDetalle";
    	document.form_detalle.action = home + "/sipB/inc/" + pag + ".html";    
    	document.form_detalle.target = "_self";
    	document.form_detalle.submit();
    }
}
function cargarPantallaMCI(pag, home,fecMov, opcion, tipope,codBan, numche, rutide, digide, tipcue){    
    if (pag == "") alert("EN CONSTRUCCION ...");
    else {
        document.form_detalle.fecmov.value =  fecMov;
        document.form_detalle.opcion.value = opcion;
        document.form_detalle.tipope.value = tipope;
        document.form_detalle.codBan.value = codBan;
        document.form_detalle.numche.value = numche;
        document.form_detalle.rutide.value = rutide;
        document.form_detalle.digide.value = digide;
        document.form_detalle.tipcue.value = tipcue;
        document.form_detalle.accionweb.value = "presupuestoInterceptorB.cargaDocumentoMCI";
    	document.form_detalle.action = home + "/sipB/inc/" + pag + ".html";    
    	document.form_detalle.target = "_self";
    	document.form_detalle.submit();
    }
    
}
function cargarPantallaCMD(pag, home, opcion, numcom, cajero){    
    if (pag == "") alert("EN CONSTRUCCION ...");
    else {
        document.form_detalle.numcom.value =  numcom;
        document.form_detalle.opcion.value = opcion;
        document.form_detalle.cajero.value = cajero;
        document.form_detalle.accionweb.value = "presupuestoInterceptorB.cargaDocumentoDetalle";
    	document.form_detalle.action = home + "/sipB/inc/" + pag + ".html";    
    	document.form_detalle.target = "_self";
    	document.form_detalle.submit();
    }
    
}

function escondeBloque(idBloque, opcion){
  var accion = 'block';
  if (opcion == 1) accion = 'none';
  if (document.layers) document.layers[idBloque].display = accion;
  else if (document.all) document.all[idBloque].style.display = accion;
  else if (document.getElementById && document.getElementById(idBloque) != null) document.getElementById(idBloque).style.display = accion;

}

function nuevoCliente(action){		
	document.login_form.action = action;
    document.login_form.accionweb.value = "valePagoInterceptor.cargaCliente";
    document.login_form.submit();	
}

function nuevoClienteFacturacion(action){		
	document.login_form.action = action;
    document.login_form.accionweb.value = "facturacionInterceptor.cargaCliente";
    document.login_form.submit();	
}

function registrarCliente(action){ 
    if(document.login_form.nombre.value == ""){
       alert('Debe ingresar nombre del Cliente');
       document.login_form.nombre.focus();
       return false;
       }
  if(document.login_form.direccion.value == ""){
       alert('Debe ingresar nombre de la Direcci&#243;n');
       document.login_form.direccion.focus();
       return false;
       }
   if(document.login_form.comuna.value == ""){
       alert('Debe ingresar nombre de la Comuna');
       document.login_form.comuna.focus();
       return false;
       }
   if(document.login_form.ciudad.value == ""){
       alert('Debe ingresar nombre de la Ciudad');
       document.login_form.ciudad.focus();
       return false;
       }
   if(document.login_form.codBan.value > 0 &&
      document.login_form.cueBan.value == ""){
       alert('Al seleccionar cuenta del Banco debe ingresar n&#250;mero de la cuenta bancaria.');
       document.login_form.cueBan.focus();
       return false;
       }
    checkRutField(document.login_form.rut_aux.value, document.login_form);   
    document.login_form.action = action;
    document.login_form.accionweb.value = "valePagoInterceptorB.registraCliente";
    document.login_form.submit();
    
   return true;    
}

function registrarClienteFacturacion(action){ 
    if(document.login_form.nombre.value == ""){
       alert('Debe ingresar nombre del Cliente');
       document.login_form.nombre.focus();
       return false;
       }
  if(document.login_form.direccion.value == ""){
       alert('Debe ingresar nombre de la Direcci�n');
       document.login_form.direccion.focus();
       return false;
       }
   if(document.login_form.comuna.value == ""){
       alert('Debe ingresar nombre de la Comuna');
       document.login_form.comuna.focus();
       return false;
       }
   if(document.login_form.ciudad.value == ""){
       alert('Debe ingresar nombre de la Ciudad');
       document.login_form.ciudad.focus();
       return false;
       }
   if(document.login_form.codBan.value > 0 &&
      document.login_form.cueBan.value == ""){
       alert('Al seleccionar cuenta del Banco debe ingresar n&#250;mero de la cuenta bancaria.');
       document.login_form.cueBan.focus();
       return false;
       }
    checkRutField(document.login_form.rut_aux.value, document.login_form);   
    document.login_form.action = action;
    document.login_form.accionweb.value = "facturacionInterceptorB.registraCliente";
    document.login_form.submit();
    
   return true;    
}
/*plan de desarrollo*/
function cargarPlan(home,tipo,form){
	form.accionweb.value = 'presupuestoInterceptorB.cargaControlPlanes';
	form.tipo.value = tipo;
	form.action = home+'/sipB/controlPlanDesarrollo.html';
	form.submit();
}
function cargarEscogeItemPlan(valor){
/* MB modifica 16/10/2015 segun petici�n Jorge Guajardo
 if(document.detallePresup.item.value == 41)
		cargarReqPlan(valor);
	else*/ 
	cargarItemPlan(valor,document.detallePresup.item.value);
	return;
}
function cargarReqPlan(home){
    document.detallePresup.itedoc.value = "41";
    document.detallePresup.accionweb.value = 'presupuestoInterceptorB.cargaPresupRequerimientoPlan';		
	document.detallePresup.action = home+'/sipB/inc/presupItemreqPlan.html';
	document.detallePresup.submit();	
}
function cargarItemPlan(home,item){
	document.detallePresup.accionweb.value = 'presupuestoInterceptorB.cargaPresupItemPlan';
	document.detallePresup.itedoc.value = item;
	document.detallePresup.action = home+'/sipB/presupItemPlan.html';
	document.detallePresup.submit();
}
function registraActividad(home){
if(document.getElementById('motiv') && document.getElementById('motiv').value.length == 0){
       alert('Debe ingresar el Objetivo General del Plan de Desarrollo');
       document.getElementById('motiv').focus();
       return false;
       }
 asigna(3);      
if(document.getElementById('descripcionActiv').value.length == 0){
       alert('Debe ingresar la Descripci�n de la Actividad');
       document.getElementById('descripcionActiv').focus();
       return false;
       }
       if(document.getElementById('objetivoActiv').value.length == 0){
       alert('Debe ingresar el Objetivo de la Actividad');
       document.getElementById('objetivoActiv').focus();
       return false;
       }
       if(document.getElementById('ejeActiv').value == 0){
       alert('Debe seleccionar el Eje estrat�gico Vinculado de la Actividad');
       document.getElementById('ejeActiv').focus();
       return false;
       }
       if(document.getElementById('areaActiv').value == 0){
       alert('Debe ingresar el �rea de acreditaci�n a fortalecer de la Actividad');
       document.getElementById('areaActiv').focus();
       return false;
       }
    document.formPresup.action = home + "/sipB/presupPlan.html";
    document.formPresup.accionweb.value = "presupuestoInterceptorB.registraPresupActividad";
    
    document.formPresup.submit();

}
function asignaActividad(){
  
 par = document.formPresup.actividad.value;
 ind = par.indexOf("@");
 codActividad = par.substring(0,ind);
 nomActividad = par.substring(ind+1);
 document.formPresup.codActividad.value = 	codActividad;
 document.formPresup.nomActividad.value = 	nomActividad;
   	
}


function asignaRut(){
 par = document.getElementById('rutResponsable').value;
 ind = par.indexOf("@");
 rutIde = par.substring(0,ind);
 par = par.substring(ind+1);
 ind = par.indexOf("@");
 digide = par.substring(0,ind); 
 nombre = par.substring(ind+1);
 document.formPresup.rutide.value = 	rutIde;
 document.formPresup.digide.value = 	digide;
 document.formPresup.nombreResponsable.value = 	nombre;
    	
}
 function agregarNuevaActividad(home, nombre){
    document.formPresup.accionweb.value = "presupuestoInterceptorB.agregaActividad";
    document.formPresup.nombre.value = nombre;
    document.formPresup.action = home + "/sipB/inc/presupActividadMod.html";
    
    document.formPresup.target = "ventana";
    var par = 'toolbar=no,status=no,menubar=no,scrollbars=yes,resizable=yes';
    var w = Centrar03(document.formPresup.target,505,750,par);
    document.formPresup.submit();
}
 function modificarActividad(home, nombre){
//alert(document.getElementById('codActividad').value);
  if(document.getElementById('codActividad').value == 0){
        alert('Debe seleccionar el Proyecto.');
        document.getElementById('codActividad').focus;
        return false;
        }
    document.formPresup.accionweb.value = "presupuestoInterceptorB.modificarActividad";
    document.formPresup.nombre.value = nombre;
    document.formPresup.action = home + "/sipB/inc/presupActividadMod.html";
    
    document.formPresup.target = "ventana";
    var par = 'toolbar=no,status=no,menubar=no,scrollbars=yes,resizable=yes';
    var w = Centrar03(document.formPresup.target,505,750,par);
    document.formPresup.submit();
}
function cargaItemDetPlan(home, mes, sucur, total){

document.detalleItemPresup.action = home+'/sipB/presupItemreqdetPlan.html';
document.detalleItemPresup.mes.value = mes;
document.detalleItemPresup.sucur.value = sucur;
document.detalleItemPresup.total.value = total;
document.detalleItemPresup.accionweb.value = 'presupuestoInterceptorB.cargaDetalleItemReqPlan';
document.detalleItemPresup.target = '_top';
document.detalleItemPresup.submit();
}

function registrarPresupPlan(home, indexi){
//alert(document.getElementById('topeUnidad').value+' < '+document.getElementById('totalItems').value);

   document.detallePresup.indexi.value = indexi;
   document.detallePresup.accionweb.value = 'presupuestoInterceptorB.RegistrarMatrizPlan';
   document.detallePresup.action = home + '/sipB/presupPlan.html';  
   document.detallePresup.submit();
}

function volverMatrizPlan(home){

document.presupItemReq.action = home+'/sipB/presupPlan.html';
document.presupItemReq.accionweb.value = 'presupuestoInterceptorB.actualizaMatrizPlan';
document.presupItemReq.target = '_top';
document.presupItemReq.submit();
}
function registrarObservacionPlan(home, indexi){
//alert(document.getElementById('topeUnidad').value+' < '+document.getElementById('totalItems').value);
//alert('registrarObservacionPlan');
   if(document.getElementById('motiv') && document.getElementById('motiv').value.length == 0){
       alert('Debe ingresar el Objetivo General del Plan de Desarrollo');
       document.getElementById('motiv').focus();
       return false;
       }
   asigna(6); 
   document.formPresup.accion.value = "1";
   document.formPresup.accionweb.value = 'presupuestoInterceptorB.registraObservacionPlan';
   document.formPresup.action = home + '/sipB/presupPlan.html';  
   document.formPresup.submit();
}

/*  fin plan de desarrollo*/

/*** Programa Anual */
function asignaObservacionPrograma(tope){
    if(tope > 0){
    for (i=1; i<=tope; i++){
     if(document.getElementById('observacion'+i).value.length <= 40){
      document.getElementById('comen1'+i).value = document.getElementById('observacion'+i).value;
      document.getElementById('comen2'+i).value = "";
      document.getElementById('comen3'+i).value = "";
      document.getElementById('comen4'+i).value = "";
     } else {
         	document.getElementById('comen1'+i).value = document.getElementById('observacion'+i).value.substr(0,40);
   		 	if(document.getElementById('observacion'+i).value.length <= 80){
   		     	document.getElementById('comen2'+i).value = document.getElementById('observacion'+i).value.substr(40);
   		     	document.getElementById('comen3'+i).value = "";
      			document.getElementById('comen4'+i).value = "";
         	} else {    
   					document.getElementById('comen2'+i).value = document.getElementById('observacion'+i).value.substr(40,40);
            		if(document.getElementById('observacion'+i).value.length <= 120){
               			document.getElementById('comen3'+i).value = document.getElementById('observacion'+i).value.substr(80);
               			document.getElementById('comen4'+i).value = "";
      			     					
            		} else {   
   			      			document.getElementById('comen3'+i).value = document.getElementById('observacion'+i).value.substr(80,40);
   			      			if(document.getElementById('observacion'+i).value.length <= 160){
               					document.getElementById('comen4'+i).value = document.getElementById('observacion'+i).value.substr(120);
            	  	        } else {
            	  	        		document.getElementById('comen4'+i).value = document.getElementById('observacion'+i).value.substr(160, 40);
            	  	        		}
   			      			}
   					}      
   			}   
	}
	}
}

/*   validaci�n de Boletas de Honorario*/
function valida(){
/*if(document.formHonorario.sede.value == -1){ VS.20.12.2016*/
/*	    alert('Debe seleccionar Sede.');*/
/*	    document.formHonorario.sede.focus();*/
/*	    document.getElementById('botonRegistrar').style.visibility="visible";*/
/*	    if(document.getElementById('botonRegistrar2')!= null)*/
/*			document.getElementById('botonRegistrar2').style.visibility="visible";*/
/*	    return false;*/
/*	 }*/
	 if(document.formHonorario.fechaBoleta.value == ""){
	    alert('Debe ingresar fecha de la Boleta.');
	    document.formHonorario.fechaBoleta.focus();
	    document.getElementById('botonRegistrar').style.visibility="visible";
	    if(document.getElementById('botonRegistrar2')!= null)
			document.getElementById('botonRegistrar2').style.visibility="visible";
	    return false;
	 }
	 
	 if(document.formHonorario.totalBoleta.value == "" || document.formHonorario.totalBoleta.value == "0"){
	    alert('Debe ingresar Valor total de la Boleta.');
	    document.formHonorario.totalBoleta.focus();
	    document.getElementById('botonRegistrar').style.visibility="visible";
	    if(document.getElementById('botonRegistrar2')!= null)
			document.getElementById('botonRegistrar2').style.visibility="visible";
	    return false;
	 } 

	 if(document.formHonorario.totalAPago.value == ""){
	    alert('Debe ingresar el Valor Total a pagar de la Boleta');
	    document.formHonorario.totalAPago.focus();
	    document.getElementById('botonRegistrar').style.visibility="visible";
	    if(document.getElementById('botonRegistrar2')!= null)
			document.getElementById('botonRegistrar2').style.visibility="visible";
	    return false;
	 }
	 if(document.formHonorario.retencion.value == ""){
	    alert('Debe ingresar el Valor de retenci�n de la Boleta, por defecto debe ser 0.');
	    document.formHonorario.retencion.focus();
	    document.getElementById('botonRegistrar').style.visibility="visible";
	    if(document.getElementById('botonRegistrar2')!= null)
			document.getElementById('botonRegistrar2').style.visibility="visible";
	    return false;
	 }
	 totalBoleta = unformatNumber(document.formHonorario.totalBoleta.value)/1 ;
	 totalAPago = unformatNumber(document.formHonorario.totalAPago.value)/1;
	 retencion = unformatNumber(document.formHonorario.retencion.value)/1 ;
	// alert('totalBoleta: '+totalBoleta+'totalAPago: '+totalAPago+'retencion: '+retencion);
	 if(totalAPago != (totalBoleta - retencion)){
	    alert('Debe revisar los valores de total boleta, retenci�n y total a pagar de la Boleta, ya que no cuadran.');
	    document.formHonorario.totalBoleta.focus();
	    document.getElementById('botonRegistrar').style.visibility="visible";
	    if(document.getElementById('botonRegistrar2')!= null)
			document.getElementById('botonRegistrar2').style.visibility="visible";
	    return false;
	 }

	 if(document.formHonorario.glosa.value == ""){
	    alert('Debe ingresar la descripci\u00f3n de la Boleta de Honorario.');
	    document.formHonorario.glosa.focus();
	    document.getElementById('botonRegistrar').style.visibility="visible";
	    if(document.getElementById('botonRegistrar2')!= null)
			document.getElementById('botonRegistrar2').style.visibility="visible";
	    return false;
	 }
	 return true;
	 }
	  function validaHonorario(){
	  if(document.formSolicitudHonorario.cuentaPresup.value == -1 ){
     	    alert('Debe seleccionar la cuenta que se va a realizar el cargo.');
		    document.formSolicitudHonorario.cuenta.focus();
		    document.getElementById('botonRegistrar').style.visibility="visible";
		    if(document.getElementById('botonRegistrar2')!= null)
		    	document.getElementById('botonRegistrar2').style.visibility="visible";
		    return false;
	    }
	   if(document.formSolicitudHonorario.valor.value == "" ){
     	    alert('Debe ingresar el valor del cargo de la cuenta.');
		    document.formSolicitudHonorario.valor.focus();
		    document.getElementById('botonRegistrar').style.visibility="visible";
		    if(document.getElementById('botonRegistrar2')!= null)
				document.getElementById('botonRegistrar2').style.visibility="visible";
		    return false;
	    }
	    totalAPago = unformatNumber(document.getElementById('totalBoleta').value) / 1;
	    valor = unformatNumber(document.getElementById('valor').value) / 1;
	    
	    
	    
	  //  alert('totalAPago: '+totalAPago);
	  //  alert('valor: '+valor);
	     if((totalAPago - valor) < 0){
	    	alert('El valor del cargo de la cuenta no debe exceder el valor total de la Boleta a Pagar.');
		    document.formSolicitudHonorario.valor.focus();
		    document.getElementById('botonRegistrar').style.visibility="visible";
		    if(document.getElementById('botonRegistrar2')!= null)
				document.getElementById('botonRegistrar2').style.visibility="visible";
		    return false;
	    
	    }
	     return true;
	}

/*  Facturas*/

function cargaFactura (tipo, pag){    	  

	if(tipo == 1 )
		document.form_Facturacion.accionweb.value = "facturacionInterceptor.cargarMenuFacturacion";
	else
	    document.form_Facturacion.accionweb.value = "facturacionInterceptor.cargaAutorizaFacturacion";
		
	 document.form_Facturacion.action = pag;	
	 document.form_Facturacion.target = "_top";
     document.form_Facturacion.tipo.value = tipo;    
     document.form_Facturacion.submit();
	    
}

function cargaFacturaB (tipo, pag){    	  

	if(tipo == 1 )
		document.form_Facturacion.accionweb.value = "facturacionInterceptorB.cargarMenuFacturacion";
	else
	    document.form_Facturacion.accionweb.value = "facturacionInterceptorB.cargaAutorizaFacturacion";
		
	 document.form_Facturacion.action = pag;	
	 document.form_Facturacion.target = "_top";
     document.form_Facturacion.tipo.value = tipo;    
     document.form_Facturacion.submit();
	    
}

function cargaOrdenCompraB (tipo, pag, orden ){
    if (orden == 'R') {
       if(tipo == 1 )
		 document.formOrdenCompra.accionweb.value = "ordenCompraRegularizacionInterceptorB.cargarOrdenCompra";
	   else
	     document.formOrdenCompra.accionweb.value = "ordenCompraRegularizacionInterceptorB.cargarAutorizaOrdenCompra";
	 }
	 else {
	      if (orden == 'D') {
            if(tipo == 1 )
		       document.formOrdenCompra.accionweb.value = "ordenCompraDecentralizadaInterceptorB.cargarOrdenCompra";
	        else
	         document.formOrdenCompra.accionweb.value = "ordenCompraDecentralizadaInterceptorB.cargarAutorizaOrdenCompra";
	      }
	      else {
	         if(tipo == 1 )
		       document.formOrdenCompra.accionweb.value = "ordenCompraCentralizadaInterceptorB.cargarOrdenCompra";
	         else
	           document.formOrdenCompra.accionweb.value = "ordenCompraCentralizadaInterceptorB.cargarAutorizaOrdenCompra";
	        }
	     }          
	 document.formOrdenCompra.action = pag;	
	 document.formOrdenCompra.target = "_parent";
     document.formOrdenCompra.tipo.value = tipo;    
     document.formOrdenCompra.submit();
	    
}

function validaFactura(){
	/*if(document.formFacturacion.sede.value == -1){
	    alert('Debe seleccionar Sede.');
	    document.formFacturacion.sede.focus();
	    return false;
	 }*/
	 if(document.formFacturacion.nombreCliente.value == "" ){
	    alert('Debe ingresar Nombre del Cliente.');
	    document.formFacturacion.nombreCliente.focus();
	    return false;
	 } 
 	if(document.formFacturacion.direccionCliente.value == "" ){
	    alert('Debe ingresar Direcci�n del Cliente.');
	    document.formFacturacion.direccionCliente.focus();
	    return false;
	 } 
	 if(document.formFacturacion.comunaCliente.value == "" ){
	    alert('Debe ingresar Comuna del Cliente.');
	    document.formFacturacion.comunaCliente.focus();
	    return false;
	 } 
	 if(document.formFacturacion.ciudadCliente.value == "" ){
	    alert('Debe ingresar Ciudad del Cliente.');
	    document.formFacturacion.ciudadCliente.focus();
	    return false;
	 } 
	 if(document.formFacturacion.giroCliente.value == "" ){
	    alert('Debe ingresar Giro del Cliente.');
	    document.formFacturacion.giroCliente.focus();
	    return false;
	 } 
     if(document.formFacturacion.servicio.value == "-1" ||
        document.formFacturacion.servicio.value == " " ||
        document.formFacturacion.tipoServicio.value == "-1" ){
	    alert('Debe seleccionar el tipo de servicio.');
	    document.formFacturacion.servicio.focus();
	    return false;
	 }
	  if(document.formFacturacion.cuenta.value == -1 ){
	    alert('Debe seleccionar cuenta de ingreso.');
	    document.formFacturacion.cuenta.focus();
	    return false;
	 }	
	  if(document.formFacturacion.tipoCuenta.value != ""  &&
	     document.formFacturacion.numTipoCuenta.value  == ""){
	    alert('Debe Ingresar valor del Tipo de Cuenta.');
	    document.formFacturacion.numTipoCuenta.focus();
	    return false;
	 } 
	 
	 return true;
	 }
	 
	 function validaOrden(origen){
	 if(document.formOrdenCompra.fechaEntregaIng.value == "" ){
	    alert('Debe ingresar la Fecha de Entrega del producto.');
	    document.formOrdenCompra.fechaEntregaIng.focus();
	    return false;
	 } 
 	if(document.formOrdenCompra.campus.value == "" ){
	    alert('Debe ingresar el Campus de env�o.');
	    document.formOrdenCompra.campus.focus();
	    return false;
	 } 
	 if(document.getElementById('edificio').value == "" ){
	    alert('Debe ingresar el edificio de env�o.');
	    document.getElementById('edificio').focus();
	    return false;
	 }  
	if(document.getElementById('piso').value == "" ){
	    alert('Debe ingresar el Piso de env�o.');
	    document.getElementById('piso').focus();
	    return false;
	 } 
	 if(document.getElementById('oficina').value == "" ){
	    alert('Debe ingresar el Oficina de env�o.');
	    document.getElementById('oficina').focus();
	    return false;
	 } 
	  if(document.formOrdenCompra.divisa.value == "" ){
	    alert('Debe ingresar el Divisa de env�o.');
	    document.formOrdenCompra.divisa.focus();
	    return false;
	 } 
	
	 if(origen == 2 || origen == 3){ // significa que es oreden de compra regularizacion
	  if(document.formOrdenCompra.rut.value == "" ){
	    alert('Debe ingresar el Proveedor.');
	    document.formOrdenCompra.rut.focus();
	    return false;
	  }
	 }  
	 if(origen == 2){ 
	 if(document.formOrdenCompra.numeroDoc.value == "" || document.formOrdenCompra.tipoDoc.value == "" ){
	    alert('Debe ingresar el Numero y tipo de documento.');
	    document.formOrdenCompra.numeroDoc.focus();
	    return false;
	 } 
	  if(document.formOrdenCompra.fechaDocIng.value == ""){
	    alert('Debe ingresar la Fecha de la Orden.');
	    document.formOrdenCompra.fechaDocIng.focus();
	    return false;
	 } 
	 if(document.formOrdenCompra.motivoReg.value == 0 && document.formOrdenCompra.otroMotReg.value == "" ){
	    alert('Debe seleccionar el motivo de la regularizaci�n, sino encuentra el adecuado ingrese el detalle en columna OTRO.');
	    document.formOrdenCompra.motivoReg.focus();
	    return false;
	 } 
	 }
	  if(document.formSolicitudProducto.producto != null && document.formSolicitudProducto.producto.value == ""){
	     alert('Debe seleccionar el Bien o Servicio.');
	     document.formSolicitudProducto.producto.focus();
	     return false;
	   }
	   if(document.formSolicitudProducto.detalleProd != null && document.formSolicitudProducto.detalleProd.value == ""){
	     alert('Debe ingresar el detalle de productos.');
	     document.formSolicitudProducto.detalleProd.focus();
	     return false;
	   }
	   if(document.formSolicitudProducto.cantidadProd != null && document.formSolicitudProducto.cantidadProd.value == ""){
	     alert('Debe ingresar la cantidad del producto.');
	     document.formSolicitudProducto.cantidadProd.focus();
	     return false;
	   }
	   if(document.formSolicitudProducto.precioProd != null && document.formSolicitudProducto.precioProd.value == ""){
	     alert('Debe ingresar el precio del producto.');
	     document.formSolicitudProducto.precioProd.focus();
	     return false;
	   }
	   if(document.formDistribucionProducto.organizacion != null && document.formDistribucionProducto.organizacion.value == ""){
	     alert('Debe ingresar la organizaci�n de distribuci�n.');
	     document.formDistribucionProducto.organizacion.focus();
	     return false;
	   }
	 /*   if(document.formDistribucionProducto.porcentajeAsignado.value == ""){
	     alert('Debe ingresar el valor de distibuci�n.');
	     document.formDistribucionProducto.porcentajeAsignado.focus();
	     return false;
	   }*/
	   
	   if(document.formDistribucionProducto.valorAsignado != null && document.formDistribucionProducto.valorAsignado.value == ""){
	       alert('Debe ingresar el valor de distibuci�n.');
	     document.formDistribucionProducto.valorAsignado.focus();
	     return false;
	   }
	
	 return true;
	 }
	 
 
	function cambiaUnidad(){
	if(document.formFacturacion.cuenta){
	nomCuenta = document.formFacturacion.cuenta.value;
	var ind = nomCuenta.indexOf("@");
	document.formFacturacion.cuentaPresup.value = nomCuenta.substring(0,ind);
	document.formFacturacion.desuni.value = nomCuenta.substring(ind+1);
	}
	return;
	
	}
 
 	function cambiaServicio(){
 	if(document.formFacturacion.servicio){
 		servicio = document.formFacturacion.servicio.value;
		var ind = servicio.indexOf("@");
		document.formFacturacion.tipoServicio.value = servicio.substring(0,ind);
		servicio = servicio.substring(ind+1);
		ind = servicio.indexOf("@");
		tipoDocumento = servicio.substring(0,ind);
		document.formFacturacion.afecto.value =  tipoDocumento;
		/*if(tipoDocumento.trim() == "FEC")
		   document.formFacturacion.tipoDocumento.value = tipoDocumento.trim() + " - Afecto";
		if(tipoDocumento.trim() == "FET")
		   document.formFacturacion.tipoDocumento.value = tipoDocumento.trim() + " - Exento";
		   // se cambia al valor del iva, eso significa si es afecta o no
		   */
		   
		if(((servicio.substring(ind+1))/1) > 0)
		   document.formFacturacion.tipoDocumento.value = tipoDocumento.trim() + " - Afecto";
		else
		   document.formFacturacion.tipoDocumento.value = tipoDocumento.trim() + " - Exento";
		      
		document.formFacturacion.valorIVA.value = servicio.substring(ind+1);
		}
	return;
	
	}
	function cambiaCondicion(){
 		condicion = document.formFacturacion.condicionV.value;
 		var ind = condicion.indexOf("@");
		document.formFacturacion.diasVencimiento.value = condicion.substring(0,ind);
	    document.formFacturacion.condicionVenta.value = condicion.substring(ind+1); 				
	return;
	
	}
	
function validarNotaCredito(form){
 
    if(form.numDoc.value==""){
    alert("Debe ingresar el n�mero de Factura.");
    form.numDoc.focus();
    return false;
    }
    cargaLogin();
}
/*Boletas de Honorario*/
function validaPrestador(form){
 //alert('validaPrestador');
    if(form.nomPrestador.value==""){
    alert("Debe ingresar el nombre del prestador.");
    form.nomPrestador.focus();
    return false;
    }
    if(form.direccion.value==""){
    alert("Debe ingresar la direcci�n del prestador.");
    form.direccion.focus();
    return false;
    }
    if(form.ciudad.value==""){
    alert("Debe ingresar la ciudad del prestador.");
    form.ciudad.focus();
    return false;
    }
 
    //alert('valida');
    return true;
}
function registrarPrestador(form){
//alert ('registrarPrestador');
	if (validaPrestador(form)) {
	 	form.accionweb.value = 'honorarioInterceptorB.registraPrestador';
	 	//alert('accionweb: '+form.accionweb.value);
		form.submit();
	}
//alert('termino');	
return;	
}	


/*control de planes de desarrollo*/
function cambiaAnno(form){
//alert(form.anno.value);
document.formMuestra.anno.value = form.anno.value;
//alert(document.formMuestra.anno.value);
}
function consultaActividadPlanDesarrollo(form, action, coduni,nomUni, tipo, titulo,titulo2,codgru){
form.coduni.value = coduni;
form.nomUni.value = nomUni;
form.tipo.value = tipo; 
form.titulo.value = titulo; 
form.titulo2.value = titulo2; 
form.codgru.value = codgru; 
form.accionweb.value = 'presupuestoInterceptorB.cargaActividadControlPlan';
form.action = action;
form.target = "_top";
form.submit();
}
function cargaPaginaCPD(form, action, codMacro,nomMacro, codUnidadMacro, codCuenta, nomCuenta, titulo, titulo2,tipo){
form.codMacro.value = codMacro;
form.nomMacro.value = nomMacro;
form.codCuenta.value = codCuenta;
form.codUnidadMacro.value = codUnidadMacro;
form.nomCuenta.value = nomCuenta;
form.tipo.value = tipo; 
form.titulo.value = titulo; 
form.titulo2.value = titulo2; 
form.accionweb.value = 'presupuestoInterceptorB.cargaActividadControlPlan';
form.action = action;
form.target = "_top";
form.submit();
}
function cargaNotificacion(form, action, codMacro,nomMacro, codUnidadMacro, codCuenta, nomCuenta, fecmov, tipcue, rutide, digide, usuario, nomTipcue,numdoc){

form.codMacro.value = codMacro;
form.nomMacro.value = nomMacro;
form.codCuenta.value = codCuenta;
form.codUnidadMacro.value = codUnidadMacro;
form.nomCuenta.value = nomCuenta;
form.tipcue.value = tipcue; 
form.nomTipcue.value = nomTipcue;
form.fecmov.value = fecmov; 
form.rutide.value = rutide; 
form.digide.value = digide; 
form.usuario.value = usuario; 
form.numdoc.value = numdoc; 
form.accionweb.value = 'presupuestoInterceptorB.cargaNotificacion';
form.action = action;
form.target = "_top";
form.submit();
}

function registrarIndicador(form,action){
   if(form.indicador.value==""){
    alert("Debe ingresar el indicador a registrar.");
    form.indicador.focus();
    return false;
    }
    if(form.comen1.value==""){
    alert("Debe ingresar descripci�n ampliada a registrar.");
    form.comen1.focus();
    return false;
    }
    if(form.comen5.value==""){
    alert("Debe ingresar metas a registrar.");
    form.comen5.focus();
    return false;
    }
form.accionweb.value = 'presupuestoInterceptorB.registraIndicador';
form.action = action;
form.target = "_top";
form.submit();
}
function registrarNuevoValorIndicador(form,action){

  if(form.comen1.value==""){
    alert("Debe ingresar descripci�n del nuevo indicador.");
    form.comen1.focus();
    return false;
    }
  if(form.fechaIni.value==""){
    alert("Debe ingresar la fecha de valor del nuevo indicador.");
    form.fechaIni.focus();
    return false;
    }
form.accionweb.value = 'presupuestoInterceptorB.registraNuevoValorIndicador';
form.action = action;
form.target = "_top";
form.submit();
}
function registrarNotificacion(form,action){
  if(form.tipoNotificacion.value=="-1"){
    alert("Debe seleccionar tipo de notificaci�n.");
    form.tipoNotificacion.focus();
    return false;
    }
  if(form.comen1.value==""){
    alert("Debe ingresar texto asociado a la notificaci�n.");
    form.comen1.focus();
    return false;
    }
form.accionweb.value = 'presupuestoInterceptorB.registraNuevaNotificacion';
form.action = action;
form.target = "_top";
form.submit();
}

function agregarPeriodo(form){
    if(form.fechaIni.value==""){
    alert("Debe ingresar la fecha de inicio de la Presupuestaci�n.");
    form.fechaIni.focus();
    return false;
    }
    if(form.fechaTer.value==""){
    alert("Debe ingresar la fecha de t�rmino de la Presupuestaci�n.");
    form.fechaTer.focus();
    return false;
    }
    if(form.anno.value==""){
    alert("Debe ingresar el a�o de la Presupuestaci�n.");
    form.anno.focus();
    return false;
    }
        fechaIni = form.fechaIni.value;
		fechaFin = form.fechaTer.value;
		fec1 = fechaIni.substring(6) + fechaIni.substring(3,5) + fechaIni.substring(0,2);
		fec2 = fechaFin.substring(6) + fechaFin.substring(3,5) + fechaFin.substring(0,2);
	    if(((fec2/1) - (fec1/1)) < 0){
			alert("La fecha de Inicio debe ser menor a la fecha de T\u00e9rmino");
			form.fechaTer.focus();
			return false;
		}
form.accionweb.value = 'finanzaInterceptor.agregarPeriodo';
form.target = "_top";
form.submit();
return true;
}
function modificarPeriodo(form, anno, fechaInicio, fechaTermino){
if(confirm('Est� seguro de Modificar?')) {
	form.accionweb.value = 'finanzaInterceptor.modificarPeriodo';
	form.fechaInicio.value = fechaInicio.value;
	form.anno.value = anno;
	form.fechaTermino.value = fechaTermino.value;
	form.target = "_top";
	form.submit();
}
return true;
}
function eliminarPeriodo(form, anno){
 
if(confirm('Est� seguro de Eliminar?')) {
form.accionweb.value = 'finanzaInterceptor.eliminarPeriodo';
form.anno.value = anno;
form.target = "_top";
form.submit();
}
return true;
}
//*   DGAT*/

		
function cargaMenuDGAT(home, menu, op, form){
	var pag = "";
	var web = "";
	//alert('cargarProyAsociado:  menu  '+menu+'  op: '+op);
	if(menu == 7){
		if (op == 1) { // asociados al rut
    		web = "cargarProyAsociado";
			pag = "proyAsociados.html";
			form.opcionMenu.value = 1;
	   		form.titulo.value = "Consulta de Proyectos Asociados";
	   		form.menu.value = 1;
	   		form.tipo.value = "";
		}
		if (op == 2) {// por a�o
    		web = "cargarProyAsociado";
			pag = "proyPorAnnio.html";
			form.opcionMenu.value = 2;
	   		form.titulo.value = "Consulta de Proyectos por A�o";
	   		form.tipo.value = "";
		}
		if (op == 3) {// por estado
    		web = "cargarProyAsociado";
			pag = "proyPorEstado.html";
			form.opcionMenu.value = 5;
	   		form.titulo.value = "Consulta de Proyectos por Estado";
	   		form.tipo.value = "";
		}
	    if (op == 4) {// por cuenta
    		web = "cargarProyAsociado";
			pag = "proyPorCuenta.html";
			form.opcionMenu.value = 7;
	   		form.titulo.value = "Consulta de Proyectos por Cuenta";
	   		form.tipo.value = "";
		}
	}

	if (pag == "") alert("EN CONSTRUCCION ...");
	else {
	    form.accionweb.value = "dgatInterceptor." + web;
		form.opcion.value = op;
		form.action = home + "/dgat/inc/" + pag;
		form.target = "_parent";	
    	form.submit();
    }
	}	
function cargaProyectoAnnoCuenta(form, pag, annio, codUnidad){
		var web = "";
    	web = "cargarProyAsociado";
		form.titulo.value ="Consulta de Proyectos Por A�o y Cuenta";
		form.annoControl.value = annio;
	    form.codUnidad.value = codUnidad;
	    form.opcionMenu.value = 3;
	    form.accionweb.value = "dgatInterceptor." + web;
    	form.action = pag;
		form.target = "_parent";	
    	form.submit();
	}
function cargaProyecto(form, pag, codigo, web){   
        form.titulo.value = "Proyecto"; 
        form.codigo.value = codigo; 
        form.accionweb.value = "dgatInterceptor." + web;
    	form.action = pag;    
    	form.target = "_self";
        form.submit();
  
}
function cargaProyectoEstado(form, pag, codigo){
		var web = "";
    	web = "cargarProyAsociado";
		form.titulo.value ="Consulta de Proyectos Por Estado";
		form.codigo.value = codigo;
	    form.opcionMenu.value = 6;
	    form.accionweb.value = "dgatInterceptor." + web;
    	form.action = pag;
		form.target = "_parent";	
    	form.submit();
	}
//**/

//*apertura de cuentas*/
function validaCuenta(form){
 if(form.numMemo.value == "" ){
    alert('Debe Ingresar el N�mero de Memo.');
	form.numMemo.focus();
	return false;
}
 if(form.rut_aut.value == "" ){
    alert('Debe Ingresar el RUT del Autorizador.');
	form.rut_aut.focus();
	return false;
}

 if(form.nombreCuenta.value == "" ){
    alert('Debe Ingresar el Nombre de la Cuenta.');
	form.nombreCuenta.focus();
	return false;
}

if(form.motivoApertura1.value == "" ){
    alert('Debe Ingresar el Motivo de Apertura.');
	form.motivoApertura1.focus();
	return false;
}
if(form.rut_aux.value == "" ){
    alert('Debe Ingresar el RUT del Responsable.');
	form.rut_aux.focus();
	return false;
}

return true;
}
function registrarCuenta(form,home){


if(validaCuenta(form)){
 if(document.formSolicitudCuenta.rut_aux.value != "" ){
     	    alert('Debe presionar el bot\u00f3n Agregar para que quede el RUT del funcionario registrado.');
		    document.formSolicitudCuenta.rut_aux.focus();
		    return false;
}
    
  	form.action = home;
  	form.accionweb.value = 'presupuestoInterceptorB.registraApertura';
	form.target = "_top";
	form.submit();
}
}

function validaAsignacion(form){
 if(form.numMemo.value == "" ){
    alert('Debe Ingresar el N�mero de Memo.');
	form.numMemo.focus();
	return false;
}


 if(form.comentario.value == "" ){
    alert('Debe Ingresar el Comentario de la Asignaci�n.');
	form.comentario.focus();
	return false;
}

 if(form.registra.value == "" ){
    alert('Debe Ingresar al menos un funcionario para la Asignaci�n.');
	form.registra.focus();
	return false;
}

return true;
}


function validaFecha(form,o){
 var fechaIng = 0;
 if (o==1)
    fechaIng = document.getElementById('fechaEntregaIng').value;
 else fechaIng = document.getElementById('fechaDocIng').value;
 
 var fecha = fechaIng.substring(6) + fechaIng.substring(3,5) + fechaIng.substring(0,2);
 if (o==1)
    document.formOrdenCompra.fechaEntrega.value = fecha;
 else 
   document.formOrdenCompra.fechaDoc.value = fecha;
 return true;
}

function registrarAsignacion(form,home){


if(validaAsignacion(form)){
 if(document.formSolicitudCuenta.rut_aux.value != "" ){
     	    alert('Debe presionar el bot\u00f3n Agregar para que quede el RUT del funcionario registrado.');
		    document.formSolicitudCuenta.rut_aux.focus();
		    return false;
}
    
  	form.action = home+'/sipB/presupSolicitudes.html';
  	form.accionweb.value = 'presupuestoInterceptorB.registraAsignacion';
	form.target = "_parent";
	form.submit();
}
}
function cargaCuenta (tipo, pag){    	  

	if(tipo == 1 )
		document.form_Facturacion.accionweb.value = "presupuestoInterceptor.cargarMenuFacturacion";
	else
	    document.form_Facturacion.accionweb.value = "presupuestoInterceptor.cargaAutorizaCuenta";
		
	 document.form_Facturacion.action = pag;	
	 document.form_Facturacion.target = "_parent";
     document.form_Facturacion.tipo.value = tipo;    
     document.form_Facturacion.submit();
	    
}
function consultaHistorialDipres(form){
	AsignaUnidad(form);
	fechaI = 0;
    fechaT = 0;
	if(form.fechaIni.value != ''){	
	fechaInicio = form.fechaIni.value;

	fechaTermino = form.fechaTer.value;
	
	fechaI = fechaInicio.substring(6) + '' + fechaInicio.substring(3,5) + ''+ fechaInicio.substring(0,2);
	if(fechaTermino != ''){
		fechaT = fechaTermino.substring(6) + '' + fechaTermino.substring(3,5) + ''+ fechaTermino.substring(0,2);
	if((fechaI/1) > (fechaT/1) ){
	    alert('La fecha de Inicio debe ser menor a la de t\00e9rmino.');
	    form.fechaIni.focus();
	    return false;
	}
	}
	form.fechaInicio.value = fechaI;
	form.fechaTermino.value = fechaT;
	}

form.accionweb.value = 'presupuestoInterceptorB.cargaAperturaCuentas';
form.target = "_parent";
form.submit();
return true;
}
function AsignaUnidad(form){
	if(form.cuenta.value == 0){
	  form.codUnidad.value = 0;
	  form.nomUnidad.value = "Todas";
	} else {
	  	var ind = form.cuenta.value.indexOf("@");	
	  	form.codUnidad.value = form.cuenta.value.substring(0,ind);
	  	form.nomUnidad.value = form.cuenta.value.substring(ind+1);
	    }
	}
	
	
	
	function ApruebaCuenta(form, home){
	if(form.numCuenta.value == "" || form.numCuenta.value == 0){
	    alert('Debe Ingresar el N�mero de Cuenta');
	    form.numCuenta.focus();
	    return false;
	 }
	
	if(form.tipoUnidad.value == ""){
	    alert('Debe Seleccionar el tipo de Unidad');
	    form.tipoUnidad.focus();
	    return false;
	 }
	if(form.tipoCuenta.value == ""){
	    alert('Debe Seleccionar el tipo de Cuenta');
	    form.tipoCuenta.focus();
	    return false;
	 }
	 if(form.sedeUnidad.value == ""){
	    alert('Debe Seleccionar la sede de la Unidad');
	    form.sedeUnidad.focus();
	    return false;
	 }
	 if(form.valorCobro.value == ""){
	    alert('Debe Ingresar Cuenta Presupuestaria');
	    form.valorCobro.focus();
	    return false;
	 }
	   	form.accionweb.value = "presupuestoInterceptorB.apruebaCuentaApertura";
		form.action = home+ "/sipB/solApruebaApertura.html";
		form.target = "_parent"; 
 		form.submit();
}

	