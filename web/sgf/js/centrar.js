function Centrar(url, name, height, width) {
    var str = "height=" + height + ",innerHeight=" + height;
    str += ",width=" + width + ",innerWidth=" + width;
    if (window.screen) {
      var ah = screen.availHeight - 30;
      var aw = screen.availWidth - 10;

      var xc = (aw - width) / 2;
      var yc = (ah - height) / 2;

      str += ",left=" + xc + ",screenX=" + xc;
      str += ",top=" + yc + ",screenY=" + yc;
    }
    return window.open(url, name, str);
}

function Centrar02(url, name, height, width, par) {
    var str = "height=" + height + ",innerHeight=" + height;
    str += ",width=" + width + ",innerWidth=" + width;
    if (window.screen) {
      var ah = screen.availHeight - 30;
      var aw = screen.availWidth - 10;

      var xc = (aw - width) / 2;
      var yc = (ah - height) / 2;

      str += ",left=" + xc + ",screenX=" + xc;
      str += ",top=" + yc + ",screenY=" + yc;
    }
    str = str + "," + par;
    return window.open(url, name, str);
}

function Centrar03(name, height, width, par) {
    var str = "height=" + height + ",innerHeight=" + height;
    str += ",width=" + width + ",innerWidth=" + width;
    if (window.screen) {
      var ah = window.screen.availHeight - 30;
      var aw = window.screen.availWidth - 10;
      var xc = (aw - width) / 2;
      var yc = (ah - height) / 2;
      str += ",left=" + xc + ",screenX=" + xc;
      str += ",top=" + yc + ",screenY=" + yc;
    }
    str = str + "," + par;
    return window.open("", name, str);
}
