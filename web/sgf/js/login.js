function Inicio(){
  document.login_form.login.focus();
}

function ValidaLogin(f) {

  if ( f.login.value.length == 0 ){
                alert( "Ingrese su nombre de usuario.\nEjemplo: nombre.apellido");
                f.login.focus();
                return;
  }
  if ( f.passwd.value.length == 0 ) {
                alert( "Ingrese su contraseņa.");
                f.passwd.focus();
                return;
  }
  cargaLogin();
  f.submit();
  
  return;
}

function noenter() {
  return !(window.event && window.event.keyCode == 13);
}

function enter(e,t){
  if (window.event) {
    if (window.event.keyCode == 13) {
      if (t==1) document.login_form.passwd.focus();
      else ValidaLogin(document.login_form);
    }
  }
  else {
    if (e.which == 13){
      if (t==1) document.login_form.passwd.focus();
      else ValidaLogin(document.login_form);
    }
  }
}

var TheBrowserName = navigator.appName;
var TheBrowserVersion = parseFloat(navigator.appVersion);

function CursorOn(t){
  if ((TheBrowserName == 'Netscape') && (TheBrowserVersion >= 5)) {
    t.style.cursor="pointer";
  } else {
    t.style.cursor="hand";
  }
}
function CursorOff(t){
  t.style.cursor="default";
}
