package cl.utfsm.sicB.modulo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import oracle.sql.NUMBER;

import com.sun.org.apache.bcel.internal.generic.NEWARRAY;

import cl.utfsm.base.util.ConexionBanner;
import cl.utfsm.conexion.ConexionAs400IBM;
import cl.utfsm.conexion.ConexionAs400ValePago;
import cl.utfsm.sicB.datos.ComprasDao;
import descad.cliente.PreswBean;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw25DTO;

public class ModuloCompras {
	ComprasDao comprasDao;

	public ComprasDao getComprasDao() {
		return comprasDao;
	}

	public void setComprasDao(ComprasDao comprasDao) {
		this.comprasDao = comprasDao;
	}
	
	public int getLibroCompras( HttpServletRequest req, String mes, String a�o){
	    // ConexionBD con = new ConexionBD();
		 HttpSession sesion = req.getSession();
			
		 ConexionBanner con = new ConexionBanner();
	     List<Presw25DTO> listpresw25DTO = new ArrayList<Presw25DTO>();
	     Presw25DTO presw25DTO = new Presw25DTO();
	     int error = -1; 
	 
	     try {
		    	String query = 	" SELECT distinct FARINVA_INVH_CODE            AS codigo," +
					    		" FVRDCIN.FVRDCIN_DOCT_CODE                    AS TIPOdOCUMENTO," +
					    		" FABINVH_VEND_INV_CODE                        AS NUMDOCUMENTO," +
					    		" TO_CHAR(FABINVH_INVOICE_DATE,'YYYYMMDD')     AS FECHAEMISION," +
					    		" LTRIM(SUBSTR(LPAD(SPRIDEN_ID,10,'000'),0,9)) AS RUTPROVEEDOR," +
					    		" LTRIM(SUBSTR(LPAD(SPRIDEN_ID,10,'000'),10))  AS DIGPROVEEDOR," +
					    		" SPRIDEN.SPRIDEN_LAST_NAME" +
					    		" || ' '" +
					    		" || SPRIDEN.SPRIDEN_FIRST_NAME AS NOMBREPROVEEDOR" +
					    		" FROM FABINVH," +
					    		" FARINVA ,  FVRDCIN,  FARINVC,  SPRIDEN" +
					    		" WHERE FABINVH_CODE               = FARINVA_INVH_CODE" +
					    		" AND FARINVA.FARINVA_FSYR_CODE 	= '" + a�o +"'" +
						    	" AND FARINVA.FARINVA_PERIOD 		= '" + mes +"'" +
						    	" AND FARINVA_ITEM                 = FVRDCIN_ITEM" +
					    		" AND FARINVA.FARINVA_FSYR_CODE    = '17'" +
					    		" AND FARINVA.FARINVA_PERIOD       = '01'" +
					    		" AND FVRDCIN_INVH_CODE            = FABINVH_CODE" +
					    		" AND FARINVC_INVH_CODE            = FVRDCIN_INVH_CODE" +
					    		" AND FARINVC_ITEM                 = FVRDCIN_ITEM" +
					    		" AND FABINVH.FABINVH_VEND_PIDM  = SPRIDEN.SPRIDEN_PIDM" +
					    		" AND SPRIDEN_CHANGE_IND        IS NULL" +
					    		" AND FVRDCIN.FVRDCIN_DOCT_CODE IN ('FAP', 'FPE', 'FAE','FEE', 'NDP')";
									    


	    	// System.out.println(query);
	    	PreparedStatement sent = con.conexion().prepareStatement(query); 
	        
		    ResultSet res = sent.executeQuery();
            Vector v = new Vector();
		    while (res.next()) {	
		    	//System.out.println("1");
		        presw25DTO = new Presw25DTO();
	    		presw25DTO.setTippro("GLC");										// tippro
	    		presw25DTO.setCodigo(res.getString(2).trim()); 						// tipo documento
	    		presw25DTO.setPres01(res.getLong(3)); 								// numero documento
	    		presw25DTO.setPres02(res.getInt(4));								// fecha de emision
	    		presw25DTO.setPres03(Long.parseLong(res.getString(5)));				// rut proveedor
	    		presw25DTO.setIndexi(res.getString(6).trim());  					// digito proveedor
	    		if(res.getString(7).trim().length() > 40)
	    			presw25DTO.setDesite(res.getString(7).substring(0,40)); 		// nombre proveedor
	    		else
	    			presw25DTO.setDesite(res.getString(7).trim());  				// nombre proveedor
	    		presw25DTO.setComen5(res.getString(1));                             // codigo banner 
	    		presw25DTO.setMespre(Integer.parseInt(mes));						//	mes libro
	    		presw25DTO.setAnopre(Integer.parseInt(a�o));						//	a�o libro
	 
		    	v = this.getImpuesto(res.getString(1));
		    	int i = 1;
		    	long totalTotal = 0;
		    	long exento = 0;
		           
		    	  for (int z = 0; z < v.size(); z++) {
	                 String codigo   = ((Vector) v.get(z)).get(0)+"";
	                 long impuesto   = Long.parseLong(((Vector) v.get(z)).get(1)+"") ;
	                 long valorImpuesto   = Long.parseLong(((Vector) v.get(z)).get(2)+"");
	                 long valorTotal      = Long.parseLong(((Vector) v.get(z)).get(3)+"");
	 	         	
		      		if(codigo.trim().equals("IVA")){
		      			presw25DTO.setItedoc(Integer.parseInt(String.valueOf(impuesto)));// tasa IVA
			    		presw25DTO.setPres05(valorTotal); 				// valor afecto			    	
			    		presw25DTO.setPres06(valorImpuesto);  			// valor IVA
			    		totalTotal = totalTotal + valorTotal + valorImpuesto;
		      		} else {
			      		if(codigo.trim().equals("NT")){
			      			presw25DTO.setPres04(valorTotal); 				// valor exento		
			      			exento = exento + valorTotal;
				    	} else {
		      	
			      		
					      		switch (i){
					      		case 1:{i++;
					      			    presw25DTO.setPres08(valorImpuesto);  				             	  //valor otro impuesto 2
							    		presw25DTO.setValr11(Integer.parseInt(String.valueOf(impuesto * 100)));//	tasa otro impuesto 2
							    		presw25DTO.setComen1(codigo);
							    		totalTotal = totalTotal + valorImpuesto;
							    		break; 
							  			}
					      		case 2:{i++;
					      			    presw25DTO.setPres09(valorImpuesto);  				             	  //valor otro impuesto 2
							    		presw25DTO.setValr12(Integer.parseInt(String.valueOf(impuesto * 100)));//	tasa otro impuesto 2
							    		presw25DTO.setComen2(codigo);									// 	codigo otro impuesto 2
							    		totalTotal = totalTotal + valorImpuesto;
							    		break; 
					            }
					      		case 3:{i++;
					      			    presw25DTO.setPres10(valorImpuesto);  				             	  //valor otro impuesto 2
							    		presw25DTO.setValr21(Integer.parseInt(String.valueOf(impuesto * 100)));//	tasa otro impuesto 2
							    		presw25DTO.setComen3(codigo);									// 	codigo otro impuesto 2
							    		totalTotal = totalTotal + valorImpuesto;
							    		break; 
					 
					      				}
					      		case 4:{i++;
					      			    presw25DTO.setPres11(valorImpuesto);  				             	  //valor otro impuesto 2
							    		presw25DTO.setValr22(Integer.parseInt(String.valueOf(impuesto * 100)));//	tasa otro impuesto 2
							    		presw25DTO.setComen4(codigo);									// 	codigo otro impuesto 2
							    		totalTotal = totalTotal + valorImpuesto;
							    		break; 					 
					      				}					      		
					      		}
				    	}
		      		}
			 
		    	}
		    	  presw25DTO.setPres07(totalTotal + exento);
		    	  listpresw25DTO.add(presw25DTO);
		    }
		     	
	        if( listpresw25DTO.size() > 0){
		    	sesion.setAttribute("listpresw25DTO", listpresw25DTO);
		    	for(Presw25DTO ss: listpresw25DTO)
		    		//System.out.println(ss.getCodigo());
		    	error = 0;
		    }
		   
		     res.close();
		     sent.close();
		     con.close();
		   
		    }
		    catch (SQLException e ) {
		      System.out.println("Error en moduloCompras.getLibroCompras : " + e );
		    }
		    return error ;
	}
	public Vector getImpuesto(String codigo){
	    // ConexionBD con = new ConexionBD();
			
		 ConexionBanner con = new ConexionBanner();
	     Vector v = new Vector();    
	     try {
	    	String query =	" SELECT  FARINTX_TRAT_CODE       as codigo," +  
					    	" FTVTRAT.FTVTRAT_RATE    AS TASAIVA," +
					    	" SUM(FARINTX_TAX_AMT)    as valor,  " +
					    	" sum(FARINTX_TAXABLE_AMT) as valorTotal" +
					    	" FROM farintx, FTVTRAT" +
					    	" WHERE FARINTX_INVH_CODE = '" + codigo + "'" +
					    	" AND FARINTX_SEQ_NUM is null" +
					    	" AND FARINTX_TRAT_CODE    = FTVTRAT.FTVTRAT_CODE" +
					    	" AND FTVTRAT_NCHG_DATE            =" +
					    	" (SELECT MAX(FTVTRAT_NCHG_DATE)" +
					    	" FROM FTVTRAT t" +
					    	" WHERE t.FTVTRAT_CODE = FTVTRAT_CODE)" +
					    	" GROUP BY FARINTX_TRAT_CODE, FTVTRAT_RATE";
					    		
	   // 	System.out.println(query);
	    	PreparedStatement sent = con.conexion().prepareStatement(query); 
	        
		    ResultSet res = sent.executeQuery();

		    while(res.next()) {
		    	 Vector a = new Vector();    
		 	     a.addElement(res.getString(1));// codigo
		 	     a.addElement(res.getLong(2)); // impuesto
		 	     a.addElement(res.getLong(3)); // valor impuesto
		 	     a.addElement(res.getLong(4)); // valor total
		    	 v.addElement(a);
		    	}
		     res.close();
		     sent.close();
		     con.close();
		   
		    }
		    catch (SQLException e ) {
		      System.out.println("Error en moduloCompras.getImpuesto : " + e );
		    }
		    return v ;
	}

	 public boolean setGeneraiSeries(List<Presw25DTO> lista){
		    PreswBean preswbean = null;
			preswbean = new PreswBean("GLC",0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
			Presw25DTO presw25 = new Presw25DTO();		
			boolean respuesta = preswbean.ingreso_presw25(lista);
		 return respuesta;
		}
}
