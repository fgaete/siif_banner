package cl.utfsm.sicB.mvc;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.sun.org.apache.bcel.internal.generic.NEWARRAY;

import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.sicB.modulo.ModuloCompras;
import descad.presupuesto.Presw25DTO;

public class ComprasInterceptor extends HandlerInterceptorAdapter {
	private ModuloCompras moduloCompras;
	
	public ModuloCompras getModuloCompras() {
		return moduloCompras;
	}
	public void setModuloCompras(ModuloCompras moduloCompras) {
		this.moduloCompras = moduloCompras;
	}
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
	}
	public void cargarMenu(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		accionweb.agregarObjeto("esCompras", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		
		
	}
	public void generaLibro(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		String mes = Util.validaParametro(accionweb.getParameter("mes"), "");
		String a�o = Util.validaParametro(accionweb.getParameter("ano"), "");
		List<Presw25DTO> listpresw25DTO = new ArrayList<Presw25DTO>();	
		int error = moduloCompras.getLibroCompras(accionweb.getReq(), mes, a�o);
		listpresw25DTO = (List<Presw25DTO> ) accionweb.getSesion().getAttribute("listpresw25DTO");
		boolean respuesta = false;
		if(error == 0){
			respuesta = moduloCompras.setGeneraiSeries(listpresw25DTO);
		//	listpresw25DTO = (List<Presw25DTO> ) accionweb.getSesion().getAttribute("listpresw25DTO");
		}
		accionweb.agregarObjeto("respuesta", respuesta);
		accionweb.agregarObjeto("esCompras", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		
	}
}
