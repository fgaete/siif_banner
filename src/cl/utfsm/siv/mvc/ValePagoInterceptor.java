package cl.utfsm.siv.mvc;
/**
 * SISTEMA DE VALE DE PAGOS POR INTERNET
 * PROGRAMADOR		: 	M�NICA BARRERA FREZ
 * FECHA ULT.MODIF  : 	26/04/2011
 * UNIDAD DE DESARROLLO INSTITUCIONAL(DTI)   
 */
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.tools.generic.DateTool;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.siv.CUENTAPRESUPUESTARIA;
import cl.utfsm.siv.LISTAVALEPAGO;
import cl.utfsm.siv.PRESF61;
import cl.utfsm.siv.modulo.ModuloValePago;
import descad.cliente.CocofBean;
import descad.cliente.CocowBean;
import descad.cliente.Funcionario;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Cocof17DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw21DTO;
import descad.presupuesto.Presw25DTO;
//   PreswBean preswbean = new PreswBean(opcion,codUnidad,0,anopar,mespar,rutide,numdoc,tipdoc,sucur,digide,"",numcom,codban,numche,fecmov,cajero,tipcue);


public class ValePagoInterceptor extends HandlerInterceptorAdapter{
	private ModuloValePago moduloValePago;
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
	}
	public void limpiaSimulador(AccionWeb accionweb) throws Exception {
		if (accionweb.getSesion().getAttribute("listaUnidad") != null)
			accionweb.getSesion().setAttribute("listaUnidad", null);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutOrigen  = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen") +"","0"));
		String nomSimulacion = "";
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		
		/*hacer este rut rutUsuario*/
		if(rutOrigen != 0) // lo vuelve a lo original
		{
			rutUsuario = rutOrigen ;
			rutOrigen = 0;
		}
		 
		
		accionweb.getSesion().setAttribute("rutUsuario", rutUsuario);
		accionweb.getSesion().setAttribute("rutOrigen", rutOrigen);
		accionweb.getSesion().setAttribute("nomSimulacion", nomSimulacion);
		accionweb.agregarObjeto("rutOrigen", rutOrigen);
		accionweb.agregarObjeto("nomSimulacion", nomSimulacion);
		
	}
	public void cargarMenu(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
	  	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.getSesion().removeAttribute("autorizaProyecto");
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.getSesion().removeAttribute("autorizaUCP");
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.getSesion().removeAttribute("autorizaDIRPRE");
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.getSesion().removeAttribute("autorizaFinanzas");
      		
	/* se debe buscar segun el usuario si tiene permiso para autorizar*/
	List<Presw18DTO> listaAutorizaciones = new ArrayList<Presw18DTO>();
	listaAutorizaciones = moduloValePago.getConsultaAutoriza(rutUsuario, dv);
	if(listaAutorizaciones != null && listaAutorizaciones.size() > 0){
		 for (Presw18DTO ss : listaAutorizaciones){
			if(ss.getIndprc().trim().equals("Y")){
				accionweb.agregarObjeto("autorizaProyecto", 1);
				accionweb.getSesion().setAttribute("autorizaProyecto", 1);
			}
			if(ss.getIndprc().trim().equals("X")){
				accionweb.agregarObjeto("autorizaUCP", 1);
				accionweb.getSesion().setAttribute("autorizaUCP", 1);
			}
			if(ss.getIndprc().trim().equals("Z")){
				accionweb.agregarObjeto("autorizaDIRPRE", 1);
				accionweb.getSesion().setAttribute("autorizaDIRPRE", 1);
			}
			if(ss.getIndprc().trim().equals("F")){
				accionweb.agregarObjeto("autorizaFinanzas", 1);
				accionweb.getSesion().setAttribute("autorizaFinanzas", 1);
			}
		 }
	}
		if(rutOrigen > 0){
			this.limpiaSimulador(accionweb);
		}
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		if(listaSolicitudes != null && listaSolicitudes.size() > 0)
			accionweb.getSesion().removeAttribute("listaSolicitudes");
				 	
		accionweb.agregarObjeto("esValePago", 1);		
		accionweb.agregarObjeto("listaAutorizaciones", listaAutorizaciones);
	}
	public void cargaOpcion(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int opcion = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
        moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
        lista = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
        List<Integer> listA�o = new ArrayList<Integer>();
        int a�oActual = 0;
        if(accionweb.getSesion().getAttribute("anno") != null)
        	a�oActual = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("anno")+ "","0"));
        listA�o = moduloValePago.getListaA�o(a�oActual);
        accionweb.agregarObjeto("listAnno", listA�o);
        accionweb.agregarObjeto("cuentasPresupuestarias", lista);
		accionweb.agregarObjeto("esValePago", 1);
		accionweb.agregarObjeto("opcion", opcion);
	    accionweb.agregarObjeto("annoSel", a�oActual);
    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		
	}
	
	public void cargaListaValePago(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		String estado = Util.validaParametro(accionweb.getParameter("estado"), "");
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
        lista = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
        List<Integer> listA�o = new ArrayList<Integer>();
        int a�oActual = 0;
        if(accionweb.getSesion().getAttribute("anno") != null)
        	a�oActual = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("anno")+ "","0"));
        listA�o = moduloValePago.getListaA�o(a�oActual);
         PreswBean preswbean = new PreswBean("CVP",unidad,0,anno,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"",estado);
      
		Collection<Presw18DTO> listaValePago = (Collection<Presw18DTO>) preswbean.consulta_presw18();
	    accionweb.agregarObjeto("estado", estado);
        accionweb.agregarObjeto("unidad", unidad);
        accionweb.agregarObjeto("annoSel", anno);
        if(listaValePago.size() > 0)
        	accionweb.agregarObjeto("hayDatoslista", 1);
        accionweb.agregarObjeto("listaValePago", listaValePago);
        accionweb.agregarObjeto("listAnno", listA�o);
        accionweb.agregarObjeto("cuentasPresupuestarias", lista);
		accionweb.agregarObjeto("esValePago", 1);
		
		
		
	}
	public void cargaAutorizaValePago(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
				
		PreswBean preswbean = null;
		Collection<Presw18DTO> listaValePago = null;
		String titulo = "";
		String tituloDetalle1 = "";
		String tituloDetalle2 = "";
		String tipcue = "";
		//System.out.println("rutUsuario: "+rutUsuario);
		//System.out.println("dv: "+dv);
		switch (tipo) {
		case  2:// lista de vales para modificar
			{preswbean = new PreswBean("VVP",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			titulo = "Modificaci�n";
			break;
		}
		case 3: case 6: case 8: case 9: case 10: // lista de vales para autorizar
			{String nomtipo = "";
			if(tipo == 3 || tipo == 6){
				titulo = "Autorizaci�n Responsable de la Cuenta";
				nomtipo = "AUV";
				accionweb.agregarObjeto("opcion2", 6);
			}
			if(tipo == 8){
			   nomtipo = "LVA";
			   titulo = "Autorizaci�n MECESUP VRA";
			   tipcue = "Y";
			   accionweb.agregarObjeto("opcion2", 7);
			}
			if(tipo == 9){
				   nomtipo = "LVA";
				   titulo = "Autorizaci�n UCP";
				   tipcue = "X";
				   accionweb.agregarObjeto("opcion2", 6);
				}
			if(tipo == 10){
				   nomtipo = "LVA";
				   titulo = "Autorizaci�n MECESUP DIPRES";
				   tipcue = "Z";
				}
			preswbean = new PreswBean(nomtipo,0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			preswbean.setTipcue(tipcue);
			tituloDetalle1 = "Autorizar";
			tituloDetalle2 = "Rechazar";
			if (tipo == 6)
				tituloDetalle1 = "Recepci�n";
			accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
			accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
		
			
			break;
			}
		case 4: // lista de vales para consulta
			{		
			String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
			int fechaInicio = Util.validaParametro(accionweb.getParameter("fechaInicio"),0);
			int fechaTermino = Util.validaParametro(accionweb.getParameter("fechaTermino"),0);
			String fechaIni = Util.validaParametro(accionweb.getParameter("fechaIni"),"");
			String fechaTer = Util.validaParametro(accionweb.getParameter("fechaTer"),"");
			int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0);
			int anno = Util.validaParametro(accionweb.getParameter("anno"),0);
			/*agrego filtro de a�o*/
			DateTool fechaActual = new DateTool();
			
			if(fechaInicio > 0 && fechaTermino == 0 ){
				fechaActual = new DateTool();
				String dia = fechaActual.getDay()+"";
				if(dia.length() == 1)
					dia = "0" + dia;
				String mes = (fechaActual.getMonth()+1)+"";
				if(mes.length()== 1)
					mes = "0" + mes;
				fechaTermino = Integer.parseInt(String.valueOf(fechaActual.getYear()) + mes + dia);
				fechaTer = dia + "/" + mes + "/" + String.valueOf(fechaActual.getYear());
				
			} else {
				if(anno == 0){
					fechaInicio = Integer.parseInt(String.valueOf(fechaActual.getYear()) + "0101");
					fechaTermino = Integer.parseInt(String.valueOf(fechaActual.getYear()) + "1231");
				} else {
					fechaInicio = Integer.parseInt(String.valueOf(anno) + "0101");
					fechaTermino = Integer.parseInt(String.valueOf(anno) + "1231");
		
				}
				accionweb.agregarObjeto("annoselec", anno);
			}
			if(unidad >= 0){
			preswbean = new PreswBean("CVP",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			preswbean.setCoduni(unidad);
			preswbean.setTipcue(estado);
			preswbean.setFecmov(fechaInicio);
			preswbean.setNumcom(fechaTermino);
			listaValePago = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			}
			titulo = "Consulta";	
			List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
			moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
			listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
			accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
		   	accionweb.agregarObjeto("estado", estado);
		   	accionweb.agregarObjeto("unidad", unidad);
		   	accionweb.agregarObjeto("fechaIni", fechaIni);
		   	accionweb.agregarObjeto("fechaTer", fechaTer);
			break;
			}
		case 5: case 7:// lista de vales para Recepci�n de vales en Finanzas
			{
			int sede = Util.validaParametro(accionweb.getParameter("sede"),0);	
			String estadoVales = Util.validaParametro(accionweb.getParameter("estadoVales"),"");
			preswbean = new PreswBean("LVP",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			preswbean.setSucur(sede);
			preswbean.setTipcue(estadoVales);
			titulo = "Recepci�n Finanzas";
			tituloDetalle1 = "Recepci�n";
			tituloDetalle2 = "Rechazar";
			accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
			accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
			tipo = 5;// para que cambie opcion en caso de que sea 7
			accionweb.agregarObjeto("opcion2", 7);
			List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
			listSede = moduloValePago.getListaSede(accionweb.getReq());
			accionweb.agregarObjeto("listSede", listSede);
			accionweb.agregarObjeto("sede", sede);
			accionweb.agregarObjeto("estadoVales", estadoVales);
			break;
			}
		case 11: // lista de vales para consulta
		{		
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0);
		if(unidad >= 0){
		preswbean = new PreswBean("CVC",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
		preswbean.setCoduni(unidad);
		preswbean.setTipcue(estado);
		listaValePago = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		}
		titulo = "Consulta";
		if(tipo == 11){
			titulo = "Existentes";
			int volver = Util.validaParametro(accionweb.getParameter("volver"),0);
			if(volver == 1) {
			int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"),0);
			String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
			String nomIdentificador = Util.validaParametro(accionweb.getParameter("nomIdentificador"),"");
			if(rutnum > 0 /*&& nomIdentificador.trim().equals("")*/)
				this.verificaRut(accionweb);
			accionweb.agregarObjeto("dvrut", dvrut);
			accionweb.agregarObjeto("rutnum", rutnum);
			accionweb.agregarObjeto("nomIdentificador", nomIdentificador);
			}
		}
		List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
		moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
		listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
	   	accionweb.agregarObjeto("estado", estado);
	   	accionweb.agregarObjeto("unidad", unidad);
		break;
		}	
		case  12:// lista de Ingresadores
		{	int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0);
		    String accion = Util.validaParametro(accionweb.getParameter("accion"),""); 
		    if(!accion.trim().equals("")){
		    	int rutide = Util.validaParametro(accionweb.getParameter("rutide"), 0);
		    	String digide = Util.validaParametro(accionweb.getParameter("digide"),""); 
		    	try {
		    	boolean error = moduloValePago.saveDeleteIngresador(rutide, digide, unidad, accion);
		    	if(error)
		    		accionweb.agregarObjeto("mensaje", "Registro exitoso");
		    	else
		    		accionweb.agregarObjeto("mensaje", "Problemas al registrar.");
			   	Thread.sleep(500);
		    	} catch (Exception e) {
					accionweb.agregarObjeto("mensaje", "No guardado registraCliente.");
					accionweb.agregarObjeto("exception", e);
					e.printStackTrace();
				}
		    }
		
			List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
			moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
			listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
			accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
		   	accionweb.agregarObjeto("unidad", unidad);
			if(unidad > 0){
		    	preswbean = new PreswBean("OLI",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
		    	preswbean.setCoduni(unidad);
		    	Collection<Presw25DTO> listaValePago25 = (Collection<Presw25DTO>) preswbean.consulta_presw25();
		    	accionweb.agregarObjeto("hayDatoslista", 1);
		    	accionweb.agregarObjeto("listaValePago25", listaValePago25);
			}
			titulo = "Ingresadores";
		break;
	}

		}
		if(tipo != 4 && tipo != 12)
			listaValePago = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		
		
		//System.out.println("listaValePago.size(): "+listaValePago.size());
	    if(listaValePago != null && listaValePago.size() > 0)
        	accionweb.agregarObjeto("hayDatoslista", "1");
	    
        accionweb.agregarObjeto("listaValePago", listaValePago);
                
        accionweb.agregarObjeto("esValePago", 1);
		
      	accionweb.agregarObjeto("opcion", String.valueOf(tipo));
    	accionweb.agregarObjeto("opcionMenu", tipo);
      	accionweb.agregarObjeto("titulo", titulo);
      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);      
      	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	}
	
	public void consultaVale(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
		long sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
		String estadoVales = Util.validaParametro(accionweb.getParameter("estadoVales"),"");
		String nomIdentificador = "";
		long rutnum = 0;
		String dvrut = "";		

		
		//System.out.println("numDoc: "+numDoc);
		if(sede > 0)
			accionweb.agregarObjeto("sedeVale", sede);
		if(!estadoVales.trim().equals(""))
			accionweb.agregarObjeto("estadoVales", estadoVales);
		Presw19DTO preswbean19DTO = new Presw19DTO();
		String titulo = "";
		String tituloDetalle1 = "";
		preswbean19DTO.setTippro("OVA");
		preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
		preswbean19DTO.setDigide(dv);
		preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
	//	preswbean19DTO.setIdsoli("9999999");
	
		String readonly = "readonly";
		

		
/*capturar los datos */
		
		if(tipo == 2) // lista de vales para consultar
		{	titulo = "Modifica ";
			tituloDetalle1 = "Modificar";
			readonly = "";
			}
		if(tipo == 3) // lista de vales para autorizar
			{
			titulo = "Autorizaci�n Responsable de la Cuenta";
			tituloDetalle1 = "Autorizar";		 
			}
		if(tipo == 4) // lista de vales para consulta
			{
			titulo = "Consulta ";
			tituloDetalle1 = "Consultar";
			}
		if(tipo == 5) // lista de vales para Recepci�n de vales en Finanzas
			{
			titulo = "Recepci�n ";
			tituloDetalle1 = "Recepci�n";
			}
		if(tipo == 8) // lista de vales para autorizar
		{
		titulo = "Autorizaci�n MECESUP VRA";
		tituloDetalle1 = "Autorizar";		 
		}
		if(tipo == 9) // lista de vales para autorizar
		{
		titulo = "Autorizaci�n UCP";
		tituloDetalle1 = "Autorizar";		 
		}
		if(tipo == 10) // lista de vales para autorizar
		{
		titulo = "Autorizaci�n MECESUP DIPRES";
		tituloDetalle1 = "Autorizar";		 
		}
		if(tipo == 11) // lista de vales para copiar
		{	titulo = "Ingreso";
			tituloDetalle1 = "Ingreso";
			readonly = "";
			nomIdentificador = Util.validaParametro(accionweb.getParameter("nomIdentificador"),"");
			dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
			rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		
			}
		List<Presw18DTO>  listaSolicitudesPago = new ArrayList<Presw18DTO>();
		List<Presw18DTO>  listaSolicitudes = new ArrayList<Presw18DTO>();
		List<Presw18DTO>  listaHistorial = new ArrayList<Presw18DTO>();
		long valorAPagar = 0;
		String identificadorInterno = "";	
		sede = 0;
		int identificador = 0; 
		
	    String nomcam = "";
	    long valnu1 = 0;
	    long valnu2 = 0;
	    String valalf = "";
	    String glosa1 = "";
	    String glosa2 = "";
	    String glosa3 = "";
	    String glosa4 = "";
	    String glosa5 = "";
	    String glosa6 = "";
	    String tipoPago = "";
	    String nombrePago = "";
	    int ind = 0;
	    String estadoFinal = "";
	    String codEstadoFinal = "";
	    long totalSolicitudVale = 0;
	    long totalSolicitudPago = 0;
		List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
		CocowBean cocowBean = new CocowBean();
		//System.out.println("preswbean19DTO : "+ preswbean19DTO.getRutide()+"  - "+preswbean19DTO.getNumdoc()+"--- "+preswbean19DTO.getTippro()+"  --- "+preswbean19DTO.getFecmov());
		listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
		//System.out.println("listCocow36DTO : "+ listCocow36DTO.size() );
	    if(listCocow36DTO != null && listCocow36DTO.size() >0 )
	    
	    for(Cocow36DTO ss: listCocow36DTO){
	    //	 if(ss.getTiping().trim().equals("VAP")){
				   
	    	if(ss.getNomcam().trim().equals("GLOSA1"))
	    		glosa1 = ss.getValalf();
	    	if(ss.getNomcam().trim().equals("GLOSA2"))
	    		glosa2 = ss.getValalf();
	     	if(ss.getNomcam().trim().equals("GLOSA3"))
	    		glosa3 = ss.getValalf();
	     	if(ss.getNomcam().trim().equals("GLOSA4"))
	    		glosa4 = ss.getValalf();
	     	if(ss.getNomcam().trim().equals("GLOSA5"))
	    		glosa5 = ss.getValalf();   
	     	if(ss.getNomcam().trim().equals("GLOSA6"))
	    		glosa6 = ss.getValalf();
   
            if(ss.getNomcam().trim().equals("RUTIDE") && tipo != 11)
             	   rutnum = ss.getValnu1();
            if(ss.getNomcam().trim().equals("DIGIDE") && tipo != 11)
            	dvrut = ss.getValalf();
            if(ss.getNomcam().trim().equals("DESIDE") && tipo != 11)
                nomIdentificador = ss.getValalf();
            if(ss.getNomcam().trim().equals("CODSUC"))
            	sede = ss.getValnu1();
            if(ss.getNomcam().trim().equals("VALPAG"))
            	valorAPagar = ss.getValnu1();
            if(ss.getNomcam().trim().equals("IDMEMO"))
            	identificadorInterno = ss.getValalf();
            if(ss.getNomcam().trim().equals("TIPVAL")){
            	tipoPago = ss.getValalf();
            	nombrePago = ss.getResval();
            }
            if(ss.getNomcam().trim().equals("CUENTA")){
            	Presw18DTO presw18DTO = new Presw18DTO();
            	presw18DTO.setCoduni(Integer.parseInt(ss.getValnu1()+""));
            	presw18DTO.setUsadom(ss.getValnu2());
               	presw18DTO.setDesuni(ss.getValalf());
               	presw18DTO.setDesite(ss.getResval());
            	listaSolicitudes.add(presw18DTO);
            	totalSolicitudVale += ss.getValnu2();
            }
            if(ss.getNomcam().trim().equals("NOMBEN")){
            	Presw18DTO presw18DTO = new Presw18DTO();
            	presw18DTO.setUsadom(ss.getValnu1());
            	presw18DTO.setDesuni(ss.getValalf());
            	presw18DTO.setRutide(Integer.parseInt(ss.getValnu2()+"")); // rut
            	presw18DTO.setIddigi(ss.getAccion());  // dv
            	listaSolicitudesPago.add(presw18DTO);
            	totalSolicitudPago += ss.getValnu1();
            }
            if(ss.getNomcam().trim().equals("HISTORIAL")){
            	Presw18DTO presw18DTO = new Presw18DTO();
            	presw18DTO.setUsadom(ss.getValnu1()); // es la fecha, 
            	presw18DTO.setUsadac(ss.getValnu2());//en caso de Resval="Autoriza" es la unidad
            	presw18DTO.setDesite(ss.getValalf()); // es el responsable
            	presw18DTO.setDesuni(ss.getResval()); // es la accion, INGRESO, MODIFICACION, AUTORIZACION, PAGO, AUTORIZA FINANZAS
            	listaHistorial.add(presw18DTO);
            }
            if(ss.getNomcam().trim().equals("ESTADO")){
            	estadoFinal = ss.getValalf();
            	codEstadoFinal = ss.getResval();
            }
	    //	 }
	    }	
	    String selected = (sede == 0)?"selected=\"selected\"":"";
	    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
	    listSede = moduloValePago.getListaSede(accionweb.getReq());
	//System.out.println("listSede: "+listSede);
	    List<Presw18DTO> listaTipoPago = new ArrayList<Presw18DTO>();
	    listaTipoPago = moduloValePago.getListaTopoPago(accionweb.getReq());
	   // System.out.println("listaTipoPago: "+listaTipoPago);
 	    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
	    moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
	    //System.out.println("listaUnidades: "+listaUnidades);
	    if(listaSolicitudes != null && listaSolicitudes.size() > 0){
	    	accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes);
	    	accionweb.getSesion().setAttribute("listaSolicitudes", listaSolicitudes);
	    }
	    if(listaSolicitudesPago != null && listaSolicitudesPago.size() > 0){
	    	accionweb.agregarObjeto("listaSolicitudesPago", listaSolicitudesPago);
	       	accionweb.getSesion().setAttribute("listaSolicitudesPago", listaSolicitudesPago);
	    }
	    if(listaHistorial != null && listaHistorial.size() > 0){
	    	accionweb.agregarObjeto("listaHistorial", listaHistorial);
	       	accionweb.getSesion().setAttribute("listaHistorial", listaHistorial);
	    }
	    accionweb.agregarObjeto("listSede", listSede);
	    
	 
	    accionweb.agregarObjeto("rutnum",rutnum);
	    accionweb.agregarObjeto("dvrut",dvrut);
	    accionweb.agregarObjeto("estado",estado);
	    accionweb.agregarObjeto("unidad",unidad);
	    accionweb.agregarObjeto("estadoFinal",estadoFinal);
	    
	    accionweb.agregarObjeto("numVale",String.valueOf(numDoc));
	    if(rutnum == 0)
	    	 accionweb.agregarObjeto("identificador","1");
	    else
	    	accionweb.agregarObjeto("identificador","2");
	    accionweb.agregarObjeto("totalSolicitudVale", totalSolicitudVale);
	    accionweb.agregarObjeto("totalSolicitudPago", totalSolicitudPago);
	    accionweb.agregarObjeto("cuentasPresupuestarias",listaUnidades);
	    accionweb.agregarObjeto("nomIdentificador",nomIdentificador);
	    accionweb.agregarObjeto("sede", sede);
	    accionweb.agregarObjeto("valorAPagar", valorAPagar);
	    accionweb.agregarObjeto("identificadorInterno",identificadorInterno);   
		accionweb.agregarObjeto("hayDatoslista", "1");

		accionweb.agregarObjeto("esValePago", 1);
		
      	accionweb.agregarObjeto("opcion", tipo);
      	accionweb.agregarObjeto("titulo", titulo);
      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
      	accionweb.agregarObjeto("tipoPago",tipoPago);
    	accionweb.agregarObjeto("glosa1",glosa1);
    	accionweb.agregarObjeto("glosa2",glosa2);
    	accionweb.agregarObjeto("glosa3",glosa3);
    	accionweb.agregarObjeto("glosa4",glosa4);
    	accionweb.agregarObjeto("glosa5",glosa5);
    	accionweb.agregarObjeto("glosa6",glosa6);
      	accionweb.agregarObjeto("read",readonly);
      	accionweb.agregarObjeto("listaTipoPago", listaTipoPago);
      	accionweb.agregarObjeto("nombrePago", nombrePago);
      	accionweb.agregarObjeto("tipoPago", tipoPago);
      	accionweb.agregarObjeto("codEstadoFinal", codEstadoFinal);
      	
    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
     // 	System.out.println("termina consulta ");	
	}
	 synchronized public void ejecutaAccion(AccionWeb accionweb) throws Exception {
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		String estadoVales = Util.validaParametro(accionweb.getParameter("estadoVales"),"");
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
		
		switch (tipo){
		case 2: {
			this.actualizaValePago(accionweb);
			break;
		}
		case 3: case 8: case 9: case 10: {
			this.autorizaValePago(accionweb);
			break;
		}
		case 5:{
			this.recepcionaValePago(accionweb);
		break;
			
		}
		case 6:{// rechaza autorizaci�n
			this.rechazaAutValePago(accionweb);
		break;
			
		}
		case 7:{// rechaza Recepci�n
			this.rechazaRecepValePago(accionweb);
		break;
			
		}
		case 11:{// registra vale
			this.registraValedePago(accionweb);
		break;
			
		}
		}
		if(!estadoVales.trim().equals(""))
			accionweb.agregarObjeto("estadoVales", estadoVales);
		if(!estado.trim().equals(""))
			accionweb.agregarObjeto("estado", estado);
		if(tipo != 11)
			consultaVale(accionweb);
		
	}
	public void imprimeVale(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
	
		String nomIdentificador = "";
		long valorAPagar = 0;
		long rutnum = 0;
		String dvrut = "";
		String identificadorInterno = "";	
		long sede = 0;
		int identificador = 0; 
		
	    String nomcam = "";
	    long valnu1 = 0;
	    long valnu2 = 0;
	    String valalf = "";
	    String glosa1 = "";
	    String glosa2 = "";
	    String glosa3 = "";
	    String glosa4 = "";
	    String glosa5 = "";
	    String glosa6 = "";
	    String estadoFinal = "";
	    String nomSede = "";
	    String tipoPago = "";
	    String nomTipoPago = "";
		
		Presw19DTO preswbean19DTO = new Presw19DTO();
		String titulo = "";
		String tituloDetalle1 = "";
		preswbean19DTO.setTippro("OVA");
		preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
		preswbean19DTO.setDigide(dv);
		preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
		
		Vector vecSolicitudes = new Vector();	
		Vector vecSolicitudPago = new Vector();	
		Vector vecHistorial = new Vector();	
		List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
		CocowBean cocowBean = new CocowBean();
		listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
		 if(listCocow36DTO != null && listCocow36DTO.size() >0 ) {
			    for(Cocow36DTO ss: listCocow36DTO){
			   //  if(ss.getTiping().trim().equals("VAP")){
			    	if(ss.getNomcam().trim().equals("GLOSA1"))
			    		glosa1 = ss.getValalf();
			    	if(ss.getNomcam().trim().equals("GLOSA2"))
			    		glosa2 = ss.getValalf();
			     	if(ss.getNomcam().trim().equals("GLOSA3"))
			    		glosa3 = ss.getValalf();
			     	if(ss.getNomcam().trim().equals("GLOSA4"))
			    		glosa4 = ss.getValalf();
			     	if(ss.getNomcam().trim().equals("GLOSA5"))
			    		glosa5 = ss.getValalf();   
			     	if(ss.getNomcam().trim().equals("GLOSA6"))
			    		glosa6 = ss.getValalf();
		   
		            if(ss.getNomcam().trim().equals("RUTIDE"))
		            	rutnum = ss.getValnu1();
		            if(ss.getNomcam().trim().equals("DIGIDE"))
		            	dvrut = ss.getValalf();
		            if(ss.getNomcam().trim().equals("DESIDE"))
		                nomIdentificador = ss.getValalf();
		            if(ss.getNomcam().trim().equals("CODSUC"))
		            	sede = ss.getValnu1();
		            if(ss.getNomcam().trim().equals("VALPAG"))
		            	valorAPagar = ss.getValnu1();
		            if(ss.getNomcam().trim().equals("IDMEMO"))
		            	identificadorInterno = ss.getValalf();
		            if(ss.getNomcam().trim().equals("TIPVAL")){
		            	tipoPago = ss.getValalf();
		            	nomTipoPago = ss.getResval();
		            }
		            if(ss.getNomcam().trim().equals("CUENTA")){
		            	Vector vec = new Vector();
		            	vec.addElement(ss.getValalf());
		            	vec.addElement(ss.getValnu2());
		               	vec.addElement(ss.getResval());
		            	vecSolicitudes.addElement(vec);
		            	
		            }
		            if(ss.getNomcam().trim().equals("NOMBEN")){
		            	Vector vec2 = new Vector();
		             	vec2.addElement(ss.getValalf());		  		      
			            vec2.addElement(ss.getValnu1());
		               	vecSolicitudPago.addElement(vec2);
		            }
		            if(ss.getNomcam().trim().equals("HISTORIAL")){
		            	String fecha = "";
		            	String diaFecha = "";
		            	String mesFecha = "";
		            	String annoFecha = "";
		            	String valor = "";
		            	String valor2 = "";
		            	if(ss.getValnu1()> 0 ) {
		    				fecha = ss.getValnu1() + "";
		    				diaFecha = fecha.substring(6);
		    				mesFecha = fecha.substring(4,6);
		    				annoFecha = fecha.substring(0,4);
		    				valor = diaFecha+"/"+mesFecha+"/"+annoFecha;   
		            	} else {
		    				valor = ss.getValnu1()+"";
		            	} 
		            	if (ss.getResval().trim().equals("AUTORIZACION"))// se ocupa de la 1-15 el resto se ocupa la observacion del rechazo
		            	   valor +=  " por " + ss.getValalf() + " Cuenta " + ss.getValnu2() ;
		            	else
		            		valor += " por " + ss.getValalf();
		            	
		            	Vector vec3 = new Vector();
		            	vec3.addElement(ss.getResval()); // es la accion, INGRESO, MODIFICACION, AUTORIZACION, PAGO, AUTORIZA FINANZA				        
		            	vec3.addElement(valor); // es la fecha, 
		            	vecHistorial.addElement(vec3);
		            }
		            if(ss.getNomcam().trim().equals("ESTADO"))
		            	estadoFinal = ss.getValalf();
			    // }
			    }	
			    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
			    listSede = moduloValePago.getListaSede(accionweb.getReq());
			    for( Presw18DTO ss:listSede){
			    	if(ss.getNummes() == sede)
			    		nomSede = ss.getDesuni();
			    }
			 
               accionweb.getSesion().setAttribute("vecSolicitudes", vecSolicitudes);
               accionweb.getSesion().setAttribute("vecSolicitudPago", vecSolicitudPago);
               accionweb.getSesion().setAttribute("vecHistorial", vecHistorial);
               accionweb.getSesion().setAttribute("glosa1", glosa1);
               accionweb.getSesion().setAttribute("glosa2", glosa2);
               accionweb.getSesion().setAttribute("glosa3", glosa3);
               accionweb.getSesion().setAttribute("glosa4", glosa4);
               accionweb.getSesion().setAttribute("glosa5", glosa5);
               accionweb.getSesion().setAttribute("glosa6", glosa6);
               accionweb.getSesion().setAttribute("tipoPago", tipoPago);
               accionweb.getSesion().setAttribute("nomTipoPago", nomTipoPago);
               if(rutnum > 0)
            	   accionweb.getSesion().setAttribute("rutnum", moduloValePago.formateoNumeroSinDecimales(Double.parseDouble(rutnum+"")) + "-" + dvrut);
               accionweb.getSesion().setAttribute("nomIdentificador", nomIdentificador);
               accionweb.getSesion().setAttribute("nomSede", nomSede);
               accionweb.getSesion().setAttribute("valorAPagar", moduloValePago.formateoNumeroSinDecimales(Double.parseDouble(valorAPagar+"")));
               accionweb.getSesion().setAttribute("identificadorInterno", identificadorInterno);
               accionweb.getSesion().setAttribute("estadoFinal", estadoFinal);
               accionweb.getSesion().setAttribute("numDoc", String.valueOf(numDoc));
		 }
	}
	public void cargaIngresoValePago(AccionWeb accionweb) throws Exception {
		verificaRut(accionweb);
		if(accionweb.getReq().getAttribute("mensaje") == null || accionweb.getReq().getAttribute("mensaje").toString().trim().equals(""))
			cargaAutorizaValePago(accionweb);
		return;
	}
	public void cargaIngresoIngresador(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
		String nomUnidad = "";
		String titulo = "Nuevo Ingresador";
	
		 moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
		 List<Presw18DTO>    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		 if(listaUnidades != null && listaUnidades.size() > 0){
			 for (Presw18DTO ss : listaUnidades){
				 if(ss.getCoduni() == unidad)
					 nomUnidad = ss.getDesuni();
			 }
		 }
		accionweb.agregarObjeto("unidad", unidad);
		accionweb.agregarObjeto("nomUnidad", nomUnidad);
      	accionweb.agregarObjeto("esValePago", 1);
      	accionweb.agregarObjeto("opcion", tipo);
    	accionweb.agregarObjeto("opcionMenu", tipo);
      	accionweb.agregarObjeto("titulo", titulo);
	}
	 synchronized public void autorizaValePago(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		String nomTipo = "";
		String tipCue = "";
		String nomAut = "";
		switch(tipo){
		case 3: {
			nomTipo = "AUC";
			nomAut = " de Responsable de Cuenta. ";
			break;
		}
		case 8: {
			nomTipo = "AUE";
			tipCue = "Y";
			nomAut = " de Proyectos. ";
			break;
		}
		case 9: {
			nomTipo = "AUE";
			tipCue = "X";
			nomAut = " de UCP. ";
			break;
		}
		case 10: {
			nomTipo = "AUE";
			tipCue = "Z";
			nomAut = " de Director de presupuestos. ";
			break;
		}
		}
		try {	 
		boolean graba = moduloValePago.saveAutoriza(numdoc, rutUsuario,dv,nomTipo,tipCue);
	    if(graba)
	    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n" + nomAut);
	    else
	    	accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n" + nomAut);
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado autorizaValePago.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		Thread.sleep(500);
	    cargaAutorizaValePago(accionweb);
	    
		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	}
	 synchronized public void actualizaValePago(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudesPago");

	    boolean registra = false;
	    
	    registra = moduloValePago.getActualizaVale(accionweb.getReq(),rutUsuario,dv,numdoc);
	    if(registra){
	   	    accionweb.agregarObjeto("mensaje", "Se actualiz� en forma exitosa el vale de pago.");
	   	    if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				accionweb.agregarObjeto("registra", 1);
			    accionweb.getSesion().removeAttribute("listaSolicitudes");
			    if(listaSolicitudesPago != null)
			    	accionweb.getSesion().removeAttribute("listaSolicitudesPago");
			}
	   	// cargarMenu(accionweb);
	   	 consultaVale(accionweb);
	    } else
	    	accionweb.agregarObjeto("mensaje", "No se modific� el vale de pago.");
		  	accionweb.agregarObjeto("esValePago", 1);   
	 
		 
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	}
	 synchronized public void recepcionaValePago(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);

		try { 
		boolean graba = moduloValePago.saveRecepciona(numdoc, rutUsuario,dv,"REV");
	    if(graba)
	    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la Recepci�n.");
	    else
	    	accionweb.agregarObjeto("mensaje", "No se Grab� la Recepci�n.");
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado recepcionaValePago.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		Thread.sleep(500);
	    cargaAutorizaValePago(accionweb);
		
	}
	 synchronized public void rechazaRecepValePago(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);		
		String glosa1 = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "","");
		try{ 
		boolean graba = moduloValePago.saveRechaza(numdoc, rutUsuario,dv,"RVF",glosa1,"");
	    if(graba)
	    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el Rechazo de la Recepci�n.");
	    else
	    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Recepci�n.");
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado rechazaRecepValePago.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		Thread.sleep(500);
	    cargaAutorizaValePago(accionweb);
		
	}
	 synchronized public void rechazaAutValePago(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);	
		String glosa = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "","");
		try { 
		boolean graba = moduloValePago.saveRechaza(numdoc, rutUsuario,dv,"RVA", glosa,"");
	    if(graba)
	    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el rechazo de la Autorizaci�n.");
	    else
	    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Autorizaci�n.");
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado rechazaAutValePago.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		Thread.sleep(500);
	    cargaAutorizaValePago(accionweb);
		
	}
	public void verificaRut(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		int identificador = Util.validaParametro(accionweb.getParameter("identificador"), 0);
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		if(listaSolicitudes != null && listaSolicitudes.size() > 0)
			accionweb.getSesion().removeAttribute("listaSolicitudes");
		List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudesPago");
		if(listaSolicitudesPago != null && listaSolicitudesPago.size() > 0)
			accionweb.getSesion().removeAttribute("listaSolicitudesPago");
	
		String nombre = "";
		if(rutnum > 0){
			Collection<Presw18DTO> lista = moduloValePago.getVerificaRut(rutnum, dvrut);
			if(lista != null && lista.size() > 0){
				 for (Presw18DTO ss : lista){
					 nombre = ss.getDesuni();
				   }
			}
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
			
		}
	   	if(!nombre.trim().equals("")){
	    		accionweb.agregarObjeto("hayDatoslista", 1);
	    		accionweb.agregarObjeto("nomIdentificador", nombre);
	    } else
	    	accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado. Debe agregarlo.");
		
	    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
	    listSede = moduloValePago.getListaSede(accionweb.getReq());
	    String comboSede = "<select name=\"sede\" id=\"sede\" class=\"estilo_Azul\" onChange=\"\" >"+
	                       "<option value=\"-1\" selected=\"selected\">Seleccione</option> ";
	  //  if(identificador == 1)
	    for (Presw18DTO ss : listSede){
	    	comboSede += "<option value=" + ss.getNummes() + ">" + ss.getDesuni() + "</option>";
	    	
	    }
	    comboSede += "</select>";
	   
	    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
	    moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
	    List<Presw18DTO> listaTipoPago = new ArrayList<Presw18DTO>();
	    listaTipoPago = moduloValePago.getListaTopoPago(accionweb.getReq());
	    
  
	    
	    accionweb.agregarObjeto("comboSede", comboSede);
	    accionweb.agregarObjeto("listSede", listSede);
		accionweb.agregarObjeto("identificador", identificador);		  		  
		accionweb.agregarObjeto("esValePago", 1);   
		accionweb.agregarObjeto("listaTipoPago",listaTipoPago); 
	
	
		accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);  
    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		
	}
	
	public void verificaIdentificadorInterno(AccionWeb accionweb) throws Exception {
		String identificadorInterno = Util.validaParametro(accionweb.getParameter("identificadorInterno"),"");
		int sede = Util.validaParametro(accionweb.getParameter("sede"),0);
		if(!identificadorInterno.trim().equals("") && identificadorInterno.trim().length() <= 40){
		     if (moduloValePago.getIdentificadorDocumento(identificadorInterno.trim(), sede))
		    	 accionweb.agregarObjeto("mensaje", "Este identificador ya existe.");
		   	  
		     }
		
	}
	
	
	
	public void cargaResultadoSolicitudVale(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int cuentaPresup = Util.validaParametro(accionweb.getParameter("cuentaPresup"), 0);
		long valorAPagar = Util.validaParametro(accionweb.getParameter("valorAPagar"), 0);
		long valor = Util.validaParametro(accionweb.getParameter("valor"), 0);
		long total = 0;
	    

		
	    long saldoCuenta = moduloValePago.getBuscaSaldo(cuentaPresup);
	    moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    int sobrepasa = 0;
	    sobrepasa = moduloValePago.getAgregaListaSolicitaVale(accionweb.getReq(),saldoCuenta);
	    switch (sobrepasa){
	    case 1: {
	    	accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" >Este valor EXCEDE el saldo de la cuenta " + cuentaPresup + ", que es de: $ " + moduloValePago.formateoNumeroSinDecimales(Double.parseDouble(saldoCuenta+""))+"</font> .");
	    	break;
	    }
	    case 2: {
	    	accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"destacado\" size=\"12px\">Lo sentimos, este valor no se puede agregar a la cuenta, ya que excede el valor del Vale, que es de: $ " + moduloValePago.formateoNumeroSinDecimales(Double.parseDouble(valorAPagar+""))+"</font> .");
	    	break;
	    }
	    case 3: {
	    	accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"destacado\" size=\"12px\">Lo sentimos, esta cuenta ya se encuentra agregada </font> .");
	    	break;
	    }
	    }
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		if (listaSolicitudes != null && listaSolicitudes.size() > 0){
			accionweb.agregarObjeto("registra", 1);
		    accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes); 
		    for (Presw18DTO ss : listaSolicitudes)
				total = total + ss.getUsadom();
            accionweb.agregarObjeto("totalSolicitudVale", total);
		}
			
	   	accionweb.agregarObjeto("esValePago", 1);   
		accionweb.agregarObjeto("identificador", 1);  
	 
	
		
	}
	public void limpiaSolicitudPago(AccionWeb accionweb) throws Exception {
		List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudesPago");
		listaSolicitudesPago = null;
		accionweb.getSesion().removeAttribute("listaSolicitudesPago");
		accionweb.agregarObjeto("listaSolicitudesPago", listaSolicitudesPago);
	
	}

	public void limpiaSolicitudVale(AccionWeb accionweb) throws Exception {
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		listaSolicitudes = null;
		accionweb.getSesion().removeAttribute("listaSolicitudes");
		accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes);
	
	}
	
	public void cargaResultadoSolicitudPago(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
	//	String nomBeneficiarioOr = Util.validaParametro(accionweb.getParameter("nomBeneficiario"), "");
	//	long valorPago = Util.validaParametro(accionweb.getParameter("valorPago"), 0);
		long valorAPagar = Util.validaParametro(accionweb.getParameter("valorAPagar"), 0);
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		long total = 0;
		String nombre = "";
		long codigoBanco = 0;
		String nomBanco = "";
		String cuentaDeposito = "";
		List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO> )accionweb.getSesion().getAttribute("listaSolicitudesPago");
		if(listaSolicitudesPago != null && listaSolicitudesPago.size() > 0)
			accionweb.agregarObjeto("mensajeSolicitudPago", "S�lo puede agregar un beneficiario.");
		else {
			listaSolicitudesPago = new ArrayList<Presw18DTO>();
		if(rutnum > 0){
			Collection<Presw25DTO> lista = moduloValePago.getVerificaRutCliente(rutnum, dvrut);
			if(lista != null && lista.size() > 0){
				 for (Presw25DTO ss : lista){
					 nombre = ss.getDesuni().trim(); //+ " " + ss.getComen1().trim() + " " + ss.getComen2().trim();
					 codigoBanco = ss.getPres01();
					 nomBanco = ss.getComen6().trim();
					 cuentaDeposito = ss.getComen5().trim();
				   }
			}
				
		}
	   	if(!nombre.trim().equals("")){
	   		Presw18DTO presw18DTO = new Presw18DTO();
	   		presw18DTO.setDesuni(nombre.toUpperCase());
	   		presw18DTO.setUsadom(valorAPagar);
	   		presw18DTO.setRutide(rutnum);
	   		presw18DTO.setIddigi(dvrut);
	   		presw18DTO.setAcumum(codigoBanco); //codigoBanco
	   		presw18DTO.setDesite(nomBanco); // nomBanco
	   		presw18DTO.setNompro(cuentaDeposito) ; // cuentaDeposito
	   		
	   		listaSolicitudesPago.add(presw18DTO);
	    	accionweb.agregarObjeto("hayDatoslista", 1);
	        accionweb.getSesion().setAttribute("listaSolicitudesPago", listaSolicitudesPago);
	        accionweb.agregarObjeto("listaSolicitudesPago", listaSolicitudesPago);
	   	}else
	    	accionweb.agregarObjeto("mensajeSolicitudPago", "Lo sentimos, este RUT no se encuentra registrado, debe presionar el bot�n '+' si desea agregarlo.");
		   
    
		}
	    accionweb.agregarObjeto("listaSolicitudesPago", listaSolicitudesPago);
	   	accionweb.agregarObjeto("esValePago", 1);   
		accionweb.agregarObjeto("identificador", 1);  
	 
	
		
	}
	 synchronized public void registraValedePago(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudesPago");

	    int numVale = 0;
	    
	    numVale = moduloValePago.getRegistraVale(accionweb.getReq(),rutUsuario,dv);
	    if(numVale > 0){
	   	    accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa el vale de pago, el n�mero es: "+ numVale);
	   	    if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				accionweb.agregarObjeto("registra", 1);
			    accionweb.getSesion().removeAttribute("listaSolicitudes");
			    if(listaSolicitudesPago != null)
			    	accionweb.getSesion().removeAttribute("listaSolicitudesPago");
			}
	   	 cargarMenu(accionweb);
	    } else
	    	accionweb.agregarObjeto("mensaje", "No se registr� el vale de pago.");
		  	accionweb.agregarObjeto("esValePago", 1);   
	 
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		
	}
	 synchronized public void recepcionaMasivo(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			String linea = Util.validaParametro(accionweb.getParameter("linea"),"");
			int opcion = Util.validaParametro(accionweb.getParameter("opcion"), 0);
			int numdoc = 0;
			String texto = "";
			try { 
				int index = 1;
				boolean graba = false;
				String mensaje = "";
				while(linea.length() > 0){
					index = linea.indexOf("@");
					if(index > 0)
						numdoc = Integer.parseInt(linea.substring(0,index));
					else{
						numdoc = Integer.parseInt(linea);
						linea = "";
					}
					if(opcion == 3){
						graba = moduloValePago.saveRecepciona(numdoc, rutUsuario,dv,"AUC");
						texto = "Autorizaci&oacute;n";
					}
					if (opcion == 5){
						graba = moduloValePago.saveRecepciona(numdoc, rutUsuario,dv,"REV");
						texto = "Recepci&oacute;n";
					}
					    if(graba)
					    	mensaje += "Se grab&oacute; en forma exitosa la "+texto+" del Vale N " + numdoc + "<br>";
					    else
					    	mensaje += "No se Grab� la "+texto+" del Vale N " + numdoc;
					    linea = linea.substring(index+1);
				Thread.sleep(100);	
				}
			   	accionweb.agregarObjeto("mensaje", mensaje);
			   
				} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado recepcionaMasivo.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
			
			
		}
	 synchronized public void eliminaResultadoSolicitudVale(AccionWeb accionweb) throws Exception {
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
		   moduloValePago.getEliminaListaSolicitaVale(accionweb.getReq());
		   	accionweb.agregarObjeto("mensaje", "Se elimin� el cargo a la cuenta.");
		    	
			List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		   	accionweb.agregarObjeto("esValePago", 1);   
			accionweb.agregarObjeto("identificador", 1);  
			long total = 0;
			if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				accionweb.agregarObjeto("registra", 1);
			    accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes); 
			    for (Presw18DTO ss : listaSolicitudes)
					total = total + ss.getUsadom();
	            accionweb.agregarObjeto("totalSolicitudVale", total);
			}
			if(tipo > 0)
			 accionweb.agregarObjeto("opcion", tipo);
			
		}
	 synchronized public void eliminaResultadoSolicitudPago(AccionWeb accionweb) throws Exception {
		
		   moduloValePago.getEliminaListaSolicitaPago(accionweb.getReq());
		   	accionweb.agregarObjeto("mensaje", "Se elimin� el cargo del Beneficiario.");
		    	
			List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudesPago");
		   	accionweb.agregarObjeto("esValePago", 1);   
			accionweb.agregarObjeto("identificador", 1);  
			long total = 0;
			if (listaSolicitudesPago != null && listaSolicitudesPago.size() > 0){
				accionweb.agregarObjeto("registra", 1);
			    accionweb.agregarObjeto("listaSolicitudesPago", listaSolicitudesPago); 
			    for (Presw18DTO ss : listaSolicitudesPago)
					total = total + ss.getUsadom();
	            accionweb.agregarObjeto("totalSolicitudPago", total);
			}
			
		}
	 synchronized public void eliminaValePago(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		String glosa1 = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "","");	
		
		String nomTipo = "";
		String tipCue = "";
		String nomAut = "";
		switch(tipo){
		case 3:case 2: {
			// tipo == 2 es para los ingresadores
			nomTipo = "ANV"; // anulaci�n
			if(tipo == 3)
				nomAut = " de Responsable de Cuenta. ";
			break;
		}
		case 5: { // rechazo
			rechazaRecepValePago(accionweb);
			break;
		}
		case 8: { // rechazo
			
			nomTipo = "RVV";
			tipCue = "Y";
			nomAut = " de Proyectos. ";
			break;
		}
		case 9: {// rechazo
			nomTipo = "RVV";
			tipCue = "X";
			nomAut = " de UCP. ";
			break;
		}
		case 10: { // rechazo
			nomTipo = "RVV";
			tipCue = "Z";
			nomAut = " de Director de presupuestos. ";
			break;
		}
		}
		if(tipo == 2 || tipo == 3){
		    boolean resultado = moduloValePago.getEliminaValePago(rutUsuario,dv,numdoc, nomTipo, tipCue );
		    if(resultado)
		    	if(tipo == 2 || tipo == 3)
		    	    accionweb.agregarObjeto("mensaje", "Qued� anulado el Vale de pago " + nomAut);
		    	else
		    		accionweb.agregarObjeto("mensaje", "Qued� Rechazado el Vale de pago " + nomAut);
		    else
		    	accionweb.agregarObjeto("mensaje", "Problemas al anular el Vale de pago" + nomAut);
		}else if(tipo != 5){
			try{ 
				boolean graba = moduloValePago.saveRechaza(numdoc, rutUsuario,dv,nomTipo,glosa1,tipCue);
			    if(graba)
			    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el Rechazo del Vale de Pago.");
			    else
			    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo del Vale de Pago.");
				} catch (Exception e) {
					accionweb.agregarObjeto("mensaje", "No guardado rechazaValePago.");
					accionweb.agregarObjeto("exception", e);
					e.printStackTrace();
				}
		}
		    Thread.sleep(500);
		    this.cargaAutorizaValePago(accionweb);
			//accionweb.agregarObjeto("titulo","Modificaci�n");
			
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		
	}
	
	
	public void cargaCliente(AccionWeb accionweb) throws Exception {
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		List<Presw18DTO> listBanco = new ArrayList<Presw18DTO>();
		listBanco = moduloValePago.getListaBanco(accionweb.getReq());
		accionweb.agregarObjeto("listBanco", listBanco);
	
		accionweb.agregarObjeto("rutnum", rutnum);
 		accionweb.agregarObjeto("dvrut", dvrut);
 		accionweb.agregarObjeto("rut_aux", rut_aux); 
 	   	accionweb.agregarObjeto("esValePago", 1);   
    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	}
	public void validaCliente(AccionWeb accionweb) throws Exception {
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
				
		List<Presw25DTO> datosCliente = new ArrayList<Presw25DTO>();
		String nomCliente = "";
		long codigoBanco = 0;
		String nomBanco = "";
		String cuentaDeposito = "";
		if(rutnum > 0){
			Collection<Presw25DTO> lista = moduloValePago.getVerificaRutCliente(rutnum, dvrut);
			if(lista != null && lista.size() > 0){
				 for (Presw25DTO ss : lista){
					 // cambiado segun peticion 25/01/2016 nomCliente = ss.getDesuni();
					 nomCliente = ss.getMotiv1().trim() + " " + ss.getMotiv2().trim();
					 codigoBanco = ss.getPres01();
					 nomBanco = ss.getComen5();
					 cuentaDeposito = ss.getComen6();
				   }
			}
			accionweb.agregarObjeto("rut_aux", rut_aux);  
		 
			
		}
	   	if(!nomCliente.trim().equals("")){
	    		accionweb.agregarObjeto("hayDatoslista", 1);
	    		accionweb.agregarObjeto("nomIdentificador", nomCliente);
	    }
		
		List<Presw18DTO> listBanco = new ArrayList<Presw18DTO>();
		listBanco = moduloValePago.getListaBanco(accionweb.getReq());
		accionweb.agregarObjeto("listBanco", listBanco);
		if(!nomCliente.trim().equals(""))
		  accionweb.agregarObjeto("mensaje", "Este rut ya se encuentra registrado como cliente, no puede agregarlo");
	    else{
		  accionweb.agregarObjeto("nomCliente", nomCliente);		 		
		  accionweb.agregarObjeto("codigoBanco",codigoBanco);
		  accionweb.agregarObjeto("nomBanco",nomBanco);
		  accionweb.agregarObjeto("cuentaDeposito",cuentaDeposito);
		  accionweb.agregarObjeto("cliente", 1);
		  
	 	  }
	    accionweb.agregarObjeto("rutnum", rutnum);
        accionweb.agregarObjeto("dvrut", dvrut);
	    accionweb.agregarObjeto("rut_aux", rut_aux);	
	   	accionweb.agregarObjeto("esValePago", 1);   
    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));

	}
	
	 synchronized public void registraCliente(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		String nombre = Util.validaParametro(accionweb.getParameter("nombre"),"");
		String direccion = Util.validaParametro(accionweb.getParameter("direccion"),"");
		String comuna = Util.validaParametro(accionweb.getParameter("comuna"),"");
		String ciudad = Util.validaParametro(accionweb.getParameter("ciudad"),"");
		int fono = Util.validaParametro(accionweb.getParameter("fono"),0);
		long anexo = Util.validaParametro(accionweb.getParameter("anexo"),0);
		String fax = Util.validaParametro(accionweb.getParameter("fax"),"");
		String giro = Util.validaParametro(accionweb.getParameter("giro"),"");
		String cueBan = Util.validaParametro(accionweb.getParameter("cueBan"),"");
		int codBan = Util.validaParametro(accionweb.getParameter("codBan"),0);
		String correo = Util.validaParametro(accionweb.getParameter("correo"),"");
		String finCorreo = Util.validaParametro(accionweb.getParameter("finCorreo"),"");

		
		String nom = moduloValePago.eliminaAcentosString(nombre);
		if(nom.trim().length() > 70)
			nom = nom.substring(0,69);
		String dir = moduloValePago.eliminaAcentosString(direccion);
		if(dir.trim().length() > 40)
			dir = dir.substring(0,39);
		String com = moduloValePago.eliminaAcentosString(comuna);
		String ciu = moduloValePago.eliminaAcentosString(ciudad);
		String fa = moduloValePago.eliminaAcentosString(fax);
		String gir = moduloValePago.eliminaAcentosString(giro);
		String corr = moduloValePago.eliminaAcentosString(correo);
		String finCorr = moduloValePago.eliminaAcentosString(finCorreo);

		List<Presw21DTO> datosCliente = new ArrayList<Presw21DTO>();
		String nomCliente = "";
		String correoProveedor = "";
		long codigoBanco = 0;
		String nomBanco = "";
		String cuentaDeposito = "";
		
		if(rutnum > 0){
			Collection<Presw25DTO> lista = moduloValePago.getVerificaRutCliente(rutnum, dvrut);
			if(lista != null && lista.size() > 0){
				 for (Presw25DTO ss : lista){
					 //nomCliente = ss.getDesuni();  cambiado febrero 2016
					 nomCliente = ss.getMotiv1() + ss.getMotiv2();
					 codigoBanco = ss.getPres01();
					 nomBanco = ss.getComen5();
					 cuentaDeposito = ss.getComen6();
				   }
			}
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
			
		}
	   	if(!nomCliente.trim().equals("")){
		
	   		accionweb.agregarObjeto("mensaje", "Este RUT ya se encuentra en nuestros registros, no puede agregarse."); 
		} else { 
	// se agrega que el nombre sea guardado en dos campos
			String nom1 = "";
			String nom2 = "";
			if(nom.trim().length() > 40) {
				nom1 = nom.substring(0,40);
				nom2 = nom.substring(40);
			} else
				nom1 = nom;
			List<Presw25DTO>  lista = new ArrayList<Presw25DTO>();
			Presw25DTO presw25 = new Presw25DTO();
			presw25.setTippro("CLI");
			presw25.setRutide(rutnum);
			presw25.setDigide(dvrut);
		//	presw25.setComen1(nom.toUpperCase());  ya no se graba aca
			presw25.setMotiv3(nom1.toUpperCase());
			presw25.setMotiv4(nom2.toUpperCase());
			presw25.setComen2(dir.toUpperCase());
			presw25.setComen3(com.toUpperCase());
			presw25.setComen4(ciu.toUpperCase());
			presw25.setPres01(fono);
			presw25.setPres02(anexo);
			presw25.setComen5(fa.toUpperCase());
			presw25.setComen6(gir.toUpperCase());
			presw25.setPres03(codBan);
			presw25.setMotiv1(cueBan.trim());
			presw25.setMotiv2(corr.trim()+"@"+finCorr.trim());
	
			lista.add(presw25);
			try {
		 	 boolean graba = moduloValePago.saveCliente(lista,rutUsuario);
		 	if (graba)	  
		 		 accionweb.agregarObjeto("mensaje", "Este Cliente se registr� en forma exitosa.");
		 	else
		 		 accionweb.agregarObjeto("mensaje", "Este Cliente NO se registr�.");
			Thread.sleep(500); 
			} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado registraCliente.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
			}
	        accionweb.agregarObjeto("cliente", 1);		
	  	   	accionweb.agregarObjeto("esValePago", 1);
	  		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		}
	}
	public ModuloValePago getModuloValePago() {
		return moduloValePago;
	}
	public void setModuloValePago(ModuloValePago moduloValePago) {
		this.moduloValePago = moduloValePago;
	}

	public void verificaRutIngresador(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String nomUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"),"");
		
	
		String nombre = "";
		String indprc = "";
		if(rutnum > 0){
			Collection<Presw18DTO> lista = moduloValePago.getVerificaRut(rutnum, dvrut);
			if(lista != null && lista.size() > 0){
				 for (Presw18DTO ss : lista){
					 nombre = ss.getDesuni();
					 indprc = ss.getIndprc();
				   }
			}
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
			accionweb.agregarObjeto("unidad", unidad); 
			accionweb.agregarObjeto("nomUnidad", nomUnidad); 
			
		}
	   	if(!nombre.trim().equals("")){
	   		if(indprc.trim().equals("F")){
	    		accionweb.agregarObjeto("hayDatoslista", 1);
	    		accionweb.agregarObjeto("nomIngresador", nombre);
	   		} else {
	   			accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado como Funcionario.");
	   			
	   		}
	    } else
	    	accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado.");
	  	accionweb.agregarObjeto("esValePago", 1);  
	  	accionweb.agregarObjeto("titulo", "Nuevo Ingresador");  
	 		
	}
	
}
