package cl.utfsm.siv.modulo;
/**
 * SISTEMA DE VALE DE PAGOS POR INTERNET
 * PROGRAMADOR		: 	M�NICA BARRERA FREZ
 * FECHA ULT.MODIF  : 	09/08/2011
 * UNIDAD DE DESARROLLO INSTITUCIONAL(DTI)   
 */
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;

import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.siv.CUENTAPRESUPUESTARIA;
import cl.utfsm.siv.LISTAVALEPAGO;
import cl.utfsm.siv.PRESF61;
import cl.utfsm.siv.datos.ValePagoDao;
import descad.cliente.CocofBean;
import descad.cliente.Ingreso_Documento;
import descad.cliente.MD5;
import descad.cliente.PreswBean;

import descad.documentos.Cocow36DTO;
import descad.presupuesto.Cocof17DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw21DTO;
import descad.presupuesto.Presw25DTO;

public class ModuloValePago {
    	ValePagoDao valePagoDao;

		public ValePagoDao getValePagoDao() {
			return valePagoDao;
		}

		public void setValePagoDao(ValePagoDao valePagoDao) {
			this.valePagoDao = valePagoDao;
		}

 public Collection<Presw18DTO> getConsulta(String tippro, int codUnidad, int item, int anno, int mes, int rutIde){
	PreswBean preswbean = new PreswBean(tippro, codUnidad, item, anno, mes, rutIde);
	Collection<Presw18DTO> listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
	return listaPresw18;
 }
 public void getCuentasAutorizadas( HttpServletRequest req, int rutide){
	 HttpSession sesion = req.getSession();
	 Collection<Presw18DTO> listaPresw18 = null;
	 
	 listaPresw18 = this.getConsulta("VCA", 0, 0, 0, 0, rutide);
	 List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
		
	 for (Presw18DTO ss : listaPresw18){
		 Presw18DTO cuentapresupuestaria = new Presw18DTO();
		 cuentapresupuestaria.setCoduni(ss.getCoduni());
		 cuentapresupuestaria.setNomtip(ss.getNomtip());
		 cuentapresupuestaria.setDesuni(ss.getDesuni());
		 cuentapresupuestaria.setIndprc(ss.getIndprc());
		 lista.add(cuentapresupuestaria);
	 }
	 
	 
	 sesion.setAttribute("cuentasPresupuestarias", lista);
	return ;
	 }
 public boolean getAgregaDistribucionPago( HttpServletRequest req, long valorAPagar, long valorPago, String desuni){
	 HttpSession sesion = req.getSession();
	 List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudesPago");
     long total = 0;
     boolean sobrepasa = false;
 	
	if(listaSolicitudesPago != null && listaSolicitudesPago.size() > 0){
		 for (Presw18DTO ss : listaSolicitudesPago)
				total = total + ss.getUsadom();
	} else {		
		listaSolicitudesPago = new ArrayList<Presw18DTO>();
	}
	total += valorPago;
	if(total <= valorAPagar) {
		 Presw18DTO presw18DTO = new Presw18DTO();
		 presw18DTO.setDesuni(desuni);
		 presw18DTO.setUsadom(valorPago);
		 listaSolicitudesPago.add(presw18DTO);
	 } else sobrepasa = true;	 
	 
	 sesion.setAttribute("listaSolicitudesPago", listaSolicitudesPago);
	 
	return sobrepasa;
	 }	
	public List<Cocof17DTO> getListaSede(AccionWeb accionweb) throws Exception {
		/*	esto es en caso de que se muestren todas las sedes */
		  CocofBean cocofBean = new CocofBean();
		    List<Cocof17DTO> listCocofBean = new ArrayList<Cocof17DTO>();
		    listCocofBean = cocofBean.buscar_todos_cocof17();	
		  
				
		   
		return listCocofBean;
		}
 
 
	public List<Presw18DTO> getListaSede( HttpServletRequest req) throws Exception {
	/*	esto es en caso de que se muestren las sedes que puede ver el usuario */
		int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
	
		PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		List<Presw18DTO> list = new ArrayList<Presw18DTO>();
		String nombre = "";
		preswbean = new PreswBean("SED",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			   for (Presw18DTO ss : lista){
				   Presw18DTO presw18DTO = new Presw18DTO();
				   presw18DTO.setNummes(ss.getNummes());
				   presw18DTO.setDesuni(ss.getDesuni());
				   list.add(presw18DTO);
			   }	
		   
	return list;
	}
	 
	public List<Presw18DTO> getListaTopoPago( HttpServletRequest req) throws Exception {
	/*	se muestran todos los tipos de pago */
		int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
	
		PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		List<Presw18DTO> list = new ArrayList<Presw18DTO>();
		Vector<String> miVector = new Vector<String>();
		Vector vec = new Vector();
		Vector vec2 = new Vector();
			
		String nombre = "";
		preswbean = new PreswBean("TVP",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		
			   for (Presw18DTO ss : lista){
				//   System.out.println(ss.getDesuni()+" - "+ ss.getTipmov());
				   miVector.add(ss.getDesuni());
				   vec2.add(ss.getTipmov());
				   vec.add(ss.getDesuni());
				 
			   }
			    Collections.sort(miVector);
			    int i = 0;
			   for(String sElemento: miVector){   
				//   System.out.println(sElemento);
				   i = 0;
				   Presw18DTO presw18DTO = new Presw18DTO();
				   presw18DTO.setDesuni(sElemento);
				   i = vec.indexOf(sElemento);
				   presw18DTO.setTipmov(vec2.get(i)+"");
				   list.add(presw18DTO);				
			   }
			   
				
		   
	return list;
	}
/* public void getListaSede( HttpServletRequest req){
	 HttpSession sesion = req.getSession();
		
	 
	 /*esto me lo debe enviar Rudy -- solo de prueba
	
	    List<LISTAVALEPAGO> listaValePago = new ArrayList<LISTAVALEPAGO>();
        LISTAVALEPAGO valePago = new LISTAVALEPAGO();
        valePago.setNumeroVale("1");
        valePago.setNumeroVale("001");
        valePago.setDescripcion("deascripcion");
        valePago.setDestinatario("Destinatario 1");
        valePago.setFecha(new Date("01/01/2010"));
        valePago.setEstado("Autorizado");
        valePago.setValor(20000);
        listaValePago.add(valePago);
        valePago = new LISTAVALEPAGO();
        valePago.setNumeroVale("2");
        valePago.setNumeroVale("002");
        valePago.setDescripcion("deascripcionv 2");
        valePago.setEstado("Autorizado");
        valePago.setDestinatario("Destinatario 2");
        valePago.setFecha(new Date("30/01/2011"));
        valePago.setValor(40000);
        listaValePago.add(valePago);
        
        valePago = new LISTAVALEPAGO();
        valePago.setNumeroVale("3");
        valePago.setNumeroVale("003");
        valePago.setDescripcion("deascripcion 3");
        valePago.setDestinatario("Destinatario 3");
        valePago.setEstado("Autorizado");
        valePago.setFecha(new Date("03/02/2011"));
        valePago.setValor(30000);
        listaValePago.add(valePago);	 
        
        
	 sesion.setAttribute("listaValePago", listaValePago);*/
/*	return;
	 }*/
 public boolean getIdentificadorDocumento(String identificador, int sede){
		// falta que Rudy indique si existe
	    // la consulta debe realizarse en la tabla cocof100, buscar si existe este identificador en el campo idmemo
	 // lo realizar� con el proceso VMI
	    PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		preswbean = new PreswBean("VMI",0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
		Presw25DTO presw25 = new Presw25DTO();
		presw25.setNumreq(sede);
		presw25.setComen1(identificador);
		lista = (Collection<Presw18DTO>) preswbean.ingreso_doc_presw25(presw25);
	
	    boolean existe = false;
	    if(lista != null && lista.size() > 0)
	    	existe = true;
	 return existe;
	}
 public Collection<Presw18DTO> getVerificaRut(int rutnum, String dv){
	  	PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		if(rutnum > 0){
			preswbean = new PreswBean("VRU",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
			lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		}
	
	 return lista;
	}
 
 public long getBuscaSaldo(int codUni){
	  	PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		long saldoCuenta = 0;
		preswbean = new PreswBean("SCU",0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
		preswbean.setCoduni(codUni);
			lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			   for (Presw18DTO ss : lista){
				   saldoCuenta = ss.getUsadom();
			   }	
	 return saldoCuenta;
	}
 
 public int getAgregaListaSolicitaVale( HttpServletRequest req, long saldoCuenta){
	 HttpSession sesion = req.getSession();
     long valor = Util.validaParametro(req.getParameter("valor"), 0);
     long valorAPagar = Util.validaParametro(req.getParameter("valorAPagar"), 0);
     int cuentaPresup = Util.validaParametro(req.getParameter("cuentaPresup"), 0);
 	 String desuni = Util.validaParametro(req.getParameter("desuni"),"");
     
	 List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudes");
	 if(listaSolicitudes != null && listaSolicitudes.size()> 0)
		 sesion.removeAttribute("listaSolicitudes");
	 long total = 0;
	 long totalTotal = 0;
	 int sobrepasa = 0;
	 String muestraSaldo = "";
	 NumberTool numbertool = new NumberTool();
	 muestraSaldo = String.valueOf(numbertool.format(saldoCuenta));
	 if(listaSolicitudes != null && listaSolicitudes.size() > 0){
		 for (Presw18DTO ss: listaSolicitudes){
			 if(ss.getCoduni() == cuentaPresup)
				 sobrepasa = 3;
			 totalTotal += ss.getUsadom();
		 }
		 totalTotal += valor;
	 } else total = valor;
	 if(sobrepasa == 0) {
	 if(total > saldoCuenta){
		 sobrepasa = 1;
		 muestraSaldo = "<font class=\"Estilo_Rojo\" >" + muestraSaldo + "</font>";
	 }
	// else { lo dej� comentado, ya que la petici�n fue solo enviar mensaje en caso de que el saldo no alcance
		 if(totalTotal <= valorAPagar) {
		if(listaSolicitudes == null)
			listaSolicitudes = new ArrayList<Presw18DTO>();
		 Presw18DTO presw18DTO = new Presw18DTO();
		 presw18DTO.setCoduni(cuentaPresup);
		 presw18DTO.setUsadom(valor);
		 presw18DTO.setDesuni(desuni);
		 presw18DTO.setNompro(muestraSaldo);
		 listaSolicitudes.add(presw18DTO);
		 } else sobrepasa = 2;
	 //}
	 }
	 sesion.setAttribute("listaSolicitudes", listaSolicitudes);
	return sobrepasa;	
 }
 public void getEliminaListaSolicitaVale( HttpServletRequest req){
	 HttpSession sesion = req.getSession();
     long valor = Util.validaParametro(req.getParameter("usadom"), 0);
     int cuentaPresup = Util.validaParametro(req.getParameter("coduni"), 0);
     String desuni = Util.validaParametro(req.getParameter("desuni"),"");
     
	 List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudes");
	 List<Presw18DTO> listaVales = new ArrayList<Presw18DTO>();
	 if(listaSolicitudes != null && listaSolicitudes.size()> 0)
		 sesion.removeAttribute("listaSolicitudes");
	 if(listaSolicitudes != null && listaSolicitudes.size() > 0){
		 for (Presw18DTO ss: listaSolicitudes){
			 if(ss.getCoduni() == cuentaPresup && ss.getUsadom() == valor && ss.getDesuni().trim().equals(desuni.trim()))
				 continue;
			 else
			 {
				 Presw18DTO presw18DTO = new Presw18DTO();
				 presw18DTO.setCoduni(ss.getCoduni());
				 presw18DTO.setUsadom(ss.getUsadom());
				 presw18DTO.setDesuni(ss.getDesuni());
				 presw18DTO.setDesite(ss.getDesite());
				 presw18DTO.setNompro(ss.getNompro());
				 listaVales.add(presw18DTO);
			 }
		 }
		 
	
	 }
	// if(listaVales.size() > 0)
	   sesion.setAttribute("listaSolicitudes", listaVales);
		
 }
 public void getEliminaListaSolicitaPago( HttpServletRequest req){
	 HttpSession sesion = req.getSession();
     long valor = Util.validaParametro(req.getParameter("usadom"), 0);
     String desuni = Util.validaParametro(req.getParameter("desuni"),"");
     
	 List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudesPago");
	 List<Presw18DTO> listaVales = new ArrayList<Presw18DTO>();
	 if(listaSolicitudesPago != null && listaSolicitudesPago.size()> 0)
		 sesion.removeAttribute("listaSolicitudesPago");
	 if(listaSolicitudesPago != null && listaSolicitudesPago.size() > 0){
		 for (Presw18DTO ss: listaSolicitudesPago){
			 if(ss.getUsadom() == valor && ss.getDesuni().trim().equals(desuni.trim()))
				 continue;
			 else
			 {
				 Presw18DTO presw18DTO = new Presw18DTO();
				 presw18DTO.setUsadom(ss.getUsadom());
				 presw18DTO.setDesuni(ss.getDesuni());
				 presw18DTO.setRutide(ss.getRutide());
				 presw18DTO.setIddigi(ss.getIddigi());
				 listaVales.add(presw18DTO);
			 }
		 }
		 
	
	 }
	 if(listaVales.size() > 0)
	   sesion.setAttribute("listaSolicitudesPago", listaVales);
		
 }
 public boolean getEliminaValePago( int rut, String dv, int numdoc, String nomTipo, String tipCue ){
	  	PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		long saldoCuenta = 0;
		preswbean = new PreswBean(nomTipo,0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
		preswbean.setRutide(rut);
		preswbean.setDigide(dv);
		preswbean.setNumdoc(numdoc);
		preswbean.setTipcue(tipCue);
		
		return preswbean.ingreso_presw19();	
		
 } 
 
 public List<Cocow36DTO> getAgregaLista(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf){
	 Cocow36DTO cocow36DTO = new Cocow36DTO();
	 cocow36DTO.setTiping(tiping);
	 cocow36DTO.setNomcam(nomcam);
	 cocow36DTO.setValnu1(valnu1);
	 cocow36DTO.setValnu2(valnu2);
	 cocow36DTO.setValalf(valalf);
	 lista.add(cocow36DTO);
	 return lista;
	 
 }
 public List<Cocow36DTO> getAgregaListaMD5(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf, String resval){
	 Cocow36DTO cocow36DTO = new Cocow36DTO();
	 cocow36DTO.setTiping(tiping);
	 cocow36DTO.setNomcam(nomcam);
	 cocow36DTO.setValnu1(valnu1);
	 cocow36DTO.setValnu2(valnu2);
	 cocow36DTO.setValalf(valalf);
	 cocow36DTO.setResval(resval);
	 lista.add(cocow36DTO);
	 return lista;
	 
 }
 public List<Integer> getListaA�o(int a�oActual) {
	 List<Integer> lista = new ArrayList<Integer>();
	
	 for(int i = 2010 ; i <= a�oActual;i++){
		 lista.add(i);
	 }
	  return lista;
   }
	public boolean saveAutoriza(int numdoc, int rutUsuario, String digide, String nomTipo, String tipCue){
		PreswBean preswbean = new PreswBean(nomTipo, 0,0,0,0, rutUsuario);
		preswbean.setDigide(digide);
		preswbean.setNumdoc(numdoc);
		preswbean.setTipcue(tipCue);
	return preswbean.ingreso_presw19();
	}
	public boolean saveRecepciona(int numdoc, int rutUsuario, String digide, String tipo){
		PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
		preswbean.setDigide(digide);
		preswbean.setNumdoc(numdoc);
		
	return preswbean.ingreso_presw19();
	}
	public boolean saveRechaza(int numdoc, int rutUsuario, String digide, String tipo, String glosa, String tipCue){
		PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
		preswbean.setDigide(digide);
		preswbean.setNumdoc(numdoc);
		preswbean.ingreso_presw19();
		
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		presw25DTO.setPres01(numdoc);
		presw25DTO.setComen1(glosa);
		presw25DTO.setRutide(rutUsuario);
		presw25DTO.setDigide(digide);
		presw25DTO.setIndexi(tipCue);
		lista.add(presw25DTO);
	return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	}

	public List<Cocow36DTO> getCreaCocow36(HttpServletRequest req, int rutide, String digide){
		List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO>) req.getSession().getAttribute("listaSolicitudesPago");
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) req.getSession().getAttribute("listaSolicitudes");
		String nomIdentificador = Util.validaParametro(req.getParameter("nomIdentificador"), "");
		long valorAPagar = Util.validaParametro(req.getParameter("valorAPagar"), 0);
		long rutnum = Util.validaParametro(req.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(req.getParameter("dvrut"),"");
		String identificadorInterno = Util.validaParametro(req.getParameter("identificadorInterno"),"");	
		String glosa1 = Util.validaParametro(req.getParameter("glosa1"),"");	
		String glosa2 = Util.validaParametro(req.getParameter("glosa2"),"");
		String glosa3 = Util.validaParametro(req.getParameter("glosa3"),"");
		String glosa4 = Util.validaParametro(req.getParameter("glosa4"),"");
		String glosa5 = Util.validaParametro(req.getParameter("glosa5"),"");
		String glosa6 = Util.validaParametro(req.getParameter("glosa6"),"");
		String tipoPago = Util.validaParametro(req.getParameter("tipoPago"),"");
		long sede = Util.validaParametro(req.getParameter("sede"), 0);
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		
	    String tiping = "VAP";
	    String nomcam = "";
	    long valnu1 = 0;
	    long valnu2 = 0;
	    String valalf = "";
	

    /* prepara archivo para grabar, llenar listCocow36DTO*/
    nomcam = "RUTIDE";
    valnu1 = rutnum;
    valnu2 = 0;
    valalf = "";
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    nomcam = "DIGIDE";
    valnu1 = 0;
    valnu2 = 0;
    valalf = dvrut;
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    nomcam = "DESIDE";
    valnu1 = 0;
    valnu2 = 0;
    valalf = nomIdentificador;
    valalf = this.eliminaAcentosString(nomIdentificador);
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    nomcam = "CODSUC";
    valnu1 = sede;
    valnu2 = 0;
    valalf = "";
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    nomcam = "VALPAG";
    valnu1 = valorAPagar;
    valnu2 = 0;
    valalf = "";
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    nomcam = "IDMEMO";
    valnu1 = 0;
    valnu2 = 0;
    valalf = identificadorInterno;
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    nomcam = "GLOSA1";
    valnu1 = 0;
    valnu2 = 0;
    valalf = glosa1;
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

    nomcam = "GLOSA2";
    valnu1 = 0;
    valnu2 = 0;
    valalf = glosa2;
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

    nomcam = "GLOSA3";
    valnu1 = 0;
    valnu2 = 0;
    valalf = glosa3;
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    nomcam = "GLOSA4";
    valnu1 = 0;
    valnu2 = 0;
    valalf = glosa4;
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

    nomcam = "GLOSA5";
    valnu1 = 0;
    valnu2 = 0;
    valalf = glosa5;
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

    nomcam = "GLOSA6";
    valnu1 = 0;
    valnu2 = 0;
    valalf = glosa6;
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    nomcam = "TIPVAL";
    valnu1 = 0;
    valnu2 = 0;
    valalf = tipoPago;
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    /* 25/05/2012 agregar MD5*/
    nomcam = "CODMD5";
    MD5 md5 = new MD5();
    String texto =md5.getMD5(rutnum + dvrut.trim() + nomIdentificador.trim() + valorAPagar) ;
    valnu1 = 0;
    valnu2 = 0;
    valalf = texto;
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    for (Presw18DTO ss : listaSolicitudes){
    	nomcam = "CUENTA";
 	    valnu1 = ss.getCoduni();
 	    valnu2 = ss.getUsadom();
 	    valalf = "";
 	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    }
		
   if(listaSolicitudesPago != null && listaSolicitudesPago.size() > 0){
	   for (Presw18DTO ss : listaSolicitudesPago){
		    md5 = new MD5();
		    texto = md5.getMD5("0" + "" + ss.getDesuni().trim() + ss.getUsadom()) ;
		 
	    	nomcam = "NOMBEN";
	 	    valnu1 = ss.getUsadom();
	 	    valnu2 = ss.getRutide();
	 	    valalf = ss.getDesuni();
	 	    // agregado 10/05/2013 para indicar el rut del beneficiario, adem�s del nombre
	 		 Cocow36DTO cocow36DTO = new Cocow36DTO();
	 		 cocow36DTO.setTiping(tiping);
	 		 cocow36DTO.setNomcam(nomcam);
	 		 cocow36DTO.setValnu1(valnu1);
	 		 cocow36DTO.setValnu2(valnu2);
	 		 cocow36DTO.setValalf(valalf);
	 		 cocow36DTO.setAccion(ss.getIddigi());
	 		 cocow36DTO.setResval(texto);
	 		 listCocow36DTO.add(cocow36DTO);
	 	   // listCocow36DTO = getAgregaListaMD5(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf,texto );
	    }
	   
   }
	return listCocow36DTO;
	}
	
	synchronized  public int getRegistraVale(HttpServletRequest req, int rutide, String digide){
		int numVale = 0;
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		listCocow36DTO = getCreaCocow36(req, rutide, digide);
		Ingreso_Documento ingresoDocumento = new Ingreso_Documento("GVA",rutide,digide);
			
		numVale = ingresoDocumento.Ingresar_Documento(listCocow36DTO);
		
	
		return numVale;
	}
	public boolean getActualizaVale(HttpServletRequest req, int rutide, String digide, int numDoc){
		boolean registra = false;
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		listCocow36DTO = getCreaCocow36(req, rutide, digide);
		Ingreso_Documento ingresoDocumento = new Ingreso_Documento("MVA",rutide,digide);
		ingresoDocumento.setNumdoc(numDoc);
			
		registra = ingresoDocumento.Actualizar_Documento(listCocow36DTO);
		
	
		return registra;
	}
	public boolean saveCliente(List<Presw25DTO> lista,int rutUsuario){
		PreswBean preswbean = new PreswBean("CLI", 0, 0, 0, 0, rutUsuario);
		
		return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	} 
	public String formateoNumeroSinDecimales(Double numero){
		/*formatear n�mero */
	     Locale l = new Locale("es","CL");
	     DecimalFormat formatter1 = (DecimalFormat)DecimalFormat.getInstance(l);
	     formatter1.applyPattern("###,###,###");
	 
	   //  formatter1.format(rs.getDouble("RUTNUM"))   /*sin decimales*/
	 return formatter1.format(numero);
	}
	public String formateoNumeroConDecimales(Double numero){
		/*formatear n�mero */
	     Locale l = new Locale("es","CL");
	     DecimalFormat formatter2 = (DecimalFormat)DecimalFormat.getInstance(l);
	     formatter2.applyPattern("###,###.###");

	   //  formatter2.format(rs.getDouble("UTMSUS"))   /*con decimales*/
	     return formatter2.format(numero);
	}
	
	 public List<Presw18DTO> getConsultaAutoriza(int rutnum, String dv){
		  	PreswBean preswbean = null;
			Collection<Presw18DTO> lista = null;
			List<Presw18DTO> listaAutoriza =  new ArrayList<Presw18DTO>();
			String nombre = "";
			if(rutnum > 0){
				preswbean = new PreswBean("PAU",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
				lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
				   for (Presw18DTO ss : lista){
					   Presw18DTO presw18DTO = new Presw18DTO();
					   presw18DTO.setIndprc(ss.getIndprc());
					   listaAutoriza.add(presw18DTO);
				   }
						
			}
		
		 return listaAutoriza;
		}
	 public boolean saveDeleteIngresador( int rut, String dv, int coduni, String tipcue ){
		  	PreswBean preswbean = null;
			Collection<Presw18DTO> lista = null;
			long saldoCuenta = 0;
			preswbean = new PreswBean("ALI",0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
			preswbean.setRutide(rut);
			preswbean.setDigide(dv);
			preswbean.setCoduni(coduni);
			preswbean.setTipcue(tipcue);
			
			return preswbean.ingreso_presw19();	
			
	 } 
	 public String eliminaAcentosString(String sTexto){
			/*elimina acentos del string*/
		   String linea = "";  
		    if(!sTexto.trim().equals("")){
		    for (int x=0; x < sTexto.length(); x++) {
		    	  if ((sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') || 
		    	      (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
		    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') || 
		    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
		    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
		    		  sTexto.charAt(x) == '\''){
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'a';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'A';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'e';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'E';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'i';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'I';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'o';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'O';
		    		   if(sTexto.charAt(x) == '�' )
		    			  linea += 'u';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'U';
		    		  if(sTexto.charAt(x) == '\'')
		    			  linea += ' ';
		    	  }
		    	  else  linea += sTexto.charAt(x);
		    	}				
		    }
		
		 return linea;
		}
	 public List<Presw18DTO> getListaBanco( HttpServletRequest req) throws Exception {
			/*	se muestran todos los bancos */
				int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
			
				PreswBean preswbean = null;
				Collection<Presw18DTO> lista = null;
				List<Presw18DTO> list = new ArrayList<Presw18DTO>();
				String nombre = "";
				preswbean = new PreswBean("BAN",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
					lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
					   for (Presw18DTO ss : lista){
						   Presw18DTO presw18DTO = new Presw18DTO();
						   presw18DTO.setItedoc(ss.getItedoc());
						   presw18DTO.setDesuni(ss.getDesuni());
						   list.add(presw18DTO);
					   }	
				   
			return list;
			}
	 public Collection<Presw25DTO> getVerificaRutCliente(int rutnum, String dv){
		  	PreswBean preswbean = null;
			Collection<Presw25DTO> lista = null;
			if(rutnum > 0){
				preswbean = new PreswBean("VCL",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
				lista = (Collection<Presw25DTO>) preswbean.consulta_presw25();
					}
		
		 return lista;
		}
}
