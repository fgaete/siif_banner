package cl.utfsm.siv;

public class PRESF61 {
	private int coduni;
	private int rutide;
	private String digide;
	private String nivaut;
	public int getCoduni() {
		return coduni;
	}
	public void setCoduni(int coduni) {
		this.coduni = coduni;
	}
	public int getRutide() {
		return rutide;
	}
	public void setRutide(int rutide) {
		this.rutide = rutide;
	}
	public String getDigide() {
		return digide;
	}
	public void setDigide(String digide) {
		this.digide = digide;
	}
	public String getNivaut() {
		return nivaut;
	}
	public void setNivaut(String nivaut) {
		this.nivaut = nivaut;
	}

}
