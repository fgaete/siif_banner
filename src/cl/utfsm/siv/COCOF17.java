package cl.utfsm.siv;

public class COCOF17 {
	private int codsuc;
	private String nomsuc;
	public int getCodsuc() {
		return codsuc;
	}
	public void setCodsuc(int codsuc) {
		this.codsuc = codsuc;
	}
	public String getNomsuc() {
		return nomsuc;
	}
	public void setNomsuc(String nomsuc) {
		this.nomsuc = nomsuc;
	}
}
