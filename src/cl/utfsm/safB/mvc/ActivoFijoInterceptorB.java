package cl.utfsm.safB.mvc;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.safB.modulo.ModuloActivoFijoB;

public class ActivoFijoInterceptorB extends HandlerInterceptorAdapter {
	private ModuloActivoFijoB moduloActivoFijoB;

	
	public ModuloActivoFijoB getModuloActivoFijoB() {
		return moduloActivoFijoB;
	}
	public void setModuloActivoFijoB(ModuloActivoFijoB moduloActivoFijoB) {
		this.moduloActivoFijoB = moduloActivoFijoB;
	}
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
	}
	public void cargarMenu(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		accionweb.agregarObjeto("esActivoFijoB", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		
		
	}
	public void cargaDocumentos(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
	    Vector  resultadoImport = (Vector) accionweb.getSesion().getAttribute("resultadoImport");
		accionweb.agregarObjeto("esActivoFijoB", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		accionweb.agregarObjeto("resultadoImport", resultadoImport);
		
		
	}
	public void registraDocumento(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
	    Vector  resultadoImport = (Vector) accionweb.getSesion().getAttribute("resultadoImport");
		accionweb.agregarObjeto("esActivoFijoB", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		accionweb.agregarObjeto("resultadoImport", resultadoImport);
		String mensaje = "";
		mensaje = moduloActivoFijoB.updteDocumentos(resultadoImport);
	/*	for(int i=0; i < resultadoImport.size() - 1 ; i++){
			int error = 0;
			error = moduloTesoreriaB.updteDocumentos(resultadoImport.get(i).toString());
			mensaje += "<br> el doc. N� "+resultadoImport.get(i).toString()+ ((error>=0)?" OK":" Error");
		}*/
		accionweb.agregarObjeto("esActivoFijoB", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		accionweb.agregarObjeto("mensaje", mensaje);
		accionweb.getSesion().removeAttribute("resultadoImport");
	}
	
}
