package cl.utfsm.safB.modulo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import cl.utfsm.base.util.ConexionBanner;
import cl.utfsm.safB.datos.ActivoFijoDao;

public class ModuloActivoFijoB {
	ActivoFijoDao activoFijoDao;

	public ActivoFijoDao getActivoFijoDao() {
		return activoFijoDao;
	}

	public void setActivoFijoDao(ActivoFijoDao activoFijoDao) {
		this.activoFijoDao = activoFijoDao;
	}

	synchronized public String updteDocumentos(Vector resultadoImport) throws Exception {
		ConexionBanner con = new ConexionBanner();
		con.conexion().setAutoCommit(false);
		PreparedStatement sent;
		String mensaje = "";
		for(int i=0; i < resultadoImport.size()  ; i++){
			int error = -1;		
			try{ 
				sent = con.conexion().prepareStatement(" SELECT FFBMAST_OTAG_CODE " +
													   " FROM FFBMAST" +
						                               " WHERE FFBMAST_OTAG_CODE = ?" );
				
		        //sent.setString(1,resultadoImport.get(i).toString());
				sent.setString(1,String.valueOf(((Vector)resultadoImport.get(i)).get(0)).trim());
				ResultSet resul = null;
				resul = sent.executeQuery();
				if(resul.next()){
					 if(resul.getString(1).trim().equals(String.valueOf(((Vector)resultadoImport.get(i)).get(0)).trim()))
					  error = 0;
				   else error = -2;
				} else error = -3;
				sent.close();
			//System.out.println("error: "+error);	
			if(error == 0) {
					sent = con.conexion().prepareStatement(" UPDATE fimsmgr.FFBMAST SET FFBMAST_INVH_CODE = ? , FFBMAST_VEND_PIDM = ?" +
							                          	   " WHERE FFBMAST_OTAG_CODE = ?" );
					
			
					sent.setString(1,String.valueOf(((Vector)resultadoImport.get(i)).get(1)).trim());
					sent.setString(2,String.valueOf(((Vector)resultadoImport.get(i)).get(2)).trim());
					sent.setString(3,String.valueOf(((Vector)resultadoImport.get(i)).get(0)).trim());
					int res = sent.executeUpdate();
					if(res < 0){
					error = -5;    	  
					} else error = res;
				
			
			}
			//System.out.println("error: 2"+error);
				if (error >= 0){
			        mensaje += "<br> El doc. N� "+((Vector)resultadoImport.get(i)).get(0)+ " Ejecutado, "+error+" registro(s) actualizado(s).";
				        
				         }
				else {
					mensaje += "<br> El doc. N� "+((Vector)resultadoImport.get(i)).get(0)+ " No Ejecuta, no encuentra("+error+")";
				}
				sent.close();
			}
			catch (SQLException e ) {
			System.out.println("Error en moduloActivoFijoB.updteDocumentos.FFBMAST : " + e.toString() );
			error = -4; 
			con.close();
			} 
				if(error >= 0) con.conexion().commit();

			    else con.conexion().rollback();		
		}
		
		con.close();
		return mensaje;
	}


	

}
