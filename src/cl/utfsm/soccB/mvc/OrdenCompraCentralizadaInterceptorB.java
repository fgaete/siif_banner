package cl.utfsm.soccB.mvc;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


import cl.utfsm.POJO.Producto;
import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.sgf.modulo.ModuloFinanza;
import cl.utfsm.sip.mvc.PresupuestoInterceptor;
import cl.utfsm.sivB.modulo.ModuloValePagoB;
import cl.utfsm.soccB.modulo.ModuloOrdenCompraCentralizadaB;
import descad.cliente.CocowBean;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.ItemDTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw21DTO;
import descad.presupuesto.Presw25DTO;

public class OrdenCompraCentralizadaInterceptorB extends HandlerInterceptorAdapter{
		static Logger log = Logger.getLogger(PresupuestoInterceptor.class);
		private ModuloOrdenCompraCentralizadaB moduloOrdenCompraCentralizadaB;
		private ModuloValePagoB moduloValePagoB;
		
		public ModuloValePagoB getModuloValePagoB() {
			return moduloValePagoB;
		}
		public void setModuloValePagoB(ModuloValePagoB moduloValePagoB) {
			this.moduloValePagoB = moduloValePagoB;
		}
		public static Logger getLog() {
		return log;
	}
	public static void setLog(Logger log) {
		OrdenCompraCentralizadaInterceptorB.log = log;
	}



	public ModuloOrdenCompraCentralizadaB getModuloOrdenCompraCentralizadaB() {
		return moduloOrdenCompraCentralizadaB;
	}
	public void setModuloOrdenCompraCentralizadaB(
			ModuloOrdenCompraCentralizadaB moduloOrdenCompraCentralizadaB) {
		this.moduloOrdenCompraCentralizadaB = moduloOrdenCompraCentralizadaB;
	}
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		
	}	
	public void cargarOrdenCompra(AccionWeb accionweb) throws Exception {
		int opcion = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
      	accionweb.agregarObjeto("control", control);
  		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
   		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
   		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
   		
   	  	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
            accionweb.getSesion().removeAttribute("autorizaProyecto");
        if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
            accionweb.getSesion().removeAttribute("autorizaUCP");
        if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
            accionweb.getSesion().removeAttribute("autorizaDIRPRE");
        if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
    	    accionweb.getSesion().removeAttribute("autorizaFinanzas");
        if(accionweb.getSesion().getAttribute("listaOrganizaciones") != null)
      		accionweb.getSesion().setAttribute("listaOrganizaciones", null);
        if(accionweb.getSesion().getAttribute("totalListaOrganizaciones") != null)
      		accionweb.getSesion().setAttribute("totalListaOrganizaciones", null);
        if(accionweb.getSesion().getAttribute("totalListaProductos") != null)
      		accionweb.getSesion().setAttribute("totalListaProductos", null);
        if(accionweb.getSesion().getAttribute("valorRemanente") != null)
      		accionweb.getSesion().setAttribute("valorRemanente", null);
        if(accionweb.getSesion().getAttribute("procesoSesion") != null)
      		accionweb.getSesion().setAttribute("procesoSesion", null);
        

    	/* se debe buscar segun el usuario si tiene permiso para autorizar*/
        List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
		moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
		listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
    	if(listaUnidades != null && listaUnidades.size() > 0){
    		 for (Presw18DTO ss : listaUnidades){
    			if(ss.getIndprc().trim().equals("Y")){
    				accionweb.agregarObjeto("autorizaProyecto", 1);
    				accionweb.getSesion().setAttribute("autorizaProyecto", 1);
    			}
    			if(ss.getIndprc().trim().equals("X")){
    				accionweb.agregarObjeto("autorizaUCP", 1);
    				accionweb.getSesion().setAttribute("autorizaUCP", 1);
    			}
    			if(ss.getIndprc().trim().equals("Z")){
    				accionweb.agregarObjeto("autorizaDIRPRE", 1);
    				accionweb.getSesion().setAttribute("autorizaDIRPRE", 1);
    			}
    			if(ss.getIndprc().trim().equals("F")){
    				accionweb.agregarObjeto("autorizaFinanzas", 1);
    				accionweb.getSesion().setAttribute("autorizaFinanzas", 1);
    			}
    		 }
    	}    	
    	if(accionweb.getSesion().getAttribute("valorRemanente") != null) {
      	   accionweb.getSesion().removeAttribute("valorRemanente");
      	   accionweb.getSesion().setAttribute("valorRemanente", 0);
    	}	
    	
    	List<String> listaCampus = moduloOrdenCompraCentralizadaB.getCampus();
    	accionweb.agregarObjeto("listaCampus", listaCampus);
    	
    	Map<String, String> listaDivisa = moduloOrdenCompraCentralizadaB.getDivisas();
    	accionweb.agregarObjeto("listaDivisa", listaDivisa);
    	
    	List<Producto> listaProducto = moduloOrdenCompraCentralizadaB.getProductos();
    	accionweb.agregarObjeto("listaProducto", listaProducto);
    	
        String fechaActual = new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
    	accionweb.agregarObjeto("fechaEntregaIng", fechaActual);
    	accionweb.agregarObjeto("importacion", "N");
    	accionweb.agregarObjeto("obsequio", "N");
    	accionweb.agregarObjeto("divisa", "CLP");
    	

    	if(rutOrigen > 0){
    			this.limpiaSimulador(accionweb);
    		}
    	 		    			
    	accionweb.agregarObjeto("opcion", opcion);
    	accionweb.agregarObjeto("esOrdenCompraCB", 1); // es para mostrar el menu  de Orden Compra
    	accionweb.agregarObjeto("titulo", "Ingreso");
	
	}
	public void cargaEdificio(AccionWeb accionweb) throws Exception {
		String campus = Util.validaParametro(accionweb.getParameter("campus")+ "","");
	   	
		List<String> listaEdificio = moduloOrdenCompraCentralizadaB.getEdificio(campus);
		accionweb.agregarObjeto("listaEdificio", listaEdificio);
	}
	public void cargaPiso(AccionWeb accionweb) throws Exception {
		String campus = Util.validaParametro(accionweb.getParameter("campus")+ "","");
		String edificio = Util.validaParametro(accionweb.getParameter("edificio")+ "","");
	   	
		List<String> listaPiso = moduloOrdenCompraCentralizadaB.getPiso(campus, edificio);
		accionweb.agregarObjeto("listaPiso", listaPiso);
		
	}
	public void cargaOficina(AccionWeb accionweb) throws Exception {
		String campus = Util.validaParametro(accionweb.getParameter("campus")+ "","");
		String edificio = Util.validaParametro(accionweb.getParameter("edificio")+ "","");
		String piso = Util.validaParametro(accionweb.getParameter("piso")+ "","");
		
		List<String> listaOficina = moduloOrdenCompraCentralizadaB.getOficina(campus, edificio, piso);
		accionweb.agregarObjeto("listaOficina", listaOficina);
		
	}

	public void cargarCodigoEnvio(AccionWeb accionweb) throws Exception {
	 String campus   = Util.validaParametro(accionweb.getParameter("campus"),"");
	 String edificio = Util.validaParametro(accionweb.getParameter("edificio"),"");
	 String piso = Util.validaParametro(accionweb.getParameter("piso"),"");
	 String oficina = Util.validaParametro(accionweb.getParameter("oficina"),"");
	
	 String codigoEntrega = "";
	 String descEntrega = "";
	
	 if (!campus.trim().equals("") && !edificio.trim().equals("") && !piso.trim().equals("") && !oficina.trim().equals("")){
	   List<String> lista = new ArrayList<String>();
	   lista = moduloOrdenCompraCentralizadaB.getCodigoEnvio(campus,edificio,piso,oficina);
	   
	   if (lista != null && lista.size() > 0){
		    codigoEntrega = lista.get(0).toString();
            descEntrega   = lista.get(1).toString();		      
            accionweb.agregarObjeto("codigoEntrega", codigoEntrega);
            accionweb.agregarObjeto("descEntrega", descEntrega);
	      }  
		}
	  else  accionweb.agregarObjeto("mensaje", "Este c�digo de envio no existe.");
	   	  
	}

	
	
	public void limpiaSimulador(AccionWeb accionweb) throws Exception {
		if (accionweb.getSesion().getAttribute("listaUnidad") != null)
			accionweb.getSesion().setAttribute("listaUnidad", null);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutOrigen  = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen") +"","0"));
		String nomSimulacion = "";
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		
		/*hacer este rut rutUsuario*/
		if(rutOrigen != 0) // lo vuelve a lo original
		{
			rutUsuario = rutOrigen ;
			rutOrigen = 0;
		}
		 
		
		accionweb.getSesion().setAttribute("rutUsuario", rutUsuario);
		accionweb.getSesion().setAttribute("rutOrigen", rutOrigen);
		accionweb.getSesion().setAttribute("nomSimulacion", nomSimulacion);
		accionweb.agregarObjeto("rutOrigen", rutOrigen);
		accionweb.agregarObjeto("nomSimulacion", nomSimulacion);
		
	}
	public void cargarDetalleProducto(AccionWeb accionweb) throws Exception {
		String producto   = Util.validaParametro(accionweb.getParameter("producto"),"");
		String obsequio   = Util.validaParametro(accionweb.getParameter("obsequio"),"");
		
		String detalle = "";
		String unidadMed = "";
		String tipoImpuesto = "";
		String cuenta = "";
		String nomCuenta = "";
		
		if (!producto.trim().equals("")){
		   List<String> lista = new ArrayList<String>();
		   lista = moduloOrdenCompraCentralizadaB.getDetalleProducto(producto, obsequio);
		   
		   if (lista != null && lista.size() > 0){
			    detalle   = lista.get(0).toString();
			    unidadMed = lista.get(1).toString();
			    tipoImpuesto  = lista.get(2).toString();
			    cuenta = lista.get(3).toString();
			    nomCuenta = lista.get(4).toString();
	            accionweb.agregarObjeto("detalle", detalle);
	            accionweb.agregarObjeto("unidadMed", unidadMed );
	            accionweb.agregarObjeto("cuenta", cuenta);
	            accionweb.agregarObjeto("nomCuenta", nomCuenta);
	            accionweb.agregarObjeto("tipoImpuesto", tipoImpuesto);
	            accionweb.agregarObjeto("codProd", producto);
		      }  
			}
		  else  {
			  accionweb.agregarObjeto("unidadMed", "" );
	          accionweb.agregarObjeto("cuenta", "");
	          accionweb.agregarObjeto("nomCuenta", "");
	          accionweb.agregarObjeto("tipoImpuesto", "");
		  }
		   	  
		}
	
	/*public void cargarValorTotalProducto(AccionWeb accionweb) throws Exception {
		int cantidad = Util.validaParametro(accionweb.getParameter("cantidad"),0);
		long precio   = Util.validaParametro(accionweb.getParameter("precio"),0);
		String tipoImpuesto = Util.validaParametro(accionweb.getParameter("tipoImpuesto"),"");
		
		long totalProd = 0;
		long totalTotal = 0;
		long impuesto = 0;
		if (cantidad > 0 && precio > 0){
			   totalProd = precio * cantidad;
			   if (tipoImpuesto.equals("19IV")) 
				    impuesto = (long) (0.19*totalProd);
			   else if (tipoImpuesto.equals("ICIV"))
				       impuesto = (long) (0.24*totalProd);
			   else if (tipoImpuesto.equals("IA35"))
			       impuesto = (long) (0.35*totalProd);
			   else if (tipoImpuesto.equals("IHIV"))
			       impuesto = (long) (0.31*totalProd);
			   else if (tipoImpuesto.equals("ILA3"))
			       impuesto = (long) (0.395*totalProd);
			   else if (tipoImpuesto.equals("ILA1"))
			       impuesto = (long) (0.505*totalProd);
			   else if (tipoImpuesto.equals("ALCI"))
			       impuesto = (long) (0.47*totalProd);
			   else impuesto = 0;
		   
		   totalTotal = totalProd + impuesto;
		   accionweb.agregarObjeto("impuesto", moduloOrdenCompraCentralizadaB.formateoNumeroConDecimales(Double.parseDouble(impuesto+"")));
		   accionweb.agregarObjeto("totalProd", totalProd);
		   accionweb.agregarObjeto("totalTotal",moduloOrdenCompraCentralizadaB.formateoNumeroConDecimales(Double.parseDouble(totalTotal+"")));
		}
	}*/
	public void cargarValorTotalProducto(AccionWeb accionweb) throws Exception {
		int cantidad = Util.validaParametro(accionweb.getParameter("cantidad"),0);
		long precio   = Util.validaParametro(accionweb.getParameter("precio"),0);
		String tipoImpuesto = Util.validaParametro(accionweb.getParameter("tipoImpuesto"),"");
		String producto = Util.validaParametro(accionweb.getParameter("producto"),"");
		String obsequio = Util.validaParametro(accionweb.getParameter("obsequio"),"");
		MathTool mathtool = new MathTool();	
		
		if (!producto.trim().equals("") && tipoImpuesto.trim().equals("")){
			   List<String> lista = new ArrayList<String>();
			   lista = moduloOrdenCompraCentralizadaB.getDetalleProducto(producto,obsequio);
			   
			   if (lista != null && lista.size() > 0){
				    tipoImpuesto  = lista.get(2).toString();
				  }
			}
		
		
		long totalProd = 0;
		long totalTotal = 0;
		long impuesto = 0;
		if (cantidad > 0 && precio > 0){
			   totalProd = precio * cantidad;
			   if (tipoImpuesto.equals("19IV")) 
				    impuesto = (long) (0.19*totalProd);
			   else if (tipoImpuesto.equals("ICIV"))
				       impuesto = (long) (0.24*totalProd);
			   else if (tipoImpuesto.equals("IA35"))
			       impuesto = (long) (0.35*totalProd);
			   else if (tipoImpuesto.equals("IHIV"))
			       impuesto = (long) (0.31*totalProd);
			   else if (tipoImpuesto.equals("ILA3"))
			       impuesto = (long) (0.395*totalProd);
			   else if (tipoImpuesto.equals("ILA1"))
			       impuesto = (long) (0.505*totalProd);
			   else if (tipoImpuesto.equals("ALCI"))
			       impuesto = (long) (0.47*totalProd);
			   else impuesto = 0;
		   
		   totalTotal = totalProd + impuesto;
		   accionweb.agregarObjeto("impuesto", moduloOrdenCompraCentralizadaB.formateoNumeroConDecimales(Double.parseDouble(Math.round(impuesto)+"")));
		   accionweb.agregarObjeto("totalProd", totalProd);
		   accionweb.agregarObjeto("totalTotal",moduloOrdenCompraCentralizadaB.formateoNumeroConDecimales(Double.parseDouble(totalTotal+"")));
		}
	}
	
	
	/*public void cargarValorRemanente(AccionWeb accionweb) throws Exception {
		long porcentajeAsignado   = Util.validaParametro(accionweb.getParameter("porcentajeAsignado"),0);
		float valorTotal = Util.validaParametro(accionweb.getParameter("valorTotal"),0);
		
		List<Presw18DTO> listaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaOrganizaciones");
		long valorRemanente = Util.validaParametro(accionweb.getSesion().getAttribute("valorRemanente"),0);
		
		if (valorRemanente == 0) {
		   if(accionweb.getSesion().getAttribute("listaOrganizaciones") != null)
			  accionweb.getSesion().removeAttribute("listaOrganizaciones");
	   }
		
		if (valorTotal > 0){
		   long remanente = 0;
		   float suma = 0;
		   float porc  = 0;
		   accionweb.getSesion().removeAttribute("valorRemanente");
		   if (listaOrganizaciones != null && listaOrganizaciones.size() > 0) {
			   for (Presw18DTO ss : listaOrganizaciones){
				suma = suma + ss.getPresac();
			   }
			   porc = (100*suma)/valorTotal; // porcentaje ya asignado
			   remanente = 100 - Math.round(porc) - porcentajeAsignado;
			
			   accionweb.getSesion().setAttribute("valorRemanente", remanente);
			   accionweb.agregarObjeto("valorRemanente",remanente);
		   }
		   else { 
			   remanente = 100 - porcentajeAsignado;
			   accionweb.getSesion().setAttribute("valorRemanente", remanente);
			   accionweb.agregarObjeto("valorRemanente",remanente);
		   }
		   
	  }	// cantidad > 0
	  	
	}*/
	public void cargarValorRemanente(AccionWeb accionweb) throws Exception {
		long valorAsignado   = Util.validaParametro(accionweb.getParameter("porcentajeAsignado"),0);
		float totalProd = Util.validaParametro(accionweb.getParameter("valorTotal"),0);
		List<Presw18DTO> listaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaOrganizaciones");
		long valorRemanente = Util.validaParametro(accionweb.getSesion().getAttribute("valorRemanente"),0);
		
		if (valorRemanente == 0) {
		   if(accionweb.getSesion().getAttribute("listaOrganizaciones") != null)
			  accionweb.getSesion().removeAttribute("listaOrganizaciones");
	   }
		
	   float suma = 0;
	   float porcentaje = 0; 
	   long remanente = 0;
	   accionweb.getSesion().removeAttribute("valorRemanente");
	   if (listaOrganizaciones != null && listaOrganizaciones.size() > 0) {
		   for (Presw18DTO ss : listaOrganizaciones){
			     suma = suma + ss.getPresac();
		   }
		   porcentaje = (100*suma)/totalProd; // porcentaje ya asignado
		   System.out.println("(100-Math.round(porcentaje)" + (100-Math.round(porcentaje)));
		    remanente = 100 - Math.round(porcentaje) - valorAsignado;
		   
		   accionweb.getSesion().setAttribute("valorRemanente", remanente);
		   accionweb.agregarObjeto("valorRemanente",remanente); 
	   }
	   else { // totalListaOrganizaciones != null
		   remanente = 100 - valorAsignado;
		   accionweb.getSesion().setAttribute("valorRemanente", remanente);
		   accionweb.agregarObjeto("valorRemanente",remanente);
	   }
	}
	
	public void cargarOpcionMenu(AccionWeb accionweb) throws Exception {
		int opcion = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		accionweb.agregarObjeto("opcion", opcion);
		
	}
	
	public void cargaResultadoDistribucionProducto(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		String cuenta = Util.validaParametro(accionweb.getParameter("cuenta").trim(),"");	
		String nomCuenta = Util.validaParametro(accionweb.getParameter("nomCuenta").trim(),"");
		long porcAsignado = Util.validaParametro(accionweb.getParameter("porcentajeAsignado"), 0);
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion").trim(), "");
		String nomOrganizacion = Util.validaParametro(accionweb.getParameter("nomOrganizacion").trim(),"");
		String producto = Util.validaParametro(accionweb.getParameter("producto"),"");
		float valorTotal = Util.validaParametro(accionweb.getParameter("valorTotal"), 0);
		int opcion = Util.validaParametro(accionweb.getParameter("opcion"), 0);
		accionweb.agregarObjeto("opcion", opcion);
		
		
		List<Presw18DTO> listaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaOrganizaciones");
		List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
		List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("totalListaProductos");
				
		try {
			 long saldoCuenta = moduloOrdenCompraCentralizadaB.getBuscaSaldo(organizacion.trim(),cuenta.trim());
			 int secuencia = 0;
			 if(totalListaOrganizaciones == null) 
				 totalListaOrganizaciones = new ArrayList<Presw18DTO>();

			 if(totalListaProductos == null) 
			     secuencia = 1;
			 else {
				 for (Presw25DTO ss : totalListaProductos){
					if (producto.equals(ss.getCodman().trim()))
					   secuencia = ss.getNumreq();
					else  secuencia = ss.getNumreq() + 1;
			 }
			 }
			 float valorAsignado = Math.round((valorTotal*porcAsignado)/100);
				
			 if(listaOrganizaciones == null) 
				listaOrganizaciones = new ArrayList<Presw18DTO>();
	
			  Presw18DTO presw18DTO = new Presw18DTO();
			  presw18DTO.setNummes(secuencia);
			  presw18DTO.setNomtip(organizacion.trim());
			  presw18DTO.setDesuni(nomOrganizacion.trim());
			  presw18DTO.setNompro(cuenta.trim());
			  presw18DTO.setDesite(nomCuenta.trim());
			  presw18DTO.setPresac((long) valorAsignado);
			  presw18DTO.setPresum(saldoCuenta);
			  listaOrganizaciones.add(presw18DTO);
			  totalListaOrganizaciones.add(presw18DTO);
			  accionweb.getSesion().setAttribute("listaOrganizaciones",listaOrganizaciones);
			  accionweb.getSesion().setAttribute("totalListaOrganizaciones",totalListaOrganizaciones);
			 // accionweb.getSesion().setAttribute("porcentajeAsignado", 0);
		     // this.cargarValorRemanente(accionweb);
	
		    
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "cargaResultadoDistribucionProducto.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
	 
		accionweb.agregarObjeto("listaOrganizaciones", listaOrganizaciones); 		
		accionweb.agregarObjeto("totalListaOrganizaciones", totalListaOrganizaciones); 
                accionweb.agregarObjeto("totalListaProductos", totalListaProductos);
                accionweb.agregarObjeto("registra", 1);	
	   	accionweb.agregarObjeto("esOrdenCompraCB", 1);   
	}
	public void cargaResultadoDistribucionProductoNuevo(AccionWeb accionweb) throws Exception {
		/***
		 * falta arreglar este metodo
		 * 
		 * 
		 * 
		 * ******/
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		/*String cuenta = Util.validaParametro(accionweb.getParameter("cuenta"),"");	
		String nomCuenta = Util.validaParametro(accionweb.getParameter("nomCuenta"),"");
		long porcAsignado = Util.validaParametro(accionweb.getParameter("porcentajeAsignado"), 0);
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"), "");
		String nomOrganizacion = Util.validaParametro(accionweb.getParameter("nomOrganizacion"),"");*/
		int numreq = Util.validaParametro(accionweb.getParameter("numreq"),0);
		float valorTotalNuevo = Util.validaParametro(accionweb.getParameter("valorTotalnuevo"), 0);
		float valorTotal = Util.validaParametro(accionweb.getParameter("valorTotal"), 0);
		int opcion = Util.validaParametro(accionweb.getParameter("opcion"), 0);
		accionweb.agregarObjeto("opcion", opcion);
		
		
		List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
					
		try {
			 int porc = 0;
			 
			 if(totalListaOrganizaciones == null) 
				 totalListaOrganizaciones = new ArrayList<Presw18DTO>();

			 if(totalListaOrganizaciones != null) 
			
				 for (Presw18DTO ss : totalListaOrganizaciones){
					if (ss.getNummes() == numreq){
					  porc = Math.round((ss.getPresac() * 100)/valorTotal);
					  ss.setAcumum(Math.round((valorTotalNuevo*porc)/100));
					}
				 }
			  accionweb.getSesion().setAttribute("totalListaOrganizaciones",totalListaOrganizaciones);
			  accionweb.agregarObjeto("totalListaOrganizaciones",totalListaOrganizaciones);
			  accionweb.agregarObjeto("numreq", numreq);
			 // accionweb.getSesion().setAttribute("porcentajeAsignado", 0);
		     // this.cargarValorRemanente(accionweb);
	
		    
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "cargaResultadoDistribucionProducto.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
	 
		accionweb.agregarObjeto("registra", 1);	
	   	accionweb.agregarObjeto("esOrdenCompraCB", 1);   
	}

	public void cargaResultadoProducto(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		String producto = Util.validaParametro(accionweb.getParameter("producto"),"");
		String nomProducto = Util.validaParametro(accionweb.getParameter("nomProducto"),"");
		long cantidad = Util.validaParametro(accionweb.getParameter("cantidad"), 0);
		String obsequio = Util.validaParametro(accionweb.getParameter("obsequio"),"");
		long precio = Util.validaParametro(accionweb.getParameter("precio"), 0);
		long impuesto = Util.validaParametro(accionweb.getParameter("impuesto"), 0);
		String cuenta = Util.validaParametro(accionweb.getParameter("cuenta"),"");	
		String detalle = Util.validaParametro(accionweb.getParameter("detalle"),"");
		int opcion = Util.validaParametro(accionweb.getParameter("opcion"), 0);
		accionweb.agregarObjeto("opcion", opcion);
			
		List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("totalListaProductos");
		try {
			int secuencia = 0;
			if(totalListaProductos == null) {
				 totalListaProductos = new ArrayList<Presw25DTO>();
			     secuencia = 1;
			  }
			 else {
				 for (Presw25DTO ss : totalListaProductos){
					secuencia = ss.getNumreq() + 1;
				 }
			 }
			
			if(totalListaProductos == null) 
			     totalListaProductos = new ArrayList<Presw25DTO>(); 
			
			  Presw25DTO presw25DTO = new Presw25DTO();
			  presw25DTO.setNumreq(secuencia); // secuencia
			  presw25DTO.setCodman(producto.trim()); // codigo del prodcto
			  presw25DTO.setComen2(nomProducto.trim()); // nombre del prodcto
			  presw25DTO.setPres01(cantidad); // cantidad de productos
			  presw25DTO.setIndmod(obsequio.trim()); // indicador obsequio
			  presw25DTO.setPres02(precio); // precio
			  presw25DTO.setPres03(impuesto); // impuesto
			  presw25DTO.setComen1(cuenta.trim());
			  int largo = detalle.length();
			  if (largo > 40  ) { 
			      presw25DTO.setMotiv1(detalle.substring(0,40));  // detalle
			      detalle = detalle.substring(41,detalle.length());
			      largo = detalle.length();
			      if (largo > 40  ) { 
				      presw25DTO.setMotiv2(detalle.substring(0,40));  // detalle
				      detalle = detalle.substring(41,detalle.length());
				      largo = detalle.length();
				      if (largo > 40  ) { 
					      presw25DTO.setMotiv3(detalle.substring(0,40));  // detalle
					      detalle = detalle.substring(41,detalle.length());
					      largo = detalle.length();
					      if (largo > 40  ) { 
						      presw25DTO.setMotiv4(detalle.substring(0,40));  // detalle
						      detalle = detalle.substring(41,detalle.length());
						      largo = detalle.length();
						      if (largo > 40  ) { 
							      presw25DTO.setMotiv5(detalle.substring(0,40));  // detalle
							      detalle = detalle.substring(41,detalle.length());
							      largo = detalle.length();
							      if (largo > 40  ) { 
								      presw25DTO.setMotiv6(detalle.substring(0,40));  // detalle
								      detalle = detalle.substring(41,detalle.length());
								  } else  presw25DTO.setMotiv6(detalle.trim());   
							  } else  presw25DTO.setMotiv5(detalle.trim());  
						  } else presw25DTO.setMotiv4(detalle.trim());  
					  } else  presw25DTO.setMotiv3(detalle.trim());  
				  } else   presw25DTO.setMotiv2(detalle.trim());  
			  } else  presw25DTO.setMotiv1(detalle.trim());   
			  totalListaProductos.add(presw25DTO);
			  
			  
			 List<Presw25DTO> listaOrganizaciones = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listaOrganizaciones"); 
			 if(accionweb.getSesion().getAttribute("listaOrganizaciones") != null)
			    accionweb.getSesion().removeAttribute("listaOrganizaciones");

			 accionweb.getSesion().setAttribute("totalListaProductos",totalListaProductos);
			 
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "cargaResultadoProducto.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}

		accionweb.agregarObjeto("totalListaProductos", totalListaProductos); 	
    	accionweb.agregarObjeto("registra", 1);	
	   	accionweb.agregarObjeto("esOrdenCompraCB", 1);   
		if(accionweb.getSesion().getAttribute("valorRemanente") != null) {
	       accionweb.getSesion().removeAttribute("valorRemanente");
	       accionweb.getSesion().setAttribute("valorRemanente", 0);
	    }	
		
	}
	
	synchronized public void eliminaResultadoSolicitudProd(AccionWeb accionweb) throws Exception {
		String producto = Util.validaParametro(accionweb.getParameter("producto"),"");
		int numsec = Util.validaParametro(accionweb.getParameter("numsec"), 0);
		String cuenta = Util.validaParametro(accionweb.getParameter("cuenta"),"");	
		     
		List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("totalListaProductos");
		Presw25DTO prod = null;
		List<Presw25DTO> listaProd = new ArrayList<Presw25DTO>();
		long remanente = Util.validaParametro(accionweb.getSesion().getAttribute("valorRemanente"),0);
		accionweb.getSesion().removeAttribute("valorRemanente");
	
		if(totalListaProductos != null && totalListaProductos.size() > 0){
			for (Presw25DTO ss: totalListaProductos){
			  if(ss.getCodman().trim().equals(producto.trim()) &&
			     ss.getNumreq() == numsec &&
			     ss.getComen1().trim().equals(cuenta.trim())) {
				  remanente = remanente - (ss.getPres02() + ss.getPres03());
				 continue;
	          }    
			  else {
				     prod = new Presw25DTO();
		      	     prod.setNumreq(ss.getNumreq()); // secuencia
				     prod.setCodman(ss.getCodman()); // codigo del prodcto
				     prod.setComen2(ss.getComen2()); // nombre del prodcto
				     prod.setPres01(ss.getPres01()); // cantidad de productos
				     prod.setIndmod(ss.getIndmod()); // indicador obsequio
				     prod.setPres02(ss.getPres02()); // precio
				     prod.setPres03(ss.getPres03()); // impuesto
				     prod.setComen1(ss.getComen1()); 
				     listaProd.add(prod);
			  }
		   }
		 }	
		
		System.out.println(totalListaProductos.size());
		    	
	 	 accionweb.getSesion().removeAttribute("totalListaProductos");
	     accionweb.getSesion().setAttribute("totalListaProductos", listaProd);
	   		 	
	 	accionweb.agregarObjeto("esOrdenCompraCB", 1);    
		accionweb.agregarObjeto("registra", 1);
		totalListaProductos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("totalListaProductos");
		accionweb.agregarObjeto("totalListaProductos", totalListaProductos); 

		accionweb.getSesion().setAttribute("valorRemanente", remanente);
		accionweb.agregarObjeto("valorRemanente",remanente);

	//	accionweb.getSesion().removeAttribute("valorRemanente");
	//	accionweb.agregarObjeto("valorRemanente", 0); 

	}
	
	synchronized public void eliminaResultadoDistribucionProd(AccionWeb accionweb) throws Exception {
		int numsec = Util.validaParametro(accionweb.getParameter("numsec"), 0);
		String cuenta = Util.validaParametro(accionweb.getParameter("cuenta"),"");	
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"");	
		
		     
		List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
		Presw18DTO org = null;
		List<Presw18DTO> listaOrg = new ArrayList<Presw18DTO>();
		List<Presw18DTO> listaTodasOrg = new ArrayList<Presw18DTO>();
		long remanente = Util.validaParametro(accionweb.getSesion().getAttribute("valorRemanente"),0);
		accionweb.getSesion().removeAttribute("valorRemanente");
		try {
			accionweb.getSesion().removeAttribute("totalListaOrganizaciones");
			accionweb.getSesion().removeAttribute("listaOrganizaciones");
			accionweb.getSesion().removeAttribute("totalListaProductos");
		  if(totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
			for (Presw18DTO ss: totalListaOrganizaciones){
				if (ss.getNummes() == numsec &&
					ss.getNompro().trim().equals(cuenta.trim()) &&
					(organizacion.trim().equals("") || ss.getNomtip().trim().equals(organizacion.trim()))){ 
					remanente = remanente + ss.getPresac();
					continue;        
					
			   }
			   else {
				    org = new Presw18DTO();
					org.setNummes(ss.getNummes());
					org.setNomtip(ss.getNomtip());
					org.setDesuni(ss.getDesuni());
					org.setNompro(ss.getNompro());
					org.setDesite(ss.getDesite());
					org.setPresac(ss.getPresac());
					org.setPresum(ss.getPresum());
				    listaOrg.add(org);
				    listaTodasOrg.add(org);
			   }
			}
			   accionweb.getSesion().setAttribute("valorRemanente", remanente);
			   accionweb.agregarObjeto("valorRemanente",remanente);
	
		  }
		  accionweb.agregarObjeto("mensaje", "Se elimin� satisfactoriamente."); 
		} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "cargaResultadoDistribucionProducto.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
		 
		//System.out.println(listaOrganizaciones.size());	
	
		
			
			
	 	accionweb.agregarObjeto("esOrdenCompraCB", 1);    
		accionweb.agregarObjeto("registra", 1);
		//totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
		accionweb.getSesion().setAttribute("totalListaOrganizaciones", listaTodasOrg);
	    accionweb.getSesion().setAttribute("listaOrganizaciones",listaOrg);
		accionweb.agregarObjeto("totalListaOrganizaciones", listaTodasOrg);
		accionweb.agregarObjeto("listaOrganizaciones", listaOrg);
		
		
		//accionweb.getSesion().setAttribute("totalListaProductos",null);
		

	}
	
	 synchronized public void registraOrdenCompra(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			
			List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
			List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("totalListaProductos");
			long fechaEntrega = Util.validaParametro(accionweb.getParameter("fechaEntrega"),0);
			String codigoEntrega = Util.validaParametro(accionweb.getParameter("codigoEntrega"),"");
			String importacion = Util.validaParametro(accionweb.getParameter("importacion"),"");
			String divisa = Util.validaParametro(accionweb.getParameter("divisa"),"");
					
		    try {
		    int numOrden = 0;
			String mensaje = "";
	        numOrden = moduloOrdenCompraCentralizadaB.getRegistraOrdenCompra(accionweb.getReq(),rutUsuario,dv);
		    if(numOrden > 0){
		       accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa la Orden de Compra Centralizada, el n�mero interno es: "+ numOrden);
		       if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
					accionweb.agregarObjeto("registra", 1);
			        accionweb.getSesion().removeAttribute("totalListaOrganizaciones");
				    accionweb.getSesion().setAttribute("totalListaOrganizaciones", null);
				    accionweb.getSesion().removeAttribute("totalListaProductos");
					accionweb.getSesion().setAttribute("totalListaProductos", null); 
					accionweb.getSesion().removeAttribute("valorRemanente");
				     
			    }
				this.cargarOrdenCompra(accionweb);
		    } else
		      	accionweb.agregarObjeto("mensaje", "No se registr� la Orden de Compra.");
		    } catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "error . registraOrdenCompra");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
		 	accionweb.agregarObjeto("esOrdenCompraCB", 1); 
			accionweb.agregarObjeto("opcion", tipo);
		}

	 synchronized public void registraBien(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			
		try {
			List<Presw25DTO> listaBienes = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listaBienes");
		    String fechaEntregaIng =  Util.validaParametro(accionweb.getParameter("fechaEntregaIng"),"");
			long fechaEntrega = Long.parseLong(fechaEntregaIng.substring(6)+""+fechaEntregaIng.substring(3,5)+""+fechaEntregaIng.substring(0,2));
			long fechaRecep = 0;
			int i = 0;
			List<Presw25DTO> grabaBienes = new ArrayList<Presw25DTO>();
			for(Presw25DTO ss:listaBienes){
				long recibido = Util.validaParametro(accionweb.getParameter("recibido"+i),0);
				long rechazado = Util.validaParametro(accionweb.getParameter("rechazado"+i),0);
				String fin = Util.validaParametro(accionweb.getParameter("fin"+i),"");
				Presw25DTO presw25DTO = new Presw25DTO();
				presw25DTO.setTippro("RRP");
				presw25DTO.setPres01(ss.getPres01());
				presw25DTO.setPres04(ss.getPres04());
				presw25DTO.setComen1(ss.getComen1());
				presw25DTO.setPres02(recibido);
				presw25DTO.setPres03(fechaEntrega);
				presw25DTO.setComen2(ss.getComen2());
				presw25DTO.setComen3(fin);
				presw25DTO.setPres05(rechazado);
				grabaBienes.add(presw25DTO);
			}
		
		    int numOrden = 0;
			String mensaje = "";
	        boolean graba = moduloOrdenCompraCentralizadaB.getRegistraRecepcionBien(accionweb.getReq(),listaBienes,rutUsuario,dv);
		    if(graba){
		       accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa el Bien ");
		       	accionweb.agregarObjeto("registra", 1);	 
				
		    } else
		      	accionweb.agregarObjeto("mensaje", "No se registr� la Orden de Compra.");
		    
		    this.cargaMenuOrdenCompraC(accionweb);
	     } catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "error . registraBien");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		  	accionweb.agregarObjeto("esOrdenCompraCB", 1);  
			accionweb.agregarObjeto("opcion", tipo);
		}
	 
	 synchronized public void cargaMenuOrdenCompraC(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			
		      if(accionweb.getSesion().getAttribute("listaOrganizaciones") != null)
		      		accionweb.getSesion().setAttribute("listaOrganizaciones", null);
		      if(accionweb.getSesion().getAttribute("totalListaOrganizaciones") != null)
		      		accionweb.getSesion().setAttribute("totalListaOrganizaciones", null);
		      if(accionweb.getSesion().getAttribute("totalListaProductos") != null)
		      		accionweb.getSesion().setAttribute("totalListaProductos", null);
		      if(accionweb.getSesion().getAttribute("valorRemanente") != null)
		      		accionweb.getSesion().setAttribute("valorRemanente", null);

			
			
			PreswBean preswbean = null;
			Collection<Presw18DTO> listaOrdenCompra = null;
			String titulo = "";
			String tituloDetalle1 = "";
			String tituloDetalle2 = "";
			String tipcue = "";
			switch (tipo) {
			case  2:// lista orden de compra para modificar
				{preswbean = new PreswBean("LCC",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"",""); 
				titulo = "Modificaci�n";
				break;
			}
			case 3: case 6:  // lista de orden de compra para autorizar responsable
				{String nomtipo = "";
				if(tipo == 3 || tipo == 6){
					titulo = "Autorizaci�n Responsable de la Organizaci�n"; 
					nomtipo = "LPA"; 
					accionweb.agregarObjeto("opcion2", 6); 
				}
				preswbean = new PreswBean(nomtipo,0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				
				tituloDetalle1 = "Autorizar";
				tituloDetalle2 = "Rechazar";
				accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1); 
				accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
				//accionweb.agregarObjeto("opcion2", 6); 			
				break;
				}
			 case 4:// lista de orden de compra Registro Valores Definitivos
				{preswbean = new PreswBean("LPP",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"",""); 
			
				titulo = "Registro Valores definitivos";
				break;
				}
			 case 5:// lista de orden de compra pendientes de reautorizar
				{ 
				
				preswbean = new PreswBean("LC1",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"",""); 
				titulo = "Reautorizaci�n de Solicitudes"; 
				//tituloDetalle1 = "Recepci�n"; 
			
				
				break;
				}
		    case 7:// lista de orden de compra pendientes de recepcionar en finanzas
				{ 
				
				preswbean = new PreswBean("LC2",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"",""); 
				titulo = "Recepci�n Finanzas"; 
				tituloDetalle1 = "Recepci�n"; 
				break;
				}
		    case 8:// lista de orden de compra pendientes de recepci�n de bienes
			{ 			
				preswbean = new PreswBean("LRP",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"",""); 
				titulo = "Recepci�n de Bienes"; 			
				break;
			}
		    case 9:// lista de orden de compra para anulaci�n
			{ 
			
				preswbean = new PreswBean("LCC",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"",""); 
				titulo = "Anulaci�n Solicitudes"; 			
				break;
				}
		    case 17:// muestra lista dela consulta
			{ 
				int fechaInicio = Util.validaParametro(accionweb.getParameter("fechaInicio"),0);
				int fechaTermino = Util.validaParametro(accionweb.getParameter("fechaTermino"),0);
				String fechaIni = Util.validaParametro(accionweb.getParameter("fechaIni"),"");
				String fechaTer = Util.validaParametro(accionweb.getParameter("fechaTer"),"");
				//int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0);
				String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"");
				DateTool fechaActual = new DateTool();
				
				if(fechaInicio > 0 && fechaTermino == 0 ){
					fechaActual = new DateTool();
					String dia = fechaActual.getDay()+"";
					if(dia.length() == 1)
						dia = "0" + dia;
					String mes = (fechaActual.getMonth()+1)+"";
					if(mes.length()== 1)
						mes = "0" + mes;
					fechaTermino = Integer.parseInt(String.valueOf(fechaActual.getYear()) + mes + dia);
					fechaTer = dia + "/" + mes + "/" + String.valueOf(fechaActual.getYear());
					
				} 
				
				if(!organizacion.equals("")){
				preswbean = new PreswBean("COC",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				//preswbean.setCoduni(unidad);
				preswbean.setCajero(organizacion);
				preswbean.setFecmov(fechaInicio);
				preswbean.setNumche(fechaTermino);
				listaOrdenCompra = (Collection<Presw18DTO>) preswbean.consulta_presw18();		
				
				}
				titulo = "Consulta";	
				List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
				moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
				listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
				accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
			   	accionweb.agregarObjeto("organizacion", organizacion);
			   	accionweb.agregarObjeto("fechaIni", fechaIni);
			   	accionweb.agregarObjeto("fechaTer", fechaTer);
				break;
			
				}
		
			}
			if(tipo != 17)
			listaOrdenCompra = (Collection<Presw18DTO>) preswbean.consulta_presw18();		
			
			
			//System.out.println("listaOrdenCompra.size(): "+listaOrdenCompra.size());
		    if(listaOrdenCompra != null && listaOrdenCompra.size() > 0) {
		      List<String> listLugarEntega = new ArrayList<String>();
		      String lugEnt = "";
		      for (Presw18DTO ss: listaOrdenCompra){
		    	  lugEnt = moduloOrdenCompraCentralizadaB.getDetalleCodigoEntrega(ss.getNomtip().trim());
		    	  listLugarEntega.add(lugEnt);
			  }
		        accionweb.agregarObjeto("listLugarEntega",listLugarEntega);
		    	accionweb.agregarObjeto("hayDatoslista", "1");
		    }	
		    
		    
	        accionweb.agregarObjeto("listaOrdenCompra", listaOrdenCompra);
	      	accionweb.agregarObjeto("esOrdenCompraCB", "1");
	      	accionweb.agregarObjeto("opcion", String.valueOf(tipo));
	    	accionweb.agregarObjeto("opcionMenu", tipo);
	      	accionweb.agregarObjeto("titulo", titulo);
	      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
	    	accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
	      	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	}
	 
    public void consultaOrdenCompra(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			String proceso = Util.validaParametro(accionweb.getParameter("proceso")+ "","");
			String procesoSesion = Util.validaParametro(accionweb.getSesion().getAttribute("procesoSesion")+ "","");
			
			String nomIdentificador = "";
			long rutnum = 0;
			String dvrut = "";
			
			Presw19DTO preswbean19DTO = new Presw19DTO();
			String titulo = "";
			String tituloDetalle1 = "";
			//preswbean19DTO.setTippro("OCC"); 
            preswbean19DTO.setTippro((!procesoSesion.equals("")?procesoSesion:(proceso.equals("")?"OCC":proceso)));
			preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
			preswbean19DTO.setDigide(dv);
			preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
			List<Presw21DTO>  listaHistorial = new ArrayList<Presw21DTO>();
			
			String readonly = "";
	/*capturar los datos */
			switch (tipo) {
			case  2: // orden de compra para modificar
			{	titulo = "Modifica ";
				tituloDetalle1 = "Modificar";
				readonly = "";
				break;
				}
			case 3: // orden de compra para autorizar responsable
				{
				titulo = "Autorizaci�n Responsable de la Cuenta";
				tituloDetalle1 = "Autorizar";	
				break;
				}
			case 4: // orden de compra Registro Valores Definitivos
				{
				titulo = "Registro Valores Definitivos ";
				tituloDetalle1 = "Valores Definitivos";
				break;
				}
	        case 5: // orden de compra pendientes de reautorizar
				{
				titulo = "Pendiente de ReAutorizar";
				tituloDetalle1 = "Pendiente de ReAutorizar";
				break;
				}
			case 7: // orden de compra pendientes de recepcionar en finanzas
				{
				titulo = "Pendiente de Recepcionar";
				tituloDetalle1 = "Pendiente de Recepcionar";
				break;
				}
            case 8: // orden de compra pendientes de recepci�n de bienes
				{
				titulo = "Pendiente de Recepci�n Bienes";
				tituloDetalle1 = "Pendiente de Recepci�n Bienes";
				break;
				}
            case 9: // orden de compra Anulaci�n de solicitud
			{
			titulo = "Anulaci�n Solicitud";
			tituloDetalle1 = "Anulaci�n Solicitud";
			break;
			}
			}
			
			long valorAPagar = 0;
			String identificadorInterno = "";	
			int identificador = 0; 
			
		    String nomcam = "";
		    long valnu1 = 0;
		    long valnu2 = 0;
		    String valalf = "";
		    String fechaEntregaIng = "";
		    String lugEntrega = "";
		    String codigolugar = "";
		    String campus = "";
		    String edificio = "";
		    String piso = "";
		    String oficina = "";
		    String indImp  = "";
		    String divisa = "";
		    long impuesto = 0;
		    long total = 0;
		    String estado = "";
		    int otro = 0;
			    
		    
		    List<Cocow36DTO>  listaOrdenCompra = new ArrayList<Cocow36DTO>();
			CocowBean cocowBean = new CocowBean();
			listaOrdenCompra = cocowBean.buscar_cocow36(preswbean19DTO);
		    if(listaOrdenCompra != null && listaOrdenCompra.size() >0 ){
		    
		    for(Cocow36DTO ss: listaOrdenCompra){
                if(ss.getNomcam().trim().equals("FECENT")) 
                  	fechaEntregaIng = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
                if(ss.getNomcam().trim().equals("LUGENT"))
                	codigolugar = ss.getValalf().trim();
                if(ss.getNomcam().trim().equals("INDIMP"))
	            	indImp = ss.getValalf().trim();
	            if(ss.getNomcam().trim().equals("CODDIV"))
	                divisa = ss.getValalf().trim();
	 		   	if(ss.getNomcam().trim().equals("VALIMP"))
		    		impuesto = ss.getValnu1();
		    	if(ss.getNomcam().trim().equals("VALTOT"))
		    		total = ss.getValnu1();
		     	if(ss.getNomcam().trim().equals("ESTADO"))
	            	estado = ss.getValalf().trim();
		        if(ss.getNomcam().trim().equals("HISTORIAL")){
	            	Presw21DTO presw21DTO = new Presw21DTO();
	            	presw21DTO.setValor1(ss.getValnu1()); // es la fecha DE OPERACION, 
	            	presw21DTO.setGlosa1(ss.getValalf()); // es el nombre usuario	            	
	            	presw21DTO.setValor2(ss.getValnu2());// es la unidad
	            	presw21DTO.setCajero(ss.getResval()); //  glosa es la accion, INGRESO, MODIFICACION, AUTORIZACION, PAGO, AUTORIZA FINANZAS
	            	listaHistorial.add(presw21DTO);
	            }
	        }
		    Map<String, String> listaDivisa = moduloOrdenCompraCentralizadaB.getDivisas();
	    	accionweb.agregarObjeto("listaDivisa", listaDivisa);
	    
	    	lugEntrega = moduloOrdenCompraCentralizadaB.getDetalleCodigoEntrega(codigolugar);
            List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
		    moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
		    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		    accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
		
			List<Producto> listaProducto = moduloOrdenCompraCentralizadaB.getProductos();
		    accionweb.agregarObjeto("listaProducto", listaProducto);
		    
	    	
		    if(tipo == 2) {
			   	List<String> listaCampus = moduloOrdenCompraCentralizadaB.getCampus();
			    	accionweb.agregarObjeto("listaCampus", listaCampus);
				    
			    	int ind = lugEntrega.indexOf("-"); 
					campus = lugEntrega.substring(0,ind);
					lugEntrega = lugEntrega.substring(ind+1);
					ind = lugEntrega.indexOf("-"); 
					edificio = lugEntrega.substring(0,ind);
					lugEntrega = lugEntrega.substring(ind+1);
					ind = lugEntrega.indexOf("-"); 
					piso = lugEntrega.substring(0,ind);
					lugEntrega = lugEntrega.substring(ind+1);
					oficina = lugEntrega.substring(0);
					//lugEntrega = moduloOrdenCompraCentralizadaB.getDetalleCodigoEntrega(lugEntrega);
				    
				    accionweb.agregarObjeto("descEntrega",lugEntrega);
				    accionweb.agregarObjeto("campus",campus);
				    accionweb.agregarObjeto("edificio",edificio);
				    accionweb.agregarObjeto("piso",piso);
					accionweb.agregarObjeto("oficina",oficina);
					List<String> listaEdificio = moduloOrdenCompraCentralizadaB.getEdificio(campus);
					accionweb.agregarObjeto("listaEdificio", listaEdificio);
				  	
					List<String> listaPiso = moduloOrdenCompraCentralizadaB.getPiso(campus, edificio);
					accionweb.agregarObjeto("listaPiso", listaPiso);
					
					List<String> listaOficina = moduloOrdenCompraCentralizadaB.getOficina(campus, edificio, piso);
					accionweb.agregarObjeto("listaOficina", listaOficina);
				  
				if(accionweb.getSesion().getAttribute("valorRemanente") != null) {
				       accionweb.getSesion().removeAttribute("valorRemanente");
				       accionweb.getSesion().setAttribute("valorRemanente", 0);
				}
			  
		    } else {
		    	for(String ss: listaDivisa.keySet()){
		    		if(ss.trim().equals(divisa.trim()))
		    			divisa = ss.trim() +"-"+ listaDivisa.get(ss);
		    		
		    		
		    	}
		    }
			 
		    accionweb.agregarObjeto("descEntrega",lugEntrega);
		    accionweb.agregarObjeto("codigoEntrega",codigolugar);
		    accionweb.agregarObjeto("fechaEntregaIng",fechaEntregaIng);
		    accionweb.agregarObjeto("lugEntrega", lugEntrega);
		    accionweb.agregarObjeto("importacion",indImp);
			accionweb.agregarObjeto("divisa",divisa);
			accionweb.agregarObjeto("impuesto",impuesto);
			accionweb.agregarObjeto("totalProd",total);
				    
			
			accionweb.agregarObjeto("numDoc",numDoc);
			accionweb.agregarObjeto("opcion",tipo);
			//PreswBean preswbean = new PreswBean("OCC",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			PreswBean preswbean = new PreswBean(!procesoSesion.equals("")?procesoSesion:(proceso.equals("")?"OCC":proceso),0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				
			preswbean.setNumdoc(numDoc);
			
			List<Presw25DTO> totalListaProductos = preswbean.consulta_presw25();
			for(Presw25DTO ss: totalListaProductos){
				String numeroBanner = ss.getComen3();
				accionweb.agregarObjeto("numeroBanner", numeroBanner);
			}
			Collection<Presw18DTO> totalListaOrganizaciones = preswbean.consulta_presw18();
			
			//if(tipo == 2 || tipo == 3 || tipo == 4) {
			if(tipo >= 2  /*&& procesoSesion.equals("")*/) {
				long saldoCuenta = 0;
				for (Presw18DTO org : totalListaOrganizaciones){
				if(org.getIndprc().trim().equals("D"))	
					 saldoCuenta = moduloOrdenCompraCentralizadaB.getBuscaSaldo(org.getNomtip().trim(),org.getNompro().trim());
				org.setPresum(saldoCuenta);
				for (Presw18DTO cAut : listaUnidades){
					     if (cAut.getNompro().trim().equals(org.getNomtip().trim()))
					    	 org.setDesuni(cAut.getDesuni().trim());
					}
				}
				
		/*		for (Presw25DTO prod : totalListaProductos){
					for(String p: listaProducto.keySet()){
			    		if(p.trim().equals(prod.getCodman().trim()))
			    			prod.setComen2(listaProducto.get(p));
			    		
			    		
			    	}
				}
			*/
				for (Presw25DTO prod : totalListaProductos){
					for(Producto p: listaProducto){
			    		if(p.getCodigo().trim().equals(prod.getCodman().trim()))
			    			prod.setComen2(p.getDescripcion());
			    		
			    		
			    	}
				}
			}	
			
			System.out.println("totalListaProductos" + totalListaProductos.size());
			accionweb.agregarObjeto("totalListaProductos",totalListaProductos);
			accionweb.getSesion().setAttribute("totalListaProductos",totalListaProductos);
						
			System.out.println("totalListaOrganizaciones" + totalListaOrganizaciones.size());
			accionweb.agregarObjeto("totalListaOrganizaciones",totalListaOrganizaciones);
			accionweb.getSesion().setAttribute("totalListaOrganizaciones",totalListaOrganizaciones);
			accionweb.agregarObjeto("listaHistorial", listaHistorial);
			
			accionweb.agregarObjeto("obsequio", "N");
			accionweb.agregarObjeto("hayDatoslista", "1");  
			if(tipo == 17){
				String numeroBanner = moduloOrdenCompraCentralizadaB.getNumeroOrden(numDoc);
				if (!numeroBanner.trim().equals("0000000000"))
					accionweb.agregarObjeto("numeroBanner", numeroBanner);
			}
			if(tipo != 7)
			   accionweb.agregarObjeto("read",readonly);
		    
		
		    } else { 
		    	accionweb.agregarObjeto("mensaje", "No existe orden de compra "+titulo+"con el n�mero " + numDoc);
		    	if(tipo == 4){
		        accionweb.agregarObjeto("pagina", "ordenCompraCMuestraLista.vm");   
		        //this.cargaAutorizaRecuperacion(accionweb);
		    	}
		    }
				    	
		   	    
		    accionweb.agregarObjeto("esOrdenCompraCB", "1");
	      	accionweb.agregarObjeto("opcion", tipo);
	      	accionweb.agregarObjeto("titulo", titulo);
	      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
	      	accionweb.agregarObjeto("proceso",proceso);
	
	    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	    }
    
    
    public void cargaBienes(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			String numeroCompra = Util.validaParametro(accionweb.getParameter("numeroCompra"),"");
			String fechaSol = Util.validaParametro(accionweb.getParameter("fechaSol"), "");
			
			accionweb.agregarObjeto("numDoc",numDoc);
			accionweb.agregarObjeto("opcion",tipo);
			PreswBean preswbean = new PreswBean("GRP",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");					
			preswbean.setCajero(numeroCompra);
			preswbean.setNumcom(numDoc);
			
			List<Presw25DTO> listaBienes = preswbean.consulta_presw25();
			for(Presw25DTO ss: listaBienes){
				accionweb.agregarObjeto("numeroOrden", ss.getComen1());
				accionweb.agregarObjeto("solicitud", ss.getPres04());
				accionweb.agregarObjeto("proveedor", ss.getRutide()+"-"+ss.getDigide());
				accionweb.agregarObjeto("fechaSol", fechaSol);
				accionweb.agregarObjeto("numeroSecuencia", ss.getPres01());

			}
	
			accionweb.agregarObjeto("listaBienes",listaBienes);
			accionweb.getSesion().setAttribute("listaBienes",listaBienes);
					    	
		   	    
		    accionweb.agregarObjeto("esOrdenCompraCB", "1");
	      	accionweb.agregarObjeto("opcion", tipo);
	      	accionweb.agregarObjeto("titulo", "Recepci�n de Bienes");
	    
	      	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	    }
	 
    public void cargaAsignacion(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");

		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		int ind = Util.validaParametro(accionweb.getParameter("ind"), 0);
		String fechaEntregaIng = Util.validaParametro(accionweb.getParameter("fechaEntregaIng"),"");
		accionweb.agregarObjeto("fechaEntregaIng", fechaEntregaIng);
		String titulo = Util.validaParametro(accionweb.getParameter("titulo"),"");
		List<Presw25DTO> totalListaProductos = (List<Presw25DTO>)accionweb.getSesion().getAttribute("totalListaProductos");
		List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>)accionweb.getSesion().getAttribute("totalListaOrganizaciones");
			
		List<Presw18DTO>  listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
		
		List<Producto> listaProducto = moduloOrdenCompraCentralizadaB.getProductos();
		accionweb.agregarObjeto("listaProducto", listaProducto);
		
		accionweb.agregarObjeto("totalListaProductos",totalListaProductos);
		accionweb.agregarObjeto("totalListaOrganizaciones",totalListaOrganizaciones);
		accionweb.agregarObjeto("ind", ind);
		String cuenta = "";
		String codigoBien  = "";
		String descripcionBien  = "";
		int i=0;
		for(Presw25DTO ss:totalListaProductos){
			if(i == ind){
				for(Presw18DTO cc:totalListaOrganizaciones){
					if(cc.getNompro().trim().equals(ss.getComen1().trim()))
						cuenta = ss.getComen1().trim()+ "-" +cc.getDesite();
					System.out.println(cc.getNompro().trim()+"-"+cc.getDesite());
				}
				for (Producto pp:listaProducto)
					if(pp.getCodigo().trim().equals(ss.getCodman().trim())){
						codigoBien = ss.getCodman().trim();
						descripcionBien = pp.getDescripcion();
					}
				
			
					for (Presw18DTO org : totalListaOrganizaciones){
						for (Presw18DTO cAut : listaUnidades){
						     if (cAut.getNompro().trim().equals(org.getNomtip().trim()))
						    	 org.setDesuni(cAut.getDesuni().trim());
						}
						org.setAcumum(org.getPresac());
					}
				
				accionweb.agregarObjeto("obsequio",( ss.getIndmod().trim().equals("S"))?"SI":"NO");
				if(ss.getPres04() > 0)
					accionweb.agregarObjeto("fechaEntregaIng",(ss.getPres04()+"").substring(6)+"/"+(ss.getPres04()+"").substring(4,6)+"/"+(ss.getPres04()+"").substring(0,4));
				accionweb.agregarObjeto("codigoBien", codigoBien);
				accionweb.agregarObjeto("descripcionBien", descripcionBien);				
				accionweb.agregarObjeto("descripcion",ss.getMotiv1().trim());
				accionweb.agregarObjeto("cantidad", ss.getPres01());
				accionweb.agregarObjeto("precioOriginal", ss.getPres02());
				long totalNeto = ss.getPres02() * ss.getPres01();
				accionweb.agregarObjeto("totalNeto",totalNeto);
				accionweb.agregarObjeto("totalImpuesto", ss.getPres03());				
				accionweb.agregarObjeto("totalTotal", totalNeto + ss.getPres03());
				accionweb.agregarObjeto("cuenta", cuenta);				
				accionweb.agregarObjeto("numreq", ss.getNumreq());	
					
			}
			i++;
		}
		
			    	
	   	    
	    accionweb.agregarObjeto("esOrdenCompraCB", "1");
      	accionweb.agregarObjeto("opcion", tipo);
     	accionweb.agregarObjeto("titulo", titulo);
     	accionweb.agregarObjeto("numDoc", numDoc);
        }
    synchronized public void ejecutaAccion(AccionWeb accionweb) throws Exception {
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		String estadoVales = Util.validaParametro(accionweb.getParameter("estadoVales"),"");
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
		String proceso = Util.validaParametro(accionweb.getParameter("proceso"),"");
		String titulo = "";
		
		switch (tipo){
		case 2: {
			this.actualizaOrdenCompra(accionweb);
			break;
		}
		case 3: {// autoriza responsable
			
			this.autorizaOrdenCompraResponsable(accionweb);
			break;
		}
		case 4:{ // actualiza valores
			this.actualizaValorDefinitivo(accionweb);
			break;
		}
		case 5:{
			this.reAutorizaOrdenCompraResponsable(accionweb);
			break;
		}
		case 6:{
			this.rechazaAutOrdenCompra(accionweb);
			break;
		}
		case 7: // orden de compra autoriza finanzas
		{
			//this.recepcionaValePago(accionweb);
			this.autorizaOrdenCompraFinanza(accionweb);
			titulo = "Pendiente de Autorizar";		
			break;
		}
		
		case 8: // orden de compra pendientes de recepci�n de bienes
		{
		//titulo = "Pendiente de Recepci�n Bienes";
		//tituloDetalle1 = "Pendiente de Recepci�n Bienes";
		break;
		}
		
		}
		
		if(tipo != 11 )
			consultaOrdenCompra(accionweb);
		
	}
    
synchronized public boolean revisaSaldoOrganizacion(List<Presw18DTO> totalListaOrganizaciones) throws Exception {
	 	
		long total = 0;
	 	long saldoCuenta = 0;
	 	boolean sobrepasa = false;
		if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
		    for (Presw18DTO ss : totalListaOrganizaciones) {
		    	if( !ss.getIndprc().trim().equals("T")){
			       total = ss.getPresac();
				   saldoCuenta = moduloOrdenCompraCentralizadaB.getBuscaSaldo(ss.getNomtip().trim(),ss.getNompro().trim());
				                                               // codOrg.          codCuenta que se dejo en Nomtip
				   if(total > saldoCuenta ) {
					   sobrepasa = true;
					   break;
				   }
		    	}
		    }	
	    }
		
		return sobrepasa;
 }

synchronized public boolean revisaAutOrganizacion(List<Presw18DTO> totalListaOrganizaciones, int numdoc, int rutUsuario, String dv, boolean modifica) throws Exception {
 	boolean aut = true;
	if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
	  if (modifica) {	
	      for (Presw18DTO ss : totalListaOrganizaciones) {	
	    	 System.out.println(ss.getDesuni() + "-" + ss.getDesuni().trim().equals("AUTORIZADA") ); 
	    	  if (!ss.getDesuni().trim().equals("AUTORIZADA")  && !ss.getIndprc().trim().equals("T")) {
	    		  aut = moduloOrdenCompraCentralizadaB.saveEstadoCuenta(numdoc,ss.getNomtip().trim(),rutUsuario,dv,"A");
	    		  if (!aut) break;
	    	  }	  
		  }
 	 }
	 else {
		   for (Presw18DTO ss : totalListaOrganizaciones) {	
			  System.out.println(ss.getDesuni() + "-" + ss.getDesuni().trim().equals("AUTORIZADA")  +" - "+ss.getIndprc().trim()); 
		   	  if (!ss.getDesuni().trim().equals("AUTORIZADA") && !ss.getIndprc().trim().equals("T")) { 
		   		  aut = false;
		   		  break;
		   	  } 	  
		   }
	 }
	}	  
   return aut;
	}	

synchronized public boolean actualizaSinFinOrganizacion(List<Presw18DTO> totalListaOrganizaciones, int numdoc, int rutUsuario, String dv) throws Exception {
 	long saldoCuenta = 0;
 	boolean sf = true;  // Sin Financiamiento
	if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
	    for (Presw18DTO ss : totalListaOrganizaciones) {
  		   saldoCuenta = moduloOrdenCompraCentralizadaB.getBuscaSaldo(ss.getNomtip().trim(),ss.getNompro().trim());
		   if(ss.getPresac() > saldoCuenta ) {
	    		if (!ss.getDesite().trim().equals("APROB.S/FINANCIAM")) 
			      sf = moduloOrdenCompraCentralizadaB.saveEstadoCuenta(numdoc,ss.getNomtip().trim(),rutUsuario,dv,"B");
		   }	
	     }
      }
	return sf;
 }
synchronized public void autorizaOrdenCompraResponsable(AccionWeb accionweb) throws Exception {
	// autoriza solo si tiene saldo
	int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
	String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
	int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
	int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
	String proceso = Util.validaParametro(accionweb.getParameter("proceso")+ "","");

	int sobrepasa = 0;
	consultaOrdenCompra(accionweb);
	List<Presw18DTO>  totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
	try {
 	boolean sobrepasaOrg = this.revisaSaldoOrganizacion(totalListaOrganizaciones); // Se revisan solo los saldos de los autorizadores de sus org.
 	// si no estan sobrepasados los saldos 
	if (!sobrepasaOrg){
		boolean aut = moduloOrdenCompraCentralizadaB.saveAutorizaResponsable(numdoc, rutUsuario, dv); // se autorizan las org del responsable
		Thread.sleep(1500);
		if (aut) {
            accionweb.getSesion().setAttribute("procesoSesion", "OC3");
			consultaOrdenCompra(accionweb);
			if(accionweb.getSesion().getAttribute("procesoSesion") != null)
				accionweb.getSesion().removeAttribute("procesoSesion");
		
			totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
			aut = revisaAutOrganizacion(totalListaOrganizaciones,numdoc,rutUsuario,dv,false); // se revisan si todas las org del vale estan autorizadas
			Thread.sleep(1500);
			aut = true;
			if (aut ) {
				sobrepasaOrg = this.revisaSaldoOrganizacion(totalListaOrganizaciones); // Se revisan los saldos todas las org del vale.
				if (sobrepasaOrg) accionweb.agregarObjeto("mensaje", "No se puede Autorizar la Orden de Compra por Organizaciones sin saldo suficiente.");
				else accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n de la Orden N� " + numdoc+".");
	
			}
			else 
				accionweb.agregarObjeto("mensaje", "Hay organizaciones de la orden de compra sin autorizar");
		}
		else 
			accionweb.agregarObjeto("mensaje", "Problemas al autorizar una Organizaci�n");
		
		
   }
	else accionweb.agregarObjeto("mensaje", "Organizaciones sin saldo suficiente.");
		


	} catch (Exception e) {
                accionweb.agregarObjeto("mensaje", "No guardado autorizaOrdenCompra.");
		accionweb.agregarObjeto("exception", e);
		e.printStackTrace();
	}
	
	Thread.sleep(500);
    cargaMenuOrdenCompraC(accionweb);
    
	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
  		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
  	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
  		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
  	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
  		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
  	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
		accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
  	
}
    
  
    synchronized public void reAutorizaOrdenCompraResponsable(AccionWeb accionweb) throws Exception {
    	int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		
		List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
		List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("totalListaProductos");
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		
		
		try {
		// revisa saldo 		
		int sobrepasa = 0;
		consultaOrdenCompra(accionweb);
		boolean sobrepasaOrg = this.revisaSaldoOrganizacion(totalListaOrganizaciones); // Se revisan solo los saldos de los autorizadores de sus org.
		String estado = "";
		if(!sobrepasaOrg)
			estado = "A";
		else
			estado = "B";
		boolean registra = false;
		for(Presw18DTO ss:totalListaOrganizaciones)  {
			if(ss.getIndprc().trim().equals("D"))
			registra = moduloOrdenCompraCentralizadaB.setReautoriza(rutUsuario, dv, numDoc, ss.getNomtip().trim(), estado);
		}
	   if(registra){
	    	/* falta verificar estado
			 * */
	    	boolean autorizado = moduloOrdenCompraCentralizadaB.getReAutorizado(numDoc);
	    //	autorizado = true;
			if(autorizado){
				/***/
				int actualiza =  moduloOrdenCompraCentralizadaB.updateEstado(rutUsuario, dv, numDoc, estado);
				// si no estan sobrepasados los saldos
			 	if (!sobrepasaOrg){
					 	accionweb.getSesion().setAttribute("procesoSesion", "OC3");
						consultaOrdenCompra(accionweb);
						if(accionweb.getSesion().getAttribute("procesoSesion") != null)
							accionweb.getSesion().removeAttribute("procesoSesion");
					
						totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
						sobrepasaOrg = this.revisaSaldoOrganizacion(totalListaOrganizaciones); // Se revisan los saldos todas las org del vale.
						if (sobrepasaOrg) { // si estan sobrepasados los saldos 
								sobrepasa = 1;
								accionweb.agregarObjeto("mensaje", "No se puede ReAutoriza la Orden de Compra por Organizaciones sin saldo suficiente");
								Thread.sleep(1500);
								totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
						}
							else sobrepasa = 0;  // todo ok y vamos a autorizar el vale
					
		        }
				else { // si estan sobrepasados los saldos  envia mensaje de sin saldo suficiente
					sobrepasa = 1;
					accionweb.agregarObjeto("mensaje", "Autorizado, Organizaci�n sin Saldo suficiente");
				}
				
				
				
				
					if(sobrepasa == 0){
					 	 String rutDvIngresador = moduloOrdenCompraCentralizadaB.getRutOperacion(numDoc,"I");
					 	// String rutDvAutorizador = moduloOrdenCompraCentralizadaB.getRutOperacion(numDoc,"A");  
					 	 String rutDvAutorizador = String.valueOf(rutUsuario) + dv; 
					 	 //String rutdv,String numvale, String fecha_invoice, String fecha_pmt_due, String fecha_trans, String cod_doct, String datos
					     Vector vec_COCOF150B = moduloOrdenCompraCentralizadaB.getDatosCOCOF150B(numDoc);
					     
					     String fechaEnt = "";
					     String lugarEnt = "";
					     String rutDvProv = "";
					     String codOrgOrden = "";
					     String codDivisa = "";
					   
					   
					     //*/
					     Vector vec2 = new Vector();
					     if (vec_COCOF150B.size() > 0) {
					    	for (int j=0;j < vec_COCOF150B.size(); j++){ 
					    	 vec2 = (Vector) vec_COCOF150B.get(0);	
					    	 fechaEnt = vec2.get(0)+"";
						     lugarEnt = vec2.get(1)+"";
						     codDivisa = vec2.get(2)+"";
						     rutDvProv = vec2.get(3)+"" + vec2.get(4)+"";
						     codOrgOrden = vec2.get(5)+"";
							 	 
					    		 Vector vec_datos = new Vector();
					    		 vec_datos = moduloOrdenCompraCentralizadaB.getDatosProductos(numDoc, Integer.parseInt(vec2.get(3)+""), vec2.get(4)+"");
					    		 String datos_parametro = "";
					    		 Vector vec = new Vector();
					    		    		 
					    		 for (int i = 0; i < vec_datos.size(); i++) {
					    			 vec = (Vector) vec_datos.get(i);
					    			 for (int y = 0; y < vec.size(); y++) {
					    				if (y == (vec.size()-1)) datos_parametro = datos_parametro + vec.get(y) + "@"; 
					    				else datos_parametro = datos_parametro + vec.get(y) + "&";
					    			 }
					    		 }
					    		//System.out.println("datos_parametro " + datos_parametro); 
					    		int error = moduloOrdenCompraCentralizadaB.saveOrdenCompra(rutUsuario,dv,String.valueOf(numDoc),fechaEnt,lugarEnt,codDivisa,rutDvProv,codOrgOrden,
					    				                                                   datos_parametro,rutDvIngresador,rutDvAutorizador, "G");
					    		if(error == -12)
					    			 accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n del archivo Historial del documento en I-Series");
					    		else if (error < 0)
					    			   accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n A BANNER");
					    		     else  accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n");
							 
							
					    	}
					     }
					}
				}
	    //   accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa la reasignaci�n de valores definitivos de Orden de Compra Centralizada, el n�mero interno es: "+ numOrden);
	       if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
				accionweb.agregarObjeto("registra", 1);
		        accionweb.getSesion().removeAttribute("totalListaOrganizaciones");
			    accionweb.getSesion().setAttribute("totalListaOrganizaciones", null);
			    accionweb.getSesion().removeAttribute("totalListaProductos");
				accionweb.getSesion().setAttribute("totalListaProductos", null); 
				accionweb.getSesion().removeAttribute("valorRemanente");
			     
		    }
			this.consultaOrdenCompra(accionweb);
	    } else
	      	accionweb.agregarObjeto("mensaje", "No se registr� la Orden de Compra.");
	   
	   
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado autorizaValePago.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
	
		/****/
	
	    accionweb.agregarObjeto("numDoc", numDoc);
	 	accionweb.agregarObjeto("esOrdenCompraCB", 1);    
		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
			accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
		if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
			accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
		if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
			accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
		if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
		   accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	
	}  	
    
    
    
    synchronized public void autorizaOrdenCompraFinanza(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		String proceso = Util.validaParametro(accionweb.getParameter("proceso")+ "","");
	
		int sobrepasa = 0;
		consultaOrdenCompra(accionweb);
		List<Presw18DTO>  totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
		try {
	    		boolean autorizado = moduloOrdenCompraCentralizadaB.getAutorizado(numdoc);
		    //	autorizado = true;
				if(autorizado){
					boolean recepcion = moduloOrdenCompraCentralizadaB.saveEstadoFinanzaOrdenCompra(numdoc, rutUsuario, dv);
					if(recepcion) {
						String mensaje = this.enviaBanner(accionweb);
						accionweb.agregarObjeto("mensaje", mensaje);
						this.consultaOrdenCompra(accionweb);

					} else {
						accionweb.agregarObjeto("mensaje", "Problemas para recepci�n Finanzas");
						}
				} else
					accionweb.agregarObjeto("mensaje", "No se registr� la Orden de Compra a Banner.");

			
			
			
	 		
		} catch (Exception e) {
	                accionweb.agregarObjeto("mensaje", "No guardado autorizaOrdenCompraFinanza.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		
		Thread.sleep(500);
	    cargaMenuOrdenCompraC(accionweb);
	    
		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
      	
    }
    
    
    /*
    synchronized  public void setAutorizaBanner(AccionWeb accionweb) throws Exception {
		int sobrepasa = 0;
		consultaOrdenCompra(accionweb);
		List<Presw18DTO>  totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
		boolean aut = false;
	 	boolean sobrepasaOrg = this.revisaSaldoOrganizacion(totalListaOrganizaciones); // Se revisan solo los saldos de los autorizadores de sus org.
	 	// si no estan sobrepasados los saldos 
		if (!sobrepasaOrg){
			 	accionweb.getSesion().setAttribute("procesoSesion", "OC3");
				consultaOrdenCompra(accionweb);
				if(accionweb.getSesion().getAttribute("procesoSesion") != null)
					accionweb.getSesion().removeAttribute("procesoSesion");
			
				totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
				sobrepasaOrg = this.revisaSaldoOrganizacion(totalListaOrganizaciones); // Se revisan los saldos todas las org del vale.
				if (sobrepasaOrg) { // si estan sobrepasados los saldos 
						sobrepasa = 1;
						accionweb.agregarObjeto("mensaje", "No se puede Autorizar el vale por Organizaciones sin saldo suficiente");
						aut = actualizaSinFinOrganizacion(totalListaOrganizaciones,numdoc,rutUsuario,dv); //se dejan en estado B las org del vale que estan sin saldo
						Thread.sleep(1500);
						totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
						if (!aut) {
							accionweb.agregarObjeto("mensaje", "Problemas al modificar estado de la Organizaci�n");
						}
					}
					else sobrepasa = 0;  // todo ok y vamos a autorizar el vale
			
        }
		else { // si estan sobrepasados los saldos  envia mensaje de sin saldo suficiente
			sobrepasa = 1;
			accionweb.agregarObjeto("mensaje", "Organizaci�n sin Saldo suficiente");
		}
		
		boolean estadoOrdenCompra = false;
		if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
		   if (sobrepasa == 0) {
			   estadoOrdenCompra = moduloOrdenCompraCentralizadaB.saveEstadoOrdenCompra(numdoc,rutUsuario,dv);
			   Thread.sleep(1500);
		   }
		}
		
		try {
		if (estadoOrdenCompra && tipo == 10) {	
	         Vector vec_COCOF150B = moduloOrdenCompraCentralizadaB.getDatosCOCOF150B(numdoc);
		   	
		     String fechaEnt = "";
		     String lugarEnt = "";
		     String rutDvProv = "";
		     String codOrgOrden = "";
		     String codDivisa = "";
		     if (vec_COCOF164B.size() > 0) {
		    	 fechaEnt = vec_COCOF164B.get(0)+"";
			     lugarEnt = vec_COCOF164B.get(1)+"";
			     codDivisa =  vec_COCOF164B.get(2)+"";
			     rutDvProv = vec_COCOF164B.get(3)+"" + vec_COCOF164B.get(4)+"";
			     codOrgOrden = vec_COCOF164B.get(5)+"";
	    		
			     Vector vec_datos = new Vector();
	    		 vec_datos = moduloOrdenCompraCentralizadaB.getDatosProcutos(numdoc);
	    		 String datos_parametro = "";
	    		 Vector vec = null;
	    		    		 
	    		 for (int i = 0; i < vec_datos.size(); i++) {
	    			 vec = (Vector) vec_datos.get(i);
	    			 for (int y = 0; y < vec.size(); y++) {
	    				if (y == (vec.size()-1)) datos_parametro = datos_parametro + vec.get(y) + "@"; 
	    				else datos_parametro = datos_parametro + vec.get(y) + "&";
	    			 }
	    		 }
	    		//System.out.println("datos_parametro " + datos_parametro); 
	    		int error = moduloOrdenCompraCentralizadaB.saveOrdenCompra(rutUsuario,dv,String.valueOf(numdoc),fechaEnt,lugarEnt,codDivisa,rutDvProv,codOrgOrden,datos_parametro);
	    		if(error == -12)
	    			 accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n del archivo Historial del documento en I-Series");
	    		else if (error < 0)
	    			   accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n A BANNER");
	    		     else  accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n");
	    
		     }
		    } 
		} catch (Exception e) {
	                accionweb.agregarObjeto("mensaje", "No guardado autorizaOrdenCompra.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
	
	 
 }

    
    
    /*
     *    synchronized public void autorizaOrdenCompra(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		String proceso = Util.validaParametro(accionweb.getParameter("proceso")+ "","");
	
		int sobrepasa = 0;
		consultaOrdenCompra(accionweb);
		List<Presw18DTO>  totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
		
	 	boolean sobrepasaOrg = this.revisaSaldoOrganizacion(totalListaOrganizaciones); // Se revisan solo los saldos de los autorizadores de sus org.
	 	// si no estan sobrepasados los saldos 
		if (!sobrepasaOrg){
			boolean aut = revisaAutOrganizacion(totalListaOrganizaciones,numdoc,rutUsuario,dv,true); // se autorizan las org del autorizador
			Thread.sleep(1500);
			if (aut) {
			 	accionweb.getSesion().setAttribute("procesoSesion", "OC3");
				consultaOrdenCompra(accionweb);
				if(accionweb.getSesion().getAttribute("procesoSesion") != null)
					accionweb.getSesion().removeAttribute("procesoSesion");
			
				totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
				aut = revisaAutOrganizacion(totalListaOrganizaciones,numdoc,rutUsuario,dv,false); // se revisan si todas las org del vale estan autorizadas
				Thread.sleep(1500);
				if (aut ) {
					sobrepasaOrg = this.revisaSaldoOrganizacion(totalListaOrganizaciones); // Se revisan los saldos todas las org del vale.
					if (sobrepasaOrg) { // si estan sobrepasados los saldos 
						sobrepasa = 1;
						accionweb.agregarObjeto("mensaje", "No se puede Autorizar el vale por Organizaciones sin saldo suficiente");
						aut = actualizaSinFinOrganizacion(totalListaOrganizaciones,numdoc,rutUsuario,dv); //se dejan en estado B las org del vale que estan sin saldo
						Thread.sleep(1500);
						totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
						if (!aut) {
							accionweb.agregarObjeto("mensaje", "Problemas al modificar estado de la Organizaci�n");
						}
					}
					else sobrepasa = 0;  // todo ok y vamos a autorizar el vale
				}
				else {
					sobrepasa = 1;
					accionweb.agregarObjeto("mensaje", "Hay organizaciones del Vale sin autorizar");
				}
			}
			else {
				sobrepasa = 1;
				accionweb.agregarObjeto("mensaje", "Problemas al autorizar una Organizaci�n");
			}
			
        }
		else { // si estan sobrepasados los saldos  envia mensaje de sin saldo suficiente
			sobrepasa = 1;
			accionweb.agregarObjeto("mensaje", "Organizaci�n sin Saldo suficiente");
		}
		
		boolean estadoOrdenCompra = false;
		if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
		   if (sobrepasa == 0) {
			   estadoOrdenCompra = moduloOrdenCompraCentralizadaB.saveEstadoOrdenCompra(numdoc,rutUsuario,dv);
			   Thread.sleep(1500);
		   }
		}
		
		try {
		if (estadoOrdenCompra && tipo == 10) {	
	         Vector vec_COCOF164B = moduloOrdenCompraCentralizadaB.getDatosCOCOF164B(numdoc);
		   	
		     String fechaEnt = "";
		     String lugarEnt = "";
		     String rutDvProv = "";
		     String codOrgOrden = "";
		     String codDivisa = "";
		     if (vec_COCOF164B.size() > 0) {
		    	 fechaEnt = vec_COCOF164B.get(0)+"";
			     lugarEnt = vec_COCOF164B.get(1)+"";
			     codDivisa =  vec_COCOF164B.get(2)+"";
			     rutDvProv = vec_COCOF164B.get(3)+"" + vec_COCOF164B.get(4)+"";
			     codOrgOrden = vec_COCOF164B.get(5)+"";
	    		
			     Vector vec_datos = new Vector();
	    		 vec_datos = moduloOrdenCompraCentralizadaB.getDatosProcutos(numdoc);
	    		 String datos_parametro = "";
	    		 Vector vec = null;
	    		    		 
	    		 for (int i = 0; i < vec_datos.size(); i++) {
	    			 vec = (Vector) vec_datos.get(i);
	    			 for (int y = 0; y < vec.size(); y++) {
	    				if (y == (vec.size()-1)) datos_parametro = datos_parametro + vec.get(y) + "@"; 
	    				else datos_parametro = datos_parametro + vec.get(y) + "&";
	    			 }
	    		 }
	    		//System.out.println("datos_parametro " + datos_parametro); 
	    		int error = moduloOrdenCompraCentralizadaB.saveOrdenCompra(rutUsuario,dv,String.valueOf(numdoc),fechaEnt,lugarEnt,codDivisa,rutDvProv,codOrgOrden,datos_parametro);
	    		if(error == -12)
	    			 accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n del archivo Historial del documento en I-Series");
	    		else if (error < 0)
	    			   accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n A BANNER");
	    		     else  accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n");
	    
		     }
		    } 
		} catch (Exception e) {
	                accionweb.agregarObjeto("mensaje", "No guardado autorizaOrdenCompra.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		
		Thread.sleep(500);
	    cargaMenuOrdenCompraC(accionweb);
	    
		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
      	
    } 
    */
	 synchronized public void actualizaOrdenCompra(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		
		List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
		List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("totalListaProductos");
		

	    boolean registra = false;
	    
	    registra = moduloOrdenCompraCentralizadaB.getActualizaOrdenCompra(accionweb.getReq(),rutUsuario,dv,numdoc);
	    if(registra){
	   	    accionweb.agregarObjeto("mensaje", "Se actualiz� en forma exitosa la actualizaci�n de Solicitud de Orden de compra.");
	   	    if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
				accionweb.agregarObjeto("registra", 1);
			    accionweb.getSesion().removeAttribute("totalListaOrganizaciones");
			    if(totalListaProductos != null)
			    	accionweb.getSesion().removeAttribute("totalListaProductos");
			}
	   	// cargarMenu(accionweb);
	   	 consultaOrdenCompra(accionweb);
	    } else
	    	accionweb.agregarObjeto("mensaje", "No se modific� la Solicitud de Orden de compra.");
	    accionweb.agregarObjeto("esOrdenCompraCB", 1);  
	 
		 
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	}
	
	 synchronized public void rechazaAutOrdenCompra(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);	
			String glosa = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "","");
			
			String codOrg = "";
			List<Presw18DTO>  totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
			if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
				for (Presw18DTO ss : totalListaOrganizaciones) {	
				    codOrg = ss.getNomtip().trim();
				    break;
				}
			}
			    
			
			try { 
			boolean graba = moduloOrdenCompraCentralizadaB.saveRechaza(numdoc, rutUsuario,dv,"RCC", glosa,"",codOrg);
		    if(graba)
		    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el rechazo de la Autorizaci�n.");
		    else
		    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Autorizaci�n.");
			} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado rechazaAutOrdenCompra.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
			Thread.sleep(500);
		    cargaMenuOrdenCompraC(accionweb);
			
		}
	 synchronized public void  rechazaReautOrdenCompra(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);	
			String glosa = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "","");
			
			String codOrg = "";
			List<Presw18DTO>  totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
			if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
				for (Presw18DTO ss : totalListaOrganizaciones) {	
				    codOrg = ss.getNomtip().trim();
				    break;
				}
			}
			    
			
			try { 
			boolean graba = moduloOrdenCompraCentralizadaB.saveRechaza(numdoc, rutUsuario,dv,"RS3", glosa,"",codOrg);
		    if(graba)
		    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el rechazo de la ReAutorizaci�n.");
		    else
		    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la ReAutorizaci�n.");
			} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado rechazaAutOrdenCompra.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
			Thread.sleep(500);
		    cargaMenuOrdenCompraC(accionweb);
			
		}
	 synchronized public void  rechazaRecepOrdenCompra(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);	
			String glosa = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "","");
			
			String codOrg = "";
			List<Presw18DTO>  totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
			if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
				for (Presw18DTO ss : totalListaOrganizaciones) {	
				    codOrg = ss.getNomtip().trim();
				    break;
				}
			}
			    
			
			try { 
			boolean graba = moduloOrdenCompraCentralizadaB.saveRechaza(numdoc, rutUsuario,dv,"RS4", glosa,"",codOrg);
		    if(graba)
		    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el rechazo de la Recepci�n en Finanzas.");
		    else
		    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Recepci�n en Finanzas.");
			} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado rechazaRecepOrdenCompra.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
			Thread.sleep(500);
		    cargaMenuOrdenCompraC(accionweb);
			
		}
	 synchronized public void anulaOrdenCompra(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);	
				
			String codOrg = "";
			List<Presw18DTO>  totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
			if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
				for (Presw18DTO ss : totalListaOrganizaciones) {	
				    codOrg = ss.getNomtip().trim();
				    break;
				}
			}
			    
			
			try { 
	//		boolean graba = moduloOrdenCompraCentralizadaB.saveRechaza(numdoc, rutUsuario,dv,"RCC", glosa,"",codOrg);
	/*	    if(graba)
		    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el rechazo de la Autorizaci�n.");
		    else
		    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Autorizaci�n.");
		    	*/
			} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado rechazaAutOrdenCompra.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
			Thread.sleep(500);
		    cargaMenuOrdenCompraC(accionweb);
			
		}	
	 synchronized public void eliminaOrdenCompra(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			
	     	boolean resultado = moduloOrdenCompraCentralizadaB.getEliminaOrdenCompra(rutUsuario,dv,numdoc,"NCC","");
		    if(resultado)
			   accionweb.agregarObjeto("mensaje", "Qued� anulada la Orden de Compra");
		
		    Thread.sleep(500);
			this.cargaMenuOrdenCompraC(accionweb);
		
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
				accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
			if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
				accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
			if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
				accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
			if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			   accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
				
		}
	 /*  revisar esta grabaci�n proceso Rap**/
	 synchronized public void registraValorDefinitivo(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			
			List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
			List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("totalListaProductos");
			long fechaEntrega = Util.validaParametro(accionweb.getParameter("fechaEntrega"),0);
			String codigoEntrega = Util.validaParametro(accionweb.getParameter("codigoEntrega"),"");
			String importacion = Util.validaParametro(accionweb.getParameter("importacion"),"");
			String divisa = Util.validaParametro(accionweb.getParameter("divisa"),"");
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			
					
			try {
				
			    int numOrden = 0;
				boolean registra = moduloOrdenCompraCentralizadaB.getReasignaOrdenCompra(accionweb.getReq(),rutUsuario,dv, numDoc);
			    if(registra){
					accionweb.agregarObjeto("mensaje", "Se registr� valores definitivos.");
					
			    }else
			      	accionweb.agregarObjeto("mensaje", "Problema en registrar valores definitivos.");
			
				} catch (Exception e) {
					accionweb.agregarObjeto("mensaje", "No guardado registraValorDefinitivo.");
					accionweb.agregarObjeto("exception", e);
					e.printStackTrace();
				}
			
				/****/
			
			this.consultaOrdenCompra(accionweb);
			accionweb.agregarObjeto("numDoc", numDoc);
		 	accionweb.agregarObjeto("esOrdenCompraCB", 1);  
		 	accionweb.agregarObjeto("opcion", tipo);
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
				accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
			if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
				accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
			if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
				accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
			if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			   accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		
		}
	 synchronized public void actualizaValorDefinitivo(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			
			List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
			List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("totalListaProductos");
			long fechaEntrega = Util.validaParametro(accionweb.getParameter("fechaEntrega"),0);
			String codigoEntrega = Util.validaParametro(accionweb.getParameter("codigoEntrega"),"");
			String importacion = Util.validaParametro(accionweb.getParameter("importacion"),"");
			String divisa = Util.validaParametro(accionweb.getParameter("divisa"),"");
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
					
			try {
			   	boolean autorizado = false;
			   	autorizado = moduloOrdenCompraCentralizadaB.getAutorizado(numDoc);
		  // 	autorizado = true;
				if(autorizado){
					String mensaje = this.enviaBanner(accionweb);
					accionweb.agregarObjeto("mensaje", mensaje);
			
				} else
					accionweb.agregarObjeto("mensaje", "No est� autorizado para registar la Orden de Compra a Banner, necesita Reautorizaci�n.");
					
			   
			
				} catch (Exception e) {
					accionweb.agregarObjeto("mensaje", "No guardado autorizaValePago.");
					accionweb.agregarObjeto("exception", e);
					e.printStackTrace();
				}
			this.consultaOrdenCompra(accionweb);
			accionweb.agregarObjeto("numDoc", numDoc);
		 	accionweb.agregarObjeto("opcion", tipo);
		 	accionweb.agregarObjeto("esOrdenCompraCB", 1);    
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
				accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
			if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
				accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
			if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
				accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
			if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			   accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		
		}
public String enviaBanner (AccionWeb accionweb) throws Exception {
	int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
	String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
	int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
	
	
	List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
	List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("totalListaProductos");
	int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
	String mensaje = "";
	/***/
	int sobrepasa = 0;
	consultaOrdenCompra(accionweb);
	boolean sobrepasaOrg = this.revisaSaldoOrganizacion(totalListaOrganizaciones); // Se revisan solo los saldos de los autorizadores de sus org.
 	// si no estan sobrepasados los saldos
 	if (!sobrepasaOrg){
		 	accionweb.getSesion().setAttribute("procesoSesion", "OC3");
			consultaOrdenCompra(accionweb);
			if(accionweb.getSesion().getAttribute("procesoSesion") != null)
				accionweb.getSesion().removeAttribute("procesoSesion");
		
			totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
			sobrepasaOrg = this.revisaSaldoOrganizacion(totalListaOrganizaciones); // Se revisan los saldos todas las org del vale.
			if (sobrepasaOrg) { // si estan sobrepasados los saldos 
					sobrepasa = 1;
					mensaje = "No se puede Autorizar la Orden de Compra por Organizaciones sin saldo suficiente";
					Thread.sleep(1500);
					totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
			}
				else sobrepasa = 0;  // todo ok y vamos a autorizar el vale
		
    }
	else { // si estan sobrepasados los saldos  envia mensaje de sin saldo suficiente
		sobrepasa = 1;
		mensaje = "Organizaci�n sin Saldo suficiente";
	}
	if(sobrepasa == 0){
		 	 String rutDvIngresador = moduloOrdenCompraCentralizadaB.getRutOperacion(numDoc,"I");
		 	// String rutDvAutorizador = moduloOrdenCompraCentralizadaB.getRutOperacion(numDoc,"A");  
		 	 String rutDvAutorizador = String.valueOf(rutUsuario) + dv; 
		 	 //String rutdv,String numvale, String fecha_invoice, String fecha_pmt_due, String fecha_trans, String cod_doct, String datos
		     Vector vec_COCOF150B = moduloOrdenCompraCentralizadaB.getDatosCOCOF150B(numDoc);
		     
		     String fechaEnt = "";
		     String lugarEnt = "";
		     String rutDvProv = "";
		     String codOrgOrden = "";
		     String codDivisa = "";
		     Vector vec2 = new Vector();
		     if (vec_COCOF150B.size() > 0) {
		    	for (int j=0;j < vec_COCOF150B.size(); j++){ 
		    	 vec2 = (Vector) vec_COCOF150B.get(0);	
		    	 fechaEnt = vec2.get(0)+"";
			     lugarEnt = vec2.get(1)+"";
			     codDivisa = vec2.get(2)+"";
			     rutDvProv = vec2.get(3)+"" + vec2.get(4)+"";
			     codOrgOrden = vec2.get(5)+"";
				    	 
	    		 Vector vec_datos = new Vector();
	    		 vec_datos = moduloOrdenCompraCentralizadaB.getDatosProductos(numDoc, Integer.parseInt(vec2.get(3)+""), vec2.get(4)+"");
	    		 String datos_parametro = "";
	    		 Vector vec = new Vector();
	    		    		 
	    		 for (int i = 0; i < vec_datos.size(); i++) {
	    			 vec = (Vector) vec_datos.get(i);
	    			 for (int y = 0; y < vec.size(); y++) {
	    				if (y == (vec.size()-1)) datos_parametro = datos_parametro + vec.get(y) + "@"; 
	    				else datos_parametro = datos_parametro + vec.get(y) + "&";
	    			 }
	    		 }
	    		//System.out.println("datos_parametro " + datos_parametro); 
	    		int error = moduloOrdenCompraCentralizadaB.saveOrdenCompra(rutUsuario,dv,String.valueOf(numDoc),fechaEnt,lugarEnt,codDivisa,rutDvProv,codOrgOrden,
	    				                                                   datos_parametro,rutDvIngresador,rutDvAutorizador, "G");
	    		if(error == -12)
	    			 mensaje =  "Problemas en la grabaci�n del archivo Historial del documento en I-Series";
	    		else if (error < 0)
	    			mensaje =  "Problemas en la grabaci�n A BANNER";
	    		     else  mensaje =  "Se grab� en forma exitosa la autorizaci�n";
		     }
		     }
	}  	 
//   accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa la reasignaci�n de valores definitivos de Orden de Compra Centralizada, el n�mero interno es: "+ numOrden);
if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
	accionweb.agregarObjeto("registra", 1);
    accionweb.getSesion().removeAttribute("totalListaOrganizaciones");
    accionweb.getSesion().setAttribute("totalListaOrganizaciones", null);
    accionweb.getSesion().removeAttribute("totalListaProductos");
	accionweb.getSesion().setAttribute("totalListaProductos", null); 
	accionweb.getSesion().removeAttribute("valorRemanente");
     
}
return mensaje;
}
	 
}
