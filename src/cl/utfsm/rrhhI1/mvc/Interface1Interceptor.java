package cl.utfsm.rrhhI1.mvc;



import java.util.ArrayList;
import java.util.HashMap;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.rrhhI1.datos.Appef400DAO;
import cl.utfsm.rrhhI1.datos.Appef401DAO;
import cl.utfsm.rrhhI1.datos.ProcesosDAO;
import cl.utfsm.rrhhI1.modulo.Appef400;
import cl.utfsm.rrhhI1.modulo.Appef401;
import cl.utfsm.rrhhI1.modulo.EquivalenciaNivel;
import cl.utfsm.rrhhI1.modulo.EquivalenciaPlanta;
import cl.utfsm.rrhhI1.modulo.ModuloInterface1;


public class Interface1Interceptor  extends HandlerInterceptorAdapter {
	ModuloInterface1 moduloInterface1;

	public ModuloInterface1 getModuloInterface1() {
		return moduloInterface1;
	} 

	public void setModuloInterface1(ModuloInterface1 moduloInterface1) {
		this.moduloInterface1 = moduloInterface1;
	}
	
	public ArrayList<Appef400> guardaActulizacion(HashMap<Integer, Appef400> listContrato,HashMap<Integer, Appef401> listDatoPersonal
													,HashMap<Integer, Appef400> listaConEnAS
													,HashMap<Integer, Appef401> ListaPerEnAS){
		
		HashMap<String, EquivalenciaPlanta> mapEquPlanta = new HashMap<String, EquivalenciaPlanta>();
		HashMap<String, EquivalenciaNivel> mapEquNivel   = new HashMap<String, EquivalenciaNivel>();		
		
		ProcesosDAO.mapDeEquivalencia(mapEquNivel, mapEquPlanta);
		EquivalenciaPlanta equePlanta;
		ArrayList<Appef400> listProble = new ArrayList<Appef400>();
		int problemas = 0;	
		Appef400  rp = new  Appef400(); 
		for ( Appef400 appef400 : listaConEnAS.values()) {				
			rp = listContrato.get(appef400.getRutide());
			if(rp != null){
				 listContrato.get(appef400.getRutide()).actulizarDatosExistentes(appef400); 
			}			 	   
	    }			
		for (Appef400 con : listContrato.values()) {			
			if(con.getFecini()== 0){
				con.setError(" No tiene fecha de inicio");
				listProble.add(con);
			}else{		 
				
				if( con.getTipcon() < 1 || con.getTipcon() > 7 || con.getTipcon() == 3 ||con.getTipcon() == 4 ||con.getTipcon() == 6 ){
					con.setError("No tiene tipo de contrato o tipo de contrato inv&aacute;lido");
					listProble.add(con);
				}else{			
					if(con.getCodpla() != null && !con.getCodpla().equals("")){
						equePlanta = mapEquPlanta.get(con.getCodpla());
						if(equePlanta != null){
							con.setCodpla(equePlanta.getCodplaAS());
							if(con.getNumniv() > 0){								 
								try {
									con.setNumniv(mapEquNivel.get(equePlanta.getCodesc()+"-"+con.getNumniv()).getNivel());
								} catch (Exception e) {
									con.setNumniv(0);
									con.setError("tiene problema de equivalencia nivel");
									listProble.add(con);						
									problemas++;
								}					
							}
						}else {
							try {
								Integer.parseInt(con.getCodpla());
							} catch (Exception e) {
								listProble.add(con);
								con.setError("tiene problema de equivalencia planta");
								problemas++;					
								System.out.print("\n con problemas:"+con.getCodpla()+""+"\n");
							}							
						}
					}					
				}
			}
		}
		
		for (Appef400 appef400 : listProble) {
			listContrato.values().remove(appef400);
		}		
		ProcesosDAO.actuProble(listProble);	
		Appef401 pr = new Appef401();
		
		for ( Appef401 appef401 : ListaPerEnAS.values()) {
			pr = listDatoPersonal.get(appef401.getRutide());
			if (pr != null) {
				listDatoPersonal.get(appef401.getRutide()).atualirDatos(appef401); 	
			}			   
	    }				
		Appef401DAO.guardarHastMap(listDatoPersonal);
		Appef400DAO.guardarHashMapContratos(listContrato,listProble);
		return listProble;
	}	
	
	public void alumnosAFuncionarios(HashMap<Integer,Appef401> listpersona ){
		
		ArrayList<Appef400> listRutFaltantes = Appef400DAO.alumnosAfuncionles();
		if(listRutFaltantes.size()>0){
			ArrayList<Appef401> listDatosPers =	ProcesosDAO.DatosPersFaltantes(listRutFaltantes);		
										
			Appef401DAO.guardarHastMap(listDatosPers);
		    for (Appef401 appef401 : listDatosPers) {
		    	listpersona.put(appef401.getRutide(), appef401);
			}
		}		
	}
		
	public void actualizarASContrato(AccionWeb accionweb){
		System.out.print("Interface 1 \n");
		System.out.print("comienzo de actualización contratos \n");
		HashMap<Integer, Appef400> listContrato = new HashMap<Integer, Appef400>() ;
		HashMap<Integer, Appef401> listDatoPersonal = new HashMap<Integer, Appef401>();	
		HashMap<Integer, Appef400> listaConEnAS = new HashMap<Integer, Appef400>();		
		HashMap<Integer, Appef401> ListaPerEnAS = new HashMap<Integer, Appef401>();			
		boolean problema = false;
		problema = ProcesosDAO.buscarCambios(listContrato, listDatoPersonal);
		if(listContrato.size() > 0 && !problema){
			problema = Appef400DAO.listaContratroAs(listContrato,listaConEnAS);
		}
		if(!problema){
			if(listDatoPersonal.size() > 0){
				problema = Appef401DAO.lisAppef01AS(listDatoPersonal,ListaPerEnAS);
			}
		}		
		ArrayList<Appef400> listProbleCon = new ArrayList<Appef400>();
		ArrayList<Appef400> listProblePer = new ArrayList<Appef400>();
		if(!problema){			
			//guardar informacion 
			listProbleCon = guardaActulizacion( listContrato , listDatoPersonal, listaConEnAS, ListaPerEnAS);
			alumnosAFuncionarios( listDatoPersonal );
		}else{
			//volver atras
			ProcesosDAO.vorverFechaAnterior();
		}		
		//actualizaciones	
		accionweb.agregarObjeto("problema"      , problema);			
		accionweb.agregarObjeto("rutProblema"   , listProbleCon);
		accionweb.agregarObjeto("rutProblemaPer", listProblePer);
		accionweb.agregarObjeto("cambPersona"   , listDatoPersonal.size());
		accionweb.agregarObjeto("cambContra"    , listContrato.size());		
		System.out.print("FIN de actualización contratos \n");		
	}
	public void cargarMenu(AccionWeb accionweb) throws Exception {		
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int tipoAcc = Util.validaParametro(accionweb.getParameter("tipoAcc"), 0); 
		accionweb.agregarObjeto("esInterface1", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		switch (tipoAcc) {
		case 2:  
			actualizarASContrato(accionweb);
			break;
		default:
			break;
		}		
	}	
}
