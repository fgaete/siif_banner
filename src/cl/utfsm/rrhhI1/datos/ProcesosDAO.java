package cl.utfsm.rrhhI1.datos;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import oracle.jdbc.OracleTypes;
import cl.utfsm.conexion.ConexionAs400RRHHI1;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhI1.modulo.Appef400;
import cl.utfsm.rrhhI1.modulo.Appef401;
import cl.utfsm.rrhhI1.modulo.EquivalenciaNivel;
import cl.utfsm.rrhhI1.modulo.EquivalenciaPlanta;

public class ProcesosDAO {
	/**
	 * @author yonis.vergara
	 * @descripcion: recupera los datos personales de las personas que no pasaron 
	 * 					sus datos personales por haber sido alumno antes de trabajador 
	 * 
	 * 
	 */
	public static ArrayList<Appef401> DatosPersFaltantes(ArrayList<Appef400> rutFaltantes ){
		
		ArrayList<Appef401> listDatosFaltantes = new ArrayList<Appef401>();		
		Appef401 per;
		ResultSet rs ;
		ConexionBannerRRHH con =   new ConexionBannerRRHH();		
		try {
			con.getConexion().setAutoCommit(false);
			Appef400DAO.cargarTmpAppef400(con, rutFaltantes);		
			CallableStatement sent = null;	
			String sql = "{call PKG_INTERFACE1_0_3.DATOS_PERSONALES_FALTANTES( "
																   +" ? "//PO_DATA_PERSONAL => PO_DATA_PERSONAL																   
																  +" )} ";														    		
			sent = con.getConexion().prepareCall(sql);
			sent.registerOutParameter(1, OracleTypes.CURSOR);			
			sent.execute();
			rs = (ResultSet)sent.getObject(1);			
			while (rs.next()) {
	    		   per = new Appef401();
	    		   per.setRutide(rs.getInt("RUTIDE")) ;
	    		   per.setDigide(rs.getString("DIGIDE"));
	    		   per.setApepat(rs.getString("APEPAT"));
	    		   per.setApemat(rs.getString("APEMAT"));
	    		   per.setNombre(rs.getString("NOMBRE"));
	    		   per.setEstciv(rs.getInt("ESTCIV"));
	    		   per.setSexo(rs.getString("SEXO"));
	    		   per.setFecnac(rs.getInt("FECNAC"));
	    		   per.setLugnac(rs.getString("LUGNAC"));
	    		   per.setPainac(rs.getString("PAINAC"));
	    		   per.setNacion(rs.getInt("NACION"));
	    		   per.setDetnac(rs.getString("DETNAC"));
	    		   per.setDomici(rs.getString("DOMICI"));
	    		   per.setComuna(rs.getInt("COMUNA"));
	    		   per.setCiudad(rs.getInt("CIUDAD"));
	    		   per.setRegion(rs.getInt("REGION"));
	    		   per.setCodgra(rs.getInt("CODGRA"));
	    		   per.setGratit(rs.getInt("GRATIT"));
	    		   per.setCodtit(rs.getInt("CODTIT"));
	    		   per.setFecing(rs.getInt("FECING"));
	    		   per.setTelefo(rs.getInt("TELEFO"));
	    		   per.setCelula(rs.getInt("CELULA"));
	    		   per.setCorele(rs.getString("CORELE"));
	    		   per.setDomele(rs.getString("DOMELE"));		    		   
	    		   if(rs.getInt("RUTIDE") == 5441782){
	    			   System.out.print("hola mundo");
	    		   }	    		   		    		   
	    		   listDatosFaltantes.add( per);				
	    	   }
			
			rs.close();
		    sent.close();
		    
			
		} catch (SQLException e) {
			System.out.println("ErrorSQL Procesos.buscarCambios: " + e);	
			
			
		}catch (Exception e) {
			System.out.println("Error Procesos.buscarCambios: " + e);
			
		}	
		try {
			con.getConexion().close();
		} catch (SQLException e) {
			System.out.println("ErrorSQL Procesos.buscarCambios Cierre conexion : " + e);				
		}		
		return listDatosFaltantes;	
		
	}
	
	/**
	 * @author yonis.vergara
	 * @param equiNivel
	 * @param equiPlanta
	 * 
	 * descripcion:optencion de quivalencias para el proceso de interface 1
	 * 
	 */
	
	public static void mapDeEquivalencia(HashMap<String, EquivalenciaNivel > equiNivel, HashMap<String, EquivalenciaPlanta> equiPlanta  ){
		
		listEquiPasoNivel(equiNivel);
		listEquiPlantaAAS400(equiPlanta);
		
	}
	/**
	 * @author yonis.vergara
	 * @param equiNivel
	 * @param equiPlanta
	 * descripcion: busqueda de equivalencia de para interface 1
	 */
	
	public static void mapEquiAs400ABanner(HashMap<String, EquivalenciaNivel > equiNivel, HashMap<String, EquivalenciaPlanta> equiPlanta){
		listEquiNivelPaso(equiNivel);
		listEquiPlantaABanner(equiPlanta);
		
	}
	/**
	 * @author yonis.vergara
	 * @param listEquiNivel
	 * descripcion: busqueda de equivalencia de nivel
	 */
	private static void listEquiPasoNivel(HashMap<String, EquivalenciaNivel> listEquiNivel){
		
		String sql = "SELECT CODESC, NUMNIV, NIVBAN FROM REMUNERABT.APREF11 ";
		ConexionAs400RRHHI1 con = new ConexionAs400RRHHI1();
		
		if(listEquiNivel == null ){
			listEquiNivel = new HashMap<String, EquivalenciaNivel>();
		}
		
		try {
			PreparedStatement sent = con.getConnection().prepareStatement(sql);
			
			try {
				ResultSet rs = sent.executeQuery();
				EquivalenciaNivel nivel ;
				while (rs.next()) {
					nivel = new EquivalenciaNivel();
					nivel.setCodesc(rs.getInt("CODESC"));
					nivel.setNivel(rs.getInt("NUMNIV"));
					nivel.setPaso(rs.getInt("NIVBAN"));
					listEquiNivel.put(rs.getInt("CODESC")+"-"+rs.getInt("NIVBAN"), nivel);
				}
				
			} catch (Exception e) {
				System.out.println("ErrorSQL Procesos.listEquiNivel: " + e);	
			}
			
			sent.close();
			con.getConnection().close();
		} catch (SQLException e) {
			System.out.println("ErrorSQL Procesos.listEquiNivel: " + e);	
		}		
	}
	/**
	 * @author yonis.vergara
	 * @param listEquiNivel
	 * descripcion: busqueda de equivalencia de nivel
	 */
	private static void listEquiNivelPaso(HashMap<String, EquivalenciaNivel> listEquiNivel){
		
		String sql = "SELECT CODESC, NUMNIV, NIVBAN,VALNIV FROM REMUNERABT.APREF11 ";
		ConexionAs400RRHHI1 con = new ConexionAs400RRHHI1();
		
		if(listEquiNivel == null ){
			listEquiNivel = new HashMap<String, EquivalenciaNivel>();
		}
		
		try {
			PreparedStatement sent = con.getConnection().prepareStatement(sql);
			
			try {
				ResultSet rs = sent.executeQuery();
				EquivalenciaNivel nivel ;
				while (rs.next()) {
					nivel = new EquivalenciaNivel();
					nivel.setCodesc(rs.getInt("CODESC"));
					nivel.setNivel(rs.getInt("NUMNIV"));
					nivel.setPaso(rs.getInt("NIVBAN"));
					nivel.setValor(rs.getInt("VALNIV"));
					listEquiNivel.put(rs.getInt("CODESC")+"-"+rs.getInt("NUMNIV"), nivel);
				}
				
			} catch (Exception e) {
				System.out.println("ErrorSQL Procesos.listEquiNivel: " + e);	
			}
			
			sent.close();
			con.getConnection().close();
		} catch (SQLException e) {
			System.out.println("ErrorSQL Procesos.listEquiNivel: " + e);	
		}		
	}
	/**
	 * @author yonis.vergara
	 * @param listEquiPlanta
	 * descripcion: busqueda de equivalencia planta
	 */
	private static void listEquiPlantaAAS400( HashMap<String, EquivalenciaPlanta> listEquiPlanta ){
		
		String sql ="SELECT CODPLA, CODESC, CLABAN FROM REMUNERABT.APREF07";
		ConexionAs400RRHHI1 con = new ConexionAs400RRHHI1();
		
		try {
			PreparedStatement sent = con.getConnection().prepareStatement(sql);
			
			try {
				
				ResultSet rs = sent.executeQuery();
				EquivalenciaPlanta equiPlanta;
				while (rs.next()) {
					equiPlanta = new EquivalenciaPlanta();
					equiPlanta.setCodesc(rs.getInt("CODESC"));
					equiPlanta.setCodplaAS(rs.getInt("CODPLA"));
					equiPlanta.setCodplaBanner(rs.getString("CLABAN"));
					listEquiPlanta.put(rs.getString("CLABAN"),equiPlanta );
				}				
			} catch (Exception e) {
				System.out.println("ErrorSQL Procesos.listEquiPlanta: " + e);	
			}
			
			sent.close();
			con.getConnection().close();
			
		} catch (SQLException e) {
			System.out.println("ErrorSQL Procesos.listEquiPlanta: " + e);	
		}
		
		
	}
	/**
	 * @author yonis.vergara
	 * @param listEquiPlanta
	 * descripcion: busquedad de equivalencia de planta
	 */
	private static void listEquiPlantaABanner( HashMap<String, EquivalenciaPlanta> listEquiPlanta ){
		
		String sql ="SELECT CODPLA, CODESC, CLABAN FROM REMUNERABT.APREF07";
		ConexionAs400RRHHI1 con = new ConexionAs400RRHHI1();
		
		try {
			PreparedStatement sent = con.getConnection().prepareStatement(sql);
			
			try {
				
				ResultSet rs = sent.executeQuery();
				EquivalenciaPlanta equiPlanta;
				while (rs.next()) {
					equiPlanta = new EquivalenciaPlanta();
					equiPlanta.setCodesc(rs.getInt("CODESC"));
					equiPlanta.setCodplaAS(rs.getInt("CODPLA"));
					equiPlanta.setCodplaBanner(rs.getString("CLABAN"));
					listEquiPlanta.put(rs.getString("CODPLA"),equiPlanta );
				}				
			} catch (Exception e) {
				System.out.println("ErrorSQL Procesos.listEquiPlanta: " + e);	
			}
			
			sent.close();
			con.getConnection().close();
			
		} catch (SQLException e) {
			System.out.println("ErrorSQL Procesos.listEquiPlanta: " + e);	
		}
		
		
	}
	
	/**
	 * @author yonis.vergara
	 * desccripcion: permite volver a la fecha anterior de ejecucion de interface 1
	 * 
	 */
	public static void vorverFechaAnterior(){
		
		String sql = "{call PKG_CONSULTA_USUARIO_RRHH.VOLVER_A_FECHA_ANTERIOR()}";
		
		CallableStatement sent = null;
		ConexionBannerRRHH con =   new ConexionBannerRRHH();
		try {
			sent = con.getConexion().prepareCall(sql);
			sent.execute();
			
		} catch (SQLException e) {
			System.out.println("SQLError Procesos.vorverFechaAnterior: " + e);	
		} catch (Exception e) {
			System.out.println("Error Procesos.vorverFechaAnterior: " + e);	
		}
		try {
			con.getConexion().close();
		}catch (SQLException e) {
			System.out.println("SQLError Procesos.vorverFechaAnterior:cerrar conexion " + e);	
		} catch (Exception e) {
			System.out.println("Error Procesos.vorverFechaAnterior:cerrar conexion " + e);	
		}
		
	}
	
	
	/**
	 * @author yonis.vergara
	 * @param datoCon
	 * @param datoPers
	 * @return
	 * descripcion: ejecuta el procedimiento que debuelve los cambios realisados despues de la fecha registrado en PVTPIVT
	 */
	public static boolean buscarCambios( HashMap<Integer, Appef400> datoCon ,HashMap<Integer, Appef401> datoPers ){
		
		
		if(datoCon == null){
			datoCon = new HashMap<Integer, Appef400>();
		}
		if(datoPers == null){
			datoPers = new HashMap<Integer, Appef401>();
		}
		Appef400 cont;
		Appef401 per;
		ResultSet rs ;
		ConexionBannerRRHH con =   new ConexionBannerRRHH();	
		boolean problema = false;
		try {
		
		con.getConexion().setAutoCommit(false);
		CallableStatement sent = null;	
	    String sql = "{call PKG_INTERFACE1_0_3.ACTUALIZAR_CONTRA_EN_AS( "
																   +" ? "//PO_DATA_PERSONAL => PO_DATA_PERSONAL
																   +",? "//PO_DATA_CONTRATO => PO_DATA_CONTRATO
																  +" )} ";
														    		
			sent = con.getConexion().prepareCall(sql);
			sent.registerOutParameter(1, OracleTypes.CURSOR);
			sent.registerOutParameter(2, OracleTypes.CURSOR);		
			
			sent.execute();
			rs = (ResultSet)sent.getObject(2);
			
			while (rs.next()) {
				   cont = new Appef400();
				   cont.setRutide(rs.getInt("RUTIDE")) ;
				   
	    		   cont.setDigide(rs.getString("DIGIDE"));
	    		   cont.setTipcon(rs.getInt("TIPCON"));
	    		   cont.setEscfon(rs.getInt("ESCFON"));
	    		   cont.setCodpla(rs.getString("CODPLA"));
	    		   cont.setNumniv(rs.getInt("NUMNIV"));
	    		   cont.setCodsuc(rs.getInt("CODSUC"));
	    		   cont.setCodcar(rs.getInt("CODCAR"));    		   
	    		   cont.setSumfij(rs.getInt("SUMFIJ"));
	    		   cont.setJornad(rs.getInt("JORNAD"));
	    		   cont.setCanhor(rs.getDouble("CANHOR"));
	    		   cont.setFecini(rs.getInt("FECINI"));
	    		   cont.setFecfin(rs.getInt("FECFIN"));
	    		   cont.setFecant(rs.getInt("FECANT"));
	    		   cont.setFecter(rs.getInt("FECTER"));
	    		   cont.setIndcol(rs.getInt("INDCOL"));
	    		   cont.setIndmov(rs.getInt("INDMOV"));
	    		   cont.setCodafp(rs.getInt("CODAFP"));
	    		   cont.setIndjub(rs.getInt("INDJUB"));
	    		   cont.setIndsal(rs.getInt("INDSAL"));
	    		   cont.setValaho(rs.getInt("VALAHO"));
	    		   cont.setUniaho(rs.getString("UNIAHO"));
	    		   cont.setCodisa(rs.getInt("CODISA"));
	    		   cont.setUniisa(rs.getString("UNIISA"));
	    		   cont.setValisa(rs.getDouble("VALISA"));	    		   
	    		   cont.setAntqui(rs.getInt("ANTQUI"));
	    		   cont.setInddep(rs.getInt("INDDEP"));
	    		   cont.setSermed(rs.getInt("SERMED"));
	    		   cont.setCuomor(rs.getInt("CUOMOR"));
	    		   cont.setSindi1(rs.getInt("SINDI1"));
	    		   cont.setSindi2(rs.getInt("SINDI2"));
	    		   cont.setSindi3(rs.getInt("SINDI3"));
	    		   cont.setTipjer(rs.getString("TIPJER"));
	    		   cont.setHorini(rs.getInt("HORINI"));
	    		   cont.setHorfin(rs.getInt("HORFIN"));
	    		   cont.setFecing(rs.getInt("FECING"));
	    		   cont.setSerpr1(rs.getString("SERPR1"));
	    		   cont.setSerpr2(rs.getString("SERPR2"));
	    		   cont.setSegces(rs.getInt("SEGCES"));
	    		   cont.setIndbon(rs.getInt("INDBON"));
	    		   cont.setCodban(rs.getInt("CODBAN"));
	    		   cont.setSucban(rs.getInt("SUCBAN"));
	    		   cont.setCueban(rs.getString("CUEBAN"));
	    		   cont.setMRetencion(rs.getInt("MRETENCION"));
	    		   cont.setFeccon(rs.getInt("FECCON"));
	    		   cont.setFecpas(rs.getInt("FECPAS"));
	    		   cont.setUniaug(rs.getString("UNIAUG"));
	    		   cont.setValaug(rs.getDouble("VALAUG"));
	    		   cont.setFecint(rs.getInt("FECINT"));
	    		   cont.setCodorg(rs.getString("CODORG"));
	    		   cont.setTippue(rs.getString("TIPPUE"));
	    		   cont.setCodpue(rs.getString("CODPUE"));
	    		   cont.setBaner1(rs.getString("BANER1"));
	    		   cont.setBaner2(rs.getString("BANER2"));
	    		   cont.setBaner3(rs.getString("BANER3"));		   
	    		   cont.setPidm(rs.getInt("PIDM"));		    		   
	    		   datoCon.put(rs.getInt("RUTIDE"), cont);
				
			}
			
			rs = (ResultSet)sent.getObject(1);
			
			while (rs.next()) {
	    		   per = new Appef401();
	    		   per.setRutide(rs.getInt("RUTIDE")) ;
	    		   per.setDigide(rs.getString("DIGIDE"));
	    		   per.setApepat(rs.getString("APEPAT"));
	    		   per.setApemat(rs.getString("APEMAT"));
	    		   per.setNombre(rs.getString("NOMBRE"));
	    		   per.setEstciv(rs.getInt("ESTCIV"));
	    		   per.setSexo(rs.getString("SEXO"));
	    		   per.setFecnac(rs.getInt("FECNAC"));
	    		   per.setLugnac(rs.getString("LUGNAC"));
	    		   per.setPainac(rs.getString("PAINAC"));
	    		   per.setNacion(rs.getInt("NACION"));
	    		   per.setDetnac(rs.getString("DETNAC"));
	    		   per.setDomici(rs.getString("DOMICI"));
	    		   per.setComuna(rs.getInt("COMUNA"));
	    		   per.setCiudad(rs.getInt("CIUDAD"));
	    		   per.setRegion(rs.getInt("REGION"));
	    		   per.setCodgra(rs.getInt("CODGRA"));
	    		   per.setGratit(rs.getInt("GRATIT"));
	    		   per.setCodtit(rs.getInt("CODTIT"));
	    		   per.setFecing(rs.getInt("FECING"));
	    		   per.setTelefo(rs.getInt("TELEFO"));
	    		   per.setCelula(rs.getInt("CELULA"));
	    		   per.setCorele(rs.getString("CORELE"));
	    		   per.setDomele(rs.getString("DOMELE"));    		       		   		    		   
	    		   datoPers.put(rs.getInt("RUTIDE"), per);				
	    	   }
			
			rs.close();
		    sent.close();
		    
			
		} catch (SQLException e) {
			System.out.println("ErrorSQL Procesos.buscarCambios: " + e);	
			problema = true;
			
		}catch (Exception e) {
			System.out.println("Error Procesos.buscarCambios: " + e);
			problema = true;
		}	
		try {
			con.getConexion().commit();
			con.getConexion().close();
		} catch (SQLException e) {
			System.out.println("ErrorSQL Procesos.buscarCambios Cierre conexion : " + e);	
			problema = true;
		}		
		return problema;		
	}
	
	/**
	 * @author yonis.vergara
	 * @param listCon
	 * 
	 */
	
	public static void actuProble(ArrayList<Appef400> listCon){		
		String sql = "{call PKG_CONSULTA_USUARIO_RRHH.UPDATE_NBRJOBS_ACTIVITY(PI_PIDM => ?) }";		
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		CallableStatement sent;
		for (Appef400 cont : listCon) { 			
			try {
				sent = con.getConexion().prepareCall(sql);				
				sent.setInt(1, cont.getPidm());
				sent.execute();
				sent.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("Error Procesos.actuProble: " + e);	
			}		
		}		
		try {
			con.getConexion().close();		
		} catch (SQLException e) {
			System.out.println("SQLError Procesos.actuProble con cierre conexion: " + e);	
		}
		
	}	

}
