package cl.utfsm.rrhhI1.datos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import cl.utfsm.conexion.ConexionAs400RRHHI1;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhI1.modulo.Appef400;


public class Appef400DAO {
	
	public static ArrayList<Appef400> alumnosAfuncionles(){
		//WHERE FECING < 20180501
		String sql = " SELECT RUTIDE , DIGIDE " +
					 " FROM TRANSFERBT.APPEF400 trCon " +
					 " WHERE RUTIDE not in (select RUTIDE from TRANSFERBT.APPEF401) and " + 
					 " RUTIDE not in (select RUTIDE from REMUNERABT.APPEF01  ) ";
		ArrayList<Appef400> listDatosPers = new ArrayList<Appef400>();
		try {
			ConexionAs400RRHHI1 con = new ConexionAs400RRHHI1();
			PreparedStatement sent = con.getConnection().prepareStatement(sql);
			
			ResultSet rs = sent.executeQuery();
			Appef400 datoPers = null;
			while(rs.next()){
				datoPers = new Appef400();
				datoPers.setRutide(rs.getInt("RUTIDE"));
				datoPers.setDigide(rs.getString("DIGIDE"));
				listDatosPers.add(datoPers);
			}
			rs.close();
			sent.close();
			con.getConnection().close();
		} catch (SQLException e) {
			System.out.print("SQLError Appef401DAO.alumnosAfuncionles: "+ e.getMessage() );
		} catch (Exception e) {
			System.out.print("Error Appef401DAO.alumnosAfuncionles: "+ e.getMessage() );
		}
		
		return listDatosPers;
	}
	
	/**
	 * Fecha de creacion 13-08-2018
	 * Creado por: Yonis Vergara
	 * Nombre metodo
	 * 	cargarTmpAppef401
	 * Descripcion:
	 * 	Carga la tabla temporal TMP_APPEF401 para revisar los datos personales faltantes
	 *  Nota por razones de  la tabla temporal este metodo no cierra conexcion
	 * @param
	 * 	con: ya avierta est no se cerrara !!! importante !!!
	 * 	listRut :se reciven los rut que se insertaran
	 */
	
	public static void cargarTmpAppef400(ConexionBannerRRHH con, ArrayList<Appef400> listRut){
		
		String sql = "INSERT INTO TMP_APPEF400(RUTIDE,DIGIDE) VALUES(?,?)";
		
		try {
			PreparedStatement sent = con.getConexion().prepareStatement(sql);
			for (Appef400 appef400 : listRut) {
				sent.setInt(1, appef400.getRutide());
				sent.setString(2, appef400.getDigide());
				sent.addBatch();
			}
			sent.executeBatch();
		} catch (SQLException e) {
			System.out.print("SQLError Appef400DAO.cargarTmpAppef401: "+ e.getMessage() );
		} catch (Exception e) {
			System.out.print("Error Appef400DAO.cargarTmpAppef401: "+ e.getMessage() );
		}
		
	}
	
	public static  boolean listaContratroAs(HashMap<Integer, Appef400> lisConTrasBan, HashMap<Integer, Appef400> listContratoAs){
		//String sql = "select * from REMUNERABT.APPEF05 where rutide in( ";
		String sql = "select * from REMUNERABT.APPEF05 ";//46 where mespro = 4 and anopro = 2018  ";
		boolean problemas = false;
	    /*
	    for (Appef400 appef400 : lisConTrasBan.values()) {
	    	if(appef400 != null){
		    	if(!primero){
					sql =sql+" , ";
				}else{
					primero = false;
				}
				sql = sql+" ? ";
				conta++;
	    	}
	    }	
	    */
		//sql = sql+")";
		
		HashMap<Integer, Appef400> listaRepuesta = new HashMap<Integer, Appef400>();
		Appef400 cont = new Appef400();
		  ConexionAs400RRHHI1 con = new ConexionAs400RRHHI1();
		  PreparedStatement sent;	  
		  ResultSet rs;		  
		  try{ 
		       sent = con.getConnection().prepareStatement(sql);
		      /*
		       for ( Appef400 appef400 : lisConTrasBan.values()) {
		    	   if(appef400 != null){
			    	   count ++;
			    	   //System.out.print(count+" \n");
			    	   sent.setInt(count, appef400.getRutide());
		    	   }
		       }
		       */
		       rs = sent.executeQuery();		       
		       try {		    	   
		    	   int conSicl = 0;
		    	   while (rs.next()) {
		    		   conSicl++;
		    		   cont = new Appef400();
		    		   cont.setRutide(rs.getInt("RUTIDE")) ;
		    		   cont.setDigide(rs.getString("DIGIDE"));
		    		   cont.setTipcon(rs.getInt("TIPCON"));
		    		   cont.setEscfon(rs.getInt("ESCFON"));
		    		   cont.setCodpla(rs.getInt("CODPLA"));
		    		   cont.setNumniv(rs.getInt("NUMNIV"));
		    		   cont.setCodsuc(rs.getInt("CODSUC"));
		    		   cont.setCodcar(rs.getInt("CODCAR"));
		    		   cont.setSumfij(rs.getInt("SUMFIJ"));
		    		   cont.setJornad(rs.getInt("JORNAD"));
		    		   cont.setCanhor(rs.getDouble("CANHOR"));
		    		   cont.setFecini(rs.getInt("FECINI"));
		    		   cont.setFecfin(rs.getInt("FECFIN"));
		    		   cont.setFecant(rs.getInt("FECANT"));
		    		   cont.setFecter(rs.getInt("FECTER"));
		    		   cont.setIndcol(rs.getInt("INDCOL"));
		    		   cont.setIndmov(rs.getInt("INDMOV"));
		    		   cont.setCodafp(rs.getInt("CODAFP"));
		    		   cont.setIndjub(rs.getInt("INDJUB"));
		    		   cont.setIndsal(rs.getInt("INDSAL"));
		    		   cont.setValaho(rs.getInt("VALAHO"));
		    		   cont.setUniaho(rs.getString("UNIAHO"));
		    		   cont.setCodisa(rs.getInt("CODISA"));
		    		   cont.setUniisa(rs.getString("UNIISA"));
		    		   cont.setValisa(rs.getDouble("VALISA"));
		    		   cont.setAntqui(rs.getDouble("ANTQUI"));
		    		   cont.setInddep(rs.getInt("INDDEP"));
		    		   cont.setSermed(rs.getInt("SERMED"));
		    		   cont.setCuomor(rs.getInt("CUOMOR"));
		    		   cont.setSindi1(rs.getInt("SINDI1"));
		    		   cont.setSindi2(rs.getInt("SINDI2"));
		    		   cont.setSindi3(rs.getInt("SINDI3"));
		    		   cont.setTipjer(rs.getString("TIPJER"));
		    		   cont.setHorini(rs.getInt("HORINI"));
		    		   cont.setHorfin(rs.getInt("HORFIN"));
		    		   cont.setFecing(rs.getInt("FECING"));
		    		   cont.setSerpr1(rs.getString("SERPR1"));
		    		   cont.setSerpr2(rs.getString("SERPR2"));
		    		   cont.setSegces(rs.getInt("SEGCES"));
		    		   cont.setIndbon(rs.getInt("INDBON"));
		    		   cont.setCodban(rs.getInt("CODBAN"));
		    		   cont.setSucban(rs.getInt("SUCBAN"));
		    		   cont.setCueban(rs.getString("CUEBAN"));
		    		   cont.setMRetencion(rs.getInt("MRETENCION"));
		    		   cont.setFeccon(rs.getInt("FECCON"));
		    		   cont.setFecpas(rs.getInt("FECPAS"));
		    		   cont.setUniaug(rs.getString("UNIAUG"));
		    		   cont.setValaug(rs.getDouble("VALAUG"));
		    		   cont.setFecint(rs.getInt("FECINT"));
		    		   cont.setCodorg(rs.getString("CODORG"));
		    		   cont.setTippue(rs.getString("TIPPUE"));
		    		   cont.setCodpue(rs.getString("CODPUE"));
		    		   cont.setBaner1(rs.getString("BANER1"));
		    		   cont.setBaner1(rs.getString("BANER2"));
		    		   cont.setBaner1(rs.getString("BANER3"));		   
		    		   
		    		   
		    		   listContratoAs.put(rs.getInt("RUTIDE"), cont);				
		    	   }		    	   
			       rs.close();
		       } catch (SQLException e){
		    	   System.out.println("ErrorSQL Appef400DAO.listaContratroAs: " + e);
			       System.out.println("cursor" );
			       problemas = true;
			   }
		       rs.close();
		       sent.close(); 
		       con.getConnection().close();       
		   }
		   catch (SQLException e){
		       System.out.println("ErrorSQL Appef400DAO.listaContratroAs: " + e);
		       System.out.println("antes del cursor " );
		       problemas = true;
		   }catch(Exception e ){
			   System.out.println("Error Appef400DAO.listaContratroAs: " + e);
			   problemas = true;
		   }
		   listContratoAs = listaRepuesta;
		return problemas;
	}
	
	private static void listContrEnAppef400(HashMap<Integer, Appef400> Listcontrato , HashMap<Integer, Integer> listUsado){
		
		String sql = "SELECT RUTIDE FROM TRANSFERBT.APPEF400 ";
		
		
		ConexionAs400RRHHI1 con = new ConexionAs400RRHHI1();
		try {
			PreparedStatement sent = con.getConnection().prepareStatement(sql);
			try {
				
				ResultSet rs = sent.executeQuery();
				
				while (rs.next()) {
					listUsado.put(rs.getInt("RUTIDE"),rs.getInt("RUTIDE"));					
				}
				
			} catch (SQLException e) {
				System.out.println("Error Appef400DAO.listContrEnAppef400: " + e);
			}
			
			sent.close();
			con.getConnection().close();
		} catch (SQLException e) {
			System.out.println("Error Appef400DAO.listContrEnAppef400: " + e);
		}		
	}
	private static boolean insertAppef400(Appef400 conCrear,ConexionAs400RRHHI1 con
										, ArrayList<Appef400> listProble){
												   // 1      2		3		4		5		6		7		8	9	10		11		12		13		14	15		16		17		18		19	20		21		22	23		24	  25		26	27		28		29		30	31		32		33	 34		35		36		37		38	 39		40		41		42		43	44			45		46	47		48		49		50	51		52		53		54	55
		String sql = "INSERT INTO TRANSFERBT.APPEF400(RUTIDE,DIGIDE,TIPCON,ESCFON,CODPLA,NUMNIV,CODSUC,CODCAR,SUMFIJ,JORNAD,CANHOR,FECINI,FECFIN,FECANT,FECTER,INDCOL,INDMOV,CODAFP,INDJUB,INDSAL,VALAHO,UNIAHO,CODISA,UNIISA,VALISA,ANTQUI,INDDEP,SERMED,CUOMOR,SINDI1,SINDI2,SINDI3,TIPJER,HORINI,HORFIN,FECING,SERPR1,SERPR2,SEGCES,INDBON,CODBAN,SUCBAN,CUEBAN,MRETENCION,FECCON,FECPAS,UNIAUG,VALAUG,FECINT,CODORG,TIPPUE,CODPUE,BANER1,BANER2,BANER3 )" +
											  "VALUES(?     ,?     ,?     ,?     ,?     ,?     ,?	  ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?         ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?     ,?)";
		
		boolean respuesta = false;
		try {
			PreparedStatement sent  = con.getConnection().prepareStatement(sql);
			try {
				sent.setInt(1, conCrear.getRutide());
				sent.setString(2, conCrear.getDigide());
				sent.setInt(3, conCrear.getTipcon());
				sent.setInt(4, conCrear.getEscfon());
				sent.setString(5, ((conCrear.getCodpla() != null && !conCrear.getCodpla().equals("") ) ? conCrear.getCodpla() : "0"));
				sent.setInt(6, conCrear.getNumniv());
				sent.setInt(7, conCrear.getCodsuc());
				sent.setInt(8, conCrear.getCodcar());
				sent.setInt(9, conCrear.getSumfij());
				sent.setInt(10, conCrear.getJornad());
				sent.setDouble(11, conCrear.getCanhor());
				sent.setInt(12, conCrear.getFecini());
				sent.setInt(13, conCrear.getFecfin());
				sent.setInt(14, conCrear.getFecant());
				sent.setInt(15, conCrear.getFecter());
				sent.setInt(16, conCrear.getIndcol());
				sent.setInt(17, conCrear.getIndmov()); 
				sent.setInt(18, conCrear.getCodafp());
				sent.setInt(19, conCrear.getIndjub());
				sent.setInt(20, conCrear.getIndsal());
				sent.setInt(21, conCrear.getValaho());
				sent.setString(22, (conCrear.getUniaho() != null && !conCrear.getUniaho().equals("") ) ? conCrear.getUniaho().trim() : "");
				sent.setInt(23, conCrear.getCodisa());
				sent.setString(24, (conCrear.getUniisa() != null && !conCrear.getUniisa().equals("") )? conCrear.getUniisa() :"");
				sent.setDouble(25, conCrear.getValisa());  
				sent.setDouble(26, conCrear.getAntqui());
				sent.setInt(27, conCrear.getInddep());
				sent.setInt(28, conCrear.getSermed());
				sent.setInt(29, conCrear.getCuomor());
				sent.setInt(30, conCrear.getSindi1());
				sent.setInt(31, conCrear.getSindi2());
				sent.setInt(32, conCrear.getSindi3());
				sent.setString(33, (conCrear.getTipjer() != null && !conCrear.getTipjer().equals("")? conCrear.getTipjer() : ""));
				sent.setInt(34, conCrear.getHorini());
				sent.setInt(35, conCrear.getHorfin());
				sent.setInt(36, conCrear.getFecing());
				sent.setString(37, (conCrear.getSerpr1() != null && !conCrear.getSerpr1().equals("")? conCrear.getSerpr1() : "") );
				sent.setString(38, (conCrear.getSerpr2() != null && !conCrear.getSerpr2().equals("")? conCrear.getSerpr2() : "") );
				sent.setInt(39, conCrear.getSegces());
				sent.setInt(40, conCrear.getIndbon());
				sent.setInt(41, conCrear.getCodban());
				sent.setInt(42, conCrear.getSucban());
				sent.setString(43, (conCrear.getCueban() != null && !conCrear.getCueban().equals("")? conCrear.getCueban() : "")); 
				sent.setInt(44, conCrear.getMRetencion());
				sent.setInt(45, conCrear.getFeccon());
				sent.setInt(46, conCrear.getFecpas());
				sent.setString(47, (conCrear.getUniaug() != null && !conCrear.getUniaug().equals("")? conCrear.getUniaug() : "")); 
				sent.setDouble(48, conCrear.getValaug());
				sent.setInt(49, conCrear.getFecint());
				sent.setString(50, (conCrear.getCodorg() != null && !conCrear.getCodorg().equals("")? conCrear.getCodorg() : ""));
				sent.setString(51, (conCrear.getTippue() != null && !conCrear.getTippue().equals("")? conCrear.getTippue() : ""));
				sent.setString(52, (conCrear.getCodpue() != null && !conCrear.getCodpue().equals("")? conCrear.getCodpue() : ""));
				sent.setString(53, (conCrear.getBaner1() != null && !conCrear.getBaner1().equals("")? conCrear.getBaner1() : ""));
				sent.setString(54, (conCrear.getBaner2() != null && !conCrear.getBaner2().equals("")? conCrear.getBaner2() : ""));
				sent.setString(55, (conCrear.getBaner3() != null && !conCrear.getBaner3().equals("")? conCrear.getBaner3() : ""));
				respuesta = sent.execute();
			} catch (Exception e) {
				System.out.println("\n rut con problema: "+conCrear.getRutide()+"-"+ conCrear.getDigide() +" \n" + e);
				System.out.println("Error Appef400DAO.insertAppef400: " + e);
				conCrear.setError("Problema con la insert");
				listProble.add(conCrear);
			}
			sent.close();
		} catch (SQLException e) {
			System.out.println("\n rut con problema: "+conCrear.getRutide()+"-"+ conCrear.getDigide() +" \n" + e);
			System.out.println("Error Appef400DAO.insertAppef400: " + e);
			conCrear.setError("Problema con la insert");
			listProble.add(conCrear);
		}	
		return respuesta;
	}
	
	private static boolean updateAppef400(Appef400 conActu
										, ConexionAs400RRHHI1 con
										, ArrayList<Appef400> listProble){
												//		1			2			3			4			5			6			7			8			9			10			11			12			13			14			15			16			17			18			19			20			21			22			23			24			25			26			27			28			29			30			31			32			33			34			35			36			37			38			39			40			41			42				43			44			45			46			47			48			49			50			51			52			53
		String sql = "UPDATE TRANSFERBT.APPEF400 SET TIPCON = ? ,ESCFON = ? ,CODPLA = ? ,NUMNIV = ? ,CODSUC = ? ,CODCAR = ? ,SUMFIJ = ? ,JORNAD = ? ,CANHOR = ? ,FECINI = ? ,FECFIN = ? ,FECANT = ? ,FECTER = ? ,INDCOL = ? ,INDMOV = ? ,CODAFP = ? ,INDJUB = ? ,INDSAL = ? ,VALAHO = ? ,UNIAHO = ? ,CODISA = ? ,UNIISA = ? ,VALISA = ? ,ANTQUI = ? ,INDDEP = ? ,SERMED = ? ,CUOMOR = ? ,SINDI1 = ? ,SINDI2 = ? ,SINDI3 = ? ,TIPJER = ? ,HORINI = ? ,HORFIN = ? ,FECING = ? ,SERPR1 = ? ,SERPR2 = ? ,SEGCES = ? ,INDBON = ? ,CODBAN = ? ,SUCBAN = ? ,CUEBAN = ? ,MRETENCION = ? ,FECCON = ? ,FECPAS = ? ,UNIAUG = ? ,VALAUG = ? ,FECINT = ? ,CODORG = ? ,TIPPUE = ? ,CODPUE = ? ,BANER1 = ? ,BANER2 = ? ,BANER3  = ? " +
								"where RUTIDE = ? ";
										// 54
		
		boolean respuesta = false; 
		try {
			PreparedStatement sent = con.getConnection().prepareStatement(sql);			
			try {
				sent.setInt(1, conActu.getTipcon());
				sent.setInt(2, conActu.getEscfon());
				sent.setString(3, ((conActu.getCodpla() != null && !conActu.getCodpla().equals("") ) ? conActu.getCodpla() : "0" ));
				sent.setInt(4, conActu.getNumniv());
				sent.setInt(5, conActu.getCodsuc());
				sent.setInt(6, conActu.getCodcar());
				sent.setInt(7, conActu.getSumfij());
				sent.setInt(8, conActu.getJornad());
				sent.setDouble(9, conActu.getCanhor());
				sent.setInt(10, conActu.getFecini());
				sent.setInt(11, conActu.getFecfin());
				sent.setInt(12, conActu.getFecant());
				sent.setInt(13, conActu.getFecter());
				sent.setInt(14, conActu.getIndcol());
				sent.setInt(15, conActu.getIndmov());
				sent.setInt(16, conActu.getCodafp());
				sent.setInt(17, conActu.getIndjub());
				sent.setInt(18, conActu.getIndsal());
				sent.setInt(19, conActu.getValaho());
				sent.setString(20, (conActu.getUniaho() != null && !conActu.getUniaho().equals("") )? conActu.getUniaho().trim() : ""  );				
				sent.setInt(21, conActu.getCodisa());
				sent.setString(22, (conActu.getUniisa() != null && !conActu.getUniisa().equals("") )? conActu.getUniisa() : "");
				sent.setDouble(23, conActu.getValisa() );
				sent.setDouble(24, conActu.getAntqui());
				sent.setInt(25, conActu.getInddep());
				sent.setInt(26, conActu.getSermed());
				sent.setInt(27, conActu.getCuomor());
				sent.setInt(28, conActu.getSindi1());
				sent.setInt(29, conActu.getSindi2());
				sent.setInt(30, conActu.getSindi3());
				sent.setString(31, (conActu.getTipjer() != null && !conActu.getTipjer().equals("")? conActu.getTipjer() : ""  ));
				sent.setInt(32, conActu.getHorini());
				sent.setInt(33, conActu.getHorfin());
				sent.setInt(34, conActu.getFecing());
				sent.setString(35, (conActu.getSerpr1() != null && !conActu.getSerpr1().equals("")? conActu.getSerpr1() : ""  ) );
				sent.setString(36, (conActu.getSerpr2() != null && !conActu.getSerpr2().equals("")? conActu.getSerpr2() : ""  ) );
				sent.setInt(37, conActu.getSegces());
				sent.setInt(38, conActu.getIndbon());
				sent.setInt(39, conActu.getCodban());
				sent.setInt(40, conActu.getSucban());
				sent.setString(41, (conActu.getCueban() != null && !conActu.getCueban().equals("")? conActu.getCueban() : ""  )); 
				sent.setInt(42, conActu.getMRetencion());
				sent.setInt(43, conActu.getFeccon());
				sent.setInt(44, conActu.getFecpas());
				sent.setString(45, (conActu.getUniaug() != null && !conActu.getUniaug().equals("")? conActu.getUniaug() : ""  )); 
				sent.setDouble(46, conActu.getValaug());
				sent.setInt(47, conActu.getFecint());
				sent.setString(48, (conActu.getCodorg() != null && !conActu.getCodorg().equals("")? conActu.getCodorg() : ""  ));
				sent.setString(49, (conActu.getTippue() != null && !conActu.getTippue().equals("")? conActu.getTippue() : ""  ));
				sent.setString(50, (conActu.getCodpue() != null && !conActu.getCodpue().equals("")? conActu.getCodpue() : ""  ));
				sent.setString(51, (conActu.getBaner1() != null && !conActu.getBaner1().equals("")? conActu.getBaner1() : ""  ));
				sent.setString(52, (conActu.getBaner2() != null && !conActu.getBaner2().equals("")? conActu.getBaner2() : ""  ));
				sent.setString(53, (conActu.getBaner3() != null && !conActu.getBaner3().equals("")? conActu.getBaner3() : ""  ));
				sent.setInt(54, conActu.getRutide());				
				respuesta = sent.execute();
				sent.close();
			} catch (SQLException e) {
				System.out.println("Error Appef400DAO.updateAppef400: " + e);
				listProble.add(conActu);
			}			
			
		} catch (Exception e) {
			System.out.println("Error Appef400DAO.updateAppef400: " + e);
			conActu.setError("Problema con la actualizacion");
			listProble.add(conActu);
		}		
		return respuesta;
	}
	
	public static void guardarHashMapContratos(HashMap<Integer, Appef400> Listcontrato 
											  ,ArrayList<Appef400> listProble){
		 
		HashMap<Integer, Integer> ListEnAppef400 = new HashMap<Integer, Integer>();
		listContrEnAppef400(Listcontrato, ListEnAppef400);
		Integer esta = 0;
		ConexionAs400RRHHI1 con = new ConexionAs400RRHHI1();
		for (Appef400 appef400 : Listcontrato.values()) {
			//seguimiento = listContrato.get(4559674);
			
			esta = ListEnAppef400.get(appef400.getRutide());
			if(esta== null || esta == 0 ){
				insertAppef400(appef400,con,listProble);
			}else{
				updateAppef400(appef400,con,listProble);
			}			
		}
		try {
			con.getConnection().close();
		} catch (SQLException e) {
			System.out.print("SQLError Appef400DAO.guardarHashMapContratos "+e.getMessage());
		} catch (Exception e) {
			System.out.print("Error Appef400DAO.guardarHashMapContratos "+e.getMessage());
		}		
	}
}
