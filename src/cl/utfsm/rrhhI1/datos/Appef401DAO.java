package cl.utfsm.rrhhI1.datos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import cl.utfsm.conexion.ConexionAs400RRHHI1;

import cl.utfsm.rrhhI1.modulo.Appef401;

public class Appef401DAO {

	 
	
	
	/**
	 * Fecha de creacion 13-08-2018
	 * Creado por: Yonis Vergara
	 * Nombre metodo
	 * 	alumnosAfuncionles
	 * Descripcion:
	 * 	En este m�todo se busca que contratos nuevos son de alumnos registrados 
	 *   esto ocasiona que los alumnos ya que a estos no se pasaron sus datos 
	 *   personales
	 * 
	 * @return
	 * 	retorna los rut de los contratos nuevos donde no se pasaron sus datos personales 
	 */
	
	
	
	public static boolean lisAppef01AS( HashMap<Integer, Appef401> lisPerBanner,HashMap<Integer, Appef401> listPer){
		
		String sql = "select * from REMUNERABT.APPEF01  ";
	   
	    boolean problemas = false;
	    /*if(lisPerBanner.size() > 0){  
	    	sql += " where rutide in ( ";
	    	for (int i = 0; i < lisPerBanner.values().size(); i++) {
	    		if(!primero){
					sql +=" , ";
				}else{
					primero = false;
				}
				sql +=" ? ";
			}		    
	    sql +=")";
	    }*/
		
		
		Appef401 per = new Appef401();
		  ConexionAs400RRHHI1 con = new ConexionAs400RRHHI1();
		  PreparedStatement sent;	  
		  ResultSet rs;		  
		  try{ 
		       sent = con.getConnection().prepareStatement(sql);
		       
		       /*for ( Appef401 appef401 : lisPerBanner.values()) {
		    	   
		    	   sent.setInt(count, appef401.getRutide()); 		    	   
		    	   count ++;
		       }		*/       
		       rs = sent.executeQuery();		       
		       try {		    	   
		    	   
		    	   while (rs.next()) {
		    		   per = new Appef401();
		    		   per.setRutide(rs.getInt("RUTIDE")) ;
		    		   per.setDigide(rs.getString("DIGIDE"));
		    		   per.setApepat(rs.getString("APEPAT"));
		    		   per.setApemat(rs.getString("APEMAT"));
		    		   per.setNombre(rs.getString("NOMBRE"));
		    		   per.setEstciv(rs.getInt("ESTCIV"));
		    		   per.setSexo(rs.getString("SEXO"));
		    		   per.setFecnac(rs.getInt("FECNAC"));
		    		   per.setLugnac(rs.getString("LUGNAC"));
		    		   per.setPainac(rs.getString("PAINAC"));
		    		   per.setNacion(rs.getInt("NACION"));
		    		   per.setDetnac(rs.getString("DETNAC"));
		    		   per.setDomici(rs.getString("DOMICI"));
		    		   per.setComuna(rs.getInt("COMUNA"));
		    		   per.setCiudad(rs.getInt("CIUDAD"));
		    		   per.setRegion(rs.getInt("REGION"));
		    		   per.setCodgra(rs.getInt("CODGRA"));
		    		   per.setGratit(rs.getInt("GRATIT"));
		    		   per.setCodtit(rs.getInt("CODTIT"));
		    		   per.setFecing(rs.getInt("FECING"));
		    		   per.setTelefo(rs.getString ("TELEFO"));
		    		   per.setCelula(rs.getString ("CELULA"));
		    		   per.setCorele(rs.getString ("CORELE"));
		    		   per.setDomele(rs.getString ("DOMELE"));
		    		   listPer.put(rs.getInt("RUTIDE"), per);				
		    	   }
		    	   
			       rs.close();
		       } catch (SQLException e){
		    	   System.out.println("SQLError Appef401DAO.lisAppef01AS: " + e);
			       System.out.println("cursor" );
			       problemas = true;
			   }		       
		       sent.close(); 
		       con.getConnection().close();	       
		   }
		   catch (SQLException e){
		       System.out.println("SQLError Appef401DAO.lisAppef01AS: " + e);
		       System.out.println("antes del cursor " );	
		       problemas = true;
		   }catch(Exception e){
			   System.out.println("Error Appef401DAO.lisAppef01AS: " + e);
		       System.out.println("antes del cursor " );	
		       problemas = true;
		   }
		
		
		   
		return problemas;		
	}
	
	public static boolean insertAppef401(Appef401 actual ,ConexionAs400RRHHI1 con){
		
		String sql = "INSERT INTO TRANsFERBT.APPEF401(RUTIDE,DIGIDE,APEPAT,APEMAT,NOMBRE,ESTCIV,SEXO,FECNAC,LUGNAC,PAINAC,NACION,DETNAC,DOMICI,COMUNA,CIUDAD,REGION,CODGRA,GRATIT,CODTIT,FECING,TELEFO,CELULA,CORELE,DOMELE) " +
					 "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		
		boolean respuesta = false ;
		try {
			try {
			PreparedStatement sent =  con.getConnection().prepareStatement(sql);
				try {				
				
				sent.setInt(1 , actual.getRutide());
				sent.setString(2 , actual.getDigide());				
				sent.setString(3 , (actual.getApepat()!= null && !actual.getApepat().trim().equals("")) ? actual.getApepat() : " " );
				sent.setString(4 , (actual.getApemat()!= null && !actual.getApemat().trim().equals("")) ? actual.getApemat() : " " );
				sent.setString(5 , (actual.getNombre()!= null && !actual.getNombre().trim().equals("")) ? actual.getNombre() : " ");
				sent.setInt(6 , actual.getEstciv());
				sent.setString(7 , actual.getSexo());
				sent.setInt(8 , actual.getFecnac());
				sent.setString(9 , actual.getLugnac());
				sent.setString(10, actual.getPainac());
				sent.setInt(11, actual.getNacion());
				sent.setString(12, actual.getDetnac());
				sent.setString(13, actual.getDomici());
				sent.setInt(14, actual.getComuna());
				sent.setInt(15, actual.getCiudad());
				sent.setInt(16, actual.getRegion());
				sent.setInt(17, actual.getCodgra());
				sent.setInt(18, actual.getGratit());
				sent.setInt(19, actual.getCodtit());
				sent.setInt(20, actual.getFecing());
				sent.setString(21, actual.getTelefo());
				sent.setString(22, actual.getCelula());
				sent.setString(23, (actual.getCorele()!= null && !actual.getCorele().trim().equals(""))? actual.getCorele() : " " );
				sent.setString(24,  (actual.getDomele()!= null && !actual.getDomele().trim().equals(""))? actual.getDomele() : " " );
				
				respuesta = sent.execute();
				
				} catch (SQLException e) {
					System.out.println("\n rut con problema: "+actual.getRutide()+"-"+ actual.getDigide() +" \n" + e);
					System.out.println("ErrorSQL Appef401DAO.insertAppef401: " + e);	
				}
			
			sent.close();
			} catch (SQLException e) {
				System.out.println("\n rut con problema: "+actual.getRutide()+"-"+ actual.getDigide() +" \n" + e);
				System.out.println("ErrorSQL Appef401DAO.insertAppef401: " + e);	
			}			
		} catch (Exception e) {
			System.out.println("Error Appef401DAO.insertAppef401: " + e);	
		}		
		return respuesta;
	}
	
	public static boolean updateAppef401(Appef401 actual , ConexionAs400RRHHI1 con){
		//                                              
		String sql = "UPDATE TRANsFERBT.APPEF401 set APEPAT = ? " +
												   ",APEMAT = ? " +
												   ",NOMBRE = ? " +
												   ",ESTCIV = ? " +
												   ",SEXO   = ? " +
												   ",FECNAC = ? " +
												   ",LUGNAC = ? " +
												   ",PAINAC = ? " +
												   ",NACION = ? " +
												   ",DETNAC = ? " +
												   ",DOMICI = ? " +
												   ",COMUNA = ? " +
												   ",CIUDAD = ? " +
												   ",REGION = ? " +
												   ",CODGRA = ? " +
												   ",GRATIT = ? " +
												   ",CODTIT = ? " +
												   ",FECING = ? " +
												   ",TELEFO = ? " +
												   ",CELULA = ? " +
												   ",CORELE = ? " +
												   ",DOMELE = ? " +
												   "where RUTIDE = ? ";
		
		
		boolean respuesta = false;
		
		
			try {
				PreparedStatement sent = con.getConnection().prepareStatement(sql);
				try {
					sent.setString(1 , (actual.getApepat()!= null && !actual.getApepat().trim().equals("")) ? actual.getApepat().trim() : " " );
					sent.setString(2 , (actual.getApemat()!= null && !actual.getApemat().trim().equals("")) ? actual.getApemat().trim() : " " );
					sent.setString(3 , (actual.getNombre()!= null && !actual.getNombre().trim().equals("")) ? actual.getNombre().trim() : " ");
					sent.setInt(4, actual.getEstciv());    //ESTCIV
					sent.setString(5, actual.getSexo());   //SEXO
					sent.setInt(6, actual.getFecnac());    //FECNAC
					sent.setString(7, actual.getLugnac()); //LUGNAC
					sent.setString(8, actual.getPainac()); //PAINAC
					sent.setInt(9, actual.getNacion());    //NACION
					sent.setString(10, actual.getDetnac());//DETNAC
					sent.setString(11, actual.getDomici());//DOMICI   
					sent.setInt(12, actual.getComuna());   //COMUNA
					sent.setInt(13, actual.getCiudad());   //CIUDAD
					sent.setInt(14, actual.getRegion());   //REGION
					sent.setInt(15, actual.getCodgra());   //CODGRA
					sent.setInt(16, actual.getGratit());   //GRATIT
					sent.setInt(17, actual.getCodtit());   //CODTIT
					sent.setInt(18, actual.getFecing());   //FECING
					sent.setString(19, actual.getTelefo());//TELEFO
					sent.setString(20, actual.getCelula());//CELULA
					sent.setString(21, (actual.getCorele()!= null && !actual.getCorele().trim().equals(""))? actual.getCorele() : " ");//CORELE
					sent.setString(22, (actual.getDomele()!= null && !actual.getDomele().trim().equals(""))? actual.getDomele() : " ");//DOMELE
					sent.setInt(23, actual.getRutide());   //RUTIDE
					respuesta = sent.execute();
					
				} catch (SQLException e) {
					System.out.println("ErrorSQL Appef401DAO.updateAppef401: "+actual.getRutide()+" " +e);
						
				} catch (Exception e){
					System.out.println("Error Appef401DAO.updateAppef401: "+actual.getRutide()+" " +e);
					
				}
				sent.close();
			} catch (SQLException e) {
				System.out.println("ErrorSQL Appef401DAO.updateAppef401: "+actual.getRutide()+" " + e);	
					
			} catch (Exception e){
				System.out.println("Error Appef401DAO.updateAppef401: "+actual.getRutide()+" " +e);	
					
			}
		
		return respuesta;
	}
	
	private static HashMap<Integer, Integer> listDeAppef401(HashMap<Integer, Appef401> listAtualPer){
		
		String sql = "SELECT RUTIDE FROM TRANsFERBT.APPEF401 ";
		
				
		HashMap<Integer, Integer> listAppef401 = new HashMap<Integer, Integer>();
		
		try {
			ConexionAs400RRHHI1 con = new ConexionAs400RRHHI1();
			
			try {
				PreparedStatement sent = con.getConnection().prepareStatement(sql);
				
				try {					
					
					ResultSet rs = sent.executeQuery();
					
					while(rs.next()){
						listAppef401.put(rs.getInt("RUTIDE"), rs.getInt("RUTIDE"));					
						
					}
					
				} catch (SQLException e) {
					System.out.print("SQLError Appef401DAO.listDeAppef401: "+ e.getMessage());
				}
				
				sent.close();
			} catch (SQLException e) {
				System.out.print("SQLError Appef401DAO.listDeAppef401: "+ e.getMessage());
			}
			
			con.getConnection().close();
		} catch (SQLException e) {
			System.out.print("SQLError Appef401DAO.listDeAppef401: "+ e.getMessage());
		} catch (Exception e) {
			System.out.print("Error Appef401DAO.listDeAppef401: "+ e.getMessage());
		}
		
		return listAppef401;
	}
	private static HashMap<Integer, Integer> listDeAppef401(ArrayList<Appef401> listAtualPer){
		
		String sql = "SELECT RUTIDE FROM TRANsFERBT.APPEF401 ";
		
				
		HashMap<Integer, Integer> listAppef401 = new HashMap<Integer, Integer>();
		
		try {
			ConexionAs400RRHHI1 con = new ConexionAs400RRHHI1();
			
			try {
				PreparedStatement sent = con.getConnection().prepareStatement(sql);
				
				try {					
					
					ResultSet rs = sent.executeQuery();
					
					while(rs.next()){
						listAppef401.put(rs.getInt("RUTIDE"), rs.getInt("RUTIDE"));					
						
					}
					
				} catch (SQLException e) {
					System.out.print("SQLError Appef401DAO.listDeAppef401: "+ e.getMessage());
				}
				
				sent.close();
			} catch (SQLException e) {
				System.out.print("SQLError Appef401DAO.listDeAppef401: "+ e.getMessage());
			}
			
			con.getConnection().close();
		} catch (SQLException e) {
			System.out.print("SQLError Appef401DAO.listDeAppef401: "+ e.getMessage());
		} catch (Exception e) {
			System.out.print("Error Appef401DAO.listDeAppef401: "+ e.getMessage());
		}
		
		return listAppef401;
	}
	
	public static boolean guardarHastMap(HashMap<Integer, Appef401> listDatoPer){
		 
		HashMap<Integer, Integer> listPersExistentes = listDeAppef401(listDatoPer);
		Integer esta ;
		ConexionAs400RRHHI1 con = new ConexionAs400RRHHI1();		
		for (Appef401 actualizar : listDatoPer.values()) {
			esta = listPersExistentes.get(actualizar.getRutide());
			if (esta == null || esta == 0) {
				insertAppef401(actualizar,con);
			}else{
				updateAppef401(actualizar,con);
			}			
		}
		try {
			con.getConnection().close();
		}catch (SQLException e) {
			System.out.print("SQLError Appef401DAO.guardarHastMap "+e.getMessage());
		} catch (Exception e) {
			System.out.print("Error Appef401DAO.guardarHastMap "+e.getMessage());
		}		
		return false;
	} 	
	public static boolean guardarHastMap(ArrayList<Appef401> listDatoPer){
		 
		HashMap<Integer, Integer> listPersExistentes = listDeAppef401(listDatoPer);
		Integer esta ;
		ConexionAs400RRHHI1 con = new ConexionAs400RRHHI1();		
		for (Appef401 actualizar : listDatoPer) {
			esta = listPersExistentes.get(actualizar.getRutide());
			if (esta == null || esta == 0) {
				insertAppef401(actualizar,con);
			}else{
				updateAppef401(actualizar,con);
			}			
		}
		try {
			con.getConnection().close();
		}catch (SQLException e) {
			System.out.print("SQLError Appef401DAO.guardarHastMap "+e.getMessage());
		} catch (Exception e) {
			System.out.print("Error Appef401DAO.guardarHastMap "+e.getMessage());
		}		
		return false;
	} 
}