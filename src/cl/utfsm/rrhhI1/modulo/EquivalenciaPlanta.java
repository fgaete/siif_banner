package cl.utfsm.rrhhI1.modulo;

public class EquivalenciaPlanta {
	
	private String codplaBanner;
	private int codesc;
	private int CodplaAS;
	
	public EquivalenciaPlanta () {
		
	}

	public String getCodplaBanner() {
		return codplaBanner;
	}

	public void setCodplaBanner(String codplaBanner) {
		this.codplaBanner = codplaBanner;
	}

	public int getCodesc() {
		return codesc;
	}

	public void setCodesc(int codesc) {
		this.codesc = codesc;
	}

	public int getCodplaAS() {
		return CodplaAS;
	}

	public void setCodplaAS(int codplaAS) {
		CodplaAS = codplaAS;
	}
	
	
}
