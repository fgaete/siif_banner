package cl.utfsm.rrhhI1.modulo;

public class Appef400 { 
	
	private int    rutide;
	private String digide;
	private int    tipcon;
	private int    escfon;
	private String codpla;
	private int    numniv;
	private int    codsuc;
	private int    codcar;
	private int    sumfij;
	private int    jornad;
	private double canhor;
	private int    fecini;
	private int    fecfin;
	private int    fecant;
	private int    fecter;
	private int    indcol;
	private int    indmov;
	private int    codafp;
	private int    indjub;
	private int    indsal;
	private int    valaho;
	private String uniaho;
	private int    codisa;
	private String uniisa;
	private double valisa;
	private double antqui;
	private int    inddep;
	private int    sermed;
	private int    cuomor;
	private int    sindi1;
	private int    sindi2;
	private int    sindi3;
	private String tipjer;
	private int    horini;
	private int    horfin;
	private int    fecing;
	private String serpr1;
	private String serpr2;
	private int    segces;
	private int    indbon;
	private int    codban;
	private int    sucban;
	private String cueban;
	private int    mRetencion;
	private int    feccon;
	private int    fecpas;
	private String uniaug;
	private double valaug;
	private int    fecint;
	private String codorg;
	private String tippue;
	private String codpue;
	private String baner1;
	private String baner2;
	private String baner3;
	private int    fecpro;
	private String propor;
	private int pidm;
	private String error;
	
	public Appef400(){
		
	}

	
	public String getError() {
		return error;
	}


	public void setError(String error) {
		this.error = error;
	}


	public int getPidm() {
		return pidm;
	}


	public void setPidm(int pidm) {
		this.pidm = pidm;
	}
 

	public int getRutide() {
		return rutide;
	}

	public void setRutide(int rutide) {
		this.rutide = rutide;
	}

	public String getDigide() {
		return digide;
	}

	public void setDigide(String digide) {
		this.digide = digide;
	}

	public int getTipcon() {
		return tipcon;
	}

	public void setTipcon(int tipcon) {
		this.tipcon = tipcon;
	}

	public int getEscfon() {
		return escfon;
	}

	public void setEscfon(int escfon) {
		this.escfon = escfon;
	}

	public String getCodpla() {
		return codpla;
	}
	
	public void setCodpla(String codpla) {
		this.codpla = codpla;
	}
	public void setCodpla(int codpla) {
		this.codpla = ""+codpla;
	}

	public int getNumniv() {
		return numniv;
	}

	public void setNumniv(int numniv) {
		this.numniv = numniv;
	}

	public int getCodsuc() {
		return codsuc;
	}

	public void setCodsuc(int codsuc) {
		this.codsuc = codsuc;
	}

	public int getCodcar() {
		return codcar;
	}

	public void setCodcar(int codcar) {
		this.codcar = codcar;
	}

	public int getSumfij() {
		return sumfij;
	}

	public void setSumfij(int sumfij) {
		this.sumfij = sumfij;
	}

	public int getJornad() {
		return jornad;
	}

	public void setJornad(int jornad) {
		this.jornad = jornad;
	}

	public double getCanhor() {
		return canhor;
	}

	public void setCanhor(double canhor) {
		this.canhor = canhor;
	}

	public int getFecini() {
		return fecini;
	}

	public void setFecini(int fecini) {
		this.fecini = fecini;
	}

	public int getFecfin() {
		return fecfin;
	}

	public void setFecfin(int fecfin) {
		this.fecfin = fecfin;
	}

	public int getFecant() {
		return fecant;
	}

	public void setFecant(int fecant) {
		this.fecant = fecant;
	}

	public int getFecter() {
		return fecter;
	}

	public void setFecter(int fecter) {
		this.fecter = fecter;
	}

	public int getIndcol() {
		return indcol;
	}

	public void setIndcol(int indcol) {
		this.indcol = indcol;
	}

	public int getIndmov() {
		return indmov;
	}

	public void setIndmov(int indmov) {
		this.indmov = indmov;
	}

	public int getCodafp() {
		return codafp;
	}

	public void setCodafp(int codafp) {
		this.codafp = codafp;
	}

	public int getIndjub() {
		return indjub;
	}

	public void setIndjub(int indjub) {
		this.indjub = indjub;
	}

	public int getIndsal() {
		return indsal;
	}

	public void setIndsal(int indsal) {
		this.indsal = indsal;
	}

	public int getValaho() {
		return valaho;
	}

	public void setValaho(int valaho) {
		this.valaho = valaho;
	}

	public String getUniaho() {
		return uniaho;
	}

	public void setUniaho(String uniaho) {
		this.uniaho = uniaho;
	}

	public int getCodisa() {
		return codisa;
	}

	public void setCodisa(int codisa) {
		this.codisa = codisa;
	}

	public String getUniisa() {
		return uniisa;
	}

	public void setUniisa(String uniisa) {
		this.uniisa = uniisa;
	}

	public double getValisa() {
		return valisa;
	}

	public void setValisa(double valisa) {
		this.valisa = valisa;
	}

	public double getAntqui() {
		return antqui;
	}

	public void setAntqui(double antqui) {
		this.antqui = antqui;
	}

	public int getInddep() {
		return inddep;
	}

	public void setInddep(int inddep) {
		this.inddep = inddep;
	}

	public int getSermed() {
		return sermed;
	}

	public void setSermed(int sermed) {
		this.sermed = sermed;
	}

	public int getCuomor() {
		return cuomor;
	}

	public void setCuomor(int cuomor) {
		this.cuomor = cuomor;
	}

	public int getSindi1() {
		return sindi1;
	}

	public void setSindi1(int sindi1) {
		this.sindi1 = sindi1;
	}

	public int getSindi2() {
		return sindi2;
	}

	public void setSindi2(int sindi2) {
		this.sindi2 = sindi2;
	}

	public int getSindi3() {
		return sindi3;
	}

	public void setSindi3(int sindi3) {
		this.sindi3 = sindi3;
	}

	public String getTipjer() {
		return tipjer;
	}

	public void setTipjer(String tipjer) {
		this.tipjer = tipjer;
	}

	public int getHorini() {
		return horini;
	}

	public void setHorini(int horini) {
		this.horini = horini;
	}

	public int getHorfin() {
		return horfin;
	}

	public void setHorfin(int horfin) {
		this.horfin = horfin;
	}

	public int getFecing() {
		return fecing;
	}

	public void setFecing(int fecing) {
		this.fecing = fecing;
	}

	public String getSerpr1() {
		return serpr1;
	}

	public void setSerpr1(String serpr1) {
		this.serpr1 = serpr1;
	}

	public String getSerpr2() {
		return serpr2;
	}

	public void setSerpr2(String serpr2) {
		this.serpr2 = serpr2;
	}

	public int getSegces() {
		return segces;
	}

	public void setSegces(int segces) {
		this.segces = segces;
	}

	public int getIndbon() {
		return indbon;
	}

	public void setIndbon(int indbon) {
		this.indbon = indbon;
	}

	public int getCodban() {
		return codban;
	}

	public void setCodban(int codban) {
		this.codban = codban;
	}

	public int getSucban() {
		return sucban;
	}

	public void setSucban(int sucban) {
		this.sucban = sucban;
	}

	public String getCueban() {
		return cueban;
	}

	public void setCueban(String cueban) {
		this.cueban = cueban;
	}

	public int getMRetencion() {
		return mRetencion;
	}

	public void setMRetencion(int retencion) {
		mRetencion = retencion;
	}

	public int getFeccon() {
		return feccon;
	}

	public void setFeccon(int feccon) {
		this.feccon = feccon;
	}

	public int getFecpas() {
		return fecpas;
	}

	public void setFecpas(int fecpas) {
		this.fecpas = fecpas;
	}

	public String getUniaug() {
		if(this.valaug == 0){
			this.uniaug = "";
		}
		return uniaug;
	}

	public void setUniaug(String uniaug) {
		this.uniaug = uniaug;
	}

	public double getValaug() {
		
		return valaug;
	}

	public void setValaug(double valaug) {
		this.valaug = valaug;
	}

	public int getFecint() {
		return fecint;
	}

	public void setFecint(int fecint) {
		this.fecint = fecint;
	}

	public String getCodorg() {
		return codorg;
	}

	public void setCodorg(String codorg) {
		this.codorg = codorg;
	}

	public String getTippue() {
		return tippue;
	}

	public void setTippue(String tippue) {
		this.tippue = tippue;
	}

	public String getCodpue() {
		return codpue;
	}

	public void setCodpue(String codpue) {
		this.codpue = codpue;
	}

	public String getBaner1() {
		return baner1;
	}

	public void setBaner1(String baner1) {
		this.baner1 = baner1;
	}

	public String getBaner2() {
		return baner2;
	}

	public void setBaner2(String baner2) {
		this.baner2 = baner2;
	}

	public String getBaner3() {
		return baner3;
	}

	public void setBaner3(String baner3) {
		this.baner3 = baner3;
	}

	public int getFecpro() {
		return fecpro;
	}

	public void setFecpro(int fecpro) {
		this.fecpro = fecpro;
	}

	public String getPropor() {
		return propor;
	}

	public void setPropor(String propor) {
		this.propor = propor;
	}
	
	public void actulizarDatosExistentes(Appef400 conAs){
		
		if(this.codorg == null || this.codorg.trim().equals("") ){			
			this.tipcon = (this.tipcon == 0 ) ? conAs.getTipcon() : this.tipcon ;
			this.escfon = (this.escfon == 0 ) ? conAs.getEscfon() : this.escfon ;
			this.codpla = (this.codpla == null || this.codpla.equals("")) ? conAs.getCodpla() : this.codpla;
			this.numniv = (this.numniv == 0) ? conAs.getNumniv() : this.numniv ;
			this.codsuc = (this.codsuc == 0) ? conAs.getCodsuc() : this.codsuc ;
			this.codcar = (this.codcar == 0) ? conAs.getCodcar() : this.codcar ;
			this.sumfij = (this.sumfij == 0) ? conAs.getSumfij() : this.sumfij ;
			this.jornad = (this.jornad == 0) ? conAs.getJornad() : this.jornad ;
			this.canhor = (this.canhor == 0) ? conAs.getCanhor() : this.canhor ;
			this.fecant = (this.fecant == 0) ? conAs.getFecant() : this.fecant ; 
			this.fecter = (this.fecter == 0) ? conAs.getFecter() : this.fecter ;
			this.codafp = (this.codafp == 0) ? conAs.getCodafp() : this.codafp ;
			this.indjub = (this.indjub == 0) ? conAs.getIndjub() : this.indjub ;
			this.codisa = (this.codisa == 0) ? conAs.getCodisa() : this.codisa ;			
			this.tipjer = (this.tipjer == null || this.tipjer.equals("")) ? conAs.getTipjer() : this.tipjer ;
			this.fecing = (this.fecing == 0) ? conAs.getFecing() : this.fecing ;
			this.serpr1 = (this.serpr1 == null || this.serpr1.equals("")) ? conAs.getSerpr1() : this.serpr1 ;
			this.serpr2 = (this.serpr2 == null || this.serpr2.equals("")) ? conAs.getSerpr2() : this.serpr2 ;
			this.feccon = (this.feccon == 0) ? conAs.getFeccon() : this.feccon ;
			this.valaug = (this.valaug == 0) ? conAs.getValaug() : this.valaug ;
			this.uniaug = (this.uniaug == null || this.uniaug.equals("")) ? conAs.getUniaug() : this.uniaug ;
			this.codorg = (this.codorg == null || this.codorg.equals("")) ? conAs.getCodorg() : this.codorg ;
			this.codpue = (this.codpue == null || this.codpue.equals("")) ? conAs.getCodpue() : this.codpue ;
			this.cueban = (this.cueban == null || this.cueban.equals("")) ? conAs.getCueban() : this.cueban ;
			this.codban = (this.codban == 0) ? conAs.getCodban() : this.codban ;
			this.uniisa = (this.uniisa == null || this.uniisa.equals("")) ? conAs.getUniisa() : this.uniisa ;
			this.valisa = (this.valisa == 0) ? conAs.getValisa() : this.valisa ;
			this.segces = (this.segces == 0) ? conAs.getSegces() : this.segces ;			
			this.tippue = (this.tippue == null || this.tippue.equals("")) ? conAs.getTippue() : this.tippue ;
			this.fecini = (this.fecini == 0) ? conAs.getFecini() : this.fecini ;
			this.fecfin = (this.fecfin == 0) ? conAs.getFecfin() : this.fecfin ;
			this.fecpas = (this.fecpas == 0) ? conAs.getFecpas() : this.fecpas ;
			this.indcol = (this.indcol == 9) ? conAs.getIndcol() : this.indcol ;
			this.indmov = (this.indmov == 9) ? conAs.getIndmov() : this.indmov ;
			this.indsal = (this.indsal == 0) ? conAs.getIndsal() : this.indsal ;
			this.inddep = (this.inddep == 9) ? conAs.getInddep() : this.inddep ;
			this.sermed = (this.sermed == 9) ? conAs.getSermed() : this.sermed ;
			this.cuomor = (this.cuomor == 9) ? conAs.getCuomor() : this.cuomor ;
		}	
		this.sindi1 = (this.sindi1 == 0) ? conAs.getSindi1() : this.sindi1 ;		
		this.valaho = (this.valaho == 0) ? conAs.getValaho() : this.valaho ;		
		this.uniaho = (this.uniaho == null || this.uniaho.trim().equals("")) ? conAs.getUniaho() : this.uniaho ;		
		this.antqui = (this.antqui == 0) ? conAs.getAntqui() : this.antqui ;		
		this.sindi2 = (this.sindi2 == 0) ? conAs.getSindi2() : this.sindi2 ;
		this.sindi3 = (this.sindi3 == 0) ? conAs.getSindi3() : this.sindi3 ;
		this.horini = (this.horini == 0) ? conAs.getHorini() : this.horini ;
		this.horfin = (this.horfin == 0) ? conAs.getHorfin() : this.horfin ;		
		this.indbon = (this.indbon == 0) ? conAs.getIndbon() : this.indbon ;
		if(this.codban > 0){ 			
			this.sucban = (this.sucban == 0) ? conAs.getSucban() : this.sucban;			
		}
		this.mRetencion = (this.mRetencion == 0) ? conAs.getMRetencion() : this.mRetencion ;				
		this.fecint = (this.fecint == 0) ? conAs.getFecint() : this.fecint ;		
		this.baner1 = (this.baner1 == null || this.baner1.equals("")) ? conAs.getBaner1() : this.baner1 ;
		this.baner2 = (this.baner2 == null || this.baner2.equals("")) ? conAs.getBaner2() : this.baner2 ;
		this.baner3 = (this.baner3 == null || this.baner3.equals("")) ? conAs.getBaner3() : this.baner3 ;
		this.fecpro = (this.fecpro == 0) ? conAs.getFecpro() : this.fecpro ;
		this.propor = (this.propor == null || this.propor.equals("")) ? conAs.getPropor() : this.propor ;
	}
	public String getRutFormato(){
		
		int largoOriginal = (this.rutide+this.digide).length();
		int ultimaPosision  = largoOriginal;
		String rutFormato =  (this.rutide+this.digide).substring(0,largoOriginal-1)+"-"+(this.rutide+this.digide).substring(largoOriginal-1);
		ultimaPosision --;
		
		while(ultimaPosision > 3){
			
			rutFormato = rutFormato.substring(0,ultimaPosision-3)+"."+rutFormato.substring(ultimaPosision-3);
			ultimaPosision -= 3;			
		}		
		return rutFormato;
	}
	
}
