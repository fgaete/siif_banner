package cl.utfsm.rrhhI1.modulo;

public class EquivalenciaNivel {
	private int nivel;
	private int codesc;
	private int paso;
	private int valor;
	
	public EquivalenciaNivel() {
		
	}
	
	
	
	public int getValor() {
		return valor;
	}



	public void setValor(int valor) {
		this.valor = valor;
	}



	public int getNivel() {
		return nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}

	public int getCodesc() {
		return codesc;
	}

	public void setCodesc(int codesc) {
		this.codesc = codesc;
	}

	public int getPaso() {
		return paso;
	}

	public void setPaso(int paso) {
		this.paso = paso;
	}
	
	
}
