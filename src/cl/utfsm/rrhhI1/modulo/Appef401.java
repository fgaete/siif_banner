package cl.utfsm.rrhhI1.modulo;

public class Appef401 {

		private int    rutide;
		private String digide;
		private String apepat;
		private String apemat;
		private String nombre;
		private int    estciv;
		private String sexo;
		private int    fecnac;
		private String lugnac;
		private String painac;
		private int    nacion;
		private String detnac;
		private String domici;
		private int    comuna;
		private int    ciudad;
		private int    region;
		private int    codgra;
		private int    gratit;
		private int    codtit;
		private int    fecing;
		private String telefo;
		private String celula;
		private String corele;
		private String domele;
		
		public Appef401(){
			
		}

		public int getRutide() {
			return rutide;
		}

		public void setRutide(int rutide) {
			this.rutide = rutide;
		}

		public String getDigide() {
			return digide;
		}

		public void setDigide(String digide) {
			this.digide = digide;
		}

		public String getApepat() {
			return apepat;
		}

		public void setApepat(String apepat) {
			this.apepat = apepat;
		}

		public String getApemat() {
			return apemat;
		}

		public void setApemat(String apemat) {
			this.apemat = apemat;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public int getEstciv() {
			return estciv;
		}

		public void setEstciv(int estciv) {
			this.estciv = estciv;
		}

		public String getSexo() {
			return sexo;
		}

		public void setSexo(String sexo) {
			this.sexo = sexo;
		}

		public int getFecnac() {
			return fecnac;
		}

		public void setFecnac(int fecnac) {
			this.fecnac = fecnac;
		}

		public String getLugnac() {
			return lugnac;
		}

		public void setLugnac(String lugnac) {
			this.lugnac = lugnac;
		}

		public String getPainac() {
			return painac;
		}

		public void setPainac(String painac) {
			this.painac = painac;
		}

		public int getNacion() {
			return nacion;
		}

		public void setNacion(int nacion) {
			this.nacion = nacion;
		}

		public String getDetnac() {
			return detnac;
		}

		public void setDetnac(String detnac) {
			this.detnac = detnac;
		}

		public String getDomici() {
			return domici;
		}

		public void setDomici(String domici) {
			this.domici = domici;
		}

		public int getComuna() {
			return comuna;
		}

		public void setComuna(int comuna) {
			this.comuna = comuna;
		}

		public int getCiudad() {
			return ciudad;
		}

		public void setCiudad(int ciudad) {
			this.ciudad = ciudad;
		}

		public int getRegion() {
			return region;
		}

		public void setRegion(int region) {
			this.region = region;
		}

		public int getCodgra() {
			return codgra;
		}

		public void setCodgra(int codgra) {
			this.codgra = codgra;
		}

		public int getGratit() {
			return gratit;
		}

		public void setGratit(int gratit) {
			this.gratit = gratit;
		}

		public int getCodtit() {
			return codtit;
		}

		public void setCodtit(int codtit) {
			this.codtit = codtit;
		}

		public int getFecing() {
			return fecing;
		}

		public void setFecing(int fecing) {
			this.fecing = fecing;
		}

		public String getTelefo() {
			return telefo;
		}
		

		public void setTelefo(int telefo) {
			this.telefo = ""+telefo;
		}
		public void setTelefo(String telefo) {
			this.telefo = telefo;
		}

		public String getCelula() {
			return celula;
		}
		

		public void setCelula(int celula) {
			this.celula = ""+celula;
		}
		public void setCelula(String celula) {
			this.celula = celula;
		}

		public String getCorele() {
			return corele;
		}

		public void setCorele(String corele) {
			this.corele = corele;
		}

		public String getDomele() {
			return domele;
		}

		public void setDomele(String domele) {
			this.domele = domele;
		}
		
		public void atualirDatos(Appef401 datPers){			
			//this.apepat = (this.apepat == null || this.apepat.equals("")) ? datPers.getApepat() : this.apepat;
			//this.apemat = (this.apemat == null || this.apemat.equals("")) ? datPers.getApemat() : this.apemat;
			//this.nombre = (this.nombre == null || this.nombre.equals("")) ? datPers.getNombre() : this.nombre;
			//this.estciv = (this.estciv == 0) ? datPers.getEstciv() : this.estciv;
			//this.sexo   = (this.sexo == null || this.equals("")) ? datPers.getSexo() : this.sexo;
			//this.fecnac = (this.fecnac == 0) ? datPers.getFecnac() : this.fecnac;
			//this.lugnac = (this.lugnac == null|| this.lugnac.equals("")) ? datPers.getLugnac() : this.lugnac;
			//this.painac = (this.painac == null || this.painac.equals("")) ? datPers.getPainac() : this.painac;
			//this.nacion = (this.nacion == 0) ? datPers.getNacion() : this.nacion;
			//this.detnac = (this.detnac == null || this.detnac.equals("")) ? datPers.getDetnac() : this.detnac;
			//this.domici = (this.domici == null || this.domici.equals("")) ? datPers.getDomici() : this.domici;
			//this.comuna = (this.comuna == 0) ? datPers.getComuna() : this.comuna;
			this.ciudad = (this.ciudad == 0) ? datPers.getCiudad() : this.ciudad;
			//this.region = (this.region == 0) ? datPers.getRegion() : this.region;
			this.codgra = (this.codgra == 0) ? datPers.getCodgra() : this.codgra;
			this.gratit = (this.gratit == 0) ? datPers.getGratit() : this.gratit;
			this.codtit = (this.codtit == 0) ? datPers.getCodtit() : this.codtit;
			//this.fecing = (this.fecing == 0) ? datPers.getFecing() : this.fecing;
			//this.telefo = (this.telefo == null || this.telefo.equals("")) ? datPers.getTelefo() : this.telefo;
			//this.celula = (this.celula == null || this.celula.equals("")) ? datPers.getCelula() : this.celula;
			//this.corele = (this.corele == null || this.corele.equals("")) ? datPers.getCorele() : this.corele;
			//this.domele = (this.domele == null || this.domele.equals("")) ? datPers.getDomele() : this.domele;		
		}		
}
