package cl.utfsm.scaB.mvc;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import org.apache.velocity.tools.generic.DateTool;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.utfsm.POJO.APPEF01;
import cl.utfsm.POJO.ASIGNATURA;
import cl.utfsm.POJO.APREF19;
import cl.utfsm.POJO.APREF24;
import cl.utfsm.POJO.Banco;
import cl.utfsm.POJO.Ciudad;
import cl.utfsm.POJO.Comuna;
import cl.utfsm.POJO.PRESF200;
import cl.utfsm.POJO.SucursalBanco;
import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.scaB.modulo.ModuloContratoAyudantiaB;
import descad.cliente.CocowBean;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw21DTO;
import descad.presupuesto.Presw25DTO;

public class ContratoAyudantiaInterceptorB extends HandlerInterceptorAdapter{
	ModuloContratoAyudantiaB moduloContratoAyudantiaB;
	
	

	public ModuloContratoAyudantiaB getModuloContratoAyudantiaB() {
		return moduloContratoAyudantiaB;
	}

	public void setModuloContratoAyudantiaB(
			ModuloContratoAyudantiaB moduloContratoAyudantiaB) {
		this.moduloContratoAyudantiaB = moduloContratoAyudantiaB;
	}

	public void cargarMenu(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		accionweb.agregarObjeto("esContratoAyudantiaB", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		
		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.getSesion().removeAttribute("autorizaProyecto");
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.getSesion().removeAttribute("autorizaUCP");
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.getSesion().removeAttribute("autorizaDIRPRE");
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.getSesion().removeAttribute("autorizaFinanzas");
	/* se debe buscar segun el usuario si tiene permiso para autorizar*/
	List<Presw18DTO> listaAutorizaciones = new ArrayList<Presw18DTO>();
	listaAutorizaciones = moduloContratoAyudantiaB.getConsultaAutoriza(rutUsuario, dv);
	if(listaAutorizaciones != null && listaAutorizaciones.size() > 0){
		 for (Presw18DTO ss : listaAutorizaciones){
			if(ss.getIndprc().trim().equals("Y")){
				accionweb.agregarObjeto("autorizaProyecto", 1);
				accionweb.getSesion().setAttribute("autorizaProyecto", 1);
			}
			if(ss.getIndprc().trim().equals("X")){
				accionweb.agregarObjeto("autorizaUCP", 1);
				accionweb.getSesion().setAttribute("autorizaUCP", 1);
			}
			if(ss.getIndprc().trim().equals("Z")){
				accionweb.agregarObjeto("autorizaDIRPRE", 1);
				accionweb.getSesion().setAttribute("autorizaDIRPRE", 1);
			}
			if(ss.getIndprc().trim().equals("F")){
				accionweb.agregarObjeto("autorizaFinanzas", 1);
				accionweb.getSesion().setAttribute("autorizaFinanzas", 1);
			}
		 }
	}
	
		
		
		if(accionweb.getSesion().getAttribute("listaSede") != null)
	       accionweb.getSesion().removeAttribute("listaSede");
	    if(accionweb.getSesion().getAttribute("listaOrg") != null)
	       accionweb.getSesion().removeAttribute("listaOrg");
	    if(accionweb.getSesion().getAttribute("listaTipoAyu") != null)
	       accionweb.getSesion().removeAttribute("listaTipoAyu");
	    if(accionweb.getSesion().getAttribute("listaProfesor") != null)
		  accionweb.getSesion().removeAttribute("listaProfesor");
	    if(accionweb.getSesion().getAttribute("listaCiudad") != null)
			  accionweb.getSesion().removeAttribute("listaCiudad");
	    if(accionweb.getSesion().getAttribute("listaComuna") != null)
			  accionweb.getSesion().removeAttribute("listaComuna");
	    if(accionweb.getSesion().getAttribute("listaBanco") != null)
			  accionweb.getSesion().removeAttribute("listaBanco");
	    if(accionweb.getSesion().getAttribute("listaSucursalBanco") != null)
			  accionweb.getSesion().removeAttribute("listaSucursalBanco");
/*	  
	    List<Presw18DTO> listaSede = new ArrayList<Presw18DTO>();
		listaSede = moduloContratoAyudantiaB.getListaSede(accionweb.getReq());
		if(listaSede != null && listaSede.size() > 0) {
	      	accionweb.getSesion().setAttribute("listaSede", listaSede);
	      	accionweb.agregarObjeto("listaSede", listaSede);
	   }  	
	    
		List<PRESF200> listaOrg = new ArrayList<PRESF200>();
		listaOrg = moduloContratoAyudantiaB.getOrganizacion();
		if(listaOrg != null && listaOrg.size() > 0)
	       accionweb.getSesion().setAttribute("listaOrg", listaOrg);
		 
		List<APREF19> listaTipoAyu = new ArrayList<APREF19>();
		listaTipoAyu = moduloContratoAyudantiaB.getTipoAyudantia();
		if(listaTipoAyu != null && listaTipoAyu.size() > 0)
		   accionweb.getSesion().setAttribute("listaTipoAyu", listaTipoAyu);
		
		List<APPEF01> listaProfesor = new ArrayList<APPEF01>();
		listaProfesor = moduloContratoAyudantiaB.getListaProfesor();
		if(listaProfesor != null && listaProfesor.size() > 0) 
		   accionweb.getSesion().setAttribute("listaProfesor", listaProfesor);
		
		List<Ciudad> listaCiudad = new ArrayList<Ciudad>();
		listaCiudad = moduloContratoAyudantiaB.getCiudades();
		if(listaCiudad != null && listaCiudad.size() > 0) 
		   accionweb.getSesion().setAttribute("listaCiudad", listaCiudad);
		
		List<Comuna> listaComuna = new ArrayList<Comuna>();
		listaComuna = moduloContratoAyudantiaB.getComunas();
		if(listaComuna != null && listaComuna.size() > 0) 
		   accionweb.getSesion().setAttribute("listaComuna", listaComuna);
		
		List<Banco> listaBanco = new ArrayList<Banco>();
		listaBanco = moduloContratoAyudantiaB.getListaBanco();
		if(listaBanco != null && listaBanco.size() > 0) 
		   accionweb.getSesion().setAttribute("listaBanco", listaBanco);
	*/ }
	public void cargarListaSemestre(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
		
		List<APREF24> lista = moduloContratoAyudantiaB.getListaSemetre(sede);
		if(lista != null)
			accionweb.getSesion().setAttribute("listaSemestre", lista);
		accionweb.agregarObjeto("listaSemestre", lista);	
		accionweb.agregarObjeto("esContratoAyudantiaB", 1); 
		
	}
	public void cargarDetallePeriodoSemestre(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int anio = Util.validaParametro(accionweb.getParameter("anio"), 0);
		int sem = Util.validaParametro(accionweb.getParameter("sem"), 0);
		String inicio = "";
		String fin = "";
		List<APREF24> lista = (List<APREF24>)accionweb.getSesion().getAttribute("listaSemestre");
		for(APREF24 ss:lista){
			if(ss.getANOAYU() == anio && ss.getSEMEST() == sem){
				if(ss.getDIAINI() < 10)
					inicio = "0" + ss.getDIAINI()+"/";
				else
					inicio = ss.getDIAINI()+"/";
				if(ss.getMESINI() < 10)
					inicio = inicio + "0" + ss.getMESINI()+"/";
				else
					inicio = inicio + ss.getMESINI()+"/";
				inicio = inicio + anio;
				if(ss.getDIAFIN() < 10)
					fin = "0" + ss.getDIAFIN()+"/";
				else
					fin = ss.getDIAFIN()+"/";
				if(ss.getMESFIN() < 10)
					fin = fin + "0" + ss.getMESFIN()+"/";
				else
					fin = fin + ss.getMESFIN()+"/";
				fin = fin + anio;
			break;
			}
			
				
		}
		accionweb.agregarObjeto("fechaPeriodoSemIni", inicio);	
		accionweb.agregarObjeto("fechaPeriodoSemFin", fin); 
	
		accionweb.agregarObjeto("listaSemestre", lista);	
		accionweb.agregarObjeto("esContratoAyudantiaB", 1); 
		
	}
	
	public void cargarDetalleAyudantia(AccionWeb accionweb) throws Exception {
		System.out.println("cargarDetalleAyudantia: ");
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
		String codayu = Util.validaParametro(accionweb.getParameter("codayu"), "");
		 if(accionweb.getSesion().getAttribute("listaSubtipoAyudantia") != null)
		    	accionweb.getSesion().removeAttribute("listaSubtipoAyudantia");
		long valMinTarifa = 0;
		long valMaxTarifa = 0;
		if (!codayu.trim().equals("")) {
			PreswBean preswbean = null;
			List<Presw21DTO> lista = null;
			preswbean = new PreswBean("SA4", 0, 0, 0, 0, 0);
			preswbean.setTipdoc(codayu);
			lista = (List<Presw21DTO>) preswbean.consulta_presw21();
			for(Presw21DTO ss:lista) {
				valMinTarifa = ss.getValor1();
				valMaxTarifa = ss.getValor2();
			}
			accionweb.agregarObjeto("valMinTarifa", valMinTarifa); 
			accionweb.agregarObjeto("valMaxTarifa", valMaxTarifa); 
			
		}
		
		List<APPEF01> listaProfesor = new ArrayList<APPEF01>();
		listaProfesor = (List<APPEF01>)accionweb.getSesion().getAttribute("listaProfesor");
		if(sede > 0){
			List<ASIGNATURA> listaAsignatura = new ArrayList<ASIGNATURA>();
			listaAsignatura = moduloContratoAyudantiaB.getListaAsignatura(sede);
			if(listaAsignatura != null && listaAsignatura.size() > 0) 
			   accionweb.agregarObjeto("listaAsignatura", listaAsignatura);
		}
		
		accionweb.agregarObjeto("listaProfesor", listaProfesor);
		accionweb.agregarObjeto("esContratoAyudantiaB", 1); 	
	/*	List<APREF24> lista = (List<APREF24>) accionweb.getSesion().getAttribute("listaSemestre");
		
		if(lista != null)
			accionweb.getSesion().removeAttribute("listaSemestre");*/
	}
	public void cargarDetalleSubTipoAyudantia(AccionWeb accionweb) throws Exception {
		System.out.println("cargarDetalleSubTipoAyudantia: ");
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		String codayu = Util.validaParametro(accionweb.getParameter("codayu"), "");
		String nivel = Util.validaParametro(accionweb.getParameter("nivel"), "");
		if(accionweb.getSesion().getAttribute("listaSubtipoAyudantia") != null)
			accionweb.getSesion().removeAttribute("listaSubtipoAyudantia");
		if (!codayu.trim().equals("") && !nivel.trim().equals("")) {
			PreswBean preswbean = null;
			Collection<Presw21DTO> lista = null;
			
			preswbean = new PreswBean("SA3",0,0,0,0,0,0,codayu,0,"",nivel,0,0,0,0,"","");
			lista = (Collection<Presw21DTO>) preswbean.consulta_presw21();			
		    for(Presw21DTO ss:lista){
		    	ss.setValor2(0);
		    	ss.setValor3(0);
		    	ss.setValor4(0);
		    	ss.setValor5(0);
		    }
		    	
			
			accionweb.agregarObjeto("listaSubtipoAyudantia", lista); 
			accionweb.getSesion().setAttribute("listaSubtipoAyudantia", lista);
		}		
		
	}
	public void calculaTotalSubAyudantia(AccionWeb accionweb) throws Exception {
		long valor1 = Util.validaParametro(accionweb.getParameter("valor1"), 0);
		long hA1 = Util.validaParametro(accionweb.getParameter("hA1"), 0);
		long hA2 = Util.validaParametro(accionweb.getParameter("hA2"), 0);
		long hA3 = Util.validaParametro(accionweb.getParameter("hA3"), 0);
		int indice = Util.validaParametro(accionweb.getParameter("indice"), 0);
		Collection<Presw21DTO> lista = (Collection<Presw21DTO>)accionweb.getSesion().getAttribute("listaSubtipoAyudantia");
		long total = 0;
		total = (valor1 * hA1) +  (valor1 * hA2) + (valor1 * hA3) ;
		accionweb.agregarObjeto("totalSubtipoAyudantia", total);
		int i=0;
		for(Presw21DTO ss:lista){
			if(i == indice){
				ss.setValor2(hA1);
				ss.setValor3(hA2);
				ss.setValor4(hA3);
				ss.setValor5(total);
			} 
		i++;
		}

	}
	public void calculaTotalMesSubAyudantia(AccionWeb accionweb) throws Exception {
		Collection<Presw21DTO> lista = (Collection<Presw21DTO>)accionweb.getSesion().getAttribute("listaSubtipoAyudantia");
		String fechafecIni = Util.validaParametro(accionweb.getParameter("fechafecIni"),"");
		String fechafecFin = Util.validaParametro(accionweb.getParameter("fechafecFin"),"");
		
		long totalMes = 0;
		for(Presw21DTO ss:lista){
			totalMes += ss.getValor5();
		}
		long totalContrato = 0;
		
		totalContrato = (totalMes/30) * moduloContratoAyudantiaB.calculoDiasTrabajados(fechafecIni, fechafecFin);
	    accionweb.agregarObjeto("totalContrato", totalContrato);
		accionweb.agregarObjeto("totalMes", totalMes);
	}
	public void calculaTotalAdministrativa(AccionWeb accionweb) throws Exception {
		long valorHora = Util.validaParametro(accionweb.getParameter("valorHora"), 0);
		int cantHora = Util.validaParametro(accionweb.getParameter("cantHora"), 0);
		String fechafecIni = Util.validaParametro(accionweb.getParameter("fechafecIni"),"");
		String fechafecFin = Util.validaParametro(accionweb.getParameter("fechafecFin"),"");
	
		long totalMes = valorHora * cantHora;
		long totalContrato = (totalMes/30) * moduloContratoAyudantiaB.calculoDiasTrabajados(fechafecIni, fechafecFin);
		  
	    accionweb.agregarObjeto("totalContrato", totalContrato);
		accionweb.agregarObjeto("totalMes", totalMes);
	}
	public void cargaModificaDatosPersonales(AccionWeb accionweb) throws Exception {
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		List<Presw18DTO> listaSede = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSede");
		//List<PRESF200> listaOrg  = (List<PRESF200>) accionweb.getSesion().getAttribute("listaOrg");
		//List<APREF19> listaTipoAyu = (List<APREF19>) accionweb.getSesion().getAttribute("listaTipoAyu");
		//List<APPEF01> listaProfesor = (List<APPEF01>) accionweb.getSesion().getAttribute("listaProfesor");
		List<Ciudad> listaCiudad = (List<Ciudad>) accionweb.getSesion().getAttribute("listaCiudad");
		List<Comuna> listaComuna = (List<Comuna>) accionweb.getSesion().getAttribute("listaComuna");
		
		this.cargaDatosPersonales(accionweb);
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		accionweb.agregarObjeto("rut_aux",rut_aux);
		accionweb.agregarObjeto("rutnum",rutnum);
		accionweb.agregarObjeto("dvrut",dvrut);
		
		if (listaSede != null && listaSede.size() > 0) accionweb.agregarObjeto("listaSede", listaSede);
		//if (listaOrg != null && listaOrg.size() > 0) accionweb.agregarObjeto("listaOrg", listaOrg);
		//if (listaTipoAyu != null && listaTipoAyu.size() > 0) accionweb.agregarObjeto("listaTipoAyu", listaTipoAyu);
		//if (listaProfesor != null && listaProfesor.size() > 0) accionweb.agregarObjeto("listaProfesor", listaProfesor);
		if (listaCiudad != null && listaCiudad.size() > 0) accionweb.agregarObjeto("listaCiudad", listaCiudad);
		if (listaComuna != null && listaComuna.size() > 0) accionweb.agregarObjeto("listaComuna", listaComuna);
	    accionweb.agregarObjeto("esContratoAyudantiaB", "1");
        accionweb.agregarObjeto("opcionMenu", opcionMenu);
        accionweb.agregarObjeto("muestraIngreso", 1);
	}
	
	public void cargaDatosPersonales(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		String identificadorDatos = Util.validaParametro(accionweb.getParameter("identificadorDatos"), "");
	
		List<PRESF200> listaOrg  = (List<PRESF200>) accionweb.getSesion().getAttribute("listaOrg");
		if(listaOrg == null || listaOrg.size() == 0){
			listaOrg = moduloContratoAyudantiaB.getOrganizacion();
			if(listaOrg != null && listaOrg.size() > 0)
		       accionweb.getSesion().setAttribute("listaOrg", listaOrg);
		}
		List<APREF19> listaTipoAyu = (List<APREF19>) accionweb.getSesion().getAttribute("listaTipoAyu");
		List<APPEF01> listaProfesor = (List<APPEF01>) accionweb.getSesion().getAttribute("listaProfesor");
		List<Ciudad> listaCiudad = (List<Ciudad>) accionweb.getSesion().getAttribute("listaCiudad");
		List<Comuna> listaComuna = (List<Comuna>) accionweb.getSesion().getAttribute("listaComuna");
		List<SucursalBanco> listaSucursalBanco = (List<SucursalBanco>) accionweb.getSesion().getAttribute("listaSucursalBanco");
		List<Banco> listaBanco = (List<Banco>) accionweb.getSesion().getAttribute("listaBanco");
		/*
		List<Presw18DTO> listaOrg  = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaOrg");
		
		if(listaOrg == null || listaOrg.size() == 0){
			listaOrg = moduloContratoAyudantiaB.getOrganizacion(rutUsuario, dv);
			if(listaOrg != null && listaOrg.size() > 0)
		       accionweb.getSesion().setAttribute("listaOrg", listaOrg);
		}*/
		if(listaTipoAyu == null || listaTipoAyu.size() == 0){
			listaTipoAyu = new ArrayList<APREF19>();
			listaTipoAyu = moduloContratoAyudantiaB.getTipoAyudantia();
			if(listaTipoAyu != null && listaTipoAyu.size() > 0)
			   accionweb.getSesion().setAttribute("listaTipoAyu", listaTipoAyu);
		}
		if(listaProfesor == null || listaProfesor.size() == 0){
			listaProfesor = new ArrayList<APPEF01>();
			listaProfesor = moduloContratoAyudantiaB.getListaProfesor();
			if(listaProfesor != null && listaProfesor.size() > 0) 
			   accionweb.getSesion().setAttribute("listaProfesor", listaProfesor);
		}	
		if(listaCiudad == null || listaCiudad.size() == 0){
			listaCiudad = new ArrayList<Ciudad>();
			listaCiudad = moduloContratoAyudantiaB.getCiudades();
			if(listaCiudad != null && listaCiudad.size() > 0) 
			   accionweb.getSesion().setAttribute("listaCiudad", listaCiudad);
		}	
		if(listaComuna == null || listaComuna.size() == 0){
			listaComuna = new ArrayList<Comuna>();
			listaComuna = moduloContratoAyudantiaB.getComunas();
			if(listaComuna != null && listaComuna.size() > 0) 
			   accionweb.getSesion().setAttribute("listaComuna", listaComuna);
		}
		if(listaBanco == null || listaBanco.size() == 0){
			listaBanco = new ArrayList<Banco>();
			listaBanco = moduloContratoAyudantiaB.getListaBanco();
			if(listaBanco != null && listaBanco.size() > 0) 
			   accionweb.getSesion().setAttribute("listaBanco", listaBanco);
		}
		
		List<Presw18DTO> listaSede = new ArrayList<Presw18DTO>();
		listaSede = moduloContratoAyudantiaB.getListaSede(accionweb.getReq());
		if(rutnum > 0 && rut_aux.trim().equals(""))
			rut_aux = moduloContratoAyudantiaB.formateoRut(rutnum+""+dvrut);
		
		if (listaSede != null && listaSede.size() > 0) accionweb.agregarObjeto("listaSede", listaSede);
		if (listaOrg != null && listaOrg.size() > 0) accionweb.agregarObjeto("listaOrg", listaOrg);
		if (listaTipoAyu != null && listaTipoAyu.size() > 0) accionweb.agregarObjeto("listaTipoAyu", listaTipoAyu);
		if (listaProfesor != null && listaProfesor.size() > 0) accionweb.agregarObjeto("listaProfesor", listaProfesor);
	    if (listaCiudad != null && listaCiudad.size() > 0) accionweb.agregarObjeto("listaCiudad", listaCiudad);
		if (listaComuna != null && listaComuna.size() > 0) accionweb.agregarObjeto("listaComuna", listaComuna);
		if (listaBanco != null && listaBanco.size() > 0) accionweb.agregarObjeto("listaBanco", listaBanco);
		if (listaSucursalBanco != null && listaSucursalBanco.size() > 0) accionweb.agregarObjeto("listaSucursalBanco", listaSucursalBanco);
			
		

		Presw19DTO preswbean19DTO = new Presw19DTO();
	   	String titulo = "";
		preswbean19DTO.setTippro("SA1"); 
	  	preswbean19DTO.setRutide(new BigDecimal(rutnum));
	  	preswbean19DTO.setDigide(dvrut);
		 
		String nomcam = "";
		long valnu1 = 0;
		long valnu2 = 0;
		String valalf = "";
		String paterno = "";
		String materno = "";
		String nombre = "";
		long sexo = 0;
		long estCivil = 0;
		String domicilio = "";
		long comuna = 0;
		long ciudad = 0;
		long region = 0;
		long codBan = 0;
		long sucBan = 0;
		String cueBan = "";
		 
	    List<Cocow36DTO>  listaDatosPersonales = new ArrayList<Cocow36DTO>();
		CocowBean cocowBean = new CocowBean();
		listaDatosPersonales = cocowBean.buscar_cocow36(preswbean19DTO);
		if(listaDatosPersonales != null && listaDatosPersonales.size() >0 ){
		   for(Cocow36DTO ss: listaDatosPersonales){
              if(ss.getNomcam().trim().equals("APEPAT"))
				 paterno = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("APEMAT"))
				 materno = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("NOMBRE"))
				 nombre = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("SEXO"))
				 sexo = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("ESTCIV"))
				 estCivil = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("DOMICI"))
				 domicilio = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("COMUNA"))
				 comuna = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("CIUDAD"))
				 ciudad = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("REGION"))
				 region = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("CODBAN"))
				 codBan = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("SUCBAN"))
				 sucBan = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("CUEBAN"))
				 cueBan = ss.getValalf().trim(); 
		    }  
		   if((paterno.trim().equals("") && identificadorDatos.trim().equals("")) || rutnum == 0){
			   listaDatosPersonales = null;
			   accionweb.agregarObjeto("mensaje", "RUT no registrado, debe ser agregado.");
			   accionweb.agregarObjeto("muestraBoton", 1);
		   } else {
			    listaSucursalBanco = moduloContratoAyudantiaB.getListaSucursalBanco(codBan);
				if(listaSucursalBanco != null && listaSucursalBanco.size() > 0) 
				   accionweb.agregarObjeto("listaSucursalBanco", listaSucursalBanco);
		   }
	     } else  if(identificadorDatos.trim().equals("") || rutnum == 0)  accionweb.agregarObjeto("mensaje", "RUT no registrado, debe ser agregado.");
		 //String rutFormateado = moduloContratoAyudantiaB.formateoRut(rutnum) + "-" + dvrut;
	
		 accionweb.agregarObjeto("rut_aux",rut_aux);
		 accionweb.agregarObjeto("paterno",paterno);
		 accionweb.agregarObjeto("materno",materno);
		 accionweb.agregarObjeto("nombre",nombre);
		 accionweb.agregarObjeto("sexo",sexo);
		 accionweb.agregarObjeto("estCivil",estCivil);
		 accionweb.agregarObjeto("domicilio",domicilio);
		 accionweb.agregarObjeto("comuna",comuna);
		 accionweb.agregarObjeto("ciudad",ciudad);
		 accionweb.agregarObjeto("region",region);
		 accionweb.agregarObjeto("codBan",codBan);
		 accionweb.agregarObjeto("sucBan",sucBan);
		 accionweb.agregarObjeto("cueBan",cueBan);
		 accionweb.agregarObjeto("rutnum",rutnum);
		 accionweb.agregarObjeto("dvrut",dvrut);
		 
		 if(opcionMenu != 2){
			 if(comuna > 0){
			 	for(Comuna ss: listaComuna) 		
    			if(ss.getCodigoComuna() == comuna) 
    				accionweb.agregarObjeto("nomComuna", ss.getNomComuna());
			 }
			 if(ciudad > 0){
			 	for(Ciudad ss: listaCiudad) 		
    			if(ciudad == ss.getCodigoCiudad())
    				accionweb.agregarObjeto("nomCiudad", ss.getNomCiudad());
			 }
			 	for(Banco ss: listaBanco) 		
    			if(codBan == ss.getCodBan())
    				accionweb.agregarObjeto("nomBanco", ss.getDesBan());
				if((sucBan > 0) && (codBan > 0)){
			   		
			 	for(SucursalBanco ss: listaSucursalBanco) 		
	   			if((sucBan == ss.getSucBan()) && (codBan == ss.getCodBan()))
	   					accionweb.agregarObjeto("nomSucursal", ss.getDesSuc());
				}
		 }
		
		 String fechaActual = new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	     accionweb.agregarObjeto("fechaSolicitudIng", fechaActual);
		 accionweb.agregarObjeto("listaDatosPersonales", listaDatosPersonales);
         accionweb.agregarObjeto("esContratoAyudantiaB", "1");
         accionweb.agregarObjeto("tipo", String.valueOf(tipo));
    	 accionweb.agregarObjeto("opcionMenu", opcionMenu);
    	 if(!identificadorDatos.trim().equals(""))
    		 accionweb.agregarObjeto("identificadorDatos", identificadorDatos);
    	
     }
	public void cargarCarrera(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
	
		
		int sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
		String glosa1 = Util.validaParametro(accionweb.getParameter("glosa1"),"");
		int curso = Util.validaParametro(accionweb.getParameter("curso"), 0);
		int carrera = Util.validaParametro(accionweb.getParameter("carrera"), 0);
				
		PreswBean preswbean = null;
		Collection<Presw21DTO> lista = null;
		if (sede > 0) {
		   preswbean = new PreswBean("SA2",curso,carrera,0,0,0,0,"",sede,"","",0,0,0,0,"","");
		   preswbean.setSucur(sede);
		   preswbean.setCoduni(curso);
		   preswbean.setItedoc(carrera);
		   lista = (Collection<Presw21DTO>) preswbean.consulta_presw21();
		   for(Presw21DTO ss:lista)
		     glosa1 = ss.getGlosa1(); 
		   accionweb.agregarObjeto("sede", sede);
		   accionweb.agregarObjeto("glosa1", glosa1);
		}
	}
	public void cargaSucursal(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int codBan = Util.validaParametro(accionweb.getParameter("codBan"), 0);
		List<SucursalBanco> listaSucursalBanco = new ArrayList<SucursalBanco>();
		listaSucursalBanco = moduloContratoAyudantiaB.getListaSucursalBanco(codBan);
		if(listaSucursalBanco != null && listaSucursalBanco.size() > 0) 
		   accionweb.agregarObjeto("listaSucursalBanco", listaSucursalBanco);
	
	}
	/*public void cargaDatosContrato(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
	
		
		int sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
		String glosa1 = Util.validaParametro(accionweb.getParameter("glosa1"),"");
		int curso = Util.validaParametro(accionweb.getParameter("curso"), 0);
		int carrera = Util.validaParametro(accionweb.getParameter("carrera"), 0);
		int rutProfesor = Util.validaParametro(accionweb.getParameter("rutProfesor"),0);
		String tipAyu = Util.validaParametro(accionweb.getParameter("tipAyu"),"");
		String nivAyu = Util.validaParametro(accionweb.getParameter("nivAyu"),"");
		String idSoli = Util.validaParametro(accionweb.getParameter("idSoli"),"");
		long valHoraAyu = Util.validaParametro(accionweb.getParameter("valHoraAyu"),0);
		String codAyu = Util.validaParametro(accionweb.getParameter("codAyu"),"");
		String subTipAyu = Util.validaParametro(accionweb.getParameter("sucTipAyu"),"");
		long valMinTarifa   = Util.validaParametro(accionweb.getParameter("valMinTarifa"),0);
		long valMaxTarifa   = Util.validaParametro(accionweb.getParameter("valMaxTarifa"),0);
		
		PreswBean preswbean = null;
		Presw21DTO lista = null;
		if (sede > 0) {
		   preswbean = new PreswBean("SA2",curso,carrera,0,0,0,0,"",sede,"","",0,0,0,0,"","");
		   lista = (Presw21DTO) preswbean.consulta_presw21();
		
		   glosa1 = lista.getGlosa1(); 
		   accionweb.agregarObjeto("sede", sede);
		   accionweb.agregarObjeto("glosa1", glosa1);
		}
		 // Glosa que se obiene a partir de la sede, carrera y curso
		
		if (!tipAyu.trim().equals("") && !nivAyu.trim().equals("")) {
			preswbean = null;
			lista = null;
			preswbean = new PreswBean("SA3",0,0,0,0,0,0,tipAyu,0,"",nivAyu,0,0,0,0,"","");
			lista = (Presw21DTO) preswbean.consulta_presw21();
			
			codAyu = lista.getGlosa1(); // C�digo de Ayudant�a
			subTipAyu = lista.getGlosa2(); // Codigo de subtipo de ayudant�a
			valHoraAyu = lista.getValor1(); // Valor hora Ayudantia
			idSoli = lista.getIdsoli(); // Identificaci�n de la solicitud
			
			accionweb.agregarObjeto("codAyu", codAyu); 
			accionweb.agregarObjeto("subTipAyu", subTipAyu); 
			accionweb.agregarObjeto("valHoraAyu", valHoraAyu);
			accionweb.agregarObjeto("idSoli", idSoli); 
		}
		
		if (!tipAyu.trim().equals("")) {
			preswbean = null;
			lista = null;
			preswbean = new PreswBean("SA4",0,0,0,0,0,0,tipAyu,0,"",nivAyu,0,0,0,0,"","");
			lista = (Presw21DTO) preswbean.consulta_presw21();
			
			valMinTarifa = lista.getValor1();
			valMaxTarifa = lista.getValor2();
			accionweb.agregarObjeto("valMinTarifa", valMinTarifa); 
			accionweb.agregarObjeto("valMaxTarifa", valMaxTarifa); 
			
		}
		
    }*/
	
	public void cargaOpcionMenu(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
		
		PreswBean preswbean = null;
		Collection<Presw18DTO> listaSolicitudes = null;
		String titulo = "";
		
		switch (tipo) {
		case  2:// lista de Solicitudes para modificar
			{
			preswbean = new PreswBean("SA7",0,0,0,0,rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			titulo = "Modificaci�n";
			break;
		   }
		case  3:// Consulta de ude las solicitudes de un rut Ingresador
			{
			preswbean = new PreswBean("SA9",0,0,0,0,rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			titulo = "Consulta";
			break;
		   }	
		case  4:// lista de solicitudes para autorizar SIIF
			{
			preswbean = new PreswBean("SAD",0,0,0,0,rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			titulo = "Autorizaci�n Responsable Organizaci�n";
			break;
		   }	
		case  5:// lista de solicitudes para CHEQUEO DGIP/SEDES
			{
			preswbean = new PreswBean("SAE",0,0,0,0,rutUsuario,0,"",sede,dv, "",0,0,0,0,"","");
			titulo = "Autorizaci�n DGIP Sedes";
			break;
		   }
		case  6:// lista de solicitudes para CHEQUEO DGAF
			{
			preswbean = new PreswBean("SAF",0,0,0,0,rutUsuario,0,"",sede,dv, "",0,0,0,0,"","");
			titulo = "Autorizaci�n DGAF";
			break;
		   }	
			
		}
		
		listaSolicitudes = (Collection<Presw18DTO>) preswbean.consulta_presw18();
	    if(listaSolicitudes != null && listaSolicitudes.size() > 0)
        	accionweb.agregarObjeto("hayDatoslista", "1");
		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	
      	List<Presw18DTO> listaSede = (List<Presw18DTO>)accionweb.getSesion().getAttribute("listaSede");
      	if(listaSede != null && listaSede.size() > 0)
 	       	accionweb.agregarObjeto("listaSede", listaSede);
 	   	
      	accionweb.agregarObjeto("sede", sede);
	    accionweb.agregarObjeto("titulo", titulo);
        accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes);
    	accionweb.agregarObjeto("esContratoAyudantiaB", "1");
	    accionweb.agregarObjeto("tipo", tipo);

	}	
	
	 public void consultaSolicitud(AccionWeb accionweb) throws Exception {
		 int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		 String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		 int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		 
		 int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		 
	
	  	 Presw19DTO preswbean19DTO = new Presw19DTO();
	   	 String titulo = "";
		 String tituloDetalle1 = "";
		 preswbean19DTO.setTippro("SAA"); 
	  	 preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
	  	 preswbean19DTO.setDigide(dv);
		 preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
		 List<Presw21DTO>  listaAyudantia = new ArrayList<Presw21DTO>();
		 List<Presw18DTO>  listaHistorial = new ArrayList<Presw18DTO>();
		 
		 String nomcam = "";
		 long valnu1 = 0;
		 long valnu2 = 0;
		 String valalf = "";
		 long rutSol = 0;
		 String dvSol = "";
		 long sede = 0;
		 String nomSede = "";
		 String codOrg = "";
		 String nomOrg = "";
		 String nomTrab = "";
		 String fechaSol = "";
		 long semAyu = 0;
		 long a�oAyu = 0;
		 String fechaIni = "";
		 String fechaFin = "";
		 String fechaEfeIni = "";
		 String fechaEfeFin = "";
		 String codAyu = "";
		 String codNiv = "";
		 long tipContrato = 0;
		 long canHora = 0;
		 long valHora = 0;
		 long valMes = 0;
		 long valCon = 0;
		 long rutProf1 = 0;
		 String dvProf1 = "";
		 String nomProf1 = "";
		 long rutProf2 = 0;
		 String dvProf2 = "";
		 String nomProf2 = "";
		 long rutProf3 = 0;
		 String dvProf3 = "";
		 String nomProf3 = "";
		 String asiAlf1 = "";
		 long asiNum1 = 0;
		 String paralelo1 = "";
		 String nomAsi1 = "";
		 String asiAlf2 = "";
		 long asiNum2 = 0;
		 String paralelo2 = "";
		 String nomAsi2 = "";
		 String asiAlf3 = "";
		 long asiNum3 = 0;
		 String paralelo3 = "";
		 String nomAsi3 = "";
		 String iddigi = "";
		 String fechaIng = "";
		 long codUni = 0;
		 String estado = "";
		 String apPaterno = "";
		 String apMaterno = "";
		 String nombre = "";
		 long sexo = 0;
		 long estCivil = 0;
		 String domicilio = "";
		 long comuna = 0;
		 long ciudad = 0;
		 long region = 0;
		 long codBan = 0;
		 long sucBan = 0;
		 String nivaca = "";
		 long totalMes = 0;
		 
		 List<Cocow36DTO>  listaSolicitud = new ArrayList<Cocow36DTO>();
		 CocowBean cocowBean = new CocowBean();
		 listaSolicitud = cocowBean.buscar_cocow36(preswbean19DTO);
		 if(listaSolicitud != null && listaSolicitud.size() >0 ){
		    for(Cocow36DTO ss: listaSolicitud){
              if(ss.getNomcam().trim().equals("RUTIDE")) 
               	rutSol = ss.getValnu1();
              if(ss.getNomcam().trim().equals("DIGIDE"))
	            dvSol = ss.getValalf().trim();
              if(ss.getNomcam().trim().equals("SUCUR"))
	            sede = ss.getValnu1();
	          if(ss.getNomcam().trim().equals("NOMSUC"))
	            nomSede = ss.getValalf().trim();
	 		  if(ss.getNomcam().trim().equals("CODORG"))
		    	codOrg = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("DESORG"))
		    	 nomOrg = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("NOMTRA"))
		    	 nomTrab = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("NIVACA"))
			    	 nivaca = ss.getValalf().trim();
			      
		      if(ss.getNomcam().trim().equals("FECSOL"))
		   		fechaSol = (ss.getValnu1() > 0)?(ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4):"";
		      if(ss.getNomcam().trim().equals("SEMEST"))
	            semAyu = ss.getValnu1();
	          if(ss.getNomcam().trim().equals("ANOAYU"))
	        	a�oAyu = ss.getValnu1(); 
	          if(ss.getNomcam().trim().equals("FECINI"))
			   	fechaIni =(ss.getValnu1() > 0)? (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4):"";
	          if(ss.getNomcam().trim().equals("FECFIN"))
			   	fechaFin = (ss.getValnu1() > 0)?(ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4):"";
	          if(ss.getNomcam().trim().equals("FEEFIN") && ss.getValnu1() > 0)
				fechaEfeIni = (ss.getValnu1() > 0)?(ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4):"";
		      if(ss.getNomcam().trim().equals("FEEFFI") && ss.getValnu1() > 0)
				fechaEfeFin = (ss.getValnu1() > 0)?(ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4):"";
		      if(ss.getNomcam().trim().equals("CODAYU"))
			    codAyu = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("CODNIV"))
				codNiv = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("TIPCON"))
		        tipContrato = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("VALHOR"))
		        valHora = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("CANHOR"))
			    canHora = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("VALMES"))
		        valMes = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("VALCON"))
			        valCon = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("RUTPRO"))
		        rutProf1 = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("DIGPRO"))
			    dvProf1 = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("NOMPRO"))
			    nomProf1 = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("RUTPRO2"))
			    rutProf2 = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("DIGPRO2"))
				dvProf2 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("NOMPRO2"))
				nomProf2 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("RUTPRO3"))
				rutProf3 = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("DIGPRO3"))
			    dvProf3 = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("NOMPRO3"))
			    nomProf3 = ss.getValalf().trim();   
		      if(ss.getNomcam().trim().equals("ASIALF1"))
				 asiAlf1 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("ASINUM1"))
				 asiNum1 = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("PARALE1"))
				 paralelo1 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("DESASI1"))
				 nomAsi1 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("ASIALF2"))
				 asiAlf2 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("ASINUM2"))
				 asiNum2 = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("PARALE2"))
				 paralelo2 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("DESASI2"))
				 nomAsi2 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("ASIALF3"))
				 asiAlf3 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("ASINUM3"))
				 asiNum3 = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("PARALE3"))
				 paralelo3 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("DESASI3"))
				 nomAsi3 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("IDDIGI"))
				 iddigi = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("FECING") && ss.getValnu1() > 0)
				 fechaIng = (ss.getValnu1() > 0)?(ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4):"";
			  if(ss.getNomcam().trim().equals("CODUNI"))
				 codUni = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("ESTADO"))
				 estado = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("APEPAT"))
				 apPaterno = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("APEMAT"))
				 apMaterno = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("NOMBRE"))
				 nombre = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("SEXO"))
				 sexo = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("ESTCIV"))
				 estCivil = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("DOMICI"))
				 domicilio = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("COMUNA"))
				 comuna = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("CIUDAD"))
				 ciudad = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("REGION"))
				 region = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("CODBAN"))
				 codBan = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("SUCBAN"))
				 sucBan = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("DETALLE")){
	           	Presw21DTO presw21DTO = new Presw21DTO();
	           	presw21DTO.setGlosa1(ss.getValalf().trim().substring(0,3)); // tipo Ayudantia, 
	           	presw21DTO.setGlosa2(ss.getValalf().trim().substring(3,6)); // Asign Alfabetica       	
	           	presw21DTO.setGlosa3(ss.getValalf().trim().substring(6)); // paralelo
	           	presw21DTO.setValor1(ss.getValnu1()); //  Asign. numerica
	           	presw21DTO.setValor2(ss.getValnu2()); // Cantidad de horas
	           	presw21DTO.setValor3(ss.getRutusu()); // Valor hora
	           	listaAyudantia.add(presw21DTO);
	           }
			  if(ss.getNomcam().trim().equals("HISTORIAL")){
		           	Presw18DTO presw18DTO = new Presw18DTO();
		           	presw18DTO.setUsadom(ss.getValnu1()); // fecha
		           	presw18DTO.setDesite(ss.getResval()); // tipo de operaci�n 
		           	presw18DTO.setDesuni(ss.getValalf()); // Nombre operador       	
		           	listaHistorial.add(presw18DTO);
		           }
		    }      
	     }
	
		 String rutFormateado = moduloContratoAyudantiaB.formateoRut(rutSol+""+dvSol);
		 List<APREF24> lista = moduloContratoAyudantiaB.getListaSemetre(Integer.parseInt(String.valueOf(sede)));
		 if(lista != null){
			accionweb.getSesion().setAttribute("listaSemestre", lista);
			accionweb.agregarObjeto("listaSemestre", lista);
		 }
		 int canniv = 0;
		 List<APREF19> listaTipoAyu = new ArrayList<APREF19>();
		 listaTipoAyu = moduloContratoAyudantiaB.getTipoAyudantia();
			if(listaTipoAyu != null && listaTipoAyu.size() > 0){
			 for(APREF19 ss: listaTipoAyu){
				 if(codAyu.trim().equals(ss.getCODAYU().trim()))
						 canniv = ss.getCANNIV();
			 }
			accionweb.agregarObjeto("canniv", canniv);
			accionweb.agregarObjeto("listaTipoAyu", listaTipoAyu);
			}
			
			long valMinTarifa = 0;
			long valMaxTarifa = 0;
			if (!codAyu.trim().equals("")) {
				PreswBean preswbean = null;
				List<Presw21DTO> list = null;
				preswbean = new PreswBean("SA4", 0, 0, 0, 0, 0);
				preswbean.setTipdoc(codAyu);
				list = (List<Presw21DTO>) preswbean.consulta_presw21();
				for(Presw21DTO ss:list) {
					valMinTarifa = ss.getValor1();
					valMaxTarifa = ss.getValor2();
				}
				accionweb.agregarObjeto("valMinTarifa", valMinTarifa); 
				accionweb.agregarObjeto("valMaxTarifa", valMaxTarifa); 
				
			}
			
			if (!codAyu.trim().equals("") && !codNiv.trim().equals("")) {
				PreswBean preswbean = null;
				Collection<Presw21DTO> list = null;
				
				preswbean = new PreswBean("SA3",0,0,0,0,0,0,codAyu,0,"",codNiv,0,0,0,0,"","");
				list = (Collection<Presw21DTO>) preswbean.consulta_presw21();	
				  for(Presw21DTO ss:list){
				    	ss.setValor2(0);
				    	ss.setValor3(0);
				    	ss.setValor4(0);
				    	ss.setValor5(0);
				    }
				for(Presw21DTO ss: list){
					for(Presw21DTO ss2: listaAyudantia){
						if(ss.getGlosa1().trim().equals(ss2.getGlosa1().trim()) && 	// subtipo ayu
						   asiAlf1.trim().equals(ss2.getGlosa2().trim()) && 		// asignat alf
						   asiNum1 == ss2.getValor1() && 						// asig num		   
						   Integer.parseInt(paralelo1.trim()) == Integer.parseInt(ss2.getGlosa3().trim()) 	// paralelo							   
						) {
							ss.setValor2(ss2.getValor2()); // cant horas
							//ss.setValor5(ss2.getValor1() * ss.getValor1());
						}
						
						if(ss.getGlosa1().trim().equals(ss2.getGlosa1().trim()) && 	// subtipo ayu
								   asiAlf2.trim().equals(ss2.getGlosa2().trim()) && 		// asignat alf
								   asiNum2 == ss2.getValor1() && 						// asig num		   
								   Integer.parseInt(paralelo2.trim()) == Integer.parseInt(ss2.getGlosa3().trim())  	// paralelo							   
								) {
									ss.setValor3(ss2.getValor2()); // cant horas
									//ss.setValor5(ss2.getValor1() * ss.getValor1());
								}
						if(ss.getGlosa1().trim().equals(ss2.getGlosa1().trim()) && 	// subtipo ayu
								   asiAlf3.trim().equals(ss2.getGlosa2().trim()) && 		// asignat alf
								   asiNum3 == ss2.getValor1() && 						// asig num		   
								   Integer.parseInt(paralelo3.trim()) == Integer.parseInt(ss2.getGlosa3().trim())  	// paralelo							   
								) {
									ss.setValor4(ss2.getValor2()); // cant horas
									//ss.setValor5(ss2.getValor1() * ss.getValor1());
								}
				    }
					ss.setValor5((ss.getValor1() * ss.getValor2()) + (ss.getValor1() * ss.getValor3()) + (ss.getValor1() * ss.getValor4()));
			      	
				     
				}
				accionweb.agregarObjeto("listaSubtipoAyudantia", list); 
				accionweb.getSesion().setAttribute("listaSubtipoAyudantia", list);
				accionweb.agregarObjeto("totalMes", valMes);
				accionweb.agregarObjeto("totalContrato", valCon);
			//	accionweb.agregarObjeto("valorMes", valMes);
			//	accionweb.agregarObjeto("valorCon", valCon);
			  
			}		
			
			List<APPEF01> listaProfesor = new ArrayList<APPEF01>();
			listaProfesor = (List<APPEF01>)accionweb.getSesion().getAttribute("listaProfesor");
			if(sede > 0){
				List<ASIGNATURA> listaAsignatura = new ArrayList<ASIGNATURA>();
				listaAsignatura = moduloContratoAyudantiaB.getListaAsignatura(Integer.parseInt(String.valueOf(sede)));
				if(listaAsignatura != null && listaAsignatura.size() > 0) 
				   accionweb.agregarObjeto("listaAsignatura", listaAsignatura);
			}
		 if(tipo != 2){
			 List<PRESF200> listaOrg  = (List<PRESF200>) accionweb.getSesion().getAttribute("listaOrg");
			 for(PRESF200 ss: listaOrg)                     
 			 if(codOrg.trim().equals(ss.getCODORG().trim()) )
 				 accionweb.agregarObjeto("nomOrganizacion", ss.getCODORG() + "-" + ss.getDESORG().trim());
		 
		 }
	     accionweb.agregarObjeto("listaProfesor", listaProfesor);	
		 accionweb.agregarObjeto("rutnum",rutSol);
		 accionweb.agregarObjeto("dvrut",dvSol);
		 accionweb.agregarObjeto("sede",sede);
	     accionweb.agregarObjeto("nomSede",nomSede);
		 accionweb.agregarObjeto("codOrg",codOrg);
		 accionweb.agregarObjeto("nomOrg",nomOrg);
		 accionweb.agregarObjeto("tipTrabajo",nomTrab);
		 accionweb.agregarObjeto("glosa1", nivaca);
		 accionweb.agregarObjeto("fechaSolicitudIng",fechaSol);
		 accionweb.agregarObjeto("semAyu",semAyu);
		 accionweb.agregarObjeto("anoAyu",a�oAyu);
		 accionweb.agregarObjeto("sem",semAyu);
		 accionweb.agregarObjeto("anio",a�oAyu);
		 accionweb.agregarObjeto("fechaPeriodoSemIni",fechaIni);
		 accionweb.agregarObjeto("fechaPeriodoSemFin",fechaFin);
		 accionweb.agregarObjeto("fechaEfecIni",fechaEfeIni);
		 accionweb.agregarObjeto("fechaEfecFin",fechaEfeFin);
		 accionweb.agregarObjeto("codAyu",codAyu);
		 accionweb.agregarObjeto("codNivel",codNiv);
		 accionweb.agregarObjeto("tipContrato",tipContrato);
		 accionweb.agregarObjeto("valorHora",valHora);
		 accionweb.agregarObjeto("canHora",canHora);
		 accionweb.agregarObjeto("valorCon",valCon);			
		 accionweb.agregarObjeto("valorMes",valMes);
		 accionweb.agregarObjeto("rutProf1",rutProf1);
		 accionweb.agregarObjeto("digProf1",dvProf1);
		 accionweb.agregarObjeto("rutProf2",rutProf2);
		 accionweb.agregarObjeto("digProf2",dvProf2);
		 accionweb.agregarObjeto("rutProf3",rutProf3);
		 accionweb.agregarObjeto("digProf3",dvProf3);
		 accionweb.agregarObjeto("asiAlf1",asiAlf1);
		 accionweb.agregarObjeto("asiNum1",asiNum1);
		 accionweb.agregarObjeto("paralelo1",paralelo1);
		 accionweb.agregarObjeto("nomAsi1",nomAsi1);
		 accionweb.agregarObjeto("asiAlf2",asiAlf2);
		 accionweb.agregarObjeto("asiNum2",asiNum2);
		 accionweb.agregarObjeto("paralelo2",paralelo2);
		 accionweb.agregarObjeto("nomAsi2",nomAsi2);
		 accionweb.agregarObjeto("asiAlf3",asiAlf3);
		 accionweb.agregarObjeto("asiNum3",asiNum3);
		 accionweb.agregarObjeto("paralelo3",paralelo3);
		 accionweb.agregarObjeto("nomAsi3",nomAsi3);
		 accionweb.agregarObjeto("iddigi",iddigi);
		 accionweb.agregarObjeto("fechaIng",fechaIng);
		 accionweb.agregarObjeto("codUni",codUni);
		 accionweb.agregarObjeto("estado",estado);
		 accionweb.agregarObjeto("apPaterno",apPaterno);
		 accionweb.agregarObjeto("apMaterno",apMaterno);
		 accionweb.agregarObjeto("nombre",nombre);
		 accionweb.agregarObjeto("sexo",sexo);
		 accionweb.agregarObjeto("estCivil",estCivil);
		 accionweb.agregarObjeto("domicilio",domicilio);
		 accionweb.agregarObjeto("comuna",comuna);
		 accionweb.agregarObjeto("ciudad",ciudad);
		 accionweb.agregarObjeto("region",region);
		 accionweb.agregarObjeto("codBan",codBan);
		 accionweb.agregarObjeto("sucBan",sucBan);
		 accionweb.agregarObjeto("numDoc",numDoc);
		 accionweb.agregarObjeto("listaHistorial",listaHistorial);
			

		 
		 /*	  ut type="hidden" id="totalMes" name="totalMes"  value="$!totalMes">
	      <input type="hidden" id="totalContrato" name="totalContrato"  value="$!totalContrato">*/
		 accionweb.agregarObjeto("listaAyudantia",listaAyudantia);
	
		 if(tipo != 2)
			 accionweb.agregarObjeto("identificadorConsulta",listaAyudantia);
		 String titulo2 = "";
		 switch (tipo){
		 case 2: {titulo2 = "Modificaci�n";
		 		 break;
		 }
		 case 3: {titulo2 = "Consulta";
 		 break;
		 }
		 case 4: {titulo2 = "Autoriza";
 		 break;
		 }
		 }
		 this.cargaDatosPersonales(accionweb);
	     accionweb.agregarObjeto("esContratoAyudantiaB", "1");
		 accionweb.agregarObjeto("titulo2",titulo2);
		  	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	      accionweb.agregarObjeto("tipo", tipo);
	 }
	
	 synchronized public void registraSolicitud(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			
			int numSolicitud = 0;
			String mensaje = "";
			numSolicitud = moduloContratoAyudantiaB.getRegistraSolicitud(accionweb.getReq(),rutUsuario,dv);
	
		    if( numSolicitud > 0){
		       accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa la Solicitud de Contrataci�n de Ayudant�a N� Solicitud: "+ numSolicitud);
		       System.out.println("numSolicitud: "+numSolicitud);
		       accionweb.agregarObjeto("registra", 1);
			  // this.cargaDatosContrato(accionweb);
		    } else
		      	accionweb.agregarObjeto("mensaje", "No se registr� la Solicitud de Contrataci�n de Ayudant�a.");
		    if(accionweb.getSesion().getAttribute("listaSemestre") != null)
		    	accionweb.getSesion().removeAttribute("listaSemestre");
		    if(accionweb.getSesion().getAttribute("listaSubtipoAyudantia") != null)
		    	accionweb.getSesion().removeAttribute("listaSubtipoAyudantia");
		    
		    accionweb.agregarObjeto("esContratoAyudantiaB", "1");  
		 	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		}
	 
	 synchronized public void actualizaSolicitud(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			
			boolean registra = false;
		    
		    registra = moduloContratoAyudantiaB.getActualizaSolicitud(accionweb.getReq(),rutUsuario,dv,numDoc);
		    if(registra){
		   	    accionweb.agregarObjeto("mensaje", "Se actualiz� en forma exitosa la Solicitud de Ayudant�a.");
		   	    accionweb.agregarObjeto("registra", 1);
		   	    consultaSolicitud(accionweb);
		    } else
		    	accionweb.agregarObjeto("mensaje", "No se modific� la Solicitud de Ayudant�a..");
		    if(accionweb.getSesion().getAttribute("listaSemestre") != null)
		    	accionweb.getSesion().removeAttribute("listaSemestre");
		    if(accionweb.getSesion().getAttribute("listaSubtipoAyudantia") != null)
		    	accionweb.getSesion().removeAttribute("listaSubtipoAyudantia");
		    
		    accionweb.agregarObjeto("esContratoAyudantiaB", "1");  
		 	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	}
	 
	synchronized public void actualizaEstadoSolicitud(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			String estado = Util.validaParametro(accionweb.getParameter("estado"),"");	
		 //	String glosa = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "",""); por si debe agregar el motivo
			
			try { 
				boolean graba = moduloContratoAyudantiaB.getActualizaEstadoSolicitud(numDoc,rutUsuario,dv,"SAB",estado);
				if (estado.trim().equals("I")) {
			        if(graba) accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la Autorizaci�n de la solicitud.");
			        else accionweb.agregarObjeto("mensaje", "No se grab� la Autorizaci�n.");
				}
				else if (estado.trim().equals("R")){
						if(graba) accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el Rechazo de la solicitud.");
				        else accionweb.agregarObjeto("mensaje", "No se grab� el Rechazo.");
				}
			} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado rechazaAutOrdenCompra.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
			Thread.sleep(500);
			consultaSolicitud(accionweb);
		   	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		} 
	
	synchronized public void actualizaEstadoSolicitudDGAF(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numSol = Util.validaParametro(accionweb.getParameter("numDoc"), 0);	
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");	
	 //	String glosa = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "",""); por si debe agregar el motivo
		
		try { 
			boolean graba = moduloContratoAyudantiaB.getActualizaEstadoSolicitud(numSol,rutUsuario,dv,"SAC",estado);
			if (estado.trim().equals("J")) {
		        if(graba) accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la Autorizaci�n de la solicitud.");
		        else accionweb.agregarObjeto("mensaje", "No se grab� la Autorizaci�n.");
			}
			else if (estado.trim().equals("L")) {
					if(graba) accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el Rechazo de la solicitud.");
			        else accionweb.agregarObjeto("mensaje", "No se grab� el Rechazo.");
			      }
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado rechazaAutOrdenCompra.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		Thread.sleep(500);
		consultaSolicitud(accionweb);
		
	  	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	} 
	
	synchronized public void actualizaEstadoSolicitudDGIP_SEDE(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");	
	 //	String glosa = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "",""); por si debe agregar el motivo
		
		try { 
			boolean graba = moduloContratoAyudantiaB.getActualizaEstadoSolicitud(numDoc,rutUsuario,dv,"SAG",estado);
			if (estado.trim().equals("A")) {
		        if(graba) accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la Autorizaci�n de la solicitud.");
		        else accionweb.agregarObjeto("mensaje", "No se grab� la Autorizaci�n.");
			}
			else if (estado.trim().equals("H")) {
					if(graba) accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el Rechazo de la solicitud.");
			        else accionweb.agregarObjeto("mensaje", "No se grab� el Rechazo.");
			      }
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado rechazaAutOrdenCompra.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		Thread.sleep(500);
		consultaSolicitud(accionweb);
	    if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	} 
	
	synchronized public void actualizaDatosPersonales(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		int numSol = Util.validaParametro(accionweb.getParameter("numSol"), 0);
		String paterno = Util.validaParametro(accionweb.getParameter("paterno"),"");
		String materno = Util.validaParametro(accionweb.getParameter("materno"),"");
		String nombre = Util.validaParametro(accionweb.getParameter("nombre"),"");
		long sexo = Util.validaParametro(accionweb.getParameter("sexo"),0);
		long estCivil = Util.validaParametro(accionweb.getParameter("estCivil"),0);
		String domicilio = Util.validaParametro(accionweb.getParameter("domicilio"),"");
		long comuna = Util.validaParametro(accionweb.getParameter("comuna"),0);
		long ciudad = Util.validaParametro(accionweb.getParameter("ciudad"),0);
		long region = Util.validaParametro(accionweb.getParameter("region"),0);
		long codBan = Util.validaParametro(accionweb.getParameter("codBan"),0);
		long sucBan = Util.validaParametro(accionweb.getParameter("sucBan"),0);
		String cueBan = Util.validaParametro(accionweb.getParameter("cueBan"),"");
				
		boolean registra = false;
		
		PreswBean preswbean = new PreswBean("SA6", 0,0,0,0, rutUsuario);
		preswbean.setRutide(rutUsuario);
		preswbean.setDigide(dv);
	 		
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		presw25DTO.setTippro("SA6");
		presw25DTO.setRutide(rutnum);
		presw25DTO.setDigide(dvrut);
		presw25DTO.setComen1(paterno);
		presw25DTO.setComen2(materno);
		presw25DTO.setComen3(nombre);
		presw25DTO.setPres01(sexo);
		presw25DTO.setPres02(estCivil);
		presw25DTO.setComen4(domicilio);
		presw25DTO.setPres03(comuna);
		presw25DTO.setPres04(ciudad);
		presw25DTO.setPres05(region);
		presw25DTO.setPres06(codBan);
		presw25DTO.setPres07(sucBan);
		presw25DTO.setComen5(cueBan);
		lista.add(presw25DTO);
		    
	    registra = preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	    if(registra){
	   	    accionweb.agregarObjeto("mensaje", "Se actualiz� en forma exitosa los datos personales.");
	   	    accionweb.agregarObjeto("registra", 1);
	   	 this.cargaDatosPersonales(accionweb);
	    } else
	    	accionweb.agregarObjeto("mensaje", "No se modificaron los datos personales.");
		this.cargaDatosPersonales(accionweb);
	    accionweb.agregarObjeto("esContratoAyudantiaB", "1");    
    }
	
	public void imprimeSolicitud(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		 String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		 int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		 
		 int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		 
	
	  	 Presw19DTO preswbean19DTO = new Presw19DTO();
	   	 String titulo = "";
		 String tituloDetalle1 = "";
		 preswbean19DTO.setTippro("SAA"); 
	  	 preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
	  	 preswbean19DTO.setDigide(dv);
		 preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
		 		 
		 String nomcam = "";
		 long valnu1 = 0;
		 long valnu2 = 0;
		 String valalf = "";
		 long rutSol = 0;
		 String dvSol = "";
		 long sede = 0;
		 String nomSede = "";
		 String codOrg = "";
		 String nomOrg = "";
		 String nomTrab = "";
		 String fechaSol = "";
		 long semAyu = 0;
		 long a�oAyu = 0;
		 String fechaIni = "";
		 String fechaFin = "";
		 String fechaEfeIni = "";
		 String fechaEfeFin = "";
		 String codAyu = "";
		 String codNiv = "";
		 long tipContrato = 0;
		 long numHora = 0;
		 long valMes = 0;
		 long rutProf1 = 0;
		 String dvProf1 = "";
		 String nomProf1 = "";
		 long rutProf2 = 0;
		 String dvProf2 = "";
		 String nomProf2 = "";
		 long rutProf3 = 0;
		 String dvProf3 = "";
		 String nomProf3 = "";
		 String asiAlf1 = "";
		 long asiNum1 = 0;
		 String paralelo1 = "";
		 String nomAsi1 = "";
		 String asiAlf2 = "";
		 long asiNum2 = 0;
		 String paralelo2 = "";
		 String nomAsi2 = "";
		 String asiAlf3 = "";
		 long asiNum3 = 0;
		 String paralelo3 = "";
		 String nomAsi3 = "";
		 String iddigi = "";
		 String fechaIng = "";
		 long codUni = 0;
		 String estado = "";
		 String apPaterno = "";
		 String apMaterno = "";
		 String nombre = "";
		 long sexo = 0;
		 long estCivil = 0;
		 String domicilio = "";
		 long comuna = 0;
		 long ciudad = 0;
		 long region = 0;
		 long codBan = 0;
		 long sucBan = 0;
		 String cueBan = "";
		 
		 Vector vecSolicitudes = new Vector();
		 Vector vecTipoAyudantia = new Vector();
		 String tipoAyu = "";
		 
		 List<Cocow36DTO>  listaSolicitud = new ArrayList<Cocow36DTO>();
		 CocowBean cocowBean = new CocowBean();
		 listaSolicitud = cocowBean.buscar_cocow36(preswbean19DTO);
		 if(listaSolicitud != null && listaSolicitud.size() >0 ){
		    for(Cocow36DTO ss: listaSolicitud){
             if(ss.getNomcam().trim().equals("RUTIDE")) 
              	rutSol = ss.getValnu1();
             if(ss.getNomcam().trim().equals("DIGIDE"))
	            dvSol = ss.getValalf().trim();
             if(ss.getNomcam().trim().equals("SUCUR"))
	            sede = ss.getValnu1();
	          if(ss.getNomcam().trim().equals("NOMSUC"))
	            nomSede = ss.getValalf().trim();
	 		  if(ss.getNomcam().trim().equals("CODORG"))
		    	codOrg = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("DESORG"))
		    	 nomOrg = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("NOMTRA"))
		    	 nomTrab = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("FECSOL"))
		   		fechaSol = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
		      if(ss.getNomcam().trim().equals("SEMEST"))
	            semAyu = ss.getValnu1();
	          if(ss.getNomcam().trim().equals("ANOAYU"))
	        	a�oAyu = ss.getValnu1(); 
	          if(ss.getNomcam().trim().equals("FECINI"))
			   	fechaIni = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
	          if(ss.getNomcam().trim().equals("FECFIN"))
			   	fechaFin = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
	          if(ss.getNomcam().trim().equals("FEEFIN") && ss.getValnu1() > 0)
				fechaEfeIni = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
		      if(ss.getNomcam().trim().equals("FEEFFI") && ss.getValnu1() > 0)
				fechaEfeFin = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
		      if(ss.getNomcam().trim().equals("CODAYU"))
			    codAyu = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("CODNIV"))
				codNiv = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("TIPCON"))
		        tipContrato = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("VALHOR"))
		        numHora = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("VALMES"))
		        valMes = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("RUTPRO"))
		        rutProf1 = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("DIGPRO"))
			    dvProf1 = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("NOMPRO"))
			    nomProf1 = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("RUTPRO2"))
			    rutProf2 = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("DIGPRO2"))
				dvProf2 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("NOMPRO2"))
				nomProf2 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("RUTPRO3"))
				rutProf3 = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("DIGPRO3"))
			    dvProf3 = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("NOMPRO3"))
			    nomProf3 = ss.getValalf().trim();   
		      if(ss.getNomcam().trim().equals("ASIALF1"))
				 asiAlf1 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("ASINUM1"))
				 asiNum1 = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("PARALE1"))
				 paralelo1 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("DESASI1"))
				 nomAsi1 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("ASIALF2"))
				 asiAlf2 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("ASINUM2"))
				 asiNum2 = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("PARALE2"))
				 paralelo2 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("DESASI2"))
				 nomAsi2 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("ASIALF3"))
				 asiAlf3 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("ASINUM3"))
				 asiNum3 = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("PARALE3"))
				 paralelo3 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("DESASI3"))
				 nomAsi3 = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("IDDIGI"))
				 iddigi = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("FECING") && ss.getValnu1() > 0)
				 fechaIng = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
			  if(ss.getNomcam().trim().equals("CODUNI"))
				 codUni = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("ESTADO"))
				 estado = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("APEPAT"))
				 apPaterno = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("APEMAT"))
				 apMaterno = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("NOMBRE"))
				 nombre = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("SEXO"))
				 sexo = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("ESTCIV"))
				 estCivil = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("DOMICI"))
				 domicilio = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("COMUNA"))
				 comuna = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("CIUDAD"))
				 ciudad = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("REGION"))
				 region = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("CODBAN"))
				 codBan = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("SUCBAN"))
				 sucBan = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("CUEBAN"))
				 cueBan = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("DETALLE")){
			     Vector vec = new Vector();
	           	 vec.add(ss.getValalf().trim().substring(0,3)); // subtipo Ayudantia, 
	             vec.add(ss.getRutusu()); // Valor hora
	        	 vec.add(ss.getValnu2()); // Cantidad de horas
	             vecTipoAyudantia.add(vec);
	           }
		    }      
	     }
	
		 String rutFormateado = moduloContratoAyudantiaB.formateoRut(rutSol+""+dvSol);
			 // Datos ayudante
		 vecSolicitudes.add(numDoc);
		 vecSolicitudes.add(rutFormateado);
		 vecSolicitudes.add(apPaterno);
		 vecSolicitudes.add(apMaterno);
		 vecSolicitudes.add(nombre);
		 vecSolicitudes.add((sexo==0?"Masculino":"Femenino"));
		 vecSolicitudes.add((estCivil==1?"Soltero":(estCivil==2?"Casado":(estCivil==3?"Separado":(estCivil==4?"Viudo":"Sin Informaci�n")))));
		 vecSolicitudes.add(domicilio);
		 vecSolicitudes.add(moduloContratoAyudantiaB.getNombreComuna(comuna));
		 vecSolicitudes.add(moduloContratoAyudantiaB.getNombreCiudad(ciudad));
		 vecSolicitudes.add(region);
		 vecSolicitudes.add(moduloContratoAyudantiaB.getNombreBanco(codBan));
		 vecSolicitudes.add(moduloContratoAyudantiaB.getNombreSucBanco(codBan,sucBan));
		 vecSolicitudes.add(cueBan);
		// Datos Solicitud
		 vecSolicitudes.add(sede);
		 vecSolicitudes.add(nomSede);
		 vecSolicitudes.add(codOrg);
		 vecSolicitudes.add(nomOrg);
		 vecSolicitudes.add(nomTrab);
		 String glosaCarrera = "";
		 vecSolicitudes.add(glosaCarrera);
		 vecSolicitudes.add(fechaSol);
		 vecSolicitudes.add(a�oAyu);
		 vecSolicitudes.add(semAyu);
		 vecSolicitudes.add(fechaIni);
		 vecSolicitudes.add(fechaFin);
		 vecSolicitudes.add(fechaEfeIni);
		 vecSolicitudes.add(fechaEfeFin);
		 vecSolicitudes.add(codAyu);
		 vecSolicitudes.add(moduloContratoAyudantiaB.getNombreAyudantia(codAyu));
		 vecSolicitudes.add((tipContrato==1?"1 - Con Previsi�n":(tipContrato==5?"5 - Honorarios":"Sin infromaci�n")));
		 
		 
		 //Datos docentes y acad�micos
		 vecSolicitudes.add(codNiv);
		 if (!codNiv.trim().equals("")) {
			 vecSolicitudes.add(moduloContratoAyudantiaB.formateoRut(rutProf1+""+dvProf1) + " " + nomProf1);
			 vecSolicitudes.add(asiAlf1+""+asiNum1+" - " +nomAsi1);
			 vecSolicitudes.add(paralelo1);
			 vecSolicitudes.add(moduloContratoAyudantiaB.formateoRut(rutProf2+""+dvProf2) + " " + nomProf2);
			 vecSolicitudes.add(asiAlf2+""+asiNum2+" - " +nomAsi2);
			 vecSolicitudes.add(paralelo2);
			 vecSolicitudes.add(moduloContratoAyudantiaB.formateoRut(rutProf3+""+dvProf3) + " " + nomProf3);
			 vecSolicitudes.add(asiAlf3+""+asiNum3+" - " +nomAsi3);
			 vecSolicitudes.add(paralelo3);
	     }		 
		 // Datos para lista de ayudantia
		
		 accionweb.getSesion().setAttribute("vecSolicitudes", vecSolicitudes);
         accionweb.getSesion().setAttribute("vecTipoAyudantia", vecTipoAyudantia);
		 accionweb.agregarObjeto("esContratoAyudantiaB", "1");
		 
	 }
	
	
	
 
}
