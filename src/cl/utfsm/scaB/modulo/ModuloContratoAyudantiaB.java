package cl.utfsm.scaB.modulo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Formatter;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.tools.generic.DateTool;

import cl.utfsm.POJO.APPEF01;
import cl.utfsm.POJO.ASIGNATURA;
import cl.utfsm.POJO.SucursalBanco;
import cl.utfsm.POJO.APREF19;
import cl.utfsm.POJO.APREF24;
import cl.utfsm.POJO.Banco;
import cl.utfsm.POJO.Ciudad;
import cl.utfsm.POJO.Comuna;
import cl.utfsm.POJO.PRESF200;
import cl.utfsm.base.util.Util;
import cl.utfsm.conexion.ConexionAs400ValePago;
import cl.utfsm.scaB.datos.ContratoAyudantiaDao;
import descad.cliente.Ingreso_Documento;
import descad.cliente.Ingreso_Documentos;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw21DTO;
import descad.presupuesto.Presw25DTO;

public class ModuloContratoAyudantiaB {
	ContratoAyudantiaDao AyudantiaDao;

	public ContratoAyudantiaDao getContratoAyudantiaDao() {
		return AyudantiaDao;
	}

	public void setContratoAyudantiaDao(ContratoAyudantiaDao AyudantiaDao) {
		this.AyudantiaDao = AyudantiaDao;
	}
	
	public List<Presw18DTO> getListaSede( HttpServletRequest req) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
	
			PreswBean preswbean = null;
			Collection<Presw18DTO> lista = null;
			List<Presw18DTO> list = new ArrayList<Presw18DTO>();
			String nombre = "";
			preswbean = new PreswBean("SED",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
				   for (Presw18DTO ss : lista){
					   Presw18DTO presw18DTO = new Presw18DTO();
					   presw18DTO.setNummes(ss.getNummes());
					   presw18DTO.setDesuni(ss.getDesuni());
					   list.add(presw18DTO);
				   }	
			   
		return list;
		}
	
	public List<PRESF200> getOrganizacion(){
		// Luego cambiar a una conexion as para Contratacion
		  	ConexionAs400ValePago con = new ConexionAs400ValePago(); 
	    	Connection conexion = con.getConnection(); 		 
	        List<PRESF200> lista = new ArrayList<PRESF200>();
	     
	     try {
	        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODORG, DESORG " +
	                                                                      " FROM USMMBP.PRESF200 "); 
	        ResultSet res = sent.executeQuery();

		    while (res.next()) { 
		    	PRESF200 presf200= new PRESF200();
		    	presf200.setCODORG(res.getString(1).trim()); // 0 codigo de la organizaci�n;
		    	presf200.setDESORG(res.getString(2).trim()); // 1 nombre de la organzaci�n;
		    	lista.add(presf200);
		     }
		     res.close();
		     sent.close();
		     conexion.close();
		    }
		    catch (SQLException e ) {
		      System.out.println("Error en moduloContratoAyudantia.getDatosOrganizacion : " + e );
		    }
		    return lista;
	}
/*	public  List<Presw18DTO> getOrganizacion(int rutUsuario, String dv) throws Exception{
			Collection<Presw18DTO> listaPresw18_AS400 = null; 
		     PreswBean preswbean = new PreswBean("PR1", 0, 0, 0, 0, rutUsuario);
			//preswbean.setTipcue(tipoCuentas);
			 preswbean.setDigide(dv);
			 listaPresw18_AS400 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			 List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
				
			 for (Presw18DTO ss : lista){
				   Presw18DTO presw18DTO = new Presw18DTO();
				   presw18DTO.setNomtip(ss.getNomtip());
				   presw18DTO.setDesuni(ss.getDesuni());
				   lista.add(presw18DTO);
			   }	
		    return lista;
	}
	*/
	public List<APREF19> getTipoAyudantia(){
		// Luego cambiar a una conexion as para Contratacion
		  ConexionAs400ValePago con = new ConexionAs400ValePago(); 
	      Connection conexion = con.getConnection(); 		 
	      List<APREF19> lista = new ArrayList<APREF19>();
	     
	     try {
	        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODAYU, DESAYU, CANNIV " +
	                                                                      " FROM REMUNERABT.APREF19 "); 
	        ResultSet res = sent.executeQuery();

		    while (res.next()) { 
		    	APREF19 apref19 = new APREF19();
		    	apref19.setCODAYU(res.getString(1).trim()); // 1 codigo de la ayudant�a;
		    	apref19.setDESAYU(res.getString(2).trim()); // 2 nombre de la ayudant�a;
		    	apref19.setCANNIV(res.getInt(3)); //3 canniv
			    lista.add(apref19);
		    }
		     res.close();
		     sent.close();
		     conexion.close();
		    }
		    catch (SQLException e ) {
		      System.out.println("Error en moduloContratoAyudantia.getTipoAyudantia : " + e );
		    }
		    return lista;
	}
	
	public String getNombreAyudantia(String codAyu){
		// Luego cambiar a una conexion as para Contratacion
		  ConexionAs400ValePago con = new ConexionAs400ValePago(); 
	      Connection conexion = con.getConnection(); 		 
	      String nombre = "";
	     
	     try {
	        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT DESAYU " +
	        		                                                      " FROM REMUNERABT.APREF19 " +
	        		                                                      " WHERE CODAYU = '" + codAyu +"'"); 
	        ResultSet res = sent.executeQuery();

		    while (res.next()) { 
		    	nombre = res.getString(1).trim(); // 2 nombre de la ayudant�a;
		    }
		    res.close();
		    sent.close();
		    conexion.close();
		    }
		    catch (SQLException e ) {
		      System.out.println("Error en moduloContratoAyudantia.getNombreAyudantia : " + e );
		    }
		    return nombre;
	}
	
	public List<APREF24> getListaSemetre(int codsuc){
		// Luego cambiar a una conexion as para Contratacion
		  ConexionAs400ValePago con = new ConexionAs400ValePago(); 
	      Connection conexion = con.getConnection(); 		 
	      List<APREF24> lista = new ArrayList<APREF24>();
	     
	     try {
	    	String query = 	" SELECT ANOAYU, CODSUC, SEMEST, DIAINI, MESINI, DIAFIN, MESFIN, CANSEM" +
							" FROM REMUNERABT.APREF24 WHERE CODSUC = " + codsuc +""+
							" ORDER BY ANOAYU desc";
	    	//System.out.println(query);
	        PreparedStatement sent = con.getConnection().prepareStatement(query); 
	        ResultSet res = sent.executeQuery();
	        int i = 0;
		    while (res.next()) { 
		    	i++;
		    	APREF24 apref24 = new APREF24();
		    	apref24.setANOAYU(res.getInt(1)); //1 ANOAYU
		    	apref24.setCODSUC(res.getInt(2)); //2 CODSUC
		    	apref24.setSEMEST(res.getInt(3)); //3 SEMEST
		    	apref24.setDIAINI(res.getInt(4)); //4 DIAINI
		    	apref24.setMESINI(res.getInt(5)); //5 MESINI
		    	apref24.setDIAFIN(res.getInt(6)); //6 DIAFIN
		    	apref24.setMESFIN(res.getInt(7)); //7 MESFIN
		    	apref24.setCANSEM(res.getInt(8)); //8 CANSEM
		    	
		    	lista.add(apref24);
		    	if(i == 2)
		    		break;
		    }
		     res.close();
		     sent.close();
		     conexion.close();
		    }
		    catch (SQLException e ) {
		      System.out.println("Error en moduloContratoAyudantia.getListaSemetre : " + e );
		    }
		    return lista;
	}

	public List<APPEF01> getListaProfesor(){
		// Luego cambiar a una conexion as para Contratacion
		  ConexionAs400ValePago con = new ConexionAs400ValePago(); 
	      Connection conexion = con.getConnection(); 		 
	      List<APPEF01> lista = new ArrayList<APPEF01>();
	     
	     try {
	        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT RUTIDE, DIGIDE, APEPAT, APEMAT, NOMBRE " +
	                                                                      " FROM REMUNERABT.APPEF01 "); 
	        ResultSet res = sent.executeQuery();

		    while (res.next()) { 
		    	APPEF01 appef01= new APPEF01();
		    	appef01.setRUTIDE(res.getInt(1)); // 0 rut del profesor
		    	appef01.setDIGIDE(res.getString(2).trim()); // 1 dv del profesor
		    	appef01.setAPEPAT(res.getString(3).trim()); // 1 apellido PaTERNO
		    	appef01.setAPEMAT(res.getString(4).trim()); // 1 apellido maTERNO
		    	appef01.setNOMBRE(res.getString(5).trim()); // 1 apellido NOMBRE
		    	lista.add(appef01);
		     }
		     res.close();
		     sent.close();
		     conexion.close();
		    }
		    catch (SQLException e ) {
		      System.out.println("Error en moduloContratoAyudantia.getListaProfesor : " + e );
		    }
		    return lista;
	}
	public List<ASIGNATURA> getListaAsignatura(int sucur){
		// Luego cambiar con lo definitivo
	   	 ConexionAs400ValePago con = new ConexionAs400ValePago();
	     List<ASIGNATURA> lista = new ArrayList<ASIGNATURA>();
	     
	     try {
	    	String query =  " SELECT ASIALF, ASINUM, DESASI  " +
            				" FROM REMUNERABT.APPEF18 " +
	    					" WHERE  CODSUC = " + sucur +
	    					" AND DESASI != '.'" +
                            " ORDER BY DESASI";
	        PreparedStatement sent = con.getConnection().prepareStatement(query); 
	        ResultSet res = sent.executeQuery();

		    while (res.next()) { 
		    	ASIGNATURA appef18= new ASIGNATURA();
		    	appef18.setASIALF(res.getString(1).trim()); // 0 codigo de la asignatura;
		    	appef18.setASINUM(res.getInt(2)); // 1 codigo numerico;
		    	appef18.setDESASI(res.getString(3).trim()); // 2 nombre asignatura
		    	lista.add(appef18);
		     }
		     res.close();
		     sent.close();
		     con.getConnection().close();
		    }
		    catch (SQLException e ) {
		      System.out.println("Error en moduloContratoAyudantia.getAsignatura : " + e );
		    }
		    return lista;
	}
	
	  public int getRegistraSolicitud(HttpServletRequest req, int rut, String dv){
		    PreswBean preswbean = new PreswBean("SA5", 0,0,0,0, rut);
		    preswbean.setRutide(rut);
			preswbean.setDigide(dv);
		   
		  	int numSol = 0;
			List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
			listCocow36DTO = this.getCreaCocow36(req, rut, dv, "SA5");
			Ingreso_Documento ingresoDocumento = new Ingreso_Documento("SA5",rut,dv);
			
			numSol = ingresoDocumento.Ingresar_Documento(listCocow36DTO);
			return numSol;
	  }
	  public boolean getActualizarSolicitud(HttpServletRequest req, int rut, String dv, int numDoc){
		    PreswBean preswbean = new PreswBean("SA5", 0,0,0,0, rut);
		    preswbean.setNumdoc(numDoc);
			preswbean.setRutide(rut);
			preswbean.setDigide(dv);
		   
		  	boolean registra = false;
			List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
			listCocow36DTO = this.getCreaCocow36(req, rut, dv, "SA5");
			Ingreso_Documento ingresoDocumento = new Ingreso_Documento("SA5",rut,dv);
			
			registra = ingresoDocumento.Actualizar_Documento(listCocow36DTO);
			return registra;
	  }
	  public List<Cocow36DTO> getCreaCocow36(HttpServletRequest req, int rutide, String digide, String tipoProceso){
		    Collection<Presw21DTO> listaSubtipoAyudantia = (Collection<Presw21DTO>) req.getSession().getAttribute("listaSubtipoAyudantia");
		    int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
				
		    long rutSol = Util.validaParametro(req.getParameter("rutnum"),0);
			String dvSol = Util.validaParametro(req.getParameter("dvrut"),"");
			String codayu = Util.validaParametro(req.getParameter("codayu"),"");
			String codNivel = Util.validaParametro(req.getParameter("codNivel"),"");
			long valorHora = Util.validaParametro(req.getParameter("valHora"), 0);
			long valorMes = Util.validaParametro(req.getParameter("valorMes"), 0);
			long valorCon = Util.validaParametro(req.getParameter("valorCon"), 0);
			String codOrg = Util.validaParametro(req.getParameter("codOrg"),"");
			long sede = Util.validaParametro(req.getParameter("sede"),0);
			String fechaSolicitudIng = Util.validaParametro(req.getParameter("fechaSolicitudIng"),"");
			long fechaSol = (!fechaSolicitudIng.trim().equals(""))?Long.parseLong(fechaSolicitudIng.substring(6) + fechaSolicitudIng.substring(3,5) + fechaSolicitudIng.substring(0,2)):0;
			String fechaPeriodoSemInicial = Util.validaParametro(req.getParameter("fechaPeriodoSemInicial"),"");
			String fechaPeriodoSemFinal = Util.validaParametro(req.getParameter("fechaPeriodoSemFinal"),"");
			String tipTrabajo = Util.validaParametro(req.getParameter("tipTrabajo"),"");
			long rutProf1 = Util.validaParametro(req.getParameter("rutProf1"),0);
			String dvProf1 = Util.validaParametro(req.getParameter("digProf1"),"");
			
			
			long canHora = Util.validaParametro(req.getParameter("cantHora"),0);
			long tipContrato = Util.validaParametro(req.getParameter("tipoContrato"),0);
			long semAyu = Util.validaParametro(req.getParameter("sem"),0);
			long a�oAyu = Util.validaParametro(req.getParameter("annio"),0);
			
			int curso = Util.validaParametro(req.getParameter("curso"), 0);
			int carrera = Util.validaParametro(req.getParameter("carrera"), 0);
			long fechaIni = (!fechaPeriodoSemInicial.trim().equals(""))?Long.parseLong(fechaPeriodoSemInicial.substring(6) + fechaPeriodoSemInicial.substring(3,5) + fechaPeriodoSemInicial.substring(0,2)):0;
			long fechaFin = (!fechaPeriodoSemFinal.trim().equals(""))?Long.parseLong(fechaPeriodoSemFinal.substring(6) + fechaPeriodoSemFinal.substring(3,5) + fechaPeriodoSemFinal.substring(0,2)):0;
			String fechaEfecIni = Util.validaParametro(req.getParameter("fechaEfecIni"),"");
			String fechaEfecFin = Util.validaParametro(req.getParameter("fechaEfecFin"),"");
			
			long fecEIni = (!fechaEfecIni.trim().equals(""))?Long.parseLong(fechaEfecIni.substring(6) + fechaEfecIni.substring(3,5) + fechaEfecIni.substring(0,2)):0;
			long fecEFin = (!fechaEfecFin.trim().equals(""))?Long.parseLong(fechaEfecFin.substring(6) + fechaEfecFin.substring(3,5) + fechaEfecFin.substring(0,2)):0;
			
			long rutProf2 = Util.validaParametro(req.getParameter("rutProf2"),0);
			String dvProf2 = Util.validaParametro(req.getParameter("digProf2"),"");
			long rutProf3 = Util.validaParametro(req.getParameter("rutProf3"),0);
			String dvProf3 = Util.validaParametro(req.getParameter("digProf3"),"");
			String asigAlf1 = Util.validaParametro(req.getParameter("asialf1"),"");
			long asigNum1 = Util.validaParametro(req.getParameter("asinum1"),0);
			String asigAlf2 = Util.validaParametro(req.getParameter("asialf2"),"");
			long asigNum2 = Util.validaParametro(req.getParameter("asinum2"),0);
			String asigAlf3 = Util.validaParametro(req.getParameter("asialf3"),"");
			long asigNum3 = Util.validaParametro(req.getParameter("asinum3"),0);
			String idDigi = "";
            long totalMes = Util.validaParametro(req.getParameter("totalMes"), 0);
         //   long totalContrato = Util.validaParametro(req.getParameter("totalContrato"), 0);

			String paralelo1 = Util.validaParametro(req.getParameter("paral1"),"0");
			String paralelo2 = Util.validaParametro(req.getParameter("paral2"),"0");
			String paralelo3 = Util.validaParametro(req.getParameter("paral3"),"0");
			int numDoc = Util.validaParametro(req.getParameter("numDoc"), 0);
			String nivaca = Util.validaParametro(req.getParameter("nivaca"),"");
			long totcon = 0;
			DateTool fechaActual = new DateTool();
			
			fechaActual = new DateTool();
			String dia = fechaActual.getDay()+"";
			if(dia.length() == 1)
				dia = "0" + dia;
			String mes = (fechaActual.getMonth()+1)+"";
			if(mes.length()== 1)
				mes = "0" + mes;
			int fechaIng = Integer.parseInt(String.valueOf(fechaActual.getYear()) + mes + dia);
			
			
			List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
			
			String tiping = tipoProceso;
			String nomcam = "";
			long valnu1 = 0;
			long valnu2 = 0;
			String valalf = "";
			long rutUsu = 0;
			
			/* prepara archivo para grabar, llenar listCocow36DTO*/
			nomcam = "RUTIDE";
		    valnu1 = rutSol;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "DIGIDE";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = dvSol;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "CODAYU";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = codayu;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "CODNIV";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = codNivel;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "VALHOR";
		    valnu1 = valorHora;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "VALCON";
		    valnu1 = valorCon;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "CODUNI";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "SUCUR";
		    valnu1 = sede;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "FECSOL";
		    valnu1 = fechaSol;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "FECINI";
		    valnu1 = fechaIni;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "FECFIN";
		    valnu1 = fechaFin;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "NOMTRA";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = tipTrabajo;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "RUTPRO";
		    valnu1 = rutProf1;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "DIGPRO";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = dvProf1;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		/*    nomcam = "PRORES";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = nombreResp;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );*/
		    
		    nomcam = "CANHOR";
		    valnu1 = canHora;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "VALMES";
		    valnu1 = valorMes;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		 
		    
		    nomcam = "VALCON";
		    valnu1 = valorCon;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "NIVACA";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = nivaca;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		     
		    nomcam = "TIPCON";
		    valnu1 = tipContrato;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "SEMEST";
		    valnu1 = semAyu;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "ANOAYU";
		    valnu1 = a�oAyu;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "ASIALF1";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = asigAlf1;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "ASINUM1";
		    valnu1 = asigNum1;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "ASIALF2";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = asigAlf2;;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "ASINUM2";
		    valnu1 = asigNum2;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "ASIALF3";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = asigAlf3;;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "ASINUM3";
		    valnu1 = asigNum3;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "IDDIGI";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = rutUsuario + dv;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "FECING";
		    valnu1 = fechaIng;       
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "FEEFIN";
		    valnu1 = fecEIni;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "FEEFFI";
		    valnu1 = fecEFin;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "RUTPRO2";
		    valnu1 = rutProf2;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "DIGPRO2";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = dvProf2;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "RUTPRO3";
		    valnu1 = rutProf3;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "DIGPRO3";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = dvProf3;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "PARALE1";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = paralelo1;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "PARALE2";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = paralelo2;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "PARALE3";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = paralelo3;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
		    
		    nomcam = "CODORG";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = codOrg;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
			   
		    String par1 = "";
		    String par2 = "";
		    String par3 = "";
		    if(paralelo1.length() == 1 ) par1 = "00" + paralelo1;
		    if(paralelo1.length() == 2 ) par1 = "0" + paralelo1;
		    if(paralelo1.length() > 2 ) par1 = paralelo1.substring(0,2);
		    if(paralelo2.length() == 1 ) par2 = "00" + paralelo2;
		    if(paralelo2.length() == 2 ) par2 = "0" + paralelo2;
		    if(paralelo2.length() > 2 ) par2 = paralelo2.substring(0,2);
		    if(paralelo3.length() == 1 ) par3 = "00" + paralelo3;
		    if(paralelo3.length() == 2 ) par3 = "0" + paralelo3;
		    if(paralelo3.length() > 2 ) par3 = paralelo3.substring(0,2);
		    if(listaSubtipoAyudantia != null && listaSubtipoAyudantia.size() > 0){ 
		    for(Presw21DTO ss: listaSubtipoAyudantia){
		    	Formatter fmt = new Formatter();
		     	nomcam = "DETALLE";
			    valnu1 = asigNum1;
			    valnu2 = ss.getValor2();
			    if(asigNum1 > 0 )
			    valalf = ss.getGlosa2().substring(0,3) + asigAlf1.substring(0,3) + par1;
			    rutUsu = ss.getValor1();
			    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
			    
			    if(asigNum2 > 0) {
					nomcam = "DETALLE";
				    valnu1 = asigNum2;
				    valnu2 = ss.getValor3();
				    valalf = ss.getGlosa2().substring(0,3) + asigAlf2.substring(0,3) + par2;
				    rutUsu = ss.getValor1();
				    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
			   }
			    if(asigNum3 > 0) {					  
					nomcam = "DETALLE";
				    valnu1 = asigNum3;
				    valnu2 = ss.getValor4();
				    valalf = ss.getGlosa2().substring(0,3) + asigAlf3.substring(0,3) + par3;
				    rutUsu = ss.getValor1();
				    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, rutUsu );
			    }
		    	
		    }
		   }
		  
		 
		    
		    return listCocow36DTO;
			}
	  
	  public List<Cocow36DTO> getAgregaLista(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf, long rutUsu){
			 Cocow36DTO cocow36DTO = new Cocow36DTO();
			 cocow36DTO.setTiping(tiping);
			 cocow36DTO.setNomcam(nomcam);
			 cocow36DTO.setValnu1(valnu1);
			 cocow36DTO.setValnu2(valnu2);
			 cocow36DTO.setValalf(valalf);
			 cocow36DTO.setRutusu(rutUsu);
			 lista.add(cocow36DTO);
			 return lista;
	 }
	  
	  public boolean getActualizaSolicitud(HttpServletRequest req, int rutide, String digide, int numSol){
			boolean registra = false;
			List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
			listCocow36DTO = this.getCreaCocow36(req, rutide, digide,"SA8");
			Ingreso_Documento ingresoDocumento = new Ingreso_Documento("SA8",rutide,digide);
			ingresoDocumento.setNumdoc(numSol);
						
			registra = ingresoDocumento.Actualizar_Documento(listCocow36DTO);
			return registra;
	 }
	  
	  public boolean getActualizaEstadoSolicitud(int numSol, int rutUsuario, String digide, String tipo, String estado){
			PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
			preswbean.setNumdoc(numSol);
			preswbean.setRutide(rutUsuario);
			preswbean.setDigide(digide);
		    preswbean.setTipcue(estado);
		    return preswbean.ingreso_presw19();
   	  } 

	  public String formateoRut(String rut) {
		  int cont = 0;
	        String format;
	        rut = rut.replace(".", "");
	        rut = rut.replace("-", "");
	        format = "-" + rut.substring(rut.length() - 1);
	        for (int i = rut.length() - 2; i >= 0; i--) {
	            format = rut.substring(i, i + 1) + format;
	            cont++;
	            if (cont == 3 && i != 0) {
	                format = "." + format;
	                cont = 0;
	            }
	        }
	       return format;
	}
	  
	  public List<SucursalBanco> getListaSucursalBanco(long codban) throws Exception {
		     ConexionAs400ValePago con = new ConexionAs400ValePago(); 
		     Connection conexion = con.getConnection(); 		 
		     List<SucursalBanco> lista = new ArrayList<SucursalBanco>();
		 	 
		     try {
		    	String query = 	" SELECT CODBAN, SUCBAN, DESSUC " +
				                " FROM REMUNERABT.APPEF04  " +
				                " WHERE CODBAN = " + codban +
				                " ORDER BY DESSUC";
	//	    	System.out.println(""+query);
		        PreparedStatement sent = con.getConnection().prepareStatement(query); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			    	SucursalBanco banco = new SucursalBanco();
			    	banco.setCodBan(res.getInt(1)); // 0 codigo del Banco;
			    	banco.setSucBan(res.getInt(2)); 	// 1 codigo de la sucursal;
			    	banco.setDesSuc(res.getString(3).trim()); // 2 Nombre de la sucursal
			        lista.add(banco);
			     }
			     res.close();
			     sent.close();
			     conexion.close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoAyudantia.getListaSucursalBanco : " + e );
			    }
			    return lista;
	  }
	  
	  
	  public List<Banco> getListaBanco() throws Exception {
			ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	List<Banco> lista = new ArrayList<Banco>();
		 	 
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODBAN, DESBAN, INDVIG " +
		                                                                      " FROM USMMBP.COCOF09 " +
		                                                                      " WHERE INDVIG = ''"); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			    	Banco banco = new Banco();
			    	banco.setCodBan(res.getInt(1)); // 0 codigo del Banco;
			    	banco.setDesBan(res.getString(2).trim()); // Nombre del banco;
			        lista.add(banco);
			     }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoAyudantia.getListaBanco : " + e );
			    }
			    return lista;
	  }
	  
	  public String getNombreBanco(long codBan) throws Exception {
			ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	String nombre = "";
		 	 
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT DESBAN " +
		                                                                      " FROM USMMBP.COCOF09 " +
		                                                                      " WHERE INDVIG = ''" +
		                                                                      " AND CODBAN = " + codBan); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			    	nombre = res.getString(1).trim(); // Nombre del banco;
			     }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoAyudantia.getNombreBanco : " + e );
			    }
			    return nombre;
	  }
	  
	  public String getNombreSucBanco(long codBan, long sucBan) throws Exception {
			ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	String nombre = "";
		 	 
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT DESSUC " +
				                											  " FROM REMUNERABT.APPEF04  " +
				                                                              " WHERE CODBAN = " + codBan +
				                                                              " AND SUCBAN = " + sucBan); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			    	nombre = res.getString(1).trim(); // Nombre del Sucursal Banco;
			     }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoAyudantia.getNombreSucBanco : " + e );
			    }
			    return nombre;
	  }
	  
	  public List<Comuna>  getComunas(){
			// Luego cambiar a una conexion as para Contratacion
		  ConexionAs400ValePago con = new ConexionAs400ValePago(); 
	      Connection conexion = con.getConnection(); 		 
	      List<Comuna> lista = new ArrayList<Comuna>();
		 	
		     
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT COMUNA, NOMCOM " +
		                                                                      " FROM REMUNERABT.APPEF02  "); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			       Comuna comuna = new  Comuna();
			       comuna.setCodigoComuna(res.getInt(1)); 			// 0 codigo de la COMUNA;
			       comuna.setNomComuna(res.getString(2).trim()); 	// 1 nombre de la COMUNA;
			       lista.add(comuna);
			     }
			     res.close();
			     sent.close();
			     conexion.close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoAyudantia.getComuna : " + e );
			    }
			    return lista;
		}
	  public List<Ciudad> getCiudades(){
			// Luego cambiar a una conexion as para Contratacion
		  ConexionAs400ValePago con = new ConexionAs400ValePago(); 
	      Connection conexion = con.getConnection(); 		 
	      List<Ciudad> lista = new ArrayList<Ciudad>();
		     
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CIUDAD, NOMCIU " +
		                                                                      " FROM REMUNERABT.APPEF03  "); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			    	Ciudad ciudad = new Ciudad();
			    	ciudad.setCodigoCiudad(res.getInt(1));        // 0 codigo de la ciudad;
			    	ciudad.setNomCiudad(res.getString(2).trim()); // 1 nombre de la ciudad;
			    	lista.add(ciudad);
			     }
			     res.close();
			     sent.close();
			     conexion.close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoAyudantia.getCiudades : " + e );
			    }
			    return lista;
		}

	  public List<Presw18DTO> getConsultaAutoriza(int rutnum, String dv){
		  	PreswBean preswbean = null;
			Collection<Presw18DTO> lista = null;
			List<Presw18DTO> listaAutoriza =  new ArrayList<Presw18DTO>();
			String nombre = "";
			if(rutnum > 0){                // MAA este proceso sigue igual no cambio
				preswbean = new PreswBean("PAU",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
				lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
				   for (Presw18DTO ss : lista){
					   Presw18DTO presw18DTO = new Presw18DTO();
					   presw18DTO.setIndprc(ss.getIndprc());
					   listaAutoriza.add(presw18DTO);
				   }
						
			}
		
		 return listaAutoriza;
		}
	  public int calculoDiasTrabajados(String fechaIni, String fechaFin) {
			 
		  
		  StringTokenizer stM = new StringTokenizer(fechaFin,"/");
		  int diaM = Integer.parseInt(stM.nextToken());    
		  int mesM = Integer.parseInt(stM.nextToken());    
		  int anoM = Integer.parseInt(stM.nextToken());    
		  
		  
		  StringTokenizer st = new StringTokenizer(fechaIni,"/");
		  int diam = Integer.parseInt(st.nextToken());    
		  int mesm = Integer.parseInt(st.nextToken());    
		  int anom = Integer.parseInt(st.nextToken());    
		  
		  int FMAYOR = Integer.parseInt(anoM+""+mesM+""+diaM);
		  int FMENOR = Integer.parseInt(anom+""+mesm+""+diam);
		  
		  int cont = 0;
		  
		  if(FMAYOR >= FMENOR){
		     int mesMaux = mesM;
		     int diaMaux = diaM;
		      
		     cont = 0;
		  
		     while(anom<=anoM){
		     
		      if(anom<anoM){
		        mesM = 12;
		        diaM = 31; //31;
		      }else{
		         mesM = mesMaux;
		         diaM = diaMaux;
		      }
		     
		     while(mesm <= mesM){
		      int DiasDelMes = 0;
		      
		       if(mesm == mesM){
		          DiasDelMes = diaM;
		       }else{
		    	 if (diam == 1) DiasDelMes = 30;
		    	 else DiasDelMes = DiasDelMes(anom,mesm); // este metodo se llama solo si el d�a del periodo no parte en 1
		       } 
		        
		        while(diam <= DiasDelMes){
		         cont++;
		         diam++;
		        }
		         diam = 1;
		         mesm ++;
		       }   
		      
		       mesm = 1;
		       anom++;
		     }
		      
		          
		        
		  System.out.println ("Numero de dias contados -> "+(cont-1));
		  System.out.println ("Con el dia de inicio incluido serian -> "+cont);
		     
		  }else{
		     System.out.println ("Error....!!La fecha Menor supera a la fecha Mayor");
		  }
		  
	  return cont;
	}
	  public static int DiasDelMes(int ano, int mes){
			 int ndias = 0;
			 int f = 0;
			
			  int an = ano;
			
			  if(an % 4 == 0){
			      f = 29;
			     }else{
			      f = 28;
			     }
			 switch (mes) {
			    case 1:   ndias = 31;  break;
			    case 2:   ndias = f;   break;
			    case 3:   ndias = 31;  break;
			    case 4:   ndias = 30;  break;
			    case 5:   ndias = 31;  break;
			    case 6:   ndias = 30;  break;
			    case 7:   ndias = 31;  break;
			    case 8:   ndias = 31;  break;
			    case 9:   ndias = 30;  break;
			    case 10:  ndias = 31;  break;
			    case 11:  ndias = 30;  break;
			    case 12:  ndias = 31;  break;
			    }
			 return ndias;
			}
	  
	  public String  getNombreComuna(long comuna){
			ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	String nombre = "";
		 	
		     
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT NOMCOM " +
		                                                                      " FROM REMUNERABT.APPEF02 " +
		                                                                      " WHERE COMUNA = " + comuna); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			      nombre = res.getString(1).trim();
			    }
			    res.close();
			    sent.close();
			    con.getConnection().close();
			 }
			 catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocenteB.getNombreComuna : " + e );
			 }
			return nombre;
		}
	  
	  public String  getNombreCiudad(long ciudad){
			ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	String nombre = "";
		 	
		     
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT NOMCIU " +
              														   	  " FROM REMUNERABT.APPEF03 " +
              														   	  " WHERE CIUDAD = " + ciudad);  
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			      nombre = res.getString(1).trim();
			    }
			    res.close();
			    sent.close();
			    con.getConnection().close();
			 }
			 catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocenteB.getNombreCiudad : " + e );
			 }
			return nombre;
	  }
	  
}
