package cl.utfsm.scdB.modulo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import cl.utfsm.POJO.APPEF01;
import cl.utfsm.POJO.APREF07;
import cl.utfsm.POJO.APREF08;
import cl.utfsm.POJO.APREF19;
import cl.utfsm.POJO.APREF23;
import cl.utfsm.POJO.APREF24;
import cl.utfsm.POJO.ASIGNATURA;
import cl.utfsm.POJO.Banco;
import cl.utfsm.POJO.Ciudad;
import cl.utfsm.POJO.Comuna;
import cl.utfsm.POJO.GradoAcademico;
import cl.utfsm.POJO.PRESF200;
import cl.utfsm.POJO.SucursalBanco;
import cl.utfsm.POJO.Titulo;
import cl.utfsm.base.util.Util;
import cl.utfsm.conexion.ConexionAs400ValePago;
import cl.utfsm.scdB.datos.ContratoDocenteDao;
import descad.cliente.Ingreso_Documento;
import descad.cliente.Ingreso_Documentos;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw25DTO;

public class ModuloContratoDocenteB {
	ContratoDocenteDao contratoDocenteDao;

	public ContratoDocenteDao getContratoDocenteDao() {
		return contratoDocenteDao;
	}

	public void setContratoDocenteDao(ContratoDocenteDao contratoDocenteDao) {
		this.contratoDocenteDao = contratoDocenteDao;
	}
	
	public List<Presw18DTO> getListaSede( HttpServletRequest req) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
	
		PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		List<Presw18DTO> list = new ArrayList<Presw18DTO>();
		String nombre = "";
		preswbean = new PreswBean("SED",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			   for (Presw18DTO ss : lista){
				   Presw18DTO presw18DTO = new Presw18DTO();
				   presw18DTO.setNummes(ss.getNummes());
				   presw18DTO.setDesuni(ss.getDesuni());
				   list.add(presw18DTO);
			   }	
		   
	return list;
	}

	public List<PRESF200> getOrganizacion(){
		// Luego cambiar a una conexion as para Contratacion
	   	 ConexionAs400ValePago con = new ConexionAs400ValePago();
	     List<PRESF200> lista = new ArrayList<PRESF200>();
	     
	     try {
	        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODORG, DESORG " +
	                                                                      " FROM USMMBP.PRESF200 "); 
	        ResultSet res = sent.executeQuery();

		    while (res.next()) { 
		    	PRESF200 presf200= new PRESF200();
		    	presf200.setCODORG(res.getString(1).trim()); // 0 codigo de la organizaci�n;
		    	presf200.setDESORG(res.getString(2).trim()); // 1 nombre de la organzaci�n;
		    	lista.add(presf200);
		     }
		     res.close();
		     sent.close();
		     con.getConnection().close();
		    }
		    catch (SQLException e ) {
		      System.out.println("Error en moduloContratoDocenteB.getDatosOrganizacion : " + e );
		    }
		    return lista;
	}
	
	public List<APPEF01> getListaProfesor(){
		// Luego cambiar a una conexion as para Contratacion
	   	 ConexionAs400ValePago con = new ConexionAs400ValePago();
	   	 List<APPEF01> lista = new ArrayList<APPEF01>();
	     
	     try {
	        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT RUTIDE, DIGIDE, APEPAT, APEMAT, NOMBRE " +
	                                                                      " FROM REMUNERABT.APPEF01 "); 
	        ResultSet res = sent.executeQuery();

		    while (res.next()) { 
		    	APPEF01 appef01= new APPEF01();
		    	appef01.setRUTIDE(res.getInt(1)); // 0 rut del profesor
		    	appef01.setDIGIDE(res.getString(2).trim()); // 1 dv del profesor
		    	appef01.setAPEPAT(res.getString(3).trim()); // 1 apellido PaTERNO
		    	appef01.setAPEMAT(res.getString(4).trim()); // 1 apellido maTERNO
		    	appef01.setNOMBRE(res.getString(5).trim()); // 1 apellido NOMBRE
		    	lista.add(appef01);
		     }
		     res.close();
		     sent.close();
		     con.getConnection().close();
		    }
		    catch (SQLException e ) {
		      System.out.println("Error en moduloContratoDocenteB.getListaProfesor : " + e );
		    }
		    return lista;
	}
	
	 public List<Titulo> getListaTitulo() throws Exception {
			ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	List<Titulo> lista = new ArrayList<Titulo>();
		 	 
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODGRA, CODTIT, DESTIT " +
		                                                                      " FROM REMUNERABT.APREF10  "); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			    	Titulo titulo = new Titulo();
			    	titulo.setCodGra(res.getInt(1)); // 0 codigo del grado;
			    	titulo.setCodTit(res.getInt(2)); 	// 1 codigo del titulo;
			    	titulo.setDesTit(res.getString(3).trim()); // 2 Nombre del titulo
			        lista.add(titulo);
			     }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocenteB.getListaTitulo : " + e );
			    }
			    return lista;
	  }
	 
	  public String  getNombreTitulo(long codGra, long codTit){
			ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	String nombre = "";
		 	 
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT DESTIT " +
		                                                                      " FROM REMUNERABT.APREF10 " +
		        		                                                      " WHERE CODGRA = " + codGra +
		        		                                                      " AND CODTIT = " + codTit);
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			      nombre = res.getString(1).trim();
			    }
			    res.close();
			    sent.close();
			    con.getConnection().close();
			 }
			 catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocenteB.getNombreTitulo : " + e );
			 }
			return nombre;
	  }		
	 
	 public List<APREF07> getListaPlanta() throws Exception {
			ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	List<APREF07> lista = new ArrayList<APREF07>();
		 	 
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODPLA, DESPLA " +
		                                                                      " FROM REMUNERABT.APREF07  "); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			    	APREF07 planta = new APREF07();
			    	planta.setCodPla(res.getInt(1)); // 0 codigo de la  planta;
			    	planta.setDesPla(res.getString(2)); 	// 1 nombre de la planta;
			        lista.add(planta);
			     }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocenteB.getListaPlanta : " + e );
			    }
			    return lista;
	  }
	 
	 public List<APREF08> getListaCargo(long codPla) throws Exception {
			ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	List<APREF08> lista = new ArrayList<APREF08>();
		 	 
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODPLA, CODCAR, DESCAR " +
		                                                                      " FROM REMUNERABT.APREF08 " +
		                                                                      " WHERE CODPLA = " + codPla); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			    	APREF08 cargo = new APREF08();
			    	cargo.setCodPla(res.getInt(1)); // 0 codigo del Planta;
			    	cargo.setCodCar(res.getInt(2)); 	// 1 codigo del Cargo;
			    	cargo.setDesCar(res.getString(3).trim()); // 2 Nombre del Cargo
			        lista.add(cargo);
			     }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocenteB.getListaCargo : " + e );
			    }
			    return lista;
	  }
	 
	 public Vector getListaValoresHora(int sede, String tipTrabajo, String tipHora) throws Exception {
			ConexionAs400ValePago con = new ConexionAs400ValePago();
			Vector vec = new Vector();
		 	 
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT VALNOM, VALMIN, VALMAX, VALNI4, VALNU5 " +
		                                                                      " FROM REMUNERABT.APREF23 " +
		                                                                      " WHERE SUCUR = " + sede +
		                                                                      " AND DOCACA = '" + tipTrabajo + "'" +
		                                                                      " AND TIPSES = '" + tipHora + "'"); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) {
			    	vec.add(res.getInt(1)); // 0-VALNOM-VALNIV1 
			    	vec.add(res.getInt(2)); // 1-VALMIN-VALNIV2 
			    	vec.add(res.getInt(3)); // 2-VALMAX-VALNIV3 
			    	vec.add(res.getInt(4)); // 3-VALNI4-VALNIV4 
			    	vec.add(res.getInt(5)); // 4-VALNU5-VALNIV5 
			    }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocente.getListaValoresHora : " + e );
			    }
			return vec;
	  }
	 
	 public Vector getListaValoresMINMAX(int sede, String tipTrabajo, String tipHora) throws Exception {
			ConexionAs400ValePago con = new ConexionAs400ValePago();
			Vector vec = new Vector();
		 	 
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT VALMIN, VALMAX " +
		                                                                      " FROM REMUNERABT.APREF23 " +
		                                                                      " WHERE SUCUR = " + sede +
		                                                                      " AND DOCACA = '" + tipTrabajo + "'" +
		                                                                      " AND TIPSES = '" + tipHora + "'"); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) {
			    	vec.add(res.getInt(1)); // 0-VALMIN 
			    	vec.add(res.getInt(2)); // 1-VALMAX 
			    }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocente.getListaValoresMINMAX : " + e );
			    }
			return vec;
	  }
	 
	 public int getTipoContrato(int rutFun) throws Exception {
			ConexionAs400ValePago con = new ConexionAs400ValePago();
			int tipCont  = 0;
		    try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT TIPCON " +
		        		                                                      " FROM REMUNERABT.APPEF05 " +
		        		                                                      " WHERE RUTIDE = " + rutFun); 
		        
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			      tipCont = res.getInt(1);
			    }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocente.getTipoContrato : " + e );
			    }
			    return tipCont;
	  }
	 
	 public int getSemanaCalculo(int sede, int a�o, int semestre) throws Exception {
			ConexionAs400ValePago con = new ConexionAs400ValePago();
			int semCal  = 0;
		    try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CANSEM " +
		        		                                                      " FROM REMUNERABT.APREF24 " +
		        		                                                      " WHERE CODSUC = " + sede +
		        		                                                      "	AND ANOAYU = " + a�o +
		        		                                                      " AND SEMEST = " + semestre); 
		        
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			      semCal = res.getInt(1);
			    }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocente.getSemanaCalculo : " + e );
			    }
			    return semCal;
	  }
	 
	 public int getListaValorNivel(int sede, int nivelHor) throws Exception {
			ConexionAs400ValePago con = new ConexionAs400ValePago();
			int valor  = 0;
		    try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT VALCAL " +
		                                                                      " FROM REMUNERABT.APREF45 " +
		                                                                      " WHERE SUCUR = " + sede +
		                                                                      " AND NUMNIV = " + nivelHor); 
		        
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			      valor = res.getInt(1);
			    }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocente.getListaValoresHora : " + e );
			    }
			    return valor;
	  }
	
	 public List<ASIGNATURA> getListaAsignaturas(long sede) throws Exception {
			ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	List<ASIGNATURA> lista = new ArrayList<ASIGNATURA>();
		 	 
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT ASIALF, ASINUM, DESASI, CODSUC " +
		                                                                      " FROM REMUNERABT.APPEF18 " +
		                                                                      " WHERE CODSUC = " + sede +
		                                                                      " AND DESASI != '.'" +
		                                                                      " ORDER BY DESASI"); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			    	ASIGNATURA asig = new ASIGNATURA();
			    	asig.setASIALF(res.getString(1).trim()); // 0 parte alf sigla asig.;
			    	asig.setASINUM(res.getInt(2)); 	// 1 parte numerica sigla asig;
			    	asig.setDESASI(res.getString(3).trim()); // 2 Nombre asignatura
			    	asig.setCODSUC(res.getInt(4)); // Sede
			        lista.add(asig);
			     }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocente.getListaAsignaturas : " + e );
			    }
			    return lista;
	  }
	 
	 public String getNombreAsignatura(String alf, int num, long sede) throws Exception {
			ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	String nomAsign = "";
		 	 
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT DESASI " +
		                                                                      " FROM REMUNERABT.APPEF18 " +
		                                                                      " WHERE CODSUC = " + sede +
		                                                                      " AND  ASIALF = '" + alf +"'" +
		                                                                      " AND ASINUM = " + num); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			    	nomAsign = res.getString(1).trim();
			     }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocente.getNombreAsignatura : " + e );
			    }
			    return nomAsign;
	  }
	 
	 public List<GradoAcademico> getListaGradoAcademico() throws Exception {
			ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	List<GradoAcademico> lista = new ArrayList<GradoAcademico>();
		 	 
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODGRA, DESGRA " +
		        		                                                      " FROM REMUNERABT.APREF09 "); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			    	GradoAcademico grado = new GradoAcademico();
			    	grado.setCodGra(res.getInt(1)); // 0 codigo del grado;
			    	grado.setDesGra(res.getString(2).trim()); // Nombre del grado;
			        lista.add(grado);
			     }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocenteB.getListaGradoAcademico : " + e );
			    }
			    return lista;
	  }
	 
	  public String getNombreGrado(long codGra){
			ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	String nombre = "";
		 	 
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT DESGRA " +
                															  " FROM REMUNERABT.APREF09 "+
                															  " WHERE CODGRA = " + codGra); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			      nombre = res.getString(1).trim();
			    }
			    res.close();
			    sent.close();
			    con.getConnection().close();
			 }
			 catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocenteB.getNombreGrado : " + e );
			 }
			return nombre;
	  }	
	  
	  
	  public List<Comuna>  getComunas(){
			// Luego cambiar a una conexion as para Contratacion
		   	ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	List<Comuna> lista = new ArrayList<Comuna>();
		 	
		     
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT COMUNA, NOMCOM " +
		                                                                      " FROM REMUNERABT.APPEF02  "); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			       Comuna comuna = new  Comuna();
			       comuna.setCodigoComuna(res.getInt(1)); 			// 0 codigo de la COMUNA;
			       comuna.setNomComuna(res.getString(2).trim()); 	// 1 nombre de la COMUNA;
			       lista.add(comuna);
			     }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocenteB.getComuna : " + e );
			    }
			    return lista;
		}
	  
	  public String  getNombreComuna(long comuna){
			ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	String nombre = "";
		 	
		     
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT NOMCOM " +
		                                                                      " FROM REMUNERABT.APPEF02 " +
		                                                                      " WHERE COMUNA = " + comuna); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			      nombre = res.getString(1).trim();
			    }
			    res.close();
			    sent.close();
			    con.getConnection().close();
			 }
			 catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocenteB.getNombreComuna : " + e );
			 }
			return nombre;
		}
	  
	  public List<Ciudad> getCiudades(){
			// Luego cambiar a una conexion as para Contratacion
		   	 ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	 List<Ciudad> lista = new ArrayList<Ciudad>();
		     
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CIUDAD, NOMCIU " +
		                                                                      " FROM REMUNERABT.APPEF03  "); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			    	Ciudad ciudad = new Ciudad();
			    	ciudad.setCodigoCiudad(res.getInt(1));        // 0 codigo de la ciudad;
			    	ciudad.setNomCiudad(res.getString(2).trim()); // 1 nombre de la ciudad;
			    	lista.add(ciudad);
			     }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocenteB.getCiudades : " + e );
			    }
			    return lista;
		}
	  
	  public String  getNombreCiudad(long ciudad){
			ConexionAs400ValePago con = new ConexionAs400ValePago();
		 	String nombre = "";
		 	
		     
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT NOMCIU " +
                														   	  " FROM REMUNERABT.APPEF03 " +
                														   	  " WHERE CIUDAD = " + ciudad);  
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			      nombre = res.getString(1).trim();
			    }
			    res.close();
			    sent.close();
			    con.getConnection().close();
			 }
			 catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocenteB.getNombreCiudad : " + e );
			 }
			return nombre;
	  }		
	  
	  public List<APREF24> getListaSemestre(long codsuc){
			// Luego cambiar a una conexion as para Contratacion
		   	 ConexionAs400ValePago con = new ConexionAs400ValePago();
		   	List<APREF24> lista = new ArrayList<APREF24>();
		     
		     try {
		    	String query = 	" SELECT ANOAYU, CODSUC, SEMEST, DIAINI, MESINI, DIAFIN, MESFIN, CANSEM" +
								" FROM REMUNERABT.APREF24 WHERE CODSUC = " + codsuc;
		        PreparedStatement sent = con.getConnection().prepareStatement(query); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			    	APREF24 apref24 = new APREF24();
			    	apref24.setANOAYU(res.getInt(1)); //1 ANOAYU
			    	apref24.setCODSUC(res.getInt(2)); //2 CODSUC
			    	apref24.setSEMEST(res.getInt(3)); //3 SEMEST
			    	apref24.setDIAINI(res.getInt(4)); //4 DIAINI
			    	apref24.setMESINI(res.getInt(5)); //5 MESINI
			    	apref24.setDIAFIN(res.getInt(6)); //6 DIAFIN
			    	apref24.setMESFIN(res.getInt(7)); //7 MESFIN
			    	apref24.setCANSEM(res.getInt(8)); //8 CANSEM
			    	
			    	lista.add(apref24);
			    }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocenteB.getListaSemetre : " + e );
			    }
			    return lista;
		}  

	
	public int getRegistraSolicitud(HttpServletRequest req, int rut, String dv){
		List<Presw18DTO> totalListaAsignaturas = (List<Presw18DTO>) req.getSession().getAttribute("totalListaAsignaturas");
		
		PreswBean preswbean = new PreswBean("SAI", 0,0,0,0, rut);
		preswbean.setRutide(rut);
		preswbean.setDigide(dv);
	
	  	int numSol = 0;
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		listCocow36DTO = this.getCreaCocow36(req, rut, dv, "SAI");
		
		if (totalListaAsignaturas != null && totalListaAsignaturas.size() > 0) {
			for (Presw18DTO ss: totalListaAsignaturas){
				ss.setTippro("SAI");
				ss.setValdoc(ss.getValdoc()*100);
				ss.setPresac(ss.getPresac()*100);
				ss.setUsadac(ss.getUsadac()*100);
				ss.setPreano(ss.getPreano()*100);
			}
		} else totalListaAsignaturas = new ArrayList<Presw18DTO>();
		
			
		Ingreso_Documentos ingresoDocumentos = new Ingreso_Documentos("SAI",rut,dv);
			
		numSol = ingresoDocumentos.Ingresar_Documento(listCocow36DTO,new ArrayList<Presw25DTO>(),totalListaAsignaturas);
		
		return numSol;
  }
	
	 public boolean getActualizaSolicitud(HttpServletRequest req, int rutide, String digide, int numSol){
		List<Presw18DTO> totalListaAsignaturas = (List<Presw18DTO>) req.getSession().getAttribute("totalListaAsignaturas");
			
		PreswBean preswbean = new PreswBean("SAM", 0,0,0,0, rutide);
		preswbean.setNumdoc(numSol);
		preswbean.setRutide(rutide);
		preswbean.setDigide(digide);
		 
		boolean registra = false;
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		listCocow36DTO = this.getCreaCocow36(req, rutide, digide,"SAM");
		Ingreso_Documentos ingresoDocumentos = new Ingreso_Documentos("SAM",rutide,digide);
		ingresoDocumentos.setNumdoc(numSol);
		
		if (totalListaAsignaturas != null && totalListaAsignaturas.size() > 0) {
			for (Presw18DTO ss: totalListaAsignaturas){
				ss.setTippro("SAM");
				ss.setNumdoc(numSol);
				ss.setValdoc(ss.getValdoc()*100);
				ss.setPresac(ss.getPresac()*100);
				ss.setUsadac(ss.getUsadac()*100);
				ss.setPreano(ss.getPreano()*100);
			}
		} else totalListaAsignaturas = new ArrayList<Presw18DTO>();
			
		registra = ingresoDocumentos.Actualizar_Documentos(listCocow36DTO,new ArrayList<Presw25DTO>(),totalListaAsignaturas);
		return registra;
	 }	
  
  public List<Cocow36DTO> getCreaCocow36(HttpServletRequest req, int rutide, String digide, String tipoProceso){
		long rutSol = Util.validaParametro(req.getParameter("rutnum"),0);
		String dvSol = Util.validaParametro(req.getParameter("dvrut"),"");
		long codUni = Util.validaParametro(req.getParameter("codUni"),0);
		long sede = Util.validaParametro(req.getParameter("sede"),0);
		String fechaSolicitudIng = Util.validaParametro(req.getParameter("fechaSolicitudIng"),"");
		String fechaSemIni = Util.validaParametro(req.getParameter("fechaSemIni"),"");
		String fechaSemFin = Util.validaParametro(req.getParameter("fechaSemFin"),"");
		
	    // 22/11/2017
		//dia  substring(6);
		//mes  substring(4,6);
		//anno substring(0,4); 24/11/2017
		long fechaSol = Long.parseLong(fechaSolicitudIng.substring(6)+""+fechaSolicitudIng.substring(3,5)+""+fechaSolicitudIng.substring(0,2));
		long fechaIni = Long.parseLong(fechaSemIni.substring(6)+""+fechaSemIni.substring(3,5)+""+fechaSemIni.substring(0,2));
		long fechaFin = Long.parseLong(fechaSemFin.substring(6)+""+fechaSemFin.substring(3,5)+""+fechaSemFin.substring(0,2));
		
		long tipContrato = Util.validaParametro(req.getParameter("tipContrato"),0);
		long semCon = Util.validaParametro(req.getParameter("sem"),0);
		long a�oCon = Util.validaParametro(req.getParameter("annio"),0);
		long sumaFija = Util.validaParametro(req.getParameter("sumaFija"),0);
		String servPres1 = "";
		String servPres2 = "";
		String trabajoEfec =  Util.validaParametro(req.getParameter("trabajoEfec"),"").trim();
		if (trabajoEfec.length() > 30) {
			 servPres1 = trabajoEfec.substring(0,29);
			 trabajoEfec = trabajoEfec.substring(29);
			 if (trabajoEfec.length() > 30) servPres2 = trabajoEfec.substring(0,29);
			 else servPres2 = trabajoEfec.substring(0);
		}
		else servPres1 = trabajoEfec.substring(0,trabajoEfec.length());
		
		long codPla = Util.validaParametro(req.getParameter("codPla"),0);
		long codCar = Util.validaParametro(req.getParameter("cargo"),0);
		long semanaCal = Util.validaParametro(req.getParameter("semCalculo"),0); // por si es tipo de trabajo ACA y DOC
		long valorMen = Util.validaParametro(req.getParameter("valorMensual"),0);
		long valorTot = Util.validaParametro(req.getParameter("valorContrato"),0);
		String tipTrabajo = Util.validaParametro(req.getParameter("tipTrabajo"),"");
		long nivelHor = Util.validaParametro(req.getParameter("nivelHor"),0);
		long numHora = Util.validaParametro(req.getParameter("hrsSem"),0); // por si es tipo de trabajo OTR
		String codOrg = Util.validaParametro(req.getParameter("codOrg"),"");
				
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		
		String tiping = tipoProceso;
		String nomcam = "";
		long valnu1 = 0;
		long valnu2 = 0;
		String valalf = "";
		
		nomcam = "RUTIDE";
	    valnu1 = rutSol;
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "DIGIDE";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = dvSol;
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "CODUNI";
	    valnu1 = codUni;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "SUCUR";
	    valnu1 = sede;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "FECSOL";
	    valnu1 = fechaSol;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "FECINI";
	    valnu1 = fechaIni;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "FECFIN";
	    valnu1 = fechaFin;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "TIPCON";
	    valnu1 = tipContrato;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "SEMEST";
	    valnu1 = semCon;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "ANODOC";
	    valnu1 = a�oCon;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "SUMFIJ";
	    valnu1 = sumaFija;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "SERPR1";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = servPres1;
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "SERPR2";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = servPres2;
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "CODPLA";
	    valnu1 = codPla;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "CODCAR";
	    valnu1 = codCar;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "SEMCAL";
	    valnu1 = (semanaCal*100000);
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "VALMES";
	    valnu1 = valorMen;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "VALTOT";
	    valnu1 = valorTot;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "TIPTRA";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = tipTrabajo;
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "NIVHOR";
	    valnu1 = nivelHor;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "CANHOR";
	    valnu1 = numHora;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "CODORG";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = codOrg;
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    return listCocow36DTO; 

		}
  
  public List<Cocow36DTO> getAgregaLista(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf){
		 Cocow36DTO cocow36DTO = new Cocow36DTO();
		 cocow36DTO.setTiping(tiping);
		 cocow36DTO.setNomcam(nomcam);
		 cocow36DTO.setValnu1(valnu1);
		 cocow36DTO.setValnu2(valnu2);
		 cocow36DTO.setValalf(valalf);
		 lista.add(cocow36DTO);
		 return lista;
 }
  
  public boolean getActualizaEstadoSolicitud(int numSol, int rutUsuario, String digide, String tipo, String estado){
		PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
		preswbean.setNumdoc(numSol);
		preswbean.setRutide(rutUsuario);
		preswbean.setDigide(digide);
	    preswbean.setTipcue(estado);
	    return preswbean.ingreso_presw19();
  } 
	
  public String formateoRut(String rut) {
	  int cont = 0;
        String format;
        rut = rut.replace(".", "");
        rut = rut.replace("-", "");
        format = "-" + rut.substring(rut.length() - 1);
        for (int i = rut.length() - 2; i >= 0; i--) {
            format = rut.substring(i, i + 1) + format;
            cont++;
            if (cont == 3 && i != 0) {
                format = "." + format;
                cont = 0;
            }
        }
       return format;
}
  
  public void getEliminaListaAsignatura( HttpServletRequest req) throws Exception{
		 HttpSession sesion = req.getSession();
	     String asigAlf = Util.validaParametro(req.getParameter("asigAlf"),"");
	     int asigNum = Util.validaParametro(req.getParameter("asigNum"),0);
	     String paralelo = Util.validaParametro(req.getParameter("paralelo"), "");
	     long sede = Util.validaParametro(req.getParameter("sede"), 0);
	     
		 List<Presw18DTO>  totalListaAsignaturas = (List<Presw18DTO> )sesion.getAttribute("totalListaAsignaturas");
		 List<Presw18DTO> listaAsignaturas = new ArrayList<Presw18DTO>();
		 
		 if(totalListaAsignaturas != null && totalListaAsignaturas.size()> 0)
			 sesion.removeAttribute("listaTotalAsignaturas");
		 
		 if(totalListaAsignaturas != null && totalListaAsignaturas.size() > 0){
			 for (Presw18DTO ss: totalListaAsignaturas){
				 if(ss.getTipmov().trim().equals(asigAlf.trim()) && 
					ss.getItedoc() == asigNum &&
					ss.getIddigi().trim().equals(paralelo.trim()))
					 continue;
				 else
				 {
					 Presw18DTO presw18DTO = new Presw18DTO();
					 presw18DTO.setTipmov(ss.getTipmov()); 
					 presw18DTO.setItedoc(ss.getItedoc());
					 presw18DTO.setIddigi(ss.getIddigi());
					 presw18DTO.setValdoc(ss.getValdoc());
					 presw18DTO.setPresac(ss.getPresac());
					 presw18DTO.setUsadac(ss.getUsadac());
					 presw18DTO.setPreano(ss.getPreano());
					 presw18DTO.setDesuni(getNombreAsignatura(ss.getTipmov(),ss.getItedoc(),sede));
					 listaAsignaturas.add(presw18DTO);
				 }
			 }
			 
		 }
		 sesion.setAttribute("totalListaAsignaturas", listaAsignaturas);	
	 }
  
	  public int calculoDiasTrabajados(String fechaIni, String fechaFin) {
	 
	  
		  StringTokenizer stM = new StringTokenizer(fechaFin,"/");
		  int diaM = Integer.parseInt(stM.nextToken());    
		  int mesM = Integer.parseInt(stM.nextToken());    
		  int anoM = Integer.parseInt(stM.nextToken());    
		  
		  
		  StringTokenizer st = new StringTokenizer(fechaIni,"/");
		  int diam = Integer.parseInt(st.nextToken());    
		  int mesm = Integer.parseInt(st.nextToken());    
		  int anom = Integer.parseInt(st.nextToken());    
		  
		  int FMAYOR = Integer.parseInt(anoM+""+mesM+""+diaM);
		  int FMENOR = Integer.parseInt(anom+""+mesm+""+diam);
		  
		  int cont = 0;
		  
		  if(FMAYOR >= FMENOR){
		     int mesMaux = mesM;
		     int diaMaux = diaM;
		      
		     cont = 0;
		  
		     while(anom<=anoM){
		     
		      if(anom<anoM){
		        mesM = 12;
		        diaM = 31; //31;
		      }else{
		         mesM = mesMaux;
		         diaM = diaMaux;
		      }
		     
		     while(mesm <= mesM){
		      int DiasDelMes = 0;
		      
		       if(mesm == mesM){
		          DiasDelMes = diaM;
		       }else{
		    	 if (diam == 1) DiasDelMes = 30;
		    	 else DiasDelMes = DiasDelMes(anom,mesm); // este metodo se llama solo si el d�a del periodo no parte en 1
		       } 
		        
		        while(diam <= DiasDelMes){
		         cont++;
		         diam++;
		        }
		         diam = 1;
		         mesm ++;
		       }   
		      
		       mesm = 1;
		       anom++;
		     }
		      
		          
		        
		  System.out.println ("Numero de dias contados -> "+(cont-1));
		  System.out.println ("Con el dia de inicio incluido serian -> "+cont);
		     
		  }else{
		     System.out.println ("Error....!!La fecha Menor supera a la fecha Mayor");
		  }
		  
	  return (cont-1);
	}

	public static int DiasDelMes(int ano, int mes){
	 int ndias = 0;
	 int f = 0;
	
	  int an = ano;
	
	  if(an % 4 == 0){
	      f = 29;
	     }else{
	      f = 28;
	     }
	 switch (mes) {
	    case 1:   ndias = 31;  break;
	    case 2:   ndias = f;   break;
	    case 3:   ndias = 31;  break;
	    case 4:   ndias = 30;  break;
	    case 5:   ndias = 31;  break;
	    case 6:   ndias = 30;  break;
	    case 7:   ndias = 31;  break;
	    case 8:   ndias = 31;  break;
	    case 9:   ndias = 30;  break;
	    case 10:  ndias = 31;  break;
	    case 11:  ndias = 30;  break;
	    case 12:  ndias = 31;  break;
	    }
	 return ndias;
	}

	 
}