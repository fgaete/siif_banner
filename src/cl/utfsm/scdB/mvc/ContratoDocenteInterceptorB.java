package cl.utfsm.scdB.mvc;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.utfsm.POJO.APPEF01;
import cl.utfsm.POJO.APREF07;
import cl.utfsm.POJO.APREF08;
import cl.utfsm.POJO.APREF23;
import cl.utfsm.POJO.APREF24;
import cl.utfsm.POJO.ASIGNATURA;
import cl.utfsm.POJO.Ciudad;
import cl.utfsm.POJO.Comuna;
import cl.utfsm.POJO.GradoAcademico;
import cl.utfsm.POJO.PRESF200;
import cl.utfsm.POJO.SucursalBanco;
import cl.utfsm.POJO.Banco;
import cl.utfsm.POJO.Titulo;
import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.scdB.modulo.ModuloContratoDocenteB;
import descad.cliente.CocowBean;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw25DTO;

public class ContratoDocenteInterceptorB  extends HandlerInterceptorAdapter{
	ModuloContratoDocenteB moduloContratoDocenteB;

	
	public ModuloContratoDocenteB getModuloContratoDocenteB() {
		return moduloContratoDocenteB;
	}


	public void setModuloContratoDocenteB(
			ModuloContratoDocenteB moduloContratoDocenteB) {
		this.moduloContratoDocenteB = moduloContratoDocenteB;
	}


	public void cargarMenu(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		accionweb.agregarObjeto("esContratoDocenteB", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		accionweb.agregarObjeto("titulo", "Ingreso");
		
		if(accionweb.getSesion().getAttribute("listaSede") != null)
		   accionweb.getSesion().removeAttribute("listaSede");
		if(accionweb.getSesion().getAttribute("listaOrg") != null)
		   accionweb.getSesion().removeAttribute("listaOrg");
		if(accionweb.getSesion().getAttribute("listaProfesor") != null)
		  accionweb.getSesion().removeAttribute("listaProfesor");
		if(accionweb.getSesion().getAttribute("listaCiudad") != null)
		   accionweb.getSesion().removeAttribute("listaCiudad");
		if(accionweb.getSesion().getAttribute("listaComuna") != null)
		   accionweb.getSesion().removeAttribute("listaComuna");
		if(accionweb.getSesion().getAttribute("listaGradoAcademico") != null)
		   accionweb.getSesion().removeAttribute("listaGradoAcademico");
		if(accionweb.getSesion().getAttribute("listaTitulo") != null)
		   accionweb.getSesion().removeAttribute("listaTitulo");
		if(accionweb.getSesion().getAttribute("listaPlanta") != null)
		   accionweb.getSesion().removeAttribute("listaCargo");
		if(accionweb.getSesion().getAttribute("listaCargo") != null)
		   accionweb.getSesion().removeAttribute("listaCargo");
		if(accionweb.getSesion().getAttribute("listaAsignaturas") != null)
		   accionweb.getSesion().removeAttribute("listaAsignaturas");
		if(accionweb.getSesion().getAttribute("totalListaAsignaturas") != null)
		   accionweb.getSesion().removeAttribute("totalListaAsignaturas");
		
		List<Presw18DTO> listaSede = new ArrayList<Presw18DTO>();
		listaSede = moduloContratoDocenteB.getListaSede(accionweb.getReq());
		if(listaSede != null && listaSede.size() > 0) {
	      	accionweb.getSesion().setAttribute("listaSede", listaSede);
	      	accionweb.agregarObjeto("listaSede", listaSede);
	   }  	
				
		
		List<PRESF200> listaOrg = new ArrayList<PRESF200>();
		listaOrg = moduloContratoDocenteB.getOrganizacion();
		if(listaOrg != null && listaOrg.size() > 0)
	       accionweb.getSesion().setAttribute("listaOrg", listaOrg);
		
		List<APPEF01> listaProfesor = new ArrayList<APPEF01>();
		listaProfesor = moduloContratoDocenteB.getListaProfesor();
		if(listaProfesor != null && listaProfesor.size() > 0) 
		   accionweb.getSesion().setAttribute("listaProfesor", listaProfesor);
		
		List<Ciudad> listaCiudad = new ArrayList<Ciudad>();
		listaCiudad = moduloContratoDocenteB.getCiudades();
		if(listaCiudad != null && listaCiudad.size() > 0) 
		   accionweb.getSesion().setAttribute("listaCiudad", listaCiudad);
		
		List<Comuna> listaComuna = new ArrayList<Comuna>();
		listaComuna = moduloContratoDocenteB.getComunas();
		if(listaComuna != null && listaComuna.size() > 0) 
		   accionweb.getSesion().setAttribute("listaComuna", listaComuna);
		
		List<GradoAcademico> listaGradoAcademico = new ArrayList<GradoAcademico>();
		listaGradoAcademico = moduloContratoDocenteB.getListaGradoAcademico();
		if(listaGradoAcademico != null && listaGradoAcademico.size() > 0) 
		   accionweb.getSesion().setAttribute("listaGradoAcademico", listaGradoAcademico);
		
		List<Titulo> listaTitulo = new ArrayList<Titulo>();
		listaTitulo = moduloContratoDocenteB.getListaTitulo();
		if(listaTitulo != null && listaTitulo.size() > 0) 
		   accionweb.getSesion().setAttribute("listaTitulo", listaTitulo);
		
		List<APREF07> listaPlanta = new ArrayList<APREF07>();
		listaPlanta = moduloContratoDocenteB.getListaPlanta();
		if(listaPlanta != null && listaPlanta.size() > 0) 
		   accionweb.getSesion().setAttribute("listaPlanta", listaPlanta);
		
		accionweb.agregarObjeto("tipo", tipo);
		if (tipo == 5)
			accionweb.agregarObjeto("titulo", "Autorizar");
		
	}
	
	public void cargaIngresoDatosPersonales(AccionWeb accionweb) throws Exception {
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
	    accionweb.agregarObjeto("esContratoDocenteB", "1");
        accionweb.agregarObjeto("opcionMenu", opcionMenu);
        accionweb.agregarObjeto("muestraIngreso", 1);
	}
	
	public void cargaDatosPersonales(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		String identificadorDatos = Util.validaParametro(accionweb.getParameter("identificadorDatos"), "");
		
		List<Presw18DTO> listaOrg  = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaOrg");
		List<Presw18DTO> listaTipoAyu = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaTipoAyu");
		List<APPEF01> listaProfesor = (List<APPEF01>) accionweb.getSesion().getAttribute("listaProfesor");
		List<Ciudad> listaCiudad = (List<Ciudad>) accionweb.getSesion().getAttribute("listaCiudad");
		List<Comuna> listaComuna = (List<Comuna>) accionweb.getSesion().getAttribute("listaComuna");
		List<GradoAcademico> listaGradoAcademico = (List<GradoAcademico>) accionweb.getSesion().getAttribute("listaGradoAcademico");
		List<Titulo> listaTitulo = (List<Titulo>) accionweb.getSesion().getAttribute("listaTitulo");
		List<APREF07> listaPlanta = (List<APREF07>) accionweb.getSesion().getAttribute("listaPlanta");
	    List<ASIGNATURA> listaAsignaturas = (List<ASIGNATURA>) accionweb.getSesion().getAttribute("listaAsignaturas");
		
		List<Presw18DTO> listaSede = new ArrayList<Presw18DTO>();
		listaSede = moduloContratoDocenteB.getListaSede(accionweb.getReq());
		
		
		if (listaSede != null && listaSede.size() > 0) accionweb.agregarObjeto("listaSede", listaSede);
		if (listaOrg != null && listaOrg.size() > 0) accionweb.agregarObjeto("listaOrg", listaOrg);
		if (listaTipoAyu != null && listaTipoAyu.size() > 0) accionweb.agregarObjeto("listaTipoAyu", listaTipoAyu);
		if (listaProfesor != null && listaProfesor.size() > 0) accionweb.agregarObjeto("listaProfesor", listaProfesor);
	    if (listaCiudad != null && listaCiudad.size() > 0) accionweb.agregarObjeto("listaCiudad", listaCiudad);
		if (listaComuna != null && listaComuna.size() > 0) accionweb.agregarObjeto("listaComuna", listaComuna);
		if (listaGradoAcademico != null && listaGradoAcademico.size() > 0) accionweb.agregarObjeto("listaGradoAcademico", listaGradoAcademico);
		if (listaTitulo != null && listaTitulo.size() > 0) accionweb.agregarObjeto("listaTitulo", listaTitulo);
		if (listaPlanta != null && listaPlanta.size() > 0) accionweb.agregarObjeto("listaPlanta", listaPlanta);
		if (listaAsignaturas != null && listaAsignaturas.size() > 0) accionweb.agregarObjeto("listaAsignaturas", listaAsignaturas);
			
		
		Presw19DTO preswbean19DTO = new Presw19DTO();
	   	String titulo = "";
		preswbean19DTO.setTippro("SAH"); 
	  	preswbean19DTO.setRutide(new BigDecimal(rutnum));
	  	preswbean19DTO.setDigide(dvrut);
		 
	  	String nomcam = "";
		long valnu1 = 0;
		long valnu2 = 0;
		String valalf = "";
		String paterno = "";
		String materno = "";
		String nombre = "";
		long sexo = 0;
		long estCivil = 0;
		String domicilio = "";
		long comuna = 0;
		long ciudad = 0;
		long region = 0;
		String fechaNac = "";
		long codNac = 0;
		String otraNac = "";
		long codGra = 0;
		long codTit = 0;
		
		 
	    List<Cocow36DTO>  listaDatosPersonales = new ArrayList<Cocow36DTO>();
		CocowBean cocowBean = new CocowBean();
		listaDatosPersonales = cocowBean.buscar_cocow36(preswbean19DTO);
		if(listaDatosPersonales != null && listaDatosPersonales.size() >0 ){
		   for(Cocow36DTO ss: listaDatosPersonales){
              if(ss.getNomcam().trim().equals("APEPAT"))
				 paterno = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("APEMAT"))
				 materno = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("NOMBRE"))
				 nombre = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("SEXO"))
				 sexo = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("ESTCIV"))
				 estCivil = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("DOMICI"))
				 domicilio = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("COMUNA"))
				 comuna = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("CIUDAD"))
				 ciudad = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("REGION"))
				 region = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("FECNAC") && ss.getValnu1() > 0 )
				 fechaNac = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
			  if(ss.getNomcam().trim().equals("NACION"))
				 codNac = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("DETNAC"))
				 otraNac = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("CODGRA"))
				 codGra = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("CODTIT"))
				 codTit = ss.getValnu1();
			   } 
			  if((paterno.trim().equals("") && identificadorDatos.trim().equals("")) || rutnum == 0){
				   listaDatosPersonales = null;
				   accionweb.agregarObjeto("mensaje", "RUT no registrado, debe ser agregado.");
				   accionweb.agregarObjeto("muestraBoton", 1);
			   }
		  } else  if(identificadorDatos.trim().equals("") || rutnum == 0)  accionweb.agregarObjeto("mensaje", "RUT no registrado, debe ser agregado.");
			 //String rutFormateado = moduloContratoAyudantiaB.formateoRut(rutnum) + "-" + dvrut;

		 if (rut_aux.equals("")) rut_aux = moduloContratoDocenteB.formateoRut(rutnum+""+dvrut);
		
		 accionweb.agregarObjeto("rut_aux",rut_aux);
		 accionweb.agregarObjeto("paterno",paterno);
		 accionweb.agregarObjeto("materno",materno);
		 accionweb.agregarObjeto("nombre",nombre);
		 accionweb.agregarObjeto("sexo",sexo);
		 accionweb.agregarObjeto("estCivil",estCivil);
		 accionweb.agregarObjeto("domicilio",domicilio);
		 accionweb.agregarObjeto("comuna",comuna);
		 accionweb.agregarObjeto("ciudad",ciudad);
		 accionweb.agregarObjeto("region",region);
		 accionweb.agregarObjeto("fechaNac",fechaNac);
		 accionweb.agregarObjeto("codNac",codNac);
		 accionweb.agregarObjeto("otraNac",otraNac);
		 accionweb.agregarObjeto("codGra",codGra);
		 accionweb.agregarObjeto("codTit",codTit);
		 accionweb.agregarObjeto("rutnum",rutnum);
		 accionweb.agregarObjeto("tipoContratoFun",moduloContratoDocenteB.getTipoContrato(rutnum));
		 accionweb.agregarObjeto("dvrut",dvrut);
		 String fechaActual = new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	     accionweb.agregarObjeto("fechaSolicitudIng", fechaActual);
		 accionweb.agregarObjeto("listaDatosPersonales", listaDatosPersonales);
         accionweb.agregarObjeto("esContratoDocenteB", "1");
         accionweb.agregarObjeto("opcion", String.valueOf(tipo));
    	 accionweb.agregarObjeto("opcionMenu", opcionMenu);
       	 
    	 if(!identificadorDatos.trim().equals(""))
   		   accionweb.agregarObjeto("identificadorDatos", identificadorDatos);
    	 
    	 if (tipo == 1) accionweb.agregarObjeto("titulo", "Ingreso");
    	 else if (tipo == 2) accionweb.agregarObjeto("titulo", "Modificaci�n");
    	 else if (tipo == 3) accionweb.agregarObjeto("titulo", "Consulta");
    	 
    	 accionweb.agregarObjeto("tipo",tipo);
   	
    }
	
	public void cargarListaSemestre(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		long sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
		List<APREF24> lista = moduloContratoDocenteB.getListaSemestre(sede);
		if(lista != null)
			accionweb.getSesion().setAttribute("listaSemestre", lista);
		
		accionweb.agregarObjeto("listaSemestre", lista);
		accionweb.agregarObjeto("esContratoDocenteB", "1"); 
		
	}
	
	public void cargarListaAsignaturas(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		long sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
		String tipTrabajo = Util.validaParametro(accionweb.getParameter("tipTrabajo"),"");
		
		if (!tipTrabajo.trim().equals("OTR")) {
		  if (sede > 0) {
			List<ASIGNATURA> listaAsignaturas = new ArrayList<ASIGNATURA>();
			listaAsignaturas = moduloContratoDocenteB.getListaAsignaturas(sede);
			if(listaAsignaturas != null && listaAsignaturas.size() > 0) {
			  accionweb.agregarObjeto("listaAsignaturas", listaAsignaturas);
		   }
			
		  }
		}
		
		List<Presw18DTO> totalListaAsignaturas = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaAsignaturas");
		accionweb.agregarObjeto("totalListaAsignaturas", totalListaAsignaturas); 
		accionweb.agregarObjeto("esContratoDocenteB", "1"); 
		
	}
	
	 public void consultaSolicitud(AccionWeb accionweb) throws Exception {
		 int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		 String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		 int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		 
		 int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		 String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		 String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		 
		 int numSol = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		 int sedeLista  = Util.validaParametro(accionweb.getParameter("sedeLista"), 0);
		 
	
	  	 Presw19DTO preswbean19DTO = new Presw19DTO();
	   	 String titulo = "";
		 String tituloDetalle1 = "";
		 preswbean19DTO.setTippro("SAL"); 
	  	 preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
	  	 preswbean19DTO.setDigide(dv);
		 preswbean19DTO.setNumdoc(new BigDecimal(numSol));
		 		 
		 String nomcam = "";
		 long valnu1 = 0;
		 long valnu2 = 0;
		 String valalf = "";
		 long rutSol = 0;
		 String dvSol = "";
		 String paterno = "";
		 String materno = "";
		 String nombre = "";
		 long sexo = 0;
		 long estCivil = 0;
		 String domicilio = "";
		 long comuna = 0;
		 String nomComuna = "";
		 long ciudad = 0;
		 String nomCiudad = "";
		 long region = 0;
		 String fechaNac = "";
		 long codNac = 0;
		 String otraNac = "";
		 long codGra = 0;
		 String nomGra = "";
		 long codTit = 0;
		 String nomTit = "";
	  	 long codUni = 0;
		 long sede = 0;
		 String nomSede = "";
		 String fechaSol = "";
		 String fechaIni = "";
		 String fechaFin = "";
		 long tipContrato = 0;
		 long semCon = 0;
		 long a�oCon = 0;
		 long sumaFija = 0;
		 String servPres1 = "";
		 String servPres2 = "";
		 long codPla = 0;
		 String nomPla = "";
		 long codCar = 0;
		 String nomCar = "";
		 long semanaCal = 0;
	   	 long valorMen = 0;
		 long valorTot = 0;
		 String tipTrabajo = "";
		 String nivelHor = "";
		 long numHora = 0;
		 String codOrg = "";
		 String nomOrg = "";
		 String estado = "";
		 
		 List<Cocow36DTO>  listaSolicitud = new ArrayList<Cocow36DTO>();
		 CocowBean cocowBean = new CocowBean();
		 listaSolicitud = cocowBean.buscar_cocow36(preswbean19DTO);
		 if(listaSolicitud != null && listaSolicitud.size() >0 ){
		    for(Cocow36DTO ss: listaSolicitud){
              if(ss.getNomcam().trim().equals("RUTIDE")) 
               	rutSol = ss.getValnu1();
              if(ss.getNomcam().trim().equals("DIGIDE"))
	            dvSol = ss.getValalf().trim();
              if(ss.getNomcam().trim().equals("APEPAT"))
  	            paterno = ss.getValalf().trim();
              if(ss.getNomcam().trim().equals("APEMAT"))
    	        materno = ss.getValalf().trim();
              if(ss.getNomcam().trim().equals("NOMBRE"))
      	        nombre = ss.getValalf().trim();
              if(ss.getNomcam().trim().equals("SEXO"))
 				 sexo = ss.getValnu1();
 			  if(ss.getNomcam().trim().equals("ESTCIV"))
 				 estCivil = ss.getValnu1();
 			  if(ss.getNomcam().trim().equals("DOMICI"))
 				 domicilio = ss.getValalf().trim();
 			 if(ss.getNomcam().trim().equals("COMUNA"))
				 comuna = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("CIUDAD"))
				 ciudad = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("REGION"))
				 region = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("FECNAC"))
			   	 fechaNac = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
			  if(ss.getNomcam().trim().equals("NACION"))
				 codNac = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("DETNAC"))
				  otraNac = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("CODGRA"))
				 codGra = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("CODTIT"))
				 codTit = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("CODUNI"))
				 codUni = ss.getValnu1();
	          if(ss.getNomcam().trim().equals("SUCUR"))
	            sede = ss.getValnu1();
	          if(ss.getNomcam().trim().equals("NOMSUCUR"))
	            nomSede = ss.getValalf().trim(); 
		      if(ss.getNomcam().trim().equals("FECSOL"))
			   	fechaSol = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
	          if(ss.getNomcam().trim().equals("FECINI"))
				fechaIni = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
		      if(ss.getNomcam().trim().equals("FECFIN"))
				fechaFin = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
		      if(ss.getNomcam().trim().equals("TIPCON"))
			     tipContrato = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("SEMEST"))
		         semCon = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("ANODOC"))
		         a�oCon = ss.getValnu1(); 
		      if(ss.getNomcam().trim().equals("SUMFIJ"))
				 sumaFija = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("SERPR1"))
		    	 servPres1 = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("SERPR2"))
		    	 servPres2 = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("CODPLA"))
			     codPla = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("DESPLA"))
			     nomPla = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("CODCAR"))
				 codCar = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("DESCAR"))
			     nomCar = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("SEMCAL"))
				 semanaCal = ss.getValnu1()/100000;
			  if(ss.getNomcam().trim().equals("VALMES"))
			     valorMen = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("VALTOT"))
			     valorTot = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("TIPTRA"))
				 tipTrabajo = ss.getValalf().trim();
	 		  if(ss.getNomcam().trim().equals("NIVHOR"))
				nivelHor = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("CANHOR"))
		        numHora = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("CODORG"))
			   	codOrg = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("DESORG"))
			   	 nomOrg = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("ESTADO"))
			    estado = ss.getValalf().trim();
		      
		    }      
	     }

		 String rutFormateado = moduloContratoDocenteB.formateoRut(rutSol+""+dvSol);
		 
		 accionweb.agregarObjeto("rutSolicitud",rutFormateado);
		 accionweb.agregarObjeto("paterno",paterno);
		 accionweb.agregarObjeto("mterno",materno);
		 accionweb.agregarObjeto("nombre",nombre);
		 accionweb.agregarObjeto("sexo",sexo);
		 accionweb.agregarObjeto("estCivil",estCivil);
		 accionweb.agregarObjeto("domicilio",domicilio);
		 accionweb.agregarObjeto("comuna",comuna);
		 accionweb.agregarObjeto("ciudad",ciudad);
		 accionweb.agregarObjeto("region",region);
		 accionweb.agregarObjeto("fechaNac",fechaNac);
		 accionweb.agregarObjeto("codNac",codNac);
		 accionweb.agregarObjeto("otraNac",otraNac);
		 accionweb.agregarObjeto("codGra",codGra);
		 accionweb.agregarObjeto("codTit",codTit);
		 accionweb.agregarObjeto("codUni",codUni);
		 accionweb.agregarObjeto("sede",sede);
		 accionweb.agregarObjeto("nomSede",nomSede);
		 accionweb.agregarObjeto("fechaSolicitudIng",fechaSol);
		 accionweb.agregarObjeto("fechaPeriodoSemIni",fechaIni);
		 accionweb.agregarObjeto("fechaPeriodoSemFin",fechaFin);
		 accionweb.agregarObjeto("tipContrato",tipContrato);
		 accionweb.agregarObjeto("semCon",semCon);
		 accionweb.agregarObjeto("anoCon",a�oCon);
		 accionweb.agregarObjeto("sumaFija",sumaFija);
		 accionweb.agregarObjeto("trabajoEfec",servPres1+ ""+servPres2);
		 accionweb.agregarObjeto("codPla",codPla);
		 accionweb.agregarObjeto("nomPla",nomPla);
		 accionweb.agregarObjeto("codCar",codCar);
		 accionweb.agregarObjeto("nomCar",nomCar);
		 accionweb.agregarObjeto("semanaCal",semanaCal);
		 accionweb.agregarObjeto("totalMensual",valorMen);
		 accionweb.agregarObjeto("totalContrato",valorTot);
		 accionweb.agregarObjeto("tipTrabajo",tipTrabajo);
		 accionweb.agregarObjeto("tipoTrabajo",tipTrabajo); // Para pantalla detalleSemanaCalculo
		 accionweb.agregarObjeto("nivelHor",nivelHor);
	   	 accionweb.agregarObjeto("numHora",numHora);
	   	 accionweb.agregarObjeto("horasSem",numHora);  // Para pantalla detalleHoras Semestre
		 accionweb.agregarObjeto("codOrg",codOrg);
		 accionweb.agregarObjeto("nomOrg",nomOrg);
		 accionweb.agregarObjeto("estado",estado);
		 accionweb.agregarObjeto("numDoc",numSol);
		 
		 accionweb.agregarObjeto("tipo", tipo); 
		 
	
		 PreswBean preswbean = null;
  		 preswbean = new PreswBean("SAL",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
		 preswbean.setNumdoc(numSol);
			
		 accionweb.getSesion().removeAttribute("totalListaAsignaturas");
		 Collection<Presw18DTO> totalListaAsignaturas = preswbean.consulta_presw18();
		 if(totalListaAsignaturas != null) {
			 String nomAsign = "";
			  for (Presw18DTO ss : totalListaAsignaturas) {
				 ss.setValdoc(ss.getValdoc()/100);
				 ss.setPresac(ss.getPresac()/100);
				 ss.setUsadac(ss.getUsadac()/100);
				 ss.setPreano(ss.getPreano()/100);
				 ss.setDesuni(moduloContratoDocenteB.getNombreAsignatura(ss.getTipmov(),ss.getItedoc(),sede));
				 ss.setAcumum(semanaCal);
		      }
		 }
		 
	     accionweb.getSesion().setAttribute("totalListaAsignaturas",totalListaAsignaturas);
		 accionweb.agregarObjeto("totalListaAsignaturas",totalListaAsignaturas);
		
		 this.cargaDatosPersonales(accionweb);
		 
		 List<APREF24> listaSemestre = moduloContratoDocenteB.getListaSemestre(sede);
		 if(listaSemestre != null) {
			accionweb.getSesion().setAttribute("listaSemestre", listaSemestre);
			accionweb.agregarObjeto("listaSemestre", listaSemestre);
	     } 
		 
		 List<APREF08> listaCargo = moduloContratoDocenteB.getListaCargo(codPla);
		 if(listaSemestre != null)  {
			accionweb.getSesion().setAttribute("listaCargo", listaCargo);
			accionweb.agregarObjeto("listaCargo", listaCargo);
		 }
		 
		 if (tipo == 3 || tipo == 4 || tipo == 5) {
			 accionweb.agregarObjeto("identificadorConsulta", "1");
			 if (tipo == 3 )
			    accionweb.agregarObjeto("titulo", "Consulta");
			 if (tipo == 4 || tipo == 5)
				accionweb.agregarObjeto("titulo", "Autorizaci�n");
			 if (tipo == 5 && sedeLista > 0 )
				accionweb.agregarObjeto("sedeLista", sedeLista);
		 }
		 else if (tipo == 2)  {
			    accionweb.agregarObjeto("titulo", "Modificaci�n");
			    if (!tipTrabajo.trim().equals("OTR")) {
				  if (sede > 0) {
					List<ASIGNATURA> listaAsignaturas = new ArrayList<ASIGNATURA>();
					listaAsignaturas = moduloContratoDocenteB.getListaAsignaturas(sede);
					if(listaAsignaturas != null && listaAsignaturas.size() > 0) {
				 	  accionweb.agregarObjeto("listaAsignaturas", listaAsignaturas);
				   }
				 }
			 }
			    
		      }
	
	     accionweb.agregarObjeto("esContratoDocenteB", "1");
		 
	 }
	 
	 public void cargaOpcionMenu(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			int sedeLista = Util.validaParametro(accionweb.getParameter("sedeLista"), 0);
			
			PreswBean preswbean = null;
			Collection<Presw18DTO> listaSolicitudes = null;
			String titulo = "";
			
			switch (tipo) {
			case  2:// lista de Solicitudes para modificar
				{
				preswbean = new PreswBean("SAK",0,0,0,0,rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				titulo = "Modificaci�n";
				break;
			   }
			case  3:// Consulta de ude las solicitudes de un rut Ingresador
				{
				preswbean = new PreswBean("SAN",0,0,0,0,rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				titulo = "Consulta";
				break;
			   }	
			case  4:// lista de solicitudes para autorizar SIIF
				{
				preswbean = new PreswBean("SAO",0,0,0,0,rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				titulo = "Autorizar";
				break;
			   }	
		case  5:// lista de solicitudes para DGIP/SEDES
				{
				preswbean = new PreswBean("SAQ",0,0,0,0,rutUsuario,0,"",sedeLista,dv, "",0,0,0,0,"","");
				titulo = "Autorizar";
				break;
			   }
			}
			
			listaSolicitudes = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		    if(listaSolicitudes != null && listaSolicitudes.size() > 0)
	        	accionweb.agregarObjeto("hayDatoslista", "1");
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		
		    accionweb.agregarObjeto("titulo", titulo);
		    accionweb.agregarObjeto("tipo", tipo);
	        accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes);
	    	accionweb.agregarObjeto("esContratoDocenteB", "1");

	    	if(accionweb.getSesion().getAttribute("listaSede") != null) {
	    	   List<Presw18DTO> listaSede = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSede");
	    	   accionweb.agregarObjeto("sedeLista", sedeLista);
	    	   accionweb.agregarObjeto("listaSede", listaSede);
	    	}
			
		}	 

	public void cargaAsignatura(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		String asignatura = Util.validaParametro(accionweb.getParameter("asignatura"),"");
		String paralelo = Util.validaParametro(accionweb.getParameter("paralelo"),"");
		long valorTeo = Util.validaParametro(accionweb.getParameter("valorTeo"),0);
		int horasTeo = Util.validaParametro(accionweb.getParameter("horasTeo"),0);
		long valorPra = Util.validaParametro(accionweb.getParameter("valorPra"),0);
        int horasPra = Util.validaParametro(accionweb.getParameter("horasPra"),0);
        long sumaFija = Util.validaParametro(accionweb.getParameter("sumaFija"),0);
        long semanaCal = Util.validaParametro(accionweb.getParameter("semanaCal"),0);
        String fechaIni = Util.validaParametro(accionweb.getParameter("fechaIni"),"");
        String fechaFin = Util.validaParametro(accionweb.getParameter("fechaFin"),"");
        
		int opcion = Util.validaParametro(accionweb.getParameter("opcion"), 0);
		accionweb.agregarObjeto("opcion", opcion);
		
		List<Presw18DTO> detalleAsignatura = new ArrayList<Presw18DTO>();
		List<Presw18DTO> totalListaAsignaturas = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaAsignaturas");
			
		try {
			 if(totalListaAsignaturas == null) 
				 totalListaAsignaturas = new ArrayList<Presw18DTO>();
			 
			 String asigAlf = "";
			 int asigNum = 0;
			 String nomAsign = "";
			 			 
			 int indice = asignatura.indexOf("@");
			 asigAlf = asignatura.substring(0,indice);
			 asignatura = asignatura.substring(indice+1);
			 indice = asignatura.indexOf("@");
			 asigNum = Integer.parseInt(asignatura.substring(0,indice));
			 nomAsign = asignatura.substring(indice+1);
			 
			 
			 Presw18DTO presw18DTO = new Presw18DTO();
			 presw18DTO.setTipmov(asigAlf);
			 presw18DTO.setItedoc(asigNum);
			 presw18DTO.setIddigi(paralelo);
			 presw18DTO.setValdoc(horasTeo);
			 presw18DTO.setPresac(valorTeo);
			 presw18DTO.setUsadac(horasPra);
			 presw18DTO.setPreano(valorPra);
			 presw18DTO.setDesuni(nomAsign);
			 presw18DTO.setAcumum(semanaCal);
			 detalleAsignatura.add(presw18DTO);
			 totalListaAsignaturas.add(presw18DTO);
		     accionweb.getSesion().setAttribute("totalListaAsignaturas",totalListaAsignaturas);
		    
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "cargaAsignaturas.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
	 
		accionweb.agregarObjeto("detalleAsignatura", detalleAsignatura); 	
		accionweb.agregarObjeto("totalListaAsignaturas", totalListaAsignaturas); 
	   	accionweb.agregarObjeto("registra", 1);	
	   	accionweb.agregarObjeto("esContratoDocenteB", "1");
	}
	
	public void calculoTotalesSolicitud(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");

        float sumaFija = Util.validaParametro(accionweb.getParameter("sumaFija"),0);
        float semanaCal = Util.validaParametro(accionweb.getParameter("semanaCal"),0);
        String fechaIni = Util.validaParametro(accionweb.getParameter("fechaIni"),"");
        String fechaFin = Util.validaParametro(accionweb.getParameter("fechaFin"),"");
        String tipoTrabajo = Util.validaParametro(accionweb.getParameter("tipoTrabajo"),"");
        
        int diasTrabajados = moduloContratoDocenteB.calculoDiasTrabajados(fechaIni,fechaFin);
        
        double totalMensual = 0.0;
        double totalContrato = 0.0;  
        double calculo = 0.0;
        List<Presw18DTO> totalListaAsignaturas = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaAsignaturas");
        
        
		try {
			if (sumaFija > 0) {
	        	totalMensual = Double.parseDouble(sumaFija+"");
	            calculo = (sumaFija/30.0);
	           	totalContrato = (calculo*Double.parseDouble(diasTrabajados+""));
	        }	
			else { 
			 if(totalListaAsignaturas != null) {
				for (Presw18DTO ss : totalListaAsignaturas) {
					totalContrato = totalContrato + (semanaCal*((ss.getPresac()*ss.getValdoc()) + (ss.getPreano()*ss.getUsadac())));
				}
				totalMensual = ((totalContrato/Double.parseDouble(diasTrabajados+""))*30.0);
			 }
			}
		    accionweb.agregarObjeto("totalContrato", Math.ceil(totalContrato)); // Redondea al entero superior
			accionweb.agregarObjeto("totalMensual", Math.ceil(totalMensual));   // Redondea al entero superior
				 
			
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "calculoTotalesSolicitud.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}	 
		System.out.println(" totalContrato " + totalContrato);
		System.out.println(" totalMensual " + totalMensual);
		accionweb.agregarObjeto("tipoTrabajo", tipoTrabajo); 
		Thread.sleep(1500);
	}
	
		
	synchronized public void registraSolicitud(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		List<Presw18DTO> totalListaAsignaturas = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaAsignaturas");
		
		long rutSol = Util.validaParametro(accionweb.getParameter("rutnum"),0);
		String dvSol = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		long codUni = Util.validaParametro(accionweb.getParameter("codUni"),0);
		long sede = Util.validaParametro(accionweb.getParameter("sede"),0);
		String fechaSolicitudIng = Util.validaParametro(accionweb.getParameter("fechaSolicitudIng"),"");
		String fechaSemIni = Util.validaParametro(accionweb.getParameter("fechaSemIni"),"");
		String fechaSemFin = Util.validaParametro(accionweb.getParameter("fechaSemFin"),"");
		long tipContrato = Util.validaParametro(accionweb.getParameter("tipContrato"),0);
		long semCon = Util.validaParametro(accionweb.getParameter("sem"),0);
		long a�oCon = Util.validaParametro(accionweb.getParameter("annio"),0);
		long sumaFija = Util.validaParametro(accionweb.getParameter("sumaFija"),0);
		String servPres1 = "";
		String servPres2 = "";
		String trabajoEfec =  Util.validaParametro(accionweb.getParameter("trabajoEfec"),"").trim();
		if (trabajoEfec.length() > 80) {
			 servPres1 = trabajoEfec.substring(0,79);
			 trabajoEfec = trabajoEfec.substring(79);
			 if (trabajoEfec.length() > 80) servPres2 = trabajoEfec.substring(0,79);
			 else servPres2 = trabajoEfec.substring(0);
		}
		else servPres1 = trabajoEfec.substring(0,trabajoEfec.length());
		
		long codPla = Util.validaParametro(accionweb.getParameter("codPla"),0);
		long codCar = Util.validaParametro(accionweb.getParameter("cargo"),0);
		long semanaCal = Util.validaParametro(accionweb.getParameter("semCalculo"),0); // por si es tipo de trabajo ACA,DOC,PTM
		long valorMen = Util.validaParametro(accionweb.getParameter("valorMensual"),0);
		long valorTot = Util.validaParametro(accionweb.getParameter("valorContrato"),0);
		String tipTrabajo = Util.validaParametro(accionweb.getParameter("tipTrabajo"),"");
		String nivelHor = Util.validaParametro(accionweb.getParameter("nivelHor"),"");
		long numHora = Util.validaParametro(accionweb.getParameter("hrsSem"),0); // por si es tipo de trabajo OTR
		String codOrg = Util.validaParametro(accionweb.getParameter("codOrg"),"");
		
		int numSolicitud = 0;
		String mensaje = "";
        numSolicitud = moduloContratoDocenteB.getRegistraSolicitud(accionweb.getReq(),rutUsuario,dv);
	    if( numSolicitud > 0){
	       accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa la Solicitud de Contrataci�n de Docencia N� Solicitud: "+ numSolicitud);
	       accionweb.agregarObjeto("registra", 1);
	       accionweb.getSesion().removeAttribute("totalListaAsignaturas");
	       accionweb.agregarObjeto("titulo","Ingreso");
	
	    } else
	      	accionweb.agregarObjeto("mensaje", "No se registr� la Solicitud de Contrataci�n de Docencia.");
	   
	   	accionweb.agregarObjeto("esContratoDocenteB", "1");      
	}
	
	synchronized public void actualizaDatosPersonales(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String paterno = Util.validaParametro(accionweb.getParameter("paterno"),"");
		String materno = Util.validaParametro(accionweb.getParameter("materno"),"");
		String nombre = Util.validaParametro(accionweb.getParameter("nombre"),"");
		long sexo = Util.validaParametro(accionweb.getParameter("sexo"),0);
		long estCivil = Util.validaParametro(accionweb.getParameter("estCivil"),0);
		String domicilio = Util.validaParametro(accionweb.getParameter("domicilio"),"");
		long comuna = Util.validaParametro(accionweb.getParameter("comuna"),0);
		long ciudad = Util.validaParametro(accionweb.getParameter("ciudad"),0);
		long region = Util.validaParametro(accionweb.getParameter("region"),0);
		String fechaNac = Util.validaParametro(accionweb.getParameter("fechaNac"),"");
		long fechaNacFun = Long.parseLong(fechaNac.substring(6)+""+fechaNac.substring(3,5)+""+fechaNac.substring(0,2));
		
		long codNac = Util.validaParametro(accionweb.getParameter("codNac"),0);
		String otraNac = Util.validaParametro(accionweb.getParameter("otraNac"),"");
		long codGra = Util.validaParametro(accionweb.getParameter("codGra"),0);
		long codTit = Util.validaParametro(accionweb.getParameter("codTit"),0);
				
		boolean registra = false;
		
		PreswBean preswbean = new PreswBean("SAJ", 0,0,0,0, rutUsuario);
		preswbean.setRutide(rutUsuario);
		preswbean.setDigide(dv);
	    		
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		presw25DTO.setTippro("SAJ");
		presw25DTO.setRutide(rutnum);
		presw25DTO.setDigide(dvrut);
		presw25DTO.setComen1(paterno);
		presw25DTO.setComen2(materno);
		presw25DTO.setComen3(nombre);
		presw25DTO.setPres01(sexo);
		presw25DTO.setPres02(estCivil);
		presw25DTO.setComen4(domicilio);
		presw25DTO.setPres03(comuna);
		presw25DTO.setPres04(ciudad);
		presw25DTO.setPres05(region);
		presw25DTO.setComen5(otraNac);
		presw25DTO.setPres06(codGra);
		presw25DTO.setPres07(codTit);
		presw25DTO.setPres08(fechaNacFun);
		presw25DTO.setPres09(codNac);
		lista.add(presw25DTO);
		    
	    registra = preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	    if(registra){
	   	    accionweb.agregarObjeto("mensaje", "Se actualiz� en forma exitosa los datos personales.");
	   	    accionweb.agregarObjeto("registra", 1);
	   	   // consultaSolicitud(accionweb);
	    } else
	    	accionweb.agregarObjeto("mensaje", "No se modificaron los datos personales.");
		
	    this.cargaDatosPersonales(accionweb);
	    accionweb.agregarObjeto("esContratoDocenteB", "1");    
    }
	
	synchronized public void actualizaSolicitud(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		List<Presw18DTO> totalListaAsignaturas = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaAsignaturas");
		
		int numSol = Util.validaParametro(accionweb.getParameter("numDoc"),0);
		
		boolean registra = false;
	    
	    registra = moduloContratoDocenteB.getActualizaSolicitud(accionweb.getReq(),rutUsuario,dv,numSol);
	    if(registra){
	   	    accionweb.agregarObjeto("mensaje", "Se actualiz� en forma exitosa la Solicitud de Docencia.");
	   	    accionweb.agregarObjeto("registra", 1);
	   	    accionweb.getSesion().removeAttribute("totalListaAsignaturas");
	   	    //consultaSolicitud(accionweb);
	    } else
	    	accionweb.agregarObjeto("mensaje", "No se modific� la Solicitud de Docencia.");
		
	    accionweb.agregarObjeto("esContratoDocenteB", "1");    
	    accionweb.agregarObjeto("titulo", "Modificaci�n");  
    }
	
	synchronized public void actualizaEstadoSolicitud(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numSol = Util.validaParametro(accionweb.getParameter("numDoc"), 0);	
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");	
	 //	String glosa = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "",""); por si debe agregar el motivo
		
		try { 
			boolean graba = moduloContratoDocenteB.getActualizaEstadoSolicitud(numSol,rutUsuario,dv,"SAP",estado);
			if (estado.trim().equals("I")) {
		        if(graba) accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la Autorizaci�n de la solicitud.");
		        else accionweb.agregarObjeto("mensaje", "No se grab� la Autorizaci�n.");
			}
			else if (estado.trim().equals("R")){
					if(graba) accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el Rechazo de la solicitud.");
			        else accionweb.agregarObjeto("mensaje", "No se grab� el Rechazo.");
			}
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado rechazaAutOrdenCompra.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		Thread.sleep(500);
		//consultaSolicitud(accionweb);
		 accionweb.agregarObjeto("esContratoDocenteB", "1");    
	} 
	
	synchronized public void actualizaEstadoSolicitudDGIP_SEDE(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numSol = Util.validaParametro(accionweb.getParameter("numDoc"), 0);	
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");	
	 //	String glosa = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "",""); por si debe agregar el motivo
		
		try { 
			boolean graba = moduloContratoDocenteB.getActualizaEstadoSolicitud(numSol,rutUsuario,dv,"SAR",estado);
			if (estado.trim().equals("A")) {
		        if(graba) accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la Autorizaci�n de la solicitud.");
		        else accionweb.agregarObjeto("mensaje", "No se grab� la Autorizaci�n.");
			}
			else if (estado.trim().equals("H")) {
					if(graba) accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el Rechazo de la solicitud.");
			        else accionweb.agregarObjeto("mensaje", "No se grab� el Rechazo.");
			      }
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado rechazaAutSolicitud.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		Thread.sleep(500);
		//consultaSolicitud(accionweb);
		 accionweb.agregarObjeto("esContratoDocenteB", "1");    
	} 
	
	
	 synchronized public void eliminaAsignatura(AccionWeb accionweb) throws Exception {
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		String asigAlf = Util.validaParametro(accionweb.getParameter("asigAlf"),"");
		int asigNum = Util.validaParametro(accionweb.getParameter("asigNum"),0);
		String paralelo = Util.validaParametro(accionweb.getParameter("paralelo"),"");
		long sede = Util.validaParametro(accionweb.getParameter("sede"),0);
					
	    moduloContratoDocenteB.getEliminaListaAsignatura(accionweb.getReq());
	   	accionweb.agregarObjeto("mensaje", "Se elimin� la asignatura de la lista.");
			    	
		List<Presw18DTO>  totalListaAsignaturas = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaAsignaturas");
		accionweb.agregarObjeto("esContratoDocenteB", 1);   
		long total = 0;
		if (totalListaAsignaturas != null && totalListaAsignaturas.size() > 0){
			accionweb.agregarObjeto("registra", 1);
		}
		accionweb.agregarObjeto("totalListaAsignaturas", totalListaAsignaturas); 	
		if(tipo > 0)
			 accionweb.agregarObjeto("opcion", tipo);
				
		 accionweb.agregarObjeto("esContratoDocenteB", "1");    
	 }
	 
	 public void cargarDetallePeriodoSemestre(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int anio = Util.validaParametro(accionweb.getParameter("anio"), 0);
			int sem = Util.validaParametro(accionweb.getParameter("sem"), 0);
			String inicio = "";
			String fin = "";
			List<APREF24> lista = (List<APREF24>)accionweb.getSesion().getAttribute("listaSemestre");
			for(APREF24 ss:lista){
				if(ss.getANOAYU() == anio && ss.getSEMEST() == sem){
					if(ss.getDIAINI() < 10)
						inicio = "0" + ss.getDIAINI()+"/";
					else
						inicio = ss.getDIAINI()+"/";
					if(ss.getMESINI() < 10)
						inicio = inicio + "0" + ss.getMESINI()+"/";
					else
						inicio = inicio + ss.getMESINI()+"/";
					inicio = inicio + anio;
					if(ss.getDIAFIN() < 10)
						fin = "0" + ss.getDIAFIN()+"/";
					else
						fin = ss.getDIAFIN()+"/";
					if(ss.getMESFIN() < 10)
						fin = fin + "0" + ss.getMESFIN()+"/";
					else
						fin = fin + ss.getMESFIN()+"/";
					fin = fin + anio;
				
				}
				
					
			}
			accionweb.agregarObjeto("fechaPeriodoSemIni", inicio);	
			accionweb.agregarObjeto("fechaPeriodoSemFin", fin); 
		
			accionweb.agregarObjeto("listaSemestre", lista);	
			accionweb.agregarObjeto("esContratoDocenteB", "1");     
			
		}
	 
	 public void cargarDetalleCargoPlanta(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int codPla = Util.validaParametro(accionweb.getParameter("codPla"), 0);
	
			List<APREF08> lista = moduloContratoDocenteB.getListaCargo(codPla);
			if(lista != null)
				accionweb.getSesion().setAttribute("listaCargo", lista);
			
			accionweb.agregarObjeto("listaCargo", lista);	
			accionweb.agregarObjeto("esContratoDocenteB", "1");     
			
		} 
	 
	 public void cargarSemanaCalculo(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			String tipTrabajo = Util.validaParametro(accionweb.getParameter("tipTrabajo"),"");
			int sede = Util.validaParametro(accionweb.getParameter("sede"),0);
			int a�o = Util.validaParametro(accionweb.getParameter("annio"),0);
			int semestre = Util.validaParametro(accionweb.getParameter("semestre"),0);
			
			int semanaCal = 0;
			if ((tipTrabajo.trim().equals("ACA") || tipTrabajo.trim().equals("DOC") || tipTrabajo.trim().equals("PTM")) &&
				 sede > 0 && a�o > 0 && semestre > 0) {
			   semanaCal = moduloContratoDocenteB.getSemanaCalculo(sede,a�o,semestre);
			   accionweb.agregarObjeto("semanaCal", semanaCal);	
	        }
	        
			accionweb.agregarObjeto("tipoTrabajo", tipTrabajo);	
			accionweb.agregarObjeto("esContratoDocenteB", 1); 
			
		}
	 
	 public void calculoValoresHoras(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			String tipTrabajo = Util.validaParametro(accionweb.getParameter("tipTrabajo"),"");
			
			int sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
			int sumaFija = Util.validaParametro(accionweb.getParameter("sumaFija"), 0);
			int nivelHor = Util.validaParametro(accionweb.getParameter("nivelHor"), 0);
			int horasTeo = Util.validaParametro(accionweb.getParameter("horasTeo"),0);
	        int horasPra = Util.validaParametro(accionweb.getParameter("horasPra"),0);
	        
			int valorPra = 0;
			int valorTeo = 0;
			int valor = 0;
			int valorMinTeo = 0;
			int valorMaxTeo = 0;
			int valorMinPra = 0;
			int valorMaxPra = 0;
			if ((sede == 2 || sede == 3) && sumaFija == 0 && nivelHor > 0) {
				if (tipTrabajo.trim().equals("DOC") || tipTrabajo.trim().equals("PTM") ||
					(tipTrabajo.trim().equals("ACA") && sede == 3)) {
				 
					Vector listaValorHorasPRA = moduloContratoDocenteB.getListaValoresHora(sede, tipTrabajo,"PR");
					Vector listaValorHorasTEO = moduloContratoDocenteB.getListaValoresHora(sede, tipTrabajo,"TE"); 
					
					// pos0-VALNOM-VALNIV1 , pos1-VALMIN-VALNIV2, pos2-VALMAX-VALNIV3, pos3-VALNI4-VALNIV4, pos4-VALNU5-VALNIV5 
					System.out.println(listaValorHorasTEO);
					System.out.println(listaValorHorasTEO);
					
					switch (nivelHor) {
				      case 1:
				    	   valorPra = (listaValorHorasPRA.size()>0?Integer.parseInt(listaValorHorasPRA.get(0)+""):0);
				           valorTeo = (listaValorHorasTEO.size()>0?Integer.parseInt(listaValorHorasTEO.get(0)+""):0);
				           break;
				      case 2:
				    	   valorPra = (listaValorHorasPRA.size()>0?Integer.parseInt(listaValorHorasPRA.get(1)+""):0);
				           valorTeo = (listaValorHorasTEO.size()>0?Integer.parseInt(listaValorHorasTEO.get(1)+""):0);
				           break;
				      case 3:
				    	   valorPra = (listaValorHorasPRA.size()>0?Integer.parseInt(listaValorHorasPRA.get(2)+""):0);
				           valorTeo = (listaValorHorasTEO.size()>0?Integer.parseInt(listaValorHorasTEO.get(2)+""):0);
				           break;
				      case 4:
				    	   valorPra = (listaValorHorasPRA.size()>0?Integer.parseInt(listaValorHorasPRA.get(3)+""):0);
				           valorTeo = (listaValorHorasTEO.size()>0?Integer.parseInt(listaValorHorasTEO.get(3)+""):0);
				           break;
				      default:
				    	   valorPra = (listaValorHorasPRA.size()>0?Integer.parseInt(listaValorHorasPRA.get(4)+""):0);
			               valorTeo = (listaValorHorasTEO.size()>0?Integer.parseInt(listaValorHorasTEO.get(4)+""):0);
				           break;
				      }
				}	
				
				if ( tipTrabajo.trim().equals("ACA") && sede == 2) { 
				    valor = moduloContratoDocenteB.getListaValorNivel(sede, nivelHor);
			        if (valor > 0 && horasPra > 0) valorPra = valor;
			        if (valor > 0 && horasTeo > 0) valorTeo = valor;
				}     
				accionweb.agregarObjeto("soloMuestra", 1);
				accionweb.agregarObjeto("valorPra", valorPra);	
				accionweb.agregarObjeto("valorTeo", valorTeo);
		    }
			
		 	if ((sede == 1 || sede == 4 || sede == 6) && sumaFija == 0 && nivelHor == 0) { 
		 		if (tipTrabajo.trim().equals("DOC") || tipTrabajo.trim().equals("PTM") || tipTrabajo.trim().equals("ACA")) {
		 		    Vector listaValorHorasPRA = moduloContratoDocenteB.getListaValoresMINMAX(sede, tipTrabajo,"PR");
					Vector listaValorHorasTEO = moduloContratoDocenteB.getListaValoresMINMAX(sede, tipTrabajo,"TE");
					valorMinTeo = (listaValorHorasTEO.size()>0?Integer.parseInt(listaValorHorasTEO.get(0)+""):0);
					valorMaxTeo = (listaValorHorasTEO.size()>0?Integer.parseInt(listaValorHorasTEO.get(1)+""):0);
					valorMinPra = (listaValorHorasPRA.size()>0?Integer.parseInt(listaValorHorasPRA.get(0)+""):0);
					valorMaxPra = (listaValorHorasPRA.size()>0?Integer.parseInt(listaValorHorasPRA.get(1)+""):0);
					accionweb.agregarObjeto("valorMinHrsTeorica",valorMinTeo);	
					accionweb.agregarObjeto("valorMaxHrsTeorica",valorMaxTeo);
					accionweb.agregarObjeto("valorMinHrsPractica",valorMinPra);	
					accionweb.agregarObjeto("valorMaxHrsPractica",valorMaxPra);
		 		}
      	
		 	}
			accionweb.agregarObjeto("esContratoDocenteB", 1); 
			
		}
	 
	 public void cargarHorasSemestre(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			String tipTrabajo = Util.validaParametro(accionweb.getParameter("tipTrabajo"),"");
			
			accionweb.agregarObjeto("tipoTrabajo", tipTrabajo);	
			accionweb.agregarObjeto("esContratoDocenteB", 1); 
			
		}
	 
	 public void cargarValorHora(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			String tipTrabajo = Util.validaParametro(accionweb.getParameter("tipTrabajo"),"");
			int sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
			int sumaFija = Util.validaParametro(accionweb.getParameter("sumaFija"), 0);
			int nivelHor = Util.validaParametro(accionweb.getParameter("nivelHor"), 0);
			int horasTeo = Util.validaParametro(accionweb.getParameter("horasTeo"),0);
	        int horasPra = Util.validaParametro(accionweb.getParameter("horasPra"),0);
			
			this.calculoValoresHoras(accionweb);
			
			accionweb.agregarObjeto("esContratoDocenteB", 1); 
			
		}
	 
	 public void imprimeSolicitud(AccionWeb accionweb) throws Exception {
		 int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		 String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		 int numSol = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		
	  	 Presw19DTO preswbean19DTO = new Presw19DTO();
	   	 String titulo = "";
		 String tituloDetalle1 = "";
		 preswbean19DTO.setTippro("SAL"); 
	  	 preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
	  	 preswbean19DTO.setDigide(dv);
		 preswbean19DTO.setNumdoc(new BigDecimal(numSol));
		 
		 Vector vecSolicitudes = new Vector();	
		 Vector vecListaAsign = new Vector();
		 		 
		 String nomcam = "";
		 long valnu1 = 0;
		 long valnu2 = 0;
		 String valalf = "";
		 long rutSol = 0;
		 String dvSol = "";
		 String paterno = "";
		 String materno = "";
		 String nombre = "";
		 long sexo = 0;
		 long estCivil = 0;
		 String domicilio = "";
		 long comuna = 0;
		 String nomComuna = "";
		 long ciudad = 0;
		 String nomCiudad = "";
		 long region = 0;
		 String fechaNac = "";
		 long codNac = 0;
		 String otraNac = "";
		 long codGra = 0;
		 String nomGra = "";
		 long codTit = 0;
		 String nomTit = "";
	  	 long codUni = 0;
		 long sede = 0;
		 String nomSede = "";
		 String fechaSol = "";
		 String fechaIni = "";
		 String fechaFin = "";
		 long tipContrato = 0;
		 long semCon = 0;
		 long a�oCon = 0;
		 long sumaFija = 0;
		 String servPres1 = "";
		 String servPres2 = "";
		 long codPla = 0;
		 String nomPla = "";
		 long codCar = 0;
		 String nomCar = "";
		 long semanaCal = 0;
	   	 long valorMen = 0;
		 long valorTot = 0;
		 String tipTrabajo = "";
		 String nivelHor = "";
		 long numHora = 0;
		 String codOrg = "";
		 String nomOrg = "";
		 String estado = "";
		 
		 List<Cocow36DTO>  listaSolicitud = new ArrayList<Cocow36DTO>();
		 CocowBean cocowBean = new CocowBean();
		 listaSolicitud = cocowBean.buscar_cocow36(preswbean19DTO);
		 if(listaSolicitud != null && listaSolicitud.size() >0 ){
		    for(Cocow36DTO ss: listaSolicitud){
              if(ss.getNomcam().trim().equals("RUTIDE")) 
               	rutSol = ss.getValnu1();
              if(ss.getNomcam().trim().equals("DIGIDE"))
	            dvSol = ss.getValalf().trim();
              if(ss.getNomcam().trim().equals("APEPAT"))
  	            paterno = ss.getValalf().trim();
              if(ss.getNomcam().trim().equals("APEMAT"))
    	        materno = ss.getValalf().trim();
              if(ss.getNomcam().trim().equals("NOMBRE"))
      	        nombre = ss.getValalf().trim();
              if(ss.getNomcam().trim().equals("SEXO"))
 				 sexo = ss.getValnu1();
 			  if(ss.getNomcam().trim().equals("ESTCIV"))
 				 estCivil = ss.getValnu1();
 			  if(ss.getNomcam().trim().equals("DOMICI"))
 				 domicilio = ss.getValalf().trim();
 			 if(ss.getNomcam().trim().equals("COMUNA"))
				 comuna = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("CIUDAD"))
				 ciudad = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("REGION"))
				 region = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("FECNAC"))
			   	 fechaNac = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
			  if(ss.getNomcam().trim().equals("NACION"))
				 codNac = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("DETNAC"))
				  otraNac = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("CODGRA"))
				 codGra = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("CODTIT"))
				 codTit = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("CODUNI"))
				 codUni = ss.getValnu1();
	          if(ss.getNomcam().trim().equals("SUCUR"))
	            sede = ss.getValnu1();
	          if(ss.getNomcam().trim().equals("NOMSUC"))
	            nomSede = ss.getValalf().trim(); 
		      if(ss.getNomcam().trim().equals("FECSOL"))
			   	fechaSol = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
	          if(ss.getNomcam().trim().equals("FECINI"))
				fechaIni = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
		      if(ss.getNomcam().trim().equals("FECFIN"))
				fechaFin = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
		      if(ss.getNomcam().trim().equals("TIPCON"))
			     tipContrato = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("SEMEST"))
		         semCon = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("ANODOC"))
		         a�oCon = ss.getValnu1(); 
		      if(ss.getNomcam().trim().equals("SUMFIJ"))
				 sumaFija = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("SERPR1"))
		    	 servPres1 = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("SERPR2"))
		    	 servPres2 = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("CODPLA"))
			     codPla = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("DESPLA"))
			     nomPla = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("CODCAR"))
				 codCar = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("DESCAR"))
			     nomCar = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("SEMCAL"))
				 semanaCal = ss.getValnu1()/100000;
			  if(ss.getNomcam().trim().equals("VALMES"))
			     valorMen = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("VALTOT"))
			     valorTot = ss.getValnu1();
			  if(ss.getNomcam().trim().equals("TIPTRA"))
				 tipTrabajo = ss.getValalf().trim();
	 		  if(ss.getNomcam().trim().equals("NIVHOR"))
				nivelHor = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("CANHOR"))
		        numHora = ss.getValnu1();
		      if(ss.getNomcam().trim().equals("CODORG"))
			   	codOrg = ss.getValalf().trim();
			  if(ss.getNomcam().trim().equals("DESORG"))
			   	 nomOrg = ss.getValalf().trim();
		      if(ss.getNomcam().trim().equals("ESTADO"))
			    estado = ss.getValalf().trim();
		      
		    }      
	     }

		 String rutFormateado = moduloContratoDocenteB.formateoRut(rutSol+""+dvSol);
		 // Datos funcionario
		 vecSolicitudes.add(numSol);
		 vecSolicitudes.add(rutFormateado);
		 vecSolicitudes.add(paterno);
		 vecSolicitudes.add(materno);
		 vecSolicitudes.add(nombre);
		 vecSolicitudes.add((sexo==0?"Masculino":"Femenino"));
		 vecSolicitudes.add((estCivil==1?"Soltero":(estCivil==2?"Casado":(estCivil==3?"Separado":(estCivil==4?"Viudo":"Sin Informaci�n")))));
		 vecSolicitudes.add(domicilio);
		 vecSolicitudes.add(moduloContratoDocenteB.getNombreComuna(comuna));
		 vecSolicitudes.add(moduloContratoDocenteB.getNombreCiudad(ciudad));
		 vecSolicitudes.add(region);
		 vecSolicitudes.add(fechaNac);
		 vecSolicitudes.add(codNac);
		 vecSolicitudes.add(otraNac);
		 vecSolicitudes.add(moduloContratoDocenteB.getNombreGrado(codGra));
		 vecSolicitudes.add(moduloContratoDocenteB.getNombreTitulo(codGra,codTit));
		 // Datos Solicitud
		 vecSolicitudes.add(codPla);
		 vecSolicitudes.add(nomPla);
		 vecSolicitudes.add(codCar);
		 vecSolicitudes.add(nomCar);
		 vecSolicitudes.add(codUni);
		 vecSolicitudes.add(sede);
		 vecSolicitudes.add(nomSede);
		 vecSolicitudes.add(codOrg);
		 vecSolicitudes.add(nomOrg);
		 vecSolicitudes.add(servPres1+ ""+servPres2);
		 vecSolicitudes.add(fechaSol);
		 vecSolicitudes.add(a�oCon);
		 vecSolicitudes.add(semCon);
		 vecSolicitudes.add(fechaIni);
		 vecSolicitudes.add(fechaFin);
		 vecSolicitudes.add((tipTrabajo.equals("ACA")?"ACA - Acad�mico":(tipTrabajo.equals("DOC")?"DOC - Docente":(tipTrabajo.equals("OTR")?"OTR - Otras":(tipTrabajo.equals("PTM")?"PTM - Part Time":"Sin informaci�n")))));
		 vecSolicitudes.add((tipContrato==1?"1 - Con Previsi�n":(tipContrato==5?"5 - Honorarios":"Sin infromaci�n")));
		 vecSolicitudes.add(sumaFija);
		 vecSolicitudes.add(tipTrabajo);
		 vecSolicitudes.add(semanaCal);
		 vecSolicitudes.add(numHora);
		 vecSolicitudes.add(nivelHor);
		 NumberFormat nf2 = 	NumberFormat.getInstance(Locale.GERMAN); 
		 
		 // Datos para lista asignatura
		 vecSolicitudes.add(nf2.format(valorMen));
		 vecSolicitudes.add(nf2.format(valorTot));
		 vecSolicitudes.add(estado);
		
		 PreswBean preswbean = null;
  		 preswbean = new PreswBean("SAL",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
		 preswbean.setNumdoc(numSol);
			
		 accionweb.getSesion().removeAttribute("totalListaAsignaturas");
		 Collection<Presw18DTO> totalListaAsignaturas = preswbean.consulta_presw18();
		 if(totalListaAsignaturas != null) {
			 String nomAsign = "";
			 long totalTeo = 0;
			 long totalPra = 0;
			 for (Presw18DTO ss : totalListaAsignaturas) {
				totalTeo = 0;
				totalPra = 0;
			    Vector vec2 = new Vector(); 
			    vec2.addElement(ss.getTipmov()+""+ss.getItedoc()+"/"+ss.getIddigi().trim()+" - " +moduloContratoDocenteB.getNombreAsignatura(ss.getTipmov(),ss.getItedoc(),sede));
			   	vec2.addElement((nf2.format(ss.getPresac()/100))+"");
				vec2.addElement((nf2.format(ss.getValdoc()/100))+"");
				vec2.addElement((nf2.format(ss.getPreano()/100))+"");
				vec2.addElement((nf2.format(ss.getUsadac()/100))+"");
			
				totalTeo = ((ss.getPresac()/100)*(ss.getValdoc()/100));
				totalPra = ((ss.getPreano()/100)*(ss.getUsadac()/100));
				if (sumaFija > 0)
				   vec2.addElement(nf2.format(totalTeo+totalPra));
				else  vec2.addElement(nf2.format((totalTeo+totalPra)*semanaCal));
				
		
	 			vecListaAsign.addElement(vec2);
		      }
		 }
		 
		 accionweb.getSesion().setAttribute("vecSolicitudes", vecSolicitudes);
         accionweb.getSesion().setAttribute("vecListaAsign", vecListaAsign);
		 accionweb.agregarObjeto("esContratoDocenteB", "1");
		 
	 }

}
