package cl.utfsm.socdB.mvc;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.velocity.tools.generic.DateTool;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.utfsm.POJO.Producto;
import cl.utfsm.POJO.Sede;
import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.sip.mvc.PresupuestoInterceptor;
import cl.utfsm.sivB.modulo.ModuloValePagoB;

import cl.utfsm.socdB.modulo.ModuloOrdenCompraDecentralizadaB;
import descad.cliente.CocowBean;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw21DTO;
import descad.presupuesto.Presw25DTO;

public class OrdenCompraDecentralizadaInterceptorB  extends HandlerInterceptorAdapter{
	static Logger log = Logger.getLogger(PresupuestoInterceptor.class);
	private ModuloOrdenCompraDecentralizadaB moduloOrdenCompraDecentralizadaB;
	private ModuloValePagoB moduloValePagoB;
	
		
		
	public ModuloValePagoB getModuloValePagoB() {
		return moduloValePagoB;
	}
	public void setModuloValePagoB(ModuloValePagoB moduloValePagoB) {
		this.moduloValePagoB = moduloValePagoB;
	}
	public static Logger getLog() {
		return log;
	}
	public static void setLog(Logger log) {
		OrdenCompraDecentralizadaInterceptorB.log = log;
	}

	public ModuloOrdenCompraDecentralizadaB getModuloOrdenCompraDecentralizadaB() {
		return moduloOrdenCompraDecentralizadaB;
	}
	public void setModuloOrdenCompraDecentralizadaB(
			ModuloOrdenCompraDecentralizadaB moduloOrdenCompraDecentralizadaB) {
		this.moduloOrdenCompraDecentralizadaB = moduloOrdenCompraDecentralizadaB;
	}
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		
	}	
	public void cargarOrdenCompra(AccionWeb accionweb) throws Exception {
		int opcion = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
      	accionweb.agregarObjeto("control", control);
  		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
   		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
   		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
   		
   		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
           accionweb.getSesion().removeAttribute("autorizaProyecto");
        if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
           accionweb.getSesion().removeAttribute("autorizaUCP");
        if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
           accionweb.getSesion().removeAttribute("autorizaDIRPRE");
        if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
    	   accionweb.getSesion().removeAttribute("autorizaFinanzas");
        if(accionweb.getSesion().getAttribute("listaOrganizaciones") != null)
      		accionweb.getSesion().setAttribute("listaOrganizaciones", null);
        if(accionweb.getSesion().getAttribute("totalListaOrganizaciones") != null)
      		accionweb.getSesion().setAttribute("totalListaOrganizaciones", null);
        if(accionweb.getSesion().getAttribute("totalListaProductos") != null)
      		accionweb.getSesion().setAttribute("totalListaProductos", null);
        if(accionweb.getSesion().getAttribute("valorRemanente") != null)
      		accionweb.getSesion().setAttribute("valorRemanente", null);

          	
    	/* se debe buscar segun el usuario si tiene permiso para autorizar*/
        List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
		moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
		listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
    	if(listaUnidades != null && listaUnidades.size() > 0){
    		 for (Presw18DTO ss : listaUnidades){
    			if(ss.getIndprc().trim().equals("Y")){
    				accionweb.agregarObjeto("autorizaProyecto", 1);
    				accionweb.getSesion().setAttribute("autorizaProyecto", 1);
    			}
    			if(ss.getIndprc().trim().equals("X")){
    				accionweb.agregarObjeto("autorizaUCP", 1);
    				accionweb.getSesion().setAttribute("autorizaUCP", 1);
    			}
    			if(ss.getIndprc().trim().equals("Z")){
    				accionweb.agregarObjeto("autorizaDIRPRE", 1);
    				accionweb.getSesion().setAttribute("autorizaDIRPRE", 1);
    			}
    			if(ss.getIndprc().trim().equals("F")){
    				accionweb.agregarObjeto("autorizaFinanzas", 1);
    				accionweb.getSesion().setAttribute("autorizaFinanzas", 1);
    			}
    		 }
    	}
    	
    	if(accionweb.getSesion().getAttribute("valorRemanente") != null) {
      	   accionweb.getSesion().removeAttribute("valorRemanente");
      	   accionweb.getSesion().setAttribute("valorRemanente", 0);
    	}	
    	
    	List<String> listaCampus = moduloOrdenCompraDecentralizadaB.getCampus();
    	accionweb.agregarObjeto("listaCampus", listaCampus);
    	
    	Map<String, String> listaDivisa = moduloOrdenCompraDecentralizadaB.getDivisas();
    	accionweb.agregarObjeto("listaDivisa", listaDivisa);
    	
    	List<Producto> listaProducto = moduloOrdenCompraDecentralizadaB.getProductos();
    	accionweb.agregarObjeto("listaProducto", listaProducto);
    	
        String fechaActual = new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
    	accionweb.agregarObjeto("fechaEntregaIng", fechaActual);
    	accionweb.agregarObjeto("importacion", "N");
    	accionweb.agregarObjeto("obsequio", "N");
    	accionweb.agregarObjeto("divisa", "CLP");
    	
    	
    	if(rutOrigen > 0){
    		this.limpiaSimulador(accionweb);
    	}
    	 		    			
       	accionweb.agregarObjeto("esOrdenCompraDB", 1); // es para mostrar el menu  de Orden Compra
       	accionweb.agregarObjeto("opcion", opcion);
       	accionweb.agregarObjeto("titulo", "Ingreso");
	}
	
public void cargaEdificio(AccionWeb accionweb) throws Exception {
		String campus = Util.validaParametro(accionweb.getParameter("campus")+ "","");
	   	
		List<String> listaEdificio = moduloOrdenCompraDecentralizadaB.getEdificio(campus);
		accionweb.agregarObjeto("listaEdificio", listaEdificio);
	}
	public void cargaPiso(AccionWeb accionweb) throws Exception {
		String campus = Util.validaParametro(accionweb.getParameter("campus")+ "","");
		String edificio = Util.validaParametro(accionweb.getParameter("edificio")+ "","");
	   	
		List<String> listaPiso = moduloOrdenCompraDecentralizadaB.getPiso(campus, edificio);
		accionweb.agregarObjeto("listaPiso", listaPiso);
		
	}
	public void cargaOficina(AccionWeb accionweb) throws Exception {
		String campus = Util.validaParametro(accionweb.getParameter("campus")+ "","");
		String edificio = Util.validaParametro(accionweb.getParameter("edificio")+ "","");
		String piso = Util.validaParametro(accionweb.getParameter("piso")+ "","");
		
		List<String> listaOficina = moduloOrdenCompraDecentralizadaB.getOficina(campus, edificio, piso);
		accionweb.agregarObjeto("listaOficina", listaOficina);
		
	}
		
	public void cargarCodigoEnvio(AccionWeb accionweb) throws Exception {
	 String campus   = Util.validaParametro(accionweb.getParameter("campus"),"");
	 String edificio = Util.validaParametro(accionweb.getParameter("edificio"),"");
	 String piso = Util.validaParametro(accionweb.getParameter("piso"),"");
	 String oficina = Util.validaParametro(accionweb.getParameter("oficina"),"");
	
	 String codigoEntrega = "";
	 String descEntrega = "";
	
	 if (!campus.trim().equals("") && !edificio.trim().equals("") && !piso.trim().equals("") && !oficina.trim().equals("")){
	   List<String> lista = new ArrayList<String>();
	   lista = moduloOrdenCompraDecentralizadaB.getCodigoEnvio(campus,edificio,piso,oficina);
	   
	   if (lista != null && lista.size() > 0){
		    codigoEntrega = lista.get(0).toString();
            descEntrega   = lista.get(1).toString();		      
            accionweb.agregarObjeto("codigoEntrega", codigoEntrega);
            accionweb.agregarObjeto("descEntrega", descEntrega);
	      }  
		}
	  else  accionweb.agregarObjeto("mensaje", "Este c�digo de envio no existe.");
	   	  
	}

	
	
	public void limpiaSimulador(AccionWeb accionweb) throws Exception {
		if (accionweb.getSesion().getAttribute("listaUnidad") != null)
			accionweb.getSesion().setAttribute("listaUnidad", null);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutOrigen  = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen") +"","0"));
		String nomSimulacion = "";
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		
		/*hacer este rut rutUsuario*/
		if(rutOrigen != 0) // lo vuelve a lo original
		{
			rutUsuario = rutOrigen ;
			rutOrigen = 0;
		}
		 
		
		accionweb.getSesion().setAttribute("rutUsuario", rutUsuario);
		accionweb.getSesion().setAttribute("rutOrigen", rutOrigen);
		accionweb.getSesion().setAttribute("nomSimulacion", nomSimulacion);
		accionweb.agregarObjeto("rutOrigen", rutOrigen);
		accionweb.agregarObjeto("nomSimulacion", nomSimulacion);
		
	}
	
	public void verificaRutProveedor(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
	    String rutnum = Util.validaParametro(accionweb.getParameter("rutnum"),"");
		
	    String nombre = "";

		if(!rutnum.equals("")){
			nombre = moduloOrdenCompraDecentralizadaB.getVerificaRut(rutnum);
		   	if(!nombre.trim().equals("")){
	    		accionweb.agregarObjeto("existeProveedor", 1);
	    		accionweb.agregarObjeto("nombreProv", nombre);
	    		accionweb.agregarObjeto("rutProv", rutnum.substring(0,rutnum.length()-1));
	    		accionweb.agregarObjeto("dvProv", rutnum.substring(rutnum.length()-1));
	        } 
		   	else {
		   		accionweb.agregarObjeto("existeProveedor", 0);
	    		accionweb.agregarObjeto("rutProv", "");
		   		accionweb.agregarObjeto("nombreProv", "Este RUT no est� registrado como proveedor. Debe solicitar a FINANZAS su creaci�n.");
		   	}
		}		  		  
		accionweb.agregarObjeto("esOrdenCompraDB", 1);  
		
	}
	

	public void cargarDetalleProducto(AccionWeb accionweb) throws Exception {
		String producto   = Util.validaParametro(accionweb.getParameter("producto"),"");
		String obsequio = Util.validaParametro(accionweb.getParameter("obsequio"),"");
		
		String detalle = "";
		String unidadMed = "";
		String tipoImpuesto = "";
		String cuenta = "";
		String nomCuenta = "";
		
		if (!producto.trim().equals("")){
		   List<String> lista = new ArrayList<String>();
		   lista = moduloOrdenCompraDecentralizadaB.getDetalleProducto(producto, obsequio);
		   
		   if (lista != null && lista.size() > 0){
			    detalle   = lista.get(0).toString();
			    unidadMed = lista.get(1).toString();
			    tipoImpuesto  = lista.get(2).toString();
			    cuenta = lista.get(3).toString();
			    nomCuenta = lista.get(4).toString();
	            accionweb.agregarObjeto("detalle", detalle);
	            accionweb.agregarObjeto("unidadMed", unidadMed );
	            accionweb.agregarObjeto("cuenta", cuenta);
	            accionweb.agregarObjeto("nomCuenta", nomCuenta);
	            accionweb.agregarObjeto("tipoImpuesto", tipoImpuesto);
	            accionweb.agregarObjeto("codProd", producto);
		      }  
			}
		  else  {
			  accionweb.agregarObjeto("unidadMed", "" );
	          accionweb.agregarObjeto("cuenta", "");
	          accionweb.agregarObjeto("nomCuenta", "");
	          accionweb.agregarObjeto("tipoImpuesto", "");
		  }
		   	  
		}
	
	public void cargarValorTotalProducto(AccionWeb accionweb) throws Exception {
		int cantidad = Util.validaParametro(accionweb.getParameter("cantidad"),0);
		long precio   = Util.validaParametro(accionweb.getParameter("precio"),0);
		String tipoImpuesto = Util.validaParametro(accionweb.getParameter("tipoImpuesto"),"");
		String obsequio = Util.validaParametro(accionweb.getParameter("obsequio"),"");
			
		long totalProd = 0;
		long totalTotal = 0;
		long impuesto = 0;
		if (cantidad > 0 && precio > 0){
		   totalProd = precio * cantidad;
		   if (tipoImpuesto.equals("19IV")) 
			    impuesto = (long) (0.19*totalProd);
		   else if (tipoImpuesto.equals("ICIV"))
			       impuesto = (long) (0.24*totalProd);
		   else if (tipoImpuesto.equals("IA35"))
		       impuesto = (long) (0.35*totalProd);
		   else if (tipoImpuesto.equals("IHIV"))
		       impuesto = (long) (0.31*totalProd);
		   else if (tipoImpuesto.equals("ILA3"))
		       impuesto = (long) (0.395*totalProd);
		   else if (tipoImpuesto.equals("ILA1"))
		       impuesto = (long) (0.505*totalProd);
		   else if (tipoImpuesto.equals("ALCI"))
		       impuesto = (long) (0.47*totalProd);
		   else impuesto = 0;
		   
		   totalTotal = totalProd + impuesto;
		   accionweb.agregarObjeto("impuesto", moduloOrdenCompraDecentralizadaB.formateoNumeroConDecimales(Double.parseDouble(impuesto+"")));
		   accionweb.agregarObjeto("totalProd", totalProd);
		   accionweb.agregarObjeto("totalTotal",moduloOrdenCompraDecentralizadaB.formateoNumeroConDecimales(Double.parseDouble(totalTotal+"")));
		}
	}
	
	public void cargarValorRemanente(AccionWeb accionweb) throws Exception {
		long valorAsignado   = Util.validaParametro(accionweb.getParameter("porcAsignado"),0);
		float totalProd = Util.validaParametro(accionweb.getParameter("valorTotal"),0);
		List<Presw18DTO> listaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaOrganizaciones");
		long valorRemanente = Util.validaParametro(accionweb.getSesion().getAttribute("valorRemanente"),0);
		
		if (valorRemanente == 0) {
		   if(accionweb.getSesion().getAttribute("listaOrganizaciones") != null)
			  accionweb.getSesion().removeAttribute("listaOrganizaciones");
	   }
		
	   float suma = 0;
	   float porcentaje = 0; 
	   long remanente = 0;
	   accionweb.getSesion().removeAttribute("valorRemanente");
	   if (listaOrganizaciones != null && listaOrganizaciones.size() > 0) {
		   for (Presw18DTO ss : listaOrganizaciones){
			     suma = suma + ss.getPresac();
		   }
		   porcentaje = (100*suma)/totalProd; // porcentaje ya asignado
		   remanente = 100 - Math.round(porcentaje) - valorAsignado;
		   accionweb.getSesion().setAttribute("valorRemanente", remanente);
		   accionweb.agregarObjeto("valorRemanente",remanente); 
	   }
	   else { // totalListaOrganizaciones != null
		   remanente = 100 - valorAsignado;
		   accionweb.getSesion().setAttribute("valorRemanente", remanente);
		   accionweb.agregarObjeto("valorRemanente",remanente);
	   }
	}
	
	public void cargarOpcionMenu(AccionWeb accionweb) throws Exception {
		int opcion = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		accionweb.agregarObjeto("opcion", opcion);
		
	}
	
	public void cargaResultadoDistribucionProducto(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		String cuenta = Util.validaParametro(accionweb.getParameter("cuenta"),"");	
		String nomCuenta = Util.validaParametro(accionweb.getParameter("nomCuenta"),"");
		long porcAsignado = Util.validaParametro(accionweb.getParameter("porcAsignado"), 0);
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"), "");
		String nomOrganizacion = Util.validaParametro(accionweb.getParameter("nomOrganizacion"),"");
		String producto = Util.validaParametro(accionweb.getParameter("producto"),"");
		float valorTotal = Util.validaParametro(accionweb.getParameter("valorTotal"), 0);
		int opcion = Util.validaParametro(accionweb.getParameter("opcion"), 0);
		accionweb.agregarObjeto("opcion", opcion);
		
		List<Presw18DTO> listaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaOrganizaciones");
		List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
		List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("totalListaProductos");
				
		try {
			 long saldoCuenta = moduloOrdenCompraDecentralizadaB.getBuscaSaldo(organizacion.trim(),cuenta.trim());
			 int secuencia = 0;
			 if(totalListaOrganizaciones == null) 
				 totalListaOrganizaciones = new ArrayList<Presw18DTO>();

			 if(totalListaProductos == null) 
				 secuencia = 1;
			 else {
				 for (Presw25DTO ss : totalListaProductos){
					if (producto.equals(ss.getCodman().trim()))
					   secuencia = ss.getNumreq();
					else  secuencia = ss.getNumreq() + 1;
				 }
			 }
	
			 if(listaOrganizaciones == null) 
				listaOrganizaciones = new ArrayList<Presw18DTO>();
			 
			 float valorAsignado = Math.round((valorTotal*porcAsignado)/100);
				
			  Presw18DTO presw18DTO = new Presw18DTO();
			  presw18DTO.setNummes(secuencia);
			  presw18DTO.setNomtip(organizacion.trim());
			  presw18DTO.setIddigi(nomOrganizacion.trim());
			  presw18DTO.setNompro(cuenta.trim());
			  presw18DTO.setDesite(nomCuenta.trim());
			  presw18DTO.setPresac((long) valorAsignado);
			  presw18DTO.setPresum(saldoCuenta);
			  listaOrganizaciones.add(presw18DTO);
			  totalListaOrganizaciones.add(presw18DTO);
			  accionweb.getSesion().setAttribute("listaOrganizaciones",listaOrganizaciones);
			  accionweb.getSesion().setAttribute("totalListaOrganizaciones",totalListaOrganizaciones);
		
		      
	
		    
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "cargaResultadoDistribucionProducto.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
	 
		accionweb.agregarObjeto("listaOrganizaciones", listaOrganizaciones); 	
		accionweb.agregarObjeto("totalListaOrganizaciones", totalListaOrganizaciones); 
		accionweb.agregarObjeto("totalListaProductos", totalListaProductos); 
    	accionweb.agregarObjeto("registra", 1);	
	   	accionweb.agregarObjeto("esOrdenCompraDB", 1);   
	}

	public void cargaResultadoProducto(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		String producto = Util.validaParametro(accionweb.getParameter("producto"),"");
		String nomProducto = Util.validaParametro(accionweb.getParameter("nomProducto"),"");
		long cantidad = Util.validaParametro(accionweb.getParameter("cantidad"), 0);
		String obsequio = Util.validaParametro(accionweb.getParameter("obsequio"),"");
		long precio = Util.validaParametro(accionweb.getParameter("precio"), 0);
		long impuesto = Util.validaParametro(accionweb.getParameter("impuesto"), 0);
		String cuenta = Util.validaParametro(accionweb.getParameter("cuenta"),"");	
		String detalle = Util.validaParametro(accionweb.getParameter("detalle"),"");
		int opcion = Util.validaParametro(accionweb.getParameter("opcion"), 0);
		accionweb.agregarObjeto("opcion", opcion);
		
		List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("totalListaProductos");
		try {
			int secuencia = 0;
			if(totalListaProductos == null) {
				 totalListaProductos = new ArrayList<Presw25DTO>();
			     secuencia = 1;
			  }
			 else {
				 for (Presw25DTO ss : totalListaProductos){
					secuencia = ss.getNumreq() + 1;
				 }
			}
			
			if(totalListaProductos == null) 
			     totalListaProductos = new ArrayList<Presw25DTO>(); 
			
			  Presw25DTO presw25DTO = new Presw25DTO();
			  presw25DTO.setNumreq(secuencia); // secuencia
			  presw25DTO.setCodman(producto.trim()); // codigo del prodcto
			  presw25DTO.setComen2(nomProducto.trim()); // nombre del prodcto
			  presw25DTO.setPres01(cantidad); // cantidad de productos
			  presw25DTO.setIndmod(obsequio.trim()); // indicador obsequio
			  presw25DTO.setPres02(precio); // precio
			  presw25DTO.setPres03(impuesto); // impuesto
			  presw25DTO.setComen1(cuenta.trim());
			  int largo = detalle.length();
			  if (largo > 40  ) { 
			      presw25DTO.setMotiv1(detalle.substring(0,40));  // detalle
			      detalle = detalle.substring(41,detalle.length());
			      largo = detalle.length();
			      if (largo > 40  ) { 
				      presw25DTO.setMotiv2(detalle.substring(0,40));  // detalle
				      detalle = detalle.substring(41,detalle.length());
				      largo = detalle.length();
				      if (largo > 40  ) { 
					      presw25DTO.setMotiv3(detalle.substring(0,40));  // detalle
					      detalle = detalle.substring(41,detalle.length());
					      largo = detalle.length();
					      if (largo > 40  ) { 
						      presw25DTO.setMotiv4(detalle.substring(0,40));  // detalle
						      detalle = detalle.substring(41,detalle.length());
						      largo = detalle.length();
						      if (largo > 40  ) { 
							      presw25DTO.setMotiv5(detalle.substring(0,40));  // detalle
							      detalle = detalle.substring(41,detalle.length());
							      largo = detalle.length();
							      if (largo > 40  ) { 
								      presw25DTO.setMotiv6(detalle.substring(0,40));  // detalle
								      detalle = detalle.substring(41,detalle.length());
								  } else  presw25DTO.setMotiv6(detalle.trim());   
							  } else  presw25DTO.setMotiv5(detalle.trim());  
						  } else presw25DTO.setMotiv4(detalle.trim());  
					  } else  presw25DTO.setMotiv3(detalle.trim());  
				  } else   presw25DTO.setMotiv2(detalle.trim());  
			  } else  presw25DTO.setMotiv1(detalle.trim());   
			  totalListaProductos.add(presw25DTO);
		
			  
			 List<Presw25DTO> listaOrganizaciones = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listaOrganizaciones"); 
			 if(accionweb.getSesion().getAttribute("listaOrganizaciones") != null)
			    accionweb.getSesion().removeAttribute("listaOrganizaciones");

			 accionweb.getSesion().setAttribute("totalListaProductos",totalListaProductos);
			 
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "cargaResultadoProducto.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}

		accionweb.agregarObjeto("totalListaProductos", totalListaProductos); 	
    	accionweb.agregarObjeto("registra", 1);	
	   	accionweb.agregarObjeto("esOrdenCompraDB", 1);   
		if(accionweb.getSesion().getAttribute("valorRemanente") != null) {
	       accionweb.getSesion().removeAttribute("valorRemanente");
	       accionweb.getSesion().setAttribute("valorRemanente", 0);
	    }	
		
	}
	
	synchronized public void eliminaResultadoSolicitudProd(AccionWeb accionweb) throws Exception {
		String producto = Util.validaParametro(accionweb.getParameter("producto"),"");
		int numsec = Util.validaParametro(accionweb.getParameter("numsec"), 0);
		String cuenta = Util.validaParametro(accionweb.getParameter("cuenta"),"");	
		     
		List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("totalListaProductos");
		Presw25DTO prod = null;
		List<Presw25DTO> listaProd = new ArrayList<Presw25DTO>();
		
		if(totalListaProductos != null && totalListaProductos.size() > 0){
			for (Presw25DTO ss: totalListaProductos){
			  if(ss.getCodman().trim().equals(producto.trim()) &&
			     ss.getNumreq() == numsec &&
			     ss.getComen1().trim().equals(cuenta.trim())) {
				 continue;
	          }    
			  else {
				     prod = new Presw25DTO();
		      	     prod.setNumreq(ss.getNumreq()); // secuencia
				     prod.setCodman(ss.getCodman()); // codigo del prodcto
				     prod.setComen2(ss.getComen2()); // nombre del prodcto
				     prod.setPres01(ss.getPres01()); // cantidad de productos
				     prod.setIndmod(ss.getIndmod()); // indicador obsequio
				     prod.setPres02(ss.getPres02()); // precio
				     prod.setPres03(ss.getPres03()); // impuesto
				     prod.setComen1(ss.getComen1()); 
				     listaProd.add(prod);
			  }
		   }
		 }	
		
		System.out.println(totalListaProductos.size());
		    	
	 	 accionweb.getSesion().removeAttribute("totalListaProductos");
	     accionweb.getSesion().setAttribute("totalListaProductos", listaProd);
	   		 	
	 	accionweb.agregarObjeto("esOrdenCompraDB", 1);    
		accionweb.agregarObjeto("registra", 1);
		totalListaProductos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("totalListaProductos");
		accionweb.agregarObjeto("totalListaProductos", totalListaProductos);
		
		System.out.println(totalListaProductos.size());
		
		accionweb.getSesion().removeAttribute("valorRemanente");
		accionweb.agregarObjeto("valorRemanente", 0); 
	
	}
	
	synchronized public void eliminaResultadoDistribucionProd(AccionWeb accionweb) throws Exception {
		int numsec = Util.validaParametro(accionweb.getParameter("numsec"), 0);
		String cuenta = Util.validaParametro(accionweb.getParameter("cuenta"),"");
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"");
		     
		List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
		Presw18DTO org = null;
		List<Presw18DTO> listaOrg = new ArrayList<Presw18DTO>();
		System.out.println(totalListaOrganizaciones.size());
		try {
		  if(totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
			for (Presw18DTO ss: totalListaOrganizaciones){
				if (ss.getNummes() == numsec &&
					ss.getNompro().trim().equals(cuenta.trim())&&
					(organizacion.trim().equals("") || ss.getNomtip().trim().equals(organizacion.trim()))){  
					continue;        
					
			   }
			   else {
				    org = new Presw18DTO();
					org.setNummes(ss.getNummes());
					org.setNomtip(ss.getNomtip());
					org.setIddigi(ss.getIddigi());
					org.setNompro(ss.getNompro());
					org.setDesite(ss.getDesite());
					org.setPresac(ss.getPresac());
					org.setPresum(ss.getPresum());
				    listaOrg.add(org);
			   }
			}	
		  }
		  accionweb.agregarObjeto("mensaje", "Se elimin� satisfactoriamente."); 
		} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "cargaResultadoDistribucionProducto.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
		 
		//System.out.println(listaOrganizaciones.size());	
		accionweb.getSesion().removeAttribute("totalListaOrganizaciones");
		accionweb.getSesion().setAttribute("totalListaOrganizaciones", listaOrg);
			
	 	accionweb.agregarObjeto("esOrdenCompraDB", 1);    
		accionweb.agregarObjeto("registra", 1);
		totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
		System.out.println(totalListaOrganizaciones.size());
		accionweb.agregarObjeto("totalListaOrganizaciones", totalListaOrganizaciones); 

	}
	
	 synchronized public void registraOrdenCompra(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			
			List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
			List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("totalListaProductos");
			long fechaEntrega = Util.validaParametro(accionweb.getParameter("fechaEntrega"),0);
			String codigoEntrega = Util.validaParametro(accionweb.getParameter("codigoEntrega"),"");
			String importacion = Util.validaParametro(accionweb.getParameter("importacion"),"");
			String divisa = Util.validaParametro(accionweb.getParameter("divisa"),"");
			int rutProveedor = Util.validaParametro(accionweb.getParameter("rutProv"),0);
			String dvProveedor = Util.validaParametro(accionweb.getParameter("dvProv"),"");
			int numeroDoc = Util.validaParametro(accionweb.getParameter("numeroDoc"),0);
			String tipoDoc = Util.validaParametro(accionweb.getParameter("tipoDoc"),"");
			long fechaDoc = Util.validaParametro(accionweb.getParameter("fechaDoc"),0);
			String motivoReg = Util.validaParametro(accionweb.getParameter("motivoReg"),"");
			
		
		    int numOrden = 0;
			String mensaje = "";
	        numOrden = moduloOrdenCompraDecentralizadaB.getRegistraOrdenCompra(accionweb.getReq(),rutUsuario,dv);
		    if(numOrden > 0){
		       accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa la Regularizaci�n de la Orden de Compra, el n�mero interno es: "+ numOrden);
		       if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
					accionweb.agregarObjeto("registra", 1);
			        accionweb.getSesion().removeAttribute("totalListaOrganizaciones");
				    accionweb.getSesion().setAttribute("totalListaOrganizaciones", null);
				    accionweb.getSesion().removeAttribute("totalListaProductos");
					accionweb.getSesion().setAttribute("totalListaProductos", null); 
					accionweb.getSesion().removeAttribute("valorRemanente");
							     
			    }
				this.cargarOrdenCompra(accionweb);
		    } else
		      	accionweb.agregarObjeto("mensaje", "No se registr� la Regularizaci�n de la Orden de Compra.");
		   
		 	accionweb.agregarObjeto("esOrdenCompraDB", 1);      
		}
	
	 public void cargarAutorizaOrdenCompra(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		/*	
			  if(accionweb.getSesion().getAttribute("listaOrganizaciones") != null)
		      		accionweb.getSesion().setAttribute("listaOrganizaciones", null);
		      if(accionweb.getSesion().getAttribute("totalListaOrganizaciones") != null)
		      		accionweb.getSesion().setAttribute("totalListaOrganizaciones", null);
		      if(accionweb.getSesion().getAttribute("totalListaProductos") != null)
		      		accionweb.getSesion().setAttribute("totalListaProductos", null);
		      if(accionweb.getSesion().getAttribute("valorRemanente") != null)
		      		accionweb.getSesion().setAttribute("valorRemanente", null);*/

		
			PreswBean preswbean = null;
			Collection<Presw18DTO> listaOrdenCompra = null;
			String titulo = "";
			String tituloDetalle1 = "";
			String tituloDetalle2 = "";
			String tipcue = "";
			switch (tipo) {
			case  2:// lista de ordenes para modificar
				{preswbean = new PreswBean("LCX",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				titulo = "Modificaci�n";
				break;
			}
			case 3:  case 6:// lista de ordenes descentralizadas para autorizar - anular
				{ preswbean = new PreswBean("LOC",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				  titulo = "Autorizaci�n Responsable de la Organizaci�n";
				  
				  tituloDetalle1 = "Autorizar";
				  tituloDetalle2 = "Rechazar";
				  accionweb.agregarObjeto("opcion2", 6);
				  break;
				}
			
			case  12:// lista de Ingresadores
			{	//int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0);
				String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"");
			    String accion = Util.validaParametro(accionweb.getParameter("accion"),""); 
			    if(!accion.trim().equals("")){
			    	int rutide = Util.validaParametro(accionweb.getParameter("rutide"), 0);
			    	String digide = Util.validaParametro(accionweb.getParameter("digide"),""); 
			    	try {
			    	boolean error = moduloValePagoB.saveDeleteIngresador(rutide, digide, organizacion, accion);
			    	if(error)
			    		accionweb.agregarObjeto("mensaje", "Registro exitoso");
			    	else
			    		accionweb.agregarObjeto("mensaje", "Problemas al registrar.");
				   	Thread.sleep(500);
			    	} catch (Exception e) {
						accionweb.agregarObjeto("mensaje", "No guardado registraCliente.");
						accionweb.agregarObjeto("exception", e);
						e.printStackTrace();
					}
			    }
			
				List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
				moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
				listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
				accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
			  // 	accionweb.agregarObjeto("unidad", unidad);
				accionweb.agregarObjeto("organizacion",organizacion);
				if(!organizacion.trim().equals("")){
			    	preswbean = new PreswBean("OL1",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			    	//preswbean.setCoduni(unidad);
			    	preswbean.setCajero(organizacion);
			    	Collection<Presw25DTO> listaValePago25 = (Collection<Presw25DTO>) preswbean.consulta_presw25();
			    	accionweb.agregarObjeto("hayDatoslista", 1);
			    	accionweb.agregarObjeto("listaValePago25", listaValePago25);
				}
				titulo = "Ingresadores";
			break;
		     }
			case 16:// lista de sedeAutorizadas
			{   
				int sedeLista = Util.validaParametro(accionweb.getParameter("sedeLista"),0);
				List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
		        List<Sede> listaSede = new ArrayList<Sede>();
		        listaSede = moduloOrdenCompraDecentralizadaB.getListaSedeAutorizadas(rutUsuario);
		        titulo = "Autorizaci�n DAF";
		        if (listaSede != null && listaSede.size() > 0) { 
		          accionweb.agregarObjeto("listaSede", listaSede);
		          accionweb.agregarObjeto("titulo", titulo);
		        }
		         
		        if (sedeLista > 0){
		          accionweb.agregarObjeto("sedeLista", sedeLista);	
		          preswbean = new PreswBean("LOY",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");	
		          preswbean.setSucur(sedeLista);
		          listaOrdenCompra = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		       }
		        
		        List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
				moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
				listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
				accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
		        
			 break;
			}
		    case 17:// muestra lista dela consulta
			{ 
				int fechaInicio = Util.validaParametro(accionweb.getParameter("fechaInicio"),0);
				int fechaTermino = Util.validaParametro(accionweb.getParameter("fechaTermino"),0);
				String fechaIni = Util.validaParametro(accionweb.getParameter("fechaIni"),"");
				String fechaTer = Util.validaParametro(accionweb.getParameter("fechaTer"),"");
				//int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0);
				String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"");
				DateTool fechaActual = new DateTool();
				
				if(fechaInicio > 0 && fechaTermino == 0 ){
					fechaActual = new DateTool();
					String dia = fechaActual.getDay()+"";
					if(dia.length() == 1)
						dia = "0" + dia;
					String mes = (fechaActual.getMonth()+1)+"";
					if(mes.length()== 1)
						mes = "0" + mes;
					fechaTermino = Integer.parseInt(String.valueOf(fechaActual.getYear()) + mes + dia);
					fechaTer = dia + "/" + mes + "/" + String.valueOf(fechaActual.getYear());
					
				} 
				
				if(!organizacion.equals("")){
				preswbean = new PreswBean("COD",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				//preswbean.setCoduni(unidad);
				preswbean.setCajero(organizacion);
				preswbean.setFecmov(fechaInicio);
				preswbean.setNumche(fechaTermino);
				listaOrdenCompra = (Collection<Presw18DTO>) preswbean.consulta_presw18();		
				
				}
				titulo = "Consulta";	
				List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
				moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
				listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
				accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
			   	accionweb.agregarObjeto("organizacion", organizacion);
			   	accionweb.agregarObjeto("fechaIni", fechaIni);
			   	accionweb.agregarObjeto("fechaTer", fechaTer);
				break;
			
				}
			

			}
			if(tipo != 4 && tipo != 12 && tipo != 16 && tipo != 17)
			  listaOrdenCompra = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			
			
		///	System.out.println("listaOrdenCompra.size(): "+listaOrdenCompra.size());
		    if(listaOrdenCompra != null && listaOrdenCompra.size() > 0) {
		      List<String> listLugarEntega = new ArrayList<String>();
		      String lugEnt = "";
		      for (Presw18DTO ss: listaOrdenCompra){
		    	  lugEnt = moduloOrdenCompraDecentralizadaB.getDetalleCodigoEntrega(ss.getNomtip().trim());
		    	  listLugarEntega.add(lugEnt);
		     }
		        accionweb.agregarObjeto("listLugarEntega",listLugarEntega);
	        	accionweb.agregarObjeto("hayDatoslista", "1");
		    }	
		    
		    
	        accionweb.agregarObjeto("listaOrdenCompra", listaOrdenCompra);
	        accionweb.agregarObjeto("esOrdenCompraDB", 1);  
	      	accionweb.agregarObjeto("opcion", String.valueOf(tipo));
	    	accionweb.agregarObjeto("opcionMenu", tipo);
	      	accionweb.agregarObjeto("titulo", titulo);
	      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
	    	accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
	      	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	}
	 
    public void consultaOrdenCompra(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			String proceso = Util.validaParametro(accionweb.getParameter("proceso")+ "","");
			String procesoSesion = Util.validaParametro(accionweb.getSesion().getAttribute("procesoSesion")+ "","");
			int sedeLista = Util.validaParametro(accionweb.getParameter("sedeLista"), 0);
			
			String nomIdentificador = "";
			long rutnum = 0;
			String dvrut = "";
			
			Presw19DTO preswbean19DTO = new Presw19DTO();
			String titulo = "";
			String tituloDetalle1 = "";
			//preswbean19DTO.setTippro("OCX"); 
			preswbean19DTO.setTippro((!procesoSesion.equals("")?procesoSesion:(proceso.equals("")?"OCX":proceso)));
			preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
			preswbean19DTO.setDigide(dv);
			preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
			List<Presw21DTO>  listaHistorial = new ArrayList<Presw21DTO>();
		
			String readonly = "";
			/*capturar los datos */
			switch (tipo) {
			case  2: // lista de solicitudes de recuperaci�n para consultar
			{	titulo = "Modifica ";
				tituloDetalle1 = "Modificar";
				readonly = "";
				break;
				}
			case 3: case 6: // lista de solicitudes de recuperaci�n para autorizar
				{
				titulo = "Autorizaci�n Responsable de la Cuenta";
				tituloDetalle1 = "Autorizar";	
				break;
				}
			case 4: // lista de solicitudes de recuperaci�n para consulta
				{
				titulo = "Consulta ";
				tituloDetalle1 = "Consultar";
				break;
				}
			case 7: // Documento Rechazado
			   {
				titulo = "Documento Rechazado";
			    break;
			   }
			case 16: {
				titulo = "Autorizaci�n DAF";
				accionweb.agregarObjeto("sedeLista",sedeLista);
				break;		 
				}   
			}
			
			long valorAPagar = 0;
			String identificadorInterno = "";	
			int identificador = 0; 
			
		    String nomcam = "";
		    long valnu1 = 0;
		    long valnu2 = 0;
		    String valalf = "";
		    String fechaEntregaIng = "";
		    String lugEntrega = "";
		    String codigolugar = "";
		    String campus = "";
		    String edificio = "";
		    String piso = "";
		    String oficina = "";
		    String indImp  = "";
		    String divisa = "";
		    long impuesto = 0;
		    long total = 0;
		    long rutProveedor = 0;
		    String dvProveedor = "";
		    String estado = "";
		    int otro = 0;
			    
		    
		    List<Cocow36DTO>  listaOrdenCompra = new ArrayList<Cocow36DTO>();
			CocowBean cocowBean = new CocowBean();
			listaOrdenCompra = cocowBean.buscar_cocow36(preswbean19DTO);
		    if(listaOrdenCompra != null && listaOrdenCompra.size() >0 ){
		    
		    for(Cocow36DTO ss: listaOrdenCompra){
                if(ss.getNomcam().trim().equals("FECENT")) 
                  	fechaEntregaIng = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
                if(ss.getNomcam().trim().equals("LUGENT"))
	                codigolugar = ss.getValalf().trim();
                if(ss.getNomcam().trim().equals("INDIMP"))
	            	indImp = ss.getValalf().trim();
	            if(ss.getNomcam().trim().equals("CODDIV"))
	                divisa = ss.getValalf().trim();
	 		   	if(ss.getNomcam().trim().equals("VALIMP"))
		    		impuesto = ss.getValnu1();
		    	if(ss.getNomcam().trim().equals("VALTOT"))
		    		total = ss.getValnu1();
		     	if(ss.getNomcam().trim().equals("RUTIDE"))
		    		rutProveedor = ss.getValnu1();
		     	if(ss.getNomcam().trim().equals("DIGIDE"))
		    		dvProveedor = ss.getValalf().trim();
		     	if(ss.getNomcam().trim().equals("ESTADO"))
	            	estado = ss.getValalf().trim();
	            if(ss.getNomcam().trim().equals("HISTORIAL")){
	            	Presw21DTO presw21DTO = new Presw21DTO();
	            	presw21DTO.setValor1(ss.getValnu1()); // es la fecha DE OPERACION, 
	            	presw21DTO.setGlosa1(ss.getValalf()); // es el nombre usuario	            	
	            	presw21DTO.setValor2(ss.getValnu2());// es la unidad
	            	presw21DTO.setCajero(ss.getResval()); //  glosa es la accion, INGRESO, MODIFICACION, AUTORIZACION, PAGO, AUTORIZA FINANZAS
	            	listaHistorial.add(presw21DTO);
	            }
	            
	        }
		    
		   		    
	    	Map<String, String> listaDivisa = moduloOrdenCompraDecentralizadaB.getDivisas();
		    accionweb.agregarObjeto("listaDivisa", listaDivisa);
		    
		    
		    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
		    moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
		    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		    accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
		
			List<Producto> listaProducto = moduloOrdenCompraDecentralizadaB.getProductos();
	    	accionweb.agregarObjeto("listaProducto", listaProducto);
		    
	    	lugEntrega = moduloOrdenCompraDecentralizadaB.getDetalleCodigoEntrega(codigolugar);
	    	
		    if(tipo == 2) {
			   	List<String> listaCampus = moduloOrdenCompraDecentralizadaB.getCampus();
		    	accionweb.agregarObjeto("listaCampus", listaCampus);
		    
		    	int ind = lugEntrega.indexOf("-"); 
				campus = lugEntrega.substring(0,ind);
				lugEntrega = lugEntrega.substring(ind+1);
				ind = lugEntrega.indexOf("-"); 
				edificio = lugEntrega.substring(0,ind);
				lugEntrega = lugEntrega.substring(ind+1);
				ind = lugEntrega.indexOf("-"); 
				piso = lugEntrega.substring(0,ind);
				lugEntrega = lugEntrega.substring(ind+1);
				oficina = lugEntrega.substring(0);
				
				
			    accionweb.agregarObjeto("campus",campus);
			    accionweb.agregarObjeto("edificio",edificio);
			    accionweb.agregarObjeto("piso",piso);
				accionweb.agregarObjeto("oficina",oficina);
				List<String> listaEdificio = moduloOrdenCompraDecentralizadaB.getEdificio(campus);
				accionweb.agregarObjeto("listaEdificio", listaEdificio);
			  	
				List<String> listaPiso = moduloOrdenCompraDecentralizadaB.getPiso(campus, edificio);
				accionweb.agregarObjeto("listaPiso", listaPiso);
				
				List<String> listaOficina = moduloOrdenCompraDecentralizadaB.getOficina(campus, edificio, piso);
				accionweb.agregarObjeto("listaOficina", listaOficina);
				
				if(accionweb.getSesion().getAttribute("valorRemanente") != null) {
				       accionweb.getSesion().removeAttribute("valorRemanente");
				       accionweb.getSesion().setAttribute("valorRemanente", 0);
				}
			  
	    } else {
	    	for(String ss: listaDivisa.keySet()){
	    		if(ss.trim().equals(divisa.trim()))
	    			divisa = ss.trim() +"-"+ listaDivisa.get(ss);
	    		
	    		
	    	}
	    }
		    accionweb.agregarObjeto("listaHistorial", listaHistorial);
		    accionweb.agregarObjeto("descEntrega",lugEntrega);
		    accionweb.agregarObjeto("codigoEntrega",codigolugar);
	    	accionweb.agregarObjeto("fechaEntregaIng",fechaEntregaIng);
		    accionweb.agregarObjeto("rutProveedor",rutProveedor);// rut a pago
			accionweb.agregarObjeto("dvProveedor",dvProveedor);
			String nomProveedor  = moduloOrdenCompraDecentralizadaB.getVerificaRut(rutProveedor+""+dvProveedor);
			String rutFormateado = moduloOrdenCompraDecentralizadaB.formateoRut(rutProveedor) + "-" + dvProveedor;
			accionweb.agregarObjeto("rut", rutFormateado);
			accionweb.agregarObjeto("nombreProv", nomProveedor);
			
			accionweb.agregarObjeto("importacion",indImp);
			accionweb.agregarObjeto("divisa",divisa);
			accionweb.agregarObjeto("impuesto",impuesto);
			accionweb.agregarObjeto("totalProd",total);
			
			accionweb.agregarObjeto("numDoc",numDoc);
	
			PreswBean preswbean = null;
			preswbean = new PreswBean("OCX",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			preswbean.setNumdoc(numDoc);
			
			List<Presw25DTO> totalListaProductos = preswbean.consulta_presw25();
			Collection<Presw18DTO> totalListaOrganizaciones = preswbean.consulta_presw18();
			
			if(tipo == 2 || tipo == 3 || tipo == 16) {
				long saldoCuenta = 0;
				for (Presw18DTO org : totalListaOrganizaciones){
					if(org.getIndprc().trim().equals("D"))	
						 saldoCuenta = moduloOrdenCompraDecentralizadaB.getBuscaSaldo(org.getNomtip().trim(),org.getNompro().trim());
					org.setPresum(saldoCuenta);
					
					for (Presw18DTO cAut : listaUnidades){
						// System.out.println(cAut.getNompro().trim() +"-"+cAut.getDesuni().trim()+"-"+org.getNomtip().trim());
					     if (cAut.getNompro().trim().equals(org.getNomtip().trim()))
					    	 org.setIddigi(cAut.getDesuni().trim());
					}
				}
				
				for (Presw25DTO prod : totalListaProductos){
					for(Producto p: listaProducto){
			    		if(p.getCodigo().trim().equals(prod.getCodman().trim()))
			    			prod.setComen2(p.getDescripcion());
			    		
			    		
			    	}
				}
			}	
			
			//System.out.println("totalListaProductos" + totalListaProductos.size());
			accionweb.agregarObjeto("totalListaProductos",totalListaProductos);
			accionweb.getSesion().setAttribute("totalListaProductos",totalListaProductos);
						
			//System.out.println("totalListaOrganizaciones" + totalListaOrganizaciones.size());
			accionweb.agregarObjeto("totalListaOrganizaciones",totalListaOrganizaciones);
			accionweb.getSesion().setAttribute("totalListaOrganizaciones",totalListaOrganizaciones);
			
			accionweb.agregarObjeto("obsequio", "N");
			accionweb.agregarObjeto("hayDatoslista", "1");     
			if(tipo != 7)
			  		accionweb.agregarObjeto("read",readonly);
		    		
		    } else { 
		    	accionweb.agregarObjeto("mensaje", "No existe orden de compra "+titulo+"con el n�mero " + numDoc);
		    	if(tipo == 4){
		           accionweb.agregarObjeto("pagina", "ordenCompraDMuestraLista.vm");   
		    	}
		    }
		    if(tipo == 17){
				String numeroBanner = moduloOrdenCompraDecentralizadaB.getNumeroOrden(numDoc);
				if (!numeroBanner.trim().equals("0000000000"))
					accionweb.agregarObjeto("numeroBanner", numeroBanner);
			}	 	    	
		   	    
		    accionweb.agregarObjeto("esOrdenCompraDB", "1");
			if (tipo == 6) accionweb.agregarObjeto("opcion","3");
			else accionweb.agregarObjeto("opcion",tipo);
	      	accionweb.agregarObjeto("titulo", titulo);
	      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
	      	accionweb.agregarObjeto("proceso",proceso);
	
	    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	    }
    
    public void imprimeOrdenCompra(AccionWeb accionweb) throws Exception {
    	int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			
		List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
	    moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
	   			
		Presw19DTO preswbean19DTO = new Presw19DTO();
		String titulo = "";
		String tituloDetalle1 = "";
		preswbean19DTO.setTippro("OCX");
		preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
		preswbean19DTO.setDigide(dv);
		preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
		Vector vecHistorial = new Vector();	
		
		long valorAPagar = 0;
		String identificadorInterno = "";	
		int identificador = 0; 
		
	    String nomcam = "";
	    long valnu1 = 0;
	    long valnu2 = 0;
	    String valalf = "";
	    String fechaEntregaIng = "";
	    String lugEntrega = "";
	    String codigolugar = "";
	    String campus = "";
	    String edificio = "";
	    String piso = "";
	    String oficina = "";
	    String indImp  = "";
	    String divisa = "";
	    long impuesto = 0;
	    long total = 0;
	    long rutProveedor = 0;
	    String dvProveedor = "";
	    String estado = "";
	    int otro = 0;
		
	    List<Cocow36DTO>  listaOrdenCompra = new ArrayList<Cocow36DTO>();
		CocowBean cocowBean = new CocowBean();
		listaOrdenCompra = cocowBean.buscar_cocow36(preswbean19DTO);
	    if(listaOrdenCompra != null && listaOrdenCompra.size() >0 ){
	    for(Cocow36DTO ss: listaOrdenCompra){
	    	if(ss.getNomcam().trim().equals("IDMEMO"))
            	identificadorInterno = ss.getValalf();
            if(ss.getNomcam().trim().equals("FECENT")) 
              	fechaEntregaIng = (ss.getValnu1()+"").substring(6)+"/"+(ss.getValnu1()+"").substring(4,6)+"/"+(ss.getValnu1()+"").substring(0,4);
            if(ss.getNomcam().trim().equals("LUGENT"))
                codigolugar = ss.getValalf().trim();
            if(ss.getNomcam().trim().equals("INDIMP"))
            	indImp = ss.getValalf().trim();
            if(ss.getNomcam().trim().equals("CODDIV"))
                divisa = ss.getValalf().trim();
 		   	if(ss.getNomcam().trim().equals("VALIMP"))
	    		impuesto = ss.getValnu1();
	    	if(ss.getNomcam().trim().equals("VALTOT"))
	    		total = ss.getValnu1();
	     	if(ss.getNomcam().trim().equals("RUTIDE"))
	    		rutProveedor = ss.getValnu1();
	     	if(ss.getNomcam().trim().equals("DIGIDE"))
	    		dvProveedor = ss.getValalf().trim();
	     	if(ss.getNomcam().trim().equals("ESTADO"))
            	estado = ss.getValalf().trim();
            if(ss.getNomcam().trim().equals("HISTORIAL")){
            	Vector vec3 = new Vector();
            	vec3.addElement(ss.getValnu1()); // es la fecha DE OPERACION, 			        
            	vec3.addElement(ss.getValalf()); // es el nombre usuario
            	vec3.addElement(ss.getValnu2()); // es la unidad
            	vec3.addElement(ss.getResval()); //  glosa es la accion, INGRESO, MODIFICACION, AUTORIZACION, PAGO, AUTORIZA FINANZAS
            	vecHistorial.addElement(vec3);
            }
            
        }
	           
    	lugEntrega = moduloOrdenCompraDecentralizadaB.getDetalleCodigoEntrega(codigolugar);
    	
    	Map<String, String> listaDivisa = moduloOrdenCompraDecentralizadaB.getDivisas();
    	for(String ss: listaDivisa.keySet()){
    		if(ss.trim().equals(divisa.trim()))
    			divisa = ss.trim() +"-"+ listaDivisa.get(ss);
    	}
    	    
    	accionweb.getSesion().setAttribute("descEntrega",lugEntrega);
    	accionweb.getSesion().setAttribute("codigoEntrega",codigolugar);
    	accionweb.getSesion().setAttribute("fechaEntregaIng",fechaEntregaIng);
    	accionweb.getSesion().setAttribute("rutProveedor",rutProveedor);// rut a pago
		String nomProveedor  = moduloOrdenCompraDecentralizadaB.getVerificaRut(rutProveedor+""+dvProveedor);
		String rutFormateado = moduloOrdenCompraDecentralizadaB.formateoRut(rutProveedor) + "-" + dvProveedor;
		accionweb.getSesion().setAttribute("rut", rutFormateado);
		accionweb.getSesion().setAttribute("nombreProv", nomProveedor);
		
		accionweb.getSesion().setAttribute("importacion",(indImp.trim().equals("S")?"SI":"NO"));
		accionweb.getSesion().setAttribute("divisa",divisa);
		accionweb.getSesion().setAttribute("impuesto",impuesto);
		accionweb.getSesion().setAttribute("totalProd",total);
		
		accionweb.getSesion().setAttribute("numDoc", String.valueOf(numDoc));
		accionweb.getSesion().setAttribute("identificadorInterno", identificadorInterno);
		accionweb.getSesion().setAttribute("opcion",tipo);
	    
		PreswBean preswbean = null;
		preswbean = new PreswBean("OCX",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
		preswbean.setNumdoc(numDoc);
		
		List<Presw25DTO> totalListaProductos = preswbean.consulta_presw25();
		Collection<Presw18DTO> totalListaOrganizaciones = preswbean.consulta_presw18();
		
		Vector vecListaOrganizaciones = null;
		if(totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
			vecListaOrganizaciones = new Vector();
			Vector detalle = null;
			for (Presw18DTO ss: totalListaOrganizaciones){
	  		if (ss.getIndprc().trim().equals("D")) {
				 detalle = new Vector();
				 detalle.addElement(ss.getNummes());
				 for (Presw18DTO cAut : listaUnidades){
				     if (cAut.getNompro().trim().equals(ss.getNomtip().trim()))
				   	  ss.setIddigi(cAut.getDesuni().trim());
					}
				 detalle.addElement(ss.getNomtip()+"-"+ss.getIddigi());
				 detalle.addElement(ss.getNompro()+"-"+ss.getDesite());
				 detalle.addElement(ss.getPresac());
				 detalle.addElement(ss.getPresum());
				 vecListaOrganizaciones.add(detalle);
				} 
		    }
		}
		
		Vector vecListaProductos = null;
		List<Producto> listaProducto = moduloOrdenCompraDecentralizadaB.getProductos();
		if(totalListaProductos != null && totalListaProductos.size() > 0){
			vecListaProductos = new Vector();
			Vector detalle = null;
			for (Presw25DTO ss: totalListaProductos){
				 detalle = new Vector();
				 detalle.addElement(ss.getNumreq());
				 detalle.addElement(ss.getCodman());
				 for(Producto p: listaProducto){
			    	  if(p.getCodigo().trim().equals(ss.getCodman().trim()))
			    		ss.setComen2(p.getDescripcion());
			     }
				 
				 detalle.addElement(ss.getComen2().trim());
				 detalle.addElement((ss.getIndmod().trim().equals("S")?"SI":"NO"));
				 detalle.addElement(ss.getPres01());
				 detalle.addElement(ss.getPres02()*ss.getPres01()); // precio
				 detalle.addElement(ss.getPres03()); // impusesto
				 detalle.addElement((ss.getPres02()*ss.getPres01())+ss.getPres03());
				 vecListaProductos.add(detalle);
		    } 
		}
				
		//System.out.println("totalListaProductos" + totalListaProductos.size());
		accionweb.getSesion().setAttribute("vecListaProductos",vecListaProductos);
					
		//System.out.println("totalListaOrganizaciones" + totalListaOrganizaciones.size());
		accionweb.getSesion().setAttribute("vecListaOrganizaciones",vecListaOrganizaciones);
		accionweb.getSesion().setAttribute("vecHistorial", vecHistorial);
		
		accionweb.agregarObjeto("hayDatoslista", "1");     
		accionweb.agregarObjeto("esOrdenCompraDB", "1");
      	accionweb.agregarObjeto("opcion", tipo);
      	accionweb.agregarObjeto("titulo", titulo);
      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
  
    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));	}
    }
    
    synchronized public void ejecutaAccion(AccionWeb accionweb) throws Exception {
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		String estadoVales = Util.validaParametro(accionweb.getParameter("estadoVales"),"");
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
		String proceso = Util.validaParametro(accionweb.getParameter("proceso"),"");
		int sedeLista = Util.validaParametro(accionweb.getParameter("sedeLista"), 0);
		
		switch (tipo){
		case 2: {
			this.actualizaOrdenCompra(accionweb);
			break;
		}
		case 3: case 8: case 9: case 10: {
			this.autorizaOrdenCompra(accionweb);
			break;
		}
		case 5:{
		//	this.recepcionaValePago(accionweb);
		break;
			
		}
		case 6:{// rechaza autorizaci�n
			this.rechazaAutOrdenCompra(accionweb);
		break;
			
		}
		case 7:{// rechaza Recepci�n
			//this.rechazaRecepValePago(accionweb);
	  	break;
			
		 }
		case 16: {
			this.autorizaDAFOrdenCompra(accionweb);
			break;
		}
		}
		if(!estadoVales.trim().equals(""))
			accionweb.agregarObjeto("estadoVales", estadoVales);
		if(!estado.trim().equals(""))
			accionweb.agregarObjeto("estado", estado);
		if(tipo != 11)
			consultaOrdenCompra(accionweb);
		
	}
    
synchronized public boolean revisaSaldoOrganizacion(List<Presw18DTO> totalListaOrganizaciones) throws Exception {
	 	
		long total = 0;
	 	long saldoCuenta = 0;
	 	boolean sobrepasa = false;
		if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
		    for (Presw18DTO ss : totalListaOrganizaciones) {
		    	if( !ss.getIndprc().trim().equals("T")){
		     	   total = ss.getPresac();
				   saldoCuenta = moduloOrdenCompraDecentralizadaB.getBuscaSaldo(ss.getNomtip().trim(),ss.getNompro().trim());
				                                               // codOrg.          codCuenta que se dejo en Nomtip
				   if(total > saldoCuenta ) {
					   sobrepasa = true;
					   break;
				   }
		      }
		    }	
	    }
		return sobrepasa;
 }

synchronized public boolean revisaAutOrganizacion(List<Presw18DTO> totalListaOrganizaciones, int numdoc, int rutUsuario, String dv, boolean modifica) throws Exception {
 	boolean aut = true;
	if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
	  if (modifica) {	
	      for (Presw18DTO ss : totalListaOrganizaciones) {	
	    	 // System.out.println(ss.getDesite() + "-" + ss.getDesite().trim().equals("AUTORIZADA") ); 
	    	  if (!ss.getDesuni().trim().equals("AUTORIZADA") && !ss.getIndprc().trim().equals("T")) {
	    		  aut = moduloOrdenCompraDecentralizadaB.saveEstadoCuenta(numdoc,ss.getNomtip().trim(),rutUsuario,dv,"A");
	    		  if (!aut) break;
	    	  }	  
		  }
 	 }
	 else {
		   for (Presw18DTO ss : totalListaOrganizaciones) {	
			//  System.out.println(ss.getDesite() + "-" + ss.getDesite().trim().equals("AUTORIZADA") ); 
			   if (!ss.getDesuni().trim().equals("AUTORIZADA") && !ss.getIndprc().trim().equals("T")) {
		   		  aut = false;
		   		  break;
		   	  } 	  
		   }
	 }
	}	  
   return aut;
	}	

synchronized public boolean actualizaSinFinOrganizacion(List<Presw18DTO> totalListaOrganizaciones, int numdoc, int rutUsuario, String dv) throws Exception {
 	long saldoCuenta = 0;
 	boolean sf = true;  // Sin Financiamiento
	if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
	    for (Presw18DTO ss : totalListaOrganizaciones) {
  		   saldoCuenta = moduloOrdenCompraDecentralizadaB.getBuscaSaldo(ss.getNomtip().trim(),ss.getNompro().trim());
		   if(ss.getPresac() > saldoCuenta ) {
	    		if (!ss.getDesuni().trim().equals("APROB.S/FINANCIAM")) 
			      sf = moduloOrdenCompraDecentralizadaB.saveEstadoCuenta(numdoc,ss.getNomtip().trim(),rutUsuario,dv,"B");
		   }	
	     }
      }
	return sf;
 }
 
 synchronized public boolean actualizaConFinOrganizacion(List<Presw18DTO> totalListaOrganizaciones, int numdoc, int rutUsuario, String dv) throws Exception {
	 	long saldoCuenta = 0;
	 	boolean cf = true;  // Sin Financiamiento
		if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
		    for (Presw18DTO ss : totalListaOrganizaciones) {
	  		   saldoCuenta = moduloOrdenCompraDecentralizadaB.getBuscaSaldo(ss.getNomtip().trim(),ss.getNompro().trim());
			   if(saldoCuenta > ss.getPresac()) {
		    		if (!ss.getDesuni().trim().equals("AUTORIZADA")) 
				      cf = moduloOrdenCompraDecentralizadaB.saveEstadoCuenta(numdoc,ss.getNomtip().trim(),rutUsuario,dv,"B");
			   }	
		     }
	      }
		return cf;
	 }
    
    synchronized public void autorizaOrdenCompra(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		String proceso = Util.validaParametro(accionweb.getParameter("proceso")+ "","");
	
		consultaOrdenCompra(accionweb);
		List<Presw18DTO>  totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
		
		 boolean sobrepasaOrg = this.revisaSaldoOrganizacion(totalListaOrganizaciones); // Se revisan solo los saldos de los autorizadores de sus org.
		// si no estan sobrepasados los saldos 
		 if (!sobrepasaOrg){
		  try {	 
			boolean aut =  moduloOrdenCompraDecentralizadaB.saveRecepciona(numdoc, rutUsuario,dv,"AOX");
			Thread.sleep(1500);
			if (aut) {
			 	accionweb.getSesion().setAttribute("procesoSesion", "OCX");
				consultaOrdenCompra(accionweb);
				if(accionweb.getSesion().getAttribute("procesoSesion") != null)
					accionweb.getSesion().removeAttribute("procesoSesion");
					
				totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
				aut = revisaAutOrganizacion(totalListaOrganizaciones,numdoc,rutUsuario,dv,false); // se revisan si todas las org del vale estan autorizadas
				Thread.sleep(1500);
				if (aut ) {
					sobrepasaOrg = this.revisaSaldoOrganizacion(totalListaOrganizaciones); // Se revisan los saldos todas las org del vale.
					if (sobrepasaOrg) accionweb.agregarObjeto("mensaje", "No se puede Autorizar la Orden de Compra por Organizaciones sin saldo suficiente.");
					else accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n de la Orden N� " + numdoc+".");
				}
				else accionweb.agregarObjeto("mensaje", "Hay organizaciones de la Orden sin autorizar.");
			 }
			else accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n.");
		  } catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "Error al autorizar la orden de compra autorizaOrdenCompra.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
		  }
		 }	  
		else accionweb.agregarObjeto("mensaje", "Organizaciones sin saldo suficiente.");
		
		 // MAA 28/12/2017 ya no es necesario porque hay un proceso que autoriza la orden completo si todas las cuentas estan autorizadas
	 /*	boolean estadoOrdenCompra = false;
		if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
		   if (sobrepasa == 0) {
			   estadoOrdenCompra = moduloOrdenCompraDecentralizadaB.saveEstadoOrdenCompra(numdoc,rutUsuario,dv);
			   Thread.sleep(1500);
		   }
		}
		*/
		Thread.sleep(500);
	    cargarAutorizaOrdenCompra(accionweb);
	    
		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
      	
    } 
    
    synchronized public void autorizaDAFOrdenCompra(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		String proceso = Util.validaParametro(accionweb.getParameter("proceso")+ "","");
	
		int sobrepasa = 0;
		boolean estadoOrdenCompra = false;
		accionweb.getSesion().setAttribute("procesoSesion", "OCX");
		consultaOrdenCompra(accionweb);
		if(accionweb.getSesion().getAttribute("procesoSesion") != null)
			accionweb.getSesion().removeAttribute("procesoSesion");
		
		List<Presw18DTO>  totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
		
		 boolean sobrepasaOrg = this.revisaSaldoOrganizacion(totalListaOrganizaciones); // Se revisan solo los saldos de los autorizadores de sus org.
		 boolean aut = false;
		// si no estan sobrepasados los saldos 
		 if (!sobrepasaOrg){
			aut = revisaAutOrganizacion(totalListaOrganizaciones,numdoc,rutUsuario,dv,false); // se autorizan las org de la orden completa
			Thread.sleep(1500);
			sobrepasa = 0;
			if (!aut) {
				try {
					aut = actualizaConFinOrganizacion(totalListaOrganizaciones,numdoc,rutUsuario,dv); //se dejan en estado A las org de la orden que estan con saldo
					Thread.sleep(1500);
					totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
					if (!aut) accionweb.agregarObjeto("mensaje", "Problemas al modificar estado de la Organizaci�n a autorizada.");	
				} catch (Exception e) {
				    accionweb.agregarObjeto("mensaje", "Error al actualizar estado a Autorizada autorizaDAFOrdenCompra.");
				    accionweb.agregarObjeto("exception", e);
				    e.printStackTrace();
			    }	
		    }
		  }	
		  else {
			aut = revisaAutOrganizacion(totalListaOrganizaciones,numdoc,rutUsuario,dv,false); // se autorizan las org del autorizad
		    Thread.sleep(1500);
		  	sobrepasa = 1;
			if (aut ) {
				try {
					aut = actualizaSinFinOrganizacion(totalListaOrganizaciones,numdoc,rutUsuario,dv); //se dejan en estado B las org del vale que estan sin saldo
					Thread.sleep(1500);
					totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
					if (!aut) accionweb.agregarObjeto("mensaje", "Problemas al modificar estado de la Organizaci�n a sin Financiamiento.");
				} catch (Exception e) {
		    	    accionweb.agregarObjeto("mensaje", "Error al actualizar estado a Sin Financiamiento autorizaDAFOrdenCompra.");
			        accionweb.agregarObjeto("exception", e);
			        e.printStackTrace();
		        }	
	        }
	      }	
		
		 boolean graba = moduloOrdenCompraDecentralizadaB.saveRecepciona(numdoc, rutUsuario,dv,"AOY");
		 if(graba) { 
			  if (sobrepasa == 0) 	
			  	   accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n de la Orden de Compra N� " + numdoc+".");
			  else accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n de la Orden de Compra N� " + numdoc+ ", pero no se traspasa a BANNER por Organizaci�n sin saldo.");
			  estadoOrdenCompra = true;
		    }
		    else {
		      accionweb.agregarObjeto("mensaje","No se grab� la autorizaci�n de la Orden de Compra N� " + numdoc+".");
		      estadoOrdenCompra = false;
		  }
		
		try {
		  if (estadoOrdenCompra && sobrepasa == 0) {	
			 String rutDvIngresador = moduloOrdenCompraDecentralizadaB.getRutOperacion(numdoc,"I");
	    	 String rutDvAutorizador = moduloOrdenCompraDecentralizadaB.getRutOperacion(numdoc,"A"); 
	    	 //String rutDvAutorizador = String.valueOf(rutUsuario) + dv; 
	 		Vector vec_COCOF164B = moduloOrdenCompraDecentralizadaB.getDatosCOCOF164B(numdoc);
		   	
		     String fechaEnt = "";
		     String lugarEnt = "";
		     String rutDvProv = "";
		     String codOrgOrden = "";
		     String codDivisa = "";
		     if (vec_COCOF164B.size() > 0) {
		    	 fechaEnt = vec_COCOF164B.get(0)+"";
			     lugarEnt = vec_COCOF164B.get(1)+"";
			     codDivisa =  vec_COCOF164B.get(2)+"";
			     rutDvProv = vec_COCOF164B.get(3)+"" + vec_COCOF164B.get(4)+"";
			     codOrgOrden = vec_COCOF164B.get(5)+"";
				    	 
	    		 Vector vec_datos = new Vector();
	    		 vec_datos = moduloOrdenCompraDecentralizadaB.getDatosProductos(numdoc);
	    		 String datos_parametro = "";
	    		 Vector vec = null;
	    		    		 
	    		 for (int i = 0; i < vec_datos.size(); i++) {
	    			 vec = (Vector) vec_datos.get(i);
	    			 for (int y = 0; y < vec.size(); y++) {
	    				if (y == (vec.size()-1)) datos_parametro = datos_parametro + vec.get(y) + "@"; 
	    				else datos_parametro = datos_parametro + vec.get(y) + "&";
	    			 }
	    		 }
	    		//System.out.println("datos_parametro " + datos_parametro); 
	    		int error = moduloOrdenCompraDecentralizadaB.saveOrdenCompra(rutUsuario,dv,String.valueOf(numdoc),fechaEnt,lugarEnt,codDivisa,
	    				                                                     rutDvProv,codOrgOrden,datos_parametro,rutDvIngresador,rutDvAutorizador);
	    		if(error == -12) accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n del archivo Historial del documento en I-Series.");
	    		else if(error == -13) accionweb.agregarObjeto("mensaje", "Problemas en la actualizaci�n del estado del Documento.");
	    		else if (error < 0) accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n A BANNER.");
	    		else  accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n.");
	     	 }
		  }	
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado autorizaOrdenCompraDescentralizada.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		
		Thread.sleep(500);
	    cargarAutorizaOrdenCompra(accionweb);
	    
		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
      	
    } 
    
	 synchronized public void actualizaOrdenCompra(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		
		List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
		List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("totalListaProductos");
		

	    boolean registra = false;
	    
	    registra = moduloOrdenCompraDecentralizadaB.getActualizaOrdenCompra(accionweb.getReq(),rutUsuario,dv,numdoc);
	    if(registra){
	   	    accionweb.agregarObjeto("mensaje", "Se actualiz� en forma exitosa la orden de compra.");
	   	    if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
				accionweb.agregarObjeto("registra", 1);
			    accionweb.getSesion().removeAttribute("totalListaOrganizaciones");
			    if(totalListaProductos != null)
			    	accionweb.getSesion().removeAttribute("totalListaProductos");
			}
	   	// cargarMenu(accionweb);
	   	 consultaOrdenCompra(accionweb);
	    } else
	    	accionweb.agregarObjeto("mensaje", "No se modific� la orden de compra.");
	    accionweb.agregarObjeto("esOrdenCompraDB", 1);  
	 
		 
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	}
	 
	 synchronized public void rechazaAutOrdenCompra(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);	
			String glosa = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			
			consultaOrdenCompra(accionweb);
			
			String codOrg = "";
			boolean graba = false;
			List<Presw18DTO>  totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
			if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
				for (Presw18DTO ss : totalListaOrganizaciones) {	
				    codOrg = ss.getNomtip().trim();
				    try { 
						graba = moduloOrdenCompraDecentralizadaB.saveRechaza(numdoc, rutUsuario,dv,"ROX", glosa,"",codOrg);
						Thread.sleep(500);
					    if(!graba)
					    	break;
					    } catch (Exception e) {
							accionweb.agregarObjeto("mensaje", "No guardado rechazaAutOrdenCompra.");
							accionweb.agregarObjeto("exception", e);
							e.printStackTrace();
					}
				}
				if(graba)
				   	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el rechazo de la Autorizaci�n.");
				else
				   	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Autorizaci�n.");   
			}
			
			this.cargarAutorizaOrdenCompra(accionweb);
			
		}
	 
	 synchronized public void rechazaAutDAFOrdenCompra(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);	
			String glosa = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "","");
			int sedeLista = Util.validaParametro(accionweb.getParameter("sedeLista"), 0);	
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			
            consultaOrdenCompra(accionweb);
			
			String codOrg = "";
			boolean graba = false;
			List<Presw18DTO>  totalListaOrganizaciones = (List<Presw18DTO>) accionweb.getSesion().getAttribute("totalListaOrganizaciones");
			if (totalListaOrganizaciones != null && totalListaOrganizaciones.size() > 0){
				for (Presw18DTO ss : totalListaOrganizaciones) {	
				    codOrg = ss.getNomtip().trim();
				    try { 
						graba = moduloOrdenCompraDecentralizadaB.saveRechaza(numdoc, rutUsuario,dv,"ROY", glosa,"",codOrg);
						Thread.sleep(500);
						if(!graba)
					    	break;
					    } catch (Exception e) {
							accionweb.agregarObjeto("mensaje", "No guardado rechazaAutDAFOrdenCompra.");
							accionweb.agregarObjeto("exception", e);
							e.printStackTrace();
					}
				}
				if(graba)
				   	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el rechazo de la Autorizaci�n.");
				else
				   	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Autorizaci�n.");   
			}
			
			this.cargarAutorizaOrdenCompra(accionweb);
			
		}
	
	 synchronized public void eliminaOrdenCompra(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
	     	boolean resultado = moduloOrdenCompraDecentralizadaB.getEliminaOrdenCompra(rutUsuario,dv,numdoc,"NRX","");
		    if(resultado)
			   accionweb.agregarObjeto("mensaje", "Qued� anulada la Orden de Compra");
		
		    Thread.sleep(500);
			this.cargarAutorizaOrdenCompra(accionweb);
		
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
				accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
			if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
				accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
			if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
				accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
			if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			   accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
				
		}
}
