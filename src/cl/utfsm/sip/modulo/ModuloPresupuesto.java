package cl.utfsm.sip.modulo;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.velocity.tools.generic.MathTool;

import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.auditoria.AuditoriaServicio;
import cl.utfsm.base.auditoria.DetalleServicio;
import cl.utfsm.base.auditoria.TipoAccionServicio;
import cl.utfsm.base.util.Util;
import cl.utfsm.sip.PRESW25;
import cl.utfsm.sip.PeriodoProcesos;
import cl.utfsm.sip.TipoCuenta;
import cl.utfsm.sip.TipoPresupuesto;
import cl.utfsm.sip.TipoUnidad;
import cl.utfsm.sip.datos.PresupuestoDao;
import descad.cliente.CocofBean;
import descad.cliente.CocowBean;
import descad.cliente.Ingreso_Documento;
import descad.cliente.MD5;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Cocof17DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw25DTO;

public class ModuloPresupuesto {
	PresupuestoDao presupuestoDao;
	
	public void agregaUnidad(HttpServletRequest req, Collection<Presw18DTO> lista){
//	public void agregaUnidad(HttpServletRequest req, Collection<Presw18> lista){
		HttpSession sesion = req.getSession();
	/*esto es para probar 
		Presw18DTO lista2 = new Presw18DTO();
		lista2.setCoduni(340610);
		lista2.setDesuni("DEPARTAMENTO DE INDUSTRIA, OPERACI�N");
		lista.add(lista2);
		/*lista2 = new Presw18DTO();
		lista2.setCoduni(111319);
		lista2.setDesuni("INGRESOS PROPIOS UNID. ACADEMICAS");
		lista.add(lista2);*/
		/*hasta ac�*/
		
		//sesion.setAttribute("listaUnidad",lista);
		sesion.setAttribute("listaOrganizacion",lista);
	}
	
	public void agregaUsuario(HttpServletRequest req, int rutUsuario, String funcionario, int codigoPerfil){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("rutUsuario",rutUsuario);
		sesion.setAttribute("funcionario",funcionario);
		sesion.setAttribute("codigoPerfil", codigoPerfil);
		
	}
	public PresupuestoDao getPresupuestoDao() {
		return presupuestoDao;
	}
	public void setPresupuestoDao(PresupuestoDao presupuestoDao) {
		this.presupuestoDao = presupuestoDao;
	}

    public void saveAuditoriaServicio (AuditoriaServicio auditoriaServicio){
    	presupuestoDao.saveAuditoriaServicio(auditoriaServicio);
    }
	public DetalleServicio getDetalleServicio(Long id){
		return presupuestoDao.getDetalleServicio(id);
	}
	 public TipoAccionServicio getTipoAccionServicio(int codigo){
		 return presupuestoDao.getTipoAccionServicio(codigo);
	 }
	public void agregaResulPresup(HttpServletRequest req, List<Presw25DTO> lista){
				HttpSession sesion = req.getSession();
				sesion.setAttribute("resultadoPresup",lista);
				
	}
	public void agregaResulVacante(HttpServletRequest req, List<PRESW25> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresupVacantes",lista);
		
}
	
	public void agregaResulPresupActualiza(HttpServletRequest req, List<PRESW25> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresup",lista);
		
}
	public void agregaResultadoPresup(HttpServletRequest req, List<Presw25DTO> lista) {
		/*  MAA itedoc por motiv2 */
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresup",lista);
	   List<Presw25DTO> listaOriginal = new ArrayList<Presw25DTO> ();
        
		for (Presw25DTO ss : lista){
			Presw25DTO obj = new Presw25DTO();
			obj.setAnopre(ss.getAnopre());
			// obj.setItedoc(ss.getItedoc());
			obj.setMotiv2(ss.getMotiv2());
			//obj.setCoduni(ss.getCoduni());
			obj.setMotiv1(ss.getMotiv1());
			obj.setPres01(ss.getPres01());
			obj.setPres02(ss.getPres02());
			obj.setPres03(ss.getPres03());
			obj.setPres04(ss.getPres04());
			obj.setPres05(ss.getPres05());
			obj.setPres06(ss.getPres06());
			obj.setPres07(ss.getPres07());
			obj.setPres08(ss.getPres08());
			obj.setPres09(ss.getPres09());
			obj.setPres10(ss.getPres10());
			obj.setPres11(ss.getPres11());
			obj.setPres12(ss.getPres12());
			obj.setComen1(ss.getComen1());
			obj.setComen2(ss.getComen2());
			obj.setComen3(ss.getComen3());
			obj.setComen4(ss.getComen4());
			obj.setComen5(ss.getComen5());
			obj.setComen6(ss.getComen6());
			obj.setRutide(ss.getRutide());
			obj.setDigide(ss.getDigide());
			obj.setDesite(ss.getDesite());
			obj.setNumreq(ss.getNumreq());
			obj.setIndexi(ss.getIndexi());
			obj.setTotpre(ss.getTotpre());
			obj.setRutide(ss.getRutide());
			obj.setDigide(ss.getDigide());
			obj.setNumreq(ss.getNumreq());
            obj.setRespue(ss.getRespue());
			listaOriginal.add(obj);
		}
	
		sesion.setAttribute("Original",listaOriginal);
		
}

	public void agregaRegistraMatriz(HttpServletRequest req, List<Presw25DTO> lista,List<Presw25DTO> lista2){
		// MAA cambia unidad a organizacion
		HttpSession sesion = req.getSession();
		List<Presw25DTO> listaPRP = new ArrayList();
		List<Presw25DTO> listaCPR = new ArrayList();
		String indexi = Util.validaParametro(req.getParameter("indexi"), "");
		//int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		String organizacion = Util.validaParametro(req.getParameter("organizacion"),"");
		for (Presw25DTO ss : lista){
			Presw25DTO presw25 = ss;
			presw25.setTippro("PRP");
			presw25.setIndexi(indexi);
			listaPRP.add((Presw25DTO) presw25);
			}
		for (Presw25DTO ss : lista2){
			Presw25DTO presw25 = ss;
			// if(ss.getCoduni() == unidad)
			if(ss.getComen4().trim().equals(organizacion.trim())) 
				presw25.setIndexi(indexi);
			
			listaCPR.add((Presw25DTO) presw25);
			}
		sesion.setAttribute("resultadoPresup",listaPRP);
		sesion.setAttribute("resultadoCPR", listaCPR);
		
}
	public List<Presw25DTO> getListaPresw25(String tipo, int rutUsuario, int anno, String dv, int sucur, int codUni, int numDoc){
    List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
    Presw25DTO presw25DTO = new Presw25DTO();
	PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
	preswbean.setDigide(dv);
	if(anno > 0)
		preswbean.setAnopar(anno);	
	if(sucur > 0)
		preswbean.setSucur(sucur);
	if(codUni > 0)
		preswbean.setCoduni(codUni);
	if(numDoc > 0)
		preswbean.setNumdoc(numDoc);
	Collection<Presw25DTO> listaPresw25 = (Collection<Presw25DTO>) preswbean.consulta_presw25();
	MathTool mathtool = new MathTool();
	for (Presw25DTO ss : listaPresw25){
		presw25DTO = new Presw25DTO();
		presw25DTO.setMespre(ss.getMespre());
		presw25DTO.setCoduni(ss.getCoduni());
		presw25DTO.setComen1(ss.getComen1());
		/* Vanessa dice que no se debe dividir por 1 mill�n los valores, para asi tener valores m�s exactos
		 * presw25DTO.setPres01(mathtool.round(mathtool.div(ss.getPres01(),1000000)));
		presw25DTO.setPres02(mathtool.round(mathtool.div(ss.getPres02(),1000000)));
		presw25DTO.setPres03(mathtool.round(mathtool.div(ss.getPres03(),1000000)));
		presw25DTO.setPres04(mathtool.round(mathtool.div(ss.getPres04(),1000000)));
		presw25DTO.setPres05(mathtool.round(mathtool.div(ss.getPres05(),1000000)));
		presw25DTO.setPres06(mathtool.round(mathtool.div(ss.getPres06(),1000000)));
		presw25DTO.setPres07(mathtool.round(mathtool.div(ss.getPres07(),1000000)));
		presw25DTO.setPres08(mathtool.round(mathtool.div(ss.getPres08(),1000000)));
		presw25DTO.setPres09(mathtool.round(mathtool.div(ss.getPres09(),1000000)));
		presw25DTO.setPres11(mathtool.round(mathtool.div(ss.getPres11(),1000000)));
		presw25DTO.setPres12(mathtool.round(mathtool.div(ss.getPres12(),1000000)));
		*/
		presw25DTO.setPres01(ss.getPres01());
		presw25DTO.setPres02(ss.getPres02());
		presw25DTO.setPres03(ss.getPres03());
		presw25DTO.setPres04(ss.getPres04());
		presw25DTO.setPres05(ss.getPres05());
		presw25DTO.setPres06(ss.getPres06());
		presw25DTO.setPres07(ss.getPres07());
		presw25DTO.setPres08(ss.getPres08());
		presw25DTO.setPres09(ss.getPres09());
		presw25DTO.setPres11(ss.getPres11());
		presw25DTO.setPres12(ss.getPres12());
		presw25DTO.setComen2(ss.getComen2());
	/*	segun Pedro debiera multiplicarse al recibir y dividirse al mostrar, por lo tanto no har� nada
	 * 5/05/2011
	 * presw25DTO.setPres10(mathtool.mul(ss.getPres10(),100).longValue());
		presw25DTO.setPres10(mathtool.mul(ss.getValuni(),100).longValue());*/
		presw25DTO.setPres10(ss.getPres10());
		presw25DTO.setValuni(ss.getValuni());
		
		presw25DTO.setCoduni(ss.getCoduni());
		presw25DTO.setTotpre(ss.getTotpre());
		lista.add(presw25DTO);
	}
		
	return lista;
	}
	
	public void agregaResultadoPresupSesion(HttpServletRequest req, List<Presw25DTO> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresupNomina",lista);
		
}
	public void agregaResultadoCPRSesion(HttpServletRequest req, List<Presw25DTO> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoCPR",lista);
		
}
	public void agregaResultadoVacanteSesion(HttpServletRequest req, List<Presw25DTO> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresupVacante",lista);
		
}
	public void agregaResultadoPresupRemSesion(HttpServletRequest req, List<Presw25DTO> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresupRemuneracion",lista);
		
}
	public void agregaPresupRequerimientoSesion(HttpServletRequest req, List<Presw25DTO> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("presupItemreq",lista);
		
}
	public void agregaPresupOtrosSesion(HttpServletRequest req, List<Presw25DTO> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresupIngresos",lista);
		
}
	public void agregaDetalleItemReqSesion(HttpServletRequest req, List<Presw25DTO> lista, List<Presw25DTO> lista2){
		HttpSession sesion = req.getSession();
		int rutUsuario = Util.validaParametro(Integer.parseInt(req.getSession().getAttribute("rutUsuario")+ ""),0);
		int a�o = Util.validaParametro(req.getParameter("anno"), 0);
		int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		int mes = Util.validaParametro(req.getParameter("mes"), 0);
		int sucur = Util.validaParametro(req.getParameter("sucur"), 0);
		int numrq = 0;
		Long totalMensual = Util.validaParametro(req.getParameter("total"), new Long(0));
		List<Presw25DTO> lista3 =  new ArrayList<Presw25DTO>();
		List<Presw25DTO> lista4 =  new ArrayList<Presw25DTO>();
		
		
			
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		
		
		
		/*primero verificar si es requerimiento nuevo, se coloca como n�mero 1000 */
		if(sucur == 0){
			if(lista2.size() > 0) {
			for (Presw25DTO ss : lista2){
				if(ss.getNumreq() > numrq)
					numrq = ss.getNumreq() ;
			}
			numrq++;
			} else numrq = 1000;
			/* y agregarlo a la lista en lista3 */
			for (Presw25DTO ss : lista){
				ss.setNumreq(numrq);
				ss.setValpr1(1); // esto es solo para marcar que es nuevo requerimiento
				ss.setCanman(0);
				lista3.add(ss);
			}
		} else { 
			numrq = sucur;
			for (Presw25DTO ss : lista){
				lista3.add(ss);
			}
		}
		
		boolean agrega = false;
		/*recorre la lista de sesi�n y ve si ya existe sino lo agrega a lista4 que tiene la suma final*/
	    if(lista2 != null && lista2.size() > 0){	    
		for (Presw25DTO ss : lista2){	
			if(ss.getNumreq() == numrq && ss.getItedoc() == itedoc){
			   for (Presw25DTO rs : lista3){
				   if(rs.getCoduni() == ss.getCoduni() && rs.getItedoc() == ss.getItedoc() && rs.getNumreq() == ss.getNumreq() && rs.getMespre() == ss.getMespre() && rs.getCodman() == ss.getCodman()){
						lista4.add(rs);
						agrega = true;
				   }	
						
				}
			} else { lista4.add(ss);
			         
			}
				} 
			} else {
				   for (Presw25DTO rs : lista3){
					  lista4.add(rs);							
					}
				   agrega = true;
			}
		
		 if(!agrega){
			   for (Presw25DTO rs : lista3){
					  lista4.add(rs);							
					}
		 }
	    
	   sesion.setAttribute("listaPresw25",lista3);
		sesion.setAttribute("resultadoDetalleItemReq",lista4);
		
}
	public void agregaResultadoPresupNomina(HttpServletRequest req, List<Presw25DTO> lista, List<Presw25DTO> lista2){
		HttpSession sesion = req.getSession();
		List<Presw25DTO> listaCOP = new ArrayList();
		List<Presw25DTO> listaCPR = new ArrayList();
		if(lista.size() > 0){
		String motiv1 = Util.validaParametro(req.getParameter("motiv1"), "");
		String motiv2 = Util.validaParametro(req.getParameter("motiv2"), "");
		String motiv3 = Util.validaParametro(req.getParameter("motiv3"), "");
		String motiv4 = Util.validaParametro(req.getParameter("motiv4"), "");
		String motiv5 = Util.validaParametro(req.getParameter("motiv5"), "");
		String motiv6 = Util.validaParametro(req.getParameter("motiv6"), "");
		String tipcon = Util.validaParametro(req.getParameter("tipcon"), "");
		String area1 = Util.validaParametro(req.getParameter("area1"), "");
		String area2 = Util.validaParametro(req.getParameter("area2"), "");
		String desite = Util.validaParametro(req.getParameter("desite"), "");
		String indexi = Util.validaParametro(req.getParameter("indexi"), "");
		//int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		String organizacion = Util.validaParametro(req.getParameter("organizacion"), "");
	
	
	/*	String motiv= Util.validaParametro(req.getParameter("motiv"), "");
		if(motiv.length() <= 40)
		  motiv1 = motiv;
		else {
			motiv1 = motiv.substring(0,40);
			if(motiv.length() <= 80)
				motiv2 = motiv.substring(40);
			else {
				motiv2 = motiv.substring(40,80);
				if(motiv.length() <= 120)
					motiv3 = motiv.substring(80);
				else {
					motiv3 = motiv.substring(80,120);
					if(motiv.length() <= 160)
						motiv4 = motiv.substring(120);
					else {
						motiv4 = motiv.substring(120, 160);
						if(motiv.length() <= 200)
							motiv5 = motiv.substring(160);
						else {
							motiv5 = motiv.substring(160,200);
							if(motiv.length() <= 240)
						       motiv6 = motiv.substring(200);
							else {
							   motiv6 = motiv.substring(200,240);
							   if(motiv.length() <= 280) 
								   tipcon = motiv.substring(240);
							   else {
								   tipcon = motiv.substring(240,280);
								   if(motiv.length() <= 320) 
									   area1 = motiv.substring(280);
								   else {
									   area1 = motiv.substring(280,320);
									   if(motiv.length() <= 360) 
										   area2 = motiv.substring(320);
									   else 
										   desite = motiv.substring(360,400);
										
									   
								   }
							   }
							}
						}
					}
				}
			}
		}*/
		int i=0;
		for (Presw25DTO ss : lista){
			i++;
			String respue = Util.validaParametro(req.getParameter("respue"+i), "");
			String comen1 = Util.validaParametro(req.getParameter("comen1"+i), "");
			String comen2 = Util.validaParametro(req.getParameter("comen2"+i), "");
			String comen3 = Util.validaParametro(req.getParameter("comen3"+i), "");
			String comen4 = Util.validaParametro(req.getParameter("comen4"+i), "");
			String comen5 = Util.validaParametro(req.getParameter("comen5"+i), "");
			String comen6 = Util.validaParametro(req.getParameter("comen6"+i), "");
			
			Presw25DTO presw25 = ss;
			
			presw25.setTippro("COP");
			presw25.setRespue(respue);
			presw25.setComen1(comen1);
			presw25.setComen2(comen2);
			presw25.setComen3(comen3);
			presw25.setComen4(comen4);
			presw25.setComen5(comen5);
			presw25.setComen6(comen6);
			presw25.setMotiv1(motiv1);
			presw25.setMotiv2(motiv2);
			presw25.setMotiv3(motiv3);
			presw25.setMotiv4(motiv4);
			presw25.setMotiv5(motiv5);
			presw25.setMotiv6(motiv6);
			presw25.setTipcon(tipcon);
			presw25.setArea1(area1);
			presw25.setArea2(area2);
			presw25.setDesite(desite);
			presw25.setIndexi(indexi);
			presw25.setCodman(organizacion);
			listaCOP.add((Presw25DTO) presw25);
		}
		for (Presw25DTO ss : lista2){
			Presw25DTO presw25 = ss;
			//if(ss.getCoduni() == unidad)
			if(ss.getComen4().trim().equals(organizacion.trim()))
				presw25.setIndexi(indexi);
			listaCPR.add((Presw25DTO) presw25);
			}
			sesion.setAttribute("resultadoCPR", listaCPR);
		}
		sesion.setAttribute("resultadoPresupNomina",listaCOP);		
}
	public void agregaResultadoPresupRemuneracion(HttpServletRequest req, List<Presw25DTO> lista, List<Presw25DTO> lista2){
		HttpSession sesion = req.getSession();
		List<Presw25DTO> listaASP = new ArrayList();
		List<Presw25DTO> listaCPR = new ArrayList();
		String indexi = Util.validaParametro(req.getParameter("indexi"), "");
		int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		
		if(lista.size() > 0){
				
		int i=0;
		for (Presw25DTO ss : lista){
			i++;
			String valor1 = Util.validaParametro(req.getParameter("valpr1_"+i), "0");
			String valor2 = Util.validaParametro(req.getParameter("valpr2_"+i), "0");
			valor1 = valor1.replace(".", "");
			valor2 = valor2.replace(".", "");
			int valpr1 = Integer.parseInt(valor1);
			int valpr2 = Integer.parseInt(valor2);
			
			Presw25DTO presw25 = ss;
			
			presw25.setTippro("ASP");
			presw25.setValpr1(valpr1);
			presw25.setValpr2(valpr2);
			presw25.setIndexi(indexi);
			listaASP.add((Presw25DTO) presw25);
		}
		for (Presw25DTO ss : lista2){
			Presw25DTO presw25 = ss;
			if(ss.getCoduni() == unidad)
				presw25.setIndexi(indexi);
			listaCPR.add((Presw25DTO) presw25);
			}
			sesion.setAttribute("resultadoCPR", listaCPR);
		}
		sesion.setAttribute("resultadoPresupRemuneracion",listaASP);		
}
	public void agregaItemReqDet(HttpServletRequest req, List<Presw25DTO> lista, List<Presw25DTO> presupItemreq , List<Presw25DTO> listarResultadoPresup){
		                                     //            resultadoDetalleItemReq, presupItemreq, listarResultadoPresup
		HttpSession sesion = req.getSession();
		List<Presw25DTO> listaRQP = new ArrayList();
		List<Presw25DTO> listaResultado = new ArrayList();
		
		String area1 = Util.validaParametro(req.getParameter("area1"),"");
		String area2 = Util.validaParametro(req.getParameter("area2"),"");
		String comen1 = Util.validaParametro(req.getParameter("comen1"),"");
		String comen2 = Util.validaParametro(req.getParameter("comen2"),"");
		String comen3 = Util.validaParametro(req.getParameter("comen3"),"");
		String comen4 = Util.validaParametro(req.getParameter("comen4"),"");
		String comen5 = Util.validaParametro(req.getParameter("comen5"),"");
		String comen6 = Util.validaParametro(req.getParameter("comen6"),"");
		String motiv1 = Util.validaParametro(req.getParameter("motiv1"),"");
		String motiv2 = Util.validaParametro(req.getParameter("motiv2"),"");
		String motiv3 = Util.validaParametro(req.getParameter("motiv3"),"");
		String motiv4 = Util.validaParametro(req.getParameter("motiv4"),"");
		String motiv5 = Util.validaParametro(req.getParameter("motiv5"),"");
		String motiv6 = Util.validaParametro(req.getParameter("motiv6"),"");
		Long total = Util.validaParametro(req.getParameter("totalRequ"),new Long(0));
		int mes = Util.validaParametro(req.getParameter("mes"),0);
		int numreq = Util.validaParametro(req.getParameter("sucur"), 0);
		int item = Util.validaParametro(req.getParameter("itedoc"), 0);
		int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		int anno = Util.validaParametro(req.getParameter("anno"), 0);
		Long totalTotal = Util.validaParametro(req.getParameter("totalMes"), new Long(0));
		int i = Util.validaParametro(req.getParameter("item"), 0);
		int itemElimina = Util.validaParametro(req.getParameter("itemElimina"), 0);
		if(itemElimina > 0) i = itemElimina;
		int j = 0;
		long pres03 = 0;
		long pres01 = 0;
		String fecha = "";
		int pres02 = 0;
			   
		int valpr = 0;
		String desite = "";
		String desuni = "";
		int canman = 0;
		long valorReq = 0;
		if(lista.size() > 0){
		for (Presw25DTO ss : lista){
			 pres02 = 0;
			 pres03 = 0;
			 pres01 = 0;
				
			if(ss.getNumreq() == numreq && ss.getMespre() == mes  ){
				//System.out.println(ss.getItedoc());
				j++;
				if(j==i) {
					  if(itemElimina > 0) {
						  if(ss.getCodman().trim().equals("COTIZACION")){
							  valorReq = ss.getPres03();
							  ss.setCanman(0);
						  } else
							  valorReq = (ss.getCanman() * ss.getValuni());  
					    	totalTotal -= valorReq;
					    	total -= valorReq;
					    	ss.setIndmod("D");
					  } else valorReq = 0;
					 canman = Util.validaParametro(req.getParameter("canman"),0);
					 pres03 = Util.validaParametro(req.getParameter("pres03"), 0);
					 pres01 = Util.validaParametro(req.getParameter("pres01"), 0);
					 fecha = Util.validaParametro(req.getParameter("fechaInicio"),"");
					 if(!fecha.trim().equals(""))
						 pres02 = Integer.parseInt(fecha.substring(6)+ fecha.substring(3,5)+fecha.substring(0,2));

					 if(pres03 > 0){
				     valorReq = pres03;
				    // canman = 1;
				    	 } else {
						     valorReq = (canman * ss.getValuni());
				     }
					 total += valorReq;				  
				     totalTotal = totalTotal + valorReq;
				
				   
				} else
					canman = ss.getCanman();
		//	if(canman > 0) {	
			ss.setTippro("RQP");
			ss.setArea1(area1);
			ss.setArea2(area2);
			ss.setComen1(comen1);
			ss.setComen2(comen2);
			ss.setComen3(comen3);
			ss.setComen4(comen4);
			ss.setComen5(comen5);
			ss.setComen6(comen6);
			ss.setMotiv1(motiv1);
			ss.setMotiv2(motiv2);
			ss.setMotiv3(motiv3);
			ss.setMotiv4(motiv4);
			ss.setMotiv5(motiv5);
			ss.setMotiv6(motiv6);
			ss.setCanman(canman);
			ss.setPres01(pres01);
			ss.setPres02(pres02);
			ss.setPres03(pres03);
			listaRQP.add((Presw25DTO)ss);
		    valpr = ss.getValpr1();
		    desite = ss.getDesite();
		    desuni = ss.getDesuni();
		//	}
		 
		    
			} else listaRQP.add(ss);
	      
		}

		}
	    /*agrega en MAN*/
		Presw25DTO presw25 = new Presw25DTO();
		presw25.setTippro("MAN");
		presw25.setCoduni(unidad);
		presw25.setItedoc(item);
		presw25.setAnopre(anno);
		presw25.setMespre(mes);
		presw25.setNumreq(numreq);
		presw25.setValpr1(valpr);
		presw25.setDesite(desite);
		presw25.setComen1(area1);
		presw25.setComen2(area2);
		presw25.setDesuni(desuni);
		presw25.setTotpre(total);
		if(total == 0)
			presw25.setIndmod("D");
        if(presupItemreq != null && presupItemreq.size() > 0){
        	i = -1;
        	boolean agrega = false;
        	for (Presw25DTO ss : presupItemreq){
        		i++;
        		if(ss.getCoduni() == unidad && ss.getItedoc() == item && ss.getMespre() == mes && ss.getNumreq() == numreq){
        			presupItemreq.set(i, presw25);
        			agrega = true;
        		}        		      			
        	}
    	    if(!agrega)
    	    	presupItemreq.add(presw25);
        } else presupItemreq.add(presw25);
		
     	Long totalItemReq = new Long(0);
     	Long ene = new Long(0);
     	Long feb = new Long(0);
    	Long mar = new Long(0);
     	Long abr = new Long(0);
     	Long may = new Long(0);
     	Long jun = new Long(0);
     	Long jul = new Long(0);
     	Long ago = new Long(0);
     	Long sep = new Long(0);
     	Long oct = new Long(0);
     	Long nov = new Long(0);
     	Long dic = new Long(0);
     	
		if(listarResultadoPresup.size() > 0){
			for (Presw25DTO ss : presupItemreq){ // agregar todo lo que est� en mantencion al presup
				if(ss.getItedoc() == item && ss.getCoduni() == unidad ) {
					switch (ss.getMespre()){
					case 1: ene = ene + ss.getTotpre();
					break;
					case 2: feb = feb + ss.getTotpre();
					break;
					case 3: mar = mar + ss.getTotpre();
					break;
					case 4: abr = abr + ss.getTotpre();
					break;
					case 5: may = may + ss.getTotpre();
					break;
					case 6: jun = jun + ss.getTotpre();
					break;
					case 7: jul = jul + ss.getTotpre();
					break;
					case 8: ago = ago + ss.getTotpre();
					break;
					case 9: sep = sep + ss.getTotpre();
					break;
					case 10: oct = oct + ss.getTotpre();
					break;
					case 11: nov = nov + ss.getTotpre();
					break;
					case 12: dic = dic + ss.getTotpre();
					break;
					}
					totalItemReq = totalItemReq + ss.getTotpre();
				}
			}
		        
			i =-1;
			for (Presw25DTO ss : listarResultadoPresup){
				i++;
				if(ss.getItedoc() == item && ss.getCoduni() == unidad){
					ss.setItedoc(item);
					ss.setPres01(ene);
					ss.setPres02(feb);
					ss.setPres03(mar);
					ss.setPres04(abr);
					ss.setPres05(may);
					ss.setPres06(jun);
					ss.setPres07(jul);
					ss.setPres08(ago);
					ss.setPres09(sep);
					ss.setPres10(oct);
					ss.setPres11(nov);
					ss.setPres12(dic);
					ss.setTotpre(Long.parseLong(String.valueOf(ss.getPres01() + ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() + ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() + ss.getPres10() + ss.getPres11() + ss.getPres12())));
				}
				listaResultado.add(ss);
				//System.out.println("result: " + ss.getTotpre());
			}
			
		}
	/*	System.out.println("tama�o presupItemreq: "+presupItemreq.size());
		for (Presw25DTO ss : presupItemreq){
			
			System.out.println("ss "+ss.getMespre()+"cant "  + ss.getPres01()+"req: "+ ss.getNumreq());
			System.out.println("ss "+ss.getMespre()+"cant " + ss.getPres02()+"req: "+ ss.getNumreq());
			System.out.println("ss "+ss.getMespre()+"cant " + ss.getPres03()+"req: "+ ss.getNumreq());
			
		}
		System.out.println("presupItemreq desp **: "+presupItemreq.size());*/
		if(presupItemreq != null )
		   sesion.setAttribute("presupItemreq",presupItemreq);
		sesion.setAttribute("resultadoDetalleItemReq",listaRQP);

		sesion.setAttribute("resultadoPresup", listaResultado);	
		//req.setAttribute("totalMes", totalTotal);
		
}
public void actualizaItemReqDet(HttpServletRequest req, List<Presw25DTO> lista, List<Presw25DTO> presupItemreq){
		        //            resultadoDetalleItemReq, presupItemreq, listarResultadoPresup
		HttpSession sesion = req.getSession();
		List<Presw25DTO> listaRQP = new ArrayList();
		List<Presw25DTO> listaResultado = new ArrayList();
		
		String area1 = Util.validaParametro(req.getParameter("area1"),"");
		String area2 = Util.validaParametro(req.getParameter("area2"),"");
		String comen1 = Util.validaParametro(req.getParameter("comen1"),"");
		String comen2 = Util.validaParametro(req.getParameter("comen2"),"");
		String comen3 = Util.validaParametro(req.getParameter("comen3"),"");
		String comen4 = Util.validaParametro(req.getParameter("comen4"),"");
		String comen5 = Util.validaParametro(req.getParameter("comen5"),"");
		String comen6 = Util.validaParametro(req.getParameter("comen6"),"");
		String motiv1 = Util.validaParametro(req.getParameter("motiv1"),"");
		String motiv2 = Util.validaParametro(req.getParameter("motiv2"),"");
		String motiv3 = Util.validaParametro(req.getParameter("motiv3"),"");
		String motiv4 = Util.validaParametro(req.getParameter("motiv4"),"");
		String motiv5 = Util.validaParametro(req.getParameter("motiv5"),"");
		String motiv6 = Util.validaParametro(req.getParameter("motiv6"),"");
		int mes = Util.validaParametro(req.getParameter("mes"),0);
		int numreq = Util.validaParametro(req.getParameter("sucur"), 0);
		int item = Util.validaParametro(req.getParameter("itedoc"), 0);
		int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		int anno = Util.validaParametro(req.getParameter("anno"), 0);


		if(lista.size() > 0){
		for (Presw25DTO ss : lista){
	
		if(ss.getNumreq() == numreq && ss.getMespre() == mes  ){
		//System.out.println(ss.getItedoc());
			
		
		ss.setTippro("RQP");
		ss.setArea1(area1);
		ss.setArea2(area2);
		ss.setComen1(comen1);
		ss.setComen2(comen2);
		ss.setComen3(comen3);
		ss.setComen4(comen4);
		ss.setComen5(comen5);
		ss.setComen6(comen6);
		ss.setMotiv1(motiv1);
		ss.setMotiv2(motiv2);
		ss.setMotiv3(motiv3);
		ss.setMotiv4(motiv4);
		ss.setMotiv5(motiv5);
		ss.setMotiv6(motiv6);
		ss.setCanman(ss.getCanman());
		ss.setPres01(ss.getPres01());
		ss.setPres02(ss.getPres02());
		ss.setPres03(ss.getPres03());
		listaRQP.add((Presw25DTO)ss);				
		
		} else listaRQP.add(ss);
		
		}
		
		}
		/*agrega en MAN*/
	
		
		for (Presw25DTO ss : presupItemreq){
		
		if(ss.getCoduni() == unidad && ss.getItedoc() == item && ss.getMespre() == mes && ss.getNumreq() == numreq){
		    ss.setTippro("MAN");
		    ss.setComen1(area1);
			ss.setComen2(area2);
			listaResultado.add((Presw25DTO)ss);
		}   else listaResultado.add(ss);     		      			
		}


		if(presupItemreq != null )
		sesion.setAttribute("presupItemreq",listaResultado);
		sesion.setAttribute("resultadoDetalleItemReq",listaRQP);
		
		
		
		}
	public void agregaResultadoPresupOtrosIng(HttpServletRequest req, List<Presw25DTO> lista){
		HttpSession sesion = req.getSession();
		List<Presw25DTO> listaINP = new ArrayList();
		String indexi = Util.validaParametro(req.getParameter("indexi"), "");
		
		if(lista.size() > 0){
			
		int i=0;
		/*para las observaciones se registra del comen2 al comen6*/
		for (Presw25DTO ss : lista){
			i++;
			String valor = Util.validaParametro(req.getParameter("valuni"+i), "0");
			valor = valor.replace(".", "");
			
			Long valuni = Long.parseLong(valor);
			String comen2 = Util.validaParametro(req.getParameter("comen1"+i), "");
			String comen3 = Util.validaParametro(req.getParameter("comen2"+i), "");
			String comen4 = Util.validaParametro(req.getParameter("comen3"+i), "");
			String comen5 = Util.validaParametro(req.getParameter("comen4"+i), "");
			String comen6 = Util.validaParametro(req.getParameter("comen5"+i), "");
			/*se reciben del comen1 al comen5 para no cambiar la funcion que distribuye, al grabar lo hace bien*/
			
			Presw25DTO presw25 = ss;
			
			presw25.setTippro("INP");
			presw25.setTotpre(valuni);
			presw25.setComen2(comen2);
			presw25.setComen3(comen3);
			presw25.setComen4(comen4);
			presw25.setComen5(comen5);
			presw25.setComen6(comen6);
			presw25.setIndexi(indexi);
			listaINP.add((Presw25DTO) presw25);
		}
		}
		sesion.setAttribute("resultadoPresupIngresos",listaINP);		
}
/* M.A. se agrego control de tope para las cuentas opercionales RESPUE: C */
	public long verificaTopeUnidad(HttpServletRequest req, List<Presw25DTO> lista){
		HttpSession sesion = req.getSession();
		String ene = Util.validaParametro(req.getParameter("mes1"),"0");
		String feb = Util.validaParametro(req.getParameter("mes2"),"0");
		String mar = Util.validaParametro(req.getParameter("mes3"),"0");
		String abr = Util.validaParametro(req.getParameter("mes4"),"0");
		String may = Util.validaParametro(req.getParameter("mes5"),"0");
		String jun = Util.validaParametro(req.getParameter("mes6"),"0");
		String jul = Util.validaParametro(req.getParameter("mes7"),"0");
		String ago = Util.validaParametro(req.getParameter("mes8"),"0");
		String sep = Util.validaParametro(req.getParameter("mes9"),"0");
		String oct = Util.validaParametro(req.getParameter("mes10"),"0");
		String nov = Util.validaParametro(req.getParameter("mes11"),"0");
		String dic = Util.validaParametro(req.getParameter("mes12"),"0");
		ene = ene.replace(".", "");
		feb = feb.replace(".", "");
		mar = mar.replace(".", "");
		abr = abr.replace(".", "");
		may = may.replace(".", "");
		jun = jun.replace(".", "");
		jul = jul.replace(".", "");
		ago = ago.replace(".", "");
		sep = sep.replace(".", "");
		oct = oct.replace(".", "");
		nov = nov.replace(".", "");
		dic = dic.replace(".", "");

		Long mes1 = Long.parseLong(ene);
		Long mes2 = Long.parseLong(feb);
		Long mes3 = Long.parseLong(mar);
		Long mes4 = Long.parseLong(abr);
		Long mes5 = Long.parseLong(may);
		Long mes6 = Long.parseLong(jun);
		Long mes7 = Long.parseLong(jul);
		Long mes8 = Long.parseLong(ago);
		Long mes9 = Long.parseLong(sep);
		Long mes10 = Long.parseLong(oct);
		Long mes11 = Long.parseLong(nov);
		Long mes12 = Long.parseLong(dic);
		Long totpre = new Long(0);
		totpre = mes1 + mes2 + mes3 + mes4 + mes5 + mes6 + mes7 + mes8 + mes9 + mes10 + mes11 + mes12;
			
		//int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		/*MB 24/10/2012 debido a que suma 2 veces el mismo item es que lo excluye en la suma, valiendo lo actual*/
		//int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		
		/*05/10/2016 MAA - Se cambian nombres de variables 'unidad(es)' por 'organizacion(es)',cuenta(s) en itedoc */
		String organizacion = Util.validaParametro(req.getParameter("organizacion"),"");
		/*MB 24/10/2012 debido a que suma 2 veces el mismo item es que lo excluye en la suma, valiendo lo actual*/
		String itedoc = Util.validaParametro(req.getParameter("itedoc"),"");


		/* M.A. se agrego control de tope para las cuentas opercionales RESPUE: C */
		long topeUnidad = 0;
		String codRespue = "";
		List<Presw25DTO> resultadoCPR = (List<Presw25DTO>) req.getSession().getAttribute("resultadoCPR");
		if(resultadoCPR != null && resultadoCPR.size() > 0){
			for (Presw25DTO ss : resultadoCPR){
				// if(ss.getCoduni() == unidad) {
				if(ss.getComen4().trim().equals(organizacion.trim())) {
					topeUnidad = ss.getTotpre();
					codRespue  = ss.getRespue();
					break;
				}	
			   
			}
		}
		
		Long sumaUnidad = new Long(0);
		List<Presw25DTO> resultadoPresup  = (List<Presw25DTO>) req.getSession().getAttribute("resultadoPresup");
		if(resultadoPresup != null && resultadoPresup.size() > 0){
			for (Presw25DTO ss : resultadoPresup){
				// if((ss.getCoduni() == unidad) && (itedoc != ss.getItedoc())) {
				if((ss.getMotiv1().trim().equals(organizacion.trim())) && (!itedoc.trim().equals(ss.getMotiv2().trim()))) {
					sumaUnidad = sumaUnidad + ss.getTotpre();
				}	
			   
			}
			sumaUnidad = sumaUnidad + totpre;
		}
		
		if (/*codRespue.trim().equals("C") && esto se comentare� pues es en todos los casos*/ sumaUnidad > topeUnidad) {
			return topeUnidad;
		}
		else return 0;
			
		
	}
	public void agregaItemActualiza(HttpServletRequest req, List<Presw25DTO> lista){
		/* MAA itedoc por motiv2*/
		HttpSession sesion = req.getSession();
		String ene = Util.validaParametro(req.getParameter("mes1"),"0");
		String feb = Util.validaParametro(req.getParameter("mes2"),"0");
		String mar = Util.validaParametro(req.getParameter("mes3"),"0");
		String abr = Util.validaParametro(req.getParameter("mes4"),"0");
		String may = Util.validaParametro(req.getParameter("mes5"),"0");
		String jun = Util.validaParametro(req.getParameter("mes6"),"0");
		String jul = Util.validaParametro(req.getParameter("mes7"),"0");
		String ago = Util.validaParametro(req.getParameter("mes8"),"0");
		String sep = Util.validaParametro(req.getParameter("mes9"),"0");
		String oct = Util.validaParametro(req.getParameter("mes10"),"0");
		String nov = Util.validaParametro(req.getParameter("mes11"),"0");
		String dic = Util.validaParametro(req.getParameter("mes12"),"0");
		ene = ene.replace(".", "");
		feb = feb.replace(".", "");
		mar = mar.replace(".", "");
		abr = abr.replace(".", "");
		may = may.replace(".", "");
		jun = jun.replace(".", "");
		jul = jul.replace(".", "");
		ago = ago.replace(".", "");
		sep = sep.replace(".", "");
		oct = oct.replace(".", "");
		nov = nov.replace(".", "");
		dic = dic.replace(".", "");

		Long mes1 = Long.parseLong(ene);
		Long mes2 = Long.parseLong(feb);
		Long mes3 = Long.parseLong(mar);
		Long mes4 = Long.parseLong(abr);
		Long mes5 = Long.parseLong(may);
		Long mes6 = Long.parseLong(jun);
		Long mes7 = Long.parseLong(jul);
		Long mes8 = Long.parseLong(ago);
		Long mes9 = Long.parseLong(sep);
		Long mes10 = Long.parseLong(oct);
		Long mes11 = Long.parseLong(nov);
		Long mes12 = Long.parseLong(dic);
		Long totpre = new Long(0);
		totpre = mes1 + mes2 + mes3 + mes4 + mes5 + mes6 + mes7 + mes8 + mes9 + mes10 + mes11 + mes12;

		// 05/10/2016 MAA - Se cambian nombres de variables 'unidad(es)' por 'organizacion(es)',cuenta(s) itedoc por string 
		//int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		//int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		String organizacion = Util.validaParametro(req.getParameter("organizacion"),"");
		String itedoc = Util.validaParametro(req.getParameter("itedoc"),"");
		
		List<Presw25DTO> listarResultado = new ArrayList<Presw25DTO>();
		String comen1 = Util.validaParametro(req.getParameter("comen1"), "");
		String comen2 = Util.validaParametro(req.getParameter("comen2"),"");
		String comen3 = Util.validaParametro(req.getParameter("comen3"),"");
		String comen4 = Util.validaParametro(req.getParameter("comen4"),"");
		String comen5 = Util.validaParametro(req.getParameter("comen5"),"");
		String comen6 = Util.validaParametro(req.getParameter("comen6"),"");
		for (Presw25DTO ss : lista){
			//System.out.println(ss.getItedoc());
		 // if(ss.getCoduni() == unidad && ss.getItedoc() == itedoc){
			if((ss.getMotiv1().trim().equals(organizacion.trim())) && (itedoc.trim().equals(ss.getMotiv2().trim()))) {
			//  System.out.println(ss.getItedoc());
			  ss.setPres01(mes1);
			  ss.setPres02(mes2);
			  ss.setPres03(mes3);
			  ss.setPres04(mes4);
			  ss.setPres05(mes5);
			  ss.setPres06(mes6);
			  ss.setPres07(mes7);
			  ss.setPres08(mes8);
			  ss.setPres09(mes9);
			  ss.setPres10(mes10);
			  ss.setPres11(mes11);
			  ss.setPres12(mes12);
			  ss.setTotpre(totpre);
			  ss.setComen1(comen1);
			  ss.setComen2(comen2);
			  ss.setComen3(comen3);
			  ss.setComen4(comen4);
			  ss.setComen5(comen5);
			  ss.setComen6(comen6);
			  // MAA
			  ss.setMotiv2(itedoc.trim());
			  ss.setMotiv1(organizacion.trim());
		
		  }
		  listarResultado.add(ss);		  
		}
		sesion.setAttribute("resultadoPresup", listarResultado);
	}
	
	public void eliminaItem(HttpServletRequest req, List<Presw25DTO> lista){
		/* MAA itedoc por motiv2*/
		HttpSession sesion = req.getSession();
		//int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		//int itedoc = Util.validaParametro(req.getParameter("item"), 0);
		String organizacion = Util.validaParametro(req.getParameter("organizacion"),"");
		String itedoc = Util.validaParametro(req.getParameter("item"),"");
		List<Presw25DTO> listarResultado = new ArrayList<Presw25DTO>();
		
		for (Presw25DTO ss : lista){
		//  if(ss.getCoduni() == unidad && ss.getItedoc() == itedoc){
			if(ss.getMotiv1().trim().equals(organizacion.trim()) && ss.getMotiv2().trim().equals(itedoc)){
			  ss.setPres01(0);
			  ss.setPres02(0);
			  ss.setPres03(0);
			  ss.setPres04(0);
			  ss.setPres05(0);
			  ss.setPres06(0);
			  ss.setPres07(0);
			  ss.setPres08(0);
			  ss.setPres09(0);
			  ss.setPres10(0);
			  ss.setPres11(0);
			  ss.setPres12(0);
			  ss.setTotpre(0);
			  ss.setComen1("");
			  ss.setTotpre(0);
			  // MAA 
			  ss.setMotiv2(itedoc);
			  ss.setMotiv1(organizacion);
		
		  }
		  listarResultado.add(ss);		  
		}

		sesion.setAttribute("resultadoPresup", listarResultado);
	}
	public void eliminaReq(HttpServletRequest req, List<Presw25DTO> lista, List<Presw25DTO> listaDetalle){
		HttpSession sesion = req.getSession();
		int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		int mes = Util.validaParametro(req.getParameter("mes"), 0);
		int sucur = Util.validaParametro(req.getParameter("sucur"), 0);
		List<Presw25DTO> listarResultado = new ArrayList<Presw25DTO>();
		List<Presw25DTO> listarResultadoDet = new ArrayList<Presw25DTO>();
		for (Presw25DTO ss : lista){
		  if(ss.getCoduni() == unidad && ss.getItedoc() == itedoc && ss.getMespre() == mes && ss.getNumreq() == sucur){
			  
			  ss.setCanman(0);
			  ss.setIndmod("D");
			  ss.setTotpre(0);
		
		  }
		  listarResultado.add(ss);		  
		}
		if(listaDetalle != null && listaDetalle.size() > 0){
		for (Presw25DTO ss : listaDetalle){
			  if(ss.getCoduni() == unidad && ss.getItedoc() == itedoc && ss.getMespre() == mes && ss.getNumreq() == sucur){
				  ss.setTippro("RQP");
				  ss.setIndmod("D");
				  ss.setCanman(0);
				  ss.setTotpre(0);
			
			  }
			  listarResultadoDet.add(ss);		  
			}
		}
		sesion.setAttribute("presupItemreq", listarResultado);
		sesion.setAttribute("resultadoDetalleItemReq", listarResultadoDet);
	}
	/*public boolean saveMatriz(List<Presw25DTO> lista, int unidad, int a�o, int rutUsuario){
		PreswBean preswbean = new PreswBean("PRP", unidad,0,a�o,0, rutUsuario);
		
		return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	}*/
	// MAA se cambia a organizacion
	
	public boolean saveMatriz(List<Presw25DTO> lista, String organizacion, int a�o, int rutUsuario){
		PreswBean preswbean = new PreswBean("PRP",0,0,a�o,0, rutUsuario);
		return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	}

	// public boolean saveREQ(List<Presw25DTO> lista, int unidad, int a�o, int rutUsuario, int mes){
	public boolean saveREQ(List<Presw25DTO> lista, String organizacion, int a�o, int rutUsuario, int mes){
		 boolean retorno = false;
		 List<Presw25DTO> listaRQP = new ArrayList();
		// int item = 0;
		 String item = "0";
		 int numreq = 0;
		 int mesAnt = 1;
		 if(lista != null && lista.size() > 0){
		for (Presw25DTO ss : lista){
			if(numreq != ss.getNumreq() /*&& mesAnt == ss.getMespre()*/){
				ss.setIndexi("I");
			/*	if(ss.getCanman() == 0)
					ss.setIndmod("D");*/
			}
			numreq = ss.getNumreq();
		//	item = ss.getItedoc();
			item = ss.getComen4();
			mesAnt = ss.getMespre();
				if(ss.getValpr1() == 1){
					
					ss.setNumreq(0);
					
				}				
				listaRQP.add(ss);
		//	System.out.println(ss.getIndexi()+ "  ind : "+ ss.getIndmod()+"  ss.getMespre(): " + ss.getMespre()+"   ss.getCanman(): "+ss.getCanman()+"  numreq: "+numreq+ "    ss.getNumreq(): "+ss.getNumreq());
			}
		 }
		//PreswBean preswbean = new PreswBean("RQP", unidad,item,a�o,mes, rutUsuario);
		PreswBean preswbean = new PreswBean("RQP",0,0,a�o,mes, rutUsuario);
		retorno = preswbean.ingreso_presw25((List<Presw25DTO> )listaRQP);
	
		return retorno;
	}

	public boolean saveDetalleREQ(List<Presw25DTO> lista, int rutUsuario){
	    boolean retorno = false;	
	
		for (Presw25DTO ss : lista){
			PreswBean preswbean = new PreswBean("REQ",ss.getCoduni(), ss.getItedoc(), ss.getAnopre(),ss.getMespre(), rutUsuario);
			 preswbean.setSucur(ss.getNumreq());
			 retorno = preswbean.ingreso_presw25((List<Presw25DTO> )lista);
			 if(!retorno) break;
		}
		return retorno;
	}
	/*se modifica con banner
	 * public boolean savePresupNomina(List<Presw25DTO> lista, int unidad, int a�o, int rutUsuario){
		PreswBean preswbean = new PreswBean("COP", unidad,0,a�o,0, rutUsuario);
		
		return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	}*/
	public boolean savePresupNomina(List<Presw25DTO> lista, String organizacion, int a�o, int rutUsuario){
		PreswBean preswbean = new PreswBean("COP", 0,0,a�o,0, rutUsuario);
		
		return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	}
	
/*	ahora se elimin� ingreso a las vacantes 24/09/2010
 * public boolean savePresupVacante(List<Presw25DTO> lista, int unidad, int a�o, int rutUsuario){
		PreswBean preswbean = new PreswBean("GNV", unidad,0,a�o,0, rutUsuario);
		
		return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	}*/
/*	public boolean savePresupRemuneracion(List<Presw25DTO> lista, int unidad, int a�o, int rutUsuario){
		PreswBean preswbean = new PreswBean("ASP", unidad,0,a�o,0, rutUsuario);
		
		return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	}*/
	public boolean savePresupRemuneracion(List<Presw25DTO> lista, String organizacion, int a�o, int rutUsuario){
		PreswBean preswbean = new PreswBean("ASP", 0,0,a�o,0, rutUsuario);
		return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	}
	public boolean savePresupOtros(List<Presw25DTO> lista, int a�o, int rutUsuario){
		PreswBean preswbean = new PreswBean("INP", 0, 0, a�o, 0, rutUsuario);
		
		return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	}
	
	public boolean saveIndicadores(String tipo, List<Presw25DTO> lista, String dv , int rutUsuario){
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(dv);
	//	System.out.println("saveIndicadores   nomTipo: "+tipo);
		return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	}
	public boolean saveNotificacion(String tipo, List<Presw25DTO> lista, String dv , int rutUsuario, String tipoNotificacion, int coduni){
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setTipcue(tipoNotificacion);
		preswbean.setCoduni(coduni);
		preswbean.setDigide(dv);
		
		return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	}
	public boolean saveValorIndicadores(String tipo, List<Presw25DTO> lista, String dv , int rutUsuario){
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(dv);
		
		return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	}
	
	public Vector getlistaReqResumenExcel(HttpServletRequest req){
		// MAA se cambia unidad por organizacion y itedoc a String
		//int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		String itedoc = Util.validaParametro(req.getParameter("itedoc"),"");
		int a�o = Util.validaParametro(req.getParameter("anno"), 0);
		// int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		String organizacion = Util.validaParametro(req.getParameter("organizacion"),"");
		int rutUsuario = Integer.parseInt(req.getSession().getAttribute("rutUsuario")+ "");

		Vector vec_datos = new Vector();
		Vector vec_d = new Vector();
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		//String nomUnidad = "";
		String nomOrganizacion = "";
		long total = 0;
		long totalTotal = 0;
		long enero = 0;
		long febrero = 0;
		long marzo = 0;
		long abril = 0;
		long mayo = 0;
		long junio = 0;
		long julio = 0;
		long agosto = 0;
		long septiembre = 0;
		long octubre = 0;
		long noviembre = 0;
		long diciembre = 0;
		// PreswBean preswbean = new PreswBean("PRE", unidad,0,a�o,0, rutUsuario);
		PreswBean preswbean = new PreswBean("PRE", 0,0,a�o,0, rutUsuario);
		preswbean.setCajero(organizacion);
		NumberFormat nf2 = 	NumberFormat.getInstance(Locale.GERMAN); 

		if(preswbean != null) {				
			listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
		}
		
		long totalAgrup = 0;
		Vector agrup = null;
		Vector agrup1 = new Vector();
		Vector agrup2 = new Vector();
		Vector agrup3 = new Vector();
		Vector agrup4 = new Vector();
		long total_ENE = 0;	
		long total_FEB = 0;	
		long total_MAR = 0;
		long total_ABR = 0;	
		long total_MAY = 0;	
		long total_JUN = 0;	
		long total_JUL = 0;	
		long total_AGO = 0;	
		long total_SEP = 0;	
		long total_OCT = 0;	
		long total_NOV = 0;	
		long total_DIC = 0;	
		long agrup_ENE = 0;	
		long agrup_FEB = 0;
		long agrup_MAR = 0;	
		long agrup_ABR = 0;	
		long agrup_MAY = 0;	
		long agrup_JUN = 0;	
		long agrup_JUL = 0;	
		long agrup_AGO = 0;	
		long agrup_SEP = 0;	
		long agrup_OCT = 0;	
		long agrup_NOV = 0;	
		long agrup_DIC = 0;
		for (Presw25DTO ss : listaPresw25){
			total = ss.getPres01() + ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() + ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() + ss.getPres10() + ss.getPres11() + ss.getPres12();
			total_ENE = ss.getPres01();
			total_FEB = ss.getPres02();
			total_MAR = ss.getPres03();
			total_ABR = ss.getPres04();
			total_MAY = ss.getPres05();
			total_JUN = ss.getPres06();
			total_JUL = ss.getPres07();
			total_AGO = ss.getPres08();
			total_SEP = ss.getPres09();
			total_OCT = ss.getPres10();
			total_NOV = ss.getPres11();
			total_DIC = ss.getPres12();
	        if(!ss.getMotiv2().trim().equals(ss.getMotiv3().trim())){
		        totalAgrup = totalAgrup + total;
		        agrup_ENE = agrup_ENE + total_ENE;
			    agrup_FEB = agrup_FEB + total_FEB;
				agrup_MAR = agrup_MAR + total_MAR;
				agrup_ABR = agrup_ABR + total_ABR;
				agrup_MAY = agrup_MAY + total_MAY;
				agrup_JUN = agrup_JUN + total_JUN;
				agrup_JUL = agrup_JUL + total_JUL;
				agrup_AGO = agrup_AGO + total_AGO;
				agrup_SEP = agrup_SEP + total_SEP;
				agrup_OCT = agrup_OCT + total_OCT;
				agrup_NOV = agrup_NOV + total_NOV;
				agrup_DIC = agrup_DIC + total_DIC;
	        }  
		    else {
		    	 agrup = new Vector();	 
				 agrup.addElement(totalAgrup);
				 agrup.addElement(agrup_ENE);
				 agrup.addElement(agrup_FEB);
				 agrup.addElement(agrup_MAR);
				 agrup.addElement(agrup_ABR);
				 agrup.addElement(agrup_MAY);
				 agrup.addElement(agrup_JUN);
				 agrup.addElement(agrup_JUL);
				 agrup.addElement(agrup_AGO);
				 agrup.addElement(agrup_SEP);
				 agrup.addElement(agrup_OCT);
				 agrup.addElement(agrup_NOV);
				 agrup.addElement(agrup_DIC);
		    	 if(ss.getMotiv2().trim().equals("2")) agrup1 = agrup;
		    	 else if(ss.getMotiv2().trim().equals("3")) agrup2 = agrup;
				 else if(ss.getMotiv2().trim().equals("4")) agrup3 = agrup;
		    	 
		    	 totalAgrup = 0;  
				 agrup_ENE = 0;	
				 agrup_FEB = 0;	
				 agrup_MAR = 0;	
				 agrup_ABR = 0;	
				 agrup_MAY = 0;	
				 agrup_JUN = 0;	
				 agrup_JUL = 0;	
				 agrup_AGO = 0;	
				 agrup_SEP = 0;	
				 agrup_OCT = 0;	
				 agrup_NOV = 0;	
				 agrup_DIC = 0;
			 
		      }
		  }  
		 agrup = new Vector();	 
		 agrup.addElement(totalAgrup);
		 agrup.addElement(agrup_ENE);
		 agrup.addElement(agrup_FEB);
		 agrup.addElement(agrup_MAR);
		 agrup.addElement(agrup_ABR);
		 agrup.addElement(agrup_MAY);
		 agrup.addElement(agrup_JUN);
		 agrup.addElement(agrup_JUL);
		 agrup.addElement(agrup_AGO);
		 agrup.addElement(agrup_SEP);
		 agrup.addElement(agrup_OCT);
		 agrup.addElement(agrup_NOV);
		 agrup.addElement(agrup_DIC);
		 agrup4 = agrup;
		
		//System.out.println("vecAgrup "+agrup1);
		for (Presw25DTO ss : listaPresw25){	
 
 		total = ss.getPres01() + ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() + ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() + ss.getPres10() + ss.getPres11() + ss.getPres12();
			//if(total > 0){
		  if(total > 0 || ss.getMotiv2().trim().equals(ss.getMotiv3().trim())){
			if(!ss.getMotiv2().trim().equals(ss.getMotiv3().trim())) 
			//vec_datos.add(String.valueOf(ss.getItedoc()));
			      vec_datos.add(ss.getMotiv2());
			else vec_datos.add("");
			vec_datos.add(ss.getDesite());
			
			agrup = new Vector();
		    if(ss.getMotiv2().trim().equals(ss.getMotiv3().trim())){
			   if(ss.getMotiv2().trim().equals("1")) agrup = agrup1;
			   else if(ss.getMotiv2().trim().equals("2")) agrup = agrup2;
               else if(ss.getMotiv2().trim().equals("3")) agrup = agrup3;
                else if(ss.getMotiv2().trim().equals("4")) agrup = agrup4;
			   
			    vec_datos.add(nf2.format(agrup.get(0)));
				vec_datos.add(nf2.format(agrup.get(1)));
				vec_datos.add(nf2.format(agrup.get(2)));
				vec_datos.add(nf2.format(agrup.get(3)));
				vec_datos.add(nf2.format(agrup.get(4)));
				vec_datos.add(nf2.format(agrup.get(5)));
				vec_datos.add(nf2.format(agrup.get(6)));
				vec_datos.add(nf2.format(agrup.get(7)));
				vec_datos.add(nf2.format(agrup.get(8)));
				vec_datos.add(nf2.format(agrup.get(9)));
				vec_datos.add(nf2.format(agrup.get(10)));
				vec_datos.add(nf2.format(agrup.get(11)));
				vec_datos.add(nf2.format(agrup.get(12)));
				vec_datos.add("");
		    }    
          	else {
	          	vec_datos.add(nf2.format(total));
				vec_datos.add(nf2.format(ss.getPres01()));
				vec_datos.add(nf2.format(ss.getPres02()));
				vec_datos.add(nf2.format(ss.getPres03()));
				vec_datos.add(nf2.format(ss.getPres04()));
				vec_datos.add(nf2.format(ss.getPres05()));
				vec_datos.add(nf2.format(ss.getPres06()));
				vec_datos.add(nf2.format(ss.getPres07()));
				vec_datos.add(nf2.format(ss.getPres08()));
				vec_datos.add(nf2.format(ss.getPres09()));
				vec_datos.add(nf2.format(ss.getPres10()));
				vec_datos.add(nf2.format(ss.getPres11()));
				vec_datos.add(nf2.format(ss.getPres12()));
				vec_datos.add(ss.getComen1().toString().trim()+ss.getComen2().toString().trim()+ss.getComen3().toString().trim()+ss.getComen4().toString().trim()+ss.getComen5().toString().trim()+ss.getComen6().toString().trim());
	        }
			//nomUnidad = ss.getCoduni() + " - " + ss.getDesuni().trim(); 
			nomOrganizacion = ss.getMotiv1().trim() + " - " + ss.getDesuni().trim();
			enero += ss.getPres01();
			febrero += ss.getPres02();
			marzo += ss.getPres03();
			abril += ss.getPres04();
			mayo += ss.getPres05();
			junio += ss.getPres06();
			julio += ss.getPres07();
			agosto += ss.getPres08();
			septiembre += ss.getPres09();
			octubre += ss.getPres10();
			noviembre += ss.getPres11();
			diciembre += ss.getPres12();
			totalTotal += total;
			}
		}
		vec_datos.add("");
		vec_datos.add("Total $");
		vec_datos.add(nf2.format(totalTotal));
		vec_datos.add(nf2.format(enero));
		vec_datos.add(nf2.format(febrero));
		vec_datos.add(nf2.format(marzo));
		vec_datos.add(nf2.format(abril));
		vec_datos.add(nf2.format(mayo));
		vec_datos.add(nf2.format(junio));
		vec_datos.add(nf2.format(julio));
		vec_datos.add(nf2.format(agosto));
		vec_datos.add(nf2.format(septiembre));
		vec_datos.add(nf2.format(octubre));
		vec_datos.add(nf2.format(noviembre));
		vec_datos.add(nf2.format(diciembre));
		// vec_datos.add(nomUnidad);
		vec_datos.add(nomOrganizacion);
		return vec_datos;
	}
		public void getlistaPresupItemreq(HttpServletRequest req){
			int a�o = Util.validaParametro(req.getParameter("anno"), 0);
			int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
			int rutUsuario = Integer.parseInt(req.getSession().getAttribute("rutUsuario")+ "");
			HttpSession sesion = req.getSession();
			
	
			/*agrega requerimiento*/
		    String nomItem = "";
		    String nomMes = "";
		    String nomUnidad = "";
		    int itedoc = 41;
			Vector vecTotales = new Vector();
			Vector vec_d = new Vector();
			List<Presw25DTO> presupItemreq = new ArrayList<Presw25DTO>();
			List<Presw25DTO> presupItemreqDet = new ArrayList<Presw25DTO>();
			
			PreswBean preswbean = new PreswBean("MAN", unidad,itedoc,a�o,0, rutUsuario);
			if(preswbean != null) {
				    presupItemreq = (List<Presw25DTO>) preswbean.consulta_presw25();
			}
			
			Long en = new Long(0);
	    	Long fe = new Long(0);
	    	Long ma = new Long(0);
	    	Long ab = new Long(0);
	    	Long my = new Long(0);
	    	Long jn = new Long(0);
	    	Long jl = new Long(0);
	    	Long ag = new Long(0);
	    	Long se = new Long(0);
	    	Long oc = new Long(0);
	    	Long no = new Long(0);
	    	Long di = new Long(0);
	    	Vector listaReqResumen = new Vector();	
	    	Vector listaReqDetResumen = new Vector();
	         
	    	if(presupItemreq != null && presupItemreq.size() > 0){
	        Vector meses = new Vector();
	        int numMes = 1;
	    //    while (numMes < 13) {
	    	for (Presw25DTO ss : presupItemreq){
	    		Presw25DTO presw25 = ss;
	        	  
	    		if(presw25.getCoduni() == unidad && 
	    				presw25.getItedoc() == itedoc && 
	    				presw25.getAnopre() == a�o && 
	    				ss.getTotpre() > 0
	    				/*&& numMes == ss.getMespre()*/){
	    			  nomItem = ss.getItedoc() + " - " + ss.getDesite();
		        	  nomUnidad = ss.getCoduni() + " - " + ss.getDesuni();
		    	switch (ss.getMespre()){	  			
		    	case 1: 	{ en = en + presw25.getTotpre();
		    	              nomMes = "Enero"; 	    	
		    	              break;
		    	}
		    	case 2: 	{fe = fe + presw25.getTotpre();
		    				nomMes = "Febrero"; 
		    				break;
		    	}
		    	
		    	case 3:    { ma = ma + presw25.getTotpre();
		    				nomMes = "Marzo";
		    				break;
		    	}
		    	
		    	case 4:    {ab = ab + presw25.getTotpre();
		    				nomMes = "Abril"; 
		    				break;
		    	}
		    	
		    	case 5: 	{my = my + presw25.getTotpre();
		    				nomMes = "Mayo"; 
		    				break;
		    	}
		    	
		    	case 6: 	{jn = jn + presw25.getTotpre();
		    				nomMes = "Junio";
		    				break;
		    	}
		    	
		    	case 7: 	{jl = jl + presw25.getTotpre();
		    				nomMes = "Julio"; 
		    				break;
		    	}
		    	
		    	case 8: 	{ag = ag + presw25.getTotpre();
		    				nomMes = "Agosto"; 
		    				break;
		    	}
		    	
		    	case 9: 	{se = se + presw25.getTotpre();
		    				nomMes = "Septiembre"; 
		    				break;
		    	}
		    	
		    	case 10: 	{oc = oc + presw25.getTotpre();
		    				nomMes = "Octubre"; 
		    				break;
		    	}
		    	
		    	case 11: 	{no = no + presw25.getTotpre();
		    				nomMes = "Noviembre";
		    				break;
		    	}
		    	
		    	case 12: 	{di = di + presw25.getTotpre();
		    				nomMes = "Diciembre"; 
		    				break;
		    	}
		    	
		    	}
		    	
		    		
		    	    vec_d = new Vector();
		    	    vec_d.addElement(ss.getMespre());
		    	    vec_d.addElement(nomMes);
		    	    vec_d.addElement(ss.getComen1().trim());
		    	    vec_d.addElement(ss.getComen2().trim());
		    	    vec_d.addElement(ss.getTotpre());
		    	    switch (ss.getMespre()){
		    	    case 1: vec_d.addElement(en);
		    	    break;
		    	    case 2 : vec_d.addElement(fe);
		    	    break;
		    	    case 3 : vec_d.addElement(ma);
		    	    break;
		    	    case 4 : vec_d.addElement(ab);
		    	    break;
		    	    case 5 : vec_d.addElement(my);
		    	    break;
		    	    case 6 : vec_d.addElement(jn);
		    	    break;
		    	    case 7 : vec_d.addElement(jl);
		    	    break;
		    	    case 8 : vec_d.addElement(ag);
		    	    break;
		    	    case 9 : vec_d.addElement(se);
		    	    break;
		    	    case 10 : vec_d.addElement(oc);
		    	    break;
		    	    case 11 : vec_d.addElement(no);
		    	    break;
		    	    case 12 : vec_d.addElement(di);
		    	    break;
		    	    }
		    	    listaReqResumen.addElement(vec_d);
	    	
	    		preswbean = new PreswBean("REQ", unidad,itedoc,a�o,ss.getMespre(), rutUsuario);
				if(preswbean != null) {
					preswbean.setSucur(ss.getNumreq());	
					presupItemreqDet = new ArrayList<Presw25DTO>();
					presupItemreqDet = (List<Presw25DTO>) preswbean.consulta_presw25();
				}
	    //	}
				
	    //	}
	 
	    	//numMes++;
	    //    }
	    //	}
	    	nomMes = "";
	    	if(presupItemreqDet.size() > 0){
	    	for(Presw25DTO pi: presupItemreqDet){
	    		switch (pi.getMespre()){	  			
		    	case 1: 	nomMes = "Enero"; 	    	
		    	              break;
		    	case 2: 	nomMes = "Febrero"; 
		    				break;	    	
		    	case 3:   	nomMes = "Marzo";
		    				break;	    	
		    	case 4:     nomMes = "Abril"; 
		    				break;	    	
		    	case 5: 	nomMes = "Mayo"; 
		    				break;	    	
		    	case 6: 	nomMes = "Junio";
		    				break;	    	
		    	case 7: 	nomMes = "Julio"; 
		    				break;	    	
		    	case 8: 	nomMes = "Agosto"; 
		    				break;	    	
		    	case 9: 	nomMes = "Septiembre"; 
		    				break;	    	
		    	case 10: 	nomMes = "Octubre"; 
		    				break;	    	
		    	case 11: 	nomMes = "Noviembre";
		    				break;	    	
		    	case 12: 	nomMes = "Diciembre"; 
		    				break;	    	
		    	}
	    		 vec_d = new Vector();
	    		 vec_d.addElement(pi.getCoduni()+" - "+pi.getDesuni()); // 0 UNIDAD
	    		 vec_d.addElement(pi.getItedoc()+" - "+ pi.getDesite()); // 1 - ITEM
	    		 vec_d.addElement(nomMes.concat(" - N� " + pi.getNumreq()+""));// 3 - requerimiento    	    	
	     		 vec_d.addElement(pi.getArea1().trim() + pi.getArea2().trim());// 2 - AREA
	    		 vec_d.addElement(pi.getComen1().trim()+pi.getComen2().trim()+pi.getComen3().trim()+pi.getComen4().trim()+pi.getComen5().trim()+pi.getComen6().trim()); // 4 - DESCRIPCION
	    		 vec_d.addElement(pi.getMotiv1().trim()+pi.getMotiv2().trim()+pi.getMotiv3().trim()+pi.getMotiv4().trim()+pi.getMotiv5().trim()+pi.getMotiv6().trim()); // 5 - MOTIVO
				 vec_d.addElement(pi.getCodman()); // 6 - ITEM
				 vec_d.addElement(pi.getTipcon()); // 7 - DETALLE
	    		 if(pi.getCodman().trim().equals("COTIZACION") && pi.getPres03() > 0){
	    			 String fecha = pi.getPres02()+"";
	    			 vec_d.addElement(pi.getPres01()); // 9 - cotizacion
	        		 vec_d.addElement(pi.getPres03()); // 10 - monto
	        		 vec_d.addElement(fecha.substring(7)+"/"+fecha.substring(4,6)+"/"+fecha.substring(0,4)); // 10 - fecha
	    		 } else {
	    			 vec_d.addElement(pi.getCodigo()); // 8 - UNIDAD
	    	     	 vec_d.addElement(pi.getValuni()); // 9 - COSTO
	        		 vec_d.addElement(pi.getCanman()); // 10 - CANTIDAD       
	    		 }
	             vec_d.addElement(pi.getNumreq()); // 11 - NUMREQ
	    		 vec_d.addElement(pi.getMespre()); // 12 - MES
	    		 if(pi.getCanman() > 0 	|| pi.getPres03() > 0)
	    			 listaReqDetResumen.addElement(vec_d);
	    	}
	    	}
	    	}
	    	}
	       	}
	       	Long totalAnual = en + fe + ma + ab + my + jn + jl + ag + se + oc + no + di;
		    vec_d = new Vector();
		    vec_d.addElement(13);
		    vec_d.addElement("Total Anual");
		    vec_d.addElement("");
		    vec_d.addElement("");
		    vec_d.addElement("");
		    vec_d.addElement(totalAnual);
		    if(totalAnual > 0)
		 	   		    listaReqResumen.addElement(vec_d);
			/*fin requerimiento*/
		 //   System.out.println(listaReqDetResumen);
			sesion.setAttribute("listaReqResumen", listaReqResumen);
			sesion.setAttribute("listaReqDetResumen", listaReqDetResumen);
						
	}
		
	public void agregaCorrientePDF(HttpServletRequest req, Vector<String> lista, Vector<String> lista2, Vector<String> lista3){
			HttpSession sesion = req.getSession();
			sesion.setAttribute("listaCorrientePDF",lista);
			sesion.setAttribute("listaReqResumenPDF", lista2);
			sesion.setAttribute("listaReqDetResumenPDF", lista3);
	}

	public void agregaCorrienteExcel(HttpServletRequest req, Vector<String> lista, Vector<String> lista2, Vector<String> lista3){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listaCorrienteExcel",lista);
		sesion.setAttribute("listaReqResumenExcel", lista2);
		sesion.setAttribute("listaReqDetResumenExcel", lista3);
		
	}
/*	public void agregaAccionweb(HttpServletRequest req, String opcion, int valor){
		req.setAttribute(opcion, valor);
	}*/
	public void agregaResumenAnualExcel(HttpServletRequest req, Vector<String> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listaResumenAnualExcel",lista);
	}
	public void agregaDisponibilidadExcel(HttpServletRequest req, Vector<String> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listaDisponibilidadExcel",lista);
	}
	public void agregaInformeExcel(HttpServletRequest req, Vector<String> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("consultaDocExcel",lista);
	}
	
	public void agregaInformePDF(HttpServletRequest req, Vector<String> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("consultaDocPDF",lista);
	}

	public void agregaOtrosIngPDF(HttpServletRequest req, Vector<String> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listaOtrosIngPDF",lista);

	}
	public void agregaRemuneracionPDF(HttpServletRequest req, Vector<String> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listaRemuneracionPDF",lista);

	}
	public void agregaNominaPDF(HttpServletRequest req, Vector<String> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listaNominaPDF",lista);
	}
	public boolean getPeriodoProcesos() throws Exception{
		return presupuestoDao.getPeriodoProcesos();
	}
	public void agregaMenuControl(HttpServletRequest req, String menu){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("menu",menu);		
	}
	public void agregaEjecucionRemuneracionExportar(HttpServletRequest req, Vector<String> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("ejecucionRemuneracionExportar",lista);
	}
	public void agregaEjecucionMensualExportar(HttpServletRequest req, Vector<String> lista1, Vector<String> lista2 , Vector<String> lista3){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("ejecucionMensualExportarET1",lista1);
		sesion.setAttribute("ejecucionMensualExportarET2",lista2);
		sesion.setAttribute("ejecucionMensualExportarET3",lista3);
	}
	public void agregaEjecucionExcel(HttpServletRequest req, Vector<String> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("informeEjecucionExcel",lista);
	}
	public void agregaMovDisponibleExcel(HttpServletRequest req, Vector<String> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("informeMovDisponibleExcel",lista);

	}
	public void agregaMovEjecucionExcel(HttpServletRequest req, Vector<String> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("informeMovEjecucionExcel",lista);

	}
	public void agregaListado(HttpServletRequest req, List<Presw25DTO> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listado",lista);
		
	}
	public List<Cocof17DTO> getListaSedeAlterada(AccionWeb accionweb) throws Exception {
		CocofBean cocofBean = new CocofBean();
	    List<Cocof17DTO> listCocofBean = new ArrayList<Cocof17DTO>();
	    listCocofBean = cocofBean.buscar_todos_cocof17();
		 /*
		  * Pedro Peralta Santiba�ez: las sedes deben salir 1 2 3 7 4 y 6 central vi�a conce sanjoaco vitacura y aca
		  * */
	    List<Cocof17DTO> lista = new ArrayList<Cocof17DTO>();
	    Cocof17DTO cocof17DTO = new Cocof17DTO();
		for(Cocof17DTO ss: listCocofBean){
			if(ss.getCodsuc().toString().trim().equals("1") || ss.getCodsuc().toString().trim().equals("2") || ss.getCodsuc().toString().trim().equals("3") || ss.getCodsuc().toString().trim().equals("7")){
				cocof17DTO = new Cocof17DTO();
				cocof17DTO.setCodsuc(ss.getCodsuc());
				cocof17DTO.setNomsuc(ss.getNomsuc());
				lista.add(cocof17DTO);
				}
		}
		for(Cocof17DTO ss: listCocofBean){
			if(ss.getCodsuc().toString().trim().equals("4") || ss.getCodsuc().toString().trim().equals("6")){
				cocof17DTO = new Cocof17DTO();
				cocof17DTO.setCodsuc(ss.getCodsuc());
				cocof17DTO.setNomsuc(ss.getNomsuc());
				lista.add(cocof17DTO);
				}
		}
	return lista;
		}
	//public List<Presw25DTO> getListaPresw25(String tipo, int rutUsuario, int anno, String dv, int sucur, int codUni, int numDoc){
	public List<Presw25DTO> getListaSede(int rut, String dv, int anno){
	    List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
	   
	    lista = this.getListaPresw25("ANO", rut, anno, dv, 0,0,0);
	return lista;
	}
	public List<Presw25DTO> getListadepartamento(int rut, String dv, int anno, int sede){
	    List<Presw25DTO> lista = new ArrayList<Presw25DTO>();	   
	    lista = this.getListaPresw25("ANS", rut, anno, dv, sede,0,0);
	
	return lista;
	}
	public List<Presw25DTO> getListaCarrera(int rut, String dv, int anno, int sede, int departamento){
	    List<Presw25DTO> lista = new ArrayList<Presw25DTO>();	   
	    lista = this.getListaPresw25("ASD", rut, anno, dv, sede,departamento,0);
	return lista;
	}
	public List<Presw25DTO> getListaMencion(int rut, String dv, int anno, int sede, int departamento, int carrera){
	    List<Presw25DTO> lista = new ArrayList<Presw25DTO>();	   
	    lista = this.getListaPresw25("ASC", rut, anno, dv, sede,departamento,carrera);
	return lista;
	}	
	public void getDetalleIngresosPregrado(HttpServletRequest req,int rut, String dv, int anno, int sede, int departamento, int carrera, int mencion){
		  List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		    Presw25DTO presw25DTO = new Presw25DTO();
			PreswBean preswbean = new PreswBean("STP", 0, 0, 0, 0, rut);
			preswbean.setDigide(dv);
			preswbean.setAnopar(anno);	
			preswbean.setSucur(sede);
			preswbean.setCoduni(departamento);
			preswbean.setNumdoc(carrera);
			preswbean.setNumcom(mencion);
			
			Collection<Presw25DTO> listaPresw25 = (Collection<Presw25DTO>) preswbean.consulta_presw25();
			MathTool mathtool = new MathTool();
			for (Presw25DTO ss : listaPresw25){
				presw25DTO = new Presw25DTO();
				presw25DTO.setDesite(ss.getDesite());
				presw25DTO.setPres01(ss.getPres01());
				presw25DTO.setPres02(ss.getPres02());
				presw25DTO.setPres03(ss.getPres03());
				presw25DTO.setPres04(ss.getPres04());
				presw25DTO.setPres05(ss.getPres05());
				presw25DTO.setPres06(ss.getPres06());
				presw25DTO.setPres07(ss.getPres07());
				lista.add(presw25DTO);
			}
			HttpSession sesion = req.getSession();
			sesion.setAttribute("presupIngresosPregrado",lista);
			sesion.setAttribute("listado",lista);

	}	
	
	public void agregaListaSede(HttpServletRequest req, Vector<String> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listaSede",lista);
	}
	
	public void agregaPresupIngreso(HttpServletRequest req, Vector<String> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listadoPresupIngreso",lista);

	}
	public void agregaPresupSerie(HttpServletRequest req, Vector<String> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listadoPresupSerie",lista);

	}
	public void agregaIndicadores(HttpServletRequest req, Vector<String> lista){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listadoIndicadores",lista);

	}
	
	
	public List<Presw25DTO> retornaTotalIngresos(List<Presw25DTO> listaPresw25, int expresado){
	List<Presw25DTO> listaTotalIngreso = new ArrayList<Presw25DTO>();
	Presw25DTO presw25DTO = new Presw25DTO();
	if(expresado == 2){
		
		for (Presw25DTO ss : listaPresw25){
			if(ss.getDesite().trim().equals("TOTAL INGRESOS")){
				presw25DTO = new Presw25DTO();
			    presw25DTO.setPres01(ss.getPres01());
			    presw25DTO.setPres02(ss.getPres02());
			    presw25DTO.setPres03(ss.getPres03());
			    presw25DTO.setPres04(ss.getPres04());
			    presw25DTO.setPres05(ss.getPres05());
			    presw25DTO.setPres06(ss.getPres06());
			    presw25DTO.setPres07(ss.getPres07());
			    listaTotalIngreso.add(presw25DTO);
			}
		}
	} else {
		presw25DTO = new Presw25DTO();
	    presw25DTO.setPres01(1);
	    presw25DTO.setPres02(1);
	    presw25DTO.setPres03(1);
	    presw25DTO.setPres04(1);
	    presw25DTO.setPres05(1);
	    presw25DTO.setPres06(1);
	    presw25DTO.setPres07(1);
	    listaTotalIngreso.add(presw25DTO);
	}
	return listaTotalIngreso;
	}
	public List<Presw25DTO> getListaEjeEstrategico(){
		List<Presw25DTO> listaEjeEstrategico = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Liderazgo en Ingenier�a, Ciencia y Tecno");
		presw25DTO.setComen2("Liderazgo en Ingenieria, Ciencia y Tecno");
		listaEjeEstrategico.add(presw25DTO);
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Universidad para toda la vida");
		presw25DTO.setComen2("Universidad para toda la vida");
		listaEjeEstrategico.add(presw25DTO);	
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Generar valor a las empresas");
		presw25DTO.setComen2("Generar valor a las empresas");
		listaEjeEstrategico.add(presw25DTO);	
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Relaci�n de largo plazo con los ex-alumn");
		presw25DTO.setComen2("Relacion de largo plazo con los ex-alumn");
		listaEjeEstrategico.add(presw25DTO);	
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Compromiso Solidario");
		presw25DTO.setComen2("Compromiso Solidario");
		listaEjeEstrategico.add(presw25DTO);	
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Gesti�n Competitiva");
		presw25DTO.setComen2("Gestion Competitiva");
		listaEjeEstrategico.add(presw25DTO);
		return listaEjeEstrategico;
	}
	public List<Presw25DTO> getListaAreaAcreditacion(){
		List<Presw25DTO> listaAreaAcreditacion = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Gesti�n Institucional");
		presw25DTO.setComen2("Gestion Institucional");
		listaAreaAcreditacion.add(presw25DTO);
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Docencia de Pregrado");
		presw25DTO.setComen2("Docencia de Pregrado");
		listaAreaAcreditacion.add(presw25DTO);	
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Investigaci�n");
		presw25DTO.setComen2("Investigacion");
		listaAreaAcreditacion.add(presw25DTO);	
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Postgrados");
		presw25DTO.setComen2("Postgrados");
		listaAreaAcreditacion.add(presw25DTO);	
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Vinculaci�n con el medio");
		presw25DTO.setComen2("Vinculacion con el medio");
		listaAreaAcreditacion.add(presw25DTO);	
		return listaAreaAcreditacion;
	}
	public List<Presw25DTO> getListaTipoActividad(){
		List<Presw25DTO> listaTipoActividad = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Equipamiento de laboratorios 1a $ 5MM");
		presw25DTO.setComen2("Equipamiento de laboratorios 1a $ 5MM");
		listaTipoActividad.add(presw25DTO);
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Equipamiento de laboratorios 1b $ 20MM");
		presw25DTO.setComen2("Equipamiento de laboratorios 1b $ 20MM");
		listaTipoActividad.add(presw25DTO);
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Mejoramiento de Indicadores de Pregrado: Retenci�n y Titulaci�n Oportuna");
		presw25DTO.setComen2("Mejoramiento Indicador Pregr: Ret y Tit ");
		listaTipoActividad.add(presw25DTO);	
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Planes de mejora de Acreditaci�n de Programas de Pregrado y Postgrados");
		presw25DTO.setComen2("Mejora Acredit Programas Pre y Postgrado");
		listaTipoActividad.add(presw25DTO);	
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Aumento de Dotaci�n de Profesores");
		presw25DTO.setComen2("Aumento de Dotacion de Profesores");
		listaTipoActividad.add(presw25DTO);
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Programas Proyecto Estrat�gico Institucional 2014-2018");
		presw25DTO.setComen2("Programas Proy Estrateg Inst 2014-2018");
		listaTipoActividad.add(presw25DTO);
	
		return listaTipoActividad;
	}
	public List<Presw25DTO> agregaDatosActividad(HttpServletRequest req,
												String codgru,
												String descripcionActiv,	
												String objetivoActiv,
												String ejeActiv,	
												String areaActiv,
												int rutide,
												String digide,
												String nombreResponsable,
												int a�o,
												int numActividad,
												String accion,
												long totalActiv,
												String tipoActividad){
		HttpSession sesion = req.getSession();
			
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
	
		Presw25DTO presw25 = new Presw25DTO();
		String parte1 = "";
		String parte2 = "";
		String motiv1 = "";
		String motiv2 = "";
		if(nombreResponsable.length() > 40)
			nombreResponsable = nombreResponsable.substring(0,39);
		if(descripcionActiv.trim().length() > 40){
			parte1 = descripcionActiv.substring(0,39);
			parte2 = descripcionActiv.substring(39);
		} else
			parte1 = descripcionActiv.trim();
		if(objetivoActiv.trim().length() > 40){
			motiv1 = objetivoActiv.substring(0,39);	
			motiv2 = objetivoActiv.substring(40);
		} else
			motiv1 = objetivoActiv.trim();	
		presw25.setComen1(parte1);
		presw25.setComen2(parte2);
		//presw25.setComen3(objetivoActiv);
		presw25.setMotiv1(motiv1);// objetivoActiv
		presw25.setMotiv2(motiv2);// objetivoActiv
		presw25.setComen4(ejeActiv);
		presw25.setComen5(areaActiv);
		presw25.setComen6(tipoActividad);
		presw25.setPres01(totalActiv);
		presw25.setRutide(rutide);
		presw25.setDigide(digide);
		presw25.setNomfun(nombreResponsable.trim());
		presw25.setAnopre(a�o);
		presw25.setMespre(numActividad);
		presw25.setDesite(accion);
		presw25.setCodgru(codgru);
		lista.add(presw25);
		
		return lista;
	}
	public List<Presw25DTO> agregaObservacionPlan(HttpServletRequest req,String motiv1 ,
			String motiv2,
			String motiv3,
			String motiv4,
			String motiv5,
			String motiv6,
			String comen1,	
			int a�o){
		HttpSession sesion = req.getSession();

		int rutUsuario = Integer.parseInt(Util.validaParametro(sesion.getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(sesion.getAttribute("dv")+"","");

		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();

		Presw25DTO presw25 = new Presw25DTO();

		presw25.setMotiv1(motiv1);
		presw25.setMotiv2(motiv2);
		presw25.setMotiv3(motiv3);
		presw25.setMotiv4(motiv4);
		presw25.setMotiv5(motiv5);
		presw25.setMotiv6(motiv6);
		presw25.setComen1(comen1);
		presw25.setRutide(rutUsuario);
		presw25.setDigide(dv);
		presw25.setAnopre(a�o);
		lista.add(presw25);

return lista;
}
	public boolean saveObservacionPlan(List<Presw25DTO> lista, int rut, String dv, int a�o){
		PreswBean preswbean = new PreswBean("GPD", 0,0,0,0, rut);
		preswbean.setDigide(dv);
		preswbean.setAnopar(a�o);		
		return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	}
	public boolean savePresupActividad(List<Presw25DTO> lista, int rut, String dv, int a�o, String codgru){
 		PreswBean preswbean = new PreswBean("GDD", 0,0,0,0, rut);
		preswbean.setDigide(dv);
		preswbean.setAnopar(a�o);	
		preswbean.setCajero(codgru);
		return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	}
	
	/*plan de desarrollo*/
	public void eliminaItemPlan(HttpServletRequest req, List<Presw25DTO> lista){
		HttpSession sesion = req.getSession();
		int codActividad = Util.validaParametro(req.getParameter("codActividad"), 0);
		//int itedoc = Util.validaParametro(req.getParameter("item"), 0);
		//MAA
		String itedoc = Util.validaParametro(req.getParameter("item"),"");
		List<Presw25DTO> listarResultado = new ArrayList<Presw25DTO>();
		for (Presw25DTO ss : lista){
		 // if(ss.getNumreq() == codActividad && ss.getItedoc() == itedoc){
			if(ss.getNumreq() == codActividad && ss.getCodman().trim().equals(itedoc.trim())){	
			  ss.setPres01(0);
			  ss.setPres02(0);
			  ss.setPres03(0);
			  ss.setPres04(0);
			  ss.setPres05(0);
			  ss.setPres06(0);
			  ss.setPres07(0);
			  ss.setPres08(0);
			  ss.setPres09(0);
			  ss.setPres10(0);
			  ss.setPres11(0);
			  ss.setPres12(0);
			  ss.setTotpre(0);
			  ss.setComen1("");
			  ss.setTotpre(0);
			  //MAA
			  ss.setCodman(itedoc);
		
		  }
		  listarResultado.add(ss);		  
		}

		sesion.setAttribute("resultadoPresup", listarResultado);
	}	
	public void agregaItemActualizaPlan(HttpServletRequest req, List<Presw25DTO> lista){
		HttpSession sesion = req.getSession();

		
		String ene = Util.validaParametro(req.getParameter("mes1"),"0");
		String feb = Util.validaParametro(req.getParameter("mes2"),"0");
		String mar = Util.validaParametro(req.getParameter("mes3"),"0");
		String abr = Util.validaParametro(req.getParameter("mes4"),"0");
		String may = Util.validaParametro(req.getParameter("mes5"),"0");
		String jun = Util.validaParametro(req.getParameter("mes6"),"0");
		String jul = Util.validaParametro(req.getParameter("mes7"),"0");
		String ago = Util.validaParametro(req.getParameter("mes8"),"0");
		String sep = Util.validaParametro(req.getParameter("mes9"),"0");
		String oct = Util.validaParametro(req.getParameter("mes10"),"0");
		String nov = Util.validaParametro(req.getParameter("mes11"),"0");
		String dic = Util.validaParametro(req.getParameter("mes12"),"0");
		ene = ene.replace(".", "");
		feb = feb.replace(".", "");
		mar = mar.replace(".", "");
		abr = abr.replace(".", "");
		may = may.replace(".", "");
		jun = jun.replace(".", "");
		jul = jul.replace(".", "");
		ago = ago.replace(".", "");
		sep = sep.replace(".", "");
		oct = oct.replace(".", "");
		nov = nov.replace(".", "");
		dic = dic.replace(".", "");

		Long mes1 = Long.parseLong(ene);
		Long mes2 = Long.parseLong(feb);
		Long mes3 = Long.parseLong(mar);
		Long mes4 = Long.parseLong(abr);
		Long mes5 = Long.parseLong(may);
		Long mes6 = Long.parseLong(jun);
		Long mes7 = Long.parseLong(jul);
		Long mes8 = Long.parseLong(ago);
		Long mes9 = Long.parseLong(sep);
		Long mes10 = Long.parseLong(oct);
		Long mes11 = Long.parseLong(nov);
		Long mes12 = Long.parseLong(dic);
		Long totpre = new Long(0);
		totpre = mes1 + mes2 + mes3 + mes4 + mes5 + mes6 + mes7 + mes8 + mes9 + mes10 + mes11 + mes12;
			
		int codActividad = Util.validaParametro(req.getParameter("codActividad"), 0);
	    String itedoc = Util.validaParametro(req.getParameter("itedoc"), "");
		//int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		List<Presw25DTO> listarResultado = new ArrayList<Presw25DTO>();
		String comen1 = Util.validaParametro(req.getParameter("comen1"), "");
		String comen2 = Util.validaParametro(req.getParameter("comen2"),"");
		String comen3 = Util.validaParametro(req.getParameter("comen3"),"");
		String comen4 = Util.validaParametro(req.getParameter("comen4"),"");
		String comen5 = Util.validaParametro(req.getParameter("comen5"),"");
		String comen6 = Util.validaParametro(req.getParameter("comen6"),"");
		for (Presw25DTO ss : lista){
			//System.out.println(ss.getItedoc());
		 // if(ss.getNumreq() == codActividad && ss.getItedoc() == itedoc){
			 if(ss.getNumreq() == codActividad && ss.getCodman().trim().equals(itedoc.trim())){
					
			//  System.out.println(ss.getItedoc());
			  ss.setPres01(mes1);
			  ss.setPres02(mes2);
			  ss.setPres03(mes3);
			  ss.setPres04(mes4);
			  ss.setPres05(mes5);
			  ss.setPres06(mes6);
			  ss.setPres07(mes7);
			  ss.setPres08(mes8);
			  ss.setPres09(mes9);
			  ss.setPres10(mes10);
			  ss.setPres11(mes11);
			  ss.setPres12(mes12);
			  ss.setTotpre(totpre);
			  ss.setComen1(comen1);
			  ss.setComen2(comen2);
			  ss.setComen3(comen3);
			  ss.setComen4(comen4);
			  ss.setComen5(comen5);
			  ss.setComen6(comen6);
		
		  }
		  listarResultado.add(ss);		  
		}
		sesion.setAttribute("resultadoPresup", listarResultado);
	}
	public void eliminaReqPlan(HttpServletRequest req, List<Presw25DTO> lista, List<Presw25DTO> listaDetalle){
		HttpSession sesion = req.getSession();
		int codActividad = Util.validaParametro(req.getParameter("codActividad"), 0);
		int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		int mes = Util.validaParametro(req.getParameter("mes"), 0);
		int sucur = Util.validaParametro(req.getParameter("sucur"), 0);
		List<Presw25DTO> listarResultado = new ArrayList<Presw25DTO>();
		List<Presw25DTO> listarResultadoDet = new ArrayList<Presw25DTO>();
		for (Presw25DTO ss : lista){
		  if(ss.getItedoc() == itedoc && ss.getMespre() == mes && ss.getNumreq() == sucur){
			  
			  ss.setCanman(0);
			  ss.setIndmod("D");
			  ss.setTotpre(0);
		
		  }
		  listarResultado.add(ss);		  
		}
		if(listaDetalle != null && listaDetalle.size() > 0){
		for (Presw25DTO ss : listaDetalle){
			  if( ss.getItedoc() == itedoc && ss.getMespre() == mes && ss.getNumreq() == sucur){
				  ss.setTippro("RPQ");
				  ss.setIndmod("D");
				  ss.setCanman(0);
				  ss.setTotpre(0);
			
			  }
			  listarResultadoDet.add(ss);		  
			}
		}
		sesion.setAttribute("presupItemreq", listarResultado);
		sesion.setAttribute("resultadoDetalleItemReq", listarResultadoDet);
	}
	public void agregaDetalleItemReqSesionPlan(HttpServletRequest req, List<Presw25DTO> lista, List<Presw25DTO> lista2){
		HttpSession sesion = req.getSession();
		int rutUsuario = Util.validaParametro(Integer.parseInt(req.getSession().getAttribute("rutUsuario")+ ""),0);
		int a�o = Util.validaParametro(req.getParameter("anno"), 0);
		int codActividad = Util.validaParametro(req.getParameter("codActividad"), 0);
		int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		int mes = Util.validaParametro(req.getParameter("mes"), 0);
		int sucur = Util.validaParametro(req.getParameter("sucur"), 0);
		int numrq = 0;
		Long totalMensual = Util.validaParametro(req.getParameter("total"), new Long(0));
		List<Presw25DTO> lista3 =  new ArrayList<Presw25DTO>();
		List<Presw25DTO> lista4 =  new ArrayList<Presw25DTO>();
		
		
			
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		
		
		
		/*primero verificar si es requerimiento nuevo, se coloca como n�mero 1000 */
		if(sucur == 0){
			if(lista2.size() > 0) {
			for (Presw25DTO ss : lista2){
				if(ss.getNumreq() > numrq)
					numrq = ss.getNumreq() ;
			}
			numrq++;
			} else numrq = 1000;
			/* y agregarlo a la lista en lista3 */
			for (Presw25DTO ss : lista){
				ss.setNumreq(numrq);
				ss.setValpr1(1); // esto es solo para marcar que es nuevo requerimiento
				ss.setCanman(0);
				lista3.add(ss);
			}
		} else { 
			numrq = sucur;
			for (Presw25DTO ss : lista){
				lista3.add(ss);
			}
		}
		
		boolean agrega = false;
		/*recorre la lista de sesi�n y ve si ya existe sino lo agrega a lista4 que tiene la suma final*/
	    if(lista2 != null && lista2.size() > 0){	    
		for (Presw25DTO ss : lista2){	
			if(ss.getNumreq() == numrq && ss.getItedoc() == itedoc){
			   for (Presw25DTO rs : lista3){
				   if(/*rs.getCoduni() == ss.getCoduni() &&*/ rs.getItedoc() == ss.getItedoc() && rs.getNumreq() == ss.getNumreq() && rs.getMespre() == ss.getMespre() && rs.getCodman() == ss.getCodman()){
						lista4.add(rs);
						agrega = true;
				   }	
						
				}
			} else { lista4.add(ss);
			         
			}
				} 
			} else {
				   for (Presw25DTO rs : lista3){
					  lista4.add(rs);							
					}
				   agrega = true;
			}
		
		 if(!agrega){
			   for (Presw25DTO rs : lista3){
					  lista4.add(rs);							
					}
		 }
	    
	   sesion.setAttribute("listaPresw25",lista3);
		sesion.setAttribute("resultadoDetalleItemReq",lista4);
		
}
		public void agregaItemReqDetPlan(HttpServletRequest req, List<Presw25DTO> lista, List<Presw25DTO> presupItemreq , List<Presw25DTO> listarResultadoPresup){
	        //            resultadoDetalleItemReq, presupItemreq, listarResultadoPresup
			HttpSession sesion = req.getSession();
			List<Presw25DTO> listaRQP = new ArrayList();
			List<Presw25DTO> listaResultado = new ArrayList();
			
			String area1 = Util.validaParametro(req.getParameter("area1"),"");
			String area2 = Util.validaParametro(req.getParameter("area2"),"");
			String comen1 = Util.validaParametro(req.getParameter("comen1"),"");
			String comen2 = Util.validaParametro(req.getParameter("comen2"),"");
			String comen3 = Util.validaParametro(req.getParameter("comen3"),"");
			String comen4 = Util.validaParametro(req.getParameter("comen4"),"");
			String comen5 = Util.validaParametro(req.getParameter("comen5"),"");
			String comen6 = Util.validaParametro(req.getParameter("comen6"),"");
			String motiv1 = Util.validaParametro(req.getParameter("motiv1"),"");
			String motiv2 = Util.validaParametro(req.getParameter("motiv2"),"");
			String motiv3 = Util.validaParametro(req.getParameter("motiv3"),"");
			String motiv4 = Util.validaParametro(req.getParameter("motiv4"),"");
			String motiv5 = Util.validaParametro(req.getParameter("motiv5"),"");
			String motiv6 = Util.validaParametro(req.getParameter("motiv6"),"");
			String codgru = Util.validaParametro(req.getParameter("codgru"),"");
			Long total = Util.validaParametro(req.getParameter("totalRequ"),new Long(0));
			int mes = Util.validaParametro(req.getParameter("mes"),0);
			int numreq = Util.validaParametro(req.getParameter("sucur"), 0);
			int item = Util.validaParametro(req.getParameter("itedoc"), 0);
			int codActividad = Util.validaParametro(req.getParameter("codActividad"), 0);
			String nomActividad = Util.validaParametro(req.getParameter("nomActividad"),"");
			int anno = Util.validaParametro(req.getParameter("anno"), 0);
			Long totalTotal = Util.validaParametro(req.getParameter("totalMes"), new Long(0));
			int i = Util.validaParametro(req.getParameter("item"), 0);
			int itemElimina = Util.validaParametro(req.getParameter("itemElimina"), 0);
			if(itemElimina > 0) i = itemElimina;
			int j = 0;
			long pres03 = 0;
			long pres01 = 0;
			String fecha = "";
			int pres02 = 0;
				   
			int valpr = 0;
			String desite = "";
			String desuni = "";
			int canman = 0;
			long valorReq = 0;
			if(lista.size() > 0){
			for (Presw25DTO ss : lista){
				 pres02 = 0;
				 pres03 = 0;
				 pres01 = 0;
				
				if(ss.getNumreq() == numreq && ss.getMespre() == mes  ){
					//System.out.println(ss.getItedoc());
					j++;
					if(j==i) {
						  if(itemElimina > 0) {
							  if(ss.getCodman().trim().equals("COTIZACION")){
								  valorReq = ss.getPres03();
								  ss.setCanman(0);
							  } else
								  valorReq = (ss.getCanman() * ss.getValuni());  
						    	totalTotal -= valorReq;
						    	total -= valorReq;
						    	ss.setIndmod("D");
						  } else valorReq = 0;
						 canman = Util.validaParametro(req.getParameter("canman"),0);
						 pres03 = Util.validaParametro(req.getParameter("pres03"), 0);
						 pres01 = Util.validaParametro(req.getParameter("pres01"), 0);
						 fecha = Util.validaParametro(req.getParameter("fechaInicio"),"");
						 if(!fecha.trim().equals(""))
							 pres02 = Integer.parseInt(fecha.substring(6)+ fecha.substring(3,5)+fecha.substring(0,2));

						 if(pres03 > 0){
					     valorReq = pres03;
					    // canman = 1;
					    	 } else {
							     valorReq = (canman * ss.getValuni());
					     }
						 total += valorReq;				  
					     totalTotal = totalTotal + valorReq;
					
					   
					} else
						canman = ss.getCanman();
			//	if(canman > 0) {	
				ss.setTippro("RPQ");
				ss.setArea1(area1);
				ss.setArea2(area2);
				ss.setComen1(comen1);
				ss.setComen2(comen2);
				ss.setComen3(comen3);
				ss.setComen4(comen4);
				ss.setComen5(comen5);
				ss.setComen6(comen6);
				ss.setMotiv1(motiv1);
				ss.setMotiv2(motiv2);
				ss.setMotiv3(motiv3);
				ss.setMotiv4(motiv4);
				ss.setMotiv5(motiv5);
				ss.setMotiv6(motiv6);
				ss.setCanman(canman);
				ss.setPres01(pres01);
				ss.setPres02(pres02);
				ss.setPres03(pres03);
				ss.setCodgru(codgru);
				listaRQP.add((Presw25DTO)ss);
			    valpr = ss.getValpr1();
			    desite = ss.getDesite();
			    desuni = ss.getDesuni();
			//	}
			 
			    
				} else listaRQP.add(ss);
		      
			}

			}
		    /*agrega en MAN*/
			Presw25DTO presw25 = new Presw25DTO();
			presw25.setTippro("MNA");
			presw25.setItedoc(item);
			presw25.setAnopre(anno);
			presw25.setMespre(mes);
			presw25.setNumreq(numreq);
			presw25.setValpr1(valpr);
			presw25.setDesite(desite);
			presw25.setComen1(area1);
			presw25.setComen2(area2);
			presw25.setDesuni(desuni);
			presw25.setTotpre(total);
			presw25.setCodgru(codgru);
			if(total == 0)
				presw25.setIndmod("D");
	        if(presupItemreq != null && presupItemreq.size() > 0){
	        	i = -1;
	        	boolean agrega = false;
	        	for (Presw25DTO ss : presupItemreq){
	        		i++;
	        		if(ss.getItedoc() == item && ss.getMespre() == mes && ss.getNumreq() == numreq){
	        			presupItemreq.set(i, presw25);
	        			agrega = true;
	        		}        		      			
	        	}
	    	    if(!agrega)
	    	    	presupItemreq.add(presw25);
	        } else presupItemreq.add(presw25);
			
	     	Long totalItemReq = new Long(0);
	     	Long ene = new Long(0);
	     	Long feb = new Long(0);
	    	Long mar = new Long(0);
	     	Long abr = new Long(0);
	     	Long may = new Long(0);
	     	Long jun = new Long(0);
	     	Long jul = new Long(0);
	     	Long ago = new Long(0);
	     	Long sep = new Long(0);
	     	Long oct = new Long(0);
	     	Long nov = new Long(0);
	     	Long dic = new Long(0);
	     	
			if(listarResultadoPresup.size() > 0){
				for (Presw25DTO ss : presupItemreq){ // agregar todo lo que est� en mantencion al presup
					if(ss.getItedoc() == item  ) {
						switch (ss.getMespre()){
						case 1: ene = ene + ss.getTotpre();
						break;
						case 2: feb = feb + ss.getTotpre();
						break;
						case 3: mar = mar + ss.getTotpre();
						break;
						case 4: abr = abr + ss.getTotpre();
						break;
						case 5: may = may + ss.getTotpre();
						break;
						case 6: jun = jun + ss.getTotpre();
						break;
						case 7: jul = jul + ss.getTotpre();
						break;
						case 8: ago = ago + ss.getTotpre();
						break;
						case 9: sep = sep + ss.getTotpre();
						break;
						case 10: oct = oct + ss.getTotpre();
						break;
						case 11: nov = nov + ss.getTotpre();
						break;
						case 12: dic = dic + ss.getTotpre();
						break;
						}
						totalItemReq = totalItemReq + ss.getTotpre();
					}
				}
			        
				i =-1;
				for (Presw25DTO ss : listarResultadoPresup){
					i++;
					if(ss.getItedoc() == item ){
						ss.setItedoc(item);
						ss.setPres01(ene);
						ss.setPres02(feb);
						ss.setPres03(mar);
						ss.setPres04(abr);
						ss.setPres05(may);
						ss.setPres06(jun);
						ss.setPres07(jul);
						ss.setPres08(ago);
						ss.setPres09(sep);
						ss.setPres10(oct);
						ss.setPres11(nov);
						ss.setPres12(dic);
						ss.setTotpre(Long.parseLong(String.valueOf(ss.getPres01() + ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() + ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() + ss.getPres10() + ss.getPres11() + ss.getPres12())));
					}
					listaResultado.add(ss);
					//System.out.println("result: " + ss.getTotpre());
				}
				
			}
		/*	System.out.println("tama�o presupItemreq: "+presupItemreq.size());
			for (Presw25DTO ss : presupItemreq){
				
				System.out.println("ss "+ss.getMespre()+"cant "  + ss.getPres01()+"req: "+ ss.getNumreq());
				System.out.println("ss "+ss.getMespre()+"cant " + ss.getPres02()+"req: "+ ss.getNumreq());
				System.out.println("ss "+ss.getMespre()+"cant " + ss.getPres03()+"req: "+ ss.getNumreq());
				
			}
			System.out.println("presupItemreq desp **: "+presupItemreq.size());*/
			if(presupItemreq != null )
			   sesion.setAttribute("presupItemreq",presupItemreq);
			sesion.setAttribute("resultadoDetalleItemReq",listaRQP);

			sesion.setAttribute("resultadoPresup", listaResultado);	
			//req.setAttribute("totalMes", totalTotal);
			
		}
		
		public void actualizaItemReqDetPlan(HttpServletRequest req, List<Presw25DTO> lista, List<Presw25DTO> presupItemreq){
	        //            resultadoDetalleItemReq, presupItemreq, listarResultadoPresup
	HttpSession sesion = req.getSession();
	List<Presw25DTO> listaRQP = new ArrayList();
	List<Presw25DTO> listaResultado = new ArrayList();
	
	String area1 = Util.validaParametro(req.getParameter("area1"),"");
	String area2 = Util.validaParametro(req.getParameter("area2"),"");
	String comen1 = Util.validaParametro(req.getParameter("comen1"),"");
	String comen2 = Util.validaParametro(req.getParameter("comen2"),"");
	String comen3 = Util.validaParametro(req.getParameter("comen3"),"");
	String comen4 = Util.validaParametro(req.getParameter("comen4"),"");
	String comen5 = Util.validaParametro(req.getParameter("comen5"),"");
	String comen6 = Util.validaParametro(req.getParameter("comen6"),"");
	String motiv1 = Util.validaParametro(req.getParameter("motiv1"),"");
	String motiv2 = Util.validaParametro(req.getParameter("motiv2"),"");
	String motiv3 = Util.validaParametro(req.getParameter("motiv3"),"");
	String motiv4 = Util.validaParametro(req.getParameter("motiv4"),"");
	String motiv5 = Util.validaParametro(req.getParameter("motiv5"),"");
	String motiv6 = Util.validaParametro(req.getParameter("motiv6"),"");
	int mes = Util.validaParametro(req.getParameter("mes"),0);
	int numreq = Util.validaParametro(req.getParameter("sucur"), 0);
	int item = Util.validaParametro(req.getParameter("itedoc"), 0);
	int codActividad = Util.validaParametro(req.getParameter("codActividad"), 0);
	int anno = Util.validaParametro(req.getParameter("anno"), 0);


	if(lista.size() > 0){
	for (Presw25DTO ss : lista){

	if(ss.getNumreq() == numreq && ss.getMespre() == mes  ){
	//System.out.println(ss.getItedoc());
		
	
	ss.setTippro("RPQ");
	ss.setArea1(area1);
	ss.setArea2(area2);
	ss.setComen1(comen1);
	ss.setComen2(comen2);
	ss.setComen3(comen3);
	ss.setComen4(comen4);
	ss.setComen5(comen5);
	ss.setComen6(comen6);
	ss.setMotiv1(motiv1);
	ss.setMotiv2(motiv2);
	ss.setMotiv3(motiv3);
	ss.setMotiv4(motiv4);
	ss.setMotiv5(motiv5);
	ss.setMotiv6(motiv6);
	ss.setCanman(ss.getCanman());
	ss.setPres01(ss.getPres01());
	ss.setPres02(ss.getPres02());
	ss.setPres03(ss.getPres03());
	listaRQP.add((Presw25DTO)ss);				
	
	} else listaRQP.add(ss);
	
	}
	
	}
	/*agrega en MAN*/

	
	for (Presw25DTO ss : presupItemreq){
	
	if(ss.getItedoc() == item && ss.getMespre() == mes && ss.getNumreq() == numreq){
	    ss.setTippro("MNA");
	    ss.setComen1(area1);
		ss.setComen2(area2);
		listaResultado.add((Presw25DTO)ss);
	}   else listaResultado.add(ss);     		      			
	}


	if(presupItemreq != null )
	sesion.setAttribute("presupItemreq",listaResultado);
	sesion.setAttribute("resultadoDetalleItemReq",listaRQP);
	
	
	
	}
	public void agregaRegistraMatrizPlan(HttpServletRequest req, List<Presw25DTO> lista,List<Presw25DTO> lista2){
			HttpSession sesion = req.getSession();
			List<Presw25DTO> listaPRP = new ArrayList();
			List<Presw25DTO> listaCPR = new ArrayList();
			String indexi = Util.validaParametro(req.getParameter("indexi"), "");
			int codActividad = Util.validaParametro(req.getParameter("codActividad"), 0);
			String codgru = Util.validaParametro(req.getParameter("codgru"),"");
			for (Presw25DTO ss : lista){
				Presw25DTO presw25 = ss;
				presw25.setTippro("PGD");
				presw25.setIndexi(indexi);
				presw25.setCodgru(codgru);
				listaPRP.add((Presw25DTO) presw25);
				}
			for (Presw25DTO ss : lista2){
				Presw25DTO presw25 = ss;
				if(ss.getNumreq() == codActividad)
					presw25.setIndexi(indexi);
				listaCPR.add((Presw25DTO) presw25);
				}
			sesion.setAttribute("resultadoPresup",listaPRP);
		sesion.setAttribute("resultadoCPR", listaCPR);
			
	}
	
	public boolean saveMatrizPlan(List<Presw25DTO> lista, int codActividad, int a�o, int rutUsuario, String dv){
		PreswBean preswbean = new PreswBean("PGD", 0,0,a�o,0, rutUsuario);
		preswbean.setDigide(dv);
		preswbean.setMespar(codActividad);
		for (Presw25DTO ss : lista){
			ss.setDesite(ss.getCodman());
		}
			
		
		return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	}
	public boolean saveREQPlan(List<Presw25DTO> lista, int codActividad, int a�o, int rutUsuario, int mes, String dv, String codgru){
		 boolean retorno = false;
		 List<Presw25DTO> listaRQP = new ArrayList();
		 int item = 0;
		 int numreq = 0;
		 int mesAnt = 1;
		 if(lista != null && lista.size() > 0){
		for (Presw25DTO ss : lista){
			if(numreq != ss.getNumreq() /*&& mesAnt == ss.getMespre()*/){
				ss.setIndexi("I");
			/*	if(ss.getCanman() == 0)
					ss.setIndmod("D");*/
			}
			numreq = ss.getNumreq();
			item = ss.getItedoc();
			mesAnt = ss.getMespre();
				if(ss.getValpr1() == 1){
					
					ss.setNumreq(0);
					
				}
			ss.setTotpre(codActividad);
			ss.setRutide(rutUsuario);
			ss.setDigide(dv);
			ss.setCodgru(codgru);
			listaRQP.add(ss);
		//	System.out.println(ss.getIndexi()+ "  ind : "+ ss.getIndmod()+"  ss.getMespre(): " + ss.getMespre()+"   ss.getCanman(): "+ss.getCanman()+"  numreq: "+numreq+ "    ss.getNumreq(): "+ss.getNumreq());
			}
		 }
		PreswBean preswbean = new PreswBean("RPQ", 0,item,a�o,mes, rutUsuario);
		preswbean.setDigide(dv);
		preswbean.setCodban(codActividad);
		preswbean.setCajero(codgru);
		retorno = preswbean.ingreso_presw25((List<Presw25DTO> )listaRQP);
	
		return retorno;
	}
	public Vector getlistaReqResumenExcelPlan(HttpServletRequest req){
		//int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		String itedoc = Util.validaParametro(req.getParameter("itedoc"), "");
		int a�o = Util.validaParametro(req.getParameter("anno"), 0);
		int codActividad = Util.validaParametro(req.getParameter("codActividad"), 0);
		String nomActividad = Util.validaParametro(req.getParameter("nomActividad"),"");
		int rutUsuario = Integer.parseInt(req.getSession().getAttribute("rutUsuario")+ "");
		String dv = Util.validaParametro(req.getSession().getAttribute("dv")+"","");
		String codgru = Util.validaParametro(req.getParameter("codgru"),"");
		


		Vector vec_datos = new Vector();
		Vector vec_d = new Vector();
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		long total = 0;
		long totalTotal = 0;
		long enero = 0;
		long febrero = 0;
		long marzo = 0;
		long abril = 0;
		long mayo = 0;
		long junio = 0;
		long julio = 0;
		long agosto = 0;
		long septiembre = 0;
		long octubre = 0;
		long noviembre = 0;
		long diciembre = 0;
		PreswBean preswbean = new PreswBean("PRD", 0,0,a�o,0, rutUsuario);
		NumberFormat nf2 = 	NumberFormat.getInstance(Locale.GERMAN); 

		if(preswbean != null) {		
			preswbean.setDigide(dv);
			preswbean.setRutide(rutUsuario);
			preswbean.setMespar(codActividad);
			preswbean.setCajero(codgru);
			listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
		}
		
		long totalAgrup = 0;
		Vector agrup = null;
		Vector agrup1 = new Vector();
		Vector agrup2 = new Vector();
		Vector agrup3 = new Vector();
		Vector agrup4 = new Vector();
		long total_ENE = 0;	
		long total_FEB = 0;	
		long total_MAR = 0;
		long total_ABR = 0;	
		long total_MAY = 0;	
		long total_JUN = 0;	
		long total_JUL = 0;	
		long total_AGO = 0;	
		long total_SEP = 0;	
		long total_OCT = 0;	
		long total_NOV = 0;	
		long total_DIC = 0;	
		long agrup_ENE = 0;	
		long agrup_FEB = 0;
		long agrup_MAR = 0;	
		long agrup_ABR = 0;	
		long agrup_MAY = 0;	
		long agrup_JUN = 0;	
		long agrup_JUL = 0;	
		long agrup_AGO = 0;	
		long agrup_SEP = 0;	
		long agrup_OCT = 0;	
		long agrup_NOV = 0;	
		long agrup_DIC = 0;
		
		for (Presw25DTO ss : listaPresw25){
			total = ss.getPres01() + ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() + ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() + ss.getPres10() + ss.getPres11() + ss.getPres12();
			total_ENE = ss.getPres01();
			total_FEB = ss.getPres02();
			total_MAR = ss.getPres03();
			total_ABR = ss.getPres04();
			total_MAY = ss.getPres05();
			total_JUN = ss.getPres06();
			total_JUL = ss.getPres07();
			total_AGO = ss.getPres08();
			total_SEP = ss.getPres09();
			total_OCT = ss.getPres10();
			total_NOV = ss.getPres11();
			total_DIC = ss.getPres12();
	        if(!ss.getCodman().trim().equals(ss.getMotiv3().trim())){
		        totalAgrup = totalAgrup + total;
		        agrup_ENE = agrup_ENE + total_ENE;
			    agrup_FEB = agrup_FEB + total_FEB;
				agrup_MAR = agrup_MAR + total_MAR;
				agrup_ABR = agrup_ABR + total_ABR;
				agrup_MAY = agrup_MAY + total_MAY;
				agrup_JUN = agrup_JUN + total_JUN;
				agrup_JUL = agrup_JUL + total_JUL;
				agrup_AGO = agrup_AGO + total_AGO;
				agrup_SEP = agrup_SEP + total_SEP;
				agrup_OCT = agrup_OCT + total_OCT;
				agrup_NOV = agrup_NOV + total_NOV;
				agrup_DIC = agrup_DIC + total_DIC;
	        }  
		    else {
		    	 agrup = new Vector();	 
				 agrup.addElement(totalAgrup);
				 agrup.addElement(agrup_ENE);
				 agrup.addElement(agrup_FEB);
				 agrup.addElement(agrup_MAR);
				 agrup.addElement(agrup_ABR);
				 agrup.addElement(agrup_MAY);
				 agrup.addElement(agrup_JUN);
				 agrup.addElement(agrup_JUL);
				 agrup.addElement(agrup_AGO);
				 agrup.addElement(agrup_SEP);
				 agrup.addElement(agrup_OCT);
				 agrup.addElement(agrup_NOV);
				 agrup.addElement(agrup_DIC);
		    	 if(ss.getCodman().trim().equals("2")) agrup1 = agrup;
		    	 else if(ss.getCodman().trim().equals("3")) agrup2 = agrup;
				 else if(ss.getCodman().trim().equals("4")) agrup3 = agrup;
		    			    	 
		    	 totalAgrup = 0;  
				 agrup_ENE = 0;	
				 agrup_FEB = 0;	
				 agrup_MAR = 0;	
				 agrup_ABR = 0;	
				 agrup_MAY = 0;	
				 agrup_JUN = 0;	
				 agrup_JUL = 0;	
				 agrup_AGO = 0;	
				 agrup_SEP = 0;	
				 agrup_OCT = 0;	
				 agrup_NOV = 0;	
				 agrup_DIC = 0;
				 
				
		      }
		  }  
		 agrup = new Vector();	 
		 agrup.addElement(totalAgrup);
		 agrup.addElement(agrup_ENE);
		 agrup.addElement(agrup_FEB);
		 agrup.addElement(agrup_MAR);
		 agrup.addElement(agrup_ABR);
		 agrup.addElement(agrup_MAY);
		 agrup.addElement(agrup_JUN);
		 agrup.addElement(agrup_JUL);
		 agrup.addElement(agrup_AGO);
		 agrup.addElement(agrup_SEP);
		 agrup.addElement(agrup_OCT);
		 agrup.addElement(agrup_NOV);
		 agrup.addElement(agrup_DIC);
		 agrup4 = agrup;

		for (Presw25DTO ss : listaPresw25){	

			total = ss.getPres01() + ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() + ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() + ss.getPres10() + ss.getPres11() + ss.getPres12();
			//if(total > 0){
			if(total > 0 || ss.getCodman().trim().equals(ss.getMotiv3().trim())){
			  if(!ss.getCodman().trim().equals(ss.getMotiv3().trim())) 
				//vec_datos.add(String.valueOf(ss.getItedoc()));
				     vec_datos.add(ss.getCodman());
			  else vec_datos.add("");
			  vec_datos.add(ss.getDesite());
					
		      agrup = new Vector();
		 	  if(ss.getCodman().trim().equals(ss.getMotiv3().trim())){
			    if(ss.getCodman().trim().equals("1")) agrup = agrup1;
			    else if(ss.getCodman().trim().equals("2")) agrup = agrup2;
		             else if(ss.getCodman().trim().equals("3")) agrup = agrup3;
		                  else if(ss.getCodman().trim().equals("4")) agrup = agrup4;
			  
			    vec_datos.add(nf2.format(agrup.get(0)));
				vec_datos.add(nf2.format(agrup.get(1)));
				vec_datos.add(nf2.format(agrup.get(2)));
				vec_datos.add(nf2.format(agrup.get(3)));
				vec_datos.add(nf2.format(agrup.get(4)));
				vec_datos.add(nf2.format(agrup.get(5)));
				vec_datos.add(nf2.format(agrup.get(6)));
				vec_datos.add(nf2.format(agrup.get(7)));
				vec_datos.add(nf2.format(agrup.get(8)));
				vec_datos.add(nf2.format(agrup.get(9)));
				vec_datos.add(nf2.format(agrup.get(10)));
				vec_datos.add(nf2.format(agrup.get(11)));
				vec_datos.add(nf2.format(agrup.get(12)));
				vec_datos.add("");
		    }    
          	else {
	          	vec_datos.add(nf2.format(total));
				vec_datos.add(nf2.format(ss.getPres01()));
				vec_datos.add(nf2.format(ss.getPres02()));
				vec_datos.add(nf2.format(ss.getPres03()));
				vec_datos.add(nf2.format(ss.getPres04()));
				vec_datos.add(nf2.format(ss.getPres05()));
				vec_datos.add(nf2.format(ss.getPres06()));
				vec_datos.add(nf2.format(ss.getPres07()));
				vec_datos.add(nf2.format(ss.getPres08()));
				vec_datos.add(nf2.format(ss.getPres09()));
				vec_datos.add(nf2.format(ss.getPres10()));
				vec_datos.add(nf2.format(ss.getPres11()));
				vec_datos.add(nf2.format(ss.getPres12()));
				vec_datos.add(ss.getComen1().toString().trim()+ss.getComen2().toString().trim()+ss.getComen3().toString().trim()+ss.getComen4().toString().trim()+ss.getComen5().toString().trim()+ss.getComen6().toString().trim());
	        }
		 	  
			enero += ss.getPres01();
			febrero += ss.getPres02();
			marzo += ss.getPres03();
			abril += ss.getPres04();
			mayo += ss.getPres05();
			junio += ss.getPres06();
			julio += ss.getPres07();
			agosto += ss.getPres08();
			septiembre += ss.getPres09();
			octubre += ss.getPres10();
			noviembre += ss.getPres11();
			diciembre += ss.getPres12();
			totalTotal += total;
			}
		}
		vec_datos.add("");
		vec_datos.add("Total $");
		vec_datos.add(nf2.format(totalTotal));
		vec_datos.add(nf2.format(enero));
		vec_datos.add(nf2.format(febrero));
		vec_datos.add(nf2.format(marzo));
		vec_datos.add(nf2.format(abril));
		vec_datos.add(nf2.format(mayo));
		vec_datos.add(nf2.format(junio));
		vec_datos.add(nf2.format(julio));
		vec_datos.add(nf2.format(agosto));
		vec_datos.add(nf2.format(septiembre));
		vec_datos.add(nf2.format(octubre));
		vec_datos.add(nf2.format(noviembre));
		vec_datos.add(nf2.format(diciembre));
		vec_datos.add(nomActividad);
		return vec_datos;
	}

		public void getlistaPresupItemreqPlan(HttpServletRequest req){
			int a�o = Util.validaParametro(req.getParameter("anno"), 0);
			int codActividad = Util.validaParametro(req.getParameter("codActividad"), 0);
			String nomActividad = Util.validaParametro(req.getParameter("nomActividad"),"");
			int rutUsuario = Integer.parseInt(req.getSession().getAttribute("rutUsuario")+ "");
			String dv = Util.validaParametro(req.getSession().getAttribute("dv")+"","");
			HttpSession sesion = req.getSession();
			
	
			/*agrega requerimiento*/
		    String nomItem = "";
		    String nomMes = "";
		    int itedoc = 41;
			Vector vecTotales = new Vector();
			Vector vec_d = new Vector();
			List<Presw25DTO> presupItemreq = new ArrayList<Presw25DTO>();
			List<Presw25DTO> presupItemreqDet = new ArrayList<Presw25DTO>();
			
			PreswBean preswbean = new PreswBean("MNA", 0,itedoc,a�o,0, rutUsuario);
			if(preswbean != null) {
				preswbean.setDigide(dv);
				preswbean.setRutide(rutUsuario);
				preswbean.setMespar(codActividad);
				presupItemreq = (List<Presw25DTO>) preswbean.consulta_presw25();
			}
			
			Long en = new Long(0);
	    	Long fe = new Long(0);
	    	Long ma = new Long(0);
	    	Long ab = new Long(0);
	    	Long my = new Long(0);
	    	Long jn = new Long(0);
	    	Long jl = new Long(0);
	    	Long ag = new Long(0);
	    	Long se = new Long(0);
	    	Long oc = new Long(0);
	    	Long no = new Long(0);
	    	Long di = new Long(0);
	    	Vector listaReqResumen = new Vector();	
	    	Vector listaReqDetResumen = new Vector();
	         
	    	if(presupItemreq != null && presupItemreq.size() > 0){
	        Vector meses = new Vector();
	        int numMes = 1;
	    //    while (numMes < 13) {
	    	for (Presw25DTO ss : presupItemreq){
	    		Presw25DTO presw25 = ss;
	        	  
	    		if(		presw25.getItedoc() == itedoc && 
	    				presw25.getAnopre() == a�o && 
	    				ss.getTotpre() > 0
	    				/*&& numMes == ss.getMespre()*/){
	    			  nomItem = ss.getItedoc() + " - " + ss.getDesite();
		    	switch (ss.getMespre()){	  			
		    	case 1: 	{ en = en + presw25.getTotpre();
		    	              nomMes = "Enero"; 	    	
		    	              break;
		    	}
		    	case 2: 	{fe = fe + presw25.getTotpre();
		    				nomMes = "Febrero"; 
		    				break;
		    	}
		    	
		    	case 3:    { ma = ma + presw25.getTotpre();
		    				nomMes = "Marzo";
		    				break;
		    	}
		    	
		    	case 4:    {ab = ab + presw25.getTotpre();
		    				nomMes = "Abril"; 
		    				break;
		    	}
		    	
		    	case 5: 	{my = my + presw25.getTotpre();
		    				nomMes = "Mayo"; 
		    				break;
		    	}
		    	
		    	case 6: 	{jn = jn + presw25.getTotpre();
		    				nomMes = "Junio";
		    				break;
		    	}
		    	
		    	case 7: 	{jl = jl + presw25.getTotpre();
		    				nomMes = "Julio"; 
		    				break;
		    	}
		    	
		    	case 8: 	{ag = ag + presw25.getTotpre();
		    				nomMes = "Agosto"; 
		    				break;
		    	}
		    	
		    	case 9: 	{se = se + presw25.getTotpre();
		    				nomMes = "Septiembre"; 
		    				break;
		    	}
		    	
		    	case 10: 	{oc = oc + presw25.getTotpre();
		    				nomMes = "Octubre"; 
		    				break;
		    	}
		    	
		    	case 11: 	{no = no + presw25.getTotpre();
		    				nomMes = "Noviembre";
		    				break;
		    	}
		    	
		    	case 12: 	{di = di + presw25.getTotpre();
		    				nomMes = "Diciembre"; 
		    				break;
		    	}
		    	
		    	}
		    	
		    		
		    	    vec_d = new Vector();
		    	    vec_d.addElement(ss.getMespre());
		    	    vec_d.addElement(nomMes);
		    	    vec_d.addElement(ss.getComen1().trim());
		    	    vec_d.addElement(ss.getComen2().trim());
		    	    vec_d.addElement(ss.getTotpre());
		    	    switch (ss.getMespre()){
		    	    case 1: vec_d.addElement(en);
		    	    break;
		    	    case 2 : vec_d.addElement(fe);
		    	    break;
		    	    case 3 : vec_d.addElement(ma);
		    	    break;
		    	    case 4 : vec_d.addElement(ab);
		    	    break;
		    	    case 5 : vec_d.addElement(my);
		    	    break;
		    	    case 6 : vec_d.addElement(jn);
		    	    break;
		    	    case 7 : vec_d.addElement(jl);
		    	    break;
		    	    case 8 : vec_d.addElement(ag);
		    	    break;
		    	    case 9 : vec_d.addElement(se);
		    	    break;
		    	    case 10 : vec_d.addElement(oc);
		    	    break;
		    	    case 11 : vec_d.addElement(no);
		    	    break;
		    	    case 12 : vec_d.addElement(di);
		    	    break;
		    	    }
		    	    listaReqResumen.addElement(vec_d);
	    	
	    		preswbean = new PreswBean("RQA", 0,itedoc,a�o,ss.getMespre(), rutUsuario);
				if(preswbean != null) {
					preswbean.setSucur(ss.getNumreq());	
					preswbean.setDigide(dv);
					preswbean.setRutide(rutUsuario);
					preswbean.setCodban(codActividad);
			
					presupItemreqDet = new ArrayList<Presw25DTO>();
					presupItemreqDet = (List<Presw25DTO>) preswbean.consulta_presw25();
				}
	    //	}
				
	    //	}
	 
	    	//numMes++;
	    //    }
	    //	}
	    	nomMes = "";
	    	if(presupItemreqDet.size() > 0){
	    	for(Presw25DTO pi: presupItemreqDet){
	    		switch (pi.getMespre()){	  			
		    	case 1: 	nomMes = "Enero"; 	    	
		    	              break;
		    	case 2: 	nomMes = "Febrero"; 
		    				break;	    	
		    	case 3:   	nomMes = "Marzo";
		    				break;	    	
		    	case 4:     nomMes = "Abril"; 
		    				break;	    	
		    	case 5: 	nomMes = "Mayo"; 
		    				break;	    	
		    	case 6: 	nomMes = "Junio";
		    				break;	    	
		    	case 7: 	nomMes = "Julio"; 
		    				break;	    	
		    	case 8: 	nomMes = "Agosto"; 
		    				break;	    	
		    	case 9: 	nomMes = "Septiembre"; 
		    				break;	    	
		    	case 10: 	nomMes = "Octubre"; 
		    				break;	    	
		    	case 11: 	nomMes = "Noviembre";
		    				break;	    	
		    	case 12: 	nomMes = "Diciembre"; 
		    				break;	    	
		    	}
	    		 vec_d = new Vector();
	    		 vec_d.addElement(pi.getCoduni()+" - "+pi.getDesuni()); // 0 UNIDAD
	    		 vec_d.addElement(pi.getItedoc()+" - "+ pi.getDesite()); // 1 - ITEM
	    		 vec_d.addElement(nomMes.concat(" - N� " + pi.getNumreq()+""));// 3 - requerimiento    	    	
	     		 vec_d.addElement(pi.getArea1().trim() + pi.getArea2().trim());// 2 - AREA
	    		 vec_d.addElement(pi.getComen1().trim()+pi.getComen2().trim()+pi.getComen3().trim()+pi.getComen4().trim()+pi.getComen5().trim()+pi.getComen6().trim()); // 4 - DESCRIPCION
	    		 vec_d.addElement(pi.getMotiv1().trim()+pi.getMotiv2().trim()+pi.getMotiv3().trim()+pi.getMotiv4().trim()+pi.getMotiv5().trim()+pi.getMotiv6().trim()); // 5 - MOTIVO
				 vec_d.addElement(pi.getCodman()); // 6 - ITEM
				 vec_d.addElement(pi.getTipcon()); // 7 - DETALLE
	    		 if(pi.getCodman().trim().equals("COTIZACION") && pi.getPres03() > 0){
	    			 String fecha = pi.getPres02()+"";
	    			 vec_d.addElement(pi.getPres01()); // 9 - cotizacion
	        		 vec_d.addElement(pi.getPres03()); // 10 - monto
	        		 vec_d.addElement(fecha.substring(7)+"/"+fecha.substring(4,6)+"/"+fecha.substring(0,4)); // 10 - fecha
	    		 } else {
	    			 vec_d.addElement(pi.getCodigo()); // 8 - UNIDAD
	    	     	 vec_d.addElement(pi.getValuni()); // 9 - COSTO
	        		 vec_d.addElement(pi.getCanman()); // 10 - CANTIDAD       
	    		 }
	             vec_d.addElement(pi.getNumreq()); // 11 - NUMREQ
	    		 vec_d.addElement(pi.getMespre()); // 12 - MES
	    		 if(pi.getCanman() > 0 	|| pi.getPres03() > 0)
	    			 listaReqDetResumen.addElement(vec_d);
	    	}
	    	}
	    	}
	    	}
	       	}
	       	Long totalAnual = en + fe + ma + ab + my + jn + jl + ag + se + oc + no + di;
		    vec_d = new Vector();
		    vec_d.addElement(13);
		    vec_d.addElement("Total Anual");
		    vec_d.addElement("");
		    vec_d.addElement("");
		    vec_d.addElement("");
		    vec_d.addElement(totalAnual);
		    if(totalAnual > 0)
		 	   		    listaReqResumen.addElement(vec_d);
			/*fin requerimiento*/
		 //   System.out.println(listaReqDetResumen);
			sesion.setAttribute("listaReqResumen", listaReqResumen);
			sesion.setAttribute("listaReqDetResumen", listaReqDetResumen);
						
	}

		
	/*agrega en MAN*/
/*	Presw25DTO presw25 = new Presw25DTO();
	presw25.setTippro("MAN");
	presw25.setCoduni(unidad);
	presw25.setItedoc(item);
	presw25.setAnopre(anno);
	presw25.setMespre(mes);
	presw25.setNumreq(numreq);
	presw25.setValpr1(valpr);
	presw25.setDesite(desite);
	presw25.setComen1(area1);
	presw25.setComen2(area2);
	presw25.setDesuni(desuni);
	presw25.setTotpre(total);
	if(total == 0)
	presw25.setIndmod("D");
	if(presupItemreq != null && presupItemreq.size() > 0){
	i = -1;
	boolean agrega = false;
	for (Presw25DTO ss : presupItemreq){
	i++;
	if(ss.getCoduni() == unidad && ss.getItedoc() == item && ss.getMespre() == mes && ss.getNumreq() == numreq){
	presupItemreq.set(i, presw25);
	agrega = true;
	}        		      			
	}
	if(!agrega)
	presupItemreq.add(presw25);
	} else presupItemreq.add(presw25);
	
	Long totalItemReq = new Long(0);
	Long ene = new Long(0);
	Long feb = new Long(0);
	Long mar = new Long(0);
	Long abr = new Long(0);
	Long may = new Long(0);
	Long jun = new Long(0);
	Long jul = new Long(0);
	Long ago = new Long(0);
	Long sep = new Long(0);
	Long oct = new Long(0);
	Long nov = new Long(0);
	Long dic = new Long(0);
	
	if(listarResultadoPresup.size() > 0){
	for (Presw25DTO ss : presupItemreq){ // agregar todo lo que est� en mantencion al presup
	if(ss.getItedoc() == item && ss.getCoduni() == unidad ) {
	switch (ss.getMespre()){
	case 1: ene = ene + ss.getTotpre();
	break;
	case 2: feb = feb + ss.getTotpre();
	break;
	case 3: mar = mar + ss.getTotpre();
	break;
	case 4: abr = abr + ss.getTotpre();
	break;
	case 5: may = may + ss.getTotpre();
	break;
	case 6: jun = jun + ss.getTotpre();
	break;
	case 7: jul = jul + ss.getTotpre();
	break;
	case 8: ago = ago + ss.getTotpre();
	break;
	case 9: sep = sep + ss.getTotpre();
	break;
	case 10: oct = oct + ss.getTotpre();
	break;
	case 11: nov = nov + ss.getTotpre();
	break;
	case 12: dic = dic + ss.getTotpre();
	break;
	}
	totalItemReq = totalItemReq + ss.getTotpre();
	}
	}
	
	i =-1;
	for (Presw25DTO ss : listarResultadoPresup){
	i++;
	if(ss.getItedoc() == item && ss.getCoduni() == unidad){
	ss.setItedoc(item);
	ss.setPres01(ene);
	ss.setPres02(feb);
	ss.setPres03(mar);
	ss.setPres04(abr);
	ss.setPres05(may);
	ss.setPres06(jun);
	ss.setPres07(jul);
	ss.setPres08(ago);
	ss.setPres09(sep);
	ss.setPres10(oct);
	ss.setPres11(nov);
	ss.setPres12(dic);
	ss.setTotpre(Long.parseLong(String.valueOf(ss.getPres01() + ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() + ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() + ss.getPres10() + ss.getPres11() + ss.getPres12())));
	}
	listaResultado.add(ss);
	//System.out.println("result: " + ss.getTotpre());
	}
	
	}*/
	/*	System.out.println("tama�o presupItemreq: "+presupItemreq.size());
	for (Presw25DTO ss : presupItemreq){
	
	System.out.println("ss "+ss.getMespre()+"cant "  + ss.getPres01()+"req: "+ ss.getNumreq());
	System.out.println("ss "+ss.getMespre()+"cant " + ss.getPres02()+"req: "+ ss.getNumreq());
	System.out.println("ss "+ss.getMespre()+"cant " + ss.getPres03()+"req: "+ ss.getNumreq());
	
	}
	System.out.println("presupItemreq desp **: "+presupItemreq.size());*/
	/*if(presupItemreq != null )
	sesion.setAttribute("presupItemreq",presupItemreq);
	sesion.setAttribute("resultadoDetalleItemReq",listaRQP);
	
	sesion.setAttribute("resultadoPresup", listaResultado);	*/
	//req.setAttribute("totalMes", totalTotal);


		public List<Presw25DTO> getListaControlPlanes(String tipo, int rutUsuario, String dv){
		    List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		    Presw25DTO presw25DTO = new Presw25DTO();
			PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
			preswbean.setDigide(dv);
			
			Collection<Presw25DTO> listaPresw25 = (Collection<Presw25DTO>) preswbean.consulta_presw25();
			for (Presw25DTO ss : listaPresw25){
		     	lista.add(ss);
			}
				
			return lista;
			}
		public List<Presw25DTO> getControlPlanes(String tipo, int rutUsuario, String dv, int anno){
		    List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		    Presw25DTO presw25DTO = new Presw25DTO();
			PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
			preswbean.setDigide(dv);
			preswbean.setAnopar(anno);
			
			Collection<Presw25DTO> listaPresw25 = (Collection<Presw25DTO>) preswbean.consulta_presw25();
			for (Presw25DTO ss : listaPresw25){
		     	lista.add(ss);
			}
				
			return lista;
			}
		public List<Presw25DTO> getListaActividadesControlPlanes(String tipo, int rutUsuario, String dv, int codUni){
		    List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		    Presw25DTO presw25DTO = new Presw25DTO();
			PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
			preswbean.setDigide(dv);
			preswbean.setCajero(codUni+"");
			
			Collection<Presw25DTO> listaPresw25 = (Collection<Presw25DTO>) preswbean.consulta_presw25();
			for (Presw25DTO ss : listaPresw25){
		     	lista.add(ss);
			}
				
			return lista;
			}
		public List<Presw25DTO> getListaPlanesDesarrollo(String tipo, int rutUsuario, String dv, String cajero, int codUni){
		    List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		    Presw25DTO presw25DTO = new Presw25DTO();
			PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
			preswbean.setDigide(dv);
			preswbean.setCajero(cajero);
			preswbean.setCoduni(codUni);
			
			Collection<Presw25DTO> listaPresw25 = (Collection<Presw25DTO>) preswbean.consulta_presw25();
			for (Presw25DTO ss : listaPresw25){
		     	lista.add(ss);
			}
				
			return lista;
			}
		public List<Presw25DTO> getActividadesPlanesDesarrollo(String tipo, int rutUsuario, String dv, String cajero, int anno){
		    List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		    Presw25DTO presw25DTO = new Presw25DTO();
			PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
			preswbean.setDigide(dv);
			preswbean.setCajero(cajero);
			preswbean.setAnopar(anno);
			
			Collection<Presw25DTO> listaPresw25 = (Collection<Presw25DTO>) preswbean.consulta_presw25();
			for (Presw25DTO ss : listaPresw25){
		     	lista.add(ss);
			}
				
			return lista;
			}
		public List<Cocow36DTO> getListaValorIndicadores(String tipo, int rutUsuario, String dv, String indicador, int codUni) {
		    List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		  
			Presw19DTO presw19DTO = new Presw19DTO();
			presw19DTO.setTippro(tipo);
			presw19DTO.setRutide(new BigDecimal(rutUsuario));
			presw19DTO.setDigide(dv);
			presw19DTO.setCoduni(new BigDecimal(codUni));
			
		
			Presw25DTO presw25DTO = new Presw25DTO();
			presw25DTO.setTippro(tipo);
			presw25DTO.setComen1(indicador);
			presw25DTO.setCoduni(codUni);
				
			List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
			List<Cocow36DTO>  listaValorIndicadores = new ArrayList<Cocow36DTO>();
			CocowBean cocowBean = new CocowBean();
			
			listCocow36DTO =  cocowBean.buscar_cocow36(presw19DTO, presw25DTO);  
			for(Cocow36DTO ss: listCocow36DTO){
				 if(ss.getNomcam().trim().equals("DETIND")){
			   	    	Cocow36DTO cocow36DTO = new Cocow36DTO();
			   	    	cocow36DTO.setNomcam(ss.getNomcam());
			   	    	cocow36DTO.setValalf(ss.getValalf());// codigo, 
			   	    	cocow36DTO.setValnu2(ss.getValnu2());// fecha
			   	    	cocow36DTO.setResval(ss.getResval());// por
			   	    	listaValorIndicadores.add(cocow36DTO);
				     }
				 if(ss.getNomcam().trim().equals("DETTEX")){
			   	    	Cocow36DTO cocow36DTO = new Cocow36DTO();
			   	 	    cocow36DTO.setNomcam(ss.getNomcam());
			   	    	cocow36DTO.setValalf(ss.getValalf());// INGRESO POR 
			   	    	cocow36DTO.setValnu2(ss.getValnu2());// fecha CREACION
			   	    	cocow36DTO.setResval(ss.getResval());// DESCRIPCION
			   	    	listaValorIndicadores.add(cocow36DTO);
				     }
				 if(ss.getNomcam().trim().equals("DETMET")){
			   	    	Cocow36DTO cocow36DTO = new Cocow36DTO();
			   	 	    cocow36DTO.setNomcam(ss.getNomcam());
			   	    	cocow36DTO.setValalf(ss.getValalf());// INGRESO POR 
			   	    	cocow36DTO.setValnu2(ss.getValnu2());// fecha CREACION
			   	    	cocow36DTO.setResval(ss.getResval());// DESCRIPCION
			   	    	listaValorIndicadores.add(cocow36DTO);
				     }
			}
				
			return listaValorIndicadores;
			}
		
		public List<Cocow36DTO> getListaIndicadores(int rutUsuario, String dv, int codCuenta){
			String nomTipo = "PIA";
			Presw19DTO preswbean19DTO = new Presw19DTO();
			preswbean19DTO.setTippro(nomTipo);
			preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
			preswbean19DTO.setDigide(dv);
			preswbean19DTO.setCoduni(new BigDecimal(codCuenta));
			List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
			List<Cocow36DTO> listaIndicadores = new ArrayList<Cocow36DTO>();
			CocowBean cocowBean = new CocowBean();
			listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
			 if(listCocow36DTO != null && listCocow36DTO.size() >0 ){
			    for(Cocow36DTO ss: listCocow36DTO){	 		
			   	    if(ss.getNomcam().trim().equals("DETIND")){
			   	    	Cocow36DTO cocow36DTO = new Cocow36DTO();
			   	    	cocow36DTO.setValalf(ss.getValalf());// codigo, 
			   	    	cocow36DTO.setValnu2(ss.getValnu2());// fecha
			   	    	cocow36DTO.setResval(ss.getResval());// por
				        listaIndicadores.add(cocow36DTO);
				     }
				 }
			 }
			 return listaIndicadores;
		}
		public List<Presw18DTO> getListaEjecucionPorItem(int codCuenta, int anno){
		    String nomTipo = "PAI";
		    PreswBean preswbean = null;
			Collection<Presw19DTO> lista = null;
			preswbean = new PreswBean(nomTipo,0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
			preswbean.setCoduni(codCuenta);
			preswbean.setAnopar(anno);
			preswbean.setMespar(12);
			List<Presw18DTO> listaPRESW18 = (List<Presw18DTO>) preswbean.consulta_presw18();
			return listaPRESW18;
		}
		public List<Presw18DTO> getListaEjecPorItemDetalle( String nomTipo, int codCuenta, int anno, int rutUsuario, String dv, int itedoc){
			PreswBean preswbean = new PreswBean(nomTipo, 0, 0, 0, 0, rutUsuario);
			preswbean.setDigide(dv);
			preswbean.setCoduni(codCuenta);
			preswbean.setAnopar(anno);
			preswbean.setItedoc(itedoc);
			List<Presw18DTO> listaPRESW18 = (List<Presw18DTO>) preswbean.consulta_presw18();
			return listaPRESW18;
		}
		public void agregaPlanDesarrolloExcel(HttpServletRequest req, Vector<String> lista, int annoControl){
			HttpSession sesion = req.getSession();
			sesion.setAttribute("planDesarrolloExcel",lista);
			if(annoControl > 0)
				sesion.setAttribute("annoControl", annoControl+"");
		}
		public boolean getVerificaMemo(String tipo, String comen1){
			  PreswBean preswbean = null;
				Collection<Presw18DTO> lista = null;
				preswbean = new PreswBean(tipo,0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
				Presw25DTO presw25 = new Presw25DTO();
				presw25.setTippro(tipo);
				presw25.setComen1(comen1);
				lista = (Collection<Presw18DTO>) preswbean.ingreso_doc_presw25(presw25);
                boolean existe = false;       
	   		for (Presw18DTO ss : lista){
				if(ss.getItedoc() == 1)
					existe = true;
			}
				
	return existe;
	}
	public boolean getVerificaCuenta(String tipo, int cuenta){
			  PreswBean preswbean = null;
				Collection<Presw18DTO> lista = null;
				preswbean = new PreswBean(tipo,0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
				preswbean.setCoduni(cuenta);
				lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
              boolean existe = false;       
	   		for (Presw18DTO ss : lista){
				if(!ss.getDesuni().trim().equals(""))
					existe = true;
			}
				
	return existe;
	}
public List<Presw18DTO> getVerificaRutCuenta(String tipo, int rut, String dv){
		  PreswBean preswbean = null;
			Collection<Presw18DTO> lista = null;
			preswbean = new PreswBean(tipo,0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
			preswbean.setRutide(rut);
			preswbean.setDigide(dv);
			lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			List<Presw18DTO> listaRut = new ArrayList<Presw18DTO>();
            for (Presw18DTO ss : lista){
            	listaRut.add(ss);
		}
			
return listaRut;
}
public List<Presw18DTO> getVerificaAsignacionCuenta(String tipo, int rut, String dv, int codUni, String tipoIngreso){
	  PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		preswbean = new PreswBean(tipo,0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
		preswbean.setRutide(rut);
		preswbean.setDigide(dv);
		preswbean.setCoduni(codUni);
		preswbean.setTipcue(tipoIngreso);
		lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		List<Presw18DTO> listaRut = new ArrayList<Presw18DTO>();
      for (Presw18DTO ss : lista){
      	listaRut.add(ss);
	}
		
return listaRut;
}
public void getAgregaListaSolicitaCuenta( HttpServletRequest req,int rutnum, String dvrut, String nomFuncionario, String tipoIngreso, String nomTipoIngreso){
	 HttpSession sesion = req.getSession();
   	 List<Cocow36DTO>  listaSolicitudes = (List<Cocow36DTO> )sesion.getAttribute("listaSolicitudesCuenta");
   
	 if(listaSolicitudes != null && listaSolicitudes.size()> 0){
		 sesion.removeAttribute("listaSolicitudesCuenta");
	     sesion.setAttribute("listaSolicitudesCuenta", null);
	 	
	 }
	 if(listaSolicitudes == null)
			listaSolicitudes = new ArrayList<Cocow36DTO>();
	 			Cocow36DTO cocow36DTO = new Cocow36DTO();
	 			cocow36DTO.setValnu1(rutnum);
	 			cocow36DTO.setValalf(dvrut);
	 			cocow36DTO.setResval(nomFuncionario);
	 			cocow36DTO.setAccion(tipoIngreso);
		 		listaSolicitudes.add(cocow36DTO);
     
	 
	 sesion.setAttribute("listaSolicitudesCuenta", listaSolicitudes);
		
}
public void getAgregaListaSolicitaAsignacion( HttpServletRequest req,int rutnum, String dvrut, String nomFuncionario, String tipoIngreso, String nomTipoIngreso, int cuenta, String nomCuenta){
	 HttpSession sesion = req.getSession();
  	 List<Cocow36DTO>  listaSolicitudes = (List<Cocow36DTO> )sesion.getAttribute("listaSolicitudesAsignacion");
  
	 if(listaSolicitudes != null && listaSolicitudes.size()> 0){
		 sesion.removeAttribute("listaSolicitudesAsignacion");
	     sesion.setAttribute("listaSolicitudesAsignacion", null);
	 	
	 }
	 if(listaSolicitudes == null)
			listaSolicitudes = new ArrayList<Cocow36DTO>();
	 			Cocow36DTO cocow36DTO = new Cocow36DTO();
	 			cocow36DTO.setValnu1(rutnum);
	 			cocow36DTO.setValalf(dvrut);
	 			cocow36DTO.setResval(nomFuncionario);
	 			cocow36DTO.setAccion(tipoIngreso);
	 			cocow36DTO.setValnu2(cuenta);
		 		listaSolicitudes.add(cocow36DTO);
    
	 
	 sesion.setAttribute("listaSolicitudesAsignacion", listaSolicitudes);
		
}
public void getEliminaListaSolicitaCuenta( HttpServletRequest req){
	 HttpSession sesion = req.getSession();
    int rutide = Util.validaParametro(req.getParameter("rutide"), 0);
    
	 List<Cocow36DTO>  listaSolicitudes = (List<Cocow36DTO> )sesion.getAttribute("listaSolicitudesCuenta");
	 List<Cocow36DTO> listaCuenta = new ArrayList<Cocow36DTO>();
	 if(listaSolicitudes != null && listaSolicitudes.size()> 0)
		 sesion.removeAttribute("listaSolicitudesCuenta");
	 if(listaSolicitudes != null && listaSolicitudes.size() > 0){
		 for (Cocow36DTO ss: listaSolicitudes){
			 if(ss.getValnu1() == rutide)
				 continue;
			 else
			 {
				 Cocow36DTO cocow36DTO = new Cocow36DTO();
				 cocow36DTO.setValnu1(ss.getValnu1());
				 cocow36DTO.setValalf(ss.getValalf());
				 cocow36DTO.setAccion(ss.getAccion());
				 cocow36DTO.setResval(ss.getResval());
				 listaCuenta.add(cocow36DTO);
			 }
		 }
		 
	
	 }
	sesion.setAttribute("listaSolicitudesCuenta", listaCuenta);
		
}
public void getEliminaListaSolicitaAsignacion( HttpServletRequest req){
	 HttpSession sesion = req.getSession();
   int rutide = Util.validaParametro(req.getParameter("rutide"), 0);
   int coduni = Util.validaParametro(req.getParameter("coduni"), 0);
   
	 List<Cocow36DTO>  listaSolicitudes = (List<Cocow36DTO> )sesion.getAttribute("listaSolicitudesAsignacion");
	 List<Cocow36DTO> listaCuenta = new ArrayList<Cocow36DTO>();
	 if(listaSolicitudes != null && listaSolicitudes.size()> 0)
		 sesion.removeAttribute("listaSolicitudesAsignacion");
	 if(listaSolicitudes != null && listaSolicitudes.size() > 0){
		 for (Cocow36DTO ss: listaSolicitudes){
			 if((ss.getValnu1() == rutide) && (ss.getValnu2() == coduni))
				 continue;
			 else
			 {
				 Cocow36DTO cocow36DTO = new Cocow36DTO();
				 cocow36DTO.setValnu1(ss.getValnu1());
				 cocow36DTO.setValnu2(ss.getValnu2());
				 cocow36DTO.setValalf(ss.getValalf());
				 cocow36DTO.setAccion(ss.getAccion());
				 cocow36DTO.setResval(ss.getResval());
				 listaCuenta.add(cocow36DTO);
			 }
		 }
	 }
	sesion.setAttribute("listaSolicitudesAsignacion", listaCuenta);
		
}

public List<Cocow36DTO> getCreaCocow36( HttpServletRequest req, int rutUsuario, String dv, String tipo){
		String numMemo = Util.validaParametro(req.getParameter("numMemo"),"");
		int numCuenta = Util.validaParametro(req.getParameter("numCuenta"), 0);
		String nombreCuenta = Util.validaParametro(req.getParameter("nombreCuenta"),"");
		String motiv1 = Util.validaParametro(req.getParameter("motivoApertura1"), "");
		String motiv2 = Util.validaParametro(req.getParameter("motivoApertura2"), "");
		String motiv3 = Util.validaParametro(req.getParameter("motivoApertura3"), "");
		String motiv4 = Util.validaParametro(req.getParameter("motivoApertura4"), "");
		int rutnum = Util.validaParametro(req.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(req.getParameter("dvrut"),"");
		int rutnumAut = Util.validaParametro(req.getParameter("rutnumAut"), 0);
		String dvrutAut = Util.validaParametro(req.getParameter("dvrutAut"),"");
		List<Cocow36DTO>  listaSolicitudes = (List<Cocow36DTO>) req.getSession().getAttribute("listaSolicitudesCuenta");
			
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
       // String tipo = "GSP";
	    String nomcam = "";
	    long valnu1 = 0;
	    long valnu2 = 0;
	    String valalf = "";

	    /* prepara archivo para grabar, llenar listCocow36DTO*/
	    nomcam = "MEMSOL";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = numMemo;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "RUTING";
	    valnu1 = rutUsuario;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "DIGING";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = dv;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "RUTAUT";
	    valnu1 = rutnumAut;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "DIGAUT";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = dvrutAut;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "CODUNI";
	    valnu1 = numCuenta;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "DESUNI";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = nombreCuenta;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	
	    
	    nomcam = "RUTRES";
	    valnu1 = rutnum;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "DIGRES";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = dvrut;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    
	    
	    nomcam = "MOTAP1";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = motiv1;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	
	    nomcam = "MOTAP2";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = motiv2;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "MOTAP3";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = motiv3;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "MOTAP4";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = motiv4;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
    
   if(listaSolicitudes != null && listaSolicitudes.size() > 0)
    for (Cocow36DTO ss : listaSolicitudes){
    	nomcam = "DETACC";
 	    valnu1 = ss.getValnu1();
 	    valnu2 = 0;
 	    valalf = ss.getValalf();

 	         Cocow36DTO cocow36DTO = new Cocow36DTO();
	 		 cocow36DTO.setTiping(tipo);
	 		 cocow36DTO.setNomcam(nomcam);
	 		 cocow36DTO.setValnu1(valnu1); // rut acceso
	 		 cocow36DTO.setValalf(valalf); // digito verificador
	 		 cocow36DTO.setResval(ss.getAccion());// accion I/A
	 		 listCocow36DTO.add(cocow36DTO);
	     }
	   
   
	return listCocow36DTO;
	}

	public int getRegistraCuenta(HttpServletRequest req, int rutide, String digide, String tipo, int numDoc){
		int numCuenta = 0;
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		listCocow36DTO = this.getCreaCocow36(req, rutide, digide, tipo);
		Ingreso_Documento ingresoDocumento = new Ingreso_Documento(tipo,rutide,digide);
		ingresoDocumento.setNumdoc(numDoc);
			
		numCuenta = ingresoDocumento.Ingresar_Documento(listCocow36DTO);	
	
		return numCuenta;
	}	

public List<Cocow36DTO> getAgregaLista(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf){
	 Cocow36DTO cocow36DTO = new Cocow36DTO();
	 cocow36DTO.setTiping(tiping);
	 cocow36DTO.setNomcam(nomcam);
	 cocow36DTO.setValnu1(valnu1);
	 cocow36DTO.setValnu2(valnu2);
	 cocow36DTO.setValalf(valalf);
	 lista.add(cocow36DTO);
	 return lista;
	 
}
/*solicitudes de cuenta*/


public List<Presw18DTO> getListaPresw18(String tipo, int rutUsuario, String dv){
    List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
    Presw18DTO presw18DTO = new Presw18DTO();
	PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
	preswbean.setDigide(dv);

	Collection<Presw18DTO> listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
	for (Presw18DTO ss : listaPresw18){
		presw18DTO = new Presw18DTO();
		presw18DTO.setNumdoc(ss.getNumdoc());
		presw18DTO.setNomtip(ss.getNomtip());
		presw18DTO.setCoduni(ss.getCoduni());
		presw18DTO.setDesuni(ss.getDesuni());
		presw18DTO.setDesite(ss.getDesite());
		presw18DTO.setNompro(ss.getNompro());
		presw18DTO.setFecdoc(ss.getFecdoc());
		presw18DTO.setRutide(ss.getRutide());
		presw18DTO.setIndpro(ss.getIndpro());
		lista.add(presw18DTO);
	}
		
	return lista;
	}
public void agregaSesion(HttpServletRequest req, List<Presw18DTO> lista, String nomLista){
//	public void agregaUnidad(HttpServletRequest req, Collection<Presw18> lista){
		HttpSession sesion = req.getSession();
	
		sesion.setAttribute(nomLista,lista);
		
	}
public boolean saveRecepciona(int numdoc, int rutUsuario, String digide, String tipo, String accion){
	PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
	preswbean.setDigide(digide);
	preswbean.setNumdoc(numdoc);
	if(!accion.trim().equals(""))
		preswbean.setTipcue(accion);

	
return preswbean.ingreso_presw19();
}

public boolean saveRechazaCuenta(int numdoc, int rutUsuario, String digide, String tipo, String glosa){
	PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
	preswbean.setDigide(digide);
	preswbean.setNumdoc(numdoc);
	preswbean.ingreso_presw19();
	
	List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
	Presw25DTO presw25DTO = new Presw25DTO();
	presw25DTO.setPres01(numdoc);
	presw25DTO.setComen1(glosa);
	presw25DTO.setRutide(rutUsuario);
	presw25DTO.setDigide(digide);
	
	lista.add(presw25DTO);
return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
}


public List<TipoUnidad> getTipoUnidad(){
	  TipoUnidad tipoUnidad = new TipoUnidad();
	    List<TipoUnidad> listTipoUnidad = new ArrayList<TipoUnidad>();
	    tipoUnidad = new TipoUnidad();
	    tipoUnidad.setCodigo("O");
	    tipoUnidad.setNombre("Operaci�n");
	    listTipoUnidad.add(tipoUnidad);
	    
	    tipoUnidad = new TipoUnidad();
	    tipoUnidad.setCodigo("R");
	    tipoUnidad.setNombre("Remuneraci�n");
	    listTipoUnidad.add(tipoUnidad);
	    
	    tipoUnidad = new TipoUnidad();
	    tipoUnidad.setCodigo("E");
	    tipoUnidad.setNombre("Personal");
	    listTipoUnidad.add(tipoUnidad);
	    
	    tipoUnidad = new TipoUnidad();
	    tipoUnidad.setCodigo("I");
	    tipoUnidad.setNombre("Inversi�n");
	    listTipoUnidad.add(tipoUnidad);
	    
	    tipoUnidad = new TipoUnidad();
	    tipoUnidad.setCodigo("P");
	    tipoUnidad.setNombre("Postgrado e Investigaci�n");
	    listTipoUnidad.add(tipoUnidad);
	    
	    tipoUnidad = new TipoUnidad();
	    tipoUnidad.setCodigo("S");
	    tipoUnidad.setNombre("Plan Estrat�gico");
	    listTipoUnidad.add(tipoUnidad);
	    
	 	    
	    return listTipoUnidad;

}

public List<TipoCuenta> getTipoCuenta(){
	TipoCuenta tipoCuenta = new TipoCuenta();
	List<TipoCuenta> listTipoCuenta = new ArrayList<TipoCuenta>();
	
	tipoCuenta = new TipoCuenta();
	tipoCuenta.setCodigo("P");
	tipoCuenta.setNombre("Presupuesto");
	listTipoCuenta.add(tipoCuenta);

	tipoCuenta = new TipoCuenta();
	tipoCuenta.setCodigo("I");
	tipoCuenta.setNombre("Ingresos propios");
	listTipoCuenta.add(tipoCuenta);

	tipoCuenta = new TipoCuenta();
	tipoCuenta.setCodigo("Y");
	tipoCuenta.setNombre("Proyectos");
	listTipoCuenta.add(tipoCuenta);

	tipoCuenta = new TipoCuenta();
	tipoCuenta.setCodigo("T");
	tipoCuenta.setNombre("Traspasos");
	listTipoCuenta.add(tipoCuenta);

	tipoCuenta = new TipoCuenta();
	tipoCuenta.setCodigo("V");
	tipoCuenta.setNombre("Valores de Cobro");
	listTipoCuenta.add(tipoCuenta);

	
	return listTipoCuenta;
}

public List<TipoPresupuesto> getTipoPresupuesto(){
	TipoPresupuesto tipoPresupuesto = new TipoPresupuesto();
	List<TipoPresupuesto> listTipoPresupuesto = new ArrayList<TipoPresupuesto>();
	
	tipoPresupuesto = new TipoPresupuesto();
	tipoPresupuesto.setCodigo("C");
	tipoPresupuesto.setNombre("Continuidad Operacional");
	listTipoPresupuesto.add(tipoPresupuesto);
	
	tipoPresupuesto = new TipoPresupuesto();
	tipoPresupuesto.setCodigo("P");
	tipoPresupuesto.setNombre("Programa Anual ");
	listTipoPresupuesto.add(tipoPresupuesto);

	tipoPresupuesto = new TipoPresupuesto();
	tipoPresupuesto.setCodigo("D");
	tipoPresupuesto.setNombre("Plan de Desarrollo");
	listTipoPresupuesto.add(tipoPresupuesto);

	tipoPresupuesto = new TipoPresupuesto();
	tipoPresupuesto.setCodigo("R");
	tipoPresupuesto.setNombre("Remuneraci�n");
	listTipoPresupuesto.add(tipoPresupuesto);

	
	return listTipoPresupuesto;
}
public List<Cocow36DTO> getCreaCocow36Asignacion( HttpServletRequest req, int rutUsuario, String dv, String tipo){
	String numMemo = Util.validaParametro(req.getParameter("numMemo"),"");
	String comentario = Util.validaParametro(req.getParameter("comentario"),"");
	List<Cocow36DTO>  listaSolicitudes = (List<Cocow36DTO>) req.getSession().getAttribute("listaSolicitudesAsignacion");
		
	List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();

    String nomcam = "";
    long valnu1 = 0;
    long valnu2 = 0;
    String valalf = "";
		
		/* prepara archivo para grabar, llenar listCocow36DTO*/
		nomcam = "MEMSOL";
		valnu1 = 0;
		valnu2 = 0;
		valalf = numMemo;
		listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
		
		nomcam = "RUTING";
		valnu1 = rutUsuario;
		valnu2 = 0;
		valalf = "";
		listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
		
		nomcam = "DIGING";
		valnu1 = 0;
		valnu2 = 0;
		valalf = dv;
		listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
		
		nomcam = "COMENT";
		valnu1 = 0;
		valnu2 = 0;
		valalf = comentario;
		listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
		
		for (Cocow36DTO ss : listaSolicitudes){
			nomcam = "DETACC";
			    valnu1 = ss.getValnu1();
			    valnu2 = ss.getValnu2();
			    valalf = ss.getValalf();
		
			     Cocow36DTO cocow36DTO = new Cocow36DTO();
		 		 cocow36DTO.setTiping(tipo);
		 		 cocow36DTO.setNomcam(nomcam);
		 		 cocow36DTO.setValnu1(valnu1); // rut acceso
		 		 cocow36DTO.setValnu2(valnu2);//cuenta
		 		 cocow36DTO.setValalf(valalf); // digito verificador
		 		 cocow36DTO.setResval(ss.getAccion());// accion I/A
		 		 listCocow36DTO.add(cocow36DTO);
		     }
		   

return listCocow36DTO;
}
public List<Cocow36DTO> getCreaCocow36RechazaAsignacion( HttpServletRequest req, int rutUsuario, String dv, String tipo){
	String numDoc = Util.validaParametro(req.getParameter("numDoc"),"");
	String motivoRechazo = Util.validaParametro(req.getParameter("motivoRechazo"),"");
			
	List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();

    String nomcam = "";
    long valnu1 = 0;
    long valnu2 = 0;
    String valalf = "";
		
		/* prepara archivo para grabar, llenar listCocow36DTO*/
		nomcam = "COMENT";
		valnu1 = 0;
		valnu2 = 0;
		valalf = motivoRechazo;
		listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );	
		   

return listCocow36DTO;
}
public List<Cocow36DTO> getCreaCocow36RechazaConfirmacion( HttpServletRequest req, int rutUsuario, String dv, String tipo){
	String numDoc = Util.validaParametro(req.getParameter("numDoc"),"");
	String motivoRechazo1 = Util.validaParametro(req.getParameter("motivoRechazo1"),"");
	String motivoRechazo2 = Util.validaParametro(req.getParameter("motivoRechazo2"),"");
	String motivoRechazo3 = Util.validaParametro(req.getParameter("motivoRechazo3"),"");
	String motivoRechazo4 = Util.validaParametro(req.getParameter("motivoRechazo4"),"");
	List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();

    String nomcam = "";
    long valnu1 = 0;
    long valnu2 = 0;
    String valalf = "";
		if(!motivoRechazo1.trim().equals("")){
				/* prepara archivo para grabar, llenar listCocow36DTO*/
				nomcam = "COMEN1";
				valnu1 = 0;
				valnu2 = 0;
				valalf = motivoRechazo1;
				listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );	
				
				nomcam = "COMEN2";
				valnu1 = 0;
				valnu2 = 0;
				valalf = motivoRechazo2;
				listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );	
				
				nomcam = "COMEN3";
				valnu1 = 0;
				valnu2 = 0;
				valalf = motivoRechazo3;
				listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );	
				
				nomcam = "COMEN4";
				valnu1 = 0;
				valnu2 = 0;
				valalf = motivoRechazo4;
				listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );	
		}

return listCocow36DTO;
}
public int getRegistraAsignacion(HttpServletRequest req, int rutide, String digide, String tipo){
	int numCuenta = 0;
	List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
	listCocow36DTO = this.getCreaCocow36Asignacion(req, rutide, digide, tipo);
	Ingreso_Documento ingresoDocumento = new Ingreso_Documento(tipo,rutide,digide);
		
	numCuenta = ingresoDocumento.Ingresar_Documento(listCocow36DTO);
	

	return numCuenta;
}

public int getApruebaRehazaAsignacion(HttpServletRequest req, int rutide, String digide, String tipo, int numDoc, String tipcue){
	int numCuenta = 0;
	List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
	listCocow36DTO = this.getCreaCocow36RechazaAsignacion(req, rutide, digide, tipo);
	Ingreso_Documento ingresoDocumento = new Ingreso_Documento(tipo,rutide,digide);
	ingresoDocumento.setNumdoc(numDoc);
	ingresoDocumento.setTipcue(tipcue);
	numCuenta = ingresoDocumento.Ingresar_Documento(listCocow36DTO);
	

	return numCuenta;
}
public int getApruebaRehaza(HttpServletRequest req, int rutide, String digide, String tipo, int numDoc, String tipcue){
	int numCuenta = 0;
	List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
	listCocow36DTO = this.getCreaCocow36RechazaAsignacion(req, rutide, digide, tipo);
	Ingreso_Documento ingresoDocumento = new Ingreso_Documento(tipo,rutide,digide);
	ingresoDocumento.setNumdoc(numDoc);
	ingresoDocumento.setTipcue(tipcue);
	numCuenta = ingresoDocumento.Ingresar_Documento(listCocow36DTO);
	

	return numCuenta;
}
public int getApruebaRehazaDIR(HttpServletRequest req, int rutide, String digide, String tipo, int numDoc){
	int numCuenta = 0;
	List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
	listCocow36DTO = this.getCreaCocow36RechazaConfirmacion(req, rutide, digide, tipo);
	Ingreso_Documento ingresoDocumento = new Ingreso_Documento(tipo,rutide,digide);
	ingresoDocumento.setNumdoc(numDoc);
	numCuenta = ingresoDocumento.Ingresar_Documento(listCocow36DTO);
	

	return numCuenta;
}
/*historial DIPRES*/
public List<Presw18DTO> getListaHistorial(String tipo, int rutUsuario, String dv, int codUni,int numdoc, int numcom){
    List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
    Presw18DTO presw18DTO = new Presw18DTO();
	PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
	preswbean.setDigide(dv);
	preswbean.setCoduni(codUni);
	preswbean.setNumdoc(numdoc);
	preswbean.setNumcom(numcom);

	Collection<Presw18DTO> listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
	for (Presw18DTO ss : listaPresw18){
		presw18DTO = new Presw18DTO();
		presw18DTO.setNumdoc(ss.getNumdoc()); // numero solicitud
		presw18DTO.setNomtip(ss.getNomtip()); // tipo solicitud
		presw18DTO.setFecdoc(ss.getFecdoc()); // fecha solicitud
		presw18DTO.setCoduni(ss.getCoduni()); // cod unidad
		presw18DTO.setDesuni(ss.getDesuni()); // nom unidad
		presw18DTO.setRutide(ss.getRutide()); // rut solicitante
		presw18DTO.setIndpro(ss.getIndpro()); // digito solicitante
		presw18DTO.setDesite(ss.getDesite()); // nombre solicitante
		presw18DTO.setNompro(ss.getNompro()); // estado solicitud
		lista.add(presw18DTO);
	}
		
	return lista;
	}
public int getRegistraAprobacionDipres(HttpServletRequest req, int rutide, String digide, String tipo){
	int numCuenta = 0;
	int numDoc = Util.validaParametro(req.getParameter("numDoc"), 0);

	List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
	listCocow36DTO = this.getCreaCocow36AprobacionDipres(req, rutide, digide, tipo);
	Ingreso_Documento ingresoDocumento = new Ingreso_Documento(tipo,rutide,digide);
	ingresoDocumento.setNumdoc(numDoc);
	numCuenta = ingresoDocumento.Ingresar_Documento(listCocow36DTO);
	

	return numCuenta;
}
public List<Cocow36DTO> getCreaCocow36AprobacionDipres( HttpServletRequest req, int rutUsuario, String dv, String tipo){
	int numDoc = Util.validaParametro(req.getParameter("numDoc"), 0);
	int numCuenta = Util.validaParametro(req.getParameter("numCuenta"), 0);
	String nombreCuenta = Util.validaParametro(req.getParameter("nombreCuenta"),"");
	int sedeUnidad = Util.validaParametro(req.getParameter("sedeUnidad"), 1);
	String tipoUnidad = Util.validaParametro(req.getParameter("tipoUnidad"), "");
	String ingresoDatos = Util.validaParametro(req.getParameter("ingresoDatos"),"");
	String unidadMadre = Util.validaParametro(req.getParameter("unidadMadre"), "");
	String tipoPresupuesto = Util.validaParametro(req.getParameter("tipoPresupuesto"),"");
	long valorCobro = Util.validaParametro(req.getParameter("valorCobro"), 0);
	String tipoCuenta = Util.validaParametro(req.getParameter("tipoCuenta"),"");
	
	List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
	List<Cocow36DTO>  listaSolicitudesCuenta = new ArrayList<Cocow36DTO>();
	listaSolicitudesCuenta = (List<Cocow36DTO>) req.getSession().getAttribute("listaSolicitudesCuenta");

    String nomcam = "";
    long valnu1 = 0;
    long valnu2 = 0;
    String valalf = "";

	/* prepara archivo para grabar, llenar listCocow36DTO*/
	nomcam = "CODUNI";
	valnu1 = numCuenta;
	valnu2 = 0;
	valalf = "";
	listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	
	nomcam = "DESUNI";
	valnu1 = 0;
	valnu2 = 0;
	valalf = nombreCuenta;
	listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	
	nomcam = "CODSED";
	valnu1 = sedeUnidad;
	valnu2 = 0;
	valalf = "";
	listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	
	nomcam = "OPEREM";
	valnu1 = 0;
	valnu2 = 0;
	valalf = tipoUnidad;
	listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	
	nomcam = "INGDAT";
	valnu1 = 0;
	valnu2 = 0;
	valalf = ingresoDatos;
	listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	
	nomcam = "CASING";
	valnu1 = 0;
	valnu2 = 0;
	valalf = unidadMadre;
	listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	
	nomcam = "INDAUT";
	valnu1 = 0;
	valnu2 = 0;
	valalf = tipoPresupuesto;
	listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	
	nomcam = "CODAG2";
	valnu1 = valorCobro;
	valnu2 = 0;
	valalf = "";
	listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );
	
	nomcam = "TIPCTA";
	valnu1 = 0;
	valnu2 = 0;
	valalf = tipoCuenta;
	listCocow36DTO = getAgregaLista(listCocow36DTO,tipo ,nomcam ,valnu1 ,valnu2 ,valalf );

	
	 if(listaSolicitudesCuenta != null && listaSolicitudesCuenta.size() > 0){
		    for (Cocow36DTO ss : listaSolicitudesCuenta){
		    	nomcam = "DETACC";
		 	    valnu1 = ss.getValnu1();
		 	    valnu2 = 0;
		 	    valalf = ss.getValalf();
		
		 	         Cocow36DTO cocow36DTO = new Cocow36DTO();
			 		 cocow36DTO.setTiping(tipo);
			 		 cocow36DTO.setNomcam(nomcam);
			 		 cocow36DTO.setValnu1(valnu1); // rut acceso
			 		 cocow36DTO.setValalf(valalf); // digito verificador
			 		 cocow36DTO.setResval(ss.getAccion());// accion I/A
			 		 listCocow36DTO.add(cocow36DTO);
			     }
	 }

return listCocow36DTO;
}
public String formateoNumeroSinDecimales(Double numero){
	/*formatear n�mero */
     Locale l = new Locale("es","CL");
     DecimalFormat formatter1 = (DecimalFormat)DecimalFormat.getInstance(l);
     formatter1.applyPattern("###,###,###");
 
   //  formatter1.format(rs.getDouble("RUTNUM"))   /*sin decimales*/
 return formatter1.format(numero);
}


public String eliminaCaracteresString(String sTexto){
	/*elimina acentos del string*/
   String linea = "";  
    if(!sTexto.trim().equals("")){
    for (int x=0; x < sTexto.length(); x++) {
    	  if (sTexto.charAt(x) == '\'' || (sTexto.charAt(x) == '\n') ||
    		   sTexto.charAt(x) == '\t'){
    		  
    		   linea += ' ';
    	  }
    	  else  linea += sTexto.charAt(x);
    	}				
    }
 return linea;
}
}
