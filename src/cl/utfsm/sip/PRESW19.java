package cl.utfsm.sip;

public class PRESW19 {
	private String TIPPRO;
	private int CODUNI;
	private int ITEDOC;
	private int ANOPAR;
	private int MESPAR;
	private int RUTIDE;
	private int FECPRO;
	private int HORPRO;
	private String ESTPRO;
	private String IDSOLI;
	private int NUMDOC;
	private String TIPDOC;
	private int SUCUR;
	private String DIGIDE;
	private String TIPOPE;
	private int NUMCOM;
	private int CODBAN;
	private int NUMCHE;
	private int FECMOV;
	private String CAJERO;
	private String TIPCUE;
	
    public PRESW19(String TIPPRO,
    		int CODUNI,
    		int ITEDOC,
    		int ANOPAR,
    		int MESPAR,
    		int RUTIDE,
    		int FECPRO,
    		int HORPRO,
    		String ESTPRO,
    		String IDSOLI,
    		int NUMDOC,
    		String TIPDOC,
    		int SUCUR,
    		String DIGIDE,
    		String TIPOPE,
    		int NUMCOM,
    		int CODBAN,
    		int NUMCHE,
    		int FECMOV,
    		String CAJERO,
    		String TIPCUE){
    	this.TIPPRO = TIPPRO;
		this.CODUNI = CODUNI;
		this.ITEDOC = ITEDOC;
		this.ANOPAR = ANOPAR;
		this.MESPAR = MESPAR;
		this.RUTIDE = RUTIDE;
		this.FECPRO = FECPRO;
		this.HORPRO = HORPRO;
		this.ESTPRO = ESTPRO;
		this.IDSOLI = IDSOLI;
		this.NUMDOC = NUMDOC;
		this.TIPDOC = TIPDOC;
		this.SUCUR = SUCUR;
		this.DIGIDE = DIGIDE;
		this.TIPOPE = TIPOPE;
		this.NUMCOM = NUMCOM;
		this.CODBAN = CODBAN;
		this.NUMCHE = NUMCHE;
		this.FECMOV = FECMOV;
		this.CAJERO = CAJERO;
		this.TIPCUE = TIPCUE;
    }

	public String getTIPPRO() {
		return TIPPRO;
	}

	public void setTIPPRO(String tippro) {
		TIPPRO = tippro;
	}

	public int getCODUNI() {
		return CODUNI;
	}

	public void setCODUNI(int coduni) {
		CODUNI = coduni;
	}

	public int getITEDOC() {
		return ITEDOC;
	}

	public void setITEDOC(int itedoc) {
		ITEDOC = itedoc;
	}

	public int getANOPAR() {
		return ANOPAR;
	}

	public void setANOPAR(int anopar) {
		ANOPAR = anopar;
	}

	public int getMESPAR() {
		return MESPAR;
	}

	public void setMESPAR(int mespar) {
		MESPAR = mespar;
	}

	public int getRUTIDE() {
		return RUTIDE;
	}

	public void setRUTIDE(int rutide) {
		RUTIDE = rutide;
	}

	public int getFECPRO() {
		return FECPRO;
	}

	public void setFECPRO(int fecpro) {
		FECPRO = fecpro;
	}

	public int getHORPRO() {
		return HORPRO;
	}

	public void setHORPRO(int horpro) {
		HORPRO = horpro;
	}

	public String getESTPRO() {
		return ESTPRO;
	}

	public void setESTPRO(String estpro) {
		ESTPRO = estpro;
	}

	public String getIDSOLI() {
		return IDSOLI;
	}

	public void setIDSOLI(String idsoli) {
		IDSOLI = idsoli;
	}

	public int getNUMDOC() {
		return NUMDOC;
	}

	public void setNUMDOC(int numdoc) {
		NUMDOC = numdoc;
	}

	public String getTIPDOC() {
		return TIPDOC;
	}

	public void setTIPDOC(String tipdoc) {
		TIPDOC = tipdoc;
	}

	public int getSUCUR() {
		return SUCUR;
	}

	public void setSUCUR(int sucur) {
		SUCUR = sucur;
	}

	public String getDIGIDE() {
		return DIGIDE;
	}

	public void setDIGIDE(String digide) {
		DIGIDE = digide;
	}

	public String getTIPOPE() {
		return TIPOPE;
	}

	public void setTIPOPE(String tipope) {
		TIPOPE = tipope;
	}

	public int getNUMCOM() {
		return NUMCOM;
	}

	public void setNUMCOM(int numcom) {
		NUMCOM = numcom;
	}

	public int getCODBAN() {
		return CODBAN;
	}

	public void setCODBAN(int codban) {
		CODBAN = codban;
	}

	public int getNUMCHE() {
		return NUMCHE;
	}

	public void setNUMCHE(int numche) {
		NUMCHE = numche;
	}

	public int getFECMOV() {
		return FECMOV;
	}

	public void setFECMOV(int fecmov) {
		FECMOV = fecmov;
	}

	public String getCAJERO() {
		return CAJERO;
	}

	public void setCAJERO(String cajero) {
		CAJERO = cajero;
	}

	public String getTIPCUE() {
		return TIPCUE;
	}

	public void setTIPCUE(String tipcue) {
		TIPCUE = tipcue;
	}
    
}

