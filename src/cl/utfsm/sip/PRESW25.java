package cl.utfsm.sip;


public class PRESW25 {
	private String TIPPRO = "";
	private int CODUNI = 0;
	private int ITEDOC = 0;
	private int ANOPRE = 0;
	private int MESPRE = 0;
	private int NUMREQ = 0;
	private String DESITE = "";
	private String IDSOLI = "";
	private String INDEXI = "";
	private long PRES01 = 0;
	private long PRES02 = 0;
	private long PRES03 = 0;
	private long PRES04 = 0;
	private long PRES05 = 0;	
	private long PRES06 = 0;
	private long PRES07 = 0;
	private long PRES08 = 0;
	private long PRES09 = 0;
	private long PRES010 = 0;
	private long PRES011 = 0;
	private long PRES012 = 0;	
	private String COMEN1 = "";
	private String COMEN2 = "";
	private String COMEN3 = "";
	private String COMEN4 = "";
	private String COMEN5 = "";
	private String COMEN6 = "";
	private String MOTIV1 = "";
	private String MOTIV2 = "";
	private String MOTIV3 = "";
	private String MOTIV4 = "";
	private String MOTIV5 = "";
	private String MOTIV6 = "";
	private String CODMAN = "";
	private int CANMAN  = 0;
	private int VALUNI  = 0;
	private String DESUNI = "";
	private String TIPFUN = "";
	private int RUTIDE = 0;
	private String DIGIDE = "";
	private String RESPUE = "";
	private String TIPCON = "";
	private int VALR11 = 0;
	private int VALR12 = 0;
	private int VALR21 = 0;
	private int VALR22 = 0;
	private int VALR31 = 0;
	private int VALR32 = 0;
	private int VALR41 = 0;
	private int VALR42 = 0;
	private int VALPR1 = 0;
	private int VALPR2 = 0;
	private String CODIGO = "";
	private String NOMFUN = "";
	private String INDMOD = "";
	private String AREA1 = "";
	private String AREA2 = "";
	private String CODGRU = "";
	private String EXIPRE = "";
	private String EXIING = "";
	private String EXINOM = "";
	private String EXIASI = "";
	public  PRESW25(){
		
	}
	public PRESW25(
		String TIPPRO,
		int CODUNI,
		int ITEDOC,
		int ANOPRE,
		int MESPRE,
		int NUMREQ,
		String DESITE,
		String IDSOLI,
		String INDEXI,
		long PRES01,
		long PRES02,
		long PRES03,
		long PRES04,
		long PRES05,	
		long PRES06,
		long PRES07,
		long PRES08,
		long PRES09,
		long PRES010,
		long PRES011,
		long PRES012,
		String COMEN1,
		String COMEN2,
		String COMEN3,
		String COMEN4,
		String COMEN5,
		String COMEN6,
		String MOTIV1,
		String MOTIV2,
		String MOTIV3,
		String MOTIV4,
		String MOTIV5,
		String MOTIV6,
		String CODMAN,
		int CANMAN,
		int VALUNI,
		String DESUNI,
		String TIPFUN,
		int RUTIDE,
		String DIGIDE,
		String RESPUE,
		String TIPCON,
		int VALRE11,
		int VALRE12,
		int VALRE21,
		int VALRE22,
		int VALRE31,
		int VALRE32,
		int VALRE41,
		int VALRE42,
		int VALPR1,
		int VALPR2,
		String CODIGO,
		String NOMFUN,
		String INDMOD,
		String AREA1,
		String AREA2,
		String CODGRU,
		String EXIPRE,
		String EXIING,
		String EXINOM,
		String EXIASI){
			this.TIPPRO = TIPPRO;
			this.CODUNI = CODUNI;
			this.ITEDOC = ITEDOC;
			this.ANOPRE = ANOPRE;
			this.MESPRE = MESPRE;
			this.NUMREQ = NUMREQ;
			this.DESITE = DESITE;
			this.IDSOLI = IDSOLI;
			this.INDEXI = INDEXI;
			this.PRES01 = PRES01;
			this.PRES02 = PRES02;
			this.PRES03 = PRES03;
			this.PRES04 = PRES04;
			this.PRES05 = PRES05;
			this.PRES06 = PRES06;
			this.PRES07 = PRES07;
			this.PRES08 = PRES08;
			this.PRES09 = PRES09;
			this.PRES010 =PRES010;
			this.PRES011 = PRES011;
			this.PRES012 = PRES012;
			this.COMEN1 = COMEN1;
			this.COMEN2 = COMEN2;
			this.COMEN3 = COMEN3;
			this.COMEN4 = COMEN4;
			this.COMEN5 = COMEN5;
			this.COMEN6 = COMEN6;
			this.MOTIV1 = MOTIV1;
			this.MOTIV2 = MOTIV2;
			this.MOTIV3 = MOTIV3;
			this.MOTIV4 = MOTIV4;
			this.MOTIV5 = MOTIV5;
			this.MOTIV6 = MOTIV6;
			this.CODMAN = CODMAN;
			this.CANMAN = CANMAN;
			this.VALUNI = VALUNI;
			this.DESUNI = DESUNI;
			this.TIPFUN = TIPFUN;
			this.RUTIDE = RUTIDE;
			this.DIGIDE = DIGIDE;
			this.RESPUE = RESPUE;
			this.TIPCON = TIPCON;
			this.VALR11 = VALRE11;
			this.VALR12 = VALRE12;
			this.VALR21 = VALRE21;
			this.VALR22 = VALRE22;
			this.VALR31 = VALRE31;
			this.VALR32 = VALRE32;
			this.VALR41 = VALRE41;
			this.VALR42 = VALRE42;
			this.VALPR1 = VALPR1;
			this.VALPR2 = VALPR2;
			this.CODIGO = CODIGO;
			this.NOMFUN = NOMFUN;
			this.INDMOD = INDMOD;
			this.AREA1 = AREA1;
			this.AREA2 = AREA2;
			this.CODGRU = CODGRU;
			this.EXIPRE = EXIPRE;
			this.EXIING = EXIING;
			this.EXINOM = EXINOM;
			this.EXIASI = EXIASI;
		}

	public String getTIPPRO() {
		return TIPPRO;
	}

	public void setTIPPRO(String tippro) {
		TIPPRO = tippro;
	}

	public int getCODUNI() {
		return CODUNI;
	}

	public void setCODUNI(int coduni) {
		CODUNI = coduni;
	}

	public int getITEDOC() {
		return ITEDOC;
	}

	public void setITEDOC(int itedoc) {
		ITEDOC = itedoc;
	}

	public int getANOPRE() {
		return ANOPRE;
	}

	public void setANOPRE(int anopre) {
		ANOPRE = anopre;
	}

	public int getMESPRE() {
		return MESPRE;
	}

	public void setMESPRE(int mespre) {
		MESPRE = mespre;
	}

	public int getNUMREQ() {
		return NUMREQ;
	}

	public void setNUMREQ(int numreq) {
		NUMREQ = numreq;
	}

	public String getDESITE() {
		return DESITE;
	}

	public void setDESITE(String desite) {
		DESITE = desite;
	}

	public String getIDSOLI() {
		return IDSOLI;
	}

	public void setIDSOLI(String idsoli) {
		IDSOLI = idsoli;
	}

	public String getINDEXI() {
		return INDEXI;
	}

	public void setINDEXI(String indexi) {
		INDEXI = indexi;
	}



	public long getPRES01() {
		return PRES01;
	}

	public void setPRES01(long pres01) {
		PRES01 = pres01;
	}

	public long getPRES02() {
		return PRES02;
	}

	public void setPRES02(long pres02) {
		PRES02 = pres02;
	}

	public long getPRES03() {
		return PRES03;
	}

	public void setPRES03(long pres03) {
		PRES03 = pres03;
	}

	public long getPRES04() {
		return PRES04;
	}

	public void setPRES04(long pres04) {
		PRES04 = pres04;
	}

	public long getPRES05() {
		return PRES05;
	}

	public void setPRES05(long pres05) {
		PRES05 = pres05;
	}

	public long getPRES06() {
		return PRES06;
	}

	public void setPRES06(long pres06) {
		PRES06 = pres06;
	}

	public long getPRES07() {
		return PRES07;
	}

	public void setPRES07(long pres07) {
		PRES07 = pres07;
	}

	public long getPRES08() {
		return PRES08;
	}

	public void setPRES08(long pres08) {
		PRES08 = pres08;
	}

	public long getPRES09() {
		return PRES09;
	}

	public void setPRES09(long pres09) {
		PRES09 = pres09;
	}

	public long getPRES010() {
		return PRES010;
	}

	public void setPRES010(long pres010) {
		PRES010 = pres010;
	}

	public long getPRES011() {
		return PRES011;
	}

	public void setPRES011(long pres011) {
		PRES011 = pres011;
	}

	public long getPRES012() {
		return PRES012;
	}

	public void setPRES012(long pres012) {
		PRES012 = pres012;
	}

	public String getCOMEN1() {
		return COMEN1;
	}

	public void setCOMEN1(String comen1) {
		COMEN1 = comen1;
	}

	public String getCOMEN2() {
		return COMEN2;
	}

	public void setCOMEN2(String comen2) {
		COMEN2 = comen2;
	}

	public String getCOMEN3() {
		return COMEN3;
	}

	public void setCOMEN3(String comen3) {
		COMEN3 = comen3;
	}

	public String getCOMEN4() {
		return COMEN4;
	}

	public void setCOMEN4(String comen4) {
		COMEN4 = comen4;
	}

	public String getCOMEN5() {
		return COMEN5;
	}

	public void setCOMEN5(String comen5) {
		COMEN5 = comen5;
	}

	public String getCOMEN6() {
		return COMEN6;
	}

	public void setCOMEN6(String comen6) {
		COMEN6 = comen6;
	}

	public String getMOTIV1() {
		return MOTIV1;
	}

	public void setMOTIV1(String motiv1) {
		MOTIV1 = motiv1;
	}

	public String getMOTIV2() {
		return MOTIV2;
	}

	public void setMOTIV2(String motiv2) {
		MOTIV2 = motiv2;
	}

	public String getMOTIV3() {
		return MOTIV3;
	}

	public void setMOTIV3(String motiv3) {
		MOTIV3 = motiv3;
	}

	public String getMOTIV4() {
		return MOTIV4;
	}

	public void setMOTIV4(String motiv4) {
		MOTIV4 = motiv4;
	}

	public String getMOTIV5() {
		return MOTIV5;
	}

	public void setMOTIV5(String motiv5) {
		MOTIV5 = motiv5;
	}

	public String getMOTIV6() {
		return MOTIV6;
	}

	public void setMOTIV6(String motiv6) {
		MOTIV6 = motiv6;
	}

	public String getCODMAN() {
		return CODMAN;
	}

	public void setCODMAN(String codman) {
		CODMAN = codman;
	}

	public int getCANMAN() {
		return CANMAN;
	}

	public void setCANMAN(int canman) {
		CANMAN = canman;
	}

	public int getVALUNI() {
		return VALUNI;
	}

	public void setVALUNI(int valuni) {
		VALUNI = valuni;
	}

	public String getDESUNI() {
		return DESUNI;
	}

	public void setDESUNI(String desuni) {
		DESUNI = desuni;
	}

	public String getTIPFUN() {
		return TIPFUN;
	}

	public void setTIPFUN(String tipfun) {
		TIPFUN = tipfun;
	}

	public int getRUTIDE() {
		return RUTIDE;
	}

	public void setRUTIDE(int rutide) {
		RUTIDE = rutide;
	}

	public String getDIGIDE() {
		return DIGIDE;
	}

	public void setDIGIDE(String digide) {
		DIGIDE = digide;
	}

	public String getRESPUE() {
		return RESPUE;
	}

	public void setRESPUE(String respue) {
		RESPUE = respue;
	}

	public String getTIPCON() {
		return TIPCON;
	}

	public void setTIPCON(String tipcon) {
		TIPCON = tipcon;
	}


	public int getVALR11() {
		return VALR11;
	}
	public void setVALR11(int valr11) {
		VALR11 = valr11;
	}
	public int getVALR12() {
		return VALR12;
	}
	public void setVALR12(int valr12) {
		VALR12 = valr12;
	}
	public int getVALR21() {
		return VALR21;
	}
	public void setVALR21(int valr21) {
		VALR21 = valr21;
	}
	public int getVALR22() {
		return VALR22;
	}
	public void setVALR22(int valr22) {
		VALR22 = valr22;
	}
	public int getVALR31() {
		return VALR31;
	}
	public void setVALR31(int valr31) {
		VALR31 = valr31;
	}
	public int getVALR32() {
		return VALR32;
	}
	public void setVALR32(int valr32) {
		VALR32 = valr32;
	}
	public int getVALR41() {
		return VALR41;
	}
	public void setVALR41(int valr41) {
		VALR41 = valr41;
	}
	public int getVALR42() {
		return VALR42;
	}
	public void setVALR42(int valr42) {
		VALR42 = valr42;
	}
	public int getVALPR1() {
		return VALPR1;
	}

	public void setVALPR1(int valpr1) {
		VALPR1 = valpr1;
	}

	public int getVALPR2() {
		return VALPR2;
	}

	public void setVALPR2(int valpr2) {
		VALPR2 = valpr2;
	}

	public String getCODIGO() {
		return CODIGO;
	}

	public void setCODIGO(String codigo) {
		CODIGO = codigo;
	}

	public String getNOMFUN() {
		return NOMFUN;
	}

	public void setNOMFUN(String nomfun) {
		NOMFUN = nomfun;
	}

	public String getINDMOD() {
		return INDMOD;
	}

	public void setINDMOD(String indmod) {
		INDMOD = indmod;
	}

	public String getAREA1() {
		return AREA1;
	}

	public void setAREA1(String area1) {
		AREA1 = area1;
	}

	public String getAREA2() {
		return AREA2;
	}

	public void setAREA2(String area2) {
		AREA2 = area2;
	}

	public String getCODGRU() {
		return CODGRU;
	}

	public void setCODGRU(String codgru) {
		CODGRU = codgru;
	}

	public String getEXIPRE() {
		return EXIPRE;
	}

	public void setEXIPRE(String exipre) {
		EXIPRE = exipre;
	}

	public String getEXIING() {
		return EXIING;
	}

	public void setEXIING(String exiing) {
		EXIING = exiing;
	}

	public String getEXINOM() {
		return EXINOM;
	}

	public void setEXINOM(String exinom) {
		EXINOM = exinom;
	}

	public String getEXIASI() {
		return EXIASI;
	}

	public void setEXIASI(String exiasi) {
		EXIASI = exiasi;
	}

		
	

}

