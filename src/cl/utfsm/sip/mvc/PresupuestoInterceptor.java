package cl.utfsm.sip.mvc;
/**
 * SISTEMA GENERAL DE FINANZAS
 * PROGRAMADOR		: 	M�NICA BARRERA FREZ
 * FECHA ULT.MODIF  : 	Octubre 2016   M. Alvarado, M. Barrera, K. Past�n 
 * UNIDAD DE DESARROLLO INSTITUCIONAL(DTI)   
 */
import java.math.BigDecimal; 
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Vector;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.auditoria.AuditoriaServicio;
import cl.utfsm.base.auditoria.DetalleServicio;
import cl.utfsm.base.auditoria.TipoAccionServicio;
import cl.utfsm.base.perfiles.modulos.ModuloPerfiles;
import cl.utfsm.base.util.Util;
import cl.utfsm.sip.PeriodoProcesos;
import cl.utfsm.sip.TipoCuenta;
import cl.utfsm.sip.TipoPresupuesto;
import cl.utfsm.sip.TipoUnidad;
import cl.utfsm.sip.modulo.ModuloPresupuesto;
import descad.cliente.CocowBean;
import descad.cliente.Funcionario;
import descad.cliente.Item;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Cocof17DTO;
import descad.presupuesto.FuncionarioDTO;
import descad.presupuesto.ItemDTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw21DTO;
import descad.presupuesto.Presw25DTO;


public class PresupuestoInterceptor extends HandlerInterceptorAdapter {
	private ModuloPresupuesto moduloPresupuesto;
	private ModuloPerfiles moduloPerfiles;
	static Logger log = Logger.getLogger(PresupuestoInterceptor.class);
	//int rutUsuario = 0;
	String funcionario = "";


	public ModuloPresupuesto getModuloPresupuesto() {
		return moduloPresupuesto;
	}

	public void setModuloPresupuesto(ModuloPresupuesto moduloPresupuesto) {
		this.moduloPresupuesto = moduloPresupuesto;

	}



	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {	
	

	}
	
	public void cuentasPresupuestarias(AccionWeb accionweb) throws Exception {
		/* MAA   ********** */
	    // if (accionweb.getSesion().getAttribute("listaUnidad") != null)
		//	accionweb.getSesion().setAttribute("listaUnidad", null);
		if (accionweb.getSesion().getAttribute("listaOrganizacion") != null)
			accionweb.getSesion().setAttribute("listaOrganizacion", null);
		
		// Collection<Presw18> listaPresw18 = (Collection<Presw18>)
		// Presw18Collection.generateCollectionPrr();
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		
		if(rutUsuario > 0){
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		String tipoCuentas = Util.validaParametro(accionweb.getParameter("tipoCuentas")," ");
		PreswBean preswbean = new PreswBean("PRR", 0, 0, 0, 0, rutUsuario);
		preswbean.setTipcue(tipoCuentas);
		preswbean.setDigide(dv);
		Collection<Presw18DTO> listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		String host = accionweb.getReq().getLocalAddr();
		moduloPresupuesto.agregaUnidad(accionweb.getReq(), listaPresw18);
		// accionweb.agregarObjeto("cuentasPresupuestarias", accionweb.getSesion().getAttribute("listaUnidad"));
		accionweb.agregarObjeto("cuentasPresupuestarias", accionweb.getSesion().getAttribute("listaOrganizacion"));
		accionweb.agregarObjeto("tipoCuentas", tipoCuentas);
		
		DetalleServicio detalleServicio = new DetalleServicio();
		detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(30));
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();
		tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(1);
		String parametro = "PRR";
		if(rutOrigen > 0)
			parametro = "sim:"+rutUsuario + "@" + parametro;
		
		AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
		auditoriaServicio.setDetalleServicio(detalleServicio);
		auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
		auditoriaServicio.setFecha(new Date());
		auditoriaServicio.setHost(host);
		auditoriaServicio.setParametro(parametro);
		if(rutOrigen > 0)
			auditoriaServicio.setUsuario(rutOrigen);
		else
			auditoriaServicio.setUsuario(rutUsuario);
		try {
			moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
			if(accionweb.getSesion().getAttribute("rutOrigen") != null){
				accionweb.getSesion().setAttribute("rutOrigen", accionweb.getSesion().getAttribute("rutOrigen"));
				accionweb.agregarObjeto("rutOrigen", accionweb.getSesion().getAttribute("rutOrigen"));
			}
			//System.out.println("origen: "+accionweb.getSesion().getAttribute("rutOrigen"));
			log.debug("Si guardada auditoria cuentasPresupuestarias");
			accionweb.agregarObjeto("control", control);
		} catch (Exception e) {
			log.debug("No guardada auditoria cuentasPresupuestarias");
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		}
	}
	
	
	public void cargaControlPresup(AccionWeb accionweb) throws Exception {
		// Collection<Presw18> lista = (Collection<Presw18>)
		// accionweb.getSesion().getAttribute("listaUnidad");
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		String selecUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"), "");
		String tipoCuentas = Util.validaParametro(accionweb.getParameter("tipoCuentas"), " ");
		accionweb.agregarObjeto("tipoCuentas", tipoCuentas);
	    accionweb.agregarObjeto("cuentasPresupuestarias", lista);
	    int codUnidad = 0;
	    int opcionMenu = 0;
		/*esto es para el menu de control presupuestario*/
	    if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			opcionMenu = Util.validaParametro(accionweb.getSesion().getAttribute("opcionMenu")+"",0);
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		
		/* fin */
	    
	    // es solo para mostrar el menu
	    int control = Util.validaParametro(accionweb.getParameter("control"), 0);
    	accionweb.agregarObjeto("control", control);
	    
        if(!selecUnidad.trim().equals("-1")){
        	accionweb.agregarObjeto("selecUnidad", selecUnidad);
        	int ind = 0;
        	String nomUnidad = "";
        	String saldo = "";
        	if(selecUnidad.indexOf("@") > 0){
        		ind = selecUnidad.indexOf("@");
        		if(ind > 0)
        			nomUnidad = selecUnidad.substring(0,ind).trim();
        		selecUnidad = selecUnidad.substring(ind+1);
        		codUnidad = Integer.parseInt(nomUnidad.trim());
        		accionweb.agregarObjeto("codUnidad", codUnidad);
        		
        	}
        	if(selecUnidad.indexOf("@") > 0){
        		ind = selecUnidad.indexOf("@");
        		if(ind > 0)
        			nomUnidad += " - " + selecUnidad.substring(0,ind).trim();
        		selecUnidad = selecUnidad.substring(ind+1).trim();
        		
        	}
        	saldo = selecUnidad;
        	
        	
        	accionweb.agregarObjeto("nomUnidad", nomUnidad);
        	accionweb.agregarObjeto("saldo", saldo);
         	
        }
	}
	public void cargaPresupuestacion(AccionWeb accionweb) throws Exception {
	      	// es solo para mostrar el menu
		    int control = Util.validaParametro(accionweb.getParameter("control"), 0);
        	accionweb.agregarObjeto("control", control);
      
	}
	public void cargaDisponibilidad(AccionWeb accionweb) throws Exception {
		// Collection<Presw18> lista = (Collection<Presw18>)
		// accionweb.getSesion().getAttribute("listaUnidad");
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int item = Util.validaParametro(accionweb.getParameter("item"), 0);
		String tipoCuentas = Util.validaParametro(accionweb.getParameter("tipoCuentas"), " ");
		accionweb.agregarObjeto("tipoCuentas", tipoCuentas);
		
//		if(accionweb.getSesion().getAttribute("menu") != null)
//			accionweb.getSesion().setAttribute("menu", null);
//		moduloPresupuesto.agregaMenuControl(accionweb.getReq(), "disponibilidad");
		Item items = new Item();
		Collection<ItemDTO> listaItem = (Collection<ItemDTO>) items.todos_items();
		
		/*esto es para el menu de control presupuestario*/
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		accionweb.getSesion().setAttribute("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		
		/* fin */
		
		String selecUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"), "");
		accionweb.agregarObjeto("cuentasPresupuestarias", lista);
	    
        if(!selecUnidad.trim().equals("-1")){
        	accionweb.agregarObjeto("selecUnidad", selecUnidad);
        	int ind = 0;
        	String nomUnidad = "";
        	String saldo = "";
        	if(selecUnidad.indexOf("@") > 0){
        		ind = selecUnidad.indexOf("@");
        		if(ind > 0)
        			nomUnidad = selecUnidad.substring(0,ind).trim();
        		selecUnidad = selecUnidad.substring(ind+1).trim();
        		accionweb.agregarObjeto("codUnidad", nomUnidad.trim());
        		
        	}
        	if(selecUnidad.indexOf("@") > 0){
        		ind = selecUnidad.indexOf("@");
        		if(ind > 0)
        			nomUnidad += " - " + selecUnidad.substring(0,ind).trim();
        		selecUnidad = selecUnidad.substring(ind+1).trim();
        		
        	}
        	saldo = selecUnidad;
        	
        	
        	accionweb.agregarObjeto("nomUnidad", nomUnidad);
           	accionweb.agregarObjeto("saldo", saldo);
        	
        }
	    
	   
		accionweb.agregarObjeto("listaItem", listaItem);
		accionweb.agregarObjeto("cuentasPresupuestarias", lista);
		accionweb.agregarObjeto("unidadSeleccionada", unidad);
	    accionweb.agregarObjeto("opcion", "1");
		accionweb.agregarObjeto("messelec", mes);
		accionweb.agregarObjeto("annoselec", anno);
		accionweb.agregarObjeto("codItem", item);
		accionweb.agregarObjeto("disponibilidad", "1");
		
/*		descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
		funcionarioBean.buscar_funcionario(rutUsuario);
		String nomSimulacion = funcionarioBean.getNombre();
		accionweb.agregarObjeto("nomSimulacion", nomSimulacion);*/

	}

	public void cargaResultadoDisponibilidad(AccionWeb accionweb)
			throws Exception {
		int item = Util.validaParametro(accionweb.getParameter("item"), 0);
		//String unidad = Util.validaParametro(accionweb.getParameter("selecUnidad"),"");
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		
		int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "","0"));
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		String tipoCuentas = Util.validaParametro(accionweb.getParameter("tipoCuentas"), " ");
		accionweb.agregarObjeto("tipoCuentas", tipoCuentas);
		Collection<Presw18DTO> listaPresw18 = null;
		// Collection<Presw18> listaPresw18 = null;
		int codUnidad = Util.validaParametro(accionweb.getParameter("codUnidad"),0);
		String nomUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"),"");
		String nomItem = "";
        if(codUnidad > 0 && a�o > 0){
			if (item == -1 || item == 0) {
				// listaPresw18 = (Collection<Presw18>)
				// Presw18Collection.generateCollectionPra(codUnidad, a�o);
				try {
					PreswBean preswbean = new PreswBean("PRA", codUnidad, 0, a�o, 0, 0);
					preswbean.setRutide(rutUsuario);
					preswbean.setDigide(dv);
					listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
				} catch (Exception E) {
					System.out.println("excepcion   " + E);

				}
			} else {
				// listaPresw18 = (Collection<Presw18>)
				// Presw18Collection.generateCollectionPmi(codUnidad, item,
				// a�o);
				PreswBean preswbean = new PreswBean("PMI", codUnidad, item,
						a�o, 0, 0);
				preswbean.setRutide(rutUsuario);
				preswbean.setDigide(dv);
				listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
				descad.cliente.Item itemdto = new descad.cliente.Item();
				itemdto.buscar_item(item);
				nomItem = item + " - " + itemdto.getNombre();
			}
		
		accionweb.agregarObjeto("detalleDisponible", listaPresw18);
		accionweb.agregarObjeto("codUnidad", codUnidad);
		accionweb.agregarObjeto("nomUnidad", nomUnidad);
		accionweb.agregarObjeto("anno", a�o);
		accionweb.agregarObjeto("item", item);
		accionweb.agregarObjeto("nomItem", nomItem);

		/* auditoria */
		String host = accionweb.getReq().getLocalAddr();
		DetalleServicio detalleServicio = new DetalleServicio();
		detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(31));
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();
		tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(1);
		String parametro = "";
		if (item == -1 || item == 0)
			parametro = "PRA";
		else
			parametro = "PMI";
		parametro += "&unidad=" + codUnidad + " - " + nomUnidad + "&item=" + item + "&anno=" + a�o
				+ "&mes=" + 0;
		AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
		auditoriaServicio.setDetalleServicio(detalleServicio);
		auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
		auditoriaServicio.setFecha(new Date());
		auditoriaServicio.setHost(host);
		if(rutOrigen > 0)
			parametro = "sim:"+ rutUsuario + "@" + parametro;
	
		auditoriaServicio.setParametro(parametro);
		if(rutOrigen > 0)
			auditoriaServicio.setUsuario(rutOrigen);
		else
			auditoriaServicio.setUsuario(rutUsuario);
		try {
			moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
			log.debug("Si guardada cargaResultadoDisponibilidad");
		} catch (Exception e) {
			log.debug("No guardada cargaResultadoDisponibilidad");
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		/* fin auditoria */
        }
	}

	public void cargaDocumento(AccionWeb accionweb) throws Exception {
		  int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		  String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
			
		  int codUnidad = Util.validaParametro(accionweb.getParameter("codUnidad"),0);
		  String nomUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"),"");
		  String opcion    = Util.validaParametro(accionweb.getParameter("opcion"),"");
		 if(rutUsuario > 0) {
		 // codUnidad = 111350;
		  Date fechaInicio  = null;
		  Date fechaTermino = null; 
		  int numdoc = 0;
		  int fecmov = 99999999;
		  
		  if (opcion.equals("FAC") || opcion.equals("IMP")) {
		    if (!accionweb.getParameter("fechaInicio").trim().equals("") && 
			  !accionweb.getParameter("fechaTermino").trim().equals("")) {
		    fechaInicio = Util.validaParametroFecha(accionweb.getParameter("fechaInicio"),new Date());
		    fechaTermino = Util.validaParametroFecha(accionweb.getParameter("fechaTermino"),new Date());
		    SimpleDateFormat outFmt = new SimpleDateFormat("yyyyMMdd");

		    String fechaIni = outFmt.format(fechaInicio);
		    String fechaTer = outFmt.format(fechaTermino);
		    
		    numdoc = Integer.parseInt(fechaIni);
		    fecmov = Integer.parseInt(fechaTer);
		   }
		  }
		  else {
			  numdoc = 0;
			  fecmov = 0;
		  }
		  
		  Collection<Presw21DTO> listaPresw21 = null;
		  if (codUnidad > 0) {
		   try {
		 	  PreswBean preswbean = new PreswBean(opcion,codUnidad,0,0,0,rutUsuario,numdoc,"",0,"","",0,0,0,fecmov,"","");
		      preswbean.setDigide(dv);
			  listaPresw21 = (Collection<Presw21DTO>) preswbean.consulta_presw21();
		   	   } catch (Exception E) {
			 System.out.println("excepcion   " + E);
		    }
		  }	 
		  
		  accionweb.agregarObjeto("consultaDoc", listaPresw21);
		  accionweb.agregarObjeto("codUnidad", codUnidad);
		  accionweb.agregarObjeto("nomUnidad", nomUnidad);
		 }
		 }
	
	public void cargaDocumentoMCI(AccionWeb accionweb) throws Exception {
		  int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		  if(rutUsuario > 0){
		  int codUnidad = Util.validaParametro(accionweb.getParameter("codUnidad"),0);
		  int fecmov    = Util.validaParametro(accionweb.getParameter("fecmov"),0);
		  String opcion    = Util.validaParametro(accionweb.getParameter("opcion"),"");
		  String tipope = Util.validaParametro(accionweb.getParameter("tipope"),"");
		  int numche     = Util.validaParametro(accionweb.getParameter("numche"),0);
		  int rutide    = Util.validaParametro(accionweb.getParameter("rutide"),0);
		  String digide = Util.validaParametro(accionweb.getParameter("digide"),"");
		  String tipcue = Util.validaParametro(accionweb.getParameter("tipcue"),"");
		  int mespar    = Util.validaParametro(accionweb.getParameter("mespar"),0);
		  int anopar    = Util.validaParametro(accionweb.getParameter("anopar"),0);
		  String nommes = Util.validaParametro(accionweb.getParameter("nommes"),"");
		  String nomUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"),"");
		  int numcom    = Util.validaParametro(accionweb.getParameter("numcom"),0);
		  String cajero = Util.validaParametro(accionweb.getParameter("cajero"),"");
		  int codban    = Util.validaParametro(accionweb.getParameter("codBan"),0);
		  int fecdoc    = Util.validaParametro(accionweb.getParameter("fecdoc"),0);
			
				  
		  Collection<Presw21DTO> listaPresw21 = null;
		  if (!opcion.equals("")) {
		   try {
			  PreswBean preswbean = new PreswBean(opcion,0,0,0,0,rutide,0,"",0,digide,
					  tipope,0,codban,numche,fecmov,"",tipcue); 
			  
		 	  listaPresw21 = (Collection<Presw21DTO>) preswbean.consulta_presw21();
		 	  String tipoCuenta = "";
		     	 if (listaPresw21.size() > 0 ) {
						accionweb.agregarObjeto("consultaDoc", listaPresw21);	
						int i = 0;
						for (Presw21DTO ss : listaPresw21){
							
							if(ss.getTipcue().trim().equals("Q"))	
								tipoCuenta = "CHE";
							
							if(ss.getTipcue().trim().equals("R"))	
								tipoCuenta = "PRO";
							
							if(ss.getTipcue().trim().equals("J"))	
								tipoCuenta = "JUD";
							
							
							accionweb.agregarObjeto("tipMov", ss.getTipmov());
							accionweb.agregarObjeto("descTipo", ss.getGlosa1());
							accionweb.agregarObjeto("fechaMov", ss.getFecdoc());
							accionweb.agregarObjeto("fechaPago", ss.getFecven());
							accionweb.agregarObjeto("comprobante", ss.getNumdoc());
							accionweb.agregarObjeto("codCajero", ss.getCajero());
							accionweb.agregarObjeto("rutEmisor", ss.getRutide());
							accionweb.agregarObjeto("digito", ss.getDigide());
							accionweb.agregarObjeto("nombreGirador", ss.getGlosa9());
							accionweb.agregarObjeto("numCheque", ss.getNrodoc());
							accionweb.agregarObjeto("codbanco", ss.getCodban());
							accionweb.agregarObjeto("nomBanco", ss.getGlosa2());
							accionweb.agregarObjeto("codCuenta",tipoCuenta );
							accionweb.agregarObjeto("nomCuenta", ss.getGlosa3());							
							accionweb.agregarObjeto("valorTotal", ss.getValor1());
							if(ss.getCodun1() > 0) {
							accionweb.agregarObjeto("codUnidad1", ss.getCodun1());
							accionweb.agregarObjeto("descUnidad1", ss.getGlosa4());
							accionweb.agregarObjeto("valorUnidad1", ss.getValor2());
							accionweb.agregarObjeto("dh1", ss.getTipdo1());
							}
							if(ss.getCodun2() > 0){
							accionweb.agregarObjeto("codUnidad2", ss.getCodun2());
							accionweb.agregarObjeto("descUnidad2", ss.getGlosa5());
							accionweb.agregarObjeto("valorUnidad2", ss.getValor3());
							accionweb.agregarObjeto("dh2", ss.getGlosa10());
							}
							if(ss.getCodun3() > 0) {
							accionweb.agregarObjeto("codUnidad3", ss.getCodun3());
							accionweb.agregarObjeto("descUnidad3", ss.getGlosa6());
							accionweb.agregarObjeto("valorUnidad3", ss.getValor4());
							accionweb.agregarObjeto("dh3", ss.getGlosa11());
							}
							if(ss.getCodun4() > 0){
							accionweb.agregarObjeto("codUnidad4", ss.getCodun4());
							accionweb.agregarObjeto("descUnidad4", ss.getGlosa7());
							accionweb.agregarObjeto("valorUnidad4", ss.getValor5());
							accionweb.agregarObjeto("dh4", ss.getGlosa12());
							}
							if(ss.getCodun5() > 0){
							accionweb.agregarObjeto("codUnidad5", ss.getCodun5());
							accionweb.agregarObjeto("descUnidad5", ss.getGlosa8());
							accionweb.agregarObjeto("valorUnidad5", ss.getValor6());
							accionweb.agregarObjeto("dh5", ss.getGlosa13());
							}
						
						}
						 	
		
		       }
		 	  } catch (Exception E) {
			 System.out.println("excepcion   " + E);
		    }
	      }	 
		  }
		 }
	
	
	
	public void cargaDocumentoDetalle(AccionWeb accionweb) throws Exception {
		  int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		  if(rutUsuario > 0){
		  int codUnidad = Util.validaParametro(accionweb.getParameter("codUnidad"),0);
		  int numdoc    = Util.validaParametro(accionweb.getParameter("numdoc"),0);
		  String tipdoc = Util.validaParametro(accionweb.getParameter("tipdoc"),"");
		  String tipcue = Util.validaParametro(accionweb.getParameter("tipcue"),"");
		  int sucur     = Util.validaParametro(accionweb.getParameter("sucur"),0);
		  int mespar    = Util.validaParametro(accionweb.getParameter("mespar"),0);
		  int anopar    = Util.validaParametro(accionweb.getParameter("anopar"),0);
		  String nommes = Util.validaParametro(accionweb.getParameter("nommes"),"");
		  String nomUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"),"");
		  int numcom    = Util.validaParametro(accionweb.getParameter("numcom"),0);
		  String cajero = Util.validaParametro(accionweb.getParameter("cajero"),"");
		  int fecmov    = Util.validaParametro(accionweb.getParameter("fecmov"),0);
		  int codban    = Util.validaParametro(accionweb.getParameter("codban"),0);
		  int numche    = Util.validaParametro(accionweb.getParameter("numche"),0);
		  int fecdoc    = Util.validaParametro(accionweb.getParameter("fecdoc"),0);
		  int rutide    = Util.validaParametro(accionweb.getParameter("rutide"),0);
		  String digide = Util.validaParametro(accionweb.getParameter("digide"),"");
		  String opcion    = Util.validaParametro(accionweb.getParameter("opcion"),"");
		  String nomClie    = Util.validaParametro(accionweb.getParameter("nomClie"),"");
		  int volver    = Util.validaParametro(accionweb.getParameter("volver"),0);
	
			  
	//	 codUnidad = 111350;// CHM // 384501;  // LIF - LID 
		  
		  Collection<Presw21DTO> listaPresw21 = null;
		  if (!opcion.equals("")) {
		   try {
			  PreswBean preswbean = new PreswBean((opcion.equals("FCC")?"DFP":opcion),codUnidad,0,anopar,mespar,rutide,numdoc,tipdoc,sucur,digide,
		 			                              "",numcom,codban,numche,fecmov,cajero,tipcue); 
		 			                     /*         lo comentari�, pues habl� con Pedro y se necesita mandar el tipcue y rutide que se env�a en consulta anterior*/
			//   PreswBean preswbean = new PreswBean(opcion,codUnidad,0,anopar,mespar,rutide,numdoc,tipdoc,sucur,digide,"",numcom,codban,numche,fecmov,cajero,tipcue); 


		 	  listaPresw21 = (Collection<Presw21DTO>) preswbean.consulta_presw21();
		 	
			 	accionweb.agregarObjeto("numdoc", numdoc);
			 	accionweb.agregarObjeto("tipdoc", tipdoc);  
			 	accionweb.agregarObjeto("nomUnidad", nomUnidad); 
			 	accionweb.agregarObjeto("codUnidad", codUnidad);
			 	accionweb.agregarObjeto("tipcue", tipcue);
			 	accionweb.agregarObjeto("anopar", anopar);
			 	accionweb.agregarObjeto("numcom", numcom);
			 	accionweb.agregarObjeto("cajero", cajero);
			 	accionweb.agregarObjeto("numche", numche);
				accionweb.agregarObjeto("fecmov", fecmov);
		     	accionweb.agregarObjeto("mespar", mespar);
			 	accionweb.agregarObjeto("fecdoc", fecdoc);
			 	accionweb.agregarObjeto("volver", volver);
			 	
			 	if (opcion.equals("PVA") || opcion.equals("LIF") || opcion.equals("MCH") || opcion.equals("MCD") || opcion.equals("IN1")
			 			/* el proceso estaba repetido y se cambio 6/10/2010  || opcion.equals("ING")*/) {
			 		if(mespar > 0) {
                     if(mespar == 1)
                    	 nommes = "Enero";
                	 else if (mespar == 2)
                		nommes = "Febrero";
                     else if (mespar == 3)
                		nommes = "Marzo";
                	 else if (mespar == 4)
                		nommes = "Abril";
                	 else if (mespar == 5)
                		nommes = "Mayo";
                	 else if (mespar == 6)
                		nommes = "Junio";
                	 else if (mespar == 7)
                		nommes = "Julio";
                	 else if (mespar == 8)
                		nommes = "Agosto";
                	 else if (mespar == 9)
                		nommes = "Septiembre";
                	 else if (mespar == 10)
                		nommes = "Octubre";
                	 else if (mespar == 11)
                		nommes = "Noviembre";
                	 else if (mespar == 12)
                		nommes = "Diciembre";
			 		}
			 	   accionweb.agregarObjeto("sucur", "1"/*Seg�n Pedro Peralta para esta consulta siempre es 1*/);	
			 	   accionweb.agregarObjeto("nomSucur", getNombreSucursal(1/*Seg�n Pedro Peralta para esta consulta siempre es 1*/));
		        }  
			 	
		     	accionweb.agregarObjeto("nommes", nommes);
		     	 if (listaPresw21.size() > 0 ) {
		     		accionweb.agregarObjeto("consultaDoc", listaPresw21);	
				
		      	if (opcion.equals("FCC") || opcion.equals("CVE") || opcion.equals("CHE")) {
			 	   //	 rutide = 0;
				 	// digide = "";
				 //	 tipcue = ""; 
			 		 Iterator it = listaPresw21.iterator();  
		             while(it.hasNext()) {  
		            	 Presw21DTO reg = (Presw21DTO) it.next();  
		            //	 rutide = reg.getRutide();
		            //	 digide = reg.getDigide();
		            //	 tipcue = "C"; // para consulta FCC
		            	 accionweb.agregarObjeto("rutide", rutide); 
		      	 	     accionweb.agregarObjeto("digide", digide);
		      	 	     accionweb.agregarObjeto("nomclie", nomClie);
         	      	 	 accionweb.agregarObjeto("nomSucur", getNombreSucursal(sucur));
         	      	 	 if (opcion.equals("CHE")) 
         	      	 		 accionweb.agregarObjeto("unidadDestino", reg.getCodun1() + " - " + reg.getGlosa3());
         	      	 	 else
         	      	 		accionweb.agregarObjeto("unidadDestino", reg.getCodun1() + " - " + reg.getGlosa2());
         	      	     accionweb.agregarObjeto("cuenta", reg.getTipcue() + " - " + reg.getGlosa2());
         	      	     if(!reg.getGlosa4().trim().equals(""))
         	      	         accionweb.agregarObjeto("prVentas", reg.getCodun1() + " - " + reg.getGlosa4());
         	      	     sucur = reg.getSucur();
		            	 break;
		             } 
		      if (opcion.equals("FCC")){
	             try {
	            	accionweb.agregarObjeto("sucur", sucur);
	            	accionweb.agregarObjeto("nomSucur", getNombreSucursal(sucur));
	            	listaPresw21 = new  ArrayList<Presw21DTO>(); 
	            	
		 			preswbean = new PreswBean(opcion,0,0,0,0,rutide,numdoc,tipdoc,sucur,digide,
		 				 			          "",0,0,0,0,"",tipcue); 

		 			listaPresw21 = (Collection<Presw21DTO>) preswbean.consulta_presw21();
		 			accionweb.agregarObjeto("consultaDoc", listaPresw21);
		 	  
		 		   } catch (Exception E) {
		 		      System.out.println("excepcion   " + E);
		 			   }	 
		      }
		       }
		       }
		 	  } catch (Exception E) {
			 System.out.println("excepcion   " + E);
		    }
	      }	 
		  }
		 }
	
	public String getNombreSucursal(int sucur){
	 String nomSucur ="";	
	 if (sucur == 1)
	   	nomSucur = "CASA CENTRAL";
	 else if( sucur == 2)
	    nomSucur = "VI�A DEL MAR";
	 else if (sucur == 3)
	    nomSucur = "TALCAHUANO";
	 else if (sucur == 4)
	    nomSucur = "SANTIAGO";
	 else if (sucur == 5)
	    nomSucur = "RANCAGUA"; 
	 else if (sucur == 6)
	    nomSucur = "A.C.A."; 
  	 return nomSucur;
	}

	 synchronized public void exportar(AccionWeb accionweb) throws Exception {

		int item = Util.validaParametro(accionweb.getParameter("item"), 0);
		String unidad = Util.validaParametro(accionweb.getParameter("unidad"),"");
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int sumpresac = Util.validaParametro(accionweb.getParameter("sumpresac"), 0);
		int sumsadac = Util.validaParametro(accionweb.getParameter("sumsadac"), 0);
		String extension = Util.validaParametro(accionweb.getParameter("extension"), "");
		String nomItem = Util.validaParametro(accionweb.getParameter("nomItem"),"");
		int indice = -1;
		Long inicial = new Long(0);
		indice = unidad.indexOf("-");
		String nomUnidad = unidad.substring(0, indice).trim() + " - " + unidad.substring(indice + 1, unidad.length()).trim();
		int codUnidad = Integer.parseInt(unidad.substring(0, indice).trim());
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");

		Vector vecdatos = new Vector();
		if(rutUsuario > 0){
			List<Presw18DTO> listaPresw18 = null;
		if (item == -1 || item == 0) {
			// accionweb.agregarObjeto("data",
			// Presw18Collection.generateCollectionPra( codUnidad, anno));
			PreswBean preswbean = new PreswBean("PRA", codUnidad, 0, anno, 0, 0);
			preswbean.setRutide(rutUsuario);
			preswbean.setDigide(dv);
			listaPresw18 = (List<Presw18DTO>) preswbean.consulta_presw18();
			accionweb.agregarObjeto("data", listaPresw18);
		} else {
			// accionweb.agregarObjeto("data",
			// Presw18Collection.generateCollectionPmi( codUnidad, item, anno));
			PreswBean preswbean = new PreswBean("PMI", codUnidad, item, anno, 0, 0);
			preswbean.setRutide(rutUsuario);
			preswbean.setDigide(dv);
			listaPresw18 = (List<Presw18DTO>) preswbean.consulta_presw18();
			accionweb.agregarObjeto("data", listaPresw18);
		   
		}
			String nommes = "";
			MathTool mathtool = new MathTool();
			int porc = 0;
			int porc2 = 0;
			Long diferencia = new Long(0);
			Long diferencia2 = new Long(0);
		
			Long sumPresum = new Long(0);
			Long sumUsadom = new Long(0);
			Long sumPresac = new Long(0);
			Long sumUsadac = new Long(0);
			for (Presw18DTO ss : listaPresw18){
			
			if(ss.getNummes() > 0) {
				switch(ss.getNummes()){
				case 1 :nommes = "Enero";
				break;
				case 2: nommes = "Febrero";
				break;
				case 3:	nommes = "Marzo";
				break;
				case 4: nommes = "Abril";
				break;
				case 5: nommes = "Mayo";
				break;
				case 6:	nommes = "Junio";
				break;
				case 7:	nommes = "Julio";
				break;
				case 8:	nommes = "Agosto";
				break;
				case 9:	nommes = "Septiembre";
				break;
				case 10: nommes = "Octubre";
				break;
				case 11: nommes = "Noviembre";
				break;
				case 12: nommes = "Diciembre";
		 		}
			} else nommes = "Saldo Inicial";
				nommes = nommes + " " + ss.getIndprc();
				porc = 0;
				porc2 = 0;
				diferencia = new Long(0);
				diferencia2 = new Long(0);
				 diferencia = ss.getPresum() - ss.getUsadom();
				
                if(ss.getPresum() != 0 && ss.getUsadom() != 0){
                   	porc = mathtool.round(mathtool.mul(mathtool.div(ss.getUsadom() ,ss.getPresum()),100));                   	            
                   
                    if(porc < 0) porc = 0;
                }
                   if(ss.getUsadac() != 0  && ss.getPresac() != 0){
                		porc2 = mathtool.round(mathtool.mul(mathtool.div(ss.getUsadac() ,ss.getPresac()),100));
                       
                        if(porc2 < 0) porc2 = 0;
                   }
                if(ss.getNummes() == 0) {
               		diferencia2 = diferencia;
               		inicial = diferencia2;
                } else
                	 diferencia2 = ss.getPresac() - ss.getUsadac() + inicial;
                   
            Vector datos = new Vector();
			datos.addElement(nommes);
			datos.addElement(ss.getPresum());
			datos.addElement(ss.getUsadom());
			datos.addElement(porc);
			datos.addElement(diferencia);
			datos.addElement(ss.getPresac());
			if(ss.getNummes() == 0)
				datos.addElement(0);
			else
				datos.addElement(ss.getUsadac());
			datos.addElement(porc2);
			if(item > 0)  // agregado saldo inicial PP 14/01/2013
				datos.addElement(ss.getValdoc());
			datos.addElement(diferencia2);
			vecdatos.addElement(datos);
	       	sumPresum = sumPresum + ss.getPresum();
			sumUsadom = sumUsadom + ss.getUsadom();
			sumPresac = ss.getPresac();
			sumUsadac = ss.getUsadac();
			
			}
			 Vector datos = new Vector();
				datos.addElement("Total");
				datos.addElement(sumPresum);
				datos.addElement(sumUsadom);
				datos.addElement("");
				datos.addElement("");
				datos.addElement(sumPresac);
				datos.addElement(sumUsadac);
				datos.addElement("");
				if(item > 0)  // agregado saldo inicial PP 14/01/2013
					datos.addElement("");
			
				datos.addElement("");
				vecdatos.addElement(datos);
		       
			moduloPresupuesto.agregaDisponibilidadExcel(accionweb.getReq(), vecdatos);
			
	
		}
		accionweb.agregarObjeto("anno", new Integer(anno));
		accionweb.agregarObjeto("unidad", new String(nomUnidad));
		accionweb.agregarObjeto("sumpresac", new Integer(sumpresac));
		accionweb.agregarObjeto("sumsadac", new Integer(sumsadac));
		accionweb.agregarObjeto("nomItem", new String(nomItem));
		accionweb.agregarObjeto("format", extension);
		accionweb.agregarObjeto("inicial", inicial);

		/* auditoria */
		String host = accionweb.getReq().getLocalAddr();
		int accion = 1;
		DetalleServicio detalleServicio = new DetalleServicio();
		detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(31));
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();

		if (extension.trim().equals("xls"))
			accion = 3;
		else if (extension.trim().equals("pdf")) {
			if (sumsadac == -1)
				accion = 2;
			else
				accion = 4;
		}
		tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(accion);
		String parametro = "";
		if (item == -1 || item == 0)
			parametro = "PRA";
		else
			parametro = "PMI";
		parametro += "&unidad=" + unidad + "&item=" + item + "&anno=" + anno
				+ "&mes=" + 0;
		AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
		auditoriaServicio.setDetalleServicio(detalleServicio);
		auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
		auditoriaServicio.setFecha(new Date());
		auditoriaServicio.setHost(host);
		auditoriaServicio.setParametro(parametro);
		auditoriaServicio.setUsuario(rutUsuario);
		try {
			moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
			log.debug("Si guardada exportar");
		} catch (Exception e) {
			log.debug("No guardada exportar");
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		/* fin auditoria */

	}

	 synchronized public void exportarEjecucion(AccionWeb accionweb) throws Exception {
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		String unidad = Util.validaParametro(accionweb.getParameter("unidad"), "");
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int ejecucion = Util.validaParametro(accionweb.getParameter("ejecucion"), 0);
		String extension = Util.validaParametro(accionweb.getParameter("extension"), "");
		String estamento = Util.validaParametro(accionweb.getParameter("estamento"), "");
		int indice = -1;
		indice = unidad.indexOf("-");
		String nomUnidad = unidad.substring(0, indice).trim() + " - " + unidad.substring(indice + 1, unidad.length()).trim();
		int codUnidad = Integer.parseInt(unidad.substring(0, indice).trim());
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		
		int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "","0"));
		Vector vec_datos = new Vector();
		Vector datos = new Vector();
		String nomMes = "";
		if (mes > 0) {
			switch (mes) {
			case 1:
				nomMes = "Enero";
				break;
			case 2:
				nomMes = "Febrero";
				break;
			case 3:
				nomMes = "Marzo";
				break;
			case 4:
				nomMes = "Abril";
				break;
			case 5:
				nomMes = "Mayo";
				break;
			case 6:
				nomMes = "Junio";
				break;
			case 7:
				nomMes = "Julio";
				break;
			case 8:
				nomMes = "Agosto";
				break;
			case 9:
				nomMes = "Septiembre";
				break;
			case 10:
				nomMes = "Octubre";
				break;
			case 11:
				nomMes = "Noviembre";
				break;
			case 12:
				nomMes = "Diciembre";
				break;
			}
		}
		if(rutUsuario > 0){
		if(ejecucion == 1){
			PreswBean preswbean = new PreswBean("PAI", codUnidad, 0, anno, mes, 0);
			preswbean.setRutide(rutUsuario);
			preswbean.setDigide(dv);
			accionweb.agregarObjeto("data", preswbean.consulta_presw18());
		} else {
			    Collection<Presw21DTO> listaPresw21 = null;
			 	PreswBean preswbean = new PreswBean("ERE", codUnidad, 0, anno, 0,	0,0, "", 0, "", "", 0, 0, 0, 0, "", estamento);
				preswbean.setRutide(rutUsuario);
				preswbean.setDigide(dv);
				listaPresw21 = (Collection<Presw21DTO>) preswbean.consulta_presw21();			
				accionweb.agregarObjeto("data", listaPresw21);
				
				
		}
		}
		accionweb.agregarObjeto("anno", new Integer(anno));
		accionweb.agregarObjeto("unidad", new String(nomUnidad));
		accionweb.agregarObjeto("nommes", new String(nomMes));
		accionweb.agregarObjeto("format", extension);

		/* auditoria */
		String host = accionweb.getReq().getLocalAddr();
		int accion = 1;
		DetalleServicio detalleServicio = new DetalleServicio();
		detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(33));
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();

		if (extension.trim().equals("xls"))
			accion = 3;
		else if (extension.trim().equals("pdf"))
			accion = 4;

		tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(accion);
		String parametro = (ejecucion == 1)?"PAI":"ERE";

		parametro += "&unidad=" + codUnidad + "&item=0" + "&anno=" + anno
				+ "&mes=" + mes;
		AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
		auditoriaServicio.setDetalleServicio(detalleServicio);
		auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
		auditoriaServicio.setFecha(new Date());
		auditoriaServicio.setHost(host);
		if(rutOrigen > 0)
			parametro = "sim:"+ rutUsuario + "@" + parametro;
	
		auditoriaServicio.setParametro(parametro);
		if(rutOrigen > 0)
			auditoriaServicio.setUsuario(rutOrigen);
		else
			auditoriaServicio.setUsuario(rutUsuario);
		try {
			moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
			log.debug("Si guardada exportarEjecucion");
		} catch (Exception e) {
			log.debug("No guardada exportarEjecucion");
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		/* fin auditoria */
	}

	public void detalleMovimiento(AccionWeb accionweb) throws Exception {
		// Collection<Presw18> lista = (Collection<Presw18>)
		// accionweb.getSesion().getAttribute("listaUnidad");
		Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		accionweb.agregarObjeto("cuentasPresupuestarias", lista);
		int item = Util.validaParametro(accionweb.getParameter("item"), 0);
		String unidad = Util.validaParametro(accionweb.getParameter("unidad"),"");
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");		
		int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "","0"));
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		Long presum = Util.validaParametro(accionweb.getParameter("presum"),new Long(0));
		Long usadom = Util.validaParametro(accionweb.getParameter("usadom"),new Long(0));
		Float porc = Util.validaParametro(accionweb.getParameter("porc"),new Float(0));
		Long diferencia = Util.validaParametro(accionweb.getParameter("diferencia"), new Long(0));
		Long preSac = Util.validaParametro(accionweb.getParameter("presac"),new Long(0));
		Long usaDac = Util.validaParametro(accionweb.getParameter("usadac"),new Long(0));
		Float porc2 = Util.validaParametro(accionweb.getParameter("porc2"),new Float(0));
		Long diferencia2 = Util.validaParametro(accionweb.getParameter("diferencia2"), new Long(0));
		int indice = -1;
		indice = unidad.indexOf("-");
		String nomUnidad = unidad.substring(indice + 1, unidad.length());
		int codUnidad = Integer.parseInt(unidad.substring(0, indice).trim());
		Collection<Presw18DTO> listaPresw18 = null;
		// Collection<Presw18> listaPresw18 = null;
		int opcion = Util.validaParametro(accionweb.getParameter("opcion"), 0);
		if(rutUsuario > 0){
		if (item == -1 || item == 0) {
			// listaPresw18 = (Collection<Presw18>)
			// Presw18Collection.generateCollectionPrm(codUnidad, anno, mes);
			PreswBean preswbean = new PreswBean("PRM", codUnidad, 0, anno, mes,	0);
			preswbean.setRutide(rutUsuario);
			preswbean.setDigide(dv);
			listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();

		} else {
			// listaPresw18 = (Collection<Presw18>)
			// Presw18Collection.generateCollectionPid(codUnidad, item, anno);
			PreswBean preswbean = new PreswBean("PID", codUnidad, item, anno,
					mes, 0);
			preswbean.setRutide(rutUsuario);
			preswbean.setDigide(dv);
			listaPresw18 = (Collection<Presw18DTO>) preswbean
					.consulta_presw18();
		}
		}
		// String nomItem = moduloPresupuesto.getItem(item);
		String nomItem = "";
        if(item >= 0){
			descad.cliente.Item itemdto = new descad.cliente.Item();
			itemdto.buscar_item(item);
			nomItem = itemdto.getNombre();
	        }
		accionweb.agregarObjeto("detalleMovimiento", listaPresw18);
		accionweb.agregarObjeto("anno", new Integer(anno));
		accionweb.agregarObjeto("mes", new Integer(mes));
		accionweb.agregarObjeto("codItem", new Integer(item));
		accionweb.agregarObjeto("unidad", new String(nomUnidad));
		accionweb.agregarObjeto("presum", new Long(presum));
		accionweb.agregarObjeto("usadom", new Long(usadom));
		accionweb.agregarObjeto("porc", new Float(porc));
		accionweb.agregarObjeto("diferencia", new Long(diferencia));
		accionweb.agregarObjeto("preSac", new Long(preSac));
		accionweb.agregarObjeto("usaDac", new Long(usaDac));
		accionweb.agregarObjeto("porc2", new Float(porc2));
		accionweb.agregarObjeto("diferencia2", new Long(diferencia2));
		accionweb.agregarObjeto("codUnidad", new Integer(codUnidad));
		accionweb.agregarObjeto("nomItem", nomItem);
		accionweb.agregarObjeto("opcion", opcion);
	/*	descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
		funcionarioBean.buscar_funcionario(rutUsuario);
		String nomSimulacion = funcionarioBean.getNombre();
		accionweb.agregarObjeto("nomSimulacion", nomSimulacion);*/
		
		/* auditoria */
		String host = accionweb.getReq().getLocalAddr();
		int accion = 1;
		DetalleServicio detalleServicio = new DetalleServicio();
		detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(32));
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();

		tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(accion);
		String parametro = "";
		if (item == -1 || item == 0)
			parametro = "PRM";
		else
			parametro = "PID";

		parametro += "&unidad=" + codUnidad + "&item=" + item + "&anno=" + anno
				+ "&mes=" + mes;
		AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
		auditoriaServicio.setDetalleServicio(detalleServicio);
		auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
		auditoriaServicio.setFecha(new Date());
		auditoriaServicio.setHost(host);
		if(rutOrigen > 0)
			parametro = "sim:"+ rutUsuario + "@" + parametro;
	
		auditoriaServicio.setParametro(parametro);
		if(rutOrigen > 0)
			auditoriaServicio.setUsuario(rutOrigen);
		else
			auditoriaServicio.setUsuario(rutUsuario);
		try {
			moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
			log.debug("Si guardada detalleMovimiento");
		} catch (Exception e) {
			log.debug("No guardada detalleMovimiento");
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		/* fin auditoria */
	}

	 synchronized public void exportarDetalleMovimiento(AccionWeb accionweb) throws Exception {
		int item = Util.validaParametro(accionweb.getParameter("item"), 0);
		String unidad = Util.validaParametro(accionweb.getParameter("nomunidad"), "");
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");		
		int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "","0"));
		Long presum = Util.validaParametro(accionweb.getParameter("presum"),new Long(0));
		Long usadom = Util.validaParametro(accionweb.getParameter("usadom"),new Long(0));
		Float porc = Util.validaParametro(accionweb.getParameter("porc"),new Float(0));
		Long diferencia = Util.validaParametro(accionweb.getParameter("diferencia"), new Long(0));
		Long preSac = Util.validaParametro(accionweb.getParameter("preSac"),new Long(0));
		Long usaDac = Util.validaParametro(accionweb.getParameter("usaDac"),new Long(0));
		Float porc2 = Util.validaParametro(accionweb.getParameter("porc2"),	new Float(0));
		Long diferencia2 = Util.validaParametro(accionweb.getParameter("diferencia2"), new Long(0));
		String extension = Util.validaParametro(accionweb.getParameter("extension"), "");
		int codUnidad = Util.validaParametro(accionweb.getParameter("unidad"),0);

		Collection<Presw18DTO> listaPresw18 = null;
		String nomUnidad = codUnidad + " - " + unidad;
		String nomMes = "";
		if (mes > 0) {
			switch (mes) {
			case 1:
				nomMes = "Enero";
				break;
			case 2:
				nomMes = "Febrero";
				break;
			case 3:
				nomMes = "Marzo";
				break;
			case 4:
				nomMes = "Abril";
				break;
			case 5:
				nomMes = "Mayo";
				break;
			case 6:
				nomMes = "Junio";
				break;
			case 7:
				nomMes = "Julio";
				break;
			case 8:
				nomMes = "Agosto";
				break;
			case 9:
				nomMes = "Septiembre";
				break;
			case 10:
				nomMes = "Octubre";
				break;
			case 11:
				nomMes = "Noviembre";
				break;
			case 12:
				nomMes = "Diciembre";
				break;
			}

		}

		// accionweb.agregarObjeto("data",
		// Presw18Collection.generateCollectionPrm(codUnidad, anno, mes));
		if(rutUsuario > 0){
		if (item == -1 || item == 0) {
			// listaPresw18 = (Collection<Presw18>)
			// Presw18Collection.generateCollectionPrm(codUnidad, anno, mes);
			PreswBean preswbean = new PreswBean("PRM", codUnidad, 0, anno, mes,
					0);
			preswbean.setRutide(rutUsuario);
			preswbean.setDigide(dv);
			listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();

		} else {
			// listaPresw18 = (Collection<Presw18>)
			// Presw18Collection.generateCollectionPid(codUnidad, item, anno);
			PreswBean preswbean = new PreswBean("PID", codUnidad, item, anno,mes, 0);
			preswbean.setRutide(rutUsuario);
			preswbean.setDigide(dv);
			listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		}
		Vector vec_datos =new Vector();
		Vector datos =new Vector();
		Long total = new Long(0);
		String fecha ="";
		for (Presw18DTO ss : listaPresw18){
		 	   total = total + ss.getValdoc();
        	   fecha = ss.getFecdoc() + "";
        	   String diaFecha = "";
        	   String mesFecha = "";
        	   String annoFecha = "";
        	   if(fecha.length() == 8){
        	    diaFecha = fecha.substring(0,2);
        	    mesFecha = fecha.substring(2,4);
        	    annoFecha = fecha.substring(4);
        	   } else {
        	    diaFecha = "0" + fecha.substring(0,1);
        	    mesFecha = fecha.substring(1,3);
        	    annoFecha = fecha.substring(3);
        	   }
        	   datos =new Vector();
        	   if (item == -1 || item == 0) 
        		   datos.addElement(ss.getItedoc()+" - "+ss.getDesite());
        	   else
        		   datos.addElement("");
        	   datos.addElement(ss.getNomtip());
        	   datos.addElement(ss.getNumdoc());
        	   datos.addElement(diaFecha+"/"+mesFecha+"/"+annoFecha);
        	   datos.addElement(ss.getNompro());
        	   datos.addElement(ss.getValdoc());
        	   vec_datos.addElement(datos);
        
			
		}
	   datos =new Vector();
   	   datos.addElement("");
   	   datos.addElement("");
   	   datos.addElement("");
   	   datos.addElement("");
   	   datos.addElement("Total");
   	   datos.addElement(total);
   	   vec_datos.addElement(datos);
		moduloPresupuesto.agregaMovDisponibleExcel(accionweb.getReq(), vec_datos);
			
		}
		accionweb.agregarObjeto("data", listaPresw18);

		accionweb.agregarObjeto("anno", new Integer(anno));
		accionweb.agregarObjeto("mes", new String(nomMes));
		accionweb.agregarObjeto("codItem", new Integer(item));
		// MB 18/06/2012 accionweb.agregarObjeto("unidad", new String(nomUnidad));
		accionweb.agregarObjeto("unidad", new String(unidad));
		accionweb.agregarObjeto("presum", new Long(presum));
		accionweb.agregarObjeto("usadom", new Long(usadom));
		accionweb.agregarObjeto("porc", new Float(porc));
		accionweb.agregarObjeto("diferencia", new Long(diferencia));
		accionweb.agregarObjeto("preSac", new Long(preSac));
		accionweb.agregarObjeto("usaDac", new Long(usaDac));
		accionweb.agregarObjeto("porc2", new Float(porc2));
		accionweb.agregarObjeto("diferencia2", new Long(diferencia2));
		accionweb.agregarObjeto("codUnidad", new Integer(codUnidad));
		accionweb.agregarObjeto("format", extension);

		/* auditoria */
		String host = accionweb.getReq().getLocalAddr();
		int accion = 1;
		if (extension.trim().equals("pdf"))
			accion = 4;
		else
			accion = 3;
		DetalleServicio detalleServicio = new DetalleServicio();
		detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(32));
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();

		tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(accion);
		String parametro = "";
		if (item == -1 || item == 0)
			parametro = "PRM";
		else
			parametro = "PID";

		parametro += "&unidad=" + codUnidad + "&item=" + item + "&anno=" + anno
				+ "&mes=" + mes;
		AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
		auditoriaServicio.setDetalleServicio(detalleServicio);
		auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
		auditoriaServicio.setFecha(new Date());
		auditoriaServicio.setHost(host);
		if(rutOrigen > 0)
			parametro = "sim:"+ rutUsuario + "@" + parametro;
	
		auditoriaServicio.setParametro(parametro);
		if(rutOrigen > 0)
			auditoriaServicio.setUsuario(rutOrigen);
		else
			auditoriaServicio.setUsuario(rutUsuario);
		try {
			moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
			log.debug("Si guardada exportarDetalleMovimiento");
		} catch (Exception e) {
			log.debug("No guardada exportarDetalleMovimiento");
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		/* fin auditoria */
	}

	public void cargaEjecucion(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
	//	Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int opcionEjec = Util.validaParametro(accionweb.getParameter("opcionEjec"), 0);
		String tipoCuentas = Util.validaParametro(accionweb.getParameter("tipoCuentas"), " ");
		accionweb.agregarObjeto("tipoCuentas", tipoCuentas);
		Item items = new Item();
		Collection<Item> listaItem = (Collection<Item>) items.todos_items();
		GregorianCalendar hoy = new GregorianCalendar(); 
		int mesActual = hoy.get(Calendar.MONTH) + 1;
		int anoActual = hoy.get(Calendar.YEAR);
		
		
		/*esto es para el menu de control presupuestario*/
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		accionweb.getSesion().setAttribute("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		/* fin */
		
		String selecUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"), "");
		accionweb.agregarObjeto("cuentasPresupuestarias", lista);
	    
        if(!selecUnidad.trim().equals("-1")){
        	accionweb.agregarObjeto("selecUnidad", selecUnidad);
        	int ind = 0;
        	String nomUnidad = "";
        	String saldo = "";
        	if(selecUnidad.indexOf("@") > 0){
        		ind = selecUnidad.indexOf("@");
        		if(ind > 0)
        			nomUnidad = selecUnidad.substring(0,ind).trim();
        		selecUnidad = selecUnidad.substring(ind+1);
        		accionweb.agregarObjeto("codUnidad", nomUnidad);
        		
        	}
        	if(selecUnidad.indexOf("@") > 0){
        		ind = selecUnidad.indexOf("@");
        		if(ind > 0)
        			nomUnidad += " - " + selecUnidad.substring(0,ind).trim();
        		selecUnidad = selecUnidad.substring(ind+1).trim();
        		
        	}
        	saldo = selecUnidad;
        	
        	
        	accionweb.agregarObjeto("nomUnidad", nomUnidad);
        	accionweb.agregarObjeto("saldo", saldo);
         	
        	
        }
		
		
		accionweb.agregarObjeto("listaItem", listaItem);
		accionweb.agregarObjeto("cuentasPresupuestarias", lista);
		accionweb.agregarObjeto("unidadSeleccionada", unidad);
		accionweb.agregarObjeto("opcion", "2");
		if(mes == 0)
			accionweb.agregarObjeto("mes", mesActual);
		else accionweb.agregarObjeto("mes", mes);
		accionweb.agregarObjeto("annoPar", anno);
	    accionweb.agregarObjeto("ejecucion", opcionEjec);	
	    accionweb.agregarObjeto("annoselec", anoActual);	
	
	/*	descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
		funcionarioBean.buscar_funcionario(rutUsuario);
		String nomSimulacion = funcionarioBean.getNombre();
		accionweb.agregarObjeto("nomSimulacion", nomSimulacion);*/
	}

	public void cargaConsultaDocumento(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		//Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		int opcion = Util.validaParametro(accionweb.getParameter("opcion"), 0);
		/*Date hoy = new Date(); 
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        String fechaActual = formatoFecha.format(hoy);*/

		/*esto es para el menu de control presupuestario*/
		String tipoCuentas = Util.validaParametro(accionweb.getParameter("tipoCuentas"), " ");
		accionweb.agregarObjeto("tipoCuentas", tipoCuentas);
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		accionweb.getSesion().setAttribute("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		/* fin */
	
		String selecUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"), "");
			    
        if(!selecUnidad.trim().equals("-1")){
        	accionweb.agregarObjeto("selecUnidad", selecUnidad);
        	int ind = 0;
        	String nomUnidad = "";
        	String saldo = "";
        	if(selecUnidad.indexOf("@") > 0){
        		ind = selecUnidad.indexOf("@");
        		if(ind > 0)
        			nomUnidad = selecUnidad.substring(0,ind);
        		selecUnidad = selecUnidad.substring(ind+1);
        		accionweb.agregarObjeto("codUnidad", nomUnidad.trim());
        		
        	}
        	if(selecUnidad.indexOf("@") > 0){
        		ind = selecUnidad.indexOf("@");
        		if(ind > 0)
        			nomUnidad += " - " + selecUnidad.substring(0,ind).trim();
        		selecUnidad = selecUnidad.substring(ind+1);
        		
        	}
        	saldo = selecUnidad;
        	
        	
        	accionweb.agregarObjeto("nomUnidad", nomUnidad);
        	/*accionweb.agregarObjeto("fechaActual", fechaActual);
        	accionweb.agregarObjeto("saldo", saldo);*/
         	
        	
        }
		
		accionweb.agregarObjeto("cuentasPresupuestarias", lista);
		 accionweb.agregarObjeto("unidadSeleccionada", unidad);
		if (opcion == 4)
		  accionweb.agregarObjeto("facturacion", "1");
		else if (opcion == 5)
		       accionweb.agregarObjeto("importacion", "1");
		else if (opcion == 6)
			   accionweb.agregarObjeto("cheque", "1");
		
	}

	public void cargaResultadoEjecucion(AccionWeb accionweb) throws Exception {
		/*PAI ejecuci�n por item
		 * ERE ejecuci�n por remuneraci�n
		 * LMP ejecuci�n por movimientos */
		Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		accionweb.agregarObjeto("cuentasPresupuestarias", lista);

		String unidad = Util.validaParametro(accionweb.getParameter("unidad"),"");
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		String estamento = Util.validaParametro(accionweb.getParameter("estamento"), "");
		String parametro = Util.validaParametro(accionweb.getParameter("parametro"), "");
	   	int indice = -1;
		indice = unidad.indexOf("@");
		String parametroServicio = "";
		int codUnidad = Integer.parseInt(unidad.substring(0, indice).trim());
		String nomUnidad = unidad.substring(indice+1).trim();
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "","0"));
		String tipoCuentas = Util.validaParametro(accionweb.getParameter("tipoCuentas"), " ");
		accionweb.agregarObjeto("tipoCuentas", tipoCuentas);
		String nomMes = "";
		
		try {
		if(rutUsuario > 0){
		if (codUnidad > 0) {
			if(parametro.trim().equals("item")){
				Collection<Presw18DTO> listaPresw18 = null;
				PreswBean preswbean = new PreswBean("PAI", codUnidad, 0, anno, mes,	0);
				preswbean.setRutide(rutUsuario);
				preswbean.setDigide(dv);
				listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		
				if (listaPresw18.size() == 0)
					listaPresw18 = null;
			    accionweb.agregarObjeto("detalleEjecucion", listaPresw18);
			    parametroServicio = "PAI";
			    int sumpresum = 0;
                int sumusadom = 0;
                int sumporc = 0;
                int sumpresac = 0;
                int sumusadac = 0;
                int sumporc2 = 0;
                int sumporc3 = 0;
                int sumpreano = 0;
                int porc = 0;
                int porc2 = 0;
                int porc3 = 0;              
                
            
                MathTool mathtool = new MathTool();
                Vector vec_datos = new Vector();
                
                 
                for (Presw18DTO ss : listaPresw18){
                       if(ss.getPresum() != 0 && ss.getUsadom() != 0){
                    		porc = mathtool.round(mathtool.mul(mathtool.div(ss.getUsadom() ,ss.getPresum()),100));                   	            
                     
                       } else
                    		porc = 0;
                    
                    if(ss.getUsadac() != 0  && ss.getPresac() != 0)
                    	porc2 = mathtool.round(mathtool.mul(mathtool.div(ss.getUsadac() ,ss.getPresac()),100));   
                     else
                        porc2 = 0;
                	
                	if(ss.getUsadac()!= 0  && ss.getPreano() != 0)
                		porc3 = mathtool.round(mathtool.mul(mathtool.div(ss.getUsadac() ,ss.getPreano()),100));                   	            
                    else
                        porc3 = 0;
           
               
                	
                	 Vector datos = new Vector();
	                	 datos.addElement(ss.getItedoc()+" - " + ss.getDesite());
	                	 datos.addElement(ss.getPresum());
	                	 datos.addElement(ss.getUsadom());
	                	 datos.addElement(porc);
	                	 datos.addElement(ss.getPresac());
	                	 datos.addElement(ss.getUsadac());
	                	 datos.addElement(porc2);
	                	 datos.addElement(ss.getPreano());
	                	 datos.addElement(porc3);
	                	 vec_datos.addElement(datos);
	                	 
                }
                moduloPresupuesto.agregaEjecucionExcel(accionweb.getReq(), vec_datos);
                	
			} else if(parametro.trim().equals("remuneracion")){
				  Collection<Presw21DTO> listaPresw21 = null;
				 	PreswBean preswbean = new PreswBean("ERE", codUnidad, 0, anno, 0,	0,0, "", 0, "", "", 0, 0, 0, 0, "", estamento);
					preswbean.setRutide(rutUsuario);
					preswbean.setDigide(dv);
					listaPresw21 = (Collection<Presw21DTO>) preswbean.consulta_presw21();
					
					if (listaPresw21.size() == 0)
						listaPresw21 = null;
					accionweb.agregarObjeto("detalleEjecucion", listaPresw21);
					parametroServicio = "ERE";
			} else  if(parametro.trim().equals("movimiento")){
					int mesInicio = Util.validaParametro(accionweb.getParameter("mesInicio"), 0);
					int mesTermino = Util.validaParametro(accionweb.getParameter("mesTermino"), 0);
				    Collection<Presw18DTO> listaPresw18 = null;
				 	PreswBean preswbean = new PreswBean("LMP", codUnidad, 0, anno, 0,	0);
				 	preswbean.setSucur(1);// siempre va 1 PP
				 	preswbean.setMespar(mesInicio);
				 	preswbean.setItedoc(mesTermino);
				//preswbean.setRutide(rutUsuario);
				//	preswbean.setDigide(dv);
					listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
					
					if (listaPresw18.size() == 0)
						listaPresw18 = null;
					accionweb.agregarObjeto("detalleEjecucion", listaPresw18);
					parametroServicio = "LMP";
			}
		}
		}
		if (mes > 0) {
			switch (mes) {
			case 1:
				nomMes = "Enero";
				break;
			case 2:
				nomMes = "Febrero";
				break;
			case 3:
				nomMes = "Marzo";
				break;
			case 4:
				nomMes = "Abril";
				break;
			case 5:
				nomMes = "Mayo";
				break;
			case 6:
				nomMes = "Junio";
				break;
			case 7:
				nomMes = "Julio";
				break;
			case 8:
				nomMes = "Agosto";
				break;
			case 9:
				nomMes = "Septiembre";
				break;
			case 10:
				nomMes = "Octubre";
				break;
			case 11:
				nomMes = "Noviembre";
				break;
			case 12:
				nomMes = "Diciembre";
				break;
			}
		}
	} catch (Exception e) {
		log.debug("No carga cargaResultadoEjecucion");
		accionweb.agregarObjeto("mensaje", "No carga cargaResultadoEjecucion ");
		accionweb.agregarObjeto("exception", e.toString());
		e.printStackTrace();
	}
		accionweb.agregarObjeto("mes", mes);
		accionweb.agregarObjeto("annoPar", anno);
		accionweb.agregarObjeto("codUnidad", codUnidad);
		accionweb.agregarObjeto("nomUnidad", nomUnidad);
		accionweb.agregarObjeto("estamento", estamento);
		accionweb.agregarObjeto("nomMes", nomMes);
		/* auditoria */
		String host = accionweb.getReq().getLocalAddr();
		int accion = 1;
		DetalleServicio detalleServicio = new DetalleServicio();
		detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(33));
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();

		tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(accion);
		

		parametroServicio += "&unidad=" + codUnidad + "&item=0" + "&anno=" + anno
				+ "&mes=" + mes;
		AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
		auditoriaServicio.setDetalleServicio(detalleServicio);
		auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
		auditoriaServicio.setFecha(new Date());
		auditoriaServicio.setHost(host);
		if(rutOrigen > 0)
			parametroServicio = "sim:"+ rutUsuario + "@" + parametroServicio;
	
		auditoriaServicio.setParametro(parametroServicio);
		if(rutOrigen > 0)
			auditoriaServicio.setUsuario(rutOrigen);
		else
			auditoriaServicio.setUsuario(rutUsuario);
		try {
			moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
			log.debug("Si guardada cargaResultadoEjecucion");
		} catch (Exception e) {
			log.debug("No guardada cargaResultadoEjecucion");
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		/* fin auditoria */
	}
	
	
	public void cargaResultadoEjecMensual(AccionWeb accionweb) throws Exception {
		/*ETR TRIMESTRAL */
		Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		accionweb.agregarObjeto("cuentasPresupuestarias", lista);

		String unidad = Util.validaParametro(accionweb.getParameter("unidad"),"");
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		//int codUnidad = Util.validaParametro(accionweb.getParameter("codUnidad"), 0);
		int indice = -1;
		indice = unidad.indexOf("@");
		int codUnidad = Integer.parseInt(unidad.substring(0, indice).trim());
	
		String parametroServicio = "";
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "","0"));
		if(rutUsuario > 0){
			if (codUnidad > 0) {
			        Collection<Presw21DTO> listaPresw21 = null;
				 	PreswBean preswbean = new PreswBean("ETR", codUnidad, 0, anno, mes,	0,0, "", 0, "", "", 0, 0, 0, 0, "", "");
				 	preswbean.setRutide(rutUsuario);
					preswbean.setDigide(dv);
					listaPresw21 = (Collection<Presw21DTO>) preswbean.consulta_presw21();
					
					if (listaPresw21.size() == 0)
						listaPresw21 = null;
					accionweb.agregarObjeto("detalleMensual", listaPresw21);
					parametroServicio = "ETR";
			}
		}
		accionweb.agregarObjeto("mes", mes);
		accionweb.agregarObjeto("annoPar", anno);
		accionweb.agregarObjeto("codUnidad", codUnidad);
		accionweb.agregarObjeto("nomUnidad", codUnidad + " - " + unidad.substring(indice+1).trim());
		accionweb.agregarObjeto("ejecucion", 4);
		/* auditoria */
		String host = accionweb.getReq().getLocalAddr();
		int accion = 1;
		DetalleServicio detalleServicio = new DetalleServicio();
		detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(33));
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();

		tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(accion);
		

		parametroServicio += "&unidad=" + codUnidad + "&item=0" + "&anno=" + anno
				+ "&mes=" + mes;
		AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
		auditoriaServicio.setDetalleServicio(detalleServicio);
		auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
		auditoriaServicio.setFecha(new Date());
		auditoriaServicio.setHost(host);
		if(rutOrigen > 0)
			parametroServicio = "sim:"+ rutUsuario + "@" + parametroServicio;
	
		auditoriaServicio.setParametro(parametroServicio);
		if(rutOrigen > 0)
			auditoriaServicio.setUsuario(rutOrigen);
		else
			auditoriaServicio.setUsuario(rutUsuario);
		try {
			moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
			log.debug("Si guardada cargaResultadoEjecucion");
		} catch (Exception e) {
			log.debug("No guardada cargaResultadoEjecucion");
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		/* fin auditoria */
	}
	public void detalleEjecucion(AccionWeb accionweb) throws Exception {
		// Collection<Presw18> lista = (Collection<Presw18>)
		// accionweb.getSesion().getAttribute("listaUnidad");
		/*PID detalle de ejecuci�n por item*/
	    String estamento = Util.validaParametro(accionweb.getParameter("estamento"), "");
		String parametro = Util.validaParametro(accionweb.getParameter("parametro"), "");
	 
		Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb
				.getSesion().getAttribute("listaUnidad");
		accionweb.agregarObjeto("cuentasPresupuestarias", lista);
		int item = Util.validaParametro(accionweb.getParameter("item"), 0);
		String unidad = Util.validaParametro(accionweb.getParameter("unidad"), "");
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		Long presum = Util.validaParametro(accionweb.getParameter("presum"),
				new Long(0));
		Long usadom = Util.validaParametro(accionweb.getParameter("usadom"),
				new Long(0));
		Float porc = Util.validaParametro(accionweb.getParameter("porc"),
				new Float(0));
		Long preSac = Util.validaParametro(accionweb.getParameter("preSac"),
				new Long(0));
		Long usaDac = Util.validaParametro(accionweb.getParameter("usaDac"),
				new Long(0));
		Float porc2 = Util.validaParametro(accionweb.getParameter("porc2"),
				new Float(0));
		Float porc3 = Util.validaParametro(accionweb.getParameter("porc3"),
				new Float(0));
		Long preAno = Util.validaParametro(accionweb.getParameter("preAno"),
				new Long(0));
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "","0"));
		
		descad.cliente.Item itemdto = new descad.cliente.Item();
		itemdto.buscar_item(item);
		String nomItem = itemdto.getNombre();
		Collection<Presw18DTO> listaPresw18 = null;
		// Collection<Presw18> listaPresw18 = null;

		int indice = -1;
		indice = unidad.indexOf("@");
		String nomUnidad = unidad.substring(0, indice).trim() + " - "
				+ unidad.substring(indice + 1, unidad.length()).trim();
		int codUnidad = Integer.parseInt(unidad.substring(0, indice).trim());
		String nomMes = "";
		if (mes > 0) {
			switch (mes) {
			case 1:
				nomMes = "Enero";
				break;
			case 2:
				nomMes = "Febrero";
				break;
			case 3:
				nomMes = "Marzo";
				break;
			case 4:
				nomMes = "Abril";
				break;
			case 5:
				nomMes = "Mayo";
				break;
			case 6:
				nomMes = "Junio";
				break;
			case 7:
				nomMes = "Julio";
				break;
			case 8:
				nomMes = "Agosto";
				break;
			case 9:
				nomMes = "Septiembre";
				break;
			case 10:
				nomMes = "Octubre";
				break;
			case 11:
				nomMes = "Noviembre";
				break;
			case 12:
				nomMes = "Diciembre";
				break;
			}
		}
		if(rutUsuario > 0){
		if (codUnidad > 0) {
			// listaPresw18 = (Collection<Presw18>)
			// Presw18Collection.generateCollectionPai(codUnidad, item, anno);
			PreswBean preswbean = new PreswBean("PID", codUnidad, item, anno,
					mes, 0);
			preswbean.setRutide(rutUsuario);
			preswbean.setDigide(dv);
			listaPresw18 = (Collection<Presw18DTO>) preswbean
					.consulta_presw18();
		}
		}
		accionweb.agregarObjeto("detalleEjecucion", listaPresw18);
		accionweb.agregarObjeto("anno", new Integer(anno));
		accionweb.agregarObjeto("mes", new Integer(mes));
		accionweb.agregarObjeto("codItem", new Integer(item));
		accionweb.agregarObjeto("unidad", new String(nomUnidad));
		accionweb.agregarObjeto("presum", new Long(presum));
		accionweb.agregarObjeto("usadom", new Long(usadom));
		accionweb.agregarObjeto("porc", new Float(porc));
		accionweb.agregarObjeto("preSac", new Long(preSac));
		accionweb.agregarObjeto("usaDac", new Long(usaDac));
		accionweb.agregarObjeto("porc2", new Float(porc2));
		accionweb.agregarObjeto("porc3", new Float(porc3));
		accionweb.agregarObjeto("preAno", new Long(preAno));
		accionweb.agregarObjeto("codUnidad", new Integer(codUnidad));
		accionweb.agregarObjeto("nomItem", new String(nomItem));
		accionweb.agregarObjeto("nomMes", new String(nomMes));
	/*	descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
		funcionarioBean.buscar_funcionario(rutUsuario);
		String nomSimulacion = funcionarioBean.getNombre();
		accionweb.agregarObjeto("nomSimulacion", nomSimulacion);*/

		/* auditoria */
		String host = accionweb.getReq().getLocalAddr();
		int accion = 1;
		DetalleServicio detalleServicio = new DetalleServicio();
		detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(34));
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();

		tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(accion);
		String parametroServicio = "PID";

		parametroServicio += "&unidad=" + codUnidad + "&item=" + item + "&anno=" + anno
				+ "&mes=" + mes;
		AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
		auditoriaServicio.setDetalleServicio(detalleServicio);
		auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
		auditoriaServicio.setFecha(new Date());
		auditoriaServicio.setHost(host);
		if(rutOrigen > 0)
			parametroServicio = "sim:"+ rutUsuario + "@" + parametroServicio;
	
		auditoriaServicio.setParametro(parametroServicio);
		if(rutOrigen > 0)
			auditoriaServicio.setUsuario(rutOrigen);
		else
			auditoriaServicio.setUsuario(rutUsuario);
		try {
			moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
			log.debug("Si guardada detalleEjecucion");
		} catch (Exception e) {
			log.debug("No guardada detalleEjecucion");
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		/* fin auditoria */
	}
	public void detalleRemuneracionEjec(AccionWeb accionweb) throws Exception {
		/*ERD detalle de ejecuci�n por remuneracion*/
	    String estamento = Util.validaParametro(accionweb.getParameter("estamento"), "");
	    String nomMes = Util.validaParametro(accionweb.getParameter("nomMes"), "");
	    String nomUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"), "");
		int mes = Util.validaParametro(accionweb.getParameter("numMes"), 0);
	    int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
	    int codUnidad = Util.validaParametro(accionweb.getParameter("codUnidad"), 0); 
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "","0"));
		
		
		Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		accionweb.agregarObjeto("cuentasPresupuestarias", lista);
		Collection<Presw21DTO> listaPresw21 = null;
		if(rutUsuario > 0){
		if (codUnidad > 0) {
			PreswBean preswbean = new PreswBean("ERD", codUnidad, 0, anno, mes,	0,0, "", 0, "", "", 0, 0, 0, 0, "", estamento);
			preswbean.setRutide(rutUsuario);
			preswbean.setDigide(dv);
			listaPresw21 = (Collection<Presw21DTO>) preswbean.consulta_presw21();
		
		}
		}
		
		String nomEstamento = "";
		if (estamento.equals("T")) nomEstamento = "Todos";
		if (estamento.equals("A")) nomEstamento = "Acad�mico";
		if (estamento.equals("N")) nomEstamento = "No Planta";
		if (estamento.equals("M")) nomEstamento = "Administrativo";
		if (estamento.equals("O")) nomEstamento = "Operarios";
		if (estamento.equals("H")) nomEstamento = "Honorarios";
		if (estamento.equals("Y")) nomEstamento = "Ayudant�as";
		if (estamento.equals("D")) nomEstamento = "Docentes";
	
		

		accionweb.agregarObjeto("detalleEjecucionRemuneracion", listaPresw21);
		accionweb.agregarObjeto("anno", new Integer(anno));
		accionweb.agregarObjeto("unidad", new String(nomUnidad));
		accionweb.agregarObjeto("estamento", nomEstamento);
		accionweb.agregarObjeto("nomMes", new String(nomMes));
		accionweb.agregarObjeto("codUnidad", new Integer(codUnidad));

		/* auditoria */
		String host = accionweb.getReq().getLocalAddr();
		int accion = 1;
		DetalleServicio detalleServicio = new DetalleServicio();
		detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(34));
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();

		tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(accion);
		String parametroServicio = "ERD";

		parametroServicio += "&unidad=" + codUnidad + "&anno=" + anno
				+ "&mes=" + mes + "&estamento=" + estamento;
		AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
		auditoriaServicio.setDetalleServicio(detalleServicio);
		auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
		auditoriaServicio.setFecha(new Date());
		auditoriaServicio.setHost(host);
		if(rutOrigen > 0)
			parametroServicio = "sim:"+ rutUsuario + "@" + parametroServicio;
	
		auditoriaServicio.setParametro(parametroServicio);
		if(rutOrigen > 0)
			auditoriaServicio.setUsuario(rutOrigen);
		else
			auditoriaServicio.setUsuario(rutUsuario);
		try {
			moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
			log.debug("Si guardada detalleEjecucion");
		} catch (Exception e) {
			log.debug("No guardada detalleEjecucion");
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		/* fin auditoria */
	}
	 synchronized public void exportarDetalleEjecucion(AccionWeb accionweb) throws Exception {
		Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		accionweb.agregarObjeto("cuentasPresupuestarias", lista);
		int item = Util.validaParametro(accionweb.getParameter("item"), 0);
		String unidad = Util.validaParametro(accionweb
				.getParameter("nomunidad"), "");
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		Long presum = Util.validaParametro(accionweb.getParameter("presum"),
				new Long(0));
		Long usadom = Util.validaParametro(accionweb.getParameter("usadom"),
				new Long(0));
		Float porc = Util.validaParametro(accionweb.getParameter("porc"),
				new Float(0));
		Long preSac = Util.validaParametro(accionweb.getParameter("preSac"),
				new Long(0));
		Long usaDac = Util.validaParametro(accionweb.getParameter("usaDac"),
				new Long(0));
		Float porc2 = Util.validaParametro(accionweb.getParameter("porc2"),
				new Float(0));
		Float porc3 = Util.validaParametro(accionweb.getParameter("porc3"),
				new Float(0));
		Long preAno = Util.validaParametro(accionweb.getParameter("preAno"),
				new Long(0));
		int codUnidad = Util.validaParametro(accionweb.getParameter("unidad"),
				0);
		String extension = Util.validaParametro(accionweb
				.getParameter("extension"), "");
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "","0"));
		

		descad.cliente.Item itemdto = new descad.cliente.Item();
		itemdto.buscar_item(item);
		String nomItem = itemdto.getNombre();
		Collection<Presw18DTO> listaPresw18 = null;

		String nomUnidad = unidad;
		String nomMes = "";
		if (mes > 0) {
			switch (mes) {
			case 1:
				nomMes = "Enero";
				break;
			case 2:
				nomMes = "Febrero";
				break;
			case 3:
				nomMes = "Marzo";
				break;
			case 4:
				nomMes = "Abril";
				break;
			case 5:
				nomMes = "Mayo";
				break;
			case 6:
				nomMes = "Junio";
				break;
			case 7:
				nomMes = "Julio";
				break;
			case 8:
				nomMes = "Agosto";
				break;
			case 9:
				nomMes = "Septiembre";
				break;
			case 10:
				nomMes = "Octubre";
				break;
			case 11:
				nomMes = "Noviembre";
				break;
			case 12:
				nomMes = "Diciembre";
				break;
			}
		}
		if(rutUsuario > 0){
		if (codUnidad > 0) {
			PreswBean preswbean = new PreswBean("PID", codUnidad, item, anno,
					mes, 0);
			preswbean.setRutide(rutUsuario);
			preswbean.setDigide(dv);
			listaPresw18 = (Collection<Presw18DTO>) preswbean
					.consulta_presw18();
			if(listaPresw18 != null){
				Vector  vec_datos = new Vector();
				Vector  datos = new Vector();
				Long suma = new Long(0);
				String fecha = "";
				String diaFecha = "";
				String mesFecha = "";
				String annoFecha = "";
				for (Presw18DTO ss : listaPresw18){
			    	suma = suma + ss.getValdoc();
                	fecha = ss.getFecdoc()+"";
                	diaFecha = "";
                	mesFecha = "";
                	annoFecha = "";
                	if(fecha.length() == 8){
                	  diaFecha = fecha.substring(0,2);
                	  mesFecha = fecha.substring(2,4);
                	  annoFecha = fecha.substring(4);
                	} else {
                	   diaFecha = "0" + fecha.substring(0,1);
                	   mesFecha = fecha.substring(1,3);
                	   annoFecha = fecha.substring(3);
                	}
                	datos = new Vector();
                	datos.addElement(ss.getNomtip());
                	datos.addElement(ss.getNumdoc());
                	datos.addElement(diaFecha+"/"+mesFecha+"/"+annoFecha);
                	datos.addElement(ss.getNompro());
                	datos.addElement(ss.getValdoc());  
                	vec_datos.addElement(datos);
				}
				datos = new Vector();
            	datos.addElement("TOTAL");
            	datos.addElement("");
            	datos.addElement("");
            	datos.addElement("");
            	datos.addElement(suma);
            	vec_datos.addElement(datos);
            	moduloPresupuesto.agregaMovEjecucionExcel(accionweb.getReq(), vec_datos);
			}
		}
		}
		accionweb.agregarObjeto("data", listaPresw18);
		accionweb.agregarObjeto("anno", new Integer(anno));
		accionweb.agregarObjeto("mes", new String(nomMes));
		accionweb.agregarObjeto("codItem", new Integer(item));
		accionweb.agregarObjeto("unidad", new String(nomUnidad));
		accionweb.agregarObjeto("presum", new Long(presum));
		accionweb.agregarObjeto("usadom", new Long(usadom));
		accionweb.agregarObjeto("porc", new Float(porc));
		accionweb.agregarObjeto("preSac", new Long(preSac));
		accionweb.agregarObjeto("usaDac", new Long(usaDac));
		accionweb.agregarObjeto("porc2", new Float(porc2));
		accionweb.agregarObjeto("porc3", new Float(porc3));
		accionweb.agregarObjeto("preAno", new Long(preAno));
		accionweb.agregarObjeto("codUnidad", new Integer(codUnidad));
		accionweb.agregarObjeto("nomItem", new String(nomItem));
		accionweb.agregarObjeto("nomMes", new String(nomMes));
		accionweb.agregarObjeto("format", extension);
		/* auditoria */
		String host = accionweb.getReq().getLocalAddr();
		int accion = 1;
		if (extension.trim().equals("pdf"))
			accion = 4;
		else
			accion = 3;
		DetalleServicio detalleServicio = new DetalleServicio();
		detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(34));
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();

		tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(accion);
		String parametro = "PID";

		parametro += "&unidad=" + codUnidad + "&item=" + item + "&anno=" + anno
				+ "&mes=" + mes;
		AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
		auditoriaServicio.setDetalleServicio(detalleServicio);
		auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
		auditoriaServicio.setFecha(new Date());
		auditoriaServicio.setHost(host);
		if(rutOrigen > 0)
			parametro = "sim:"+ rutUsuario + "@" + parametro;
	
		auditoriaServicio.setParametro(parametro);
		if(rutOrigen > 0)
			auditoriaServicio.setUsuario(rutOrigen);
		else
			auditoriaServicio.setUsuario(rutUsuario);
		try {
			moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
			log.debug("Si guardada detalleEjecucion");
		} catch (Exception e) {
			log.debug("No guardada detalleEjecucion");
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		/* fin auditoria */
	}
	 
	 
	public void cargaPresup(AccionWeb accionweb) throws Exception {
		//04/10/2016 KP - Se cambian nombres de variables y etiquetas 'unidad(es)' por 'organizacion(es)'		
		//Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaOrganizacion");
		//int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		List<Presw25DTO> resultadoPresup  = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		List<Presw25DTO> resultadoCPR = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoCPR");
		int a�o =Integer.parseInt( Util.validaParametro(accionweb.getSesion().getAttribute("anno")+"","0"));
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		//int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"), "");
		
		int volver = Util.validaParametro(accionweb.getParameter("volver"), 0);
		// int itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), 0);	
		String itedoc = Util.validaParametro(accionweb.getParameter("itedoc"),"");	
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		int menu = Util.validaParametro(accionweb.getParameter("menu"), 0);
		Long tope = new Long(0);
		/*esto es para el menu de control presupuestario*/
	    int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		accionweb.getSesion().setAttribute("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		/* fin */
		
		
	//	if(a�o == 0) a�o = 2009;
		List<Presw25DTO> presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
	 	List<Presw25DTO> resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
	 	
	 //	List<Presw25DTO> resultadoPresup  = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
	 	if(resultadoPresup != null){
	 		for (Presw25DTO ss : resultadoPresup){
 	 			//if(ss.getCoduni() != unidad){
 	 			if(!ss.getMotiv1().trim().equals(organizacion.trim())){
 	 		 		accionweb.getSesion().removeAttribute("resultadoPresup");
	 				if(accionweb.getSesion().getAttribute("Original") != null)
	 					accionweb.getSesion().removeAttribute("Original");
	 				if(presupItemreq != null )
	 					accionweb.getSesion().removeAttribute("presupItemreq");
	 				if(resultadoDetalleItemReq != null && volver != 1)
	 					accionweb.getSesion().removeAttribute("resultadoDetalleItemReq");
	 				if(accionweb.getSesion().getAttribute("resultadoCPR") != null)
	 					accionweb.getSesion().removeAttribute("resultadoCPR");
	 				break;
	 			}
	 		}
	 	    	
	 	}
		
		List<Presw25DTO> listaPRESW25 = new ArrayList<Presw25DTO>();
		boolean consulta = true;
		if(rutUsuario > 0){
		if(resultadoCPR != null && resultadoCPR.size() > 0 && menu == 0){
			for (Presw25DTO ss : resultadoCPR){
				if(ss.getAnopre() == a�o && ss.getRutide() == rutUsuario){
					consulta = false;
				    listaPRESW25 = resultadoCPR;
				    break;
				}
			}
			
		}
		if (consulta){
		PreswBean preswbean = new PreswBean("CPR", 0,0,a�o,0, rutUsuario);
		preswbean.setDigide(dv);
		if(preswbean != null) {
		listaPRESW25 = (List<Presw25DTO>) preswbean.consulta_presw25();
	/*	for (Presw25DTO ss : listaPRESW25){
			System.out.println("CPR"+ss.getIndexi());
		}*/
 	 		
		}
		}
		}
		if(listaPRESW25 == null || listaPRESW25.size() == 0)
			accionweb.agregarObjeto("mensaje", "No tiene permitido Presupuestaci�n");
		else {
            moduloPresupuesto.agregaResultadoCPRSesion(accionweb.getReq(), listaPRESW25);
            listaPRESW25 = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoCPR");
           /* for (Presw25DTO ss : listaPRESW25){
    			System.out.println(" 2 "+ss.getIndexi());
    		}*/
		}
		
		//accionweb.agregarObjeto("unidadSeleccionada", unidad);
		//accionweb.getMav().addObject("unidadesPresup", listaPRESW25);
		accionweb.agregarObjeto("organizacionSeleccionada", organizacion.trim());
		accionweb.agregarObjeto("cuentasPresupuestarias", lista);
		accionweb.getMav().addObject("organizacionesPresup", listaPRESW25);
		accionweb.agregarObjeto("anno", a�o); 
		accionweb.agregarObjeto("volver", volver);
		accionweb.agregarObjeto("itedoc", itedoc);
		accionweb.agregarObjeto("control", control);
	/*	descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
		funcionarioBean.buscar_funcionario(rutUsuario);
		String nomSimulacion = funcionarioBean.getNombre();
		accionweb.agregarObjeto("nomSimulacion", nomSimulacion);*/
		
	}
	
		
/* SE ELIMIN� LA OPCI�N DE SOLICITUD DE VACANTE 24/09/2010
 * 	public void cargaResultadoPresupVacante(AccionWeb accionweb)	throws Exception {
		int rutUsuario =Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String cargo = Util.validaParametro(accionweb.getParameter("cargo"), "");
		int puestos = Util.validaParametro(accionweb.getParameter("puestos"), 0);
		String motivo = Util.validaParametro(accionweb.getParameter("motivo"), "");
		List<Presw25DTO> resultadoPresupVacante = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupVacante");
		if(!cargo.trim().equals("") && puestos > 0 && !motivo.trim().equals("")) {
		Presw25DTO lista = new Presw25DTO(); 
		lista.setTippro("GNV");
		lista.setAnopre(a�o);
		lista.setCoduni(unidad);
		lista.setComen1(cargo);
		lista.setPres01(puestos);
		lista.setComen2(motivo);
		
		if(resultadoPresupVacante == null )		
			resultadoPresupVacante =  new ArrayList<Presw25DTO>();
	
		resultadoPresupVacante.add(lista);
		
		moduloPresupuesto.agregaResultadoVacanteSesion(accionweb.getReq(), resultadoPresupVacante);
		resultadoPresupVacante = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupVacante");
		}
		//for (Presw25DTO ss : resultadoPresupVacante){
		//	System.out.println("ss."+ ss.getComen1());
		//}
		
		accionweb.agregarObjeto("resultadoPresupVacante", resultadoPresupVacante);
  		accionweb.agregarObjeto("codUni", unidad);
		accionweb.agregarObjeto("anno", a�o);
	}
	*/	
	public void cargaResultadoPresup(AccionWeb accionweb)	throws Exception {
		/* MAA unidad e itedoc*/
		int rutUsuario = Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "",0);
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		//int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		//String nomUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"), "");
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"), "");
		String nomOrganizacion = Util.validaParametro(accionweb.getParameter("nomOrganizacion"), "");
		String sucur = Util.validaParametro(accionweb.getParameter("sucur"), "");
		int volver = Util.validaParametro(accionweb.getParameter("volver"), 0);
		//String nombreUnidad  = "";
		//accionweb.getSesion().setAttribute("codigoUnidad", unidad);
		String nombreOrganizacion  = "";
		accionweb.getSesion().setAttribute("codigoOrganizacion", organizacion);
		
		Collection<Presw18DTO> listaOrganizacion = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaOrganizacion");
		String codInmod = Util.validaParametro(accionweb.getParameter("codInmod"), "");
		String codIndexi = Util.validaParametro(accionweb.getParameter("codIndexi"), "");
		String codRespue = Util.validaParametro(accionweb.getParameter("codRespue"),"");
		if(codInmod.trim().equals("A") || codInmod.trim().equals("I"))
			accionweb.agregarObjeto("codInmod", codInmod);
		if(!codIndexi.trim().equals(""))
			accionweb.agregarObjeto("codIndexi", codIndexi);
		List<Presw25DTO> presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		List<Presw25DTO> resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
	 	List<Presw25DTO> resultadoPresup  = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
	 	List<Presw25DTO> resultadoPresupOriginal  = (List<Presw25DTO>) accionweb.getSesion().getAttribute("Original");
	 	
	 	List<Presw25DTO> resultadoPresupImport  = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupImport");
	 	List<descad.presupuesto.Presw25DTO> Item41  = new ArrayList<descad.presupuesto.Presw25DTO>();	
	 	List<Presw25DTO> resultadoPresupImport2  = new ArrayList<Presw25DTO>();
	 	
	 	List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		listaPresw25 = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		Long totalItems = new Long(0);
	 	 String muestraProg = "";
	 	try {
		if(resultadoPresupImport != null && resultadoPresupImport.size() > 0){// topeUnidad
			if(resultadoPresup != null){
				for (descad.presupuesto.Presw25DTO ss : resultadoPresup){
					// nombreUnidad = ss.getDesuni();
					nombreOrganizacion = ss.getDesuni();
					if (ss.getItedoc() == 41) {					
						Item41.add(ss);
					}
					/*esto es para agregar los programas en caso de grabar los objetivos - petici�n de octubre 2011*/
					
					String existeProg = "";
					Presw25DTO presw25DTO = new Presw25DTO();
					// if(ss.getCoduni() == unidad && (ss.getRespue().trim().equals("D1") || ss.getRespue().trim().equals("D2") || ss.getRespue().trim().equals("D3") || ss.getRespue().trim().equals("D4"))){
					if(organizacion.trim().equals(ss.getMotiv1().trim()) && (ss.getRespue().trim().equals("D1") || ss.getRespue().trim().equals("D2") || ss.getRespue().trim().equals("D3") || ss.getRespue().trim().equals("D4"))){ 
						presw25DTO = ss;					
						for (Presw25DTO rr : resultadoPresupImport){
							//if(ss.getCoduni() == unidad && ss.getRespue().trim().equals(rr.getRespue().trim())){
							if(organizacion.trim().equals(ss.getMotiv1().trim()) && ss.getRespue().trim().equals(rr.getRespue().trim())){
							    rr = ss;
								existeProg = "S";
							}
						}	
					if(existeProg.trim().equals(""))
	    				 resultadoPresupImport.add(presw25DTO);
	    			muestraProg = "S";
						
						
					}
			
				}
				if(muestraProg.trim().equals("S"))
		    		accionweb.agregarObjeto("muestraProg", "S");
		   
				
			}

			/*MB comentarea para presupuesto 22/10/2015 segun correo
			 * 
			 if (Item41 != null && Item41.size() > 0)
				resultadoPresupImport.addAll(Item41);
				*/
			for (Presw25DTO ss : resultadoPresupImport){	
			int largo = ss.getDesite().length();
			if( largo > 30)
				ss.setDesite(ss.getDesite().substring(0,30));
			//ss.setDesuni(nombreUnidad);
			ss.setDesuni(nombreOrganizacion);
			resultadoPresupImport2.add(ss);
			}
			moduloPresupuesto.agregaResulPresup(accionweb.getReq(), resultadoPresupImport2);
			accionweb.getSesion().setAttribute("resultadoPresup", resultadoPresupImport2);
			resultadoPresup  = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
			accionweb.agregarObjeto("resultadoPresup",resultadoPresup);
			listaPresw25 = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
			accionweb.getSesion().removeAttribute("resultadoPresupImport");
		}	

	 	
	 //	System.out.println("original/*** "+resultadoPresupOriginal.get(0).getPres01());
	 	//List<Integer> cambia = new ArrayList<Integer>();
		List<String> cambia = new ArrayList<String>();
	 	
	 	if(resultadoPresup != null){
	 		for (Presw25DTO ss : resultadoPresup){
	 			//if(ss.getCoduni() != unidad){
	 			if(!ss.getMotiv1().trim().equals(organizacion.trim())){
	 		 		accionweb.getSesion().removeAttribute("resultadoPresup");	 				
	 				if(presupItemreq != null )
	 					accionweb.getSesion().removeAttribute("presupItemreq");
	 				if(resultadoDetalleItemReq != null && volver != 1)
	 					accionweb.getSesion().removeAttribute("resultadoDetalleItemReq");
	 				break;
	 			} else {
	 				boolean hay = false;
	 				for (Presw25DTO oo : resultadoPresupOriginal){
	 				//System.out.println("ss.getItedoc(); "+ss.getItedoc()+"   oo.getItedoc; "+oo.getItedoc());
	 				// if ((ss.getItedoc() == oo.getItedoc()) && (ss.getRespue().trim().equals("")) ){
	 		  		if (ss.getMotiv2().trim().equals(oo.getMotiv2().trim()) && ss.getRespue().trim().equals("") ){
	 					hay = true;
	 					if ((oo.getPres01() != ss.getPres01()) ||
	 						(oo.getPres02() != ss.getPres02()) ||
	 						(oo.getPres03() != ss.getPres03()) ||
	 						(oo.getPres04() != ss.getPres04()) ||
	 						(oo.getPres05() != ss.getPres05()) ||
	 						(oo.getPres06() != ss.getPres06()) ||
	 						(oo.getPres07() != ss.getPres07()) ||
	 						(oo.getPres08() != ss.getPres08()) ||
	 						(oo.getPres09() != ss.getPres09()) ||
	 						(oo.getPres10() != ss.getPres10()) ||
	 						(oo.getPres11() != ss.getPres11()) ||
	 						(oo.getPres12() != ss.getPres12()) ||
	 						!(oo.getComen1().trim().equals(ss.getComen1().trim())) ||
	 						!(oo.getComen2().trim().equals(ss.getComen2().trim())) ||
	 						!(oo.getComen3().trim().equals(ss.getComen3().trim())) ||
	 						!(oo.getComen4().trim().equals(ss.getComen4().trim())) ||
	 						!(oo.getComen5().trim().equals(ss.getComen5().trim())) ||
	 						!(oo.getComen6().trim().equals(ss.getComen6().trim())) ) {
        					// cambia.add(oo.getItedoc());
	 						cambia.add(oo.getMotiv2().trim());
	 						}
	 	    			} 
	 				}
	 			}
	 		}
	 	  
	 	}
	 	Long tope = new Long(0);
	 	List<Presw25DTO> resultadoCPR = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoCPR");
	 	if(resultadoCPR != null && resultadoCPR.size() > 0){
		for (Presw25DTO ss : resultadoCPR){
			//if(ss.getCoduni() == unidad)
			if(ss.getComen4().trim().equals(organizacion.trim()))						
				tope = ss.getTotpre();
		}
	 	}
		accionweb.agregarObjeto("topeUnidad", tope);
	 	
	 	
		
	//		System.out.println("*****"+cambia);	
		if(rutUsuario > 0){
		if(volver != 1 || listaPresw25 == null) {
			
			//PreswBean preswbean = new PreswBean("PRE", unidad,0,a�o,0, rutUsuario);
			PreswBean preswbean = new PreswBean("PRE", 0,0,a�o,0, rutUsuario);
			preswbean.setCajero(organizacion.trim());
			preswbean.setDigide(dv);
			if(preswbean != null) {				
			listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
			moduloPresupuesto.agregaResultadoPresup(accionweb.getReq(), listaPresw25);
			if (codRespue.trim().equals("P")) {
				  for (Presw25DTO ss : listaPresw25){
					//if(ss.getCoduni() == unidad){
					if(ss.getMotiv1().trim().equals(organizacion.trim())){
							
						if (ss.getRespue().trim().equals("D1") && !ss.getComen1().trim().equals("")){
				     	   accionweb.agregarObjeto("muestraProg", "S");
						   break;
						}   
					}
					    
				 }
				}
			}
		} else {
			if (codRespue.trim().equals("P")) {
			  for (Presw25DTO ss : listaPresw25){
				//if(ss.getCoduni() == unidad){
				  if(ss.getMotiv1().trim().equals(organizacion.trim())){						
					if (ss.getRespue().trim().equals("D1") && !ss.getComen1().trim().equals("")){
			     	   accionweb.agregarObjeto("muestraProg", "S");
					   break;
					}   
				}
				    
			 }
			}
			
			if (cambia.size() > 0)
				 accionweb.agregarObjeto("cambia", cambia );
				
		}
		
		}
     		     				
     	List<Presw25DTO> original = (List<Presw25DTO>) accionweb.getSesion().getAttribute("Original");
	    
     	for (Presw25DTO ss : listaPresw25){
	    	//if(ss.getCoduni() == unidad)
     		if(ss.getMotiv1().trim().equals(organizacion.trim()))				
		 		 totalItems = totalItems + ss.getPres01() + ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() + ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() + ss.getPres10() + ss.getPres11() + ss.getPres12();
		}
     	
	 	}catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No registra presupuesto.cargaResultadoPresup");
			accionweb.agregarObjeto("exception", e.toString());
			e.printStackTrace();
		}
     	
     	
        if(accionweb.getSesion().getAttribute("tippro") != null)
        	accionweb.getSesion().removeAttribute("tippro");
        accionweb.getSesion().setAttribute("tippro", "PRE");
		accionweb.agregarObjeto("totalItems", totalItems);
	    accionweb.agregarObjeto("resultadoPresup", listaPresw25);
	 	//accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidad);	
	 	accionweb.agregarObjeto("cuentasPresupuestarias", listaOrganizacion);	
		
		String funcionario = accionweb.getSesion().getAttribute("funcionario") + "";
 		accionweb.agregarObjeto("funcionario", funcionario);
 		accionweb.agregarObjeto("anno", a�o); 
		//accionweb.agregarObjeto("codUni", unidad);
		//accionweb.agregarObjeto("unidad", unidad);
		//accionweb.agregarObjeto("nomUnidad",nomUnidad);
 		accionweb.agregarObjeto("codOrg", organizacion);
		accionweb.agregarObjeto("organizacion", organizacion);
		accionweb.agregarObjeto("nomOrganizacion",nomOrganizacion);
	
		accionweb.agregarObjeto("volver", 0);
		}
	
	public void volverPresupRequerimiento(AccionWeb accionweb) throws Exception {
		cargaPresupRequerimiento(accionweb);
		List<Presw25DTO> presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		List<Presw25DTO> resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		
		moduloPresupuesto.actualizaItemReqDet(accionweb.getReq(), resultadoDetalleItemReq, presupItemreq);
		presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
		accionweb.agregarObjeto("control", control);
		cargaPresupRequerimiento(accionweb);
	}
	public void cargaResultadoPresupPlan(AccionWeb accionweb)	throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int volver = Util.validaParametro(accionweb.getParameter("volver"), 0);
		int codActividad = Util.validaParametro(accionweb.getParameter("codActividad"), 0);
		String nombreItem= "";
		String nomActividad = Util.validaParametro(accionweb.getParameter("nomActividad"),"");
		String codgru = Util.validaParametro(accionweb.getParameter("codgru"),"");
		
		Collection<Presw25DTO> listaPlanDesarrollo = (Collection<Presw25DTO>) accionweb.getSesion().getAttribute("listaPlanDesarrollo");
		//String codInmod = Util.validaParametro(accionweb.getParameter("codInmod"), "");
		//String codIndexi = Util.validaParametro(accionweb.getParameter("codIndexi"), "");
		//if(codInmod.trim().equals("A") || codInmod.trim().equals("I"))
		//	accionweb.agregarObjeto("codInmod", codInmod);
		//if(!codIndexi.trim().equals(""))
		//	accionweb.agregarObjeto("codIndexi", codIndexi);
		List<Presw25DTO> presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		List<Presw25DTO> resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
	 	List<Presw25DTO> resultadoPresup  = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
	 	List<Presw25DTO> resultadoPresupOriginal  = (List<Presw25DTO>) accionweb.getSesion().getAttribute("Original");
	 	
	 	List<Presw25DTO> resultadoPresupImport  = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupImport");
	 	List<descad.presupuesto.Presw25DTO> Item41  = new ArrayList<descad.presupuesto.Presw25DTO>();	
	 	List<Presw25DTO> resultadoPresupImport2  = new ArrayList<Presw25DTO>();
	 	 
	 	
		if(resultadoPresupImport != null && resultadoPresupImport.size() > 0){
			/*   MB LO SACA SEGUN CORREO DE PRESUPUESTO 20/10/2015
			if(resultadoPresup != null){
				for (descad.presupuesto.Presw25DTO ss : resultadoPresup){
					nombreItem = ss.getDesite();
					if (ss.getItedoc() == 41) {					
						Item41.add(ss);
					}
				}
			}

			if (Item41 != null && Item41.size() > 0)
				resultadoPresupImport.addAll(Item41);
				
				*
				*/
			
			for (Presw25DTO ss : resultadoPresupImport){	
				int largo = ss.getDesite().length();
				if( largo > 30)
					ss.setDesite(ss.getDesite().substring(0,30));
			//	ss.setDesuni(nomActividad);
				resultadoPresupImport2.add(ss);
			}
			moduloPresupuesto.agregaResultadoPresup(accionweb.getReq(), resultadoPresupImport2);
		//	accionweb.getSesion().setAttribute("resultadoPresup", resultadoPresupImport);
			resultadoPresup  = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
			accionweb.getSesion().removeAttribute("resultadoPresupImport");
		}	

	 	
	 //	System.out.println("original/*** "+resultadoPresupOriginal.get(0).getPres01());
	 	//List<Integer> cambia = new ArrayList<Integer>();
		List<String> cambia = new ArrayList<String>();
		 
	 	Long totalItems = new Long(0);
	 	if(resultadoPresup != null){
	 		for (Presw25DTO ss : resultadoPresup){
	 			if(ss.getNumreq() != codActividad){
	 				accionweb.getSesion().removeAttribute("resultadoPresup");	 				
	 				if(presupItemreq != null )
	 					accionweb.getSesion().removeAttribute("presupItemreq");
	 				if(resultadoDetalleItemReq != null && volver != 1)
	 					accionweb.getSesion().removeAttribute("resultadoDetalleItemReq");
	 				break;
	 			} else {
	 				boolean hay = false;
	 				if(resultadoPresupOriginal != null ){
	 				for (Presw25DTO oo : resultadoPresupOriginal){
	 				//System.out.println("ss.getItedoc(); "+ss.getItedoc()+"   oo.getItedoc; "+oo.getItedoc());
	 					//if (ss.getItedoc() == oo.getItedoc()){
	 		 				
	 					if (ss.getCodman().trim().equals(oo.getCodman().trim())){
	 					hay = true;
	 					if ((oo.getPres01() != ss.getPres01()) ||
	 						(oo.getPres02() != ss.getPres02()) ||
	 						(oo.getPres03() != ss.getPres03()) ||
	 						(oo.getPres04() != ss.getPres04()) ||
	 						(oo.getPres05() != ss.getPres05()) ||
	 						(oo.getPres06() != ss.getPres06()) ||
	 						(oo.getPres07() != ss.getPres07()) ||
	 						(oo.getPres08() != ss.getPres08()) ||
	 						(oo.getPres09() != ss.getPres09()) ||
	 						(oo.getPres10() != ss.getPres10()) ||
	 						(oo.getPres11() != ss.getPres11()) ||
	 						(oo.getPres12() != ss.getPres12()) ||
	 						!(oo.getComen1().trim().equals(ss.getComen1().trim())) ||
	 						!(oo.getComen2().trim().equals(ss.getComen2().trim())) ||
	 						!(oo.getComen3().trim().equals(ss.getComen3().trim())) ||
	 						!(oo.getComen4().trim().equals(ss.getComen4().trim())) ||
	 						!(oo.getComen5().trim().equals(ss.getComen5().trim())) ||
	 						!(oo.getComen6().trim().equals(ss.getComen6().trim()))) {
//	 						if(cambia.indexOf(oo.getItedoc()) < 0)
	 						  // cambia.add(oo.getItedoc());
	 						 cambia.add(oo.getCodman().trim());
	 						  
	 						   //break;
	 					}
	 				} 
	 				}
	 				}
	 			//	System.out.println(hay);
	 				if(!hay)
	 				 //cambia.add(ss.getItedoc());
	 					cambia.add(ss.getCodman().trim());
	 			}
	 		}
	 	  
	 	}
	 //	Long tope = new Long(0);
	 //	if(listaActividad != null && listaActividad.size() > 0){
	//	for (Presw25DTO ss : listaActividad){
	//		if(ss.getNumreq() == actividad)
	//			tope = ss.getTotpre();
	//	}
	// 	}
	//	accionweb.agregarObjeto("topeUnidad", tope);
	 	
	 	
		
	//		System.out.println("*****"+cambia);	
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		listaPresw25 = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		if(rutUsuario > 0){
		if(volver != 1 || listaPresw25 == null) {
			PreswBean preswbean = new PreswBean("PRD", 0,0,a�o,0, rutUsuario);
			preswbean.setDigide(dv);
			preswbean.setMespar(codActividad);
			preswbean.setCajero(codgru);
			if(preswbean != null) {				
			listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
			moduloPresupuesto.agregaResulPresup(accionweb.getReq(), listaPresw25);
			}
		} else {
			if (cambia.size() > 0)
				 accionweb.agregarObjeto("cambia", cambia );
		}
		
		}
     		     				
     	List<Presw25DTO> original = (List<Presw25DTO>) accionweb.getSesion().getAttribute("Original");
	    
     	for (Presw25DTO ss : listaPresw25){
	    	if(ss.getNumreq() == codActividad)
		 		 totalItems = totalItems + ss.getPres01() + ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() + ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() + ss.getPres10() + ss.getPres11() + ss.getPres12();
		}
        if(accionweb.getSesion().getAttribute("codActividad") != null)
        	accionweb.getSesion().removeAttribute("codActividad");
        if(accionweb.getSesion().getAttribute("tippro") != null)
        	accionweb.getSesion().removeAttribute("tippro");
        accionweb.getSesion().setAttribute("tippro", "PRD");
        accionweb.getSesion().setAttribute("codActividad", codActividad);
		accionweb.agregarObjeto("totalItems", totalItems);
	    accionweb.agregarObjeto("resultadoPresup", listaPresw25);
	// 	accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidad);	
		String funcionario = accionweb.getSesion().getAttribute("funcionario") + "";
 		accionweb.agregarObjeto("funcionario", funcionario);
 		accionweb.agregarObjeto("anno", a�o); 
		accionweb.agregarObjeto("codActividad", codActividad);
		accionweb.agregarObjeto("nomActividad", nomActividad);
		accionweb.agregarObjeto("volver", 0);
		accionweb.agregarObjeto("codgru", codgru);
		}
	

	 synchronized public void cargaPresupRequerimiento(AccionWeb accionweb) throws Exception {
		Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		List<Presw25DTO> listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		int retorna = Util.validaParametro(accionweb.getParameter("retorna"), 0);
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		
		 /*para que cuando vuelva del req no cargue nuevamente la matriz, pero ya no es necesario
		 if(retorna != 1) {
			
			moduloPresupuesto.agregaResultadoMatriz(accionweb.getReq(), listarResultadoPresup);
			listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
			}
	*/
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int item = Util.validaParametro(accionweb.getParameter("item"), 0);
		 // MB comenta para prespuesto 22/10/2015
		// if(item != 41) item = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
	    String nomUnidad = "";
	    String nomItem = "";
		
		List<Presw25DTO> presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		if(presupItemreq == null || (presupItemreq != null && presupItemreq.size() == 0)) {
	
			List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
			if(rutUsuario > 0){
			PreswBean preswbean = new PreswBean("MAN", unidad,item,anno,0, rutUsuario);
			preswbean.setDigide(dv);
			if(preswbean != null) {
				listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
				moduloPresupuesto.agregaPresupRequerimientoSesion(accionweb.getReq(), listaPresw25);
				presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
			}
			}
		}
		Long en = new Long(0);
    	Long fe = new Long(0);
    	Long ma = new Long(0);
    	Long ab = new Long(0);
    	Long my = new Long(0);
    	Long jn = new Long(0);
    	Long jl = new Long(0);
    	Long ag = new Long(0);
    	Long se = new Long(0);
    	Long oc = new Long(0);
    	Long no = new Long(0);
    	Long di = new Long(0);
    	if(presupItemreq != null && presupItemreq.size() > 0){
        	
    	for (Presw25DTO ss : presupItemreq){
    		Presw25DTO presw25 = ss;
        	  
    		if(presw25.getCoduni() == unidad && presw25.getItedoc() == item && presw25.getAnopre() == anno ){
    			  nomItem = ss.getItedoc() + " - " + ss.getDesite();
	        	  nomUnidad = ss.getCoduni() + " - " + ss.getDesuni();
	    	switch (ss.getMespre()){	  			
	    	case 1: 	en = en + presw25.getTotpre();
	    	break;
	    	case 2: 	fe = fe + presw25.getTotpre();
	    	break;
	    	case 3:     ma = ma + presw25.getTotpre();
	    	break;
	    	case 4: 	ab = ab + presw25.getTotpre();
	    	break;
	    	case 5: 	my = my + presw25.getTotpre();
	    	break;
	    	case 6: 	jn = jn + presw25.getTotpre();
	    	break;
	    	case 7: 	jl = jl + presw25.getTotpre();
	    	break;
	    	case 8: 	ag = ag + presw25.getTotpre();
	    	break;
	    	case 9: 	se = se + presw25.getTotpre();
	    	break;
	    	case 10: 	oc = oc + presw25.getTotpre();
	    	break;
	    	case 11: 	no = no + presw25.getTotpre();
	    	break;
	    	case 12: 	di = di + presw25.getTotpre();
	    	break;
	    	}
    		}
    		if(presw25.getTotpre() == 0){
    			List<Presw25DTO>  resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
    			List<Presw25DTO> listarResultadoDet = new ArrayList<Presw25DTO>();
    			if(resultadoDetalleItemReq != null && resultadoDetalleItemReq.size() > 0){
    				for (Presw25DTO rs : resultadoDetalleItemReq){
    					  if(rs.getCoduni() == unidad && rs.getItedoc() == ss.getItedoc() && rs.getMespre() == ss.getMespre() && rs.getNumreq() == ss.getNumreq()){
    						  rs.setTippro("RQP");
    						  rs.setIndmod("D");
    						  rs.setCanman(0);
    						  rs.setTotpre(0);
    					
    					  }
    					  listarResultadoDet.add(rs);		  
    					}
    				}
    				
    				accionweb.getSesion().setAttribute("resultadoDetalleItemReq", listarResultadoDet);
    		}
    	}
		} else { for (Presw25DTO ss : listarResultadoPresup){

			          if(ss.getItedoc() == item){
			        	  nomItem = ss.getItedoc() + " - " + ss.getDesite();
			  			//System.out.println("ss.getItedoc(): "+ss.getItedoc());
			          }
			          if(ss.getCoduni() == unidad)
			        	  nomUnidad = ss.getCoduni() + " - " + ss.getDesuni();
		           }			
		}
    	
 //   System.out.println("lista valores : "+ en +"f : "+ fe +  "ma: "+ ma +"ab: "+ ab + "may: "+my +"junio: "+ jn +"julio : " +jl + "agosto; "+ag + "sep: "+se + "oc: "+ oc +"nov . "+ no +"dic: "+ di+"TAMA�O: "+presupItemreq.size());
    accionweb.agregarObjeto("enero",en);
    accionweb.agregarObjeto("febrero",fe);
    accionweb.agregarObjeto("marzo", ma);
    accionweb.agregarObjeto("abril", ab);
    accionweb.agregarObjeto("mayo", my);
    accionweb.agregarObjeto("junio", jn);
    accionweb.agregarObjeto("julio", jl);
    accionweb.agregarObjeto("agosto", ag);
    accionweb.agregarObjeto("septiembre", se );
    accionweb.agregarObjeto("octubre", oc);
    accionweb.agregarObjeto("noviembre", no);
    accionweb.agregarObjeto("diciembre", di);
    accionweb.agregarObjeto("total", en + fe + ma + ab + my + jn + jl + ag + se + oc + no + di);
	accionweb.agregarObjeto("cuentasPresupuestarias", lista);
	accionweb.agregarObjeto("unidadSeleccionada", unidad);
	accionweb.agregarObjeto("presupItemreq", presupItemreq);
	accionweb.agregarObjeto("resultadoPresup", listarResultadoPresup);
	accionweb.agregarObjeto("anno", anno);	
	accionweb.agregarObjeto("itedoc", item);	
	accionweb.agregarObjeto("nomUnidad", nomUnidad);
	accionweb.agregarObjeto("nomItem", nomItem);
	accionweb.agregarObjeto("control", control);
	}

	 synchronized public void RegistrarMatriz(AccionWeb accionweb) throws Exception {
	   // MAA cambio de unidad a organizacion 	 
	   // Collection<Presw18DTO> listaUnidad = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
	   //Collection<Presw18DTO> listaUnidad = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		Collection<Presw18DTO> listaOrganizacion = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaOrganizacion");
		List<Presw25DTO> listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		List<Presw25DTO> resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
		List<Presw25DTO> presupItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemReq");
		List<Presw25DTO>  resultadoCPR= (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoCPR");
		
		List<Presw25DTO> listaPRESW25 = new ArrayList<Presw25DTO>();
		int a�o =Integer.parseInt( Util.validaParametro(accionweb.getSesion().getAttribute("anno")+"","0"));
		int rutUsuario = Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "",0);
		//int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"");
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
	
	
				
		try {
					moduloPresupuesto.agregaRegistraMatriz(accionweb.getReq(), listarResultadoPresup, resultadoCPR);
					listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");

			
				 // boolean graba = moduloPresupuesto.saveMatriz(listarResultadoPresup,unidad, anno, rutUsuario);
					boolean graba = moduloPresupuesto.saveMatriz(listarResultadoPresup,organizacion, anno, rutUsuario);	
				/*	for(Presw25DTO ss : listarResultadoPresup){
						System.out.println(ss.getCoduni()+"tipo: *****"+ ss.getIndexi()+"  indmod: "+ ss.getIndmod());
					}*/
					if(graba){
						if(resultadoDetalleItemReq != null && resultadoDetalleItemReq.size()>0)
				/*	for(Presw25DTO ss : resultadoDetalleItemReq){
						System.out.println("tipo: *****"+ ss.getTippro()+" val: "+ ss.getCanman()+"  total: "+ ss.getTotpre() + "  ind: "+ ss.getIndmod()+" pres1: "+ss.getPres01()+"  pres2: "+ss.getPres02()+"pres3; "+ss.getPres03());
					}*/
					//graba = moduloPresupuesto.saveREQ( resultadoDetalleItemReq, unidad, anno, rutUsuario, mes);
					  graba = moduloPresupuesto.saveREQ( resultadoDetalleItemReq,organizacion, anno, rutUsuario, mes);		

				if(graba) {
					accionweb.agregarObjeto("mensaje", "Registr� exitosamente Presupuestaci�n");
					if(accionweb.getSesion().getAttribute("presupItemreq") != null)
						accionweb.getSesion().removeAttribute("presupItemreq");
					if(accionweb.getSesion().getAttribute("resultadoPresup") != null)
						accionweb.getSesion().removeAttribute("resultadoPresup");
					if(accionweb.getSesion().getAttribute("Original") != null)
						accionweb.getSesion().removeAttribute("Original");
					if(accionweb.getSesion().getAttribute("resultadoDetalleItemReq") != null)
						accionweb.getSesion().removeAttribute("resultadoDetalleItemReq");
					if(accionweb.getSesion().getAttribute("resultadoPresupNomina") != null)
						accionweb.getSesion().removeAttribute("resultadoPresupNomina");
					if(accionweb.getSesion().getAttribute("resultadoPresupVacante") != null)
						accionweb.getSesion().removeAttribute("resultadoPresupVacante");
					if(accionweb.getSesion().getAttribute("resultadoPresupRemuneracion") != null)
						accionweb.getSesion().removeAttribute("resultadoPresupRemuneracion");
					if(accionweb.getSesion().getAttribute("resultadoPresupIngresos") != null)
						accionweb.getSesion().removeAttribute("resultadoPresupIngresos");
					

				} else
					accionweb.agregarObjeto("mensaje", "No registra Presupuestaci�n");
			} else
				accionweb.agregarObjeto("mensaje", "No registra Presupuestaci�n");
			cargaPresup(accionweb);
		
			
			cargaResultadoPresup(accionweb);

			//accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidad);
			//accionweb.agregarObjeto("unidadSeleccionada", unidad);
			accionweb.agregarObjeto("cuentasPresupuestarias", listaOrganizacion);
			accionweb.agregarObjeto("organizacionSeleccionada", organizacion);


		}catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No registra Presupuesto.RegistrarMatriz");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		
		
	}
	
	 synchronized public void cargaResultadoPresupNomina(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		//int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"), "");
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		Collection<Presw18DTO> listaUnidad = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		
		if(accionweb.getSesion().getAttribute("resultadoPresupNomina") != null)
			accionweb.getSesion().removeAttribute("resultadoPresupNomina");
		if(accionweb.getSesion().getAttribute("resultadoPresupVacante") != null)
			accionweb.getSesion().removeAttribute("resultadoPresupVacante");
		
		/*nomina*/
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		List<Presw25DTO> listaVacante = new ArrayList<Presw25DTO>();
	   	int hayCambios = 0; 	 
		if(rutUsuario > 0){
	   	PreswBean preswbean = new PreswBean("CON", 0,0,a�o,0, rutUsuario);
	   	preswbean.setCajero(organizacion);
		preswbean.setDigide(dv);
		if(preswbean != null) {
		  	listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
		  	moduloPresupuesto.agregaResultadoPresupSesion(accionweb.getReq(), listaPresw25);
		  	listaPresw25 = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupNomina");
		  	String codInmod = Util.validaParametro(accionweb.getParameter("codInmod"), "");
			String codIndexi = Util.validaParametro(accionweb.getParameter("codIndexi"), "");
			if(codInmod.trim().equals("A") || codInmod.trim().equals("I"))
				accionweb.agregarObjeto("codInmod", codInmod);
			if(!codIndexi.trim().equals(""))
				accionweb.agregarObjeto("codIndexi", codIndexi);
		}
		
 	   	accionweb.agregarObjeto("resultadoPresupNomina", listaPresw25);		
 	   	/*verifica si hay modificaciones*/
 	
 	   	for (Presw25DTO ss : listaPresw25){
 	   		if(!ss.getRespue().trim().equals("") || 
 	   				!ss.getComen1().trim().equals("") || 
 	   				!ss.getMotiv1().trim().equals("")){ 
 	   			hayCambios = 1;
 	   			break;
 	   		}			
 	   	}
 	   	
 	   	/*vacantes se elimina sep 2010*/
 	  	
 	/*  listaVacante = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupVacante");
 	 if(rutUsuario > 0){
 	  if(listaVacante == null || listaVacante.size() == 0){
	   preswbean = new PreswBean("SNV", unidad,0,a�o,0, rutUsuario);
		if(preswbean != null) {
			listaVacante = (List<Presw25DTO>) preswbean.consulta_presw25();
		  	moduloPresupuesto.agregaResultadoVacanteSesion(accionweb.getReq(), listaVacante);
		  	listaVacante = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupVacante");
		}
 	  }
 	 }*/
		}
	//   	accionweb.agregarObjeto("resultadoPresupVacante", listaVacante);		
	   	/*verifica si hay modificaciones*/
//	   	int hayCambios2 = 0;
	   /*	for (Presw25DTO ss : listaVacante){
	   		if(!ss.getRespue().trim().equals("") || 
	   				!ss.getComen1().trim().equals("") || 
	   				!ss.getMotiv1().trim().equals("")){
	   			hayCambios2 = 1;
	   			break;
	   		}			
	   	}*/
 	   	
   	
 	   	accionweb.agregarObjeto("hayCambios", hayCambios);	
 	 //  	accionweb.agregarObjeto("hayCambios2", hayCambios2);
 		//accionweb.agregarObjeto("codUni", unidad);
 	    accionweb.agregarObjeto("codOrg", organizacion);
		accionweb.agregarObjeto("anno", a�o);
 		accionweb.agregarObjeto("control", control);
 		String funcionario = accionweb.getSesion().getAttribute("funcionario") + "";
 		accionweb.agregarObjeto("funcionario", funcionario);
	}
	 
	 synchronized public void registraPresupNomina(AccionWeb accionweb) throws Exception {
		 // 05/10/2016 KP - Se cambian nombres de variables y etiquetas 'unidad(es)' por 'organizacion(es)'
		//int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"), "");
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		List<Presw25DTO>  resultadoCPR= (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoCPR");
		
			
		//Collection<Presw18DTO> listaUnidad = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		Collection<Presw18DTO> listaOrganizacion = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaOrganizacion");
		
		List<Presw25DTO> listarResultadoPresupNomina = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupNomina");	
		moduloPresupuesto.agregaResultadoPresupNomina(accionweb.getReq(), listarResultadoPresupNomina, resultadoCPR);
		listarResultadoPresupNomina = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupNomina");	
		/*
		 * ahora se elimin� ingreso a las vacantes
		 */
		/*List<Presw25DTO> resultadoPresupVacante = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupVacante");	
		if(resultadoPresupVacante != null)
		for (Presw25DTO ss : resultadoPresupVacante){
			ss.setTippro("GNV");
			ss.setCoduni(unidad);
			ss.setAnopre(anno);
			ss.setIndexi("");
			ss.setDigide("");		
		}*/
		String codInmod = "";
		String codIndexi = "";
		for (Presw25DTO ss : listarResultadoPresupNomina){
		//	System.out.println("tip: "+ ss.getIndmod()+ " ind " + ss.getIndexi());
		//	if(ss.getCoduni() == unidad) {
		 if(ss.getMotiv1().trim().equals(organizacion)) {
			codInmod = ss.getIndmod();
			codIndexi = ss.getIndexi();
			}
		}
		 	   
		try {
		//	boolean graba = moduloPresupuesto.savePresupNomina(listarResultadoPresupNomina, unidad, anno, rutUsuario);
			boolean graba = moduloPresupuesto.savePresupNomina(listarResultadoPresupNomina, organizacion, anno, rutUsuario);
			if(graba) {
				/*ahora se elimin� ingreso a las vacantes 24/09/2010
				 * if(resultadoPresupVacante == null || resultadoPresupVacante.size() == 0){
					resultadoPresupVacante = new ArrayList<Presw25DTO>();
					Presw25DTO lista = new Presw25DTO();
					lista.setTippro("GNV");
					lista.setAnopre(anno);
					lista.setCoduni(unidad);
					lista.setComen1("");
					lista.setComen2("");
					lista.setPres01(0);
					resultadoPresupVacante.add(lista);
				}
				
				graba = moduloPresupuesto.savePresupVacante(resultadoPresupVacante, unidad, anno, rutUsuario);
				if(graba){*/
				accionweb.agregarObjeto("mensaje", "Registr� exitosamente la N�mina");
				accionweb.agregarObjeto("codInmod", codInmod);
				accionweb.agregarObjeto("codIndexi", codIndexi);
				cargaPresup(accionweb);
				cargaResultadoPresupNomina(accionweb);
			/*	if(accionweb.getSesion().getAttribute("resultadoPresupNomina") != null)
					accionweb.getSesion().removeAttribute("resultadoPresupNomina");
				if(accionweb.getSesion().getAttribute("resultadoPresupVacante") != null)
					accionweb.getSesion().removeAttribute("resultadoPresupVacante");*/
			
			/*	ahora se elimin� ingreso a las vacantes 24/09/2010
			 * } else
					accionweb.agregarObjeto("mensaje", "No registra Vacantes");*/
				
			} else
				accionweb.agregarObjeto("mensaje", "No registra N�mina");
			
			
			  
	       // accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidad);
	       //accionweb.agregarObjeto("unidadSeleccionada", unidad);
	    	accionweb.agregarObjeto("cuentasPresupuestarias", listaOrganizacion);
	    	accionweb.agregarObjeto("organizacionSeleccionada", organizacion);
	    	accionweb.agregarObjeto("control", 2);
	    	//accionweb.agregarObjeto("codUni", unidad);
	    	accionweb.agregarObjeto("codOrg", organizacion);
			
		}catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No registra la N�mina");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}				
	}
	 
	 
	 synchronized public void cargaResultadoPresupRemuneracion(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		//int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);	
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"), "");	
		
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		// Collection<Presw18DTO> listaUnidad = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		Collection<Presw18DTO> listaOrganizacion = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaOrganizacion");
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
	   	//PreswBean preswbean = new PreswBean("ASI", unidad,0,a�o,0, rutUsuario);
	 	PreswBean preswbean = new PreswBean("ASI", 0,0,a�o,0, rutUsuario);
	 	preswbean.setCajero(organizacion);
	  	preswbean.setDigide(dv);
		if(preswbean != null) {
			
		  	listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
			moduloPresupuesto.agregaResultadoPresupRemSesion(accionweb.getReq(), listaPresw25);
			listaPresw25 =  (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupRemuneracion");
			String codInmod = Util.validaParametro(accionweb.getParameter("codInmod"), "");
			String codIndexi = Util.validaParametro(accionweb.getParameter("codIndexi"), "");
			if(codInmod.trim().equals("A") || codInmod.trim().equals("I"))
				accionweb.agregarObjeto("codInmod", codInmod);
			if(!codIndexi.trim().equals(""))
				accionweb.agregarObjeto("codIndexi", codIndexi);

		}
 	   	accionweb.agregarObjeto("resultadoPresupRemuneracion", listaPresw25);		
 	    accionweb.agregarObjeto("anno1", a�o - 1);
 	    accionweb.agregarObjeto("anno2", a�o - 2);
 	    accionweb.agregarObjeto("anno3", a�o - 3);
 	    accionweb.agregarObjeto("anno4", a�o - 4);
 	    accionweb.agregarObjeto("anno", a�o);

 	   	/*verifica si hay modificaciones*/
 	   	int hayCambios = 0;
 	   	for (Presw25DTO ss : listaPresw25){
 	   		if(ss.getValpr1() > 0 || ss.getValpr2() > 0){
 	   			hayCambios = 1;
 	   			break;
 	   		}	
   		if (ss.getIndexi().equals("A")) 
 	   		  accionweb.agregarObjeto("fechaAprobacion", (ss.getPres01()+"").substring(6)+"/"+
 	   				                                     (ss.getPres01()+"").substring(4,6)+"/"+
 	   				                                     (ss.getPres01()+"").substring(0,4));
 	   		   

 	   	}
 	   	accionweb.agregarObjeto("hayCambios", hayCambios);	 
 		String funcionario = accionweb.getSesion().getAttribute("funcionario") + "";
 		accionweb.agregarObjeto("funcionario", funcionario);
 		//accionweb.agregarObjeto("codUni", unidad);
 		accionweb.agregarObjeto("codOrg", organizacion);
 		accionweb.agregarObjeto("anno", a�o);
		accionweb.agregarObjeto("control", control);
	}
	 synchronized public void registraPresupRemuneracion(AccionWeb accionweb) throws Exception {
		 // MAA se cambia unidad por organizacion
		//int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),""); 
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);				
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		// Collection<Presw18DTO> listaUnidad = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		Collection<Presw18DTO> listaOrganizacion = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaOrganizacion");
		List<Presw25DTO>  resultadoCPR= (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoCPR");
		String p_pres05 = Util.validaParametro(accionweb.getParameter("p_pres05"),"");	
		String p_pres06 = Util.validaParametro(accionweb.getParameter("p_pres06"),"");	
		String p_pres07 = Util.validaParametro(accionweb.getParameter("p_pres07"),"");
		String p_pres08 = Util.validaParametro(accionweb.getParameter("p_pres08"),"");
		String p_pres09 = Util.validaParametro(accionweb.getParameter("p_pres09"),"");
		String p_pres10 = Util.validaParametro(accionweb.getParameter("p_pres10"),"");
		String p_pres11 = Util.validaParametro(accionweb.getParameter("p_pres11"),"");
		String p_pres12 = Util.validaParametro(accionweb.getParameter("p_pres12"),"");	
		String p_valr11 = Util.validaParametro(accionweb.getParameter("p_valr11"),"");	
		String p_valr12 = Util.validaParametro(accionweb.getParameter("p_valr12"),"");
		String p_valr21 = Util.validaParametro(accionweb.getParameter("p_valr21"),"");
		String p_valr22 = Util.validaParametro(accionweb.getParameter("p_valr22"),"");
		String p_valr31 = Util.validaParametro(accionweb.getParameter("p_valr31"),"");
		
		p_pres05 = p_pres05.replace(".", "");
		p_pres06 = p_pres06.replace(".", "");
		p_pres07 = p_pres07.replace(".", "");
		p_pres08 = p_pres08.replace(".", "");
		p_pres09 = p_pres09.replace(".", "");
		p_pres10 = p_pres10.replace(".", "");
		p_pres11 = p_pres11.replace(".", "");
		p_pres12 = p_pres12.replace(".", "");
		p_valr11 = p_valr11.replace(".", "");
		p_valr12 = p_valr12.replace(".", "");
		p_valr21 = p_valr21.replace(".", "");
		p_valr22 = p_valr22.replace(".", "");
		p_valr31 = p_valr31.replace(".", "");
		
		Long pres05 = Long.parseLong(p_pres05);
		Long pres06 = Long.parseLong(p_pres06);
		Long pres07 = Long.parseLong(p_pres07);
		Long pres08 = Long.parseLong(p_pres08);
		Long pres09 = Long.parseLong(p_pres09);
		Long pres10 = Long.parseLong(p_pres10);
		Long pres11 = Long.parseLong(p_pres11);
		Long pres12 = Long.parseLong(p_pres12);
		int  valr11 = Integer.parseInt(p_valr11);
		int  valr12 = Integer.parseInt(p_valr12);
		int  valr21 = Integer.parseInt(p_valr21);
		int  valr22 = Integer.parseInt(p_valr22);
		int  valr31 = Integer.parseInt(p_valr31);
		
		
		List<Presw25DTO> listarResultadoPresupRemuneracion = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupRemuneracion");	
		moduloPresupuesto.agregaResultadoPresupRemuneracion(accionweb.getReq(), listarResultadoPresupRemuneracion,resultadoCPR);
		listarResultadoPresupRemuneracion = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupRemuneracion");	
		resultadoCPR= (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoCPR");

		try {
		/*for (Presw25DTO ss : listarResultadoPresupRemuneracion){
		 		System.out.println(ss.getIndexi()+" cod " + ss.getIndmod() );
		 	}*/
		//	boolean graba = moduloPresupuesto.savePresupRemuneracion(listarResultadoPresupRemuneracion, unidad, anno, rutUsuario);
			List<Presw25DTO> listaResultado = new ArrayList<Presw25DTO>();
			if (listarResultadoPresupRemuneracion != null && listarResultadoPresupRemuneracion.size() > 0 ) {
				for (Presw25DTO ss : listarResultadoPresupRemuneracion){
				   ss.setDesite(organizacion);	
				   ss.setTippro("ASP");
				   ss.setPres05(pres05);
				   ss.setPres06(pres06);
				   ss.setPres07(pres07);
				   ss.setPres08(pres08);
				   ss.setPres09(pres09);
				   ss.setPres10(pres10);
				   ss.setPres11(pres11);
				   ss.setPres12(pres12);
				   ss.setValr11(valr11);
				   ss.setValr12(valr12);
				   ss.setValr21(valr21);
				   ss.setValr22(valr22);
				   ss.setValr31(valr31);
				   listaResultado.add(ss);
				}
				
			}
			boolean graba = moduloPresupuesto.savePresupRemuneracion(listaResultado, organizacion, anno, rutUsuario);

			if(graba) {
				accionweb.agregarObjeto("mensaje", "Registr� exitosamente la Remuneraci�n");
				if(accionweb.getSesion().getAttribute("resultadoPresupRemuneracion") != null)   
				 	accionweb.getSesion().removeAttribute("resultadoPresupRemuneracion");
				
				accionweb.getSesion().setAttribute("resultadoPresupRemuneracion",listaResultado);
				accionweb.agregarObjeto("resultadoPresupRemuneracion", listaResultado);	
				
			} else
				accionweb.agregarObjeto("mensaje", "No registra Remuneraci�n");
			
			cargaPresup(accionweb);
			cargaResultadoPresupRemuneracion(accionweb);
			  
	       // accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidad);
	    	//accionweb.agregarObjeto("unidadSeleccionada", unidad);
			accionweb.agregarObjeto("cuentasPresupuestarias", listaOrganizacion);
	    	accionweb.agregarObjeto("organizacionSeleccionada", organizacion);
	    	accionweb.agregarObjeto("control", control);
			
		}catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No registra Remuneraci�n");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
			
		
	}
	public void cargaResultadoPresupIngresos(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		Collection<Presw18DTO> listaUnidad = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
	   	PreswBean preswbean = new PreswBean("ING", unidad,0,a�o,0, rutUsuario);
		preswbean.setDigide(dv);
		if(preswbean != null) {
			listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
		}
 	   	accionweb.agregarObjeto("resultadoPresupIngresos", listaPresw25);	
 	   	
 	   	/*verifica si hay modificaciones*/
 	   	int hayCambios = 0;
 	   	for (Presw25DTO ss : listaPresw25){
			if(ss.getTotpre() > 0 || !ss.getComen2().trim().equals("")){
				hayCambios = 1;
				break;
			}			
		}
 	   	accionweb.agregarObjeto("hayCambios", hayCambios);	
 		String funcionario = accionweb.getSesion().getAttribute("funcionario") + "";
 		accionweb.agregarObjeto("funcionario", funcionario);
 		accionweb.agregarObjeto("codUni", unidad);
 		accionweb.agregarObjeto("anno", a�o);
	}

	public void cargaDetalleItemReq(AccionWeb accionweb) throws Exception {
		// Modificado por MAA
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		//int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"");
		//int itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		String itedoc = Util.validaParametro(accionweb.getParameter("itedoc"),"");
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		int sucur = Util.validaParametro(accionweb.getParameter("sucur"), 0);
		Long totalMensual = Util.validaParametro(accionweb.getParameter("total"),new Long(0));
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		Long total = new Long(0);
		Long totalRq = new Long(0);
		/*falta mandar sucur  0 cuando es nueva*/
		//Collection<Presw18DTO> listaUnidad = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		Collection<Presw18DTO> listaOrganizacion = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaOrganizacion");
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		List<Presw25DTO> resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
		boolean existe = false;
		if(resultadoDetalleItemReq != null && resultadoDetalleItemReq.size() > 0 && sucur > 0){
		for (Presw25DTO ss : resultadoDetalleItemReq){
			//if(ss.getAnopre() == a�o && ss.getCoduni() == unidad && ss.getItedoc() == itedoc && ss.getMespre() == mes && ss.getNumreq() == sucur){
			if(ss.getAnopre() == a�o && ss.getComen3().trim().equals(organizacion.trim()) && ss.getComen4().trim().equals(itedoc+"") && ss.getMespre() == mes && ss.getNumreq() == sucur){
				listaPresw25.add(ss);
				existe = true;
			}			
		}
		}
		if(!existe){
			// PreswBean preswbean = new PreswBean("REQ", unidad,itedoc,a�o,mes, rutUsuario);
			PreswBean preswbean = new PreswBean("REQ", 0,0,a�o,mes, rutUsuario);
			preswbean.setDigide(dv);
			preswbean.setCajero(organizacion);
			preswbean.setTipdoc(itedoc.substring(0,2));
			preswbean.setTipope(itedoc.substring(3));
			if(preswbean != null) {
				preswbean.setSucur(sucur);	
				listaPresw25 = new ArrayList<Presw25DTO>();
				listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
			}
			}
	    if(resultadoDetalleItemReq == null)
	    	resultadoDetalleItemReq = new ArrayList<Presw25DTO>();
		moduloPresupuesto.agregaDetalleItemReqSesion(accionweb.getReq(), listaPresw25, resultadoDetalleItemReq);
		
		listaPresw25 = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listaPresw25");
		if(listaPresw25 != null)
			accionweb.getSesion().removeAttribute("listaPresw25");
		resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
	  	//String nomUnidad = "";
		String nomOrganizacion = "";
	  	String nomItem = "";
	  	String nomMes = "";
	  	String area1 = "";
	  	String area2 = "";
	  	String comen1 = "";
	  	String comen2 = "";
	  	String comen3 = "";
	  	String comen4 = "";
	  	String comen5 = "";
	  	String comen6 = "";
	  	String motiv1 = "";
		String motiv2 = "";
		String motiv3 = "";
		String motiv4 = "";
		String motiv5 = "";
		String motiv6 = "";
		int canman = 0;
		int valpr1 = 0;
		int numReq = 0;
		List<Presw25DTO> presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");		
	
	 	for (Presw25DTO ss : listaPresw25){
	 		//System.out.println(ss.getNumreq());
		  	// nomUnidad = ss.getCoduni() + " - " + ss.getDesuni();
	 		nomOrganizacion = ss.getComen3() + " - " + ss.getDesuni();
		    // nomItem = ss.getItedoc() + " - " + ss.getDesite();
	 		nomItem = ss.getComen4() + " - " + ss.getDesite();
		    numReq = ss.getNumreq();
			area1 = ss.getArea1();
		  	area2 = ss.getArea2();
		  	comen1 = ss.getComen1();
		  	comen2 = ss.getComen2();
		  	comen3 = ss.getComen3();
		  	comen4 = ss.getComen4();
		  	comen5 = ss.getComen5();
		  	comen6 = ss.getComen6();
		  	motiv1 = ss.getMotiv1();
			motiv2 = ss.getMotiv2();
			motiv3 = ss.getMotiv3();
			motiv4 = ss.getMotiv4();
			motiv5 = ss.getMotiv5();
			motiv6 = ss.getMotiv6();
			valpr1 = ss.getValpr1();
			canman = ss.getCanman();
		//	totalRq = totalRq + (ss.getCanman() * ss.getValuni());
		    switch (ss.getMespre()){
		    case 1: nomMes = "ENERO";
		            break;
		    
		    case 2:nomMes = "FEBRERO";
		    break;
		    case 3:nomMes = "MARZO";
		    break;
		    case 4:nomMes = "ABRIL";
		    break;
		    case 5:nomMes = "MAYO";
		    break;
		    case 6:nomMes = "JUNIO";
		    break;
		    case 7:nomMes = "JULIO";
		    break;
		    case 8:nomMes = "AGOSTO";
		    break;
		    case 9:nomMes = "SEPTIEMBRE";
		    break;
		    case 10:nomMes = "OCTUBRE";
		    break;
		    case 11:nomMes = "NOVIEMBRE";
		    break;
		    case 12:nomMes = "DICIEMBRE";
		    break;
		    }
		
	    	
	   
	 	}
	
	 	for (Presw25DTO rs : presupItemreq){
  		  
    		//if(rs.getCoduni() == unidad && rs.getItedoc() == itedoc && rs.getAnopre() == a�o  && rs.getMespre() == mes){
	 		if(rs.getComen3().trim().equals(organizacion.trim()) && rs.getComen4().trim().equals(itedoc+"") && rs.getAnopre() == a�o  && rs.getMespre() == mes){
    		  if(rs.getNumreq() == numReq )	
    			  totalRq = totalRq + rs.getTotpre();
	    	
    		  total = total + rs.getTotpre();
    		}
    	}
		 
	 	// accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidad);
		// accionweb.agregarObjeto("nomUnidad", nomUnidad);
	 	 accionweb.agregarObjeto("cuentasPresupuestarias", listaOrganizacion);
		 accionweb.agregarObjeto("nomOrganizacion", nomOrganizacion);
		 accionweb.agregarObjeto("nomItem", nomItem);    
		 accionweb.agregarObjeto("nomMes", nomMes);  
		 accionweb.agregarObjeto("numReq", numReq); 
	 	 accionweb.agregarObjeto("resultadoDetalleItemReq", resultadoDetalleItemReq);	
	 	 accionweb.agregarObjeto("area1",area1);
	 	 accionweb.agregarObjeto("area2",area2);
	 	 accionweb.agregarObjeto("comen1",comen1);
	 	 accionweb.agregarObjeto("comen2",comen2);
	 	 accionweb.agregarObjeto("comen3",comen3);
	 	 accionweb.agregarObjeto("comen4",comen4);
	 	 accionweb.agregarObjeto("comen5",comen5);
	 	 accionweb.agregarObjeto("comen6",comen6);
	 	 accionweb.agregarObjeto("motiv1",motiv1);
	 	 accionweb.agregarObjeto("motiv2",motiv2);
	 	 accionweb.agregarObjeto("motiv3",motiv3);
	 	 accionweb.agregarObjeto("motiv4",motiv4);
	 	 accionweb.agregarObjeto("motiv5",motiv5);
	 	 accionweb.agregarObjeto("motiv6",motiv6);
	 	 accionweb.agregarObjeto("valpr1",valpr1);
	 	 accionweb.agregarObjeto("totalReq",totalRq);
	 	// total = totalMensual + total;
			
	 	 accionweb.agregarObjeto("total",(total - totalRq));
		 accionweb.agregarObjeto("anno", a�o);		 
	 	// accionweb.agregarObjeto("unidad",unidad);
		 accionweb.agregarObjeto("organizacion",organizacion);
	 	 accionweb.agregarObjeto("numreq",numReq);
	 	 accionweb.agregarObjeto("item",itedoc);
	 	 accionweb.agregarObjeto("mes",mes);
	 	 accionweb.agregarObjeto("totalMes", total);
	 	 accionweb.agregarObjeto("control", control);
	}
	 synchronized public void actualizarItemReq(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		int itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		int numreq = Util.validaParametro(accionweb.getParameter("sucur"), 0);
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		

		List<Presw25DTO> listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		
		List<Presw25DTO> resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");	
		List<Presw25DTO> presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		moduloPresupuesto.agregaItemReqDet(accionweb.getReq(), resultadoDetalleItemReq, presupItemreq, listarResultadoPresup);
		presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
		Long totalMes = (Long) accionweb.getSesion().getAttribute("totalMes");
		accionweb.agregarObjeto("totalMes", totalMes);
		//cargaPresupRequerimiento(accionweb);
		accionweb.agregarObjeto("control", control);
		cargaDetalleItemReq(accionweb);
		
	}
	
	 synchronized public void actualizaItem(AccionWeb accionweb) throws Exception {
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		List<Presw25DTO> listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		long superaTope = moduloPresupuesto.verificaTopeUnidad(accionweb.getReq(), listarResultadoPresup);
			NumberFormat nf2 = 	NumberFormat.getInstance(Locale.GERMAN);
        try {
		if (superaTope == 0) {
			moduloPresupuesto.agregaItemActualiza(accionweb.getReq(), listarResultadoPresup);
			listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
			accionweb.agregarObjeto("control", control);
			cargaPresup(accionweb);
		}
		else {
		   accionweb.agregarObjeto("superaTope", "1");	
		   accionweb.agregarObjeto("mensaje", "Se ha superado el tope permitido para la unidad. Esta unidad tiene un tope de $" + nf2.format(superaTope));
		   cargaPresup(accionweb); 
		   }
        }catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No registra presupuesto.actualizaItem");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
	}
	 
	 synchronized public void eliminaItem(AccionWeb accionweb) throws Exception {
	  //MAA cambios de unidad a organizacion
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		//int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"");
		List<Presw25DTO> listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		moduloPresupuesto.eliminaItem(accionweb.getReq(), listarResultadoPresup);
		listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		accionweb.agregarObjeto("control", control);
		//accionweb.agregarObjeto("unidadSeleccionada", unidad);
		accionweb.agregarObjeto("organizacionSeleccionada", organizacion);
		cargaPresup(accionweb);

	}
	
	 synchronized public void cargaPresupPrograma(AccionWeb accionweb) throws Exception {
		  int rutUsuario = Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "",0);
	     // int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		  String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"), "");
	      int a�o = Util.validaParametro(Integer.parseInt(accionweb.getSesion().getAttribute("anno")+""), 0);
	     // String nomUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"),"");
	      String nomOrganizacion = Util.validaParametro(accionweb.getParameter("nomOrganizacion"),"");
	      int accionProg = Util.validaParametro(accionweb.getParameter("accionProg"),0);
	      // accionProg   1: Ingresar    2: Actualizar
	      
	      accionweb.agregarObjeto("organizacion", organizacion);
	      accionweb.agregarObjeto("anno", a�o);
	      accionweb.agregarObjeto("nomOrganizacion",nomOrganizacion);
	      accionweb.agregarObjeto("accionProg",accionProg);
	      if (accionProg == 0)  
	    	  accionweb.agregarObjeto("mensaje","");
	      
	      List<Presw25DTO> resultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
	   	  if (resultadoPresup != null && resultadoPresup.size() > 0) {
		      for (Presw25DTO ss : resultadoPresup){
		 		  if (!organizacion.trim().equals(ss.getMotiv1().trim())){
		    	  // if(ss.getCoduni() != unidad){
		 		 	accionweb.getSesion().removeAttribute("resultadoPresup");	 				
		 			break;
		  		} 
		      }
	      }
	   	  try {
	         if (resultadoPresup == null || resultadoPresup.size() == 0) {
				//PreswBean preswbean = new PreswBean("PRE", unidad,0,a�o,0, rutUsuario);
	            PreswBean preswbean = new PreswBean("PRE", 0,0,a�o,0, rutUsuario);
	            preswbean.setCajero(organizacion);
				if(preswbean != null) 				
				  resultadoPresup = (List<Presw25DTO>) preswbean.consulta_presw25();
		  }
	   	  } catch (Exception E) {
				System.out.println("excepcion  presupupesto.cargaPresupPrograma " + E.toString());

			}
	    	  
	      if(resultadoPresup != null) {
			  String observacion1 = "";
		      String observacion2 = "";
		      String observacion3 = "";
		      String observacion4 = "";
		      for (Presw25DTO ss : resultadoPresup){
    			// if(ss.getCoduni() == unidad) {
		    	  if (!organizacion.trim().equals(ss.getComen4().trim())){
    				if (ss.getRespue().trim().equals("D1") && !ss.getComen1().trim().equals("")) 
    					observacion1 = ss.getComen1().trim() + " " + ss.getComen2().trim() + " " + ss.getComen3().trim() + " " + ss.getComen4().trim();
	    			if (ss.getRespue().trim().equals("D2") && !ss.getComen1().trim().equals(""))
	    				observacion2 = ss.getComen1().trim() + " " + ss.getComen2().trim() + " " + ss.getComen3().trim() + " " + ss.getComen4().trim();
	    			if (ss.getRespue().trim().equals("D3") && !ss.getComen1().trim().equals(""))
	    				observacion3 = ss.getComen1().trim() + " " + ss.getComen2().trim() + " " + ss.getComen3().trim() + " " + ss.getComen4().trim();
	    			if (ss.getRespue().trim().equals("D4") && !ss.getComen1().trim().equals(""))
	    				observacion4 = ss.getComen1().trim() + " " + ss.getComen2().trim() + " " + ss.getComen3().trim() + " " + ss.getComen4().trim();
    			}	
		      } 
		      accionweb.agregarObjeto("observacion1", observacion1);
		      accionweb.agregarObjeto("observacion2", observacion2);
		      accionweb.agregarObjeto("observacion3", observacion3);
		      accionweb.agregarObjeto("observacion4", observacion4);
		      if (accionProg == 1) // Ingresar
		         accionweb.agregarObjeto("accionProg", "2");
		      else if (accionProg == 0 ) { // Para cuando viene desde la LUPA
		    	      if (!observacion1.trim().equals("")) 
		    	        accionweb.agregarObjeto("accionProg", "0");
		    	      else accionweb.agregarObjeto("accionProg", "1"); 
		      } 	  
		      
		  }
		  
	}
	
	
	 synchronized public void agregaPresupPrograma(AccionWeb accionweb) throws Exception {
		  /* MAA se cambia de unidad a organizacion */
	      int rutUsuario = Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "",0);
	      int a�o = Util.validaParametro(Integer.parseInt(accionweb.getSesion().getAttribute("anno")+""), 0);
		  String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		  // int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		  String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"");
		  int accionProg = Util.validaParametro(accionweb.getParameter("accionProg"), 0);
		try{	  
		  List<Presw25DTO> resultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		  if(resultadoPresup == null || resultadoPresup.size() == 0 ) { // accionProg == 2 Para cuando ya existe y se quiere modificar la descricpion del programa
				PreswBean preswbean = new PreswBean("PRE",0,0,a�o,0, rutUsuario);
				preswbean.setCajero(organizacion);
				if(preswbean != null) 				
					resultadoPresup = (List<Presw25DTO>) preswbean.consulta_presw25();
		 }
	//	  System.out.println("tama�o: ******** "+resultadoPresup.size());
		 if(resultadoPresup != null && resultadoPresup.size() > 0 || accionProg == 2){
				    	String comen1 = "";
				    	String comen2 = "";
				    	String comen3 = "";
				    	String comen4 = "";
				    	for(int i=1;i<=4;i++){
				    		for (Presw25DTO ss : resultadoPresup){
				    			//if(ss.getCoduni() == unidad && ss.getRespue().trim().equals("D"+i)) {
				    			if(ss.getMotiv1().trim().equals(organizacion.trim()) && ss.getRespue().trim().equals("D"+i)) {	
				    			/*
				    				comen1 = Util.validaParametro(accionweb.getParameter("comen1"+i), "");
				    				comen2 = Util.validaParametro(accionweb.getParameter("comen2"+i), "");
				    				comen3 = Util.validaParametro(accionweb.getParameter("comen3"+i), "");
				    				comen4 = Util.validaParametro(accionweb.getParameter("comen4"+i), "");
				    				*/
				    				  String obs = Util.validaParametro(accionweb.getParameter("observacion"+i),"");
				    				  obs = moduloPresupuesto.eliminaCaracteresString(obs);
				    				if(obs.length() < 40){
				    					comen1 = obs;
					    				comen2 = "";
					    				comen3 = "";
					    				comen4 = "";
				    				} else {
				    					if(obs.length() < 80){
				    						comen1 = obs.substring(0,40);
				    						comen2 = obs.substring(40);
				    						comen3 = "";
				    						comen4 = "";
				    					}else {
				    						if(obs.length() < 120){
				    							comen1 = obs.substring(0,40);
					    						comen2 = obs.substring(40,80);
					    						comen3 = obs.substring(80);
					    						comen4 = "";
				    						} else {
				    							if(obs.length() < 160){
					    							comen1 = obs.substring(0,40);
						    						comen2 = obs.substring(40,80);
						    						comen3 = obs.substring(80,120);
						    						comen4 = obs.substring(120);
				    						    }else {
				    						    	comen1 = obs.substring(0,40);
						    						comen2 = obs.substring(40,80);
						    						comen3 = obs.substring(80,120);
						    						comen4 = obs.substring(120,160);
				    						    }
				    					}
				    				}
				    				}
				    				
				    				ss.setComen1(comen1);
				    				ss.setComen2(comen2);
				    				ss.setComen3(comen3);
				    				ss.setComen4(comen4);
				    				//ss.setCoduni(unidad);
				    				ss.setMotiv1(organizacion);
				    				ss.setAnopre(a�o);
				    			}	
				    		}
				    	}
				    	if(accionProg == 1)
	                	   moduloPresupuesto.agregaResultadoPresup(accionweb.getReq(), resultadoPresup);
				    	else
				    		moduloPresupuesto.agregaResulPresup(accionweb.getReq(), resultadoPresup);
					    	
				    	if (accionProg == 1) 
				    	   accionweb.agregarObjeto("mensaje", "La informaci�n del programa ha sido agregada, ahora debe ingresar la presupuestaci�n y registrar.");
				       	else 
				    	   accionweb.agregarObjeto("mensaje", "La informaci�n del programa ha sido modificado, recuerde registrar.");	
		  
		 }
		 }catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "agregaPresupPrograma accionProg: "+accionProg);
				accionweb.agregarObjeto("exception", e.toString());
				e.printStackTrace();
			}
				
		  cargaPresupPrograma(accionweb);
	}
	
	 synchronized public void eliminaReq(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		int itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		int sucur = Util.validaParametro(accionweb.getParameter("sucur"), 0);
		int existe = 0;
		String nomItem = "";
		String nomUnidad = "";
		
		List<Presw25DTO> listaPresupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		List<Presw25DTO>  resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
		List<Presw25DTO>  lista = new ArrayList<Presw25DTO>();
		if(resultadoDetalleItemReq != null && resultadoDetalleItemReq.size() > 0){
			for (Presw25DTO ss : resultadoDetalleItemReq){
				if(ss.getMespre() == mes && ss.getNumreq() == sucur)
				   existe = 1;
			}
		} else resultadoDetalleItemReq = new ArrayList<Presw25DTO>(); 
		if(resultadoDetalleItemReq == null || resultadoDetalleItemReq.size() == 0 || existe == 0){
			PreswBean preswbean = new PreswBean("REQ", unidad,itedoc,a�o,mes, rutUsuario);
	    	preswbean.setDigide(dv);
			if(preswbean != null) {
				preswbean.setSucur(sucur);	
				lista = new ArrayList<Presw25DTO>();
				lista = (List<Presw25DTO>) preswbean.consulta_presw25();
				if(lista != null && lista.size() > 0){
					for (Presw25DTO ss : lista){
						ss.setTippro("RQP");
						ss.setCanman(0);
						ss.setTotpre(0);
						ss.setIndmod("D");
						resultadoDetalleItemReq.add(ss);
					}
						
				}
			}
		} 
		moduloPresupuesto.eliminaReq(accionweb.getReq(), listaPresupItemreq, resultadoDetalleItemReq);
		listaPresupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
		
		Long en = new Long(0);
    	Long fe = new Long(0);
    	Long ma = new Long(0);
    	Long ab = new Long(0);
    	Long my = new Long(0);
    	Long jn = new Long(0);
    	Long jl = new Long(0);
    	Long ag = new Long(0);
    	Long se = new Long(0);
    	Long oc = new Long(0);
    	Long no = new Long(0);
    	Long di = new Long(0);
		for (Presw25DTO ss : listaPresupItemreq){
    		Presw25DTO presw25 = ss;
        	  
    		if(presw25.getCoduni() == unidad && presw25.getItedoc() == itedoc && presw25.getAnopre() == a�o ){
    			  nomItem = ss.getItedoc() + " - " + ss.getDesite();
	        	  nomUnidad = ss.getCoduni() + " - " + ss.getDesuni();
	    	switch (ss.getMespre()){	  			
	    	case 1: 	en = en + presw25.getTotpre();
	    	break;
	    	case 2: 	fe = fe + presw25.getTotpre();
	    	break;
	    	case 3:     ma = ma + presw25.getTotpre();
	    	break;
	    	case 4: 	ab = ab + presw25.getTotpre();
	    	break;
	    	case 5: 	my = my + presw25.getTotpre();
	    	break;
	    	case 6: 	jn = jn + presw25.getTotpre();
	    	break;
	    	case 7: 	jl = jl + presw25.getTotpre();
	    	break;
	    	case 8: 	ag = ag + presw25.getTotpre();
	    	break;
	    	case 9: 	se = se + presw25.getTotpre();
	    	break;
	    	case 10: 	oc = oc + presw25.getTotpre();
	    	break;
	    	case 11: 	no = no + presw25.getTotpre();
	    	break;
	    	case 12: 	di = di + presw25.getTotpre();
	    	break;
	    	}
    		}
    	}
		
		  accionweb.agregarObjeto("enero",en);
		    accionweb.agregarObjeto("febrero",fe);
		    accionweb.agregarObjeto("marzo", ma);
		    accionweb.agregarObjeto("abril", ab);
		    accionweb.agregarObjeto("mayo", my);
		    accionweb.agregarObjeto("junio", jn);
		    accionweb.agregarObjeto("julio", jl);
		    accionweb.agregarObjeto("agosto", ag);
		    accionweb.agregarObjeto("septiembre", se );
		    accionweb.agregarObjeto("octubre", oc);
		    accionweb.agregarObjeto("noviembre", no);
		    accionweb.agregarObjeto("diciembre", di);
		    accionweb.agregarObjeto("total", en + fe + ma + ab + my + jn + jl + ag + se + oc + no + di);
			//accionweb.agregarObjeto("cuentasPresupuestarias", lista);
			accionweb.agregarObjeto("unidadSeleccionada", unidad);
			accionweb.agregarObjeto("presupItemreq", listaPresupItemreq);
			//accionweb.agregarObjeto("resultadoPresup", listarResultadoPresup);
			accionweb.agregarObjeto("anno", a�o);	
			accionweb.agregarObjeto("itedoc", itedoc);	
			accionweb.agregarObjeto("nomUnidad", nomUnidad);
			accionweb.agregarObjeto("nomItem", nomItem);
			accionweb.agregarObjeto("control", control);
		cargaPresup(accionweb);

	}
	 synchronized public void actualizaMatriz(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		List<Presw25DTO> listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
	   	List<Presw25DTO> resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
	   	List<Presw25DTO> presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		int volver = Util.validaParametro(accionweb.getParameter("volver"), 0);
		int item = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		List<Presw25DTO> listarResultado = new ArrayList<Presw25DTO>();
			
	 	Long totalItemReq = new Long(0);
     	Long ene = new Long(0);
     	Long feb = new Long(0);
    	Long mar = new Long(0);
     	Long abr = new Long(0);
     	Long may = new Long(0);
     	Long jun = new Long(0);
     	Long jul = new Long(0);
     	Long ago = new Long(0);
     	Long sep = new Long(0);
     	Long oct = new Long(0);
     	Long nov = new Long(0);
     	Long dic = new Long(0);
		for (Presw25DTO ss : presupItemreq){ // agregar todo lo que est� en mantencion al presup
			if(ss.getItedoc() == item && unidad == ss.getCoduni() /*&& ss.getMespre() == mes*/) {
				switch (ss.getMespre()){
				case 1: ene = ene + ss.getTotpre();
				break;
				case 2: feb = feb + ss.getTotpre();
				break;
				case 3: mar = mar + ss.getTotpre();
				break;
				case 4: abr = abr + ss.getTotpre();
				break;
				case 5: may = may + ss.getTotpre();
				break;
				case 6: jun = jun + ss.getTotpre();
				break;
				case 7: jul = jul + ss.getTotpre();
				break;
				case 8: ago = ago + ss.getTotpre();
				break;
				case 9: sep = sep + ss.getTotpre();
				break;
				case 10: oct = oct + ss.getTotpre();
				break;
				case 11: nov = nov + ss.getTotpre();
				break;
				case 12: dic = dic + ss.getTotpre();
				break;
				}
				totalItemReq = totalItemReq + ss.getTotpre();
				if(ss.getTotpre() == 0) ss.setIndmod("D");
			}
		}
	  	if(listarResultadoPresup.size() > 0){
	  		int i = -1;
		for (Presw25DTO ss : listarResultadoPresup){
			i++;
			if(unidad == ss.getCoduni() && ss.getItedoc() == item) {
			ss.setPres01(ene);
			ss.setPres02(feb);
			ss.setPres03(mar);
			ss.setPres04(abr);
			ss.setPres05(may);
			ss.setPres06(jun);
			ss.setPres07(jul);
			ss.setPres08(ago);
			ss.setPres09(sep);
			ss.setPres10(oct);
			ss.setPres11(nov);
			ss.setPres12(dic);
			ss.setTotpre(totalItemReq);			
			}
			listarResultado.add(ss);
		//	System.out.println(ss.getItedoc()+ "**" + ss.getComen1()+" ** "+ss.getComen2()+ " *** "+" ** "+ss.getComen3()+" ** "+ss.getComen4()+" ** "+ss.getComen5()+" ** "+ss.getComen6());
		}
	  //  moduloPresupuesto.agregaResultadoMatriz(accionweb.getReq(), listarResultado);
		
	
		moduloPresupuesto.agregaResulPresup(accionweb.getReq(), listarResultado);
		//	accionweb.getSesion().setAttribute("resultadoPresup", resultadoPresupImport);
		listarResultado  = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		
	    cargaPresup(accionweb);
	   	accionweb.agregarObjeto("resultadoPresup", listarResultado);
		accionweb.agregarObjeto("control", control);
		accionweb.agregarObjeto("unidadSeleccionada", unidad);
	  	}
 	  
	}
	public void cargaPresupItem(AccionWeb accionweb) throws Exception {
	
	   // MAA cambios a Organizacion de Unidad
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		// int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion").trim(),"");
		//int itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		String itedoc = Util.validaParametro(accionweb.getParameter("itedoc").trim(),"");
		// Collection<Presw18DTO> listaUnidad = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		Collection<Presw18DTO> listaOrganizacion = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaOrganizacion");
		List<Presw25DTO> listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
	//	System.out.println(listarResultadoPresup.size());
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
	/*	Presw25DTO presw25 = new  Presw25DTO();
		
		for (Presw25DTO ss : listarResultadoPresup){
			if(ss.getCoduni() == unidad && ss.getItedoc() == itedoc )
				presw25 = ss;
		}*/
		//accionweb.agregarObjeto("unidad", unidad);
		accionweb.agregarObjeto("itedoc", itedoc);
		accionweb.agregarObjeto("organizacion", organizacion);
	 	accionweb.agregarObjeto("resultadoPresup", listarResultadoPresup);
 	   // accionweb.agregarObjeto("unidadSeleccionada", unidad);
 	   // accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidad);
 	    accionweb.agregarObjeto("organizacionSeleccionada", organizacion);
	    accionweb.agregarObjeto("cuentasPresupuestarias", listaOrganizacion);
 	    accionweb.agregarObjeto("control", control);
 	    
 	    
	}
	
	synchronized public void actualizaPresupItem(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		int itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		int indice = Util.validaParametro(accionweb.getParameter("i"), 0);
		Collection<Presw18DTO> listaUnidad = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		List<Presw25DTO> listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		List<Presw25DTO> presw25 = new  ArrayList<Presw25DTO>();
		for (Presw25DTO ss : listarResultadoPresup){
			if( unidad == ss.getCoduni() && ss.getItedoc() == itedoc )
				presw25.add(ss);
		}
			
 	   	accionweb.agregarObjeto("listPresw25", presw25);	
 	   	accionweb.agregarObjeto("unidadSeleccionada", unidad);
 	    accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidad);
	}

	public void cargaOtroIng(AccionWeb accionweb) throws Exception {
		// 06/10/2016 KP - Se cambian nombres de variables y etiquetas 'unidad(es)' por 'organizacion(es)'
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int a�o = Util.validaParametro(Integer.parseInt(accionweb.getSesion().getAttribute("anno")+""), 0);
		//int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		int organizacion = Util.validaParametro(accionweb.getParameter("organizacion"), 0);
		//Collection<Presw18DTO> listaUnidad = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		Collection<Presw18DTO> listaOrganizacion = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaOrganizacion");
		
		/*esto es para el menu de control presupuestario*/
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		accionweb.getSesion().setAttribute("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		/* fin */
		
		// es solo para mostrar el menu
	    int control = Util.validaParametro(accionweb.getParameter("control"), 0);
    	accionweb.agregarObjeto("control", control);
    	int hayCambios = 0;
    	List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		if(accionweb.getSesion().getAttribute("resultadoPresupIngresos") != null){
			listaPresw25 = (List<Presw25DTO>)accionweb.getSesion().getAttribute("resultadoPresupIngresos") ;
		/*	for (Presw25DTO ss : listaPresw25){
				System.out.println(ss.getValuni()+" sesion  conc: "+ ss.getComen1()+"  vv: "+ss.getComen2()+"sig : "+ss.getComen3());
			}*/
			//accionweb.getSesion().removeAttribute("resultadoPresupIngresos");
		} else {
		
		listaPresw25 = new ArrayList<Presw25DTO>();
	   	PreswBean preswbean = new PreswBean("ING", 0,0,a�o,0, rutUsuario);
		preswbean.setDigide(dv);
		if(preswbean != null) {
		  	listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
		/*	for (Presw25DTO ss : listaPresw25){
				System.out.println(ss.getValuni()+" ING  conc: "+ ss.getComen1()+"  vv: "+ss.getComen2()+"sig : "+ss.getComen3());
			}*/
		  	moduloPresupuesto.agregaPresupOtrosSesion(accionweb.getReq(), listaPresw25);
		  	listaPresw25 = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupIngresos");
		  	if(listaPresw25 != null && listaPresw25.size() == 0)
				listaPresw25 = null;
		  	else {
			 	if(listaPresw25 == null ){
					accionweb.agregarObjeto("mensaje", "No tiene permitido Presupuestaci�n");
					
			  	} else {
			  		/*verifica si hay modificaciones*/
			 	   //	int ind = 0;
			 	   	for (Presw25DTO ss : listaPresw25){
			 	   		/*ind++;
			 	   	    accionweb.agregarObjeto("comen1"+ind, ss.getComen2());
			 	   	    accionweb.agregarObjeto("comen2"+ind, ss.getComen3());
			 	   	    accionweb.agregarObjeto("comen3"+ind, ss.getComen4());
			 	   	    accionweb.agregarObjeto("comen4"+ind, ss.getComen5());
			 	   	    accionweb.agregarObjeto("comen5"+ind, ss.getComen6());*/
						if(ss.getTotpre() > 0 || !ss.getComen2().trim().equals("") || !ss.getComen3().trim().equals("") || !ss.getComen4().trim().equals("") || !ss.getComen5().trim().equals("") || !ss.getComen6().trim().equals("")){
							hayCambios = 1;
							break;
						}			
					}
			  	}
		  	}
			} else listaPresw25 = null;
	}
	
	 // 	if(a�o == 0) a�o = 2009;
 	   	accionweb.agregarObjeto("resultadoPresupIngresos", listaPresw25);	
 	   //	accionweb.agregarObjeto("unidadSeleccionada", unidad);
 	    accionweb.agregarObjeto("cuentasPresupuestarias", listaOrganizacion);
 	   	accionweb.agregarObjeto("organizacionSeleccionada", organizacion);
 	    
 	    accionweb.agregarObjeto("anno", a�o);
 	   	accionweb.agregarObjeto("hayCambios", hayCambios); 	     	    
	}
	
	 synchronized public void registraPresupOtrosIng(AccionWeb accionweb) throws Exception {
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		// es solo para mostrar el menu
	    int control = Util.validaParametro(accionweb.getParameter("control"), 0);
    	accionweb.agregarObjeto("control", control);
		
		Collection<Presw18DTO> listaUnidad = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		
		List<Presw25DTO> listaResultadoPresupIngresos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupIngresos");	
		moduloPresupuesto.agregaResultadoPresupOtrosIng(accionweb.getReq(), listaResultadoPresupIngresos);
		listaResultadoPresupIngresos = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupIngresos");	
		/*for (Presw25DTO ss : listaResultadoPresupIngresos){
			System.out.println(ss.getValuni()+" registra  conc: "+ ss.getComen1()+"  vv: "+ss.getComen2()+"sig : "+ss.getComen3());
		}*/
		 	   
					
		try {
			boolean graba = moduloPresupuesto.savePresupOtros(listaResultadoPresupIngresos, anno, rutUsuario);
			 cargaOtroIng(accionweb);
			if(graba){
				accionweb.agregarObjeto("mensaje", "Registr� exitosamente Otros Negocios");
				//accionweb.getSesion().removeAttribute("resultadoPresupIngresos");
			}
			else
				accionweb.agregarObjeto("mensaje", "No registra Otros Negocios");
			
		   
	 	   accionweb.agregarObjeto("anno", anno);
	       accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidad);
	      
			
		}catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No registra Otros Negocios");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
			
		
	}
	 synchronized public void exportarCorriente(AccionWeb accionweb) throws Exception {
		int itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String extension = Util.validaParametro(accionweb.getParameter("extension"), "");
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		
		PreswBean preswbean = new PreswBean("PRE", unidad,0,a�o,0, rutUsuario);
		preswbean.setDigide(dv);
		if(preswbean != null) {				
			accionweb.agregarObjeto("data", preswbean.consulta_presw25());
        }
	
		accionweb.agregarObjeto("format", extension);

	
	}
	



	public void corrienteExcel(AccionWeb accionweb) throws Exception {
		// MAA cambia cuenta por organizacion e itedoc a string
		//int itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		String itedoc = Util.validaParametro(accionweb.getParameter("itedoc"),"");
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		//int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"");
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");

		Vector vec_d = new Vector();
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		//String nomUnidad = "";
		String nomOrganizacion = "";
		Long total = new Long(0);
		Long totalTotal = new Long(0);
		Long enero = new Long(0);
		Long febrero = new Long(0);
		Long marzo = new Long(0);
		Long abril = new Long(0);
		Long mayo = new Long(0);
		Long junio = new Long(0);
		Long julio = new Long(0);
		Long agosto = new Long(0);
		Long septiembre = new Long(0);
		Long octubre = new Long(0);
		Long noviembre = new Long(0);
		Long diciembre = new Long(0);
		//PreswBean preswbean = new PreswBean("PRE", unidad,0,a�o,0, rutUsuario);
		PreswBean preswbean = new PreswBean("PRE", 0,0,a�o,0, rutUsuario);
		preswbean.setCajero(organizacion);
		preswbean.setDigide(dv);
		Vector vec_datos = new Vector();

		if(preswbean != null) {				
			listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
		}
		
		long totalAgrup = 0;
		Vector agrup = null;
		Vector agrup1 = new Vector();
		Vector agrup2 = new Vector();
		Vector agrup3 = new Vector();
		Vector agrup4 = new Vector();
		long total_ENE = 0;	
		long total_FEB = 0;	
		long total_MAR = 0;
		long total_ABR = 0;	
		long total_MAY = 0;	
		long total_JUN = 0;	
		long total_JUL = 0;	
		long total_AGO = 0;	
		long total_SEP = 0;	
		long total_OCT = 0;	
		long total_NOV = 0;	
		long total_DIC = 0;	
		long agrup_ENE = 0;	
		long agrup_FEB = 0;
		long agrup_MAR = 0;	
		long agrup_ABR = 0;	
		long agrup_MAY = 0;	
		long agrup_JUN = 0;	
		long agrup_JUL = 0;	
		long agrup_AGO = 0;	
		long agrup_SEP = 0;	
		long agrup_OCT = 0;	
		long agrup_NOV = 0;	
		long agrup_DIC = 0;
		for (Presw25DTO ss : listaPresw25){
			total = ss.getPres01() + ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() + ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() + ss.getPres10() + ss.getPres11() + ss.getPres12();
			total_ENE = ss.getPres01();
			total_FEB = ss.getPres02();
			total_MAR = ss.getPres03();
			total_ABR = ss.getPres04();
			total_MAY = ss.getPres05();
			total_JUN = ss.getPres06();
			total_JUL = ss.getPres07();
			total_AGO = ss.getPres08();
			total_SEP = ss.getPres09();
			total_OCT = ss.getPres10();
			total_NOV = ss.getPres11();
			total_DIC = ss.getPres12();
	        if(!ss.getMotiv2().trim().equals(ss.getMotiv3().trim())){
		        totalAgrup = totalAgrup + total;
		        agrup_ENE = agrup_ENE + total_ENE;
			    agrup_FEB = agrup_FEB + total_FEB;
				agrup_MAR = agrup_MAR + total_MAR;
				agrup_ABR = agrup_ABR + total_ABR;
				agrup_MAY = agrup_MAY + total_MAY;
				agrup_JUN = agrup_JUN + total_JUN;
				agrup_JUL = agrup_JUL + total_JUL;
				agrup_AGO = agrup_AGO + total_AGO;
				agrup_SEP = agrup_SEP + total_SEP;
				agrup_OCT = agrup_OCT + total_OCT;
				agrup_NOV = agrup_NOV + total_NOV;
				agrup_DIC = agrup_DIC + total_DIC;
	        }  
		    else {
		    	 agrup = new Vector();	 
				 agrup.addElement(totalAgrup);
				 agrup.addElement(agrup_ENE);
				 agrup.addElement(agrup_FEB);
				 agrup.addElement(agrup_MAR);
				 agrup.addElement(agrup_ABR);
				 agrup.addElement(agrup_MAY);
				 agrup.addElement(agrup_JUN);
				 agrup.addElement(agrup_JUL);
				 agrup.addElement(agrup_AGO);
				 agrup.addElement(agrup_SEP);
				 agrup.addElement(agrup_OCT);
				 agrup.addElement(agrup_NOV);
				 agrup.addElement(agrup_DIC);
		    	 if(ss.getMotiv2().trim().equals("2")) agrup1 = agrup;
		    	 else if(ss.getMotiv2().trim().equals("3")) agrup2 = agrup;
				 else if(ss.getMotiv2().trim().equals("4")) agrup3 = agrup;
		    	 
		    	 totalAgrup = 0;  
				 agrup_ENE = 0;	
				 agrup_FEB = 0;	
				 agrup_MAR = 0;	
				 agrup_ABR = 0;	
				 agrup_MAY = 0;	
				 agrup_JUN = 0;	
				 agrup_JUL = 0;	
				 agrup_AGO = 0;	
				 agrup_SEP = 0;	
				 agrup_OCT = 0;	
				 agrup_NOV = 0;	
				 agrup_DIC = 0;
				 
				 
		      }
		  } 
		 agrup = new Vector();	 
		 agrup.addElement(totalAgrup);
		 agrup.addElement(agrup_ENE);
		 agrup.addElement(agrup_FEB);
		 agrup.addElement(agrup_MAR);
		 agrup.addElement(agrup_ABR);
		 agrup.addElement(agrup_MAY);
		 agrup.addElement(agrup_JUN);
		 agrup.addElement(agrup_JUL);
		 agrup.addElement(agrup_AGO);
		 agrup.addElement(agrup_SEP);
		 agrup.addElement(agrup_OCT);
		 agrup.addElement(agrup_NOV);
		 agrup.addElement(agrup_DIC);
		 agrup4 = agrup;
		
		
		for (Presw25DTO ss : listaPresw25){		
			total = ss.getPres01() + ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() + ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() + ss.getPres10() + ss.getPres11() + ss.getPres12();
			if(total > 0 || ss.getMotiv2().trim().equals(ss.getMotiv3().trim())){
			vec_d = new Vector();
			//vec_d.add(ss.getItedoc());
			if(!ss.getMotiv2().trim().equals(ss.getMotiv3().trim())) 
				//vec_datos.add(String.valueOf(ss.getItedoc()));
				      vec_d.add(ss.getMotiv2());
				else vec_d.add("");
				vec_d.add(ss.getDesite());
				
				agrup = new Vector();
			    if(ss.getMotiv2().trim().equals(ss.getMotiv3().trim())){
				   if(ss.getMotiv2().trim().equals("1")) agrup = agrup1;
				   else if(ss.getMotiv2().trim().equals("2")) agrup = agrup2;
	               else if(ss.getMotiv2().trim().equals("3")) agrup = agrup3;
	                else if(ss.getMotiv2().trim().equals("4")) agrup = agrup4;
				   
				    vec_d.add(agrup.get(0));
					vec_d.add(agrup.get(1));
					vec_d.add(agrup.get(2));
					vec_d.add(agrup.get(3));
					vec_d.add(agrup.get(4));
					vec_d.add(agrup.get(5));
					vec_d.add(agrup.get(6));
					vec_d.add(agrup.get(7));
					vec_d.add(agrup.get(8));
					vec_d.add(agrup.get(9));
					vec_d.add(agrup.get(10));
					vec_d.add(agrup.get(11));
					vec_d.add(agrup.get(12));
					vec_d.add("");
			    }    
	          	else {
		          	vec_d.add(total);
					vec_d.add(ss.getPres01());
					vec_d.add(ss.getPres02());
					vec_d.add(ss.getPres03());
					vec_d.add(ss.getPres04());
					vec_d.add(ss.getPres05());
					vec_d.add(ss.getPres06());
					vec_d.add(ss.getPres07());
					vec_d.add(ss.getPres08());
					vec_d.add(ss.getPres09());
					vec_d.add(ss.getPres10());
					vec_d.add(ss.getPres11());
					vec_d.add(ss.getPres12());
					vec_d.add(ss.getComen1().toString().trim()+ss.getComen2().toString().trim()+ss.getComen3().toString().trim()+ss.getComen4().toString().trim()+ss.getComen5().toString().trim()+ss.getComen6().toString().trim());
		        }
			
			vec_datos.add(vec_d);
			//nomUnidad = ss.getCoduni() + " - " + ss.getDesite(); 
			nomOrganizacion = ss.getMotiv1().trim() + " - " + ss.getDesite();
			enero += ss.getPres01();
			febrero += ss.getPres02();
			marzo += ss.getPres03();
			abril += ss.getPres04();
			mayo += ss.getPres05();
			junio += ss.getPres06();
			julio += ss.getPres07();
			agosto += ss.getPres08();
			septiembre += ss.getPres09();
			octubre += ss.getPres10();
			noviembre += ss.getPres11();
			diciembre += ss.getPres12();
			totalTotal += total;
			}
		}
		vec_d = new Vector();
		vec_d.add("");
		vec_d.add("Total");
		vec_d.add(totalTotal);
		vec_d.add(enero);
		vec_d.add(febrero);
		vec_d.add(marzo);
		vec_d.add(abril);
		vec_d.add(mayo);
		vec_d.add(junio);
		vec_d.add(julio);
		vec_d.add(agosto);
		vec_d.add(septiembre);
		vec_d.add(octubre);
		vec_d.add(noviembre);
		vec_d.add(diciembre);
		vec_d.add("");
		if(totalTotal > 0)
			vec_datos.add(vec_d);

		/*agrega requerimiento*/
	    String nomItem = "";
	    String nomMes = "";
	    //itedoc = 41;
	    itedoc = "41";
		Vector vecTotales = new Vector();
		List<Presw25DTO> presupItemreq = new ArrayList<Presw25DTO>();
		List<Presw25DTO> presupItemreqDet = new ArrayList<Presw25DTO>();
		
		//preswbean = new PreswBean("MAN", unidad,itedoc,a�o,0, rutUsuario);
		preswbean = new PreswBean("MAN", 0,0,a�o,0, rutUsuario);
		preswbean.setCajero(organizacion);
		preswbean.setDigide(dv);
		if(preswbean != null) {
			    presupItemreq = (List<Presw25DTO>) preswbean.consulta_presw25();
		}
		
		Long en = new Long(0);
    	Long fe = new Long(0);
    	Long ma = new Long(0);
    	Long ab = new Long(0);
    	Long my = new Long(0);
    	Long jn = new Long(0);
    	Long jl = new Long(0);
    	Long ag = new Long(0);
    	Long se = new Long(0);
    	Long oc = new Long(0);
    	Long no = new Long(0);
    	Long di = new Long(0);
    	Vector listaReqResumenExcel = new Vector();	
    	Vector listaReqDetResumenExcel = new Vector();
         
    	if(presupItemreq != null && presupItemreq.size() > 0){
        Vector meses = new Vector();
        int numMes = 1;
    //    while (numMes < 13) {
    	for (Presw25DTO ss : presupItemreq){
    		Presw25DTO presw25 = ss;
        	  
    		//if(presw25.getCoduni() == unidad && presw25.getItedoc() == itedoc && presw25.getAnopre() == a�o /*&& numMes == ss.getMespre()*/){
    		if(presw25.getComen3().trim().equals(organizacion.trim()) && presw25.getComen4().trim().equals(itedoc.trim()) && presw25.getAnopre() == a�o ){
    			  nomItem = ss.getItedoc() + " - " + ss.getDesite();
	        	  // nomUnidad = ss.getCoduni() + " - " + ss.getDesuni();
    			  nomOrganizacion = ss.getComen3().trim() + " - " + ss.getDesuni();
    			  
	    	switch (ss.getMespre()){	  			
	    	case 1: 	{ en = en + presw25.getTotpre();
	    	              nomMes = "Enero"; 	    	
	    	              break;
	    	}
	    	case 2: 	{fe = fe + presw25.getTotpre();
	    				nomMes = "Febrero"; 
	    				break;
	    	}
	    	
	    	case 3:    { ma = ma + presw25.getTotpre();
	    				nomMes = "Marzo";
	    				break;
	    	}
	    	
	    	case 4:    {ab = ab + presw25.getTotpre();
	    				nomMes = "Abril"; 
	    				break;
	    	}
	    	
	    	case 5: 	{my = my + presw25.getTotpre();
	    				nomMes = "Mayo"; 
	    				break;
	    	}
	    	
	    	case 6: 	{jn = jn + presw25.getTotpre();
	    				nomMes = "Junio";
	    				break;
	    	}
	    	
	    	case 7: 	{jl = jl + presw25.getTotpre();
	    				nomMes = "Julio"; 
	    				break;
	    	}
	    	
	    	case 8: 	{ag = ag + presw25.getTotpre();
	    				nomMes = "Agosto"; 
	    				break;
	    	}
	    	
	    	case 9: 	{se = se + presw25.getTotpre();
	    				nomMes = "Septiembre"; 
	    				break;
	    	}
	    	
	    	case 10: 	{oc = oc + presw25.getTotpre();
	    				nomMes = "Octubre"; 
	    				break;
	    	}
	    	
	    	case 11: 	{no = no + presw25.getTotpre();
	    				nomMes = "Noviembre";
	    				break;
	    	}
	    	
	    	case 12: 	{di = di + presw25.getTotpre();
	    				nomMes = "Diciembre"; 
	    				break;
	    	}
	    	
	    	}
	    	
	    		
	    	    vec_d = new Vector();
	    	    vec_d.addElement(ss.getMespre());
	    	    vec_d.addElement(nomMes);
	    	    vec_d.addElement(ss.getComen1().trim());
	    	    vec_d.addElement(ss.getComen2().trim());
	    	    vec_d.addElement(ss.getTotpre());
	    	    switch (ss.getMespre()){
	    	    case 1: vec_d.addElement(en);
	    	    break;
	    	    case 2 : vec_d.addElement(fe);
	    	    break;
	    	    case 3 : vec_d.addElement(ma);
	    	    break;
	    	    case 4 : vec_d.addElement(ab);
	    	    break;
	    	    case 5 : vec_d.addElement(my);
	    	    break;
	    	    case 6 : vec_d.addElement(jn);
	    	    break;
	    	    case 7 : vec_d.addElement(jl);
	    	    break;
	    	    case 8 : vec_d.addElement(ag);
	    	    break;
	    	    case 9 : vec_d.addElement(se);
	    	    break;
	    	    case 10 : vec_d.addElement(oc);
	    	    break;
	    	    case 11 : vec_d.addElement(no);
	    	    break;
	    	    case 12 : vec_d.addElement(di);
	    	    break;
	    	    }
	    	    if(ss.getTotpre() > 0)
	    	       listaReqResumenExcel.addElement(vec_d);
    	
    		//preswbean = new PreswBean("REQ", unidad,itedoc,a�o,ss.getMespre(), rutUsuario);
	    	preswbean = new PreswBean("REQ", 0,0,a�o,ss.getMespre(), rutUsuario);    
	    	preswbean.setCajero(organizacion);
	    	preswbean.setTipdoc(itedoc.substring(0,2));
			preswbean.setTipope(itedoc.substring(3));
   			preswbean.setDigide(dv);
			if(preswbean != null) {
				preswbean.setSucur(ss.getNumreq());	
				presupItemreqDet = new ArrayList<Presw25DTO>();
				presupItemreqDet = (List<Presw25DTO>) preswbean.consulta_presw25();
			}
    	}
			
    //	}
 
    	//numMes++;
    //    }
    //	}
    	nomMes = "";
    	if(presupItemreqDet.size() > 0){
    	for(Presw25DTO pi: presupItemreqDet){
        	switch (pi.getMespre()){	  			
	    	case 1: 	nomMes = "Enero"; 	    	
	    	              break;
	    	case 2: 	nomMes = "Febrero"; 
	    				break;	    	
	    	case 3:   	nomMes = "Marzo";
	    				break;	    	
	    	case 4:     nomMes = "Abril"; 
	    				break;	    	
	    	case 5: 	nomMes = "Mayo"; 
	    				break;	    	
	    	case 6: 	nomMes = "Junio";
	    				break;	    	
	    	case 7: 	nomMes = "Julio"; 
	    				break;	    	
	    	case 8: 	nomMes = "Agosto"; 
	    				break;	    	
	    	case 9: 	nomMes = "Septiembre"; 
	    				break;	    	
	    	case 10: 	nomMes = "Octubre"; 
	    				break;	    	
	    	case 11: 	nomMes = "Noviembre";
	    				break;	    	
	    	case 12: 	nomMes = "Diciembre"; 
	    				break;	    	
	    	}
    		 vec_d = new Vector();
    		 vec_d.addElement(pi.getCoduni()+" - "+pi.getDesuni()); // 0 UNIDAD
    		 vec_d.addElement(pi.getItedoc()+" - "+ pi.getDesite()); // 1 - ITEM
    		 vec_d.addElement(nomMes.concat(" - N� " + pi.getNumreq()+""));// 3 - requerimiento    	    	
     		 vec_d.addElement(pi.getArea1().trim() + pi.getArea2().trim());// 2 - AREA
    		 vec_d.addElement(pi.getComen1().trim()+pi.getComen2().trim()+pi.getComen3().trim()+pi.getComen4().trim()+pi.getComen5().trim()+pi.getComen6().trim()); // 4 - DESCRIPCION
    		 vec_d.addElement(pi.getMotiv1().trim()+pi.getMotiv2().trim()+pi.getMotiv3().trim()+pi.getMotiv4().trim()+pi.getMotiv5().trim()+pi.getMotiv6().trim()); // 5 - MOTIVO
			 vec_d.addElement(pi.getCodman()); // 6 - ITEM
			 vec_d.addElement(pi.getTipcon()); // 7 - DETALLE
    		 if(pi.getCodman().trim().equals("COTIZACION") && pi.getPres03() > 0){
    			 String fecha = pi.getPres02()+"";
    			 vec_d.addElement(pi.getPres01()); // 9 - cotizacion
        		 vec_d.addElement(pi.getPres03()); // 10 - monto
        		 vec_d.addElement(fecha.substring(7)+"/"+fecha.substring(4,6)+"/"+fecha.substring(0,4)); // 10 - fecha
    		 } else {
    			 vec_d.addElement(pi.getCodigo()); // 8 - UNIDAD
    	     	 vec_d.addElement(pi.getValuni()); // 9 - COSTO
        		 vec_d.addElement(pi.getCanman()); // 10 - CANTIDAD       
    		 }
             vec_d.addElement(pi.getNumreq()); // 11 - NUMREQ
    		 vec_d.addElement(pi.getMespre()); // 12 - MES
    		 if(pi.getCanman() > 0 	|| pi.getPres03() > 0)
    		     listaReqDetResumenExcel.addElement(vec_d);
    	}
    	}
    	}
       	}
       	Long totalAnual = en + fe + ma + ab + my + jn + jl + ag + se + oc + no + di;
	    vec_d = new Vector();
	    vec_d.addElement(13);
	    vec_d.addElement("Total Anual");
	    vec_d.addElement("");
	    vec_d.addElement("");
	    vec_d.addElement("");
	    vec_d.addElement(totalAnual);
	    if(totalAnual > 0)
	    			listaReqResumenExcel.addElement(vec_d);
		/*fin requerimiento*/
    	moduloPresupuesto.agregaCorrienteExcel(accionweb.getReq(), vec_datos, listaReqResumenExcel, listaReqDetResumenExcel);
	
		
	}
	 synchronized public void cargaResumenAnual(AccionWeb accionweb) throws Exception {
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int codUnidad = Util.validaParametro(accionweb.getParameter("codUnidad"), 0);
		String nomUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"), "");
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");


		Vector vec_d = new Vector();
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		Long enero = new Long(0);
		Long febrero = new Long(0);
		Long marzo = new Long(0);
		Long abril = new Long(0);
		Long mayo = new Long(0);
		Long junio = new Long(0);
		Long julio = new Long(0);
		Long agosto = new Long(0);
		Long septiembre = new Long(0);
		Long octubre = new Long(0);
		Long noviembre = new Long(0);
		Long diciembre = new Long(0);
		Long sum_gasto_por_item = new Long(0);
		Long sum_totales_gasto = new Long(0);
		Long sum_totales_presup = new Long(0);
		
		PreswBean preswbean = new PreswBean("ERA", codUnidad,0,a�o,0, rutUsuario);
		preswbean.setDigide(dv);
		Vector vec_datos = new Vector();
		
		if(preswbean != null) {				
			listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
		}
			
		accionweb.agregarObjeto("detalleResumen", listaPresw25);
		accionweb.agregarObjeto("anno", a�o);
		accionweb.agregarObjeto("codUnidad", codUnidad);
		accionweb.agregarObjeto("nomUnidad", nomUnidad);
			
	}
	public void resumenAnualExcel(AccionWeb accionweb) throws Exception {
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int codUnidad = Util.validaParametro(accionweb.getParameter("codUnidad"), 0);
		int nomUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"), 0);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");


		Vector vec_d = new Vector();
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		Long enero = new Long(0);
		Long febrero = new Long(0);
		Long marzo = new Long(0);
		Long abril = new Long(0);
		Long mayo = new Long(0);
		Long junio = new Long(0);
		Long julio = new Long(0);
		Long agosto = new Long(0);
		Long septiembre = new Long(0);
		Long octubre = new Long(0);
		Long noviembre = new Long(0);
		Long diciembre = new Long(0);
		Long sum_gasto_por_item = new Long(0);
		Long sum_totales_gasto = new Long(0);
		Long sum_totales_presup = new Long(0);
		
		PreswBean preswbean = new PreswBean("ERA", codUnidad,0,a�o,0, rutUsuario);
		preswbean.setDigide(dv);
		Vector vec_datos = new Vector();
		
		if(preswbean != null) {				
			listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
		}
		for (Presw25DTO ss : listaPresw25){
			vec_d = new Vector();
			vec_d.add(ss.getItedoc()+" - "+ss.getDesite());
			vec_d.add(ss.getPres01());
			vec_d.add(ss.getPres02());
			vec_d.add(ss.getPres03());
			vec_d.add(ss.getPres04());
			vec_d.add(ss.getPres05());
			vec_d.add(ss.getPres06());
			vec_d.add(ss.getPres07());
			vec_d.add(ss.getPres08());
			vec_d.add(ss.getPres09());
			vec_d.add(ss.getPres10());
			vec_d.add(ss.getPres11());
			vec_d.add(ss.getPres12());
			if (ss.getItedoc() == 888) {
			  sum_totales_gasto = ss.getPres01() + ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() + ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() + ss.getPres10() + ss.getPres11() + ss.getPres12();
			  vec_d.add(sum_totales_gasto);
			  vec_d.add("*");
			  vec_datos.add(vec_d);
			}
			else if (ss.getItedoc() == 889) {
			      sum_totales_presup = ss.getPres01() + ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() + ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() + ss.getPres10() + ss.getPres11() + ss.getPres12();
			      vec_d.add("*");
				  vec_d.add(sum_totales_presup);
				  vec_datos.add(vec_d);
			}
			else {
					sum_gasto_por_item = ss.getPres01() + ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() + ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() + ss.getPres10() + ss.getPres11() + ss.getPres12();
					vec_d.add(sum_gasto_por_item);
					vec_d.add(ss.getTotpre());
					vec_datos.add(vec_d);
		    	}
			
		}
		
		moduloPresupuesto.agregaResumenAnualExcel(accionweb.getReq(), vec_datos);
			
	}

	public void informeDocumentoExcel(AccionWeb accionweb) throws Exception {
		  int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		  String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		  int codUnidad = Util.validaParametro(accionweb.getParameter("codUnidad"),0);
		  String nomUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"),"");
		  String opcion    = Util.validaParametro(accionweb.getParameter("opcion"),"");
		
		 // codUnidad = 111350;
		  Date fechaInicio  = null;
		  Date fechaTermino = null; 
		  int numdoc = 0;
		  int fecmov = 99999999;
		  
		  if (opcion.equals("FAC") || opcion.equals("IMP")) {
		   if ( accionweb.getParameter("fechaInicio") != null &&
			    accionweb.getParameter("fechaTermino") != null &&   
			   !accionweb.getParameter("fechaInicio").trim().equals("") && 
		       !accionweb.getParameter("fechaTermino").trim().equals("")) {
			 fechaInicio = Util.validaParametroFecha(accionweb.getParameter("fechaInicio"),new Date());
			 fechaTermino = Util.validaParametroFecha(accionweb.getParameter("fechaTermino"),new Date());
			 SimpleDateFormat outFmt = new SimpleDateFormat("yyyyMMdd");

			 String fechaIni = outFmt.format(fechaInicio);
			 String fechaTer = outFmt.format(fechaTermino);
			    
			 numdoc = Integer.parseInt(fechaIni);
			 fecmov = Integer.parseInt(fechaTer);
		   }
		 }
		 else {
		  numdoc = 0;
		  fecmov = 0;
		 }
			 if(numdoc > 20171231)	
				 numdoc = 20171231;
			 if(fecmov > 20171231)
				 fecmov = 20171231;	  
		 Collection<Presw21DTO> listaPresw21 = null;
		 Vector vec_detalle = null;
		 Vector vec_datos = new Vector();
		 if (codUnidad > 0) {
		   try {
		 	  PreswBean preswbean = new PreswBean(opcion,codUnidad,0,0,0,rutUsuario,numdoc,"",0,"","",0,0,0,fecmov,"","");
		      preswbean.setDigide(dv);
			  listaPresw21 = (Collection<Presw21DTO>) preswbean.consulta_presw21();
			  String fecha = "";
			  String diaFecha  = "";
			  String mesFecha  = "";
			  String annoFecha = "";
			  if(listaPresw21.size() > 0) {
				for (Presw21DTO inf : listaPresw21){
				  vec_detalle = new Vector();	
				  vec_detalle.addElement(inf.getNumdoc()); // 0
				  vec_detalle.addElement(inf.getTipdoc()); // 1
				  vec_detalle.addElement(inf.getGlosa1()); // 2
				  fecha = inf.getFecdoc()+"";
				  if (opcion.equals("FAC") || opcion.equals("IMP")) {
					  if(fecha.length() == 8) {
						  diaFecha  = fecha.substring(0,2);
						  mesFecha  = fecha.substring(2,4);
						  annoFecha = fecha.substring(4);
					  }  
					  else {
						  diaFecha  = "0" + fecha.substring(0,1);
						  mesFecha  = fecha.substring(1,3);
						  annoFecha = fecha.substring(3);
					  }
					  fecha = diaFecha + "/" + mesFecha + "/" + annoFecha;
				  }  
				  vec_detalle.addElement(fecha); // 3
				  vec_detalle.addElement(inf.getValor1()); // 4
				  vec_detalle.addElement(inf.getValor2()); // 5
				  vec_detalle.addElement(inf.getValor3()); // 6
				  vec_detalle.addElement(inf.getSucur()); // 7
				  vec_detalle.addElement(inf.getGlosa2()); // 8
				  vec_datos.addElement(vec_detalle); 
				 }
			  }
			  
		 	  } catch (Exception E) {
		        System.out.println("excepcion   " + E);
			   }
		  }	 
		  moduloPresupuesto.agregaInformeExcel(accionweb.getReq(), vec_datos);
		}
		
		public void informeDocumentoPDF(AccionWeb accionweb) throws Exception {
			  int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			  String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
			  int codUnidad = Util.validaParametro(accionweb.getParameter("codUnidad"),0);
			  String nomUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"),"");
			  String opcion    = Util.validaParametro(accionweb.getParameter("opcion"),"");
				
			  //codUnidad = 111350;
			  Date fechaInicio  = null;
			  Date fechaTermino = null; 
			  int numdoc = 0;
			  int fecmov = 99999999;
			  
			  if (opcion.equals("FAC") || opcion.equals("IMP")) {
			   if ( accionweb.getParameter("fechaInicio") != null &&
				    accionweb.getParameter("fechaTermino") != null &&   
				   !accionweb.getParameter("fechaInicio").trim().equals("") && 
			       !accionweb.getParameter("fechaTermino").trim().equals("")) {
				 fechaInicio = Util.validaParametroFecha(accionweb.getParameter("fechaInicio"),new Date());
				 fechaTermino = Util.validaParametroFecha(accionweb.getParameter("fechaTermino"),new Date());
				 SimpleDateFormat outFmt = new SimpleDateFormat("yyyyMMdd");

				 String fechaIni = outFmt.format(fechaInicio);
				 String fechaTer = outFmt.format(fechaTermino);
				    
				 numdoc = Integer.parseInt(fechaIni);
				 fecmov = Integer.parseInt(fechaTer);
			   }
			 }
			 else {
			  numdoc = 0;
			  fecmov = 0;
			 }
				 if(numdoc > 20171231)	
					 numdoc = 20171231;
				 if(fecmov > 20171231)
					 fecmov = 20171231;		  
			 Collection<Presw21DTO> listaPresw21 = null;
			 Vector vec_detalle = null;
			 Vector vec_datos = new Vector();
			 if (codUnidad > 0) {
			   try {
			 	  PreswBean preswbean = new PreswBean(opcion,codUnidad,0,0,0,rutUsuario,numdoc,"",0,"","",0,0,0,fecmov,"","");
			      preswbean.setDigide(dv);
				  listaPresw21 = (Collection<Presw21DTO>) preswbean.consulta_presw21();
				  String fecha = "";
				  String diaFecha  = "";
				  String mesFecha  = "";
				  String annoFecha = "";
				  if(listaPresw21.size() > 0) {
					for (Presw21DTO inf : listaPresw21){
					  vec_detalle = new Vector();	
					  vec_detalle.addElement(inf.getNumdoc()); // 0
					  vec_detalle.addElement(inf.getTipdoc()); // 1
					  vec_detalle.addElement(inf.getGlosa1()); // 2
					  fecha = inf.getFecdoc()+"";
					  if (opcion.equals("FAC") || opcion.equals("IMP")) {
						  if(fecha.length() == 8) {
							  diaFecha  = fecha.substring(0,2);
							  mesFecha  = fecha.substring(2,4);
							  annoFecha = fecha.substring(4);
						  }  
						  else {
							  diaFecha  = "0" + fecha.substring(0,1);
							  mesFecha  = fecha.substring(1,3);
							  annoFecha = fecha.substring(3);
						  }
						  fecha = diaFecha + "/" + mesFecha + "/" + annoFecha;
					  }	  
					  vec_detalle.addElement(fecha); // 3
					  vec_detalle.addElement(inf.getValor1()); // 4
					  vec_detalle.addElement(inf.getValor2()); // 5
					  vec_detalle.addElement(inf.getValor3()); // 6
					  vec_detalle.addElement(inf.getSucur()); // 7
					  vec_detalle.addElement(inf.getGlosa2()); // 8
					  vec_datos.addElement(vec_detalle); 
					 }
				  }
				  
			 	  } catch (Exception E) {
			        System.out.println("excepcion   " + E);
				   }
			  }	 
			  moduloPresupuesto.agregaInformePDF(accionweb.getReq(), vec_datos);
			}

	public void otrosingPDF(AccionWeb accionweb) throws Exception {
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");

		Vector vec_datos = new Vector();
		long total = 0;
		NumberFormat nf2 = 	NumberFormat.getInstance(Locale.GERMAN);
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		PreswBean preswbean = new PreswBean("ING", 0,0,a�o,0, rutUsuario);
		preswbean.setDigide(dv);
		if(preswbean != null) {
			listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();

		
			for (Presw25DTO ss : listaPresw25){		
				vec_datos.addElement(ss.getComen1());
				vec_datos.addElement(nf2.format(ss.getTotpre()));
				vec_datos.addElement(ss.getComen2().toString().trim()+ss.getComen3().toString().trim()+ss.getComen4().toString().trim()+ss.getComen5().toString().trim()+ss.getComen6().toString().trim());
				total += ss.getTotpre();
			}
			vec_datos.addElement(total + "");
		}
			
		moduloPresupuesto.agregaOtrosIngPDF(accionweb.getReq(), vec_datos);
	}
	
	public void remuneracionPDF(AccionWeb accionweb) throws Exception {
		// 06/10/2016 KP - Se cambian nombres de variables y etiquetas 'unidad(es)' por 'organizacion(es)'
		//20/10/2016 KP - Se obtienen m�s datos desde ASI para visualizar por PDF
		//int itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		String itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), "");
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		//int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"), "");
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
	   //	PreswBean preswbean = new PreswBean("ASI", unidad,0,a�o,0, rutUsuario);
		PreswBean preswbean = new PreswBean("ASI", 0,0,a�o,0, rutUsuario);
		preswbean.setCajero(organizacion);
		preswbean.setDigide(dv);
		if(preswbean != null) 
		  	listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();

	    NumberFormat nf2 = 	NumberFormat.getInstance(Locale.GERMAN); 
	   // String nomUnidad = "";
	    String nomOrganizacion = "";
	    String observacion = "";
	    Vector vec_datos = new Vector();
	    
		for (Presw25DTO ss : listaPresw25){	
			vec_datos.addElement(ss.getComen1());
			vec_datos.addElement(nf2.format(ss.getPres02()));
			vec_datos.addElement(nf2.format(ss.getPres03()));
			vec_datos.addElement(nf2.format(ss.getPres04()));
			vec_datos.addElement(nf2.format(ss.getPres05()));
			vec_datos.addElement(nf2.format(ss.getPres06()));
			vec_datos.addElement(nf2.format(ss.getPres07()));
			vec_datos.addElement(nf2.format(ss.getPres08()));
			vec_datos.addElement(nf2.format(ss.getPres09()));
			vec_datos.addElement(nf2.format(ss.getPres10()));
			vec_datos.addElement(nf2.format(ss.getPres11()));
			vec_datos.addElement(nf2.format(ss.getPres12()));
			vec_datos.addElement(nf2.format(ss.getValr11()));
			vec_datos.addElement(nf2.format(ss.getValr12()));			
			vec_datos.addElement(nf2.format(ss.getValr21()));	
			vec_datos.addElement(nf2.format(ss.getValr22()));	
			vec_datos.addElement(nf2.format(ss.getValr31()));	
			//nomUnidad = ss.getCoduni() + " - " + ss.getDesuni();
			nomOrganizacion = ss.getMotiv3() + " - " + ss.getDesuni();
			}
		//vec_datos.addElement(nomUnidad);
		vec_datos.addElement(nomOrganizacion);
		moduloPresupuesto.agregaRemuneracionPDF(accionweb.getReq(), vec_datos);
	}
	
	
public void nominaPDF(AccionWeb accionweb) throws Exception {
		//07/10/2016 KP - Se cambian nombres de variables y etiquetas 'unidad(es)' por 'organizacion(es)'		
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		//int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"), "");			
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");

		
	List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
   //	PreswBean preswbean = new PreswBean("CON", unidad,0,a�o,0, rutUsuario);
	PreswBean preswbean = new PreswBean("CON", 0,0,a�o,0, rutUsuario);
	preswbean.setCajero(organizacion);
 	preswbean.setDigide(dv);
	if(preswbean != null) {
	  	listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25(); //--> ACA NO TRAE NADA
	}
    Vector vec_datos = new Vector();
    NumberFormat nf2 = 	NumberFormat.getInstance(Locale.GERMAN); 
    String nomUnidad = "";
    String observacion = "";
    int i = 0;
	for (Presw25DTO ss : listaPresw25){	
		vec_datos.addElement(ss.getTipfun());
		vec_datos.addElement(nf2.format(ss.getRutide()) + " - " + ss.getDigide());
		vec_datos.addElement(ss.getNomfun());
		vec_datos.addElement(ss.getRespue());
		vec_datos.addElement(ss.getComen1().toString().trim()+ss.getComen2().toString().trim()+ss.getComen3().toString().trim()+ss.getComen4().toString().trim()+ss.getComen5().toString().trim()+ss.getComen6().toString().trim());
		if(i==0){
			//nomUnidad = ss.getCoduni() + " - " + ss.getDesuni();
			nomUnidad = ss.getCodgru().trim() + " - " + ss.getDesuni();
			observacion = ss.getMotiv1() + ss.getMotiv2() +  ss.getMotiv3() + ss.getMotiv4() +  ss.getMotiv5() + ss.getMotiv6()+ ss.getTipcon() + ss.getArea1() + ss.getArea2() + ss.getDesite();
		}
		i++;
		
	}
	
    vec_datos.addElement(nomUnidad);
	vec_datos.addElement(observacion);
	moduloPresupuesto.agregaNominaPDF(accionweb.getReq(), vec_datos );		
	}
	
	synchronized public void ejecucionRemuneracionExportar(AccionWeb accionweb) throws Exception {
		/* ERE ejecuci�n por remuneraci�n*/
		Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		accionweb.agregarObjeto("cuentasPresupuestarias", lista);

		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		String estamento = Util.validaParametro(accionweb.getParameter("estamento"), "");
		String parametroServicio = "";
		int codUnidad = Util.validaParametro(accionweb.getParameter("codUnidad"),0);
		Vector vec_detalle = new Vector();
		Vector vec_datos = new Vector();

		
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");

		  try {
			  Collection<Presw21DTO> listaPresw21 = null;
		      if (codUnidad > 0) {
				 
				 	PreswBean preswbean = new PreswBean("ERE", codUnidad, 0, anno, 0,	0,0, "", 0, "", "", 0, 0, 0, 0, "", estamento);
				 	preswbean.setRutide(rutUsuario);
					preswbean.setDigide(dv);
					listaPresw21 = (Collection<Presw21DTO>) preswbean.consulta_presw21();
					
					if (listaPresw21.size() == 0)
						listaPresw21 = null;
				
		
		     }
		      MathTool mathtool = new MathTool();
		      if(listaPresw21.size() > 0) {
		    		int porc = 0;
					int porc2 = 0;
					long sumValor1 = 0;
					long sumValor2 = 0;
					long sumValor3 = 0;
					int indice = 1;
		for (Presw21DTO inf : listaPresw21){
					porc = 0;
					porc2 = 0;
					if(indice < 13){
					sumValor1 = sumValor1 + inf.getValor1();
					sumValor2 = sumValor2 + inf.getValor2();
					sumValor3 = sumValor3 + inf.getValor3();
					if(inf.getValor1() > 0 && inf.getValor2() > 0){
						porc = mathtool.round(mathtool.mul(mathtool.div(inf.getValor2(),inf.getValor1()),100));
						porc2 = mathtool.round(mathtool.mul(mathtool.div(sumValor2,sumValor1),100));
					}
           }
				  vec_detalle = new Vector();	
				  vec_detalle.addElement(inf.getGlosa1().trim()); // 0
				  vec_detalle.addElement(inf.getValor1()); // 1
				  vec_detalle.addElement(inf.getValor2()); // 2
			      vec_detalle.addElement(porc); // 3
				  vec_detalle.addElement(inf.getValor3()); // 4
				  vec_detalle.addElement(sumValor1); // 5
				  vec_detalle.addElement(sumValor2); // 6
				  vec_detalle.addElement(porc2); // 7
				  vec_detalle.addElement(sumValor3); // 8
				  vec_datos.addElement(vec_detalle); 
				  indice = indice + 1;
				 }
			  }
			  
		 	  } catch (Exception E) {
		        System.out.println("excepcion   " + E);
			   }
		  
		  moduloPresupuesto.agregaEjecucionRemuneracionExportar(accionweb.getReq(), vec_datos);
		}
	
	public void ejecucionMensualExportar(AccionWeb accionweb) throws Exception {
		/*ETR TRIMESTRAL */
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		int codUnidad = Util.validaParametro(accionweb.getParameter("codUnidad"),0);
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		String nomUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"),"");
 	
		Vector vec_detalleET1 = new Vector();
		Vector vec_detalleET2 = new Vector();
		Vector vec_detalleET3 = new Vector();
		Vector vec_datos1 = new Vector();
		Vector vec_datos2 = new Vector();
		Vector vec_datos3 = new Vector();
		String valorSalida = "";

		String parametroServicio = "";
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");

		String nomMes = "";
		
		switch (mes) {
		case 1:
			nomMes = "Enero";
			break;
		case 2:
			nomMes = "Febrero";
			break;
		case 3:
			nomMes = "Marzo";
			break;
		case 4:
			nomMes = "Abril";
			break;
		case 5:
			nomMes = "Mayo";
			break;
		case 6:
			nomMes = "Junio";
			break;
		case 7:
			nomMes = "Julio";
			break;
		case 8:
			nomMes = "Agosto";
			break;
		case 9:
			nomMes = "Septiembre";
			break;
		case 10:
			nomMes = "Octubre";
			break;
		case 11:
			nomMes = "Noviembre";
			break;
		case 12:
			nomMes = "Diciembre";
			break;
		}
		
		  try {
			  Collection<Presw21DTO> listaPresw21 = null;
			   if (codUnidad > 0) {
			        
				 	PreswBean preswbean = new PreswBean("ETR", codUnidad, 0, anno, mes,	0,0, "", 0, "", "", 0, 0, 0, 0, "", "");
				 	preswbean.setRutide(rutUsuario);
					preswbean.setDigide(dv);
					listaPresw21 = (Collection<Presw21DTO>) preswbean.consulta_presw21();
					
					if (listaPresw21.size() == 0)
						listaPresw21 = null;
					accionweb.agregarObjeto("detalleMensual", listaPresw21);
					parametroServicio = "ETR";
			}
              MathTool mathtool = new MathTool();
		      /*if(listaPresw21.size() > 0) {
		    		int porc = 0;
					int porc2 = 0;
					long sumValor1 = 0;
					long sumValor2 = 0;
					long sumValor3 = 0;
					int ind = 1;
					 String line1 = "[";
		             String line2 = "[";
		             String conceptos = "[";
		             long max = 0;
		             boolean ingresa = true; // este sw es para que se haga solo 1 vez que es cuando se escribe et2
				for (Presw21DTO lista : listaPresw21){	
					vec_detalleET1 = new Vector();
					vec_detalleET2 = new Vector();
					vec_detalleET3 = new Vector();
				
				       if(lista.getTippro().equals("ET1")){
		                   if(lista.getTippag() >= 0 && !lista.getGlosa1().substring(0,5).equals("TOTAL"))          
		                   {
		                	   vec_detalleET1.addElement(lista.getTippag()+" - "+lista.getGlosa1());
		                	   vec_detalleET1.addElement(lista.getValor1());
		                	   vec_detalleET1.addElement(lista.getValor2());
		                	   vec_detalleET1.addElement(lista.getValor3());
		                	   vec_detalleET1.addElement(lista.getValor4());  
		                	   line1 = line1 + "[" + lista.getValor1() + "," + ind + "]";
		                	   line2 = line2 + "[" + lista.getValor3() + "," + ind + "]";
		                	   conceptos = conceptos + "'" + lista.getTippag()+" - "+lista.getGlosa1().trim() + "'" ;
		                	   if(lista.getValor3() > max)
		                		   max = lista.getValor3();
		                	   if(lista.getValor1() > max)
		                		   max = lista.getValor1();
		                   }
		                   else {
		                	   vec_detalleET1.addElement(lista.getGlosa1());
		                	   vec_detalleET1.addElement(lista.getValor1());
		                	   vec_detalleET1.addElement(lista.getValor2());
		                	   vec_detalleET1.addElement(lista.getValor3());
		                	   vec_detalleET1.addElement(lista.getValor4());                             
		                   }
		                   vec_datos1.addElement(vec_detalleET1);
		                   valorSalida = "ET1";
				       }else {
				    	   if(lista.getTippro().equals("ET2"))	{         
				    		   vec_detalleET2.addElement(lista.getGlosa1());
				    		   vec_detalleET2.addElement(lista.getValor1()); 
				    		   vec_datos2.addElement(vec_detalleET2);
				    	   }else{
				    		   if(lista.getTippro().equals("ET3"))	{
				    			   
				                    if(!lista.getGlosa1().substring(0,5).equals("TOTAL"))          
				                    {
				                    	vec_detalleET3.addElement(lista.getGlosa1());
				                    	vec_detalleET3.addElement(lista.getValor1());
				                    	vec_detalleET3.addElement(lista.getValor2());
				                    	vec_detalleET3.addElement(lista.getValor3());
				                    	line1 = line1 + "[" + lista.getValor1() + "," + ind + "]";
					                	line2 = line2 + "[" + lista.getValor2() + "," + ind + "]";
					                	conceptos = conceptos + "'" + lista.getGlosa1().trim()+ "'" ;
					                	 if(lista.getValor2() > max)
					                		   max = lista.getValor2();
					                	 if(lista.getValor1() > max)
					                		   max = lista.getValor1();
				                    }
				                    else {
				                    	vec_detalleET3.addElement(lista.getGlosa1());
				                    	vec_detalleET3.addElement(lista.getValor1());
				                    	vec_detalleET3.addElement(lista.getValor2());
				                    	vec_detalleET3.addElement(lista.getValor3());                              
				                    }
				                    vec_datos3.addElement(vec_detalleET3);
				    	   }
				       }
				
				 }
				   ind = ind + 1;
				   if((lista.getTippro().equals("ET1") || lista.getTippro().equals("ET3")) && !lista.getGlosa1().substring(0,5).equals("TOTAL")){
				   if(ind < (listaPresw21.size())){
            		   line1 = line1 + ",";
            		   line2 = line2 + ",";
            		   conceptos = conceptos + ",";
				   } else {
            		   line1 = line1 + "]";
            		   line2 = line2 + "]";
            		   conceptos = conceptos + "]";
				   }
				   } else {
					   if(lista.getTippro().equals("ET2") && ingresa){
						   line1 = line1.substring(0,line1.length()-1) + "]";
	            		   line2 = line2.substring(0,line2.length()-1) + "]";
	            		   conceptos = conceptos.substring(0,conceptos.length()-1) + "]";
	            		   ingresa = false;
					   }
				   }
			
			  }
				//calculo del maximo del grafico
				
				String maxiValor = String.valueOf(max);
				String valorRec = "";
				for(int i = 1;i<(maxiValor.length()-1);i++){
					valorRec = valorRec + "0";
				}
				valorRec = String.valueOf(Integer.parseInt(maxiValor.substring(0,2)) + 1) + valorRec;
				
				//((cantidad de elementos (ejeY/2) * 100) + 100)px 

				int estilo = (((listaPresw21.size()-1)/2) * 100) + 100;
			//	System.out.println("line1: "+ line1);
			//	System.out.println("line2: "+ line2);
			//	System.out.println("concepto: "+ conceptos);
				accionweb.agregarObjeto("valorSalida", valorSalida);
				accionweb.agregarObjeto("line1", line1);
				accionweb.agregarObjeto("line2", line2);
				accionweb.agregarObjeto("conceptos", conceptos);
				accionweb.agregarObjeto("maximo", valorRec);
				accionweb.agregarObjeto("estilo", estilo+"px");
				accionweb.agregarObjeto("nomUnidad", nomUnidad);
				accionweb.agregarObjeto("anno", anno);
				accionweb.agregarObjeto("nomMes", nomMes);
		      }*/
              
              // nuevo chart con jquery
              int margen = 0;
		      if(listaPresw21.size() > 0) {
		    		int porc = 0;
					int porc2 = 0;
					long sumValor1 = 0;
					long sumValor2 = 0;
					long sumValor3 = 0;
					//int ind = 1;
					 String line1 = "[";
		             String line2 = "[";
		             String coma = "";
		             String conceptos = "[";
		             long max = 0;
		             boolean ingresa = true; // este sw es para que se haga solo 1 vez que es cuando se escribe et2
				for (Presw21DTO lista : listaPresw21){	
					vec_detalleET1 = new Vector();
					vec_detalleET2 = new Vector();
					vec_detalleET3 = new Vector();
				
				       if(lista.getTippro().equals("ET1")){
		                   if(lista.getTippag() >= 0 && !lista.getGlosa1().substring(0,5).equals("TOTAL"))          
		                   {
		                	   vec_detalleET1.addElement(lista.getTippag()+" - "+lista.getGlosa1().trim());
		                	   vec_detalleET1.addElement(lista.getValor1());
		                	   vec_detalleET1.addElement(lista.getValor2());
		                	   vec_detalleET1.addElement(lista.getValor3());
		                	   vec_detalleET1.addElement(lista.getValor4());  
		                	   line1 = line1 + coma + lista.getValor1();
		                	   line2 = line2 + coma + lista.getValor3();
		                	   
		                	   int maxc = lista.getGlosa1().trim().length();
			               if (maxc > 25) maxc = 25;
			                   margen = 180;
                  	   conceptos = conceptos + coma + "'" + lista.getTippag()+" - "+lista.getGlosa1().trim().substring(0,maxc) + "'" ;
                	   if(lista.getValor3() > max)
		                		   max = lista.getValor3();
		                	   if(lista.getValor1() > max)
		                		   max = lista.getValor1();
		                   }
		                   else {
		                	   vec_detalleET1.addElement(lista.getGlosa1());
		                	   vec_detalleET1.addElement(lista.getValor1());
		                	   vec_detalleET1.addElement(lista.getValor2());
		                	   vec_detalleET1.addElement(lista.getValor3());
		                	   vec_detalleET1.addElement(lista.getValor4());                             
		                   }
		                   vec_datos1.addElement(vec_detalleET1);
		                   valorSalida = "ET1";
				       }else {
				    	   if(lista.getTippro().equals("ET2"))	{         
				    		   vec_detalleET2.addElement(lista.getGlosa1());
				    		   vec_detalleET2.addElement(lista.getValor1()); 
				    		   vec_datos2.addElement(vec_detalleET2);
				    	   }else{
				    		   if(lista.getTippro().equals("ET3"))	{
				    			   
				                    if(!lista.getGlosa1().substring(0,5).equals("TOTAL"))          
				                    {
				                    	vec_detalleET3.addElement(lista.getGlosa1());
				                    	vec_detalleET3.addElement(lista.getValor1());
				                    	vec_detalleET3.addElement(lista.getValor2());
				                    	vec_detalleET3.addElement(lista.getValor3());
				                    	line1 = line1 + coma + lista.getValor1();
					                	line2 = line2 + coma + lista.getValor2();
			              	            int maxc = lista.getGlosa1().trim().length();
					             	if (maxc > 15) maxc = 15;
					                	margen = 100;
    	conceptos = conceptos + coma + "'" + lista.getGlosa1().trim().substring(0,maxc)+ "'" ;
					                	 if(lista.getValor2() > max)
					                		   max = lista.getValor2();
					                	 if(lista.getValor1() > max)
					                		   max = lista.getValor1();
				                    }
				                    else {
				                    	vec_detalleET3.addElement(lista.getGlosa1());
				                    	vec_detalleET3.addElement(lista.getValor1());
				                    	vec_detalleET3.addElement(lista.getValor2());
				                    	vec_detalleET3.addElement(lista.getValor3());                              
				                    }
				                    vec_datos3.addElement(vec_detalleET3);
				    	   }
				       }
				
				 }
			
				   if(lista.getGlosa1().substring(0,5).equals("TOTAL")){
					   line1 = line1 + "]";
	          		   line2 = line2 + "]";
	          		   conceptos = conceptos + "]";
				   }
				   coma = ",";
			
			  }
				/*calculo del maximo del grafico*/
				
				String maxiValor = String.valueOf(max);
				String valorRec = "";
				int estilo = (((listaPresw21.size()-1)/2) * 100) + 100;
				accionweb.agregarObjeto("valorSalida", valorSalida);
				accionweb.agregarObjeto("line1", line1);
				accionweb.agregarObjeto("line2", line2);
				accionweb.agregarObjeto("conceptos", conceptos);
				accionweb.agregarObjeto("maximo", valorRec);
				accionweb.agregarObjeto("estilo", estilo+"px");
				accionweb.agregarObjeto("nomUnidad", nomUnidad);
				accionweb.agregarObjeto("anno", anno);
				accionweb.agregarObjeto("nomMes", nomMes);
		  		accionweb.agregarObjeto("margen", margen);
	    }
			  
		 	  } catch (Exception E) {
		        System.out.println("excepcion   " + E);
			   }
		  
		  moduloPresupuesto.agregaEjecucionMensualExportar(accionweb.getReq(), vec_datos1, vec_datos2, vec_datos3);
		}
	public void presupIngresoExportar(AccionWeb accionweb) throws Exception {
		List<Presw25DTO> listado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listado"); 
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int ingreso = Util.validaParametro(accionweb.getParameter("cargaIngreso"), 0);
		String nomSede = Util.validaParametro(accionweb.getParameter("nomSede"), "");                        
		String nomUni = Util.validaParametro(accionweb.getParameter("nomUni"), "");  
		String nomDoc = Util.validaParametro(accionweb.getParameter("nomDoc"), ""); 

		
		Vector vec_detalleET1 = new Vector();
		Vector vec_detalleET2 = new Vector();
		Vector vec_detalleET3 = new Vector();
		Vector vec_datos1 = new Vector();
		Vector vec_datos2 = new Vector();
		Vector vec_datos3 = new Vector();
		String valorSalida = "";

		String parametroServicio = "";
		String titulo = "Ingreso Pregrado Diurno USM " + anno;
		String label_line6 = "Pago por Caja";
		String label_line5 = "Cred. Univ."; 
		String label_line3 = "Becas Gobierno";
		String label_line1 = "Becas Privadas";
		String label_line2 = "Becas USM";
		String label_line4 = "CAE";
		String label_line7 = "";

		String subtitulo = "";
		if(ingreso > 1)
			subtitulo = "Por Sede " + nomSede;
		if(ingreso > 2)
		   subtitulo = subtitulo + " , Departamento " + nomUni;
		   
		if(ingreso > 3)
		   subtitulo = subtitulo + ", Carrera " + nomDoc;
	
		
		
		  try {
		
              MathTool mathtool = new MathTool();
                        
              // nuevo chart con jquery
              int margen = 0;
		      if(listado.size() > 0) {
		    		int porc = 0;
					int porc2 = 0;
					long sumValor1 = 0;
					long sumValor2 = 0;
					long sumValor3 = 0;
					//int ind = 1;
					 String line1 = "[";
		             String line2 = "[";
		             String line3 = "[";
		             String line4 = "[";
		             String line5 = "[";
		             String line6 = "[";
		             String line7 = "[";
		             String coma = "";
		             String conceptos = "[";
		             long max = 0;
		             String nombre = "";
		             int index = -1;
		             boolean ingresa = true; // este sw es para que se haga solo 1 vez que es cuando se escribe et2
		             int inicio = 0;
				for (Presw25DTO lista : listado){	
					vec_detalleET1 = new Vector();
					vec_detalleET2 = new Vector();
					vec_detalleET3 = new Vector();
					index = -1;
			        line6 = line6 + coma + lista.getPres02();
		            line5 = line5 + coma + lista.getPres03();
		            line3 = line3 + coma + lista.getPres04();
		            line1 = line4 + coma + lista.getPres05();
		            line2 = line2 + coma + lista.getPres06();
		            line4 = line4 + coma + lista.getPres07();
		                	   
		            int maxc = lista.getComen1().trim().length();
			        if (maxc > 25) maxc = 25;
			            margen = 130;
			            if(lista.getMespre() == 6)
			            	   conceptos = conceptos + coma + "'ACA '" ;
			               else{
			            	   nombre =   lista.getComen1().trim(); 
			            	   index = nombre.indexOf("DEPARTAMENTO DE");
			            	   if(index < 0){
			            		   index = nombre.indexOf("DEPARTAMENTO");
			            		   if(index < 0){
				            		   index = nombre.indexOf("CARRERA DE");
				            		   if(index < 0){
					            		   index = nombre.indexOf("CARRERA");
					            		   if(index < 0){
					            			   index = nombre.indexOf("DEPTO.");
					            			   if(index < 0){
					            				   index = nombre.indexOf("LICENCIATURA");
					            				   if(index >= 0){
					            					   nombre = "LIC." + nombre.substring(index+12);  
					            				   }
					            			   }else
					            				   nombre = nombre.substring(index+9);
					            		   }else
					            			   nombre = nombre.substring(index+8);
				            		   } else nombre = nombre.substring(index+11);
			            		   } else nombre = nombre.substring(index+13);
			            	   } else nombre = nombre.substring(index+16);
			            	   conceptos = conceptos + coma + "'" + nombre.trim() + "'" ;
			               }
		                
		                   coma = ",";
				}
				
			
				       line1 = line1 + "]";
	          		   line2 = line2 + "]";
	          		   line3 = line3 + "]";
	          		   line4 = line4 + "]";
	          		   line5 = line5 + "]";
	          		   line6 = line6 + "]";
	          		   line7 = line7 + "]";
	          		   conceptos = conceptos + "]";	  		 
			
	          		String series = "[{ name:'" + label_line1 + "', data:" + line1+ "}, { name:'" + label_line2+"', data:" + line2 + "}";
	          		if(!label_line3.trim().equals(""))
	          			series += ", {name:'" + label_line3 + "', data:" + line3 + "}";
	          		if(!label_line4.trim().equals(""))
	          			series += ", {name:'" + label_line4 + "', data:" + line4 + "}";
	          		if(!label_line5.trim().equals(""))
	          			series += ", {name:'" + label_line5 + "', data:" + line5 + "}";
	          		if(!label_line6.trim().equals(""))
	          			series += ", {name:'" + label_line6 + "', data:" + line6 + "}";
	          		if(!label_line7.trim().equals(""))
	          			series += ", {name:'" + label_line7 + "', data:" + line7 + "}";
	          		series += "]";
				/*calculo del maximo del grafico*/
				
				String maxiValor = String.valueOf(max);
				String valorRec = "";
				int estilo = (((listado.size()-1)/2) * 100) + 100;
				accionweb.agregarObjeto("valorSalida", valorSalida);
				accionweb.agregarObjeto("series", series);			
				accionweb.agregarObjeto("conceptos", conceptos);
				accionweb.agregarObjeto("maximo", valorRec);
				accionweb.agregarObjeto("estilo", estilo+"px");		
		  		accionweb.agregarObjeto("margen", margen);
		  		accionweb.agregarObjeto("titulo", titulo);
		  		accionweb.agregarObjeto("subtitulo", subtitulo);
		  		accionweb.agregarObjeto("chart", "chartStacked.js");		
		  		
	    }
			  
		 	  } catch (Exception E) {
		        System.out.println("excepcion   " + E);
			   }
		  
	}
	public void presupSerieTiempoExportar(AccionWeb accionweb) throws Exception {
		List<Presw25DTO> listado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listado"); 
		String tipo = Util.validaParametro(accionweb.getParameter("tipo"), "");
		int tipoGraf = Util.validaParametro(accionweb.getParameter("tipoGraf"), 0);
	
		Vector vec_detalleET1 = new Vector();
		Vector vec_detalleET2 = new Vector();
		Vector vec_detalleET3 = new Vector();
		Vector vec_datos1 = new Vector();
		Vector vec_datos2 = new Vector();
		Vector vec_datos3 = new Vector();
		String valorSalida = "";
		String chart = "";
		if(tipoGraf == 1)
			chart= "chartStacked.js";
		else
			chart = "chartLine.js";
		 int margen = 130;
	      
		String parametroServicio = "";
		String titulo = "Series de Tiempo e Informaci�n Estad�stica ";
		String label_line1 = "";
		String label_line2 = ""; 
		String label_line3 = "";
		String label_line4 = "";
		String label_line5 = "";
		String label_line6 = "";
		String label_line7 = "";
		String subtitulo = "";
		String coma = "";
		String conceptos = "[";
        if(tipo.trim().equals("AFD")) {
        	 label_line1 = "95% de Fondo";
    		 label_line2 = "5% de Fondo"; 
    		 label_line3 = "";
    		 label_line4 = "";
    		 label_line5 = "";
    		 label_line6 = "";
    		 label_line7 = "";
    		 subtitulo = "Aporte Fiscal Directo (AFD)";
    		 margen = 150;
        }
        if(tipo.trim().equals("FSC") ) {
       	 label_line1 = "Total";   
   		 subtitulo = "Fondos Solidario de Cr�dito Universitario";
       	
       }
        if(tipo.trim().equals("FGB") ) {
          	 label_line1 = "Total";   
      		 subtitulo = "Fondos Gubernamentales para Becas de Arancel";
          	
          }
        if(tipo.trim().equals("STP")){
        	String nomSede = Util.validaParametro(accionweb.getParameter("nomSede"), "");
    		String nomDepartamento = Util.validaParametro(accionweb.getParameter("nomDepartamento"), "");
    		String nomCarrera = Util.validaParametro(accionweb.getParameter("nomCarrera"), "");
    		String nomMencion = Util.validaParametro(accionweb.getParameter("nomMencion"), "");
    		titulo = "Financiamiento Ingresos por Arancel Anual";
        	subtitulo = "Sede " + nomSede.trim() + ", Departamento " + nomDepartamento.trim() + ", Carrera " + nomCarrera.trim() +", Menci�n " + nomMencion.trim();
        	margen = 190;
        }
        if(tipo.trim().equals("AFI")) {
        	margen = 180;
           	List<Cocof17DTO> listCocofBean = new ArrayList<Cocof17DTO>();
			listCocofBean = moduloPresupuesto.getListaSedeAlterada(accionweb);
		    subtitulo = "Aporte Fiscal Indirecto (AFI)";
	       	int i = 0;   
			 
			for (Cocof17DTO lista : listCocofBean){
		  	    i++;
				
			   	switch(i){
            	case 1:    label_line1 = lista.getNomsuc().trim()+"";
            	break;
            	case 2: label_line2 = lista.getNomsuc().trim()+"";
            	break;
            	case 3:    label_line3 = lista.getNomsuc().trim()+"";
            	break;
            	case 4: label_line4 = lista.getNomsuc().trim()+"";
            	break;
            	case 5:    label_line5 = lista.getNomsuc().trim()+"";
            	break;
            	case 6: label_line6 = "ACA";
            	break;
            	case 7: label_line7 = lista.getNomsuc().trim()+"";
            	break;
            	} 
	                 
       	
       }
        } else   conceptos = "[";
        
		
		
		  try {
		
                  // nuevo chart con jquery
             if(listado.size() > 0) {
    
					//int ind = 1;
					 String line1 = "[";
		             String line2 = "[";
		             String line3 = "[";
		             String line4 = "[";
		             String line5 = "[";
		             String line6 = "[";
		             String line7 = "[";
		             coma = "";
		             long max = 0;
		             int k=0;
		         	int annoIni = Util.validaParametro(accionweb.getParameter("annoIni"), 0);
		        	int annoFin = Util.validaParametro(accionweb.getParameter("annoFin"), 0);
		        
		      	for (Presw25DTO lista : listado){
		   			vec_detalleET1 = new Vector();
					vec_detalleET2 = new Vector();
					vec_detalleET3 = new Vector();
					if(tipo.trim().equals("STP")) {
						  coma = ",";
					  	k++;
			        	 switch (k){
		     			 case 1:  {  label_line4 = lista.getDesite().trim()+"";
		     			        line4 = line4 + lista.getPres07()+ coma + lista.getPres06()+ coma + lista.getPres05() + coma + lista.getPres04() + coma + lista.getPres03() + coma + lista.getPres02() + coma + lista.getPres01();
  			                    conceptos = conceptos + "'" + annoIni + "'" ;
		     			 		break;
		     			 }
		     			 case 2:{ label_line3 = lista.getDesite().trim()+"";
		     			    line3 = line3 + lista.getPres07()+ coma + lista.getPres06()+ coma + lista.getPres05() + coma + lista.getPres04() + coma + lista.getPres03() + coma + lista.getPres02() + coma + lista.getPres01();
		                    conceptos = conceptos + coma + "'" + (annoIni + 1)+ "'" ;
     		
		     			 		break;
		     			 }
		     			 case 3: {   label_line2 = lista.getDesite().trim()+"";
		     			    line2 = line2 + lista.getPres07()+ coma + lista.getPres06()+ coma + lista.getPres05() + coma + lista.getPres04() + coma + lista.getPres03() + coma + lista.getPres02() + coma + lista.getPres01();
		                    conceptos = conceptos + coma + "'" + (annoIni+2)+ "'" ;
     		            	break;
		     			 }
		     			 case 4: {label_line1 = lista.getDesite().trim()+"";
		     			    line1 = line1 + lista.getPres07()+ coma + lista.getPres06()+ coma + lista.getPres05() + coma + lista.getPres04() + coma + lista.getPres03() + coma + lista.getPres02() + coma + lista.getPres01();
		                    conceptos = conceptos + coma + "'" + (annoIni+3)+ "'" ;
     				       break;
		     			 }
		     			 case 5:{ // label_line5 = lista.getDesite().trim()+"";
		     			 	//line5 = line5+ lista.getPres07()+ coma + lista.getPres06()+ coma + lista.getPres05() + coma + lista.getPres04() + coma + lista.getPres03() + coma + lista.getPres02() + coma + lista.getPres01();
		                    conceptos = conceptos + coma + "'" + (annoIni+4)+ "'" ;
		                    break;
		     			 }
		     			 case 6:{ //label_line6 = lista.getDesite().trim()+"";
		     			 	//line6 = line6 + lista.getPres07()+ coma + lista.getPres06()+ coma + lista.getPres05() + coma + lista.getPres04() + coma + lista.getPres03() + coma + lista.getPres02() + coma + lista.getPres01();
		                    conceptos = conceptos + coma + "'" + (annoIni+5)+ "'" ;
		                    break;
		     			 }
		     			 case 7: {//label_line7 = lista.getDesite().trim()+"";
		     			 	//line7 = line7 + lista.getPres07()+ coma + lista.getPres06()+ coma + lista.getPres05() + coma + lista.getPres04() + coma + lista.getPres03() + coma + lista.getPres02() + coma + lista.getPres01();
		                    conceptos = conceptos + coma + "'" + (annoIni+6)+ "'" ;     		
		     			        break;
		     			 }
		     			 }
			      
  			          
  			        }
           			else {
				    line1 = line1 + coma + lista.getPres01();
		            if(tipo.trim().equals("AFI")) {
		            	line2 = line2 + coma + lista.getPres02();
		   		        line3 = line3 + coma + lista.getPres03();
				        line4 = line4 + coma + lista.getPres04();
				        line5 = line5 + coma + lista.getPres05();
			            line6 = line6 + coma + lista.getPres06();
			            line7 = line7 + coma + lista.getPres07();
			            conceptos = conceptos + coma + "'" + lista.getAnopre() + "'" ;
			          
		         		            	
		        
		            } else {           
		            		
		            	
		           				if(tipo.trim().equals("AFD")) 
		           					line2 = line2 + coma + lista.getPres02();
		            
		           				conceptos = conceptos + coma + "'" + lista.getAnopre() + "'" ;
		            			}
		            }
                    coma = ",";
				}				
			
				       line1 = line1 + "]";
	          		   line2 = line2 + "]";
	          		   line3 = line3 + "]";
	          		   line4 = line4 + "]";
	          		   line5 = line5 + "]";
	          		   line6 = line6 + "]";
	          		   line7 = line7 + "]";
	          		   conceptos = conceptos + "]";	  
	          		   
	          		 String series = "[{ name:'" + label_line1 + "', data:" + line1+ "}";
	          		 	if(!label_line2.trim().equals(""))		          		
	          		        series += ", { name:'" + label_line2+"', data:" + line2 + "}";
		          		if(!label_line3.trim().equals(""))
		          			series += ", {name:'" + label_line3 + "', data:" + line3 + "}";
		          		if(!label_line4.trim().equals(""))
		          			series += ", {name:'" + label_line4 + "', data:" + line4 + "}";
		          		if(!label_line5.trim().equals(""))
		          			series += ", {name:'" + label_line5 + "', data:" + line5 + "}";
		          		if(!label_line6.trim().equals(""))
		          			series += ", {name:'" + label_line6 + "', data:" + line6 + "}";
		          		if(!label_line7.trim().equals(""))
		          			series += ", {name:'" + label_line7 + "', data:" + line7 + "}";
		          		series += "]";
			  
				/*calculo del maximo del grafico*/
				
				String maxiValor = String.valueOf(max);
				String valorRec = "";
				int estilo = (((listado.size()-1)/2) * 100) + 100;
				accionweb.agregarObjeto("valorSalida", valorSalida);
				accionweb.agregarObjeto("series", series);
				accionweb.agregarObjeto("conceptos", conceptos);
				accionweb.agregarObjeto("maximo", valorRec);
				accionweb.agregarObjeto("estilo", estilo+"px");		
		  		accionweb.agregarObjeto("margen", margen);
		  		accionweb.agregarObjeto("titulo", titulo);
		  		accionweb.agregarObjeto("subtitulo", subtitulo);
		  		accionweb.agregarObjeto("chart", chart);		  		
		  		
	    }
			  
		 	  } catch (Exception E) {
		        System.out.println("excepcion   " + E);
			   }
		  
		}

	public ModuloPerfiles getModuloPerfiles() {
		return moduloPerfiles;
	}

	public void setModuloPerfiles(ModuloPerfiles moduloPerfiles) {
		this.moduloPerfiles = moduloPerfiles;
	}

	public String getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(String funcionario) {
		this.funcionario = funcionario;
	}
	public void limpiaSimulador(AccionWeb accionweb) throws Exception {
		if (accionweb.getSesion().getAttribute("listaUnidad") != null)
			accionweb.getSesion().setAttribute("listaUnidad", null);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutOrigen  = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen") +"","0"));
		String nomSimulacion = "";
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		
		/*hacer este rut rutUsuario*/
		if(rutOrigen != 0) // lo vuelve a lo original
		{
			rutUsuario = rutOrigen ;
			rutOrigen = 0;
		}
		 
		
		accionweb.getSesion().setAttribute("rutUsuario", rutUsuario);
		accionweb.getSesion().setAttribute("rutOrigen", rutOrigen);
		accionweb.getSesion().setAttribute("nomSimulacion", nomSimulacion);
		accionweb.agregarObjeto("rutOrigen", rutOrigen);
		accionweb.agregarObjeto("nomSimulacion", nomSimulacion);
		
	}
	public void cargaIngreso(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		List<Presw25DTO> listado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listado");
		if(listado != null && listado.size() == 0)
		     accionweb.getSesion().removeAttribute("listado");
		if(rutOrigen > 0){
			this.limpiaSimulador(accionweb);
		}
		accionweb.agregarObjeto("cargar", "cargar();");
		accionweb.agregarObjeto("cargaIngreso", 1);	
		if(rutUsuario > 0 && anno > 0){
		
		try {
			List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
			listaPresw25 = moduloPresupuesto.getListaPresw25("ANO", rutUsuario, anno, dv, 0,0,0);	
		

			accionweb.agregarObjeto("listaIngreso", listaPresw25);	
			moduloPresupuesto.agregaListado(accionweb.getReq(), listaPresw25);
			
			/*esto para la auditoria*/
			String host = accionweb.getReq().getLocalAddr();
			DetalleServicio detalleServicio = new DetalleServicio();
			detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(30));
			TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();
			tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(1);
			String parametro = "ANO";
		
			
			AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
			auditoriaServicio.setDetalleServicio(detalleServicio);
			auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
			auditoriaServicio.setFecha(new Date());
			auditoriaServicio.setHost(host);
			auditoriaServicio.setParametro(parametro);
			if(rutOrigen > 0)
			auditoriaServicio.setUsuario(rutOrigen);
		else
			auditoriaServicio.setUsuario(rutUsuario);
			moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
			
			//System.out.println("origen: "+accionweb.getSesion().getAttribute("rutOrigen"));
			log.debug("Si guardada auditoria cuentasPresupuestarias");
			
		} catch (Exception e) {
			log.debug("No guardada auditoria cuentasPresupuestarias");
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		}	
		accionweb.agregarObjeto("menuSerieTiempo", 1); // es para no mostrar el menu en ingreso

	}
	public void cargaMenuDocumento(AccionWeb accionweb) throws Exception {
		accionweb.agregarObjeto("menuSerieTiempo", 1); // es para no mostrar el menu en D
	}
		
	public void cargarMenuPlanDesarrollo(AccionWeb accionweb) throws Exception {
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
      	accionweb.agregarObjeto("control", control);
  
		accionweb.agregarObjeto("menuPlanDesarrollo", 1); // es para no mostrar el menu en Plan de desarrollo
	}
	
	
	
	public void cargarPresupSolicitudes(AccionWeb accionweb) throws Exception {
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
      	accionweb.agregarObjeto("control", control);
  
		accionweb.agregarObjeto("menuPresupSolicitudes", 1); // es para mostrar el menu  de solicitudes
	}
	
	public void cargaIngresoDepartamento(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int sucur = Util.validaParametro(accionweb.getParameter("sucur"), 0);
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");		
		String nomSede = Util.validaParametro(accionweb.getParameter("nomSede"),"");
		List<Presw25DTO> listado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listado");
		if(listado != null && listado.size() == 0)
		     accionweb.getSesion().removeAttribute("listado");
		accionweb.agregarObjeto("cargar", "cargarDepartamento(" + sucur + "," + nomSede + ");");
		accionweb.agregarObjeto("cargaIngreso", 2);
		
		if(rutOrigen > 0){
			rutUsuario = rutOrigen;
			accionweb.getSesion().setAttribute("rutOrigen", rutOrigen);
			accionweb.getSesion().setAttribute("rutUsuario", rutUsuario);
		}
		if(rutUsuario > 0 && anno > 0){

			try {
				List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
				listaPresw25 = moduloPresupuesto.getListaPresw25("ANS", rutUsuario, anno, dv, sucur,0,0);			
				accionweb.agregarObjeto("listaIngresoDepartamento", listaPresw25);
				moduloPresupuesto.agregaListado(accionweb.getReq(), listaPresw25);
				
				String host = accionweb.getReq().getLocalAddr();
				accionweb.agregarObjeto("nomSede", nomSede);
				accionweb.agregarObjeto("sucur", sucur);
				accionweb.agregarObjeto("anno", anno);
			
			
			
				DetalleServicio detalleServicio = new DetalleServicio();
				detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(30));
				TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();
				tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(1);
				String parametro = "ANS";
			
			
				AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
				auditoriaServicio.setDetalleServicio(detalleServicio);
				auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
				auditoriaServicio.setFecha(new Date());
				auditoriaServicio.setHost(host);
				auditoriaServicio.setParametro(parametro);
				if(rutOrigen > 0)
					auditoriaServicio.setUsuario(rutOrigen);
				else
					auditoriaServicio.setUsuario(rutUsuario);
				moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
				
				//System.out.println("origen: "+accionweb.getSesion().getAttribute("rutOrigen"));
				log.debug("Si guardada auditoria cargaIngresoCarrera");
				
			} catch (Exception e) {
				log.debug("No guardada auditoria cargaIngresoCarrera");
				accionweb.agregarObjeto("mensaje", "No guardado");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
			}	
			accionweb.agregarObjeto("menuSerieTiempo", 1); // es para no mostrar el menu en ingreso

	}
	public void cargaIngresoCarrera(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int sucur = Util.validaParametro(accionweb.getParameter("sucur"), 0);
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int codUni = Util.validaParametro(accionweb.getParameter("codUni"), 0);
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		String nomSede = Util.validaParametro(accionweb.getParameter("nomSede"),"");
		String nomUni = Util.validaParametro(accionweb.getParameter("nomUni"),"");
		List<Presw25DTO> listado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listado");
		if(listado != null && listado.size() == 0)
		     accionweb.getSesion().removeAttribute("listado");
	
		if(rutOrigen > 0){
			rutUsuario = rutOrigen;
			accionweb.getSesion().setAttribute("rutOrigen", rutOrigen);
			accionweb.getSesion().setAttribute("rutUsuario", rutUsuario);
		}
		accionweb.agregarObjeto("cargar", "cargarCarrera(" + codUni + "," + nomUni + ");");
		accionweb.agregarObjeto("cargaIngreso", 3);
		if(rutUsuario > 0 && anno > 0){
				try {
					List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
					listaPresw25 = moduloPresupuesto.getListaPresw25("ASD", rutUsuario, anno, dv, sucur,codUni,0);	
					accionweb.agregarObjeto("listaIngresoCarrera", listaPresw25);
					moduloPresupuesto.agregaListado(accionweb.getReq(), listaPresw25);
					
					String host = accionweb.getReq().getLocalAddr();
					accionweb.agregarObjeto("nomSede", nomSede);
					accionweb.agregarObjeto("sucur", sucur);
					accionweb.agregarObjeto("anno", anno);
					accionweb.agregarObjeto("nomUni", nomUni);
					accionweb.agregarObjeto("codUni", codUni);
					
					DetalleServicio detalleServicio = new DetalleServicio();
					detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(30));
					TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();
					tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(1);
					String parametro = "ASD";
				
					
					AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
					auditoriaServicio.setDetalleServicio(detalleServicio);
					auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
					auditoriaServicio.setFecha(new Date());
					auditoriaServicio.setHost(host);
					auditoriaServicio.setParametro(parametro);
					if(rutOrigen > 0)
						auditoriaServicio.setUsuario(rutOrigen);
					else
						auditoriaServicio.setUsuario(rutUsuario);
				
				moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
				
				//System.out.println("origen: "+accionweb.getSesion().getAttribute("rutOrigen"));
				log.debug("Si guardada auditoria cargaIngresoCarrera");
				
			} catch (Exception e) {
				log.debug("No guardada auditoria cargaIngresoCarrera");
				accionweb.agregarObjeto("mensaje", "No guardado");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
			}	
			accionweb.agregarObjeto("menuSerieTiempo", 1); // es para no mostrar el menu en ingreso
	
	}
	public void cargaIngresoMencion(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int sucur = Util.validaParametro(accionweb.getParameter("sucur"), 0);
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int codUni = Util.validaParametro(accionweb.getParameter("codUni"), 0);
		String nomSede = Util.validaParametro(accionweb.getParameter("nomSede"),"");
		String nomUni = Util.validaParametro(accionweb.getParameter("nomUni"),"");
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		String nomDoc = Util.validaParametro(accionweb.getParameter("nomDoc"),"");
		List<Presw25DTO> listado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listado");
		if(listado != null && listado.size() == 0)
		     accionweb.getSesion().removeAttribute("listado");
	
		
		if(rutOrigen > 0){
			rutUsuario = rutOrigen;
			accionweb.getSesion().setAttribute("rutOrigen", rutOrigen);
			accionweb.getSesion().setAttribute("rutUsuario", rutUsuario);
		}
		accionweb.agregarObjeto("cargar", "cargarMencion(" + numDoc + "," + nomDoc + ");");
		accionweb.agregarObjeto("cargaIngreso", 4);
		if(rutUsuario > 0 && anno > 0){
			
			try {
				List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
				listaPresw25 = moduloPresupuesto.getListaPresw25("ASC", rutUsuario, anno, dv, sucur,codUni,numDoc);	

				accionweb.agregarObjeto("listaIngresoMencion", listaPresw25);
				moduloPresupuesto.agregaListado(accionweb.getReq(), listaPresw25);
				
			
				
				String host = accionweb.getReq().getLocalAddr();
				accionweb.agregarObjeto("nomSede", nomSede);
				accionweb.agregarObjeto("sucur", sucur);
				accionweb.agregarObjeto("anno", anno);
				accionweb.agregarObjeto("nomUni", nomUni);
				accionweb.agregarObjeto("codUni", codUni);
				accionweb.agregarObjeto("numDoc", numDoc);
				accionweb.agregarObjeto("nomDoc", nomDoc);
				
				DetalleServicio detalleServicio = new DetalleServicio();
				detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(30));
				TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();
				tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(1);
				String parametro = "ASC";
			
				
				AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
				auditoriaServicio.setDetalleServicio(detalleServicio);
				auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
				auditoriaServicio.setFecha(new Date());
				auditoriaServicio.setHost(host);
				auditoriaServicio.setParametro(parametro);
				if(rutOrigen > 0)
					auditoriaServicio.setUsuario(rutOrigen);
				else
					auditoriaServicio.setUsuario(rutUsuario);

				moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
				
				//System.out.println("origen: "+accionweb.getSesion().getAttribute("rutOrigen"));
				log.debug("Si guardada auditoria cargaIngresoMencion");
				
			} catch (Exception e) {
				log.debug("No guardada auditoria cargaIngresoMencion");
				accionweb.agregarObjeto("mensaje", "No guardado");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
			}	
			accionweb.agregarObjeto("menuSerieTiempo", 1); // es para no mostrar el menu en ingreso

	}
	public void cargaSeries(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
	    int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		
		if(rutOrigen > 0){
			this.limpiaSimulador(accionweb);
		}	
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		accionweb.agregarObjeto("control", control);
	
		
	}
	public void cargaAportes(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		String tipo = Util.validaParametro(accionweb .getParameter("tipo"),"");
		accionweb.agregarObjeto("control", control);
		
		/*esto es para el menu de control presupuestario*/
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		accionweb.getSesion().setAttribute("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		/* fin */
		
		List<Presw25DTO> listado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listado");
		if(listado != null && listado.size() == 0)
		     accionweb.getSesion().removeAttribute("listado");
		
		if(rutOrigen > 0){
			rutUsuario = rutOrigen;
			accionweb.getSesion().setAttribute("rutOrigen", rutOrigen);
			accionweb.getSesion().setAttribute("rutUsuario", rutUsuario);
		}
		if(tipo.trim().equals("AFI")){
			List<Cocof17DTO> listCocofBean = new ArrayList<Cocof17DTO>();
			listCocofBean = moduloPresupuesto.getListaSedeAlterada(accionweb);
		    accionweb.agregarObjeto("listCocofBean", listCocofBean);
		}
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(dv);
	
		Collection<Presw25DTO> listaPresw25 = (Collection<Presw25DTO>) preswbean.consulta_presw25();
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = null;
		for (Presw25DTO ss : listaPresw25){
			presw25DTO = new Presw25DTO();
			presw25DTO.setAnopre(ss.getAnopre());
			presw25DTO.setPres01(ss.getPres01());
			presw25DTO.setPres02(ss.getPres02());
			presw25DTO.setPres03(ss.getPres03());
			presw25DTO.setPres04(ss.getPres04());
			presw25DTO.setPres05(ss.getPres05());
			presw25DTO.setPres06(ss.getPres06());
			presw25DTO.setPres07(ss.getPres07());
			presw25DTO.setPres08(ss.getPres08());
			presw25DTO.setPres09(ss.getPres09());
			presw25DTO.setPres10(ss.getPres10());
			presw25DTO.setPres11(ss.getPres11());
			presw25DTO.setPres12(ss.getPres12());
			presw25DTO.setIndexi(ss.getIndexi());
			presw25DTO.setComen1(ss.getComen1());
			lista.add(presw25DTO);
		}
		
		
		
		moduloPresupuesto.agregaListado(accionweb.getReq(), lista);
		String host = accionweb.getReq().getLocalAddr();
		//moduloPresupuesto.agregaAporteFiscalDirecto(accionweb.getReq(), listaPresw25);
		//accionweb.agregarObjeto("listaAporteFiscalDirecto", accionweb.getSesion().getAttribute("listaAporteFiscalDirecto"));
		accionweb.agregarObjeto("listaAporte", listaPresw25);
		accionweb.agregarObjeto("tipo", tipo);
		
		DetalleServicio detalleServicio = new DetalleServicio();
		detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(30));
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();
		tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(1);
		String parametro = tipo;
	
		
		AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
		auditoriaServicio.setDetalleServicio(detalleServicio);
		auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
		auditoriaServicio.setFecha(new Date());
		auditoriaServicio.setHost(host);
		auditoriaServicio.setParametro(parametro);
		if(rutOrigen > 0)
			auditoriaServicio.setUsuario(rutOrigen);
		else
			auditoriaServicio.setUsuario(rutUsuario);
		try {
			moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
			
			//System.out.println("origen: "+accionweb.getSesion().getAttribute("rutOrigen"));
			log.debug("Si guardada auditoria "+tipo);
			
		} catch (Exception e) {
			log.debug("No guardada auditoria " + tipo);
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		

	}
	
	public void cargaPresupInstitucional(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		String tipo = Util.validaParametro(accionweb .getParameter("tipo"),"");
		accionweb.agregarObjeto("control", control);
		List<Presw25DTO> listado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listado");
		Vector lista = new Vector();
		if(listado != null && listado.size() == 0)
		     accionweb.getSesion().removeAttribute("listado");
		/*esto es para el menu de control presupuestario*/
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		accionweb.getSesion().setAttribute("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		/* fin */
		
		if(rutOrigen > 0){
			rutUsuario = rutOrigen;
			accionweb.getSesion().setAttribute("rutOrigen", rutOrigen);
			accionweb.getSesion().setAttribute("rutUsuario", rutUsuario);
		}
		String titulo = "Presupuesto Institucional de Fondos Centrales";
		if(tipo.trim().equals("PIG"))
			titulo = "Presupuesto Institucional Global";
		accionweb.agregarObjeto("titulo", titulo);
		accionweb.agregarObjeto("tipo", tipo);
	    if(anno > 0 && !tipo.trim().equals("")){
		PreswBean preswbean = new PreswBean(tipo,0,0,0,0,rutUsuario,0,"",0,"","",0,0,0,0,"","");
		preswbean.setAnopar(anno);
		preswbean.setDigide(dv);
		Collection<Presw25DTO> listaPresw25 = (Collection<Presw25DTO>) preswbean.consulta_presw25();
		Presw25DTO presw25DTO = new Presw25DTO();
		for (Presw25DTO ss : listaPresw25){		
			presw25DTO = new Presw25DTO();
			presw25DTO.setAnopre(ss.getAnopre());
			presw25DTO.setPres01(ss.getPres01());
			presw25DTO.setPres02(ss.getPres02());
			presw25DTO.setPres03(ss.getPres03());
			presw25DTO.setPres04(ss.getPres04());
			presw25DTO.setPres05(ss.getPres05());
			presw25DTO.setPres06(ss.getPres06());
			presw25DTO.setPres07(ss.getPres07());
			presw25DTO.setPres08(ss.getPres08());
			presw25DTO.setPres09(ss.getPres09());
			presw25DTO.setPres10(ss.getPres10());
			presw25DTO.setPres11(ss.getPres11());
			presw25DTO.setPres12(ss.getPres12());
			presw25DTO.setIndexi(ss.getIndexi());
			presw25DTO.setComen1(ss.getComen1());
			lista.add(presw25DTO);
		}
		moduloPresupuesto.agregaListado(accionweb.getReq(), lista);
		
		String host = accionweb.getReq().getLocalAddr();
		accionweb.agregarObjeto("listaPresupInstitucional", listaPresw25);
		accionweb.agregarObjeto("annoFin", anno);
		accionweb.agregarObjeto("annoIni", (anno - 5));
		accionweb.agregarObjeto("anno", anno);
		accionweb.agregarObjeto("tipo", tipo);
		
		DetalleServicio detalleServicio = new DetalleServicio();
		detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(30));
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();
		tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(1);
		String parametro = tipo;
	
		
		AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
		auditoriaServicio.setDetalleServicio(detalleServicio);
		auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
		auditoriaServicio.setFecha(new Date());
		auditoriaServicio.setHost(host);
		auditoriaServicio.setParametro(parametro);
		if(rutOrigen > 0)
			auditoriaServicio.setUsuario(rutOrigen);
		else
			auditoriaServicio.setUsuario(rutUsuario);
		try {
			moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
			
			//System.out.println("origen: "+accionweb.getSesion().getAttribute("rutOrigen"));
			log.debug("Si guardada auditoria "+tipo);
			
		} catch (Exception e) {
			log.debug("No guardada auditoria " + tipo);
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
	    }

	}
	public void cargaIngresoPregrado(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");		
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		int sede = Util.validaParametro(accionweb.getParameter("sede"), 99);
		int expresado = Util.validaParametro(accionweb.getParameter("expresado"), 1);
		
		GregorianCalendar hoy = new GregorianCalendar(); 
		int mesActual = hoy.get(Calendar.MONTH) + 1;
		int anoActual = hoy.get(Calendar.YEAR);
		if(anno == 0) anno = anoActual;
		accionweb.agregarObjeto("control", control);
		List<Presw25DTO> listado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listado");
		if(listado != null && listado.size() == 0)
		     accionweb.getSesion().removeAttribute("listado");
		List<Presw25DTO> presupIngresosPregrado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupIngresosPregrado");
		if(presupIngresosPregrado != null){
			accionweb.getSesion().removeAttribute("presupIngresosPregrado");
			presupIngresosPregrado = null;
		}
		Vector lista = new Vector();
	
		/*esto es para el menu de control presupuestario*/
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		accionweb.getSesion().setAttribute("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
   		accionweb.agregarObjeto("annoFin", anno);
		accionweb.agregarObjeto("annoIni", (anno - 6));
		accionweb.agregarObjeto("tipoGraf", 1);
		/* fin */
		//public List<Presw25DTO> getListaPresw25(String tipo, int rutUsuario, int anno, String dv, int sucur, int codUni, int numDoc){

		String titulo = "Ingresos de Pregrado";
		accionweb.agregarObjeto("titulo", titulo);
		 if(anno > 0 && sede > 0){
				List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
				listaPresw25 = moduloPresupuesto.getListaSede(rutUsuario, dv, anno);
				accionweb.agregarObjeto("listaSede", listaPresw25);	
				moduloPresupuesto.getDetalleIngresosPregrado(accionweb.getReq(), rutUsuario, dv, anno, sede, 999999, 99999, 99999);
				presupIngresosPregrado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupIngresosPregrado");
		   		accionweb.agregarObjeto("presupIngresosPregrado", presupIngresosPregrado);		
			   
		
		
		String host = accionweb.getReq().getLocalAddr();
		accionweb.agregarObjeto("annoselec", anno);
		accionweb.agregarObjeto("sedeselec", sede);
		accionweb.agregarObjeto("tipo", "STP");
		accionweb.agregarObjeto("expresado", expresado);
		
		DetalleServicio detalleServicio = new DetalleServicio();
		detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(30));
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();
		tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(1);
		String parametro = titulo;
	
		
		AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
		auditoriaServicio.setDetalleServicio(detalleServicio);
		auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
		auditoriaServicio.setFecha(new Date());
		auditoriaServicio.setHost(host);
		auditoriaServicio.setParametro(parametro);
		if(rutOrigen > 0)
			auditoriaServicio.setUsuario(rutOrigen);
		else
			auditoriaServicio.setUsuario(rutUsuario);
		try {
			moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
			
			//System.out.println("origen: "+accionweb.getSesion().getAttribute("rutOrigen"));
			//log.debug("Si guardada auditoria "+tipo);
			
		} catch (Exception e) {
			//og.debug("No guardada auditoria " + tipo);
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
	    }

	}
	public void cargaIngresoPregradoDepto(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");		
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		int sede = Util.validaParametro(accionweb.getParameter("sede"), 99);
		
		if(anno > 0 && sede > 0){
			   	List<Presw25DTO> listadepto = new ArrayList<Presw25DTO>();
			    listadepto = moduloPresupuesto.getListadepartamento(rutUsuario, dv, anno, sede);
				accionweb.agregarObjeto("listadepto", listadepto);
		}
	}
	public void cargaIngresoPregradoCarr(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");		
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		int sede = Util.validaParametro(accionweb.getParameter("sede"), 99);
		int departamento = Util.validaParametro(accionweb.getParameter("departamento"), 999999);
		
		if(anno > 0 && sede > 0 && departamento > 0){
			List<Presw25DTO> listacarrera = new ArrayList<Presw25DTO>();
			listacarrera = moduloPresupuesto.getListaCarrera(rutUsuario, dv, anno, sede, departamento);
			accionweb.agregarObjeto("listacarrera", listacarrera);

		}
	}
	public void cargaIngresoPregradoMencion(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");		
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		int sede = Util.validaParametro(accionweb.getParameter("sede"), 99);
		int departamento = Util.validaParametro(accionweb.getParameter("departamento"), 999999);
		int carrera = Util.validaParametro(accionweb.getParameter("carrera"), 99999);
		
		if(anno > 0 && sede > 0 && departamento > 0 && carrera > 0){
			List<Presw25DTO> listaMencion = new ArrayList<Presw25DTO>();
			listaMencion = moduloPresupuesto.getListaMencion(rutUsuario, dv, anno, sede, departamento, carrera);
			accionweb.agregarObjeto("listamencion", listaMencion);

		}
	}
	public void cargaIngresoPregradoDetalle(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");		
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		int sede = Util.validaParametro(accionweb.getParameter("sede"), 99);
		int departamento = Util.validaParametro(accionweb.getParameter("departamento"), 999999);
		int carrera = Util.validaParametro(accionweb.getParameter("carrera"), 99999);
		int mencion = Util.validaParametro(accionweb.getParameter("mencion"), 99999);
		int expresado = Util.validaParametro(accionweb.getParameter("expresado"), 1);
		String nomSede = Util.validaParametro(accionweb.getParameter("nomSede"), "Todas");
		String nomDepartamento = Util.validaParametro(accionweb.getParameter("nomDepartamento"), "Todos");
		String nomCarrera = Util.validaParametro(accionweb.getParameter("nomCarrera"), "Todas");
		String nomMencion = Util.validaParametro(accionweb.getParameter("nomMencion"), "Todas");
		GregorianCalendar hoy = new GregorianCalendar(); 
		int mesActual = hoy.get(Calendar.MONTH) + 1;
		int anoActual = hoy.get(Calendar.YEAR);
		List<Presw25DTO> listado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listado");
		if(listado != null && listado.size() == 0)
		     accionweb.getSesion().removeAttribute("listado");
		
		if(anno > 0 && sede > 0 && departamento > 0 && carrera > 0){
			List<Presw25DTO> presupIngresosPregrado = new ArrayList<Presw25DTO>();
			moduloPresupuesto.getDetalleIngresosPregrado(accionweb.getReq(), rutUsuario, dv, anno, sede, departamento, carrera, mencion);
			presupIngresosPregrado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupIngresosPregrado");
			List<Presw25DTO> listaTotalIngreso = new ArrayList<Presw25DTO>();
			listaTotalIngreso = moduloPresupuesto.retornaTotalIngresos(presupIngresosPregrado, expresado);
						
			
	   		accionweb.agregarObjeto("presupIngresosPregrado", presupIngresosPregrado);		
	   		accionweb.agregarObjeto("annoFin", anno);
			accionweb.agregarObjeto("annoIni", (anno - 6));
			accionweb.agregarObjeto("annoselec", anno);
			accionweb.agregarObjeto("anoActual", anoActual);
			accionweb.agregarObjeto("tipo", "STP");
			accionweb.agregarObjeto("tipoGraf", 1);
			accionweb.agregarObjeto("nomSede", nomSede);
			accionweb.agregarObjeto("nomDepartamento", nomDepartamento);
			accionweb.agregarObjeto("nomCarrera", nomCarrera);
			accionweb.agregarObjeto("nomMencion", nomMencion);
			accionweb.agregarObjeto("expresado", expresado);
			accionweb.agregarObjeto("listaTotalIngreso", listaTotalIngreso);

		}
	}
	public void cargaIngresoPregradoCompara(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");		
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		int sede = Util.validaParametro(accionweb.getParameter("sede"), 99);
		int expresado = Util.validaParametro(accionweb.getParameter("expresado"), 1);
		
		String nomSede = Util.validaParametro(accionweb.getParameter("nomSede"), "Todas");
		String nomDepartamento = Util.validaParametro(accionweb.getParameter("nomDepartamento"), "Todos");
		String nomCarrera = Util.validaParametro(accionweb.getParameter("nomCarrera"), "Todas");
		String nomMencion = Util.validaParametro(accionweb.getParameter("nomMencion"), "Todas");
		GregorianCalendar hoy = new GregorianCalendar(); 
		int mesActual = hoy.get(Calendar.MONTH) + 1;
		int anoActual = hoy.get(Calendar.YEAR);
		if(anno == 0) anno = anoActual;
		accionweb.agregarObjeto("control", control);
		List<Presw25DTO> listado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listado");
		if(listado != null && listado.size() == 0)
		     accionweb.getSesion().removeAttribute("listado");
		List<Presw25DTO> presupIngresosPregradoAnt = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupIngresosPregrado");
		List<Presw25DTO> comparaPresupIngresosPregrado = null;
		if(presupIngresosPregradoAnt != null){
			//accionweb.getSesion().removeAttribute("presupIngresosPregrado");
			accionweb.getSesion().setAttribute("presupIngresosPregradoAnt", presupIngresosPregradoAnt);
			List<Presw25DTO> listaTotalIngreso = new ArrayList<Presw25DTO>();
			listaTotalIngreso = moduloPresupuesto.retornaTotalIngresos(presupIngresosPregradoAnt, expresado);
		
			accionweb.agregarObjeto("listaTotalIngreso", listaTotalIngreso);
		}
		Vector lista = new Vector();
		
   		accionweb.agregarObjeto("annoFin", anno);
		accionweb.agregarObjeto("annoIni", (anno - 6));
		accionweb.agregarObjeto("tipoGraf", 1);
		/* fin */
		//public List<Presw25DTO> getListaPresw25(String tipo, int rutUsuario, int anno, String dv, int sucur, int codUni, int numDoc){

		String titulo = "Ingresos de Pregrado";
		accionweb.agregarObjeto("titulo", titulo);
		 if(anno > 0 && sede > 0){
				List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
				listaPresw25 = moduloPresupuesto.getListaSede(rutUsuario, dv, anno);
				accionweb.agregarObjeto("listaSede", listaPresw25);	
				moduloPresupuesto.getDetalleIngresosPregrado(accionweb.getReq(), rutUsuario, dv, anno, sede, 999999, 99999, 99999);
				comparaPresupIngresosPregrado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupIngresosPregrado");
				accionweb.getSesion().setAttribute("comparaPresupIngresosPregrado", comparaPresupIngresosPregrado);
				accionweb.getSesion().setAttribute("presupIngresosPregrado", presupIngresosPregradoAnt);
		   		accionweb.agregarObjeto("comparaPresupIngresosPregrado", comparaPresupIngresosPregrado);		
		   		accionweb.agregarObjeto("presupIngresosPregradoAnt", presupIngresosPregradoAnt);
				accionweb.agregarObjeto("nomSede", nomSede);
				accionweb.agregarObjeto("nomDepartamento", nomDepartamento);
				accionweb.agregarObjeto("nomCarrera", nomCarrera);
				accionweb.agregarObjeto("nomMencion", nomMencion);
				accionweb.agregarObjeto("expresado", expresado);
		
		
		String host = accionweb.getReq().getLocalAddr();
		accionweb.agregarObjeto("annoselec", anno);
		accionweb.agregarObjeto("sedeselec", sede);
		accionweb.agregarObjeto("tipo", "STP");
		
		DetalleServicio detalleServicio = new DetalleServicio();
		detalleServicio = moduloPresupuesto.getDetalleServicio(new Long(30));
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();
		tipoAccionServicio = moduloPresupuesto.getTipoAccionServicio(1);
		String parametro = titulo;
	
		
		AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
		auditoriaServicio.setDetalleServicio(detalleServicio);
		auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
		auditoriaServicio.setFecha(new Date());
		auditoriaServicio.setHost(host);
		auditoriaServicio.setParametro(parametro);
		if(rutOrigen > 0)
			auditoriaServicio.setUsuario(rutOrigen);
		else
			auditoriaServicio.setUsuario(rutUsuario);
		try {
			moduloPresupuesto.saveAuditoriaServicio(auditoriaServicio);
			
			//System.out.println("origen: "+accionweb.getSesion().getAttribute("rutOrigen"));
			//log.debug("Si guardada auditoria "+tipo);
			
		} catch (Exception e) {
			//og.debug("No guardada auditoria " + tipo);
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
	    }

	}
	 synchronized public void cargaIngresoPregradoComparaDetalle(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");		
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		int sede = Util.validaParametro(accionweb.getParameter("sede"), 99);
		int departamento = Util.validaParametro(accionweb.getParameter("departamento"), 999999);
		int carrera = Util.validaParametro(accionweb.getParameter("carrera"), 99999);
		int mencion = Util.validaParametro(accionweb.getParameter("mencion"), 99999);
		int expresado = Util.validaParametro(accionweb.getParameter("expresado"), 1);
		
		String nomSede = Util.validaParametro(accionweb.getParameter("nomSede"), "Todas");
		String nomDepartamento = Util.validaParametro(accionweb.getParameter("nomDepartamento"), "Todos");
		String nomCarrera = Util.validaParametro(accionweb.getParameter("nomCarrera"), "Todas");
		String nomMencion = Util.validaParametro(accionweb.getParameter("nomMencion"), "Todas");
		GregorianCalendar hoy = new GregorianCalendar(); 
		int mesActual = hoy.get(Calendar.MONTH) + 1;
		int anoActual = hoy.get(Calendar.YEAR);
		 MathTool mathtool = new MathTool();
		
		if(anno > 0 && sede > 0 && departamento > 0 && carrera > 0){
			List<Presw25DTO> presupIngresosPregradoAnt = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupIngresosPregradoAnt");
			List<Presw25DTO> comparaPresupIngresosPregrado = new ArrayList<Presw25DTO>();
			moduloPresupuesto.getDetalleIngresosPregrado(accionweb.getReq(), rutUsuario, dv, anno, sede, departamento, carrera, mencion);
			comparaPresupIngresosPregrado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupIngresosPregrado");
			accionweb.getSesion().setAttribute("comparaPresupIngresosPregrado", comparaPresupIngresosPregrado);
			accionweb.getSesion().setAttribute("presupIngresosPregrado", presupIngresosPregradoAnt);
			accionweb.agregarObjeto("comparaPresupIngresosPregrado", comparaPresupIngresosPregrado);		
	   		accionweb.agregarObjeto("presupIngresosPregradoAnt", presupIngresosPregradoAnt);
			accionweb.agregarObjeto("annoFin", anno);
			accionweb.agregarObjeto("annoIni", (anno - 6));
			accionweb.agregarObjeto("annoselec", anno);
			accionweb.agregarObjeto("anoActual", anoActual);
			accionweb.agregarObjeto("tipo", "STP");
			accionweb.agregarObjeto("tipoGraf", 1);
			accionweb.agregarObjeto("nomSede", nomSede);
			accionweb.agregarObjeto("nomDepartamento", nomDepartamento);
			accionweb.agregarObjeto("nomCarrera", nomCarrera);
			accionweb.agregarObjeto("nomMencion", nomMencion);
			accionweb.agregarObjeto("expresado", expresado);
			
			List<Presw25DTO> listaTotalIngreso = new ArrayList<Presw25DTO>();
			Presw25DTO presw25DTO = new Presw25DTO();
			listaTotalIngreso = moduloPresupuesto.retornaTotalIngresos(comparaPresupIngresosPregrado, expresado);
			accionweb.agregarObjeto("comparaListaTotalIngreso", listaTotalIngreso);
			
			
			/*compara los resultados*/
			List<Presw25DTO> resultadoPresupIngresosPregrado = new ArrayList<Presw25DTO>();
			 presw25DTO = new Presw25DTO();
			Presw25DTO presw25DTOCompara = new Presw25DTO();
			int i=0;
			long pres1 = 0;
			long pres2 = 0;
			long pres3 = 0;
			long pres4 = 0;
			long pres5 = 0;
			long pres6 = 0;
			long pres7 = 0;
			for (Presw25DTO ss : presupIngresosPregradoAnt){
				
				if (i < presupIngresosPregradoAnt.size()-1) {
				pres1 = 0;
				pres2 = 0;
				pres3 = 0;
				pres4 = 0;
				pres5 = 0;
				pres6 = 0;
				pres7 = 0;
				presw25DTOCompara = comparaPresupIngresosPregrado.get(i);
				Presw25DTO presw25DTOResul = new Presw25DTO();	
				
				/*
				if(presw25DTOCompara.getPres01() > 0)
					pres1 = ((ss.getPres01()/1) / (presw25DTOCompara.getPres01()/1)) * 10000;
				if(presw25DTOCompara.getPres02() > 0)					
					pres2 = (ss.getPres02() / presw25DTOCompara.getPres02()) * 10000;
				if(presw25DTOCompara.getPres03() > 0)
					pres3 = (ss.getPres03() / presw25DTOCompara.getPres03()) * 10000;
				if(presw25DTOCompara.getPres04() > 0)					
					pres4 = (ss.getPres04() / presw25DTOCompara.getPres04()) * 10000;
				if(presw25DTOCompara.getPres05() > 0)
					pres5 = (ss.getPres05() / presw25DTOCompara.getPres05()) * 10000;
				if(presw25DTOCompara.getPres06() > 0)
					pres6 = (ss.getPres06() / presw25DTOCompara.getPres06()) * 10000;
				if(presw25DTOCompara.getPres07() > 0)
					pres7 = ((ss.getPres07() / presw25DTOCompara.getPres07()) * 10000);
				*/
				/*mathtool.div(mathtool.mul(ss.getPres01(), 100), presw25DTOCompara.getPres01()));*/
				if(presw25DTOCompara.getPres01() > 0)
					pres1 = ((ss.getPres01()*100) / presw25DTOCompara.getPres01()) * 100;
				if(presw25DTOCompara.getPres02() > 0)					
					pres2 = ((ss.getPres02()*100) / presw25DTOCompara.getPres02()) * 100;
				if(presw25DTOCompara.getPres03() > 0)
					pres3 = ((ss.getPres03()*100) / presw25DTOCompara.getPres03()) * 100;
				if(presw25DTOCompara.getPres04() > 0)					
					pres4 = ((ss.getPres04()*100) / presw25DTOCompara.getPres04()) * 100;
				if(presw25DTOCompara.getPres05() > 0)
					pres5 = ((ss.getPres05()*100) / presw25DTOCompara.getPres05()) * 100;
				if(presw25DTOCompara.getPres06() > 0)
					pres6 = ((ss.getPres06()*100) / presw25DTOCompara.getPres06()) * 100;
				if(presw25DTOCompara.getPres07() > 0)
					pres7 = ((ss.getPres07()*100) / presw25DTOCompara.getPres07()) * 100;
				
				presw25DTOResul.setDesite(ss.getDesite());
				presw25DTOResul.setPres01(pres1);
				presw25DTOResul.setPres02(pres2);
				presw25DTOResul.setPres03(pres3);
				presw25DTOResul.setPres04(pres4);
				presw25DTOResul.setPres05(pres5);
				presw25DTOResul.setPres06(pres6);
				presw25DTOResul.setPres07(pres7);
				resultadoPresupIngresosPregrado.add(presw25DTOResul);
				}
				i++;				
			}	
			accionweb.agregarObjeto("resultadoPresupIngresosPregrado", resultadoPresupIngresosPregrado);
			accionweb.getSesion().setAttribute("resultadoPresupIngresosPregrado", resultadoPresupIngresosPregrado);
		}
	}
	public void exportarIngreso(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		listaPresw25 = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listado");
		Vector datos = new Vector();
		Vector lista = new Vector();
		 MathTool mathtool = new MathTool();
		 long total1 = 0;
		 long total2 = 0;
		 long total3 = 0;
		 long total4 = 0;
		 long total5 = 0;
		 long total6 = 0;
		 long total7 = 0;
		 long total8 = 0;
		 long total9 = 0;
		 long total11 = 0;
		 long total12 = 0;
		 Double totalporc = new Double(0);
		 Double totalporc2 = new Double(0);
		 long totalSaldoA�o = 0;
		 long totalSaldoActual = 0;
		 long totalArancel = 0;
		
		for (Presw25DTO ss : listaPresw25){
			Double porc = new Double(0);
			Double porc2 = new Double(0); 
			totalSaldoA�o = totalSaldoA�o + ss.getPres09();
			totalSaldoActual = totalSaldoActual + ss.getPres12();
			totalArancel = totalArancel + ss.getPres01();
			porc = mathtool.roundTo(1,(mathtool.div(ss.getPres10(), 100)));
			porc2 = mathtool.roundTo(1,(mathtool.div(ss.getValuni(), 100)));
			total1 = total1 + ss.getPres01();
			total2 = total2 + ss.getPres02();
			total3 = total3 + ss.getPres03();
			total4 = total4 + ss.getPres04();
			total5 = total5 + ss.getPres05();
			total6 = total6 + ss.getPres06();
			total7 = total7 + ss.getPres07();
			total8 = total8 + ss.getPres08();
			total9 = total9 + ss.getPres09();
			total11 = total11 + ss.getPres11();
			total12 = total12 + ss.getPres12();
		    datos = new Vector();
		    if(ss.getMespre() > 0)
		    	datos.addElement(ss.getMespre());
		    else
		    	datos.addElement(ss.getCoduni());
			datos.addElement(ss.getComen1());
			datos.addElement(ss.getPres01());
			datos.addElement(ss.getPres02());
			datos.addElement(ss.getPres03());
			datos.addElement(ss.getPres04());
			datos.addElement(ss.getPres05());
			datos.addElement(ss.getPres06());
			datos.addElement(ss.getPres07());
			datos.addElement(ss.getPres08());
			datos.addElement(ss.getPres09());
			datos.addElement(porc);
			datos.addElement(ss.getPres11());
			datos.addElement(ss.getPres12());
			datos.addElement(porc2);
			datos.addElement(ss.getCoduni());
		    lista.add(datos);
		}	
		totalporc = mathtool.roundTo(1,(mathtool.mul(mathtool.div(totalSaldoA�o, totalArancel),100)));
		totalporc2 = mathtool.roundTo(1,(mathtool.mul(mathtool.div(totalSaldoActual,totalArancel),100)));
	    datos = new Vector();
		datos.addElement("Total");
		datos.addElement("");
		datos.addElement(total1);
		datos.addElement(total2);
		datos.addElement(total3);
		datos.addElement(total4);
		datos.addElement(total5);
		datos.addElement(total6);
		datos.addElement(total7);
		datos.addElement(total8);
		datos.addElement(total9);
		datos.addElement(totalporc);
		datos.addElement(total11);
		datos.addElement(total12);
		datos.addElement(totalporc2);	
	    lista.add(datos);
		moduloPresupuesto.agregaPresupIngreso(accionweb.getReq(), lista);
	}
	public void exportarAporteFiscal(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String tipo = Util.validaParametro(accionweb.getParameter("tipo"), "");
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		listaPresw25 = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listado");
		Vector datos = new Vector();
		Vector lista = new Vector();
		Vector vec_sede = new Vector();
		MathTool mathtool = new MathTool();
    	int i=0;
    	
    	int expresado = Util.validaParametro(accionweb.getParameter("expresado"), 0);
		
    	List<Presw25DTO> listaTotalIngreso = new ArrayList<Presw25DTO>();
		
    	listaTotalIngreso = moduloPresupuesto.retornaTotalIngresos(listaPresw25, expresado);
    	
   
    	
		for (Presw25DTO ss : listaPresw25){	
			i++;
		    datos = new Vector();
			if(tipo.trim().equals("STP")){
				
				if(i < listaPresw25.size()){
					datos.addElement(ss.getDesite().trim());
				/*solicitaron eliminar la morosidad   09/09/2011
				
				if(i==listaPresw25.size()){
				datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres07(),100)));
				datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres06(),100)));
				datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres05(),100)));
				datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres04(),100)));
				datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres03(),100)));
				datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres02(),100)));
				datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres01(),100)));
				} else {*/

						
				   	for(Presw25DTO listaTotal : listaTotalIngreso)	 {
				   		if(expresado == 1){
				   	   		datos.addElement(ss.getPres07());
					   		datos.addElement(ss.getPres06());
					   		datos.addElement(ss.getPres05());
					   		datos.addElement(ss.getPres04());
					   		datos.addElement(ss.getPres03());
					   		datos.addElement(ss.getPres02());
					   		datos.addElement(ss.getPres01());
				   			
				   		} else {
				   			// en caso de ser saldo del a�o la f�rmula a aplicar es SA/Tot ingresos + SA
				   			if (ss.getDesite().substring(0,5).equals("SALDO")){
				   				
				   				if((listaTotal.getPres07() + ss.getPres07()) > 0)
				   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres07(),mathtool.add(listaTotal.getPres07(),ss.getPres07())),100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres06() + ss.getPres06()) > 0)
				   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres06(),mathtool.add(listaTotal.getPres06(),ss.getPres06())),100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres05() + ss.getPres05()) > 0)
				   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres05(),mathtool.add(listaTotal.getPres05(),ss.getPres05())),100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres04() + ss.getPres04()) > 0)
				   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres04(),mathtool.add(listaTotal.getPres04(),ss.getPres04())),100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres03() + ss.getPres03()) > 0)
				   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres03(),mathtool.add(listaTotal.getPres03(),ss.getPres03())),100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres02() + ss.getPres02()) > 0)
				   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres02(),mathtool.add(listaTotal.getPres02(),ss.getPres02())),100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres01() + ss.getPres01()) > 0)
				   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres01(),mathtool.add(listaTotal.getPres01(),ss.getPres01())),100)));
				   				else
				   					datos.addElement("0");
				   				
				   				
				   			} else {	
			   				       if(listaTotal.getPres07() > 0 && ss.getPres07() > 0)
							   		datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres07(),listaTotal.getPres07()), 100)));
			   					else
				   					datos.addElement("0");
			   				    if(listaTotal.getPres06() > 0 && ss.getPres06() > 0)
							   		datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres06(),listaTotal.getPres06()), 100)));
			   				 else
				   					datos.addElement("0");
			   				    if(listaTotal.getPres05() > 0 && ss.getPres05() > 0)
							   		datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres05(),listaTotal.getPres05()), 100)));
			   				 else
				   					datos.addElement("0");
			   				    if(listaTotal.getPres04() > 0 && ss.getPres04() > 0)
							   		datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres04(),listaTotal.getPres04()), 100)));
			   				 else
				   					datos.addElement("0");
			   				    if(listaTotal.getPres03() > 0 && ss.getPres03() > 0)
							   		datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres03(),listaTotal.getPres03()), 100)));
			   				 else
				   					datos.addElement("0");
			   				    if(listaTotal.getPres02() > 0 && ss.getPres02() > 0)
							   		datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres02(),listaTotal.getPres02()), 100)));
			   				 else
				   					datos.addElement("0");
			   				    if(listaTotal.getPres01() > 0 && ss.getPres01() > 0)
							   		datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres01(),listaTotal.getPres01()), 100)));
			   				 else
				   					datos.addElement("0");
			   				
								   	   		
				   			}
				   			}
				   	      }
					   	
				}
			}else {
			datos.addElement(ss.getAnopre());
			if(tipo.trim().equals("AFI")){
				datos.addElement(ss.getPres11());
				datos.addElement(mathtool.roundTo(1,(mathtool.div(ss.getPres12(), 100)))+"");				
			}
			datos.addElement(ss.getPres01());			
			datos.addElement(ss.getPres02());
			if(tipo.trim().equals("FSC"))
				datos.addElement(mathtool.roundTo(1,(mathtool.div(ss.getPres03(), 100)))+"");
			else			
				datos.addElement(ss.getPres03());
			datos.addElement(ss.getPres04());
			if(tipo.trim().equals("AFD"))
					datos.addElement(mathtool.roundTo(1,(mathtool.div(ss.getPres05(), 100)))+"");
				else			
					datos.addElement(ss.getPres05());
			
			datos.addElement(ss.getPres06());
			datos.addElement(ss.getPres07());
			if(tipo.trim().equals("PIF") || tipo.trim().equals("PIG")) {
				datos.addElement(ss.getIndexi());
				datos.addElement(ss.getComen1());
			} 
			}
			if(datos != null && datos.size() > 0)	
				lista.add(datos);
		}

	   
		moduloPresupuesto.agregaPresupSerie(accionweb.getReq(), lista);
		  if(tipo.trim().equals("AFI")) {
	        	List<Cocof17DTO> listCocofBean = new ArrayList<Cocof17DTO>();
				listCocofBean = moduloPresupuesto.getListaSedeAlterada(accionweb);
				
				for(Cocof17DTO ss: listCocofBean){
				    datos = new Vector();
				    datos.addElement(ss.getCodsuc());
				    datos.addElement(ss.getNomsuc());
				    vec_sede.addElement(datos);
				}
				moduloPresupuesto.agregaListaSede(accionweb.getReq(), vec_sede);
				
		  }
			   
	}
	 synchronized public void exportarComparaPresupIngreso(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String tipo = Util.validaParametro(accionweb.getParameter("tipo"), "");
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		List<Presw25DTO> presupIngresosPregradoAnt = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupIngresosPregradoAnt");
		List<Presw25DTO> comparaPresupIngresosPregrado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("comparaPresupIngresosPregrado");
		List<Presw25DTO> resultadoPresupIngresosPregrado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupIngresosPregrado");
		int expresado = Util.validaParametro(accionweb.getParameter("expresado"), 0);
			
		Vector datos = new Vector();
		Vector lista = new Vector();
		Vector lista2 = new Vector();
		Vector lista3 = new Vector();
		MathTool mathtool = new MathTool();
    	int i=0;
    	List<Presw25DTO> listaTotalIngreso = new ArrayList<Presw25DTO>();
    	listaTotalIngreso = moduloPresupuesto.retornaTotalIngresos(presupIngresosPregradoAnt, expresado);
    	
		for (Presw25DTO ss : presupIngresosPregradoAnt){	
			i++;
		    datos = new Vector();
			datos.addElement(ss.getDesite().trim());
			/*solicitaron eliminar la morosidad   09/09/2011
			 * if(i==presupIngresosPregradoAnt.size()){
				datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres07(),100)));
				datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres06(),100)));
				datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres05(),100)));
				datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres04(),100)));
				datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres03(),100)));
				datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres02(),100)));
				datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres01(),100)));
				} else {*/
			     if(i < presupIngresosPregradoAnt.size()){
					for(Presw25DTO listaTotal : listaTotalIngreso)	 {
				   		if(expresado == 1){
				   	   		datos.addElement(ss.getPres07());
					   		datos.addElement(ss.getPres06());
					   		datos.addElement(ss.getPres05());
					   		datos.addElement(ss.getPres04());
					   		datos.addElement(ss.getPres03());
					   		datos.addElement(ss.getPres02());
					   		datos.addElement(ss.getPres01());
				   			
				   		} else {
				   		// en caso de ser saldo del a�o la f�rmula a aplicar es SA/Tot ingresos + SA
				   			if (ss.getDesite().substring(0,5).equals("SALDO")){
				   				if((listaTotal.getPres07() + ss.getPres07()) > 0)
				   				datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres07(),mathtool.add(listaTotal.getPres07(),ss.getPres07())),100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres06() + ss.getPres06()) > 0)
						   				datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres06(),mathtool.add(listaTotal.getPres06(),ss.getPres06())),100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres05() + ss.getPres05()) > 0)
						   			datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres05(),mathtool.add(listaTotal.getPres05(),ss.getPres05())),100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres04() + ss.getPres04()) > 0)
						   			datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres04(),mathtool.add(listaTotal.getPres04(),ss.getPres04())),100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres03() + ss.getPres03()) > 0)
						   			datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres03(),mathtool.add(listaTotal.getPres03(),ss.getPres03())),100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres02() + ss.getPres02()) > 0)
						   			datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres02(),mathtool.add(listaTotal.getPres02(),ss.getPres02())),100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres01() + ss.getPres01()) > 0)
						   			datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres01(),mathtool.add(listaTotal.getPres01(),ss.getPres01())),100)));
				   				else
				   					datos.addElement("0");
					
				   				
				   			} else {
				   				if((listaTotal.getPres07() + ss.getPres07()) > 0)
				   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres07(),listaTotal.getPres07()), 100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres06() + ss.getPres06()) > 0)
				   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres06(),listaTotal.getPres06()), 100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres05() + ss.getPres05()) > 0)
				   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres05(),listaTotal.getPres05()), 100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres04() + ss.getPres04()) > 0)
				   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres04(),listaTotal.getPres04()), 100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres03() + ss.getPres03()) > 0)
				   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres03(),listaTotal.getPres03()), 100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres02() + ss.getPres02()) > 0)
				   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres02(),listaTotal.getPres02()), 100)));
				   				else
				   					datos.addElement("0");
				   				if((listaTotal.getPres01() + ss.getPres01()) > 0)
				   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres01(),listaTotal.getPres01()), 100)));
				   				else
				   					datos.addElement("0");
						   		
				   			}   	   		
				   			}
				   	      }
					  lista.add(datos);	
				}
			
				
		  
		}
	accionweb.getSesion().setAttribute("listaIngresosPregradoAnt", lista);
	i=0;
  	listaTotalIngreso = new ArrayList<Presw25DTO>();
	listaTotalIngreso = moduloPresupuesto.retornaTotalIngresos(comparaPresupIngresosPregrado, expresado);
	for (Presw25DTO ss : comparaPresupIngresosPregrado){	
		i++;
	    datos = new Vector();
		datos.addElement(ss.getDesite().trim());
		/**	/*solicitaron eliminar la morosidad   09/09/2011
		 * 
		 if(i==comparaPresupIngresosPregrado.size()){
			datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres07(),100)));
			datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres06(),100)));
			datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres05(),100)));
			datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres04(),100)));
			datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres03(),100)));
			datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres02(),100)));
			datos.addElement(mathtool.roundTo(2, mathtool.div(ss.getPres01(),100)));
			} else {*/
		    if(i < comparaPresupIngresosPregrado.size()){
				for(Presw25DTO listaTotal : listaTotalIngreso)	 {
			   		if(expresado == 1){
			   	   		datos.addElement(ss.getPres07());
				   		datos.addElement(ss.getPres06());
				   		datos.addElement(ss.getPres05());
				   		datos.addElement(ss.getPres04());
				   		datos.addElement(ss.getPres03());
				   		datos.addElement(ss.getPres02());
				   		datos.addElement(ss.getPres01());
			   			
			   		} else {
			   			// en caso de ser saldo del a�o la f�rmula a aplicar es SA/Tot ingresos + SA
			   			if (ss.getDesite().substring(0,5).equals("SALDO")){
			   				if((listaTotal.getPres07() + ss.getPres07()) > 0)
			   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres07(),mathtool.add(listaTotal.getPres07(),ss.getPres07())),100)));
			   				else
			   					datos.addElement("0");
			   				if((listaTotal.getPres06() + ss.getPres06()) > 0)
			   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres06(),mathtool.add(listaTotal.getPres06(),ss.getPres06())),100)));
			   				else
			   					datos.addElement("0");
			   				if((listaTotal.getPres05() + ss.getPres05()) > 0)
			   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres05(),mathtool.add(listaTotal.getPres05(),ss.getPres05())),100)));
			   				else
			   					datos.addElement("0");
			   				if((listaTotal.getPres04() + ss.getPres04()) > 0)
			   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres04(),mathtool.add(listaTotal.getPres04(),ss.getPres04())),100)));
			   				else
			   					datos.addElement("0");
			   				if((listaTotal.getPres03() + ss.getPres03()) > 0)
			   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres03(),mathtool.add(listaTotal.getPres03(),ss.getPres03())),100)));
			   				else
			   					datos.addElement("0");
			   				if((listaTotal.getPres02() + ss.getPres02()) > 0)
			   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres02(),mathtool.add(listaTotal.getPres02(),ss.getPres02())),100)));
			   				else
			   					datos.addElement("0");
			   				if((listaTotal.getPres01() + ss.getPres01()) > 0)
			   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres01(),mathtool.add(listaTotal.getPres01(),ss.getPres01())),100)));
			   				else
			   					datos.addElement("0");
				
			   				
			   			} else {
			   	
			   			if((listaTotal.getPres07() + ss.getPres07()) > 0)
			   				datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres07(),listaTotal.getPres07()), 100)));
			   			else
		   					datos.addElement("0");
		   				if((listaTotal.getPres06() + ss.getPres06()) > 0)
		   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres06(),listaTotal.getPres06()), 100)));
		   				else
		   					datos.addElement("0");
		   				if((listaTotal.getPres05() + ss.getPres05()) > 0)
		   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres05(),listaTotal.getPres05()), 100)));
		   				else
		   					datos.addElement("0");
		   				if((listaTotal.getPres04() + ss.getPres04()) > 0)
		   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres04(),listaTotal.getPres04()), 100)));
		   				else
		   					datos.addElement("0");
		   				if((listaTotal.getPres03() + ss.getPres03()) > 0)
		   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres03(),listaTotal.getPres03()), 100)));
		   				else
		   					datos.addElement("0");
		   				if((listaTotal.getPres02() + ss.getPres02()) > 0)
		   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres02(),listaTotal.getPres02()), 100)));
		   				else
		   					datos.addElement("0");
		   				if((listaTotal.getPres01() + ss.getPres01()) > 0)
		   					datos.addElement(mathtool.roundTo(2, mathtool.mul(mathtool.div(ss.getPres01(),listaTotal.getPres01()), 100)));
		   				else
		   					datos.addElement("0");
		   				
			   			}
					   	   		}
			   	      }
				 lista2.add(datos);
			}
		
			
	   
	}
accionweb.getSesion().setAttribute("listaComparaPresupIngresosPregrado", lista2);
i=0;
for (Presw25DTO ss : resultadoPresupIngresosPregrado){	
	   datos = new Vector();
	    datos.addElement(ss.getDesite().trim());
	    if((ss.getPres07()) > 0)
	    	datos.addElement(mathtool.roundTo(1, mathtool.div(ss.getPres07(),100)));
	    else
				datos.addElement("0");
	    if((ss.getPres06()) > 0)
	    	datos.addElement(mathtool.roundTo(1, mathtool.div(ss.getPres06(),100)));
	    else
				datos.addElement("0");
	    if((ss.getPres05()) > 0)
	    	datos.addElement(mathtool.roundTo(1, mathtool.div(ss.getPres05(),100)));
	    else
				datos.addElement("0");
	    if((ss.getPres04()) > 0)
	    	datos.addElement(mathtool.roundTo(1, mathtool.div(ss.getPres04(),100)));
	    else
				datos.addElement("0");
	    if((ss.getPres03()) > 0)
	    	datos.addElement(mathtool.roundTo(1, mathtool.div(ss.getPres03(),100)));
	    else
				datos.addElement("0");
	    if((ss.getPres02()) > 0)
	    	datos.addElement(mathtool.roundTo(1, mathtool.div(ss.getPres02(),100)));
	    else
				datos.addElement("0");
	    if((ss.getPres01()) > 0)
	    	datos.addElement(mathtool.roundTo(1, mathtool.div(ss.getPres01(),100)));
	    else
				datos.addElement("0");
	
	
		
    lista3.add(datos);
}
accionweb.getSesion().setAttribute("listaResultadoPresupIngresosPregrado", lista3);
	
			   
	}
	
	public void cargaIndicadores(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
	    int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		
		if(rutOrigen > 0){
			this.limpiaSimulador(accionweb);
		}	
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		accionweb.agregarObjeto("control", control);
		accionweb.agregarObjeto("menuIndicadores", 1); // es para no mostrar el menu en ingreso

		
	}
	public void cargaIndicadorPresup(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
	    int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		String titulo = Util.validaParametro(accionweb.getParameter("titulo"),"");
		String tipo = Util.validaParametro(accionweb.getParameter("tipo"),"");
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		List<Presw25DTO> listaIndicador = null;
		
		if(rutOrigen > 0){
			this.limpiaSimulador(accionweb);
		}	
		if (anno > 0){
			listaIndicador = 	moduloPresupuesto.getListaPresw25(tipo, rutUsuario, anno, dv, 0, 0, 0);
			accionweb.agregarObjeto("listaIndicador", listaIndicador);
			accionweb.agregarObjeto("annoFin", anno);
			accionweb.agregarObjeto("annoIni", (anno - 6));
			accionweb.agregarObjeto("annoselec", anno);
			
		}
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		accionweb.agregarObjeto("titulo", titulo);	
		accionweb.agregarObjeto("control", control);
		accionweb.agregarObjeto("tipo", tipo);
		accionweb.agregarObjeto("menuIndicadores", 1); // es para no mostrar el menu en ingreso

		
	}
	
	public void exportarIndicador(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
	    int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		String tipo = Util.validaParametro(accionweb.getParameter("tipo"),"");
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		List<Presw25DTO> listaIndicador = null;
		Vector vecAnno = new Vector();
		Vector datos = new Vector();
		Vector lista = new Vector();
		String titulo = "";
	
		if (anno > 0){
			listaIndicador = 	moduloPresupuesto.getListaPresw25(tipo, rutUsuario, anno, dv, 0, 0, 0);
			
			
            for(int i=(anno - 6); i<= anno ;i++){
        	if(i < anno)
        		vecAnno.addElement(i); 
        	else
        		vecAnno.addElement(i + "*");
        	
            }
		
		MathTool mathtool = new MathTool();
    	int i=0;
		for (Presw25DTO ss : listaIndicador){	
			i++;
		    datos = new Vector();
			if(tipo.trim().equals("IIP")){ // indicador de presupuestacion
				titulo = "Indicadores de Presupuestaci�n";	
			
				datos.addElement(ss.getComen1().trim());
				datos.addElement(ss.getPres07());
				datos.addElement(ss.getPres06());
				datos.addElement(ss.getPres05());
				datos.addElement(ss.getPres04());
				datos.addElement(ss.getPres03());
				datos.addElement(ss.getPres02());
				datos.addElement(ss.getPres01());
				
			}else {
				titulo = "Indicadores de Gecti�n Patrimonial";	
			
				datos.addElement(ss.getComen1().trim());
				datos.addElement(ss.getPres07());
				datos.addElement(ss.getPres06());
				datos.addElement(ss.getPres05());
				datos.addElement(ss.getPres04());
				datos.addElement(ss.getPres03());
				datos.addElement(ss.getPres02());
				datos.addElement(ss.getPres01());
			}
				
		    lista.add(datos);
		}

	   
	   
		moduloPresupuesto.agregaIndicadores(accionweb.getReq(), lista);
        accionweb.getSesion().setAttribute("annoIndicadores", vecAnno);
        accionweb.getSesion().setAttribute("titulo", titulo);
        accionweb.getSesion().setAttribute("tipo", tipo);
		}	   
	}
	public void presupInstitucionalExportar(AccionWeb accionweb) throws Exception {
		List<Presw25DTO> listado = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listado"); 
		String tipo = Util.validaParametro(accionweb.getParameter("tipo"), "");
		int tipoGraf = Util.validaParametro(accionweb.getParameter("tipoGraf"), 0); // en este caso 1 se refiere a ingresos y 2 se refiere a egresos 
		
		Vector vec_detalleET1 = new Vector();
		Vector vec_detalleET2 = new Vector();
		Vector vec_detalleET3 = new Vector();
		Vector vec_datos1 = new Vector();
		Vector vec_datos2 = new Vector();
		Vector vec_datos3 = new Vector();
		String valorSalida = "";
		String chart = "chartLine.js";
		int margen = 130;
	      
		String parametroServicio = "";
		String titulo = "Series de Tiempo e Informaci�n Estad�stica ";
		String label_line1 = "";
		String label_line2 = ""; 
		String label_line3 = "";
		String label_line4 = "";
		String label_line5 = "";
		String label_line6 = "";
		String label_line7 = "";
		String subtitulo = "";
		String coma = "";
		String conceptos = "[";
	   	int annoIni = Util.validaParametro(accionweb.getParameter("annoIni"), 0);
    	int annoFin = Util.validaParametro(accionweb.getParameter("annoFin"), 0);
    	margen = 250;
   
        if(tipoGraf == 1) {
        if(tipo.trim().equals("PIF"))	
            subtitulo = "Presupuesto Institucional de Ingresos Fondos Centrales.";
        else
        	 subtitulo = "Presupuesto Institucional de Ingresos Global.";
        }
        else{
            if(tipo.trim().equals("PIF"))	
                subtitulo = "Presupuesto Institucional de Egresos Fondos Centrales.";
            else
            	 subtitulo = "Presupuesto Institucional de Egresos Global.";
        }
	       
	/*		int j=1; 
			for (Presw25DTO lista : listado){
				if((tipoGraf == 1 && lista.getIndexi().trim().equals("I") && lista.getComen1().indexOf("Total") < 0 ) ||
				  (tipoGraf == 2 && lista.getIndexi().trim().equals("E") && lista.getComen1().indexOf("Total") < 0)	){
			
			   	switch(j){
            	case 1: label_line1 = lista.getComen1().trim();
            	break;
            	case 2: label_line2 = lista.getComen1().trim();
            	break;
            	case 3: label_line3 = lista.getComen1().trim();
            	break;
            	case 4: label_line4 = lista.getComen1().trim();
            	break;
            	case 5: label_line5 = lista.getComen1().trim();
            	break;
            	case 6: label_line6 = lista.getComen1().trim();
            	break;
            	case 7: label_line7 = lista.getComen1().trim();
            	break;
            	} 
			   	j++;
				}
           }*/
       // } 
        
		
		
		  try {
		
                  // nuevo chart con jquery
             if(listado.size() > 0) {
    
					//int ind = 1;
					 String line1 = "[";
		             String line2 = "[";
		             String line3 = "[";
		             String line4 = "[";
		             String line5 = "[";
		             String line6 = "[";
		             String line7 = "[";
		             coma = "";
		             long max = 0;
		             int k=0;
		              
		      	for (Presw25DTO lista : listado){
		   			vec_detalleET1 = new Vector();
					vec_detalleET2 = new Vector();
					vec_detalleET3 = new Vector();
					if((tipoGraf == 1 && lista.getIndexi().trim().equals("I") && lista.getComen1().indexOf("Total") < 0 ) ||
						(tipoGraf == 2 && lista.getIndexi().trim().equals("E") && lista.getComen1().indexOf("Total") < 0)	){
				         coma = ",";

					  	k++;
			        	 switch (k){
		     			 case 1:  {  label_line1 = lista.getComen1().trim();
		     			        line1 = line1 + lista.getPres06()+ coma + lista.getPres05() + coma + lista.getPres04() + coma + lista.getPres03() + coma + lista.getPres02() + coma + lista.getPres01();
 			                    conceptos = conceptos + "'" + annoFin + "'" ;
		     			 		break;
		     			 }
		     			 case 2:{  label_line2 = lista.getComen1().trim();
	     			        line2 = line2 + lista.getPres06()+ coma + lista.getPres05() + coma + lista.getPres04() + coma + lista.getPres03() + coma + lista.getPres02() + coma + lista.getPres01();
	 			                  conceptos = conceptos + coma + "'" + (annoFin + 1)+ "'" ;
    		
		     			 		break;
		     			 }
		     			 case 3: {  label_line3 = lista.getComen1().trim();
	     			        line3 = line3 + lista.getPres06()+ coma + lista.getPres05() + coma + lista.getPres04() + coma + lista.getPres03() + coma + lista.getPres02() + coma + lista.getPres01();
	 			                      conceptos = conceptos + coma + "'" + (annoFin+2)+ "'" ;
    		            	break;
		     			 }
		     			 case 4: { label_line4 = lista.getComen1().trim();
	     			               line4 = line4 + lista.getPres06()+ coma + lista.getPres05() + coma + lista.getPres04() + coma + lista.getPres03() + coma + lista.getPres02() + coma + lista.getPres01();
	 			                   conceptos = conceptos + coma + "'" + (annoFin+3)+ "'" ;
    				       break;
		     			 }
		     			 case 5:{  label_line5 = lista.getComen1().trim();
	     			               line5 = line5 + lista.getPres06()+ coma + lista.getPres05() + coma + lista.getPres04() + coma + lista.getPres03() + coma + lista.getPres02() + coma + lista.getPres01();
	 			                   conceptos = conceptos + coma + "'" + (annoFin+4)+ "'" ;
		                    break;
		     			 }
		     			 case 6:{  label_line6 = lista.getComen1().trim();
	     			               line6 = line6 + lista.getPres06()+ coma + lista.getPres05() + coma + lista.getPres04() + coma + lista.getPres03() + coma + lista.getPres02() + coma + lista.getPres01();
	 			                   conceptos = conceptos + coma + "'" + (annoFin+5)+ "'" ;
		                    break;
		     			 }
		     			 case 7: { label_line7 = lista.getComen1().trim();
	     			               line7 = line7 + lista.getPres06()+ coma + lista.getPres05() + coma + lista.getPres04() + coma + lista.getPres03() + coma + lista.getPres02() + coma + lista.getPres01();
	 			                   conceptos = conceptos + coma + "'" + (annoFin+6)+ "'" ;     		
		     			        break;
		     			 }
		     			 }
		
		      	}
				}				
			
				       line1 = line1 + "]";
	          		   line2 = line2 + "]";
	          		   line3 = line3 + "]";
	          		   line4 = line4 + "]";
	          		   line5 = line5 + "]";
	          		   line6 = line6 + "]";
	          		   line7 = line7 + "]";
	          		   conceptos = conceptos + "]";	  
	          		   
	          		 String series = "[{ name:'" + label_line1 + "', data:" + line1+ "}";
	          		 	if(!label_line2.trim().equals(""))		          		
	          		        series += ", { name:'" + label_line2+"', data:" + line2 + "}";
		          		if(!label_line3.trim().equals(""))
		          			series += ", {name:'" + label_line3 + "', data:" + line3 + "}";
		          		if(!label_line4.trim().equals(""))
		          			series += ", {name:'" + label_line4 + "', data:" + line4 + "}";
		          		if(!label_line5.trim().equals(""))
		          			series += ", {name:'" + label_line5 + "', data:" + line5 + "}";
		          		if(!label_line6.trim().equals(""))
		          			series += ", {name:'" + label_line6 + "', data:" + line6 + "}";
		          		if(!label_line7.trim().equals(""))
		          			series += ", {name:'" + label_line7 + "', data:" + line7 + "}";
		          		series += "]";
			  
				/*calculo del maximo del grafico*/
				
				String maxiValor = String.valueOf(max);
				String valorRec = "";
				int estilo = (((listado.size()-1)/2) * 100) + 100;
				accionweb.agregarObjeto("valorSalida", valorSalida);
				accionweb.agregarObjeto("series", series);
				accionweb.agregarObjeto("conceptos", conceptos);
				accionweb.agregarObjeto("maximo", valorRec);
				accionweb.agregarObjeto("estilo", estilo+"px");		
		  		accionweb.agregarObjeto("margen", margen);
		  		accionweb.agregarObjeto("titulo", titulo);
		  		accionweb.agregarObjeto("subtitulo", subtitulo);
		  		accionweb.agregarObjeto("chart", chart);		  		
		  		
	    }
			  
		 	  } catch (Exception E) {
		        System.out.println("excepcion   " + E);
			   }
		  
		}
	/*metodos de plan de desarrollo*/
	public void cargaPresupPlan(AccionWeb accionweb) throws Exception {
       // this.cargaPresup(accionweb);
//System.out.println("carga presup");
		// ver si tiene plan de desarrollo
		int a�o =Integer.parseInt( Util.validaParametro(accionweb.getSesion().getAttribute("anno")+"","0"));
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int codActividad = Util.validaParametro(accionweb.getParameter("codActividad"), 0);
		String nomActividad = Util.validaParametro(accionweb.getParameter("nomActividad"),"");
		String nombre = "";
		String codgru = "";
		
		List<Presw25DTO> listaPRESW25 = null;
		PreswBean preswbean = new PreswBean("CPR", 0,0,a�o,0, rutUsuario);
		preswbean.setDigide(dv);
	
		if(preswbean != null) {
		listaPRESW25 = (List<Presw25DTO>) preswbean.consulta_presw25();
		accionweb.getSesion().setAttribute("resultadoCPR", listaPRESW25);

		for (Presw25DTO ss : listaPRESW25){
			if(ss.getRespue().trim().equals("D")){
				nombre = ss.getComen3().trim();	
				if(nombre.length() > 35)
					nombre = nombre.substring(0,35);
				nombre = ss.getComen2().trim() + " - "+ nombre.trim();
				codgru = ss.getComen2().trim();
			}
		}
		accionweb.agregarObjeto("nombre", nombre);
		accionweb.agregarObjeto("codgru", codgru);
		accionweb.agregarObjeto("unidadesPresup", listaPRESW25);
		}
		
		/*esto es para el menu de control presupuestario*/
	    int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		accionweb.getSesion().setAttribute("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		/* fin */
		
		
		
		
		
		
		List<Presw25DTO> listaPlanDesarrollo = new ArrayList<Presw25DTO>();
		if(accionweb.getSesion().getAttribute("listaPlanDesarrollo") != null)
			accionweb.getSesion().removeAttribute("listaPlanDesarrollo");
		 preswbean = new PreswBean("PDR", 0,0,a�o,0, rutUsuario);
		if(preswbean != null) {
			preswbean.setDigide(dv);
			preswbean.setCajero(codgru);
			listaPlanDesarrollo = (List<Presw25DTO>) preswbean.consulta_presw25(); 
			if(listaPlanDesarrollo != null && listaPlanDesarrollo.size() > 0){
			   accionweb.agregarObjeto("planDesarrollo", listaPlanDesarrollo);
			   accionweb.getSesion().setAttribute("planDesarrollo", listaPlanDesarrollo);
				for (Presw25DTO ss : listaPlanDesarrollo){
					accionweb.agregarObjeto("motiv1", ss.getMotiv1().trim());
					accionweb.agregarObjeto("motiv2", ss.getMotiv2().trim());
					accionweb.agregarObjeto("motiv3", ss.getMotiv3().trim());
					accionweb.agregarObjeto("motiv4", ss.getMotiv4().trim());
					accionweb.agregarObjeto("motiv5", ss.getMotiv5().trim());
					accionweb.agregarObjeto("motiv6", ss.getMotiv6().trim());
				}
				List<Presw25DTO> listaPlanActividad = new ArrayList<Presw25DTO>();
				 preswbean = new PreswBean("GLP", 0,0,a�o,0, rutUsuario);
				 preswbean.setCajero(codgru);
				 listaPlanActividad = preswbean.consulta_presw25();
				 accionweb.agregarObjeto("listaPlanActividad", listaPlanActividad);
							
		}else {
				List<Presw25DTO> listaEjeEstrategico = new ArrayList<Presw25DTO>();
				listaEjeEstrategico = moduloPresupuesto.getListaEjeEstrategico();
				
				List<Presw25DTO> listaAreaAcreditacion = new ArrayList<Presw25DTO>();
				listaAreaAcreditacion = moduloPresupuesto.getListaAreaAcreditacion();
				accionweb.agregarObjeto("listaEjeEstrategico", listaEjeEstrategico);
				accionweb.agregarObjeto("listaAreaAcreditacion", listaAreaAcreditacion);
			}
		}	
		accionweb.agregarObjeto("actividadSeleccionada", codActividad); 
		accionweb.agregarObjeto("nomActividad", nomActividad);
		accionweb.agregarObjeto("codActividad", codActividad);
		accionweb.agregarObjeto("anno", a�o); 
		Collection<Presw18DTO> listaUnidad = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("resultadoCPR");
		accionweb.agregarObjeto("unidadesPresup", listaUnidad);
		//System.out.println("carga presup final");
		
		int volver = Util.validaParametro(accionweb.getParameter("volver"), 0);
		// int itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), 0);	
		String itedoc = Util.validaParametro(accionweb.getParameter("itedoc"),"");	
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		int menu = Util.validaParametro(accionweb.getParameter("menu"), 0);
	
		
		accionweb.agregarObjeto("anno", a�o); 
		accionweb.agregarObjeto("volver", volver);
		accionweb.agregarObjeto("itedoc", itedoc);
		accionweb.agregarObjeto("control", control);

		
				
	}
	 synchronized public void registraPresupActividad(AccionWeb accionweb) throws Exception {
		int a�o =Integer.parseInt( Util.validaParametro(accionweb.getSesion().getAttribute("anno")+"","0"));
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		String descripcionActiv = Util.validaParametro(accionweb.getParameter("descripcionActiv"), "");	
		String objetivoActiv = Util.validaParametro(accionweb.getParameter("objetivoActiv"), "");	
		String ejeActiv = Util.validaParametro(accionweb.getParameter("ejeActiv"), "");	
		String areaActiv = Util.validaParametro(accionweb.getParameter("areaActiv"), "");
        int rutide = Util.validaParametro(accionweb.getParameter("rutide"), 0);
        String digide = Util.validaParametro(accionweb.getParameter("digide"), "");	
        String nombreResponsable = Util.validaParametro(accionweb.getParameter("nombreResponsable"), "");	
		String tipoActividad = Util.validaParametro(accionweb.getParameter("tipoActividad"),"");
		int numActividad = Util.validaParametro(accionweb.getParameter("numActividad"), 0);
		String accion = Util.validaParametro(accionweb.getParameter("accion"), "");
		long totalActiv = Util.validaParametro(accionweb.getParameter("totalActiv"), 0);
		String codgru = Util.validaParametro(accionweb.getParameter("codgru"), "");	
			
		List<Presw25DTO> listaPresw25DTO = moduloPresupuesto.agregaDatosActividad(accionweb.getReq(),codgru,descripcionActiv,objetivoActiv,ejeActiv, areaActiv, rutide, digide,nombreResponsable, a�o, numActividad,
				accion,	totalActiv, tipoActividad);
		
		try {
	
			boolean graba = moduloPresupuesto.savePresupActividad(listaPresw25DTO, rutide, digide, a�o, codgru);
			if(graba) {
				if(accion.trim().equals(""))
				   accionweb.agregarObjeto("mensaje", "Registr� exitosamente el proyecto del Plan de desarrollo.");
				else
					accionweb.agregarObjeto("mensaje", "Registr� exitosamente la Eliminaci�n de proyecto del Plan de desarrollo.");
				
				List<Presw25DTO> listaEjeEstrategico = new ArrayList<Presw25DTO>();
				listaEjeEstrategico = moduloPresupuesto.getListaEjeEstrategico();
				
				List<Presw25DTO> listaAreaAcreditacion = new ArrayList<Presw25DTO>();
				listaAreaAcreditacion = moduloPresupuesto.getListaAreaAcreditacion();
				List<Presw25DTO> listaTipoActividad = new ArrayList<Presw25DTO>();
				listaTipoActividad = moduloPresupuesto.getListaTipoActividad();
				descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
				Collection listaFuncionarios = funcionarioBean.todos_funcionarios();
				List<FuncionarioDTO> listaFunc = new ArrayList<FuncionarioDTO>(listaFuncionarios);

				accionweb.agregarObjeto("listaEjeEstrategico", listaEjeEstrategico);
				accionweb.agregarObjeto("listaAreaAcreditacion", listaAreaAcreditacion);
				accionweb.agregarObjeto("listaFunc", listaFuncionarios);
				accionweb.agregarObjeto("listaTipoActividad", listaTipoActividad);
				if(numActividad > 0 && accion.trim().equals(""))
					modificarActividad(accionweb);
			} else
				accionweb.agregarObjeto("mensaje", "No registra Proyecto del Plan de desarrollo.");
			
			this.cargaPresupPlan(accionweb);
			accionweb.agregarObjeto("actividadSeleccionada", numActividad);
	    	accionweb.agregarObjeto("control", control);
			
		}catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No registra Actividad del Plan de desarrollo.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
			
		
	} 
	
	
	
	 synchronized public void registraObservacionPlan(AccionWeb accionweb) throws Exception {
		int a�o =Integer.parseInt( Util.validaParametro(accionweb.getSesion().getAttribute("anno")+"","0"));
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
//System.out.println("registraObservacionPlan");
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		String motiv1 = Util.validaParametro(accionweb.getParameter("motiv1"), "");
		String motiv2 = Util.validaParametro(accionweb.getParameter("motiv2"), "");
		String motiv3 = Util.validaParametro(accionweb.getParameter("motiv3"), "");
		String motiv4 = Util.validaParametro(accionweb.getParameter("motiv4"), "");
		String motiv5 = Util.validaParametro(accionweb.getParameter("motiv5"), "");
		String motiv6 = Util.validaParametro(accionweb.getParameter("motiv6"), "");	
		String comen2 = Util.validaParametro(accionweb.getParameter("codgru"), "");	
		if(motiv1.length() > 40)
			motiv1 = motiv1.substring(0,39);
		if(motiv2.length() > 40)
			motiv2 = motiv2.substring(0,39);
		if(motiv3.length() > 40)
			motiv3 = motiv3.substring(0,39);
		if(motiv4.length() > 40)
			motiv4 = motiv4.substring(0,39);
		if(motiv5.length() > 40)
			motiv5 = motiv5.substring(0,39);
		if(motiv6.length() > 40)
			motiv6 = motiv6.substring(0,39);
		if(comen2.length() > 40)
			comen2 = comen2.substring(0,39);
		
		
		int accion = Util.validaParametro(accionweb.getParameter("accion"), 0); // 1:registrar    2:modificar   3: eliminar
		
		List<Presw25DTO> listaPresw25DTO = moduloPresupuesto.agregaObservacionPlan(accionweb.getReq(), motiv1, motiv2, motiv3, motiv4, motiv5, motiv6, comen2, a�o);
		
		try {
	
			boolean graba = moduloPresupuesto.saveObservacionPlan(listaPresw25DTO, rutUsuario, dv, a�o);
			if(graba) {
				switch(accion){
				case 1:{ accionweb.agregarObjeto("mensaje", "Registr� exitosamente la Observaci�n del Plan de desarrollo.");
				break;
				}
				case 2:{		accionweb.agregarObjeto("mensaje", "Registr� exitosamente la Modificaci�n de la Observaci�n del Plan de desarrollo.");
				break;					
				}
				case 3:{		accionweb.agregarObjeto("mensaje", "Registr� exitosamente la Eliminaci�n de la Observaci�n del Plan de desarrollo.");
				break;					
				}
				}
				
			} else
				accionweb.agregarObjeto("mensaje", "No registra Observaci�n del Plan de desarrollo.");
			
			this.cargaPresupPlan(accionweb);
			accionweb.agregarObjeto("control", control);
			
		}catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No registra Observaci�n del Plan de desarrollo.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
			
		
	} 
	 synchronized public void agregaActividad(AccionWeb accionweb) throws Exception {
		String nombre = Util.validaParametro(accionweb.getParameter("nombre"), "");
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String motiv1 = Util.validaParametro(accionweb.getParameter("motiv1"), "");
		String motiv2 = Util.validaParametro(accionweb.getParameter("motiv2"), "");
		String motiv3 = Util.validaParametro(accionweb.getParameter("motiv3"), "");
		String motiv4 = Util.validaParametro(accionweb.getParameter("motiv4"), "");
		String motiv5 = Util.validaParametro(accionweb.getParameter("motiv5"), "");
		String motiv6 = Util.validaParametro(accionweb.getParameter("motiv6"), "");
		String codgru = Util.validaParametro(accionweb.getParameter("codgru"), "");

		List<Presw25DTO> listaEjeEstrategico = new ArrayList<Presw25DTO>();
		listaEjeEstrategico = moduloPresupuesto.getListaEjeEstrategico();
		
		List<Presw25DTO> listaAreaAcreditacion = new ArrayList<Presw25DTO>();
		listaAreaAcreditacion = moduloPresupuesto.getListaAreaAcreditacion();
		List<Presw25DTO> listaTipoActividad = new ArrayList<Presw25DTO>();
		listaTipoActividad = moduloPresupuesto.getListaTipoActividad();
		descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
		Collection listaFuncionarios = funcionarioBean.todos_funcionarios();
		List<FuncionarioDTO> listaFunc = new ArrayList<FuncionarioDTO>(listaFuncionarios);
	/*	for(FuncionarioDTO ss : listaFunc){
			ss.getNomcom();
			ss.getRutide();
		}*/
		accionweb.agregarObjeto("listaFunc", listaFunc);
		accionweb.agregarObjeto("listaEjeEstrategico", listaEjeEstrategico);
		accionweb.agregarObjeto("listaAreaAcreditacion", listaAreaAcreditacion);
		accionweb.agregarObjeto("listaTipoActividad", listaTipoActividad);
	    accionweb.agregarObjeto("nombre", nombre);
	    accionweb.agregarObjeto("motiv1", motiv1);
	    accionweb.agregarObjeto("motiv2", motiv2);
		accionweb.agregarObjeto("motiv3", motiv3);
		accionweb.agregarObjeto("motiv4", motiv4);
	    accionweb.agregarObjeto("motiv5", motiv5);
	    accionweb.agregarObjeto("motiv6", motiv6); 
	    accionweb.agregarObjeto("codgru", codgru);

	}

	 synchronized public void modificarActividad(AccionWeb accionweb) throws Exception {
		String nombre = Util.validaParametro(accionweb.getParameter("nombre"), "");
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int codActividad = Util.validaParametro(accionweb.getParameter("codActividad"), 0);
		int a�o =Integer.parseInt( Util.validaParametro(accionweb.getSesion().getAttribute("anno")+"","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		String codgru = Util.validaParametro(accionweb.getParameter("codgru"), "");
		

		List<Presw25DTO> listaEjeEstrategico = new ArrayList<Presw25DTO>();
		listaEjeEstrategico = moduloPresupuesto.getListaEjeEstrategico();
		
		List<Presw25DTO> listaAreaAcreditacion = new ArrayList<Presw25DTO>();
		listaAreaAcreditacion = moduloPresupuesto.getListaAreaAcreditacion();
		List<Presw25DTO> listaTipoActividad = new ArrayList<Presw25DTO>();
		listaTipoActividad = moduloPresupuesto.getListaTipoActividad();
		descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
		Collection listaFuncionarios = funcionarioBean.todos_funcionarios();
		List<FuncionarioDTO> listaFunc = new ArrayList<FuncionarioDTO>(listaFuncionarios);

		
		accionweb.agregarObjeto("listaEjeEstrategico", listaEjeEstrategico);
		accionweb.agregarObjeto("listaAreaAcreditacion", listaAreaAcreditacion);
	    accionweb.agregarObjeto("nombre", nombre);
	    accionweb.agregarObjeto("listaTipoActividad", listaTipoActividad);
		accionweb.agregarObjeto("listaFunc", listaFuncionarios);
		accionweb.agregarObjeto("codgru", codgru);  // descripcion de grupo
		
		List<Presw25DTO> listaPlanActividad = new ArrayList<Presw25DTO>();
		PreswBean preswbean = new PreswBean("GLP", 0,0,a�o,0, rutUsuario);
		 preswbean.setCajero(codgru);
		 listaPlanActividad = preswbean.consulta_presw25();
		 accionweb.agregarObjeto("listaPlanActividad", listaPlanActividad);
		
	  //  List<Presw25DTO> listaPRESW25 = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listaPlanActividad");
	    	if(listaPlanActividad != null && listaPlanActividad.size() > 0){
			for (Presw25DTO ss : listaPlanActividad){
				if(ss.getNumreq() == codActividad){
					accionweb.agregarObjeto("descripcion", ss.getComen1().trim()+ss.getComen2().trim());
					accionweb.agregarObjeto("objetivo", ss.getMotiv1().trim() + ss.getMotiv2().trim());
					accionweb.agregarObjeto("ejeEstrategico", ss.getComen4().trim());
					accionweb.agregarObjeto("areaAcreditacion", ss.getComen5().trim());
					accionweb.agregarObjeto("tipoActividad", ss.getComen6().trim());
					accionweb.agregarObjeto("totalRecurso", ss.getPres01());
					accionweb.agregarObjeto("numReq", ss.getNumreq());
					accionweb.agregarObjeto("rutide", ss.getRutide());
					accionweb.agregarObjeto("digide", ss.getDigide().trim());
					accionweb.agregarObjeto("anno",ss.getAnopre() );
				}
			}	
			
		}
		
		}
	
	 synchronized public void cargaPresupRequerimientoPlan(AccionWeb accionweb) throws Exception {
		Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		List<Presw25DTO> listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		int retorna = Util.validaParametro(accionweb.getParameter("retorna"), 0);
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		String codgru = Util.validaParametro(accionweb.getParameter("codgru"),"");
		
		
		 /*para que cuando vuelva del req no cargue nuevamente la matriz, pero ya no es necesario
		 if(retorna != 1) {
			
			moduloPresupuesto.agregaResultadoMatriz(accionweb.getReq(), listarResultadoPresup);
			listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
			}
	*/
		int codActividad = Util.validaParametro(accionweb.getParameter("codActividad"), 0);
		String nomActividad = Util.validaParametro(accionweb.getParameter("nomActividad"),"");
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int item = Util.validaParametro(accionweb.getParameter("item"), 0);
		// MB comenta para presupuesto 22/10/2015
		// if(item != 41) item = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
	    String nomItem = "";
		
		List<Presw25DTO> presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		if(presupItemreq == null || (presupItemreq != null && presupItemreq.size() == 0)) {
	
			List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
			if(rutUsuario > 0){
			PreswBean preswbean = new PreswBean("MNA", 0,item,anno,0, rutUsuario);
			preswbean.setDigide(dv);
			preswbean.setMespar(codActividad);
			preswbean.setCajero(codgru);
			if(preswbean != null) {
				listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
				moduloPresupuesto.agregaPresupRequerimientoSesion(accionweb.getReq(), listaPresw25);
				presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
			}
			}
		}
		Long en = new Long(0);
    	Long fe = new Long(0);
    	Long ma = new Long(0);
    	Long ab = new Long(0);
    	Long my = new Long(0);
    	Long jn = new Long(0);
    	Long jl = new Long(0);
    	Long ag = new Long(0);
    	Long se = new Long(0);
    	Long oc = new Long(0);
    	Long no = new Long(0);
    	Long di = new Long(0);
    	if(presupItemreq != null && presupItemreq.size() > 0){
        	
    	for (Presw25DTO ss : presupItemreq){
    		Presw25DTO presw25 = ss;
        	  
    		if(presw25.getItedoc() == item && presw25.getAnopre() == anno ){
    			  nomItem = ss.getItedoc() + " - " + ss.getDesite();
	        	//  nomUnidad = ss.getCoduni() + " - " + ss.getDesuni();
	    	switch (ss.getMespre()){	  			
	    	case 1: 	en = en + presw25.getTotpre();
	    	break;
	    	case 2: 	fe = fe + presw25.getTotpre();
	    	break;
	    	case 3:     ma = ma + presw25.getTotpre();
	    	break;
	    	case 4: 	ab = ab + presw25.getTotpre();
	    	break;
	    	case 5: 	my = my + presw25.getTotpre();
	    	break;
	    	case 6: 	jn = jn + presw25.getTotpre();
	    	break;
	    	case 7: 	jl = jl + presw25.getTotpre();
	    	break;
	    	case 8: 	ag = ag + presw25.getTotpre();
	    	break;
	    	case 9: 	se = se + presw25.getTotpre();
	    	break;
	    	case 10: 	oc = oc + presw25.getTotpre();
	    	break;
	    	case 11: 	no = no + presw25.getTotpre();
	    	break;
	    	case 12: 	di = di + presw25.getTotpre();
	    	break;
	    	}
    		}
    		if(presw25.getTotpre() == 0){
    			List<Presw25DTO>  resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
    			List<Presw25DTO> listarResultadoDet = new ArrayList<Presw25DTO>();
    			if(resultadoDetalleItemReq != null && resultadoDetalleItemReq.size() > 0){
    				for (Presw25DTO rs : resultadoDetalleItemReq){
    					  if(rs.getItedoc() == ss.getItedoc() && rs.getMespre() == ss.getMespre() && rs.getNumreq() == ss.getNumreq()){
    						  rs.setTippro("RPQ");
    						  rs.setCodgru(codgru);
    						  rs.setIndmod("D");
    						  rs.setCanman(0);
    						  rs.setTotpre(0);
    					
    					  }
    					  listarResultadoDet.add(rs);		  
    					}
    				}
    				
    				accionweb.getSesion().setAttribute("resultadoDetalleItemReq", listarResultadoDet);
    		}
    	}
		} else { for (Presw25DTO ss : listarResultadoPresup){

			          if(ss.getItedoc() == item){
			        	  nomItem = ss.getItedoc() + " - " + ss.getDesite();
			  			//System.out.println("ss.getItedoc(): "+ss.getItedoc());
			          }
			         
		           }			
		}
    	
 //   System.out.println("lista valores : "+ en +"f : "+ fe +  "ma: "+ ma +"ab: "+ ab + "may: "+my +"junio: "+ jn +"julio : " +jl + "agosto; "+ag + "sep: "+se + "oc: "+ oc +"nov . "+ no +"dic: "+ di+"TAMA�O: "+presupItemreq.size());
    accionweb.agregarObjeto("enero",en);
    accionweb.agregarObjeto("febrero",fe);
    accionweb.agregarObjeto("marzo", ma);
    accionweb.agregarObjeto("abril", ab);
    accionweb.agregarObjeto("mayo", my);
    accionweb.agregarObjeto("junio", jn);
    accionweb.agregarObjeto("julio", jl);
    accionweb.agregarObjeto("agosto", ag);
    accionweb.agregarObjeto("septiembre", se );
    accionweb.agregarObjeto("octubre", oc);
    accionweb.agregarObjeto("noviembre", no);
    accionweb.agregarObjeto("diciembre", di);
    accionweb.agregarObjeto("total", en + fe + ma + ab + my + jn + jl + ag + se + oc + no + di);
	accionweb.agregarObjeto("cuentasPresupuestarias", lista);
	accionweb.agregarObjeto("actividadSeleccionada", codActividad);
	accionweb.agregarObjeto("nomActividad", nomActividad);
	accionweb.agregarObjeto("presupItemreq", presupItemreq);
	accionweb.agregarObjeto("resultadoPresup", listarResultadoPresup);
	accionweb.agregarObjeto("anno", anno);	
	accionweb.agregarObjeto("itedoc", item);	
	accionweb.agregarObjeto("nomItem", nomItem);
	accionweb.agregarObjeto("control", control);
	accionweb.agregarObjeto("codgru", codgru);
	}
	
	public void cargaPresupItemPlan(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int codActividad = Util.validaParametro(accionweb.getParameter("codActividad"), 0);
		//int itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		String itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), "");
	//	
//		Collection<Presw18DTO> listaUnidad = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		List<Presw25DTO> listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		String nomActividad = Util.validaParametro(accionweb.getParameter("nomActividad"), "");
		
	/*	Presw25DTO presw25 = new  Presw25DTO();
		
		for (Presw25DTO ss : listarResultadoPresup){
			if(ss.getCoduni() == unidad && ss.getItedoc() == itedoc )
				presw25 = ss;
		}*/
		accionweb.agregarObjeto("codActividad", codActividad);
		accionweb.agregarObjeto("itedoc", itedoc);
	 	accionweb.agregarObjeto("resultadoPresup", listarResultadoPresup);
 	   	accionweb.agregarObjeto("actividadSeleccionada", codActividad);
 		accionweb.agregarObjeto("nomActividad", nomActividad);
 	 //   accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidad);
 	    accionweb.agregarObjeto("control", control);
	}
	
	 synchronized public void eliminaItemPlan(AccionWeb accionweb) throws Exception {
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		int codActividad = Util.validaParametro(accionweb.getParameter("codActividad"), 0);
			
		List<Presw25DTO> listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		moduloPresupuesto.eliminaItemPlan(accionweb.getReq(), listarResultadoPresup);
		listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		accionweb.agregarObjeto("control", control);
		accionweb.agregarObjeto("actividadSeleccionada", codActividad);
		this.cargaPresupPlan(accionweb);
	}
	 synchronized public void actualizaItemPlan(AccionWeb accionweb) throws Exception {
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		List<Presw25DTO> listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		String codgru = Util.validaParametro(accionweb.getParameter("codgru"),"");
		
			moduloPresupuesto.agregaItemActualizaPlan(accionweb.getReq(), listarResultadoPresup);
			listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
			accionweb.agregarObjeto("control", control);
			cargaPresupPlan(accionweb);
			Collection<Presw18DTO> listaUnidad = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("resultadoCPR");
			accionweb.agregarObjeto("unidadesPresup", listaUnidad);
		
		
	}
	 synchronized public void eliminaReqPlan(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		int codActividad = Util.validaParametro(accionweb.getParameter("codActividad"), 0);
		int itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		int sucur = Util.validaParametro(accionweb.getParameter("sucur"), 0);
		String codgru = Util.validaParametro(accionweb.getParameter("codgru"),"");
		int existe = 0;
		String nomItem = "";
		String nomActividad = Util.validaParametro(accionweb.getParameter("nomActividad"),"");
		
		List<Presw25DTO> listaPresupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		List<Presw25DTO>  resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
		List<Presw25DTO>  lista = new ArrayList<Presw25DTO>();
		if(resultadoDetalleItemReq != null && resultadoDetalleItemReq.size() > 0){
			for (Presw25DTO ss : resultadoDetalleItemReq){
				if(ss.getMespre() == mes && ss.getNumreq() == sucur)
				   existe = 1;
			}
		} else resultadoDetalleItemReq = new ArrayList<Presw25DTO>(); 
		if(resultadoDetalleItemReq == null || resultadoDetalleItemReq.size() == 0 || existe == 0){
			PreswBean preswbean = new PreswBean("RQA", 0,itedoc,a�o,mes, rutUsuario);
	    	preswbean.setDigide(dv);
	    	preswbean.setCodban(codActividad);
	    	preswbean.setCajero(codgru);
			if(preswbean != null) {
				preswbean.setSucur(sucur);	
				lista = new ArrayList<Presw25DTO>();
				lista = (List<Presw25DTO>) preswbean.consulta_presw25();
				if(lista != null && lista.size() > 0){
					for (Presw25DTO ss : lista){
						ss.setTippro("RPQ");
						ss.setCanman(0);
						ss.setTotpre(0);
						ss.setIndmod("D");
						resultadoDetalleItemReq.add(ss);
					}
						
				}
			}
		} 
		moduloPresupuesto.eliminaReqPlan(accionweb.getReq(), listaPresupItemreq, resultadoDetalleItemReq);
		listaPresupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
		
		Long en = new Long(0);
    	Long fe = new Long(0);
    	Long ma = new Long(0);
    	Long ab = new Long(0);
    	Long my = new Long(0);
    	Long jn = new Long(0);
    	Long jl = new Long(0);
    	Long ag = new Long(0);
    	Long se = new Long(0);
    	Long oc = new Long(0);
    	Long no = new Long(0);
    	Long di = new Long(0);
		for (Presw25DTO ss : listaPresupItemreq){
    		Presw25DTO presw25 = ss;
        	  
    		if(presw25.getNumreq() == codActividad && presw25.getItedoc() == itedoc && presw25.getAnopre() == a�o ){
    			  nomItem = ss.getItedoc() + " - " + ss.getDesite();
    			  nomActividad = ss.getNumreq() + " - " + ss.getMotiv1();
	    	switch (ss.getMespre()){	  			
	    	case 1: 	en = en + presw25.getTotpre();
	    	break;
	    	case 2: 	fe = fe + presw25.getTotpre();
	    	break;
	    	case 3:     ma = ma + presw25.getTotpre();
	    	break;
	    	case 4: 	ab = ab + presw25.getTotpre();
	    	break;
	    	case 5: 	my = my + presw25.getTotpre();
	    	break;
	    	case 6: 	jn = jn + presw25.getTotpre();
	    	break;
	    	case 7: 	jl = jl + presw25.getTotpre();
	    	break;
	    	case 8: 	ag = ag + presw25.getTotpre();
	    	break;
	    	case 9: 	se = se + presw25.getTotpre();
	    	break;
	    	case 10: 	oc = oc + presw25.getTotpre();
	    	break;
	    	case 11: 	no = no + presw25.getTotpre();
	    	break;
	    	case 12: 	di = di + presw25.getTotpre();
	    	break;
	    	}
    		}
    	}
		
		    accionweb.agregarObjeto("enero",en);
		    accionweb.agregarObjeto("febrero",fe);
		    accionweb.agregarObjeto("marzo", ma);
		    accionweb.agregarObjeto("abril", ab);
		    accionweb.agregarObjeto("mayo", my);
		    accionweb.agregarObjeto("junio", jn);
		    accionweb.agregarObjeto("julio", jl);
		    accionweb.agregarObjeto("agosto", ag);
		    accionweb.agregarObjeto("septiembre", se );
		    accionweb.agregarObjeto("octubre", oc);
		    accionweb.agregarObjeto("noviembre", no);
		    accionweb.agregarObjeto("diciembre", di);
		    accionweb.agregarObjeto("total", en + fe + ma + ab + my + jn + jl + ag + se + oc + no + di);
			//accionweb.agregarObjeto("cuentasPresupuestarias", lista);
			accionweb.agregarObjeto("actividadSeleccionada", codActividad);
			accionweb.agregarObjeto("presupItemreq", listaPresupItemreq);
			//accionweb.agregarObjeto("resultadoPresup", listarResultadoPresup);
			accionweb.agregarObjeto("anno", a�o);	
			accionweb.agregarObjeto("itedoc", itedoc);	
			accionweb.agregarObjeto("nomActividad", nomActividad);
			accionweb.agregarObjeto("nomItem", nomItem);
			accionweb.agregarObjeto("control", control);
		this.cargaPresupPlan(accionweb);

	}
	public void cargaDetalleItemReqPlan(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int codActividad = Util.validaParametro(accionweb.getParameter("codActividad"), 0);
		int itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		int sucur = Util.validaParametro(accionweb.getParameter("sucur"), 0);
		Long totalMensual = Util.validaParametro(accionweb.getParameter("total"),new Long(0));
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		String codgru = Util.validaParametro(accionweb.getParameter("codgru"),"");
		Long total = new Long(0);
		Long totalRq = new Long(0);
		/*falta mandar sucur  0 cuando es nueva*/
	//	Collection<Presw18DTO> listaUnidad = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		List<Presw25DTO> resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
		boolean existe = false;
		if(resultadoDetalleItemReq != null && resultadoDetalleItemReq.size() > 0 && sucur > 0){
		for (Presw25DTO ss : resultadoDetalleItemReq){
			if(ss.getAnopre() == a�o && /*ss.getCoduni() == unidad && */ss.getItedoc() == itedoc && ss.getMespre() == mes && ss.getNumreq() == sucur){
				listaPresw25.add(ss);
				existe = true;
			}			
		}
		}
		if(!existe){
			PreswBean preswbean = new PreswBean("RQA", 0,itedoc,a�o,mes, rutUsuario);
			preswbean.setDigide(dv);
			preswbean.setCodban(codActividad);
			preswbean.setCajero(codgru);
			
			if(preswbean != null) {
				preswbean.setSucur(sucur);	
				listaPresw25 = new ArrayList<Presw25DTO>();
				listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
			}
			}
	    if(resultadoDetalleItemReq == null)
	    	resultadoDetalleItemReq = new ArrayList<Presw25DTO>();
		moduloPresupuesto.agregaDetalleItemReqSesionPlan(accionweb.getReq(), listaPresw25, resultadoDetalleItemReq);
		
		listaPresw25 = (List<Presw25DTO>) accionweb.getSesion().getAttribute("listaPresw25");
		if(listaPresw25 != null)
			accionweb.getSesion().removeAttribute("listaPresw25");
		resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
	  	String nomActividad = Util.validaParametro(accionweb.getParameter("nomActividad"),"");
	  	String nomItem = "";
	  	String nomMes = "";
	  	String area1 = "";
	  	String area2 = "";
	  	String comen1 = "";
	  	String comen2 = "";
	  	String comen3 = "";
	  	String comen4 = "";
	  	String comen5 = "";
	  	String comen6 = "";
	  	String motiv1 = "";
		String motiv2 = "";
		String motiv3 = "";
		String motiv4 = "";
		String motiv5 = "";
		String motiv6 = "";
		int canman = 0;
		int valpr1 = 0;
		int numReq = 0;
		List<Presw25DTO> presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");		
	
	 	for (Presw25DTO ss : listaPresw25){
	 		//System.out.println(ss.getNumreq());
	 		// ver como sacarlo nomActividad = ss.getNumreq() + " - " + ss.getMotiv1();
		    nomItem = ss.getItedoc() + " - " + ss.getDesite();
		    numReq = ss.getNumreq();
			area1 = ss.getArea1();
		  	area2 = ss.getArea2();
		  	comen1 = ss.getComen1();
		  	comen2 = ss.getComen2();
		  	comen3 = ss.getComen3();
		  	comen4 = ss.getComen4();
		  	comen5 = ss.getComen5();
		  	comen6 = ss.getComen6();
		  	motiv1 = ss.getMotiv1();
			motiv2 = ss.getMotiv2();
			motiv3 = ss.getMotiv3();
			motiv4 = ss.getMotiv4();
			motiv5 = ss.getMotiv5();
			motiv6 = ss.getMotiv6();
			valpr1 = ss.getValpr1();
			canman = ss.getCanman();
		//	totalRq = totalRq + (ss.getCanman() * ss.getValuni());
		    switch (ss.getMespre()){
		    case 1: nomMes = "ENERO";
		            break;
		    
		    case 2:nomMes = "FEBRERO";
		    break;
		    case 3:nomMes = "MARZO";
		    break;
		    case 4:nomMes = "ABRIL";
		    break;
		    case 5:nomMes = "MAYO";
		    break;
		    case 6:nomMes = "JUNIO";
		    break;
		    case 7:nomMes = "JULIO";
		    break;
		    case 8:nomMes = "AGOSTO";
		    break;
		    case 9:nomMes = "SEPTIEMBRE";
		    break;
		    case 10:nomMes = "OCTUBRE";
		    break;
		    case 11:nomMes = "NOVIEMBRE";
		    break;
		    case 12:nomMes = "DICIEMBRE";
		    break;
		    }
		
	    	
	   
	 	}
	
	 	for (Presw25DTO rs : presupItemreq){
  		  
    		if(/*rs.getCoduni() == unidad && */rs.getItedoc() == itedoc && rs.getAnopre() == a�o  && rs.getMespre() == mes){
    		  if(rs.getNumreq() == numReq )	
    			  totalRq = totalRq + rs.getTotpre();
	    	
    		  total = total + rs.getTotpre();
    		}
    	}
		 
	 	// accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidad);
		 accionweb.agregarObjeto("nomActividad", nomActividad);
		 accionweb.agregarObjeto("nomItem", nomItem);    
		 accionweb.agregarObjeto("nomMes", nomMes);  
		 accionweb.agregarObjeto("numReq", numReq); 
	 	 accionweb.agregarObjeto("resultadoDetalleItemReq", resultadoDetalleItemReq);	
	 	 accionweb.agregarObjeto("area1",area1);
	 	 accionweb.agregarObjeto("area2",area2);
	 	 accionweb.agregarObjeto("comen1",comen1);
	 	 accionweb.agregarObjeto("comen2",comen2);
	 	 accionweb.agregarObjeto("comen3",comen3);
	 	 accionweb.agregarObjeto("comen4",comen4);
	 	 accionweb.agregarObjeto("comen5",comen5);
	 	 accionweb.agregarObjeto("comen6",comen6);
	 	 accionweb.agregarObjeto("motiv1",motiv1);
	 	 accionweb.agregarObjeto("motiv2",motiv2);
	 	 accionweb.agregarObjeto("motiv3",motiv3);
	 	 accionweb.agregarObjeto("motiv4",motiv4);
	 	 accionweb.agregarObjeto("motiv5",motiv5);
	 	 accionweb.agregarObjeto("motiv6",motiv6);
	 	 accionweb.agregarObjeto("valpr1",valpr1);
	 	 accionweb.agregarObjeto("totalReq",totalRq);
	 	// total = totalMensual + total;
			
	 	 accionweb.agregarObjeto("total",(total - totalRq));
		 accionweb.agregarObjeto("anno", a�o);		 
	 	 accionweb.agregarObjeto("codActividad",codActividad);
		 accionweb.agregarObjeto("nomActividad",nomActividad);
	 	 accionweb.agregarObjeto("numreq",numReq);
	 	 accionweb.agregarObjeto("item",itedoc);
	 	 accionweb.agregarObjeto("mes",mes);
	 	 accionweb.agregarObjeto("totalMes", total);
	 	 accionweb.agregarObjeto("control", control);
	 	 accionweb.agregarObjeto("codgru", codgru);
	}
	 synchronized public void actualizarItemReqPlan(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int codActividad = Util.validaParametro(accionweb.getParameter("codActividad"), 0);
		int itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		int numreq = Util.validaParametro(accionweb.getParameter("sucur"), 0);
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		
		

		List<Presw25DTO> listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		
		List<Presw25DTO> resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");	
		List<Presw25DTO> presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		moduloPresupuesto.agregaItemReqDetPlan(accionweb.getReq(), resultadoDetalleItemReq, presupItemreq, listarResultadoPresup);
		presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
		Long totalMes = (Long) accionweb.getSesion().getAttribute("totalMes");
		accionweb.agregarObjeto("totalMes", totalMes);
		//cargaPresupRequerimiento(accionweb);
		accionweb.agregarObjeto("control", control);
		cargaDetalleItemReqPlan(accionweb);
		
	}
	
	public void volverPresupRequerimientoPlan(AccionWeb accionweb) throws Exception {
		cargaPresupRequerimientoPlan(accionweb);
		List<Presw25DTO> presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		List<Presw25DTO> resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		
		moduloPresupuesto.actualizaItemReqDetPlan(accionweb.getReq(), resultadoDetalleItemReq, presupItemreq);
		presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
		accionweb.agregarObjeto("control", control);
		cargaPresupRequerimientoPlan(accionweb);
	}
	
	 synchronized public void RegistrarMatrizPlan(AccionWeb accionweb) throws Exception {
	  
		List<Presw25DTO> listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		List<Presw25DTO> resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
		List<Presw25DTO> presupItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemReq");
		List<Presw25DTO> planDesarrollo = (List<Presw25DTO>) accionweb.getSesion().getAttribute("planDesarrollo");
		
		int a�o =Integer.parseInt( Util.validaParametro(accionweb.getSesion().getAttribute("anno")+"","0"));
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int codActividad = Util.validaParametro(accionweb.getParameter("codActividad"), 0);
		String nomActividad = Util.validaParametro(accionweb.getParameter("nomActividad"),"");
		int mes = Util.validaParametro(accionweb.getParameter("mes"), 0);
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);	
		String codgru = Util.validaParametro(accionweb.getParameter("codgru"),"");
		
	
	
		moduloPresupuesto.agregaRegistraMatrizPlan(accionweb.getReq(), listarResultadoPresup, planDesarrollo);
		listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		
		try {
			
					boolean graba = moduloPresupuesto.saveMatrizPlan(listarResultadoPresup,codActividad, anno, rutUsuario, dv);
				/*	for(Presw25DTO ss : listarResultadoPresup){
						System.out.println(ss.getCoduni()+"tipo: *****"+ ss.getIndexi()+"  indmod: "+ ss.getIndmod());
					}*/
					if(graba){
						if(resultadoDetalleItemReq != null && resultadoDetalleItemReq.size()>0)
				/*	for(Presw25DTO ss : resultadoDetalleItemReq){
						System.out.println("tipo: *****"+ ss.getTippro()+" val: "+ ss.getCanman()+"  total: "+ ss.getTotpre() + "  ind: "+ ss.getIndmod()+" pres1: "+ss.getPres01()+"  pres2: "+ss.getPres02()+"pres3; "+ss.getPres03());
					}*/
					graba = moduloPresupuesto.saveREQPlan( resultadoDetalleItemReq, codActividad, anno, rutUsuario, mes, dv, codgru);

				if(graba) {
					accionweb.agregarObjeto("mensaje", "Registr� exitosamente Presupuestaci�n");
					if(accionweb.getSesion().getAttribute("presupItemreq") != null)
						accionweb.getSesion().removeAttribute("presupItemreq");
					if(accionweb.getSesion().getAttribute("resultadoPresup") != null)
						accionweb.getSesion().removeAttribute("resultadoPresup");
					if(accionweb.getSesion().getAttribute("Original") != null)
						accionweb.getSesion().removeAttribute("Original");
					if(accionweb.getSesion().getAttribute("resultadoDetalleItemReq") != null)
						accionweb.getSesion().removeAttribute("resultadoDetalleItemReq");
					if(accionweb.getSesion().getAttribute("resultadoPresupNomina") != null)
						accionweb.getSesion().removeAttribute("resultadoPresupNomina");
					if(accionweb.getSesion().getAttribute("resultadoPresupVacante") != null)
						accionweb.getSesion().removeAttribute("resultadoPresupVacante");
					if(accionweb.getSesion().getAttribute("resultadoPresupRemuneracion") != null)
						accionweb.getSesion().removeAttribute("resultadoPresupRemuneracion");
					if(accionweb.getSesion().getAttribute("resultadoPresupIngresos") != null)
						accionweb.getSesion().removeAttribute("resultadoPresupIngresos");
					

				} else
					accionweb.agregarObjeto("mensaje", "No registra Presupuestaci�n");
			} else
				accionweb.agregarObjeto("mensaje", "No registra Presupuestaci�n");
					
						
			cargaPresupPlan(accionweb);	
			
			cargaResultadoPresupPlan(accionweb);
			List<Presw25DTO> listaPRESW25 = null;
			PreswBean preswbean = new PreswBean("CPR", 0,0,a�o,0, rutUsuario);
			preswbean.setDigide(dv);
			if(preswbean != null) {
			listaPRESW25 = (List<Presw25DTO>) preswbean.consulta_presw25();
			if(listaPRESW25 != null && listaPRESW25.size() > 0){
				accionweb.getSesion().setAttribute("resultadoCPR", listaPRESW25);
				accionweb.agregarObjeto("unidadesPresup", listaPRESW25);
			}
			}
			
			accionweb.agregarObjeto("actividadSeleccionada", codActividad);
			accionweb.agregarObjeto("nomActividad", nomActividad);
			accionweb.agregarObjeto("codActividad", codActividad);

		}catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No registra Presupuestaci�n");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		
		
	}
	public void corrienteExcelPlan(AccionWeb accionweb) throws Exception {
		//int itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		String itedoc = Util.validaParametro(accionweb.getParameter("itedoc"), "");
		
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int codActividad = Util.validaParametro(accionweb.getParameter("codActividad"), 0);
		String nomActividad = Util.validaParametro(accionweb.getParameter("nomActividad"),"");
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		String comen2 = Util.validaParametro(accionweb.getParameter("comen2"),"");
		String codgru = Util.validaParametro(accionweb.getParameter("codgru"),"");

		Vector vec_d = new Vector();
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		
		Long total = new Long(0);
		Long totalTotal = new Long(0);
		Long enero = new Long(0);
		Long febrero = new Long(0);
		Long marzo = new Long(0);
		Long abril = new Long(0);
		Long mayo = new Long(0);
		Long junio = new Long(0);
		Long julio = new Long(0);
		Long agosto = new Long(0);
		Long septiembre = new Long(0);
		Long octubre = new Long(0);
		Long noviembre = new Long(0);
		Long diciembre = new Long(0);
		PreswBean preswbean = new PreswBean("PRD", 0,0,a�o,0, rutUsuario);
		preswbean.setDigide(dv);
		preswbean.setMespar(codActividad);
		preswbean.setCajero(codgru);
		
		Vector vec_datos = new Vector();

		if(preswbean != null) {				
			listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
		}
		
		long totalAgrup = 0;
		Vector agrup = null;
		Vector agrup1 = new Vector();
		Vector agrup2 = new Vector();
		Vector agrup3 = new Vector();
		Vector agrup4 = new Vector();
		long total_ENE = 0;	
		long total_FEB = 0;	
		long total_MAR = 0;
		long total_ABR = 0;	
		long total_MAY = 0;	
		long total_JUN = 0;	
		long total_JUL = 0;	
		long total_AGO = 0;	
		long total_SEP = 0;	
		long total_OCT = 0;	
		long total_NOV = 0;	
		long total_DIC = 0;	
		long agrup_ENE = 0;	
		long agrup_FEB = 0;
		long agrup_MAR = 0;	
		long agrup_ABR = 0;	
		long agrup_MAY = 0;	
		long agrup_JUN = 0;	
		long agrup_JUL = 0;	
		long agrup_AGO = 0;	
		long agrup_SEP = 0;	
		long agrup_OCT = 0;	
		long agrup_NOV = 0;	
		long agrup_DIC = 0;
		for (Presw25DTO ss : listaPresw25){
			total = ss.getPres01() + ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() + ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() + ss.getPres10() + ss.getPres11() + ss.getPres12();
			total_ENE = ss.getPres01();
			total_FEB = ss.getPres02();
			total_MAR = ss.getPres03();
			total_ABR = ss.getPres04();
			total_MAY = ss.getPres05();
			total_JUN = ss.getPres06();
			total_JUL = ss.getPres07();
			total_AGO = ss.getPres08();
			total_SEP = ss.getPres09();
			total_OCT = ss.getPres10();
			total_NOV = ss.getPres11();
			total_DIC = ss.getPres12();
	        if(!ss.getCodman().trim().equals(ss.getMotiv3().trim())){
		        totalAgrup = totalAgrup + total;
		        agrup_ENE = agrup_ENE + total_ENE;
			    agrup_FEB = agrup_FEB + total_FEB;
				agrup_MAR = agrup_MAR + total_MAR;
				agrup_ABR = agrup_ABR + total_ABR;
				agrup_MAY = agrup_MAY + total_MAY;
				agrup_JUN = agrup_JUN + total_JUN;
				agrup_JUL = agrup_JUL + total_JUL;
				agrup_AGO = agrup_AGO + total_AGO;
				agrup_SEP = agrup_SEP + total_SEP;
				agrup_OCT = agrup_OCT + total_OCT;
				agrup_NOV = agrup_NOV + total_NOV;
				agrup_DIC = agrup_DIC + total_DIC;
	        }  
		    else {
		    	 agrup = new Vector();	 
				 agrup.addElement(totalAgrup);
				 agrup.addElement(agrup_ENE);
				 agrup.addElement(agrup_FEB);
				 agrup.addElement(agrup_MAR);
				 agrup.addElement(agrup_ABR);
				 agrup.addElement(agrup_MAY);
				 agrup.addElement(agrup_JUN);
				 agrup.addElement(agrup_JUL);
				 agrup.addElement(agrup_AGO);
				 agrup.addElement(agrup_SEP);
				 agrup.addElement(agrup_OCT);
				 agrup.addElement(agrup_NOV);
				 agrup.addElement(agrup_DIC);
		    	 if(ss.getCodman().trim().equals("2")) agrup1 = agrup;
		    	 else if(ss.getCodman().trim().equals("3")) agrup2 = agrup;
				 else if(ss.getCodman().trim().equals("4")) agrup3 = agrup;
		    	 
		    			    	 
		    	 totalAgrup = 0;  
				 agrup_ENE = 0;	
				 agrup_FEB = 0;	
				 agrup_MAR = 0;	
				 agrup_ABR = 0;	
				 agrup_MAY = 0;	
				 agrup_JUN = 0;	
				 agrup_JUL = 0;	
				 agrup_AGO = 0;	
				 agrup_SEP = 0;	
				 agrup_OCT = 0;	
				 agrup_NOV = 0;	
				 agrup_DIC = 0;
				 
				 
		      }
		  }  
		 agrup = new Vector();	 
		 agrup.addElement(totalAgrup);
		 agrup.addElement(agrup_ENE);
		 agrup.addElement(agrup_FEB);
		 agrup.addElement(agrup_MAR);
		 agrup.addElement(agrup_ABR);
		 agrup.addElement(agrup_MAY);
		 agrup.addElement(agrup_JUN);
		 agrup.addElement(agrup_JUL);
		 agrup.addElement(agrup_AGO);
		 agrup.addElement(agrup_SEP);
		 agrup.addElement(agrup_OCT);
		 agrup.addElement(agrup_NOV);
		 agrup.addElement(agrup_DIC);
		 agrup4 = agrup;
		
		// System.out.println("agrup4 " + agrup4);
				

		 for (Presw25DTO ss : listaPresw25){		
				total = ss.getPres01() + ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() + ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() + ss.getPres10() + ss.getPres11() + ss.getPres12();
			    if(total > 0 || ss.getCodman().trim().equals(ss.getMotiv3().trim())){
				vec_d = new Vector();
				//vec_d.add(ss.getItedoc());
				if(!ss.getCodman().trim().equals(ss.getMotiv3().trim())) 
					//vec_datos.add(String.valueOf(ss.getItedoc()));
					      vec_d.add(ss.getCodman().trim());
					else vec_d.add("");
					vec_d.add(ss.getDesite());
					
					agrup = new Vector();
				    if(ss.getCodman().trim().equals(ss.getMotiv3().trim())){
					   if(ss.getCodman().trim().equals("1")) agrup = agrup1;
					   else if(ss.getCodman().trim().equals("2")) agrup = agrup2;
		               else if(ss.getCodman().trim().equals("3")) agrup = agrup3;
		                else if(ss.getCodman().trim().equals("4")) agrup = agrup4;
					   
					    vec_d.add(agrup.get(0));
						vec_d.add(agrup.get(1));
						vec_d.add(agrup.get(2));
						vec_d.add(agrup.get(3));
						vec_d.add(agrup.get(4));
						vec_d.add(agrup.get(5));
						vec_d.add(agrup.get(6));
						vec_d.add(agrup.get(7));
						vec_d.add(agrup.get(8));
						vec_d.add(agrup.get(9));
						vec_d.add(agrup.get(10));
						vec_d.add(agrup.get(11));
						vec_d.add(agrup.get(12));
						vec_d.add("");
				    }    
		          	else {
			          	vec_d.add(total);
						vec_d.add(ss.getPres01());
						vec_d.add(ss.getPres02());
						vec_d.add(ss.getPres03());
						vec_d.add(ss.getPres04());
						vec_d.add(ss.getPres05());
						vec_d.add(ss.getPres06());
						vec_d.add(ss.getPres07());
						vec_d.add(ss.getPres08());
						vec_d.add(ss.getPres09());
						vec_d.add(ss.getPres10());
						vec_d.add(ss.getPres11());
						vec_d.add(ss.getPres12());
						vec_d.add(ss.getComen1().toString().trim()+ss.getComen2().toString().trim()+ss.getComen3().toString().trim()+ss.getComen4().toString().trim()+ss.getComen5().toString().trim()+ss.getComen6().toString().trim());
			        }
				
				vec_datos.add(vec_d);
			
				enero += ss.getPres01();
			febrero += ss.getPres02();
			marzo += ss.getPres03();
			abril += ss.getPres04();
			mayo += ss.getPres05();
			junio += ss.getPres06();
			julio += ss.getPres07();
			agosto += ss.getPres08();
			septiembre += ss.getPres09();
			octubre += ss.getPres10();
			noviembre += ss.getPres11();
			diciembre += ss.getPres12();
			totalTotal += total;
			}
		}
		vec_d = new Vector();
		vec_d.add("");
		vec_d.add("Total");
		vec_d.add(totalTotal);
		vec_d.add(enero);
		vec_d.add(febrero);
		vec_d.add(marzo);
		vec_d.add(abril);
		vec_d.add(mayo);
		vec_d.add(junio);
		vec_d.add(julio);
		vec_d.add(agosto);
		vec_d.add(septiembre);
		vec_d.add(octubre);
		vec_d.add(noviembre);
		vec_d.add(diciembre);
		vec_d.add("");
		if(totalTotal > 0)
			vec_datos.add(vec_d);

		/*agrega requerimiento*/
		/*esto era para el item 41... pero ahora no se toma en cuenta*/
		/*
	    String nomItem = "";
	    String nomMes = "";
	    itedoc = 41;
		Vector vecTotales = new Vector();
		List<Presw25DTO> presupItemreq = new ArrayList<Presw25DTO>();
		List<Presw25DTO> presupItemreqDet = new ArrayList<Presw25DTO>();
		
		preswbean = new PreswBean("MNA", 0,itedoc,a�o,0, rutUsuario);
		preswbean.setDigide(dv);	
		preswbean.setMespar(codActividad);
		preswbean.setCajero(codgru);
		if(preswbean != null) {
			    presupItemreq = (List<Presw25DTO>) preswbean.consulta_presw25();
		}
		
		Long en = new Long(0);
    	Long fe = new Long(0);
    	Long ma = new Long(0);
    	Long ab = new Long(0);
    	Long my = new Long(0);
    	Long jn = new Long(0);
    	Long jl = new Long(0);
    	Long ag = new Long(0);
    	Long se = new Long(0);
    	Long oc = new Long(0);
    	Long no = new Long(0);
    	Long di = new Long(0);
    	Vector listaReqResumenExcel = new Vector();	
    	Vector listaReqDetResumenExcel = new Vector();
         
    	if(presupItemreq != null && presupItemreq.size() > 0){
        Vector meses = new Vector();
        int numMes = 1;
    //    while (numMes < 13) {
    	for (Presw25DTO ss : presupItemreq){
    		Presw25DTO presw25 = ss;
        	  
    		if(		presw25.getItedoc() == itedoc && 
    				presw25.getAnopre() == a�o 
    				//&& numMes == ss.getMespre()
    				 ){
    			  nomItem = ss.getItedoc() + " - " + ss.getDesite();
	        //	  nomUnidad = ss.getCoduni() + " - " + ss.getDesuni();
	    	switch (ss.getMespre()){	  			
	    	case 1: 	{ en = en + presw25.getTotpre();
	    	              nomMes = "Enero"; 	    	
	    	              break;
	    	}
	    	case 2: 	{fe = fe + presw25.getTotpre();
	    				nomMes = "Febrero"; 
	    				break;
	    	}
	    	
	    	case 3:    { ma = ma + presw25.getTotpre();
	    				nomMes = "Marzo";
	    				break;
	    	}
	    	
	    	case 4:    {ab = ab + presw25.getTotpre();
	    				nomMes = "Abril"; 
	    				break;
	    	}
	    	
	    	case 5: 	{my = my + presw25.getTotpre();
	    				nomMes = "Mayo"; 
	    				break;
	    	}
	    	
	    	case 6: 	{jn = jn + presw25.getTotpre();
	    				nomMes = "Junio";
	    				break;
	    	}
	    	
	    	case 7: 	{jl = jl + presw25.getTotpre();
	    				nomMes = "Julio"; 
	    				break;
	    	}
	    	
	    	case 8: 	{ag = ag + presw25.getTotpre();
	    				nomMes = "Agosto"; 
	    				break;
	    	}
	    	
	    	case 9: 	{se = se + presw25.getTotpre();
	    				nomMes = "Septiembre"; 
	    				break;
	    	}
	    	
	    	case 10: 	{oc = oc + presw25.getTotpre();
	    				nomMes = "Octubre"; 
	    				break;
	    	}
	    	
	    	case 11: 	{no = no + presw25.getTotpre();
	    				nomMes = "Noviembre";
	    				break;
	    	}
	    	
	    	case 12: 	{di = di + presw25.getTotpre();
	    				nomMes = "Diciembre"; 
	    				break;
	    	}
	    	
	    	}
	    	
	    		
	    	    vec_d = new Vector();
	    	    vec_d.addElement(ss.getMespre());
	    	    vec_d.addElement(nomMes);
	    	    vec_d.addElement(ss.getComen1().trim());
	    	    vec_d.addElement(ss.getComen2().trim());
	    	    vec_d.addElement(ss.getTotpre());
	    	    switch (ss.getMespre()){
	    	    case 1: vec_d.addElement(en);
	    	    break;
	    	    case 2 : vec_d.addElement(fe);
	    	    break;
	    	    case 3 : vec_d.addElement(ma);
	    	    break;
	    	    case 4 : vec_d.addElement(ab);
	    	    break;
	    	    case 5 : vec_d.addElement(my);
	    	    break;
	    	    case 6 : vec_d.addElement(jn);
	    	    break;
	    	    case 7 : vec_d.addElement(jl);
	    	    break;
	    	    case 8 : vec_d.addElement(ag);
	    	    break;
	    	    case 9 : vec_d.addElement(se);
	    	    break;
	    	    case 10 : vec_d.addElement(oc);
	    	    break;
	    	    case 11 : vec_d.addElement(no);
	    	    break;
	    	    case 12 : vec_d.addElement(di);
	    	    break;
	    	    }
	    	    if(ss.getTotpre() > 0)
	    	       listaReqResumenExcel.addElement(vec_d);
    	
    		preswbean = new PreswBean("RQA", 0,itedoc,a�o,ss.getMespre(), rutUsuario);
   			preswbean.setDigide(dv);
   			preswbean.setCodban(codActividad);
   			preswbean.setCajero(codgru);
			if(preswbean != null) {
				preswbean.setSucur(ss.getNumreq());	
				presupItemreqDet = new ArrayList<Presw25DTO>();
				presupItemreqDet = (List<Presw25DTO>) preswbean.consulta_presw25();
			}
    	}
			
    //	}
 
    	//numMes++;
    //    }
    //	}
    	nomMes = "";
    	if(presupItemreqDet.size() > 0){
    	for(Presw25DTO pi: presupItemreqDet){
        	switch (pi.getMespre()){	  			
	    	case 1: 	nomMes = "Enero"; 	    	
	    	              break;
	    	case 2: 	nomMes = "Febrero"; 
	    				break;	    	
	    	case 3:   	nomMes = "Marzo";
	    				break;	    	
	    	case 4:     nomMes = "Abril"; 
	    				break;	    	
	    	case 5: 	nomMes = "Mayo"; 
	    				break;	    	
	    	case 6: 	nomMes = "Junio";
	    				break;	    	
	    	case 7: 	nomMes = "Julio"; 
	    				break;	    	
	    	case 8: 	nomMes = "Agosto"; 
	    				break;	    	
	    	case 9: 	nomMes = "Septiembre"; 
	    				break;	    	
	    	case 10: 	nomMes = "Octubre"; 
	    				break;	    	
	    	case 11: 	nomMes = "Noviembre";
	    				break;	    	
	    	case 12: 	nomMes = "Diciembre"; 
	    				break;	    	
	    	}
    		 vec_d = new Vector();
    		 vec_d.addElement(nomActividad); // 0 UNIDAD
    		 vec_d.addElement(pi.getItedoc()+" - "+ pi.getDesite()); // 1 - ITEM
    		 vec_d.addElement(nomMes.concat(" - N� " + pi.getNumreq()+""));// 3 - requerimiento    	    	
     		 vec_d.addElement(pi.getArea1().trim() + pi.getArea2().trim());// 2 - AREA
    		 vec_d.addElement(pi.getComen1().trim()+pi.getComen2().trim()+pi.getComen3().trim()+pi.getComen4().trim()+pi.getComen5().trim()+pi.getComen6().trim()); // 4 - DESCRIPCION
    		 vec_d.addElement(pi.getMotiv1().trim()+pi.getMotiv2().trim()+pi.getMotiv3().trim()+pi.getMotiv4().trim()+pi.getMotiv5().trim()+pi.getMotiv6().trim()); // 5 - MOTIVO
			 vec_d.addElement(pi.getCodman()); // 6 - ITEM
			 vec_d.addElement(pi.getTipcon()); // 7 - DETALLE
    		 if(pi.getCodman().trim().equals("COTIZACION") && pi.getPres03() > 0){
    			 String fecha = pi.getPres02()+"";
    			 vec_d.addElement(pi.getPres01()); // 9 - cotizacion
        		 vec_d.addElement(pi.getPres03()); // 10 - monto
        		 vec_d.addElement(fecha.substring(7)+"/"+fecha.substring(4,6)+"/"+fecha.substring(0,4)); // 10 - fecha
    		 } else {
    			 vec_d.addElement(pi.getCodigo()); // 8 - UNIDAD
    	     	 vec_d.addElement(pi.getValuni()); // 9 - COSTO
        		 vec_d.addElement(pi.getCanman()); // 10 - CANTIDAD       
    		 }
             vec_d.addElement(pi.getNumreq()); // 11 - NUMREQ
    		 vec_d.addElement(pi.getMespre()); // 12 - MES
    		 if(pi.getCanman() > 0 	|| pi.getPres03() > 0)
    		     listaReqDetResumenExcel.addElement(vec_d);
    	}
    	}
    	}
       	}
       	Long totalAnual = en + fe + ma + ab + my + jn + jl + ag + se + oc + no + di;
	    vec_d = new Vector();
	    vec_d.addElement(13);
	    vec_d.addElement("Total Anual");
	    vec_d.addElement("");
	    vec_d.addElement("");
	    vec_d.addElement("");
	    vec_d.addElement(totalAnual);
	    if(totalAnual > 0)
	    			listaReqResumenExcel.addElement(vec_d);
	    */
		/*fin requerimiento*/
		Vector<String> listaReqResumenExcel = new Vector<String>();
		Vector<String> listaReqDetResumenExcel = new Vector<String>();
    	moduloPresupuesto.agregaCorrienteExcel(accionweb.getReq(), vec_datos, listaReqResumenExcel, listaReqDetResumenExcel);
	
		
	}

	 synchronized public void actualizaMatrizPlan(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int a�o = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int codActividad = Util.validaParametro(accionweb.getParameter("codActividad"), 0);
		String nomActividad = Util.validaParametro(accionweb.getParameter("nomActividad"),"");
	    int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		List<Presw25DTO> listarResultadoPresup = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
	   	List<Presw25DTO> resultadoDetalleItemReq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoDetalleItemReq");
	   	List<Presw25DTO> presupItemreq = (List<Presw25DTO>) accionweb.getSesion().getAttribute("presupItemreq");
		int volver = Util.validaParametro(accionweb.getParameter("volver"), 0);
		//int item = Util.validaParametro(accionweb.getParameter("itedoc"), 0);
		String item = Util.validaParametro(accionweb.getParameter("itedoc"), "");
		
		String codgru = Util.validaParametro(accionweb.getParameter("codgru"),"");
		List<Presw25DTO> listarResultado = new ArrayList<Presw25DTO>();
			
	 	Long totalItemReq = new Long(0);
     	Long ene = new Long(0);
     	Long feb = new Long(0);
    	Long mar = new Long(0);
     	Long abr = new Long(0);
     	Long may = new Long(0);
     	Long jun = new Long(0);
     	Long jul = new Long(0);
     	Long ago = new Long(0);
     	Long sep = new Long(0);
     	Long oct = new Long(0);
     	Long nov = new Long(0);
     	Long dic = new Long(0);
		for (Presw25DTO ss : presupItemreq){ // agregar todo lo que est� en mantencion al presup
			//if(ss.getItedoc() == item  /*&& ss.getMespre() == mes*/) {
			if(ss.getMotiv2().trim().equals(item.trim())  /*&& ss.getMespre() == mes*/) {
				
				switch (ss.getMespre()){
				case 1: ene = ene + ss.getTotpre();
				break;
				case 2: feb = feb + ss.getTotpre();
				break;
				case 3: mar = mar + ss.getTotpre();
				break;
				case 4: abr = abr + ss.getTotpre();
				break;
				case 5: may = may + ss.getTotpre();
				break;
				case 6: jun = jun + ss.getTotpre();
				break;
				case 7: jul = jul + ss.getTotpre();
				break;
				case 8: ago = ago + ss.getTotpre();
				break;
				case 9: sep = sep + ss.getTotpre();
				break;
				case 10: oct = oct + ss.getTotpre();
				break;
				case 11: nov = nov + ss.getTotpre();
				break;
				case 12: dic = dic + ss.getTotpre();
				break;
				}
				totalItemReq = totalItemReq + ss.getTotpre();
				if(ss.getTotpre() == 0) ss.setIndmod("D");
			}
		}
	  	if(listarResultadoPresup.size() > 0){
	  		int i = -1;
		for (Presw25DTO ss : listarResultadoPresup){
			i++;
			//if(ss.getNumreq() == codActividad && ss.getItedoc() == item) {
			if(ss.getNumreq() == codActividad && ss.getMotiv2().trim().equals(item)) {
			ss.setPres01(ene);
			ss.setPres02(feb);
			ss.setPres03(mar);
			ss.setPres04(abr);
			ss.setPres05(may);
			ss.setPres06(jun);
			ss.setPres07(jul);
			ss.setPres08(ago);
			ss.setPres09(sep);
			ss.setPres10(oct);
			ss.setPres11(nov);
			ss.setPres12(dic);
			ss.setTotpre(totalItemReq);			
			}
			listarResultado.add(ss);
		//	System.out.println(ss.getItedoc()+ "**" + ss.getComen1()+" ** "+ss.getComen2()+ " *** "+" ** "+ss.getComen3()+" ** "+ss.getComen4()+" ** "+ss.getComen5()+" ** "+ss.getComen6());
		}
	  //  moduloPresupuesto.agregaResultadoMatriz(accionweb.getReq(), listarResultado);
		
	
		moduloPresupuesto.agregaResulPresup(accionweb.getReq(), listarResultado);
		//	accionweb.getSesion().setAttribute("resultadoPresup", resultadoPresupImport);
		listarResultado  = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresup");
		
	    cargaPresupPlan(accionweb);
	   	accionweb.agregarObjeto("resultadoPresup", listarResultado);
		accionweb.agregarObjeto("control", control);
		accionweb.agregarObjeto("actividadSeleccionada", codActividad);
		accionweb.agregarObjeto("nomActividad", nomActividad);
		accionweb.agregarObjeto("codgru", codgru);
	  	}
 	  
	}
	public void corrientePDF(AccionWeb accionweb) throws Exception {
		Vector vec_datos = new Vector();
		Vector listaReqResumen = new Vector();
		Vector listaReqDetResumen = new Vector();
		vec_datos = moduloPresupuesto.getlistaReqResumenExcel(accionweb.getReq());
		moduloPresupuesto.getlistaPresupItemreq(accionweb.getReq());
		listaReqResumen = (Vector) accionweb.getSesion().getAttribute("listaReqResumen");
		listaReqDetResumen = (Vector) accionweb.getSesion().getAttribute("listaReqDetResumen");
	//	System.out.println("listaReqResumen en presup: "+listaReqResumen);
		moduloPresupuesto.agregaCorrientePDF(accionweb.getReq(), vec_datos, listaReqResumen, listaReqDetResumen);
	}
	
	public void corrientePDFPlan(AccionWeb accionweb) throws Exception {
		Vector vec_datos = new Vector();
		Vector listaReqResumen = new Vector();
		Vector listaReqDetResumen = new Vector();
		vec_datos = moduloPresupuesto.getlistaReqResumenExcelPlan(accionweb.getReq());
		moduloPresupuesto.getlistaPresupItemreqPlan(accionweb.getReq());
		listaReqResumen = (Vector) accionweb.getSesion().getAttribute("listaReqResumen");
		listaReqDetResumen = (Vector) accionweb.getSesion().getAttribute("listaReqDetResumen");
	//	System.out.println("listaReqResumen en presup: "+listaReqResumen);
		moduloPresupuesto.agregaCorrientePDF(accionweb.getReq(), vec_datos, listaReqResumen, listaReqDetResumen);
	}
	public void cargaControlPlanes(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		String titulo = Util.validaParametro(accionweb.getParameter("titulo"),"");
		String tipo = Util.validaParametro(accionweb.getParameter("tipo"),"");
		List<Presw25DTO> listaControlPlanes = null;
		int annoControl = Util.validaParametro(accionweb.getParameter("annoControl"), 0);
	
	//	if(rutOrigen > 0){
	//		this.limpiaSimulador(accionweb);
	//	}	
	
		listaControlPlanes = 	moduloPresupuesto.getControlPlanes(tipo, rutUsuario, dv, annoControl);
		accionweb.agregarObjeto("listaControlPlanes", listaControlPlanes);
	
			
	
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control de planes; 2 = planes de desarrollo
		accionweb.agregarObjeto("titulo", titulo);	
		accionweb.agregarObjeto("control", control);
		accionweb.agregarObjeto("tipo", tipo);
		accionweb.agregarObjeto("menuPlanDesarrollo", 1); // es para no mostrar el menu en ingreso
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("annoControl", annoControl);
	
		}
	
	public void cargaActividadControlPlan(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		String titulo = Util.validaParametro(accionweb.getParameter("titulo"),"");
		String titulo2 = Util.validaParametro(accionweb.getParameter("titulo2"),"");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"),0);
		int codCuenta = Util.validaParametro(accionweb.getParameter("codCuenta"), 0);
		int codUnidadMacro = Util.validaParametro(accionweb.getParameter("codUnidadMacro"), 0);
		String nomCuenta = Util.validaParametro(accionweb.getParameter("nomCuenta"),"");
		String codMacro = Util.validaParametro(accionweb.getParameter("codMacro"), "");
		String nomMacro = Util.validaParametro(accionweb.getParameter("nomMacro"),"");
		int annoControl = Util.validaParametro(accionweb.getParameter("annoControl"),0);
		
		List<Presw25DTO> listaControlPlanes = null;
		String nomTipo = "";
		
		//if(rutOrigen > 0){
		//	this.limpiaSimulador(accionweb);
		//}
		switch(tipo){
		case 1 :{// muestra actividades de macrounidad
				nomTipo = "DAM";
				listaControlPlanes = 	moduloPresupuesto.getActividadesPlanesDesarrollo(nomTipo, rutUsuario, dv,codMacro, annoControl);	
				accionweb.agregarObjeto("listaActividadControlPlanes", listaControlPlanes);
				break;			
				}
		case 2 :{ // desplegar datos de la actividad
			    nomTipo = "DDA";
				listaControlPlanes = 	moduloPresupuesto.getListaPlanesDesarrollo(nomTipo, rutUsuario, dv,"", codCuenta);
		
				for (Presw25DTO ss : listaControlPlanes){
						accionweb.agregarObjeto("responsable",ss.getComen1());
					/*	antes eran estos valores cambiaron a lo siguiente PP 23/09/2013
					 *  accionweb.agregarObjeto("montoAprobado",ss.getPres01());
						accionweb.agregarObjeto("montoEjecutado",ss.getPres02());
						accionweb.agregarObjeto("montoPorcentaje",ss.getPres03());*/
						accionweb.agregarObjeto("montoEjecutado",ss.getPres01());
						accionweb.agregarObjeto("montoPorcentaje",ss.getPres02());
				
						accionweb.agregarObjeto("comen2",ss.getComen2());
						accionweb.agregarObjeto("comen3",ss.getComen3());
						accionweb.agregarObjeto("comen4",ss.getComen4());
						accionweb.agregarObjeto("comen5",ss.getComen5());
						accionweb.agregarObjeto("comen6",ss.getComen6());
						accionweb.agregarObjeto("motiv1",ss.getMotiv1());
						accionweb.agregarObjeto("motiv2",ss.getMotiv2());
						accionweb.agregarObjeto("motiv3",ss.getMotiv3());
						accionweb.agregarObjeto("motiv4",ss.getMotiv4());
						accionweb.agregarObjeto("motiv5",ss.getMotiv5());
						accionweb.agregarObjeto("motiv6",ss.getMotiv6());
						accionweb.agregarObjeto("anno1",ss.getValr11());
						accionweb.agregarObjeto("anno2",ss.getValr12());
						accionweb.agregarObjeto("anno3",ss.getValr21());
						accionweb.agregarObjeto("anno4",ss.getValr22());
						accionweb.agregarObjeto("anno5",ss.getValr31());
						accionweb.agregarObjeto("anno6",ss.getValr32());
						accionweb.agregarObjeto("anno7",ss.getValr41());
						accionweb.agregarObjeto("anno8",ss.getValr42());
						//accionweb.agregarObjeto("anno11",ss.getv);
						//accionweb.agregarObjeto("anno12",ss.getValpr1());
						accionweb.agregarObjeto("valorAnno1",ss.getPres03());
						accionweb.agregarObjeto("valorAnno2",ss.getPres04());
						accionweb.agregarObjeto("valorAnno3",ss.getPres05());
						accionweb.agregarObjeto("valorAnno4",ss.getPres06());
						accionweb.agregarObjeto("valorAnno5",ss.getPres07());
						accionweb.agregarObjeto("valorAnno6",ss.getPres08());
						accionweb.agregarObjeto("valorAnno7",ss.getPres09());
						accionweb.agregarObjeto("valorAnno8",ss.getPres10());
							
				}
				break;
			
		}
		case 3: {//muestra de indicadores
			
			 List<Cocow36DTO> listaIndicadores = moduloPresupuesto.getListaIndicadores(rutUsuario, dv, codCuenta);	
			 accionweb.agregarObjeto("listaIndicadores", listaIndicadores);
			 break;
		}
		case 4:  {// Agrega nuevo valor indicador
			    nomTipo = "ODI";
			    List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
				CocowBean cocowBean = new CocowBean();
				 String comen1 = "";
				 String comen2 = "";
				 String comen3 = "";
				 String comen4 = "";
				 String comen5 = "";
				listCocow36DTO =  moduloPresupuesto.getListaValorIndicadores(nomTipo, rutUsuario, dv, titulo, codCuenta);
				 int i = 1;
				for(Cocow36DTO ss: listCocow36DTO){
			
					if(ss.getNomcam().trim().equals("DETTEX")){
						switch (i){
						case 1: comen1 = ss.getResval();
						break;
						case 2: comen2 = ss.getResval();
						break;
						case 3: comen3 = ss.getResval();
						break;
						case 4: comen4 = ss.getResval();
						break;
					}
					}
					 if(ss.getNomcam().trim().equals("DETMET")){
						 comen5 = ss.getResval(); 
					 }
					i++;
				}	
		      accionweb.agregarObjeto("indicador", titulo);
		      accionweb.agregarObjeto("comen1", comen1);
		      accionweb.agregarObjeto("comen2", comen2);
		      accionweb.agregarObjeto("comen3", comen3);
		      accionweb.agregarObjeto("comen4", comen4);
		      accionweb.agregarObjeto("comen5", comen5);
		    break;
		}
		case 5: {//modifica indicador
			
			break;
		}
		case 6: {//ejecucion por item --- se cambi� por disponibilidad mensual
			int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
			List<Presw18DTO> listaPRESW18 = new ArrayList<Presw18DTO>();
            if(anno == 0){ // a�o actual
            	if(annoControl > 0)
            		anno = annoControl;
            	else {
            		Date fecActual = new Date();		     
            		anno = 1900 + fecActual.getYear() ;
            	}
        
            }
        	Item items = new Item();
    		Collection<Item> listaItem = (Collection<Item>) items.todos_items();
    		accionweb.agregarObjeto("listaItem", listaItem);
         //   listaPRESW18 = moduloPresupuesto.getListaEjecucionPorItem(codCuenta, anno);			
           //if(listaPRESW18 != null && listaPRESW18.size() >0 )
			//	accionweb.agregarObjeto("listaEjecucion", listaPRESW18);
			
			accionweb.agregarObjeto("codItem", titulo);
			accionweb.agregarObjeto("nomItem", titulo2);
			accionweb.agregarObjeto("annoselec", anno);
			accionweb.agregarObjeto("tipoCuentas", "C");
		
			 break;
		}
		case 7: {//Ver indicadores
			//System.out.println("tipo: "+7);
			 nomTipo = "ODI";
			 String comen1 = "";
			 String comen2 = "";
			 String comen3 = "";
			 String comen4 = "";
			 String comen5 = "";
			    List<Cocow36DTO> listaIndicadores = new ArrayList<Cocow36DTO>();
			    List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
				  
				PreswBean preswbean = new PreswBean(nomTipo, 0, 0, 0, 0, rutUsuario);
				preswbean.setDigide(dv);
				preswbean.setCoduni(codCuenta);
			
				Presw25DTO presw25DTO = new Presw25DTO();
				presw25DTO.setTippro(nomTipo);
				presw25DTO.setComen1(titulo);
				presw25DTO.setCoduni(codCuenta);
					
			
				List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
				CocowBean cocowBean = new CocowBean();
			
				listCocow36DTO =  moduloPresupuesto.getListaValorIndicadores(nomTipo, rutUsuario, dv, titulo, codCuenta);
				 int i = 1;
				for(Cocow36DTO ss: listCocow36DTO){
			
					if(ss.getNomcam().trim().equals("DETTEX")){
						switch (i){
						case 1: comen1 = ss.getResval();
						break;
						case 2: comen2 = ss.getResval();
						break;
						case 3: comen3 = ss.getResval();
						break;
						case 4: comen4 = ss.getResval();
						break;
						
						}
					}
					if(ss.getNomcam().trim().equals("DETMET")){
					    comen5 = ss.getResval();
				
					}
					i++;
				}	
			
			
			      accionweb.agregarObjeto("comen1", comen1);
			      accionweb.agregarObjeto("comen2", comen2);
			      accionweb.agregarObjeto("comen3", comen3);
			      accionweb.agregarObjeto("comen4", comen4);
			      accionweb.agregarObjeto("comen5", comen5);
			  nomTipo = "VVI";
			  List<Cocow36DTO> listaValorIndicadores = new ArrayList<Cocow36DTO>();
			  listaValorIndicadores = moduloPresupuesto.getListaValorIndicadores(nomTipo, rutUsuario, dv, titulo, codCuenta);
		      accionweb.agregarObjeto("listaValorIndicadores", listaValorIndicadores); 
		      accionweb.agregarObjeto("indicador", titulo);
		      titulo = "";
		
			break;
		}
		case 8: {// Modificacion de indicador
		        nomTipo = "MNI";          		
			
		    	Collection<Presw19DTO> lista = null;
				PreswBean preswbean = new PreswBean(nomTipo, 0, 0, 0, 0, rutUsuario);
				preswbean.setDigide(dv);
				preswbean.setCoduni(codCuenta);
			//	preswbean.setAnopar(anno);
				List<Presw25DTO> listaPRESW25 = (List<Presw25DTO>) preswbean.consulta_presw25();

			 if(listaPRESW25 != null && listaPRESW25.size() >0 )
				accionweb.agregarObjeto("listaEjecucion", listaPRESW25);
			 break;
		}
		case 9: {// lista notificaciones
	        nomTipo = "ODA";          		
		
		  	Collection<Presw19DTO> lista = null;
			PreswBean preswbean = new PreswBean(nomTipo, 0, 0, 0, 0, rutUsuario);
			preswbean.setDigide(dv);
			preswbean.setCoduni(codCuenta);
			List<Presw25DTO> listaPRESW25 = (List<Presw25DTO>) preswbean.consulta_presw25();

		 if(listaPRESW25 != null && listaPRESW25.size() >0 )
			accionweb.agregarObjeto("listaNotificaciones", listaPRESW25);
		 break;
	}
		case 10: {// detalle ejecucion por item
			int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		    nomTipo = "DEA";
		     if(anno == 0){// a�o actual
	            	Date fecActual = new Date();		     
	        		anno = 1900 + fecActual.getYear() ;
	        
	            } 		
			if(anno > 0){
			  	Collection<Presw19DTO> lista = null;
				PreswBean preswbean = new PreswBean(nomTipo, 0, 0, 0, 0, rutUsuario);
				preswbean.setDigide(dv);
				preswbean.setCoduni(codCuenta);
				preswbean.setAnopar(anno);
				preswbean.setItedoc(Integer.parseInt(titulo));
				List<Presw18DTO> listaPRESW25 = (List<Presw18DTO>) preswbean.consulta_presw18();

			 if(listaPRESW25 != null && listaPRESW25.size() >0 )
				accionweb.agregarObjeto("listaDetalleEjecucion", listaPRESW25);
			}
			accionweb.agregarObjeto("codItem", titulo);
			accionweb.agregarObjeto("nomItem", titulo2);
			accionweb.agregarObjeto("annoselec", anno);
			 break;
		}
		case 11: {//Ver datos de indicadores
		      nomTipo = "ODI";
			  List<Cocow36DTO> listaIndicadores = new ArrayList<Cocow36DTO>();
			  listaIndicadores = moduloPresupuesto.getListaValorIndicadores(nomTipo, rutUsuario, dv, titulo, codCuenta);
			 int i = 1;
			  if(listaIndicadores.size() > 0){
			  for(Cocow36DTO ss: listaIndicadores){
				if(ss.getNomcam().trim().equals("DETTEX")){
					switch (i){
					case 1: accionweb.agregarObjeto("comen1", ss.getResval());
					break;
					case 2: accionweb.agregarObjeto("comen2", ss.getResval());
					break;
					case 3: accionweb.agregarObjeto("comen3", ss.getResval());
					break;
					case 4: accionweb.agregarObjeto("comen4", ss.getResval());
					break;
					}
				}
				if(ss.getNomcam().trim().equals("DETMET")){
					accionweb.agregarObjeto("comen5", ss.getResval());
				
				}
				i++;
			  }
			  }
			
			
		      accionweb.agregarObjeto("indicador", titulo);
		      titulo = "";
			break;
		}
		case 12: {//Historial
	        nomTipo = "GIM";          		
			Collection<Presw19DTO> lista = null;
			PreswBean preswbean = new PreswBean(nomTipo, 0, 0, 0, 0, rutUsuario);
			preswbean.setDigide(dv);
			preswbean.setCoduni(codCuenta);
		//	preswbean.setAnopar(anno);
			List<Presw25DTO> listaPRESW25 = (List<Presw25DTO>) preswbean.consulta_presw25();

		 if(listaPRESW25 != null && listaPRESW25.size() >0 )
			accionweb.agregarObjeto("listaHistorial", listaPRESW25);
		 break;
	}
		}
	/*	if(!tipo.trim().equals("PIA")){
		if(tipo.trim().equals("DAM"))
			listaControlPlanes = 	moduloPresupuesto.getListaPlanesDesarrollo(tipo, rutUsuario, dv,codgru, 0);
		else
			listaControlPlanes = 	moduloPresupuesto.getListaPlanesDesarrollo(tipo, rutUsuario, dv,codgru, codUni);
		accionweb.agregarObjeto("listaActividadControlPlanes", listaControlPlanes);
		}
	    
		if(tipo.trim().equals("DDA")){
		
		}
		if(tipo.trim().equals("PIA")){
			
		}
	*/
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control de planes; 2 = planes de desarrollo
		accionweb.agregarObjeto("titulo", titulo);	
		accionweb.agregarObjeto("titulo2", titulo2);	
		accionweb.agregarObjeto("control", control);
		accionweb.agregarObjeto("tipo", tipo);
		accionweb.agregarObjeto("menuPlanDesarrollo", 1); // es para no mostrar el menu en ingreso
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("codCuenta", codCuenta);
		accionweb.agregarObjeto("nomCuenta", nomCuenta);
		accionweb.agregarObjeto("codMacro", codMacro);
		accionweb.agregarObjeto("nomMacro", nomMacro);
		accionweb.agregarObjeto("codUnidadMacro", codUnidadMacro);
		accionweb.agregarObjeto("annoControl", annoControl);
		
		
		}
	public void cargaNotificacion(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		int codCuenta = Util.validaParametro(accionweb.getParameter("codCuenta"), 0);
		int codUnidadMacro = Util.validaParametro(accionweb.getParameter("codUnidadMacro"), 0);
		String nomCuenta = Util.validaParametro(accionweb.getParameter("nomCuenta"),"");
		String codMacro = Util.validaParametro(accionweb.getParameter("codMacro"), "");
		String nomMacro = Util.validaParametro(accionweb.getParameter("nomMacro"),"");
		String tipcue = Util.validaParametro(accionweb.getParameter("tipcue"),"");
		String nomTipcue = Util.validaParametro(accionweb.getParameter("nomTipcue"),"");
		int fecmov = Util.validaParametro(accionweb.getParameter("fecmov"), 0);
		int rutide = Util.validaParametro(accionweb.getParameter("rutide"), 0);
		String digide = Util.validaParametro(accionweb.getParameter("digide"),"");
		String usuario = Util.validaParametro(accionweb.getParameter("usuario"),"");
		int numdoc = Util.validaParametro(accionweb.getParameter("numdoc"), 0);
		int annoControl = Util.validaParametro(accionweb.getParameter("annoControl"), 0);
	
		List<Presw25DTO> listaControlPlanes = null;
		String nomTipo = "";
		// detalle notificaciones

        nomTipo = "ODN";          		
	
	  Collection<Presw19DTO> lista = null;
		PreswBean preswbean = new PreswBean(nomTipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setRutide(rutide);
		preswbean.setDigide(digide);
		preswbean.setCoduni(codCuenta);
		preswbean.setFecmov(fecmov);
		preswbean.setTipcue(tipcue);
		preswbean.setNumdoc(numdoc);
		List<Presw25DTO> listaPRESW25 = (List<Presw25DTO>) preswbean.consulta_presw25();
          
	 if(listaPRESW25 != null && listaPRESW25.size() >0 ){
		 int i = 1;
		 String tipoNotificacion = "";
		  for(Presw25DTO ss: listaPRESW25){
			  switch(i){
			  case 1: accionweb.agregarObjeto("comen1", ss.getComen1().trim());
			  break;
			  case 2: accionweb.agregarObjeto("comen2", ss.getComen1().trim());
			  break;
			  case 3: accionweb.agregarObjeto("comen3", ss.getComen1().trim());
			  break;
			  case 4: accionweb.agregarObjeto("comen4", ss.getComen1().trim());
			  break;
			 }
			  i++;
		  }	
	 }
	 accionweb.agregarObjeto("tipcue", tipcue);
	 accionweb.agregarObjeto("fecmov", fecmov);
	 accionweb.agregarObjeto("rutide",rutide );
	 accionweb.agregarObjeto("digide",digide );
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control de planes; 2 = planes de desarrollo
		accionweb.agregarObjeto("control", control);
		accionweb.agregarObjeto("menuPlanDesarrollo", 1); // es para no mostrar el menu en ingreso
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("codCuenta", codCuenta);
		accionweb.agregarObjeto("nomCuenta", nomCuenta);
		accionweb.agregarObjeto("codMacro", codMacro);
		accionweb.agregarObjeto("nomMacro", nomMacro);
		accionweb.agregarObjeto("codUnidadMacro", codUnidadMacro); 
		accionweb.agregarObjeto("nomTipcue", nomTipcue);
		accionweb.agregarObjeto("usuario", usuario);
		accionweb.agregarObjeto("numDoc", numdoc);
	    accionweb.agregarObjeto("annoControl", annoControl);
	}
	public void registraIndicador(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		String titulo = Util.validaParametro(accionweb.getParameter("titulo"),"");
		String titulo2 = Util.validaParametro(accionweb.getParameter("titulo2"),"");
		int tipoAccion = Util.validaParametro(accionweb.getParameter("tipoAccion"),0);
		int codCuenta = Util.validaParametro(accionweb.getParameter("codCuenta"), 0);
		int codUnidadMacro = Util.validaParametro(accionweb.getParameter("codUnidadMacro"), 0);
		String nomCuenta = Util.validaParametro(accionweb.getParameter("nomCuenta"),"");
		String codMacro = Util.validaParametro(accionweb.getParameter("codMacro"), "");
		String nomMacro = Util.validaParametro(accionweb.getParameter("nomMacro"),"");
		String indicador = Util.validaParametro(accionweb.getParameter("indicador"),"");
		String comen1 = Util.validaParametro(accionweb.getParameter("comen1"),"");
		String comen2 = Util.validaParametro(accionweb.getParameter("comen2"),"");
		String comen3 = Util.validaParametro(accionweb.getParameter("comen3"),"");
		String comen4 = Util.validaParametro(accionweb.getParameter("comen4"),"");
		String comen5 = Util.validaParametro(accionweb.getParameter("comen5"),"");
		int annoControl = Util.validaParametro(accionweb.getParameter("annoControl"), 0);
        boolean error = false;
        List<Presw25DTO> listaControlPlanes = new ArrayList<Presw25DTO>();
        Presw25DTO presw25DTO = new Presw25DTO();
        presw25DTO.setCoduni(codCuenta);
        presw25DTO.setDesite(indicador);
        presw25DTO.setComen1(comen1);
        presw25DTO.setComen2(comen2);
        presw25DTO.setComen3(comen3);
        presw25DTO.setComen4(comen4);
        presw25DTO.setComen5(comen5);
        presw25DTO.setRutide(rutUsuario);
        presw25DTO.setDigide(dv);
        listaControlPlanes.add(presw25DTO);
    	try {
    		boolean existeIndicador = false;
    		String nomTipo = "GNT";
    		
			if(tipoAccion == 11)
				nomTipo = "MNI";
			if(tipoAccion != 11){
    		List<Cocow36DTO> listaIndicadores = new ArrayList<Cocow36DTO>();
    		listaIndicadores = moduloPresupuesto.getListaIndicadores(rutUsuario, dv, codCuenta);
           
    		for (Cocow36DTO ss : listaIndicadores) {	
    			if(indicador.trim().equals(ss.getValalf().trim()))
    				existeIndicador = true;
    		}
			} 
		//	System.out.println("registraIndicador   nomTipo: "+nomTipo);
    		if(existeIndicador)
    			accionweb.agregarObjeto("mensaje", "Este indicador ya se encuentra registrado");
    		else {
    		  error = moduloPresupuesto.saveIndicadores(nomTipo, listaControlPlanes, dv, rutUsuario);
    		  if(error){
    			  this.cargaActividadControlPlan(accionweb);
    			  accionweb.agregarObjeto("mensaje", "Registr� exitosamente Indicador.");
    		  } else
    			  accionweb.agregarObjeto("mensaje", "Problemas en registrar indicador");
    		}
    		 
    	} catch (Exception e) {
			log.debug("No guardada registraIndicador");
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
    	
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control de planes; 2 = planes de desarrollo
		accionweb.agregarObjeto("titulo", titulo);	
		accionweb.agregarObjeto("titulo2", titulo2);	
		accionweb.agregarObjeto("control", control);
		accionweb.agregarObjeto("tipo", tipoAccion);
		accionweb.agregarObjeto("menuPlanDesarrollo", 1); // es para no mostrar el menu en ingreso
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("codCuenta", codCuenta);
		accionweb.agregarObjeto("nomCuenta", nomCuenta);
		accionweb.agregarObjeto("codMacro", codMacro);
		accionweb.agregarObjeto("nomMacro", nomMacro);
		accionweb.agregarObjeto("codUnidadMacro", codUnidadMacro);
        accionweb.agregarObjeto("annoControl", annoControl);
		
	}
	
	
	 synchronized public void registraNuevoValorIndicador(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		String titulo = Util.validaParametro(accionweb.getParameter("titulo"),"");
		String titulo2 = Util.validaParametro(accionweb.getParameter("titulo2"),"");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"),0);
		int codCuenta = Util.validaParametro(accionweb.getParameter("codCuenta"), 0);
		int codUnidadMacro = Util.validaParametro(accionweb.getParameter("codUnidadMacro"), 0);
		String nomCuenta = Util.validaParametro(accionweb.getParameter("nomCuenta"),"");
		String codMacro = Util.validaParametro(accionweb.getParameter("codMacro"), "");
		String nomMacro = Util.validaParametro(accionweb.getParameter("nomMacro"),"");
		String comen1 = Util.validaParametro(accionweb.getParameter("comen1"),"");
		String comen2 = Util.validaParametro(accionweb.getParameter("comen2"),"");
		String comen3 = Util.validaParametro(accionweb.getParameter("comen3"),"");
		String comen4 = Util.validaParametro(accionweb.getParameter("comen4"),"");
		String descr1 = Util.validaParametro(accionweb.getParameter("descr1"),"");
		String descr2 = Util.validaParametro(accionweb.getParameter("descr2"),"");
		String descr3 = Util.validaParametro(accionweb.getParameter("descr3"),"");
		String descr4 = Util.validaParametro(accionweb.getParameter("descr4"),"");
		String fechaIni = Util.validaParametro(accionweb.getParameter("fechaIni"),"");
		int fecha = Integer.parseInt(fechaIni.substring(6) + fechaIni.substring(3,5) + fechaIni.substring(0,2));
        boolean error = false;
        List<Presw25DTO> listaControlPlanes = new ArrayList<Presw25DTO>();
        Presw25DTO presw25DTO = new Presw25DTO();
        presw25DTO.setCoduni(codCuenta);
        presw25DTO.setDesite(titulo);
        presw25DTO.setComen1(comen1);
        presw25DTO.setComen2(comen2);
        presw25DTO.setComen3(comen3);
        presw25DTO.setComen4(comen4);
        presw25DTO.setPres01(fecha);
        presw25DTO.setRutide(rutUsuario);
        presw25DTO.setDigide(dv);
        listaControlPlanes.add(presw25DTO);
    	try {
    		  error = moduloPresupuesto.saveIndicadores("GVI", listaControlPlanes, dv, rutUsuario);
    		  if(error)
    			  accionweb.agregarObjeto("mensaje", "Registr� exitosamente el valor del Indicador.");
    		  else
    			  accionweb.agregarObjeto("mensaje", "Problemas en registrar el valor del indicador");
    		  this.cargaActividadControlPlan(accionweb);
    	} catch (Exception e) {
			log.debug("No guardada registraIndicador");
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control de planes; 2 = planes de desarrollo
		cargaActividadControlPlan(accionweb);
		accionweb.agregarObjeto("titulo", titulo);	
		accionweb.agregarObjeto("titulo2", titulo2);	
		accionweb.agregarObjeto("control", control);
		accionweb.agregarObjeto("tipo", tipo);
		accionweb.agregarObjeto("menuPlanDesarrollo", 1); // es para no mostrar el menu en ingreso
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("codCuenta", codCuenta);
		accionweb.agregarObjeto("nomCuenta", nomCuenta);
		accionweb.agregarObjeto("codMacro", codMacro);
		accionweb.agregarObjeto("nomMacro", nomMacro);
		accionweb.agregarObjeto("codUnidadMacro", codUnidadMacro);
		accionweb.agregarObjeto("comen1", descr1);
		accionweb.agregarObjeto("comen2", descr2);
		accionweb.agregarObjeto("comen3", descr3);
		accionweb.agregarObjeto("comen4", descr4);
		accionweb.agregarObjeto("indicador", titulo);	
	}
	 synchronized public void registraNuevaNotificacion(AccionWeb accionweb) throws Exception {
		/* falta**/
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		String titulo = Util.validaParametro(accionweb.getParameter("titulo"),"");
		String titulo2 = Util.validaParametro(accionweb.getParameter("titulo2"),"");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"),0);
		int codCuenta = Util.validaParametro(accionweb.getParameter("codCuenta"), 0);
		int codUnidadMacro = Util.validaParametro(accionweb.getParameter("codUnidadMacro"), 0);
		String nomCuenta = Util.validaParametro(accionweb.getParameter("nomCuenta"),"");
		String codMacro = Util.validaParametro(accionweb.getParameter("codMacro"), "");
		String nomMacro = Util.validaParametro(accionweb.getParameter("nomMacro"),"");
		String comen1 = Util.validaParametro(accionweb.getParameter("comen1"),"");
		String comen2 = Util.validaParametro(accionweb.getParameter("comen2"),"");
		String comen3 = Util.validaParametro(accionweb.getParameter("comen3"),"");
		String comen4 = Util.validaParametro(accionweb.getParameter("comen4"),"");
		String tipoNotificacion = Util.validaParametro(accionweb.getParameter("tipoNotificacion"),"");
		int annoControl = Util.validaParametro(accionweb.getParameter("annoControl"), 0);
        boolean error = false;
        List<Presw25DTO> listaControlPlanes = new ArrayList<Presw25DTO>();
        Presw25DTO presw25DTO = new Presw25DTO();
        presw25DTO.setCoduni(codCuenta);
        presw25DTO.setComen1(comen1);  
        presw25DTO.setRutide(rutUsuario);
        presw25DTO.setDigide(dv);
        listaControlPlanes.add(presw25DTO);
        if(!comen2.trim().equals("")){
        	presw25DTO = new Presw25DTO();
            presw25DTO.setCoduni(codCuenta);
            presw25DTO.setComen1(comen2);  
            presw25DTO.setRutide(rutUsuario);
            presw25DTO.setDigide(dv);
            listaControlPlanes.add(presw25DTO);
        }
        if(!comen3.trim().equals("")){
        	presw25DTO = new Presw25DTO();
            presw25DTO.setCoduni(codCuenta);
            presw25DTO.setComen1(comen3);  
            presw25DTO.setRutide(rutUsuario);
            presw25DTO.setDigide(dv);
            listaControlPlanes.add(presw25DTO);
        }
        if(!comen4.trim().equals("")){
        	presw25DTO = new Presw25DTO();
            presw25DTO.setCoduni(codCuenta);
            presw25DTO.setComen1(comen4);  
            presw25DTO.setRutide(rutUsuario);
            presw25DTO.setDigide(dv);
            listaControlPlanes.add(presw25DTO);
        }
    	try {
    		  error = moduloPresupuesto.saveNotificacion("GNA", listaControlPlanes, dv, rutUsuario, tipoNotificacion, codCuenta);
    		  if(error)
    			  accionweb.agregarObjeto("mensaje", "Registr� exitosamente la notificaci�n.");
    		  else
    			  accionweb.agregarObjeto("mensaje", "Problemas en registrar la notificaci�n");
    		  this.cargaActividadControlPlan(accionweb);
    	} catch (Exception e) {
			log.debug("No guardada notificaci�n");
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control de planes; 2 = planes de desarrollo
		accionweb.agregarObjeto("titulo", titulo);	
		accionweb.agregarObjeto("titulo2", titulo2);	
		accionweb.agregarObjeto("control", control);
		accionweb.agregarObjeto("tipo", tipo);
		accionweb.agregarObjeto("menuPlanDesarrollo", 1); // es para no mostrar el menu en ingreso
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("codCuenta", codCuenta);
		accionweb.agregarObjeto("nomCuenta", nomCuenta);
		accionweb.agregarObjeto("codMacro", codMacro);
		accionweb.agregarObjeto("nomMacro", nomMacro);
		accionweb.agregarObjeto("codUnidadMacro", codUnidadMacro);
		accionweb.agregarObjeto("annoControl", annoControl);
		
	}
	
	/*metodo periodo */
	public void cargaPresupPeriodo(AccionWeb accionweb) throws Exception {
      //  this.cargaPresup(accionweb);		
		int a�o =Integer.parseInt( Util.validaParametro(accionweb.getSesion().getAttribute("anno")+"","0"));
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int codActividad = Util.validaParametro(accionweb.getParameter("codActividad"), 0);
		String nomActividad = Util.validaParametro(accionweb.getParameter("nomActividad"),"");
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		String nombre = "";
		String codgru = "";
		
	    int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		accionweb.getSesion().setAttribute("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		
		accionweb.agregarObjeto("anno", a�o); 
		accionweb.agregarObjeto("control", control);
				
	}	
	
/* se elimina esta opci�n de vacante 24/09/2010
 * 	public void eliminaResultadoPresupVacante(AccionWeb accionweb)	throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int ao = Util.validaParametro(accionweb.getParameter("anno"), 0);
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String cargo = Util.validaParametro(accionweb.getParameter("cargo"), "");
		int puestos = Util.validaParametro(accionweb.getParameter("puestos"), 0);
		String motivo = Util.validaParametro(accionweb.getParameter("motivo"), "");
		List<Presw25DTO> resultadoPresupVacante = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupVacante");
		List<Presw25DTO> listaResultadoPresupVacante = new ArrayList<Presw25DTO>();
		
        Presw25DTO lista = new Presw25DTO(); 
		lista.setTippro("GNV");
		lista.setAnopre(ao);
		lista.setCoduni(unidad);
		lista.setComen1(cargo);
		lista.setPres01(puestos);
		lista.setComen2(motivo);
           	for (Presw25DTO ss : resultadoPresupVacante) {	
           		if(ss.getAnopre() == ao &&
		      ss.getCoduni() == unidad &&
		      ss.getComen1().trim().equals(cargo.trim()) &&
		      ss.getPres01() == puestos &&
		      ss.getComen2().trim().equals(motivo.trim()))
			System.out.println("iguales");
		   else {
			   listaResultadoPresupVacante.add(ss);
;		   }
		}

			
		moduloPresupuesto.agregaResultadoVacanteSesion(accionweb.getReq(), listaResultadoPresupVacante);
		listaResultadoPresupVacante = (List<Presw25DTO>) accionweb.getSesion().getAttribute("resultadoPresupVacante");
	//	for (Presw25DTO ss : listaResultadoPresupVacante){
	//		System.out.println("ss."+ ss.getComen1());
	//	}
		accionweb.agregarObjeto("resultadoPresupVacante", listaResultadoPresupVacante);
	}*/
	public void controlPlanDesarrolloVector(AccionWeb accionweb) throws Exception {
		  int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		  String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		  int annoControl = Util.validaParametro(accionweb.getParameter("annoControl"), 0);
		  List<Presw25DTO> listaControlPlanes = null;
		  listaControlPlanes = 	moduloPresupuesto.getControlPlanes("DPD", rutUsuario, dv, annoControl);
		  Vector vec = new Vector();
		  
		  NumberTool numbertool = new NumberTool();
		  MathTool mathtool = new MathTool();
			
		  for (Presw25DTO ss : listaControlPlanes){
			  Vector vec_datos = new Vector();
			  vec_datos.addElement(ss.getCoduni()+"-"+ ss.getComen1().trim());
			  vec_datos.addElement(String.valueOf(ss.getPres01()));
			  vec_datos.addElement(String.valueOf(ss.getPres02()));
			  vec_datos.addElement(String.valueOf(mathtool.round(mathtool.div(ss.getPres03(),100))));
			  vec_datos.addElement(String.valueOf(ss.getPres04()));
			  vec_datos.addElement(String.valueOf(ss.getPres05()));
			  vec_datos.addElement(String.valueOf(ss.getPres06()));	
			  vec.addElement(vec_datos);
	  }
		  
	          
		  moduloPresupuesto.agregaPlanDesarrolloExcel(accionweb.getReq(), vec, annoControl);
		}
	
	public void controlPlanActividadVector(AccionWeb accionweb) throws Exception {
		  int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		  String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		  String nomTipo = Util.validaParametro(accionweb.getParameter("nomTipo")+"","");
		  String codMacro = Util.validaParametro(accionweb.getParameter("codMacro")+"", "");
		  int annoControl = Util.validaParametro(accionweb.getParameter("annoControl"), 0);
		  List<Presw25DTO> listaActividadControlPlanes = null;
		  listaActividadControlPlanes = 	moduloPresupuesto.getActividadesPlanesDesarrollo(nomTipo, rutUsuario, dv,codMacro, annoControl);	
		  Vector vec = new Vector();
				
		  for (Presw25DTO ss : listaActividadControlPlanes){
			  Vector vec_datos = new Vector();
			  vec_datos.addElement(ss.getCoduni()+"-"+ ss.getDesuni().trim());
			  vec_datos.addElement(String.valueOf(ss.getPres01()));
			  vec_datos.addElement(String.valueOf(ss.getPres02()));
			  vec_datos.addElement(String.valueOf(ss.getPres03()));
			  vec.addElement(vec_datos);
		  }
	          
		  moduloPresupuesto.agregaPlanDesarrolloExcel(accionweb.getReq(), vec, annoControl);
		}
	
	public void controlPlanEjecucionVector(AccionWeb accionweb) throws Exception {
		  int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		  String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		  String nomTipo = Util.validaParametro(accionweb.getParameter("nomTipo")+"","");
		  String codMacro = Util.validaParametro(accionweb.getParameter("codMacro")+"", "");
		  int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		  int codCuenta = Util.validaParametro(accionweb.getParameter("codCuenta"), 0);
		  int annoControl = Util.validaParametro(accionweb.getParameter("annoControl"), 0);
			
		  MathTool mathtool = new MathTool();	
		  
		  List<Presw18DTO> listaPRESW18 = new ArrayList<Presw18DTO>();
            if(anno == 0){ // a�o actual
            	Date fecActual = new Date();		     
        		anno = 1900 + fecActual.getYear() ;
        
            }
            	
            listaPRESW18 = moduloPresupuesto.getListaEjecucionPorItem(codCuenta, anno);		
		  
		  Vector vec = new Vector();
			
		  for (Presw18DTO ss : listaPRESW18){
			  Vector vec_datos = new Vector();
			  vec_datos.addElement(ss.getItedoc()+"-"+ ss.getDesite().trim());
			  vec_datos.addElement(String.valueOf(ss.getPresac()));
			  vec_datos.addElement(String.valueOf(ss.getUsadac()));
			  vec_datos.addElement(String.valueOf(mathtool.sub(ss.getPresac(),ss.getUsadac())));
			  vec.addElement(vec_datos);
		  }
	          
		  moduloPresupuesto.agregaPlanDesarrolloExcel(accionweb.getReq(), vec, annoControl);
		}

	public void controlPlanDetalleEjecVector(AccionWeb accionweb) throws Exception {
		  int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		  String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		  String nomTipo = Util.validaParametro(accionweb.getParameter("nomTipo")+"","");
		  String codMacro = Util.validaParametro(accionweb.getParameter("codMacro")+"", "");
		  int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		  int codCuenta = Util.validaParametro(accionweb.getParameter("codCuenta"), 0);
		  int codItem = Util.validaParametro(accionweb.getParameter("codItem"), 0);
		  String nomItem = Util.validaParametro(accionweb.getParameter("nomItem")+"", "");
		  
			  
		  List<Presw18DTO> listaPRESW18 = new ArrayList<Presw18DTO>();
          if(anno == 0){ // a�o actual
          	Date fecActual = new Date();		     
      		anno = 1900 + fecActual.getYear() ;
      
          }
          	
          listaPRESW18 = moduloPresupuesto.getListaEjecPorItemDetalle(nomTipo, codCuenta, anno, rutUsuario,dv,codItem);		
		  
		  Vector vec = new Vector();
			
		  for (Presw18DTO ss : listaPRESW18){
		       	String fecha = ss.getFecdoc()+"";
		       	String diaFecha = "";
		       	String mesFecha = "";
		       	String annoFecha= "";
             	if(fecha.length() < 8){
             		 diaFecha = "0" + fecha.substring(0,1);
				     mesFecha = fecha.substring(1,3);
				     annoFecha = fecha.substring(3);
             	} else {
					diaFecha = fecha.substring(0,2);
					mesFecha = fecha.substring(2,4);
					annoFecha = fecha.substring(4);
             	}
				fecha = diaFecha+"/"+mesFecha+"/"+annoFecha;
			  Vector vec_datos = new Vector();
			  vec_datos.addElement(ss.getTipmov().trim() +ss.getNomtip().trim());
			  vec_datos.addElement(String.valueOf(ss.getNumdoc()));
			  vec_datos.addElement(fecha);
			  vec_datos.addElement(ss.getNompro().trim());
			  vec_datos.addElement(String.valueOf(ss.getValdoc()));
			  vec.addElement(vec_datos);
		  }
		 
		  moduloPresupuesto.agregaPlanDesarrolloExcel(accionweb.getReq(), vec, 0);
		}

	public void controlPlanIndicadoresVector(AccionWeb accionweb) throws Exception {
		  int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		  String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		  String nomTipo = Util.validaParametro(accionweb.getParameter("nomTipo")+"","");
		  String codMacro = Util.validaParametro(accionweb.getParameter("codMacro")+"", "");
		  int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		  int codCuenta = Util.validaParametro(accionweb.getParameter("codCuenta"), 0);
		  MathTool mathtool = new MathTool();	
		
		  List<Cocow36DTO> listaIndicadores = moduloPresupuesto.getListaIndicadores(rutUsuario, dv, codCuenta);	
		  accionweb.agregarObjeto("listaIndicadores", listaIndicadores);
		
	   	  
		  Vector vec = new Vector();
		  String fecha= "";	 
		  String diaFecha = "";
		  String mesFecha = "";
		  String annoFecha = "";
		  for (Cocow36DTO ss : listaIndicadores){
			    fecha = ss.getValnu2()+"";
				diaFecha = fecha.substring(6);
				mesFecha = fecha.substring(4,6);
				annoFecha = fecha.substring(0,4);
				fecha = diaFecha+"/"+mesFecha+"/"+annoFecha; 
			  Vector vec_datos = new Vector();
			  vec_datos.addElement(ss.getValalf().trim());
			  vec_datos.addElement(fecha);
			  vec_datos.addElement(ss.getResval());
			  vec.addElement(vec_datos);
		  }
	      	
          
		  moduloPresupuesto.agregaPlanDesarrolloExcel(accionweb.getReq(), vec,0);
		}
	
	public void controlPlanVerIndicadorVector(AccionWeb accionweb) throws Exception {
		  int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		  String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		  String indicador = Util.validaParametro(accionweb.getParameter("indicador")+"", "");
		  int codCuenta = Util.validaParametro(accionweb.getParameter("codCuenta"), 0);
		  String nomTipo = "";			 
		  
		  nomTipo = "VVI";
		  List<Cocow36DTO> listaValorIndicadores = new ArrayList<Cocow36DTO>();
		  listaValorIndicadores = moduloPresupuesto.getListaValorIndicadores(nomTipo, rutUsuario, dv, indicador, codCuenta);
	      
	   	  
		  Vector vec = new Vector();
		  String fecha= "";	 
		  String diaFecha = "";
		  String mesFecha = "";
		  String annoFecha = "";
		  long fechaAnt = 0;
		  for (Cocow36DTO ss : listaValorIndicadores){
			    fecha = String.valueOf(ss.getValnu2());
				diaFecha = fecha.substring(6);
				mesFecha = fecha.substring(4,6);
				annoFecha = fecha.substring(0,4);
				fecha = diaFecha+"/"+mesFecha+"/"+annoFecha; 
			  Vector vec_datos = new Vector();
			  if(fechaAnt != ss.getValnu2()){
			    vec_datos.addElement(fecha);
			    vec_datos.addElement(ss.getValalf());
			  } else{
				vec_datos.addElement("");
				vec_datos.addElement(""); 
			  }
			   vec_datos.addElement(ss.getResval());
			  vec.addElement(vec_datos);		  
					
             	
	 			fechaAnt = ss.getValnu2();
		  }      	
        
		  moduloPresupuesto.agregaPlanDesarrolloExcel(accionweb.getReq(), vec,0);
		}
	public void controlPlanHistorialVector(AccionWeb accionweb) throws Exception {
		  int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		  String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		  String indicador = Util.validaParametro(accionweb.getParameter("indicador")+"", "");
		  int codCuenta = Util.validaParametro(accionweb.getParameter("codCuenta"), 0);
		  String nomTipo = "";	
			
		  
	        nomTipo = "GIM";          		
			Collection<Presw19DTO> lista = null;
			PreswBean preswbean = new PreswBean(nomTipo, 0, 0, 0, 0, rutUsuario);
			preswbean.setDigide(dv);
			preswbean.setCoduni(codCuenta);
		//	preswbean.setAnopar(anno);
			List<Presw25DTO> listaPRESW25 = (List<Presw25DTO>) preswbean.consulta_presw25();
			String fecha = "";
			String diaFecha = "";
			String mesFecha = "";
			String annoFecha = "";
			Vector vec = new Vector();
			
		  for (Presw25DTO ss : listaPRESW25){
			    fecha = String.valueOf(ss.getPres01());
				diaFecha = fecha.substring(6);
				mesFecha = fecha.substring(4,6);
				annoFecha = fecha.substring(0,4);
				fecha = diaFecha+"/"+mesFecha+"/"+annoFecha; 
			    Vector vec_datos = new Vector();
			    vec_datos.addElement(ss.getComen1().trim());
			    vec_datos.addElement(fecha);
			 	vec_datos.addElement(ss.getComen2().trim());
				vec_datos.addElement(ss.getPres02()); 
			    vec_datos.addElement(ss.getComen3().trim());
			    vec_datos.addElement(ss.getPres03());
			    vec_datos.addElement(ss.getIndmod());
			    vec.addElement(vec_datos);	
		  }      	
      
		  moduloPresupuesto.agregaPlanDesarrolloExcel(accionweb.getReq(), vec,0);
		}
	
	public void controlPlanNotificacionVector(AccionWeb accionweb) throws Exception {
		  int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		  String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		  String indicador = Util.validaParametro(accionweb.getParameter("indicador")+"", "");
		  int codCuenta = Util.validaParametro(accionweb.getParameter("codCuenta"), 0);
		  String nomTipo = Util.validaParametro(accionweb.getParameter("nomTipo"), "");;	
		  NumberTool numbertool = new NumberTool();
			
		  	Collection<Presw19DTO> lista = null;
			PreswBean preswbean = new PreswBean(nomTipo, 0, 0, 0, 0, rutUsuario);
			preswbean.setDigide(dv);
			preswbean.setCoduni(codCuenta);
			List<Presw25DTO> listaPRESW25 = (List<Presw25DTO>) preswbean.consulta_presw25();
          
			String fecha = "";
			String diaFecha = "";
			String mesFecha = "";
			String annoFecha = "";
			String horas = "";
			String hora = "";
			Vector vec = new Vector();
			
			
		  for (Presw25DTO ss : listaPRESW25){
			    fecha = String.valueOf(ss.getPres01());
				diaFecha = fecha.substring(6);
				mesFecha = fecha.substring(4,6);
				annoFecha = fecha.substring(0,4);
				fecha = diaFecha+"/"+mesFecha+"/"+annoFecha; 
				horas = String.valueOf(ss.getPres02());
				
				if(horas.length() < 6)
					hora = "0" + horas.substring(0,1) + ":" + horas.substring(1,3)+":"+horas.substring(4,5);
				else
					hora = horas.substring(0,2) + ":" + horas.substring(2,4)+":"+horas.substring(4,6);
			
			    Vector vec_datos = new Vector();
			    vec_datos.addElement(fecha);
			 	vec_datos.addElement(hora);
				vec_datos.addElement(ss.getComen1().trim()); 
			    vec_datos.addElement(numbertool.format(ss.getRutide())+"-"+ss.getDigide()+" " + ss.getComen2().trim());
			    vec_datos.addElement(ss.getComen3().trim());
			    vec.addElement(vec_datos);			    
			    
		  }      	
    
		  moduloPresupuesto.agregaPlanDesarrolloExcel(accionweb.getReq(), vec,0);
		}
	
	
	public void cargaAperturaCuentas(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		String titulo = Util.validaParametro(accionweb.getParameter("titulo"),"");
		String tipo = Util.validaParametro(accionweb.getParameter("tipo"),"");
		String fechaActual = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
		accionweb.agregarObjeto("fechaActual", fechaActual);
      	 List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO> )accionweb.getSesion().getAttribute("listaSolicitudesCuenta");
    	 if(listaSolicitudes != null && listaSolicitudes.size()> 0){
    		 accionweb.getSesion().removeAttribute("listaSolicitudesCuenta");
    		 accionweb.getSesion().setAttribute("listaSolicitudesCuenta", null);
    			
    	 }
    
    	 switch (opcionMenu){
    	 case 2: { // listado de modificaci�n
    		 listaSolicitudes = moduloPresupuesto.getListaPresw18("SCM", rutUsuario, dv);
    		 accionweb.agregarObjeto("listaSolicitudesCuenta", listaSolicitudes);
    		 accionweb.agregarObjeto("tituloIr", "Ir a Solicitud de Cuenta");
    		 if(listaSolicitudes != null && listaSolicitudes.size() > 0)
    			 accionweb.agregarObjeto("hayDatoslista", 1);
    		 break;
    	 }
    	 case 3:{ //listado  aprobaci�n de cuentas
    		 listaSolicitudes = moduloPresupuesto.getListaPresw18("AUP", rutUsuario, dv);
    		 accionweb.agregarObjeto("listaSolicitudesCuenta", listaSolicitudes);
    		 if(listaSolicitudes != null && listaSolicitudes.size() > 0)
    			 accionweb.agregarObjeto("hayDatoslista", 1);
    		 accionweb.agregarObjeto("tituloIr", "Ir a Solicitud de Cuenta");
    		 accionweb.agregarObjeto("tituloDetalle1", "Aprobar");
    		 accionweb.agregarObjeto("tituloDetalle2", "Rechazar");
    		 accionweb.agregarObjeto("opcion", "5"); // aprobaci�n
    		 accionweb.agregarObjeto("opcion2", "6"); // rechazo
    		 break;
    		}
    	 case 4:{ // listado aprobaci�n DIPRES
    		 listaSolicitudes = moduloPresupuesto.getListaPresw18("RSC", rutUsuario, dv);
    		 accionweb.agregarObjeto("listaSolicitudesCuenta", listaSolicitudes);
    		 if(listaSolicitudes != null && listaSolicitudes.size() > 0)
    			 accionweb.agregarObjeto("hayDatoslista", 1);
    		 accionweb.agregarObjeto("tituloIr", "Ir a Solicitud de Cuenta");
    		 //accionweb.agregarObjeto("tituloDetalle1", "Aprobar");
    		 //accionweb.agregarObjeto("tituloDetalle2", "Rechazar");
    		 //accionweb.agregarObjeto("opcion", "7"); // aprobaci�n
    		 //accionweb.agregarObjeto("opcion2", "8"); // rechazo
    		 break;
    	 }
    	 case 5:{ //solicitud de asignacion de cuentas
    		 listaSolicitudes = moduloPresupuesto.getListaPresw18("VCA", rutUsuario, dv);
    		 accionweb.agregarObjeto("listaCuenta", listaSolicitudes);
    		 if(listaSolicitudes != null && listaSolicitudes.size()> 0){
        		 accionweb.getSesion().removeAttribute("listaSolicitudesAsignacion");
        		 accionweb.getSesion().setAttribute("listaSolicitudesAsignacion", null);
        			
        	 }
    		 break;
    		}
    	 case 7:{ // listado de consulta de solicitudes de creaci�n de cuentas
    		 listaSolicitudes = moduloPresupuesto.getListaPresw18("CSC", rutUsuario, dv);
    		 accionweb.agregarObjeto("listaSolicitudesCuenta", listaSolicitudes);
    		 if(listaSolicitudes != null && listaSolicitudes.size() > 0)
    			 accionweb.agregarObjeto("hayDatoslista", 1);
    		 accionweb.agregarObjeto("tituloIr", "Ir a Solicitud de Cuenta");
    		 break;
    	 }
    	 case 8:{ // listado de recepci�n de asignaci�n de cuentas
    		 listaSolicitudes = moduloPresupuesto.getListaPresw18("PSA", rutUsuario, dv);
    		 accionweb.agregarObjeto("listaSolicitudesAsignacion", listaSolicitudes);
    		 if(listaSolicitudes != null && listaSolicitudes.size() > 0)
    			 accionweb.agregarObjeto("hayDatoslista", 1);
    		 accionweb.agregarObjeto("tituloIr", "Ir a Solicitud Asignaci�n");
    		 accionweb.agregarObjeto("tituloDetalle1", "Aprobar");
    		 accionweb.agregarObjeto("tituloDetalle2", "Rechazar");
    		 accionweb.agregarObjeto("opcion", "9"); // aprobaci�n
    		 accionweb.agregarObjeto("opcion2", "10"); // rechazo
    		 break;
    	 }
    	 case 9:{ // listado de consulta de asignaci�n de cuentas
    		 listaSolicitudes = moduloPresupuesto.getListaPresw18("CSA", rutUsuario, dv);
    		 accionweb.agregarObjeto("listaSolicitudesAsignacion", listaSolicitudes);
    		 if(listaSolicitudes != null && listaSolicitudes.size() > 0)
    			 accionweb.agregarObjeto("hayDatoslista", 1);
    		 accionweb.agregarObjeto("tituloIr", "Ir a Solicitud Asignaci�n");
    		 break;
    	 }
    	 case 10:{ // listado de Historial de solicitudes a DIPRES
    		 listaSolicitudes = moduloPresupuesto.getListaPresw18("VCA", rutUsuario, dv);
    		 int codUnidad = Util.validaParametro(accionweb.getParameter("codUnidad"), 0);
    		 int fechaInicio = Util.validaParametro(accionweb.getParameter("fechaInicio"), 0);
    		 int fechaTermino = Util.validaParametro(accionweb.getParameter("fechaTermino"), 0);
    		 String fechaIni = Util.validaParametro(accionweb.getParameter("fechaIni"),"");
    		 String fechaTer = Util.validaParametro(accionweb.getParameter("fechaTer"),"");
    		
    		 if(fechaInicio > 0 && fechaTermino == 0 ){
 				DateTool fecActual = new DateTool();
 				String dia = fecActual.getDay()+"";
 				if(dia.length() == 1)
 					dia = "0" + dia;
 				String mes = (fecActual.getMonth()+1)+"";
 				if(mes.length()== 1)
 					mes = "0" + mes;
 				fechaTermino = Integer.parseInt(String.valueOf(fecActual.getYear()) + mes + dia);
 				fechaTer = dia + "/" + mes + "/" + String.valueOf(fecActual.getYear());
 				
 			}
    		 accionweb.agregarObjeto("listaCuenta", listaSolicitudes);
    		 listaSolicitudes = moduloPresupuesto.getListaHistorial("LSV", rutUsuario, dv,codUnidad, fechaInicio, fechaTermino );
    		 accionweb.agregarObjeto("listaSolicitudesHistorial", listaSolicitudes);
    		 if(listaSolicitudes != null && listaSolicitudes.size() > 0)
    			 accionweb.agregarObjeto("hayDatoslista", 1);
    		 accionweb.agregarObjeto("tituloIr", "Ir a Solicitud Asignaci�n");
    		 accionweb.agregarObjeto("fechaIni", fechaIni);
    		 accionweb.agregarObjeto("fechaTer", fechaTer);
    		 break;
    	 }
     	 case 11:{ // listado de consulta de apertura de cuentas por aprobar DIPRES
    		 listaSolicitudes = moduloPresupuesto.getListaPresw18("SCD", rutUsuario, dv);
    		 accionweb.agregarObjeto("listaSolicitudesCuenta", listaSolicitudes);
    		 if(listaSolicitudes != null && listaSolicitudes.size() > 0)
    			 accionweb.agregarObjeto("hayDatoslista", 1);
    		 accionweb.agregarObjeto("tituloIr", "Ir a Solicitud de Cuenta");
    		 break;
    	 }
    	 }
    	int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control de planes; 2 = planes de desarrollo
	     
		accionweb.agregarObjeto("titulo", titulo);	
		accionweb.agregarObjeto("control", control);
		
		accionweb.agregarObjeto("tipo", tipo);
		accionweb.agregarObjeto("menuPresupSolicitudes", 1); // es para no mostrar el menu en ingreso
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		
		}
	
	 synchronized public void ejecutaAccion(AccionWeb accionweb) throws Exception {
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		String estadoFactura = Util.validaParametro(accionweb.getParameter("estadoFactura"),"");
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
	try{
		
		switch (tipo){
		case 5: case 6:{  // aprueba/rechaza responsable de cuenta
			String accion ="";
			if(tipo == 5)
				accion = "A"; // aprueba
			if(tipo == 6)
				accion = "R"; // rechaza
			this.rechazaAutCuenta(accionweb, "ARC", accion);
		break;
			
		}
	
		/*case 7:{// env�a a dipres
			this.rechazaAutCuenta(accionweb, "AFC",""); // env�a a DIPRES
		break;
			
		}*/
		case 8:{// rechaza equipo dipres
			String glosa1 = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "","");	
			
			boolean graba = moduloPresupuesto.saveRechazaCuenta(numDoc, rutUsuario, dv, "RFC", glosa1); // rechaza DIPRES
			if(graba)
				accionweb.agregarObjeto("mensaje", "Registr� exitosamente el Rechazo de Solicitud en DIPRES");
			else
				accionweb.agregarObjeto("mensaje", "No Registr� el Rechazo de Solicitud en DIPRES");
	
		break;
			
		}
		case 9: case 10:{// aprueba rechaza 
			String tipcue = "";
			if(tipo == 9)
				tipcue = "P";  // aprobada
			else
				tipcue = "X"; // rechazada
			int numCuenta = moduloPresupuesto.getApruebaRehazaAsignacion(accionweb.getReq(), rutUsuario, dv, "ARA", numDoc, tipcue); // rechaza DIPRES
			if(numCuenta >= 0)
				if(tipo == 9)
					accionweb.agregarObjeto("mensaje", "Registr� exitosamente la Aprobaci�n DIPRES");
				else
					accionweb.agregarObjeto("mensaje", "Registr� exitosamente el Rechazo DIPRES");
			else
				if(tipo == 9)
					accionweb.agregarObjeto("mensaje", "No Registr� la Aprobaci�n DIPRES");
				else
					accionweb.agregarObjeto("mensaje", "No Registr� el Rechazo DIPRES");
	
		break;
			
		}
		case 11:{// aprueba director presupuesto			
			this.confirmaCuentaDIR(accionweb, "GSC",""); // aprueba DIPRES
			
		break;
			
		}
		
		case 12:{// rechaza director presupuesto
			int numCuenta = moduloPresupuesto.getApruebaRehazaDIR(accionweb.getReq(), rutUsuario, dv, "RDC", numDoc); // rechaza DIPRES
			if(numCuenta > 0)
				accionweb.agregarObjeto("mensaje", "Registr� exitosamente el Rechazo DIPRES");
			else
				accionweb.agregarObjeto("mensaje", "No Registr� el Rechazo DIPRES");		
			
			
		break;
			
		}
		
		/*
		case 11:{// registra vale
			this.registraFacturacion(accionweb);
		break;
			
		}*/
	
		}
		this.cargaAperturaCuentas(accionweb);
	} catch (Exception e) {
		accionweb.agregarObjeto("mensaje", "No guardado rechazaAutCuenta.");
		accionweb.agregarObjeto("exception", e);
		e.printStackTrace();
	}
	
		
		
	}
	
	
	 synchronized public void rechazaAutCuenta(AccionWeb accionweb, String tippro, String accion) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);		
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
		try { 
		boolean graba = moduloPresupuesto.saveRecepciona(numdoc, rutUsuario,dv, tippro, accion);
	    if(graba){
	    	if(tipo == 5 || tipo == 1)
	    		accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la Aprobaci�n de la Autorizaci�n.");
	    	else
	    		accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el rechazo de la Autorizaci�n.");
	    } else
	    {
	    	if(tipo == 5 || tipo == 1)
	    		accionweb.agregarObjeto("mensaje", "No Grab� la Aprobaci�n de la Autorizaci�n.");
	    	else
	    		accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Autorizaci�n.");
	    }
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado rechazaAutCuenta.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		Thread.sleep(500);
		this.cargaAperturaCuentas(accionweb);
	   // this.cargaAutorizaFacturacion(accionweb);
		
	}
	
	 synchronized public void confirmaCuentaDIR(AccionWeb accionweb, String tippro, String accion) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);		
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
		try { 
		boolean graba = moduloPresupuesto.saveRecepciona(numdoc, rutUsuario,dv, tippro, accion);
	    if(graba)
	    		accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la Aprobaci�n de la Autorizaci�n.");
	    	else
	      		accionweb.agregarObjeto("mensaje", "No Grab� la Aprobaci�n de la Autorizaci�n.");
	    
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado rechazaAutCuenta.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		Thread.sleep(500);
		this.cargaAperturaCuentas(accionweb);
	 
		
	}
	public void verificaRut(AccionWeb accionweb) throws Exception {
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		String tipo = "VRF";
		List<Presw18DTO> listaRut = null;
		String nomResponsableCuenta = "";
		listaRut = moduloPresupuesto.getVerificaRutCuenta(tipo, rutnum, dvrut);
		for (Presw18DTO ss : listaRut){
			if(ss.getDesuni().trim().equals(""))
				accionweb.agregarObjeto("mensaje", "<span class=\"Estilo_Rojo\"> Este n�mero de RUT NO se encuentra registrado.</span>");
			else {
				nomResponsableCuenta = ss.getDesuni().trim();
			
				if(ss.getItedoc() == 1)
					accionweb.agregarObjeto("mensaje", " &nbsp;&nbsp;&nbsp;<span class=\"Estilo_Rojo\"> El RUT pertenece a " +nomResponsableCuenta +"tiene contrato NO vigente.</span>");
				else
					accionweb.agregarObjeto("mensaje", " &nbsp;&nbsp;" + nomResponsableCuenta );
			}
		}
		
			
			
		}
	public void cargaResultadoSolicitudCuenta(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutnum = Util.validaParametro(accionweb.getParameter("rut")+ "",0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dv"),"");
		String tipoIngreso = Util.validaParametro(accionweb.getParameter("tipoIngreso"), "");
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		
				
	 /*verifica rut */
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		String tipo = "VRF";
		List<Presw18DTO> listaRut = null;
		String nomResponsableCuenta = "";
		String nomTipoIngreso = "";
		if(tipoIngreso.trim().equals("I"))
			nomTipoIngreso = "Ingresador";
		else
			nomTipoIngreso = "Autorizador";
		listaRut = moduloPresupuesto.getVerificaRutCuenta(tipo, rutnum, dvrut);
		boolean ingresa = true;
	 	List<Cocow36DTO>  listaSolicitudes = (List<Cocow36DTO> )accionweb.getSesion().getAttribute("listaSolicitudesCuenta");
			 
		for (Presw18DTO ss : listaRut){
			if(ss.getDesuni().trim().equals(""))
				accionweb.agregarObjeto("mensajeSolicitud", "<span class=\"Estilo_Rojo\"> Este n�mero de RUT NO se encuentra registrado.</span>");
			else {
				nomResponsableCuenta = ss.getDesuni().trim();
			
				if(ss.getItedoc() == 1)
					accionweb.agregarObjeto("mensajeSolicitud", nomResponsableCuenta +" &nbsp;&nbsp;&nbsp;<span class=\"Estilo_Rojo\"> Este RUT tiene contrato NO vigente.</span>");
				else 
					accionweb.agregarObjeto("mensajeSolicitud", " &nbsp;&nbsp;&nbsp;");
					
			 	 if(listaSolicitudes != null && listaSolicitudes.size() > 0)
			 		 for (Cocow36DTO ls: listaSolicitudes){
			 			 if(ls.getValnu1() == rutnum)
			 				 ingresa = false;
			 		 }
			 	 else listaSolicitudes =  new ArrayList<Cocow36DTO>();
			}
			    if(ingresa){
			     moduloPresupuesto.getAgregaListaSolicitaCuenta(accionweb.getReq(),rutnum,dvrut, nomResponsableCuenta, tipoIngreso, nomTipoIngreso);
			     listaSolicitudes = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudesCuenta");
			    } else 			
			    	accionweb.agregarObjeto("mensajeSolicitud", " &nbsp;&nbsp;&nbsp;<span class=\"Estilo_Rojo\"> Este RUT ya se encuentra agregado.</span>");
			     accionweb.agregarObjeto("listaSolicitudesCuenta", listaSolicitudes);
					
			
			accionweb.agregarObjeto("opcionMenu", opcionMenu);
		}
	}
	public void cargaResultadoSolicitudAsignacion(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutnum = Util.validaParametro(accionweb.getParameter("rut")+ "",0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dv"),"");
		String tipoIngreso = Util.validaParametro(accionweb.getParameter("tipoIngreso"), "");
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		int codUnidad = Util.validaParametro(accionweb.getParameter("codUnidad"), 0);
		String nomUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"), "");
		
				
	 /*verifica rut */
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		String tipo = "VRF";
		List<Presw18DTO> listaRut = null;
		String nomResponsableCuenta = "";
		String nomTipoIngreso = "";
		if(tipoIngreso.trim().equals("I"))
			nomTipoIngreso = "Ingresador";
		else
			nomTipoIngreso = "Autorizador";
		listaRut = moduloPresupuesto.getVerificaRutCuenta(tipo, rutnum, dvrut);
		boolean ingresa = true;
	 	List<Cocow36DTO>  listaSolicitudes = (List<Cocow36DTO> )accionweb.getSesion().getAttribute("listaSolicitudesAsignacion");
			 
		for (Presw18DTO ss : listaRut){
			if(ss.getDesuni().trim().equals("")){
				accionweb.agregarObjeto("mensajeSolicitud", "<span class=\"Estilo_Rojo\"> Este n�mero de RUT NO se encuentra registrado.</span>");
				ingresa = false;
			} else {
				nomResponsableCuenta = ss.getDesuni().trim();
			
				if(ss.getItedoc() == 1){
					accionweb.agregarObjeto("mensajeSolicitud", nomResponsableCuenta +" &nbsp;&nbsp;&nbsp;<span class=\"Estilo_Rojo\"> Este RUT tiene contrato NO vigente.</span>");
					ingresa = false;
				} else 
					accionweb.agregarObjeto("mensajeSolicitud", " &nbsp;&nbsp;&nbsp;");
					
			 	 if(listaSolicitudes != null && listaSolicitudes.size() > 0)
			 		 for (Cocow36DTO ls: listaSolicitudes){
			 			 if((ls.getValnu1() == rutnum) && (ls.getValnu2() == codUnidad) )
			 				 ingresa = false;
			 		 }
			 	 else listaSolicitudes =  new ArrayList<Cocow36DTO>();
			}
		}
			    if(ingresa){
			    	// verifica si ya tiene asignada esta cuenta
			     listaRut = new ArrayList<Presw18DTO>();
			     tipo = "VLA";
			     listaRut = moduloPresupuesto.getVerificaAsignacionCuenta(tipo, rutnum, dvrut, codUnidad, tipoIngreso);
			     for (Presw18DTO ss : listaRut){
				    switch (ss.getItedoc()){
				    /*case 0:{ accionweb.agregarObjeto("mensajeSolicitud", " &nbsp;&nbsp;&nbsp;<span class=\"Estilo_Rojo\"> Este Usuario no est� asignado a la Cuenta </span>");
								ingresa = false;
								break;
				    }*/
				    case 1: { accionweb.agregarObjeto("mensajeSolicitud", " &nbsp;&nbsp;&nbsp;<span class=\"Estilo_Rojo\"> Este RUT est� asociado a la cuenta con el mismo nivel de acceso. </span>");
								ingresa = false;
					break;
				    	}
				    case 2: { accionweb.agregarObjeto("mensajeSolicitud", " &nbsp;&nbsp;&nbsp;<span class=\"Estilo_Rojo\"> Este RUT est� asociado a la cuenta con otro nivel de acceso, al registrar se modificar� el nivel de acceso. </span>");
					//ingresa = false;
					break;
				    }
					default :	accionweb.agregarObjeto("mensajeSolicitud", " &nbsp;&nbsp;&nbsp;");
						}
					}
                if(ingresa){
			     moduloPresupuesto.getAgregaListaSolicitaAsignacion(accionweb.getReq(),rutnum,dvrut, nomResponsableCuenta, tipoIngreso, nomTipoIngreso, codUnidad, nomUnidad);
			     listaSolicitudes = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudesAsignacion");
			   
                }
			     accionweb.agregarObjeto("opcionMenu", opcionMenu);
			    } else 			
			    	accionweb.agregarObjeto("mensajeSolicitud", " &nbsp;&nbsp;&nbsp;<span class=\"Estilo_Rojo\"> Este RUT ya se encuentra asociado a esta unidad .</span>");
			     
			    if(listaSolicitudes != null && listaSolicitudes.size() > 0)
			    	  accionweb.agregarObjeto("registra", 1);
			    accionweb.agregarObjeto("listaSolicitudesAsignacion", listaSolicitudes);
					
			
			
		
	}
	
	public void cargaDatosAprobacion(AccionWeb accionweb) throws Exception {
		int numDoc =  Util.validaParametro(accionweb.getParameter("numDoc"),0);
		String titulo = Util.validaParametro(accionweb.getParameter("titulo"),"");
		
		
		String fechaActual = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
		accionweb.agregarObjeto("fechaActual", fechaActual);
    		 
    	List<Cocof17DTO> listCocofBean = new ArrayList<Cocof17DTO>();
		listCocofBean = moduloPresupuesto.getListaSedeAlterada(accionweb);
	    accionweb.agregarObjeto("listCocofBean", listCocofBean);

	        
	    List<TipoUnidad> listTipoUnidad = new ArrayList<TipoUnidad>();
	    listTipoUnidad = moduloPresupuesto.getTipoUnidad();
	    accionweb.agregarObjeto("listTipoUnidad", listTipoUnidad);
	    
	    List<TipoPresupuesto> listTipoPresupuesto = new ArrayList<TipoPresupuesto>();
	    listTipoPresupuesto = moduloPresupuesto.getTipoPresupuesto();
	    accionweb.agregarObjeto("listTipoPresupuesto", listTipoPresupuesto);
	    
	    List<TipoCuenta> listTipoCuenta = new ArrayList<TipoCuenta>();
	    listTipoCuenta = moduloPresupuesto.getTipoCuenta();
	    accionweb.agregarObjeto("listTipoCuenta", listTipoCuenta);
	   	 		    
		accionweb.agregarObjeto("numDoc", numDoc);
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
		 
		    
		accionweb.agregarObjeto("titulo", titulo);	
		accionweb.agregarObjeto("control", control);
		
		accionweb.agregarObjeto("menuPresupSolicitudes", 1); // es para no mostrar el menu en ingreso
		accionweb.agregarObjeto("numMemo", "DP-SAC "+numDoc+"/"+fechaActual.substring(6));
	}
	
	public void cargaAprobacionDipres(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
			int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
			int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
			String titulo = Util.validaParametro(accionweb.getParameter("titulo"),"");
			String tipo = Util.validaParametro(accionweb.getParameter("tipo"),"");
			int numDoc =  Util.validaParametro(accionweb.getParameter("numDoc"),0);
			
			this.consultaCuenta(accionweb);
			this.cargaDatosAprobacion(accionweb);
			
			int control = Util.validaParametro(accionweb.getParameter("control"), 0);
			 
	    			   	 		    
			accionweb.agregarObjeto("titulo", titulo);	
			accionweb.agregarObjeto("control", control);
			accionweb.agregarObjeto("numDoc", numDoc);
			
			accionweb.agregarObjeto("tipo", tipo);
			accionweb.agregarObjeto("menuPresupSolicitudes", 1); // es para no mostrar el menu en ingreso
			accionweb.agregarObjeto("opcionMenu", opcionMenu);
			
	   	 	}		
	 synchronized public void apruebaCuentaApertura(AccionWeb accionweb) throws Exception {
		String tipoCuenta = Util.validaParametro(accionweb.getParameter("tipoCuenta"),"");
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numCuenta = Util.validaParametro(accionweb.getParameter("numCuenta"), 0);
		int valorCobro = Util.validaParametro(accionweb.getParameter("valorCobro"), 0);
		String nombreCuenta = Util.validaParametro(accionweb.getParameter("nombreCuenta"), "");
		boolean existe = false;
		String mensaje = "";
		
		
		
		int sedeUnidad = Util.validaParametro(accionweb.getParameter("sedeUnidad"), 1);
		String tipoUnidad = Util.validaParametro(accionweb.getParameter("tipoUnidad"), "");
		String ingresoDatos = Util.validaParametro(accionweb.getParameter("ingresoDatos"),"");
		String unidadMadre = Util.validaParametro(accionweb.getParameter("unidadMadre"), "");
		String tipoPresupuesto = Util.validaParametro(accionweb.getParameter("tipoPresupuesto"),"");
		
		
	    try {
		//this.consultaCuenta(accionweb);
		
		 // valida numero de cuenta no debe existir
		  existe = moduloPresupuesto.getVerificaCuenta("VEC", numCuenta);
	      if(existe){
	    	     this.cargaDatosAprobacion(accionweb);
	    	     accionweb.agregarObjeto("mensajeCuenta", "Este n�mero de Cuenta ya se encuentra registrado.");
	    	     mensaje +=  " EL n�mero de Cuenta ya se encuentra registrado.";
	    		 accionweb.agregarObjeto("mensaje", mensaje);
	    	     accionweb.agregarObjeto("sedeUnidad", sedeUnidad);
	    	     accionweb.agregarObjeto("tipoUnidad", tipoUnidad);
	    	     accionweb.agregarObjeto("ingresoDatos", ingresoDatos);
	    	     accionweb.agregarObjeto("unidadMadre", unidadMadre);
	    	     accionweb.agregarObjeto("tipoPresupuesto", tipoPresupuesto);
	    	     accionweb.agregarObjeto("tipoCuenta", tipoCuenta);
	    	     accionweb.agregarObjeto("valorCobro", valorCobro);
	    	     accionweb.agregarObjeto("codUnidad", numCuenta);
	    	     accionweb.agregarObjeto("descripcionUnidad", nombreCuenta);
	    	   
	    	      } else {
		    		 // valida numero de cuenta si debe existir
				  existe = moduloPresupuesto.getVerificaCuenta("VEC", valorCobro);
			      if(!existe){
			    	     this.cargaDatosAprobacion(accionweb);
			    	     accionweb.agregarObjeto("mensajeIngreso", "El n�mero de Cuenta a cobro no existe.");
			    	     mensaje +=  " EL n�mero de Cuenta a cobro NO se encuentra registrado.";
			    	     accionweb.agregarObjeto("mensaje", mensaje);
			    	     accionweb.agregarObjeto("sedeUnidad", sedeUnidad);
			    	     accionweb.agregarObjeto("tipoUnidad", tipoUnidad);
			    	     accionweb.agregarObjeto("ingresoDatos", ingresoDatos);
			    	     accionweb.agregarObjeto("unidadMadre", unidadMadre);
			    	     accionweb.agregarObjeto("tipoPresupuesto", tipoPresupuesto);
			    	     accionweb.agregarObjeto("tipoCuenta", tipoCuenta);
			    	     accionweb.agregarObjeto("valorCobro", valorCobro);
			    	     accionweb.agregarObjeto("codUnidad", numCuenta);
			    	     accionweb.agregarObjeto("descripcionUnidad", nombreCuenta);
			      } else {
		    	      
						int error = moduloPresupuesto.getRegistraAprobacionDipres(accionweb.getReq(), rutUsuario, dv, "AFC"); // autoriza equipo DIPRES
					if(error >= 0){
						accionweb.agregarObjeto("mensaje", "Env�o a Director DIPRES de solicitud de cuenta registrada.");
						accionweb.agregarObjeto("registra", 1);
					} else
						accionweb.agregarObjeto("mensaje", "Problemas al registrar Env�o a Director DIPRES de solicitud");
			      }
	      }
			this.cargaAperturaCuentas(accionweb);
	    }catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No registr� la Solicitud de Aprobaci�n DIPRES de Apertura de Cuentas.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		
	}
	  public void validaMemoApertura(AccionWeb accionweb)throws Exception {
		  String numMemo = Util.validaParametro(accionweb.getParameter("numMemo"), "");
		  boolean existe = false;
		  existe = moduloPresupuesto.getVerificaMemo("VMS", numMemo);
	      if(existe)
	    	  accionweb.agregarObjeto("mensaje", "<span class=\"Estilo_Rojo\"> Este n�mero de Memo ya se encuentra registrado.</span>");

	      else
	     	  accionweb.agregarObjeto("mensaje", "");

	    
	  }
	  public void validaCuentaApertura(AccionWeb accionweb)throws Exception {
		  int numCuenta = Util.validaParametro(accionweb.getParameter("numCuenta"), 0);
		  boolean existe = false;
		  existe = moduloPresupuesto.getVerificaCuenta("VEC", numCuenta);
	      if(existe)
	    	  accionweb.agregarObjeto("mensaje", "<span class=\"Estilo_Rojo\"> Este n�mero de Cuenta ya se encuentra registrado.</span>");

	      else
	     	  accionweb.agregarObjeto("mensaje", "");
 
	    
	  }
	  public void validaCuentaExistente(AccionWeb accionweb)throws Exception {
		  int numCuenta = Util.validaParametro(accionweb.getParameter("numCuenta"), 0);
		  boolean existe = false;
		  existe = moduloPresupuesto.getVerificaCuenta("VEC", numCuenta);
	      if(!existe)
	    	  accionweb.agregarObjeto("mensaje", "<span class=\"Estilo_Rojo\"> Este n�mero de Cuenta NO se encuentra registrado.</span>");

	      else
	     	  accionweb.agregarObjeto("mensaje", "");

	    
	  }
	  synchronized public void eliminaResultadoSolicitudCuenta(AccionWeb accionweb) throws Exception {
			int rutide = Util.validaParametro(accionweb.getParameter("rutide"), 0);
			int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
			
			   moduloPresupuesto.getEliminaListaSolicitaCuenta(accionweb.getReq());
			   accionweb.agregarObjeto("mensaje", "Se elimin� el Funcionario de la cuenta.");
			    	
			   List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudesCuenta");
			   accionweb.agregarObjeto("listaSolicitudesCuenta", listaSolicitudes); 
			   accionweb.agregarObjeto("opcionMenu", opcionMenu);
				
			}
	  synchronized public void eliminaResultadoSolicitudAsignacion(AccionWeb accionweb) throws Exception {
			int rutide = Util.validaParametro(accionweb.getParameter("rutide"), 0);
			int coduni = Util.validaParametro(accionweb.getParameter("coduni"), 0);
			int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
			
			   moduloPresupuesto.getEliminaListaSolicitaAsignacion(accionweb.getReq());
			   accionweb.agregarObjeto("mensaje", "Se elimin� el Funcionario de la cuenta.");
			    	
			   List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudesAsignacion");
			   accionweb.agregarObjeto("listaSolicitudesAsignacion", listaSolicitudes); 
	
				accionweb.agregarObjeto("opcionMenu", opcionMenu);
			}
		
	  synchronized public void registraApertura(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
			String numMemo = Util.validaParametro(accionweb.getParameter("numMemo"), "");
			int numCuenta = Util.validaParametro(accionweb.getParameter("numCuenta"), 0);
			String titulo = Util.validaParametro(accionweb.getParameter("titulo"),"");	
			int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
			int control = Util.validaParametro(accionweb.getParameter("control"), 0);
			String fechaMemo = Util.validaParametro(accionweb.getParameter("fechaMemo"),"");
			String rut_aut = Util.validaParametro(accionweb.getParameter("rut_aut"),"");
			int rutnumAut = Util.validaParametro(accionweb.getParameter("rutnumAut"), 0);
			String dvrutAut = Util.validaParametro(accionweb.getParameter("dvrutAut"), "");
			String nombreCuenta = Util.validaParametro(accionweb.getParameter("nombreCuenta"),"");
			String motivoApertura1 = Util.validaParametro(accionweb.getParameter("motivoApertura1"),"");
			String motivoApertura2 = Util.validaParametro(accionweb.getParameter("motivoApertura2"),"");
			String motivoApertura3 = Util.validaParametro(accionweb.getParameter("motivoApertura3"),"");
			String motivoApertura4 = Util.validaParametro(accionweb.getParameter("motivoApertura4"),"");
			int opcion = Util.validaParametro(accionweb.getParameter("opcion"), 0);
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
			int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
			String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
			String numMemoAnt = Util.validaParametro(accionweb.getParameter("numMemoAnt"), "");
			List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudesCuenta");
			String tipo = "VRF";
		
    		List<Presw18DTO> listaRut = null;    		
    		String nombre = "";
    		String mensaje = "";
			
			try {
				boolean existe = false;
				
				//* verifica numero de memo que no exista siempre y cuando se modifica no se necesita verificar si no hay cambio*/
				if(!numMemoAnt.trim().equals(numMemo.trim()) && !numMemo.trim().equals("")){
				existe = moduloPresupuesto.getVerificaMemo("VMS", numMemo);
			    if(existe){
			    	mensaje = " El n�mero de Memo ya se encuentra registrado.";
    	            accionweb.agregarObjeto("mensajeMemo", "Este n�mero de Memo ya se encuentra registrado.");

			    }
				}
				
		   		/*verifica rut autorizador*/
	    		listaRut = moduloPresupuesto.getVerificaRutCuenta(tipo, rutnumAut, dvrutAut);
	    		for (Presw18DTO ss : listaRut){
	    			if(ss.getDesuni().trim().equals("")){
	    				mensaje += " El n�mero de RUT Autorizador NO se encuentra registrado.";
	    	            accionweb.agregarObjeto("nomAut", "Este n�mero de RUT NO se encuentra registrado.");
                        existe = true;
	    			} else 
	    				nombre = ss.getDesuni().trim();
	    			
	    			if(ss.getItedoc() == 1){
	    				mensaje +=  " El n�mero de RUT Autorizador tiene contrato NO vigente.";
	    	            accionweb.agregarObjeto("nomAut", "El n�mero de RUT Autorizador tiene contrato NO vigente.");
	    	            existe = true;
	    			} else 			    			
	    					accionweb.agregarObjeto("nomAut", " &nbsp;&nbsp;"); // asi al recargar no muestra el nombre
	    	    			
	    		}
	    		
	    		 // valida numero de cuenta
				  existe = moduloPresupuesto.getVerificaCuenta("VEC", numCuenta);
			      if(existe){
			    	     accionweb.agregarObjeto("mensajeCuenta", "Este n�mero de Cuenta ya se encuentra registrado.");
			    	     mensaje +=  " EL n�mero de Cuenta ya se encuentra registrado.";
			      } 
			      
			      
	    	    // valida rut responsable
		    	
	    		listaRut = moduloPresupuesto.getVerificaRutCuenta(tipo, rutnum, dvrut);
	    		for (Presw18DTO ss : listaRut){
	    			if(ss.getDesuni().trim().equals("")){
	    				mensaje += " El n�mero de RUT Autorizador NO se encuentra registrado.";
	    	            accionweb.agregarObjeto("nomRes", "Este n�mero de RUT NO se encuentra registrado.");
                        existe = true;
	    			} else 
	    				nombre = ss.getDesuni().trim();
	    			
	    			if(ss.getItedoc() == 1){
	    				mensaje += " El n�mero de RUT Autorizador tiene contrato NO vigente. ";
	    	            accionweb.agregarObjeto("nomRes", "El n�mero de RUT Autorizador tiene contrato NO vigente.");
	    	            existe = true;
	    			} else 			    			
	    					accionweb.agregarObjeto("nomRes", " &nbsp;&nbsp;"); // asi al recargar no muestra el nombre
	    	    			
	    		} 
			
			 	   if(!existe){					
			 			    if(opcionMenu == 2)
			 			    	tipo = "MSP";
			 			   if(opcionMenu == 1)
			 			    	tipo = "GSP";
							int graba = moduloPresupuesto.getRegistraCuenta(accionweb.getReq(), rutUsuario, dv, tipo, numDoc);
							if(graba > 0) {
										accionweb.agregarObjeto("mensaje", "Registr� exitosamente la Solicitud de Apertura de Cuentas.");
										accionweb.agregarObjeto("registra", 1);
										this.cargarPresupSolicitudes(accionweb);
												
								} else
									accionweb.agregarObjeto("mensaje", "No registr� la Solicitud de Apertura de Cuentas.");
				    	   } else if(!mensaje.trim().equals("")){
				    		   			accionweb.agregarObjeto("mensaje", mensaje);
										accionweb.agregarObjeto("numMemo", numMemo);
										if(numCuenta > 0)
											accionweb.agregarObjeto("numCuenta", numCuenta);
										accionweb.agregarObjeto("titulo", titulo);	
										accionweb.agregarObjeto("menuPresupSolicitudes", 1); // es para no mostrar el menu en ingreso
										accionweb.agregarObjeto("opcionMenu", opcionMenu);
										accionweb.agregarObjeto("control", control);
										accionweb.agregarObjeto("fechaActual", fechaMemo);
										accionweb.agregarObjeto("rut_aut", rut_aut);
										accionweb.agregarObjeto("rutnumAut", rutnumAut);
										accionweb.agregarObjeto("dvrutAut", dvrutAut);
										accionweb.agregarObjeto("rut_aux", rut_aux);
										accionweb.agregarObjeto("rutnum", rutnum);
										accionweb.agregarObjeto("dvrut", dvrut);
										accionweb.agregarObjeto("nombreCuenta", nombreCuenta);
										accionweb.agregarObjeto("motivoApertura1", motivoApertura1);
										accionweb.agregarObjeto("motivoApertura2", motivoApertura2);
										accionweb.agregarObjeto("motivoApertura3", motivoApertura3);
										accionweb.agregarObjeto("motivoApertura4", motivoApertura4);
										accionweb.agregarObjeto("listaSolicitudesCuenta", listaSolicitudes);
										accionweb.agregarObjeto("opcion", opcion);
				    	   }
		
			   
			}catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No registr� la Solicitud de Apertura de Cuentas.");
				accionweb.agregarObjeto("titulo", titulo);	
				accionweb.agregarObjeto("control", control);
				accionweb.agregarObjeto("tipo", tipo);
				accionweb.agregarObjeto("menuPresupSolicitudes", 1); // es para no mostrar el menu en ingreso
				accionweb.agregarObjeto("opcionMenu", 1);
				accionweb.agregarObjeto("exception", e.toString());
				e.printStackTrace();
			}
				
		}
	  synchronized public void registraAsignacion(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
			String numMemo = Util.validaParametro(accionweb.getParameter("numMemo"), "");
			String titulo = Util.validaParametro(accionweb.getParameter("titulo"),"");	
			int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
			int control = Util.validaParametro(accionweb.getParameter("control"), 0);
			String fechaMemo = Util.validaParametro(accionweb.getParameter("fechaMemo"),"");
			String comentario = Util.validaParametro(accionweb.getParameter("comentario"),"");
			int opcion = Util.validaParametro(accionweb.getParameter("opcion"), 0);
			//String numMemoAnt = Util.validaParametro(accionweb.getParameter("numMemoAnt"), "");
			List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudesAsignacion");
			String tipo = "GAC";
		
    		List<Presw18DTO> listaRut = null;    		
    		String nombre = "";
    		String mensaje = "";
			
			try {
				boolean existe = false;
				
				
				//* verifica numero de memo que no exista siempre y cuando se modifica no se necesita verificar si no hay cambio*/
				// se agregar� cuando se agregue el proceso por parte de PP
				//if(!numMemoAnt.trim().equals(numMemo.trim()) && !numMemo.trim().equals("")){
				
				
				existe = moduloPresupuesto.getVerificaMemo("VMS", numMemo);
			    if(existe){
			    	mensaje = " El n�mero de Memo ya se encuentra registrado.";
    	            accionweb.agregarObjeto("mensajeMemo", "Este n�mero de Memo ya se encuentra registrado.");

			    }
				//}
				
			
			 	   if(!existe){					
			 				int graba = moduloPresupuesto.getRegistraAsignacion(accionweb.getReq(), rutUsuario, dv, tipo);
			 				this.cargaAperturaCuentas(accionweb);
							if(graba > 0) {
									accionweb.agregarObjeto("mensaje", "Registr� exitosamente la Solicitud de Asignaci�n de Cuentas.");
									accionweb.agregarObjeto("registra", 1);
							} else
									accionweb.agregarObjeto("mensaje", "No registr� la Solicitud de Asignaci�n de Cuentas.");
				    	   }
				if(!mensaje.trim().equals(""))
			    	accionweb.agregarObjeto("mensaje", mensaje);
				//accionweb.agregarObjeto("numMemo", numMemo);
			
				accionweb.agregarObjeto("titulo", titulo);	
				accionweb.agregarObjeto("menuPresupSolicitudes", 1); // es para no mostrar el menu en ingreso
				accionweb.agregarObjeto("opcionMenu", opcionMenu);
				accionweb.agregarObjeto("control", control);
				accionweb.agregarObjeto("fechaActual", fechaMemo);
				//accionweb.agregarObjeto("comentario", comentario);
				//accionweb.agregarObjeto("listaSolicitudesAsignacion", listaSolicitudes);
				accionweb.agregarObjeto("opcion", opcion);
				
		
			   
			}catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No registr� la Solicitud de Asignaci�n de Cuentas.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
				
		}
		public void consultaCuenta(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("opcion"), 0);
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			
			int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
			int control = Util.validaParametro(accionweb.getParameter("control"), 0);
			switch(opcionMenu){
			case 3: { // aprobaci�n responsable de cuenta
				accionweb.agregarObjeto("opcion", 5); // aprobar
				accionweb.agregarObjeto("opcion2", 6); // rechazar
				break;
			}
			case 4: { // aprobaci�n equipo DIPRES
				accionweb.agregarObjeto("opcion", 7); // aprobar
				accionweb.agregarObjeto("opcion2", 8); // rechazar
				break;
			}
			case 11: { // aprobaci�n DIPRES
				accionweb.agregarObjeto("opcion", 11); // aprobar
				accionweb.agregarObjeto("opcion2", 12); // rechazar
				break;
			}
			}
			Presw19DTO preswbean19DTO = new Presw19DTO();
			if(opcionMenu == 11)
				preswbean19DTO.setTippro("DSE"); // para confirmar en DIPRES
			else
				preswbean19DTO.setTippro("DSP");
			preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
			preswbean19DTO.setDigide(dv);
			preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
			
			
			String tituloDetalle1 = "";
			String pagina = "";
			String memorandum = "";
		    long rutIngresador = 0;
		    String dvIngresador = "";
		    String nombreIngresador = "";
		    long rutAutorizador = 0;
		    String dvAutorizador = "";
		    String nombreAutorizador = "";
		    long codUnidad = 0;
		    String descripcionUnidad = "";
		    long rutResponsable = 0;
		    String dvResponsable = "";
		    String nombreResponsable = "";
		    String motivoApertura1 = "";
		    String motivoApertura2 = "";
		    String motivoApertura3 = "";
		    String motivoApertura4 = "";
		    String estadoSolicitud = "";
		    String titulo = "";
		    long fecha = 0;
		    long codUniAsignada = 0;
		    String descUniAsignada = "";
		    long codSedeUnidad = 0;
		    String nomSede = "";
		    String tipoUnidad = "";
		    String ingresoDatos = "";
		    String unidadMadre = "";
		    String tipoPresupuesto = "";
		    long ingresoValorCobro = 0;
		    String tipoCuenta = "";
		    
		    
		    String readonly = "readonly";
			

			
	/*capturar los datos */
			
			if(tipo == 2) // lista de cuentas para consultar
			{	titulo = "Modifica Solicitud de Cuenta";
				tituloDetalle1 = "Modificar";
				readonly = "";
				}
			if(tipo == 3) // lista de cuentas para autorizar
				{
				titulo = "Aprobaci�n Responsable de la Cuenta";
				tituloDetalle1 = "Aprobar";		 
				}
			if(tipo == 5 || tipo == 6) // lista de cuentas para consulta
				{
				titulo = "Consulta ";
				tituloDetalle1 = "Consultar";
				}
			if(tipo == 4) // lista de cuentas para Recepci�n de facturas en Finanzas
				{
				titulo = "Aprobaci�n Equipo DIPRES";
				tituloDetalle1 = "Aprobaci�n Equipo DIPRES";
				}
			if(tipo == 7) // lista de solicitudes de creaci�n de cuentas
			{
			titulo = "Consulta solicitudes de Creaci�n de Cuentas";
			tituloDetalle1 = "Consulta Solicitud de creaci�n de Cuenta";
			}
			if(tipo == 11) // lista de solicitudes para confirmaci�n de cuentas
			{
			titulo = "Confirmaci�n Solicitud de Creaci�n de Cuenta en DIPRES";
			tituloDetalle1 = "Consulta Solicitud de creaci�n de Cuenta";
			}
		
			List<Cocow36DTO>  listaSolicitudesCuenta = new ArrayList<Cocow36DTO>();
			List<Cocow36DTO>  listaHistorial = new ArrayList<Cocow36DTO>();
		
			List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
			CocowBean cocowBean = new CocowBean();
			listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
			//System.out.println("listCocow36DTO : "+ listCocow36DTO.size() );
			String afecto = "";
			   if(accionweb.getSesion().getAttribute("listaSolicitudesCuenta") != null){
			    	accionweb.getSesion().setAttribute("listaSolicitudesCuenta", null);
			    	accionweb.getSesion().removeAttribute("listaSolicitudesCuenta");
			    }
			    if(accionweb.getSesion().getAttribute("listaHistorial") != null){
			    	accionweb.getSesion().setAttribute("listaHistorial", null);
			    	accionweb.getSesion().removeAttribute("listaHistorial");
			    }
		    if(listCocow36DTO != null && listCocow36DTO.size() >0 ){
		    
		    for(Cocow36DTO ss: listCocow36DTO){	   		 
		    	    
		    	if(ss.getNomcam().trim().equals("MEMSOL"))
		    		memorandum = ss.getValalf();	
		  
		    	if(ss.getNomcam().trim().equals("RUTING"))
		    		rutIngresador = ss.getValnu1();
		    		
		     	if(ss.getNomcam().trim().equals("DIGING"))
		     		dvIngresador = ss.getValalf();
		     	
		     	if(ss.getNomcam().trim().equals("NOMING"))
		     		nombreIngresador = ss.getValalf();
		     	
		     	if(ss.getNomcam().trim().equals("RUTAUT"))
		     		rutAutorizador = ss.getValnu1();  
		     	
		     	if(ss.getNomcam().trim().equals("DIGAUT"))
		     		dvAutorizador = ss.getValalf().trim();   
		     	
	            if(ss.getNomcam().trim().equals("NOMAUT"))
	            	nombreAutorizador = ss.getValalf();
	            
	            if(ss.getNomcam().trim().equals("CODUNI"))
	            	codUnidad = ss.getValnu1();
	            
	            if(ss.getNomcam().trim().equals("DESUNI"))
	            	descripcionUnidad = ss.getValalf().trim();
	            
	            if(ss.getNomcam().trim().equals("RUTRES"))
	            	rutResponsable = ss.getValnu1();
		     	
	            if(ss.getNomcam().trim().equals("DIGRES"))
	            	dvResponsable = ss.getValalf().trim();
	            
	            if(ss.getNomcam().trim().equals("NOMRES"))
	            	nombreResponsable = ss.getValalf().trim();
	            
	            if(ss.getNomcam().trim().equals("MOTAP1"))
	            	motivoApertura1 = ss.getValalf().trim();
	            
	            if(ss.getNomcam().trim().equals("MOTAP2"))
	            	motivoApertura2 = ss.getValalf().trim();
	            
	            if(ss.getNomcam().trim().equals("MOTAP3"))
	            	motivoApertura3 = ss.getValalf().trim();
	            
	            if(ss.getNomcam().trim().equals("MOTAP4"))
	            	motivoApertura4 = ss.getValalf().trim();
	            
	            if(ss.getNomcam().trim().equals("ESTADO"))
	            	estadoSolicitud = ss.getValalf().trim();
	            
	            if(ss.getNomcam().trim().equals("FECSOL"))
	            	fecha = ss.getValnu1();
	                 
	            if(ss.getNomcam().trim().equals("DETACC")){
		             	// System.out.println("DETACC");
	            	Cocow36DTO cocow36DTO = new Cocow36DTO();
	            	cocow36DTO.setValnu1(ss.getValnu1()); // rut acceso
	            	cocow36DTO.setValalf(ss.getValalf().trim()); // digito verificador
	            	cocow36DTO.setAccion(ss.getAccion()); // accion
	            	cocow36DTO.setResval(ss.getResval()); // funcionario
	            	listaSolicitudesCuenta.add(cocow36DTO);
	      
	            }
	       
	            if(ss.getNomcam().trim().equals("HISTORIAL")){
	            	// System.out.println("HISTORIAL");
	            	Cocow36DTO cocow36DTO = new Cocow36DTO();
	            	cocow36DTO.setValnu1(ss.getValnu1()); // es la fecha, 
	            	cocow36DTO.setValalf(ss.getValalf().trim()); // nombre del funcionario
	            	cocow36DTO.setResval(ss.getResval()); // comentario
	               	listaHistorial.add(cocow36DTO);
	            }
	         
	            
	            if(opcionMenu == 11){
	            	  if(ss.getNomcam().trim().equals("CODUNX"))
	            		  codUniAsignada = ss.getValnu1();
	            	  if(ss.getNomcam().trim().equals("DESUNIX"))
	            		  descUniAsignada = ss.getValalf().trim();
	            	  if(ss.getNomcam().trim().equals("CODSED")){
	            		  codSedeUnidad = ss.getValnu1();
	            		  nomSede = ss.getValalf();
	            	  }
	            	  if(ss.getNomcam().trim().equals("OPEREM"))
	            		  tipoUnidad = ss.getValalf().trim();
	            	  if(ss.getNomcam().trim().equals("INGDAT"))
	            		  ingresoDatos = ss.getValalf().trim();
	            	  if(ss.getNomcam().trim().equals("CASING"))
	            		  unidadMadre = ss.getValalf().trim();
	            	  if(ss.getNomcam().trim().equals("INDAUT"))
	            		  tipoPresupuesto = ss.getValalf().trim();
	            	  if(ss.getNomcam().trim().equals("CODAG2"))
	            		  ingresoValorCobro = ss.getValnu1();
	            	  if(ss.getNomcam().trim().equals("TIPCTA"))
	            		  tipoCuenta = ss.getValalf().trim();
	            	
	            }
		    }
		  
		 
		   
		    if(listaSolicitudesCuenta != null && listaSolicitudesCuenta.size() > 0){
		    	accionweb.agregarObjeto("listaSolicitudesCuenta", listaSolicitudesCuenta);
		    	accionweb.getSesion().setAttribute("listaSolicitudesCuenta", listaSolicitudesCuenta);
		    }
		
		    if(listaHistorial != null && listaHistorial.size() > 0){
		    	accionweb.agregarObjeto("listaHistorial", listaHistorial);
		       	accionweb.getSesion().setAttribute("listaHistorial", listaHistorial);
		    }
		      
		   
		
		    accionweb.agregarObjeto("memorandum",memorandum);
		    accionweb.agregarObjeto("rutIngresador",rutIngresador);
		    accionweb.agregarObjeto("dvIngresador",dvIngresador);
		    accionweb.agregarObjeto("nombreIngresador",nombreIngresador);
		    accionweb.agregarObjeto("rutAutorizador",rutAutorizador);
		    accionweb.agregarObjeto("dvAutorizador",dvAutorizador);
		    accionweb.agregarObjeto("nombreAutorizador",nombreAutorizador);
		   	accionweb.agregarObjeto("codUnidad",codUnidad);
		    accionweb.agregarObjeto("descripcionUnidad",descripcionUnidad);
		    accionweb.agregarObjeto("rutResponsable",rutResponsable);
		    accionweb.agregarObjeto("dvResponsable",dvResponsable);
		    accionweb.agregarObjeto("nombreResponsable",nombreResponsable);
		    accionweb.agregarObjeto("motivoApertura1",motivoApertura1);
		    accionweb.agregarObjeto("motivoApertura2",motivoApertura2);
		    accionweb.agregarObjeto("motivoApertura3",motivoApertura3);
		    accionweb.agregarObjeto("motivoApertura4",motivoApertura4);
		    accionweb.agregarObjeto("estadoSolicitud",estadoSolicitud);
		    accionweb.agregarObjeto("fechaSolicitud",fecha);
			//accionweb.agregarObjeto("opcionMenu", tipo);
			accionweb.agregarObjeto("titulo", titulo);
			accionweb.agregarObjeto("control", control);
			accionweb.agregarObjeto("numDoc", numDoc);
			accionweb.agregarObjeto("opcionMenu", opcionMenu);
			
			
			if(opcionMenu == 11){
				String nomTipoUnidad = "";
				String nomTipoPresupuesto = "";
				String nomTipoCuenta = "";
			    List<TipoUnidad> listTipoUnidad = new ArrayList<TipoUnidad>();
			    listTipoUnidad = moduloPresupuesto.getTipoUnidad();
			    for(TipoUnidad ss: listTipoUnidad){
			    	if(ss.getCodigo().trim().equals(tipoUnidad))
			    		nomTipoUnidad = ss.getNombre();
			    }
  		 
					   
			    
			    List<TipoPresupuesto> listTipoPresupuesto = new ArrayList<TipoPresupuesto>();
			    listTipoPresupuesto = moduloPresupuesto.getTipoPresupuesto();
			    for(TipoPresupuesto ss: listTipoPresupuesto){
			    	if(ss.getCodigo().trim().equals(tipoPresupuesto))
			    		nomTipoPresupuesto = ss.getNombre();
			    }
					   
			    
			    List<TipoCuenta> listTipoCuenta = new ArrayList<TipoCuenta>();
			    listTipoCuenta = moduloPresupuesto.getTipoCuenta();
			    for(TipoCuenta ss: listTipoCuenta){
			    	if(ss.getCodigo().trim().equals(tipoCuenta))
			    		nomTipoCuenta = ss.getNombre();
			    }
			    
					accionweb.agregarObjeto("codUniAsignada", codUniAsignada);
					accionweb.agregarObjeto("descUniAsignada", descUniAsignada);
					accionweb.agregarObjeto("codSedeUnidad", codSedeUnidad);
					accionweb.agregarObjeto("nomSede", nomSede);
					accionweb.agregarObjeto("tipoUnidad", tipoUnidad);
					accionweb.agregarObjeto("ingresoDatos", (ingresoDatos.trim().equals("S"))?"SI":"NO");
					accionweb.agregarObjeto("unidadMadre", (unidadMadre.trim().equals("S"))?"SI":"NO");
					accionweb.agregarObjeto("tipoPresupuesto", tipoPresupuesto);
					accionweb.agregarObjeto("ingresoValorCobro", ingresoValorCobro);
					accionweb.agregarObjeto("tipoCuenta", tipoCuenta);
					accionweb.agregarObjeto("nomTipoUnidad", nomTipoUnidad);
					accionweb.agregarObjeto("nomTipoPresupuesto", nomTipoPresupuesto);
					accionweb.agregarObjeto("nomTipoCuenta", nomTipoCuenta);
					}
	
			
			accionweb.agregarObjeto("hayDatoslista", "1"); 
		    } else {
		    		accionweb.agregarObjeto("mensaje", "Lo sentimos, esta cuenta no existe.");
					pagina = "solMuestraListaCuenta.vm";
				}
			accionweb.agregarObjeto("menuPresupSolicitudes", 1); // es para mostrar el menu  de solicitudes

	
		}
		public void consultaAsignacion(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("opcion"), 0);
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			
			int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
			int control = Util.validaParametro(accionweb.getParameter("control"), 0);
			switch(opcionMenu){
			case 8: { // consulta asignacion  aprobaci�n  DIPRES
				accionweb.agregarObjeto("opcion", 1); // aprobar
				accionweb.agregarObjeto("opcion2", 2); // rechazar
				break;
			}
			case 4: { // aprobaci�n DIPRES
				accionweb.agregarObjeto("opcion", 7); // aprobar
				accionweb.agregarObjeto("opcion2", 8); // rechazar
				break;
			}
			}
			Presw19DTO preswbean19DTO = new Presw19DTO();
			preswbean19DTO.setTippro("DSA");
			preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
			preswbean19DTO.setDigide(dv);
			preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
			
			
			String tituloDetalle1 = "";
			String pagina = "";
			String memorandum = "";
		    long rutIngresador = 0;
		    String dvIngresador = "";
		    String nomIngresador = "";
		    String comentario = "";
		    long fecdoc = 0;
		    String estadoSolicitud = "";
		    String titulo = "";
		     
		    
		    String readonly = "readonly";
			

			
	/*capturar los datos */
			
			if(tipo == 8) // recepci�n de asignaci�n DIPRES
			{	titulo = "Recepci�n Asignaci�n DIPRES";
				tituloDetalle1 = "Recepci�n";
				readonly = "";
				}
			if(tipo == 9) // consulta asignaci�n
				{
				titulo = "Consulta Asignaci�n";
				tituloDetalle1 = "Consulta";		 
				}
			if(tipo == 10) // lista de cuentas para consulta
				{
				titulo = "Consulta Solicitud ";
				tituloDetalle1 = "Consultar";
				}
			if(tipo == 4) // lista de cuentas para Recepci�n de facturas en Finanzas
				{
				titulo = "Aprobaci�n DIPRES";
				tituloDetalle1 = "Aprobaci�n DIPRES";
				}
			if(tipo == 7) // lista de solicitudes de creaci�n de cuentas
			{
			titulo = "Consulta solicitudes de Creaci�n de Cuentas";
			tituloDetalle1 = "Consulta Solicitud de creaci�n de Cuenta";
			}

		
			List<Cocow36DTO>  listaSolicitudesAsignacion = new ArrayList<Cocow36DTO>();
			List<Cocow36DTO>  listaHistorial = new ArrayList<Cocow36DTO>();
		
			List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
			CocowBean cocowBean = new CocowBean();
			listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
			//System.out.println("listCocow36DTO : "+ listCocow36DTO.size() );
			String afecto = "";
		    if(listCocow36DTO != null && listCocow36DTO.size() >0 ){
		    
		    for(Cocow36DTO ss: listCocow36DTO){	   		 
		    	    
		    	if(ss.getNomcam().trim().equals("MEMSOL"))
		    		memorandum = ss.getValalf();	
		  
		    	if(ss.getNomcam().trim().equals("RUTING"))
		    		rutIngresador = ss.getValnu1();
		    		
		     	if(ss.getNomcam().trim().equals("DIGING"))
		     		dvIngresador = ss.getValalf();
		     	
		    	if(ss.getNomcam().trim().equals("NOMING"))
		    		nomIngresador = ss.getValalf();
		    	
		     	if(ss.getNomcam().trim().equals("COMENT"))
		     		comentario = ss.getValalf();
		     	
		     	if(ss.getNomcam().trim().equals("FECSOL"))
		     		fecdoc = ss.getValnu1();  
		     	
		     	if(ss.getNomcam().trim().equals("ESTSOL"))
		     		estadoSolicitud = ss.getValalf().trim();   
		     	
	                       
	             if(ss.getNomcam().trim().equals("DETACC")){
	            	// System.out.println("DETACC");
	            	Cocow36DTO cocow36DTO = new Cocow36DTO();
	            	cocow36DTO.setValnu1(ss.getValnu1()); // rut acceso
	            	cocow36DTO.setValalf(ss.getValalf().trim()); // digito verificador
	            	cocow36DTO.setResval(ss.getResval()); // nombre el acceso
	            	cocow36DTO.setValnu2(ss.getValnu2()); // cuenta
	            	cocow36DTO.setAccion(ss.getAccion()); // accion
	            	listaSolicitudesAsignacion.add(cocow36DTO);
	      
	            }
	       
	            if(ss.getNomcam().trim().equals("HISTORIAL")){
	            	// System.out.println("HISTORIAL");
	            	Cocow36DTO cocow36DTO = new Cocow36DTO();
	            	cocow36DTO.setValnu1(ss.getValnu1()); // es el rut 
	            	cocow36DTO.setValalf(ss.getValalf()); // digito rut
	            	cocow36DTO.setResval(ss.getResval()); // operaci�n
	               	listaHistorial.add(cocow36DTO);
	            }
	          
		    }
		  
		 
		   
		    if(listaSolicitudesAsignacion != null && listaSolicitudesAsignacion.size() > 0){
		    	accionweb.agregarObjeto("listaSolicitudesAsignacion", listaSolicitudesAsignacion);
		    	accionweb.getSesion().setAttribute("listaSolicitudesAsignacion", listaSolicitudesAsignacion);
		    }
		
		    if(listaHistorial != null && listaHistorial.size() > 0){
		    	accionweb.agregarObjeto("listaHistorial", listaHistorial);
		       	accionweb.getSesion().setAttribute("listaHistorial", listaHistorial);
		    }
		      
		
		
		    accionweb.agregarObjeto("memorandum",memorandum);
		    accionweb.agregarObjeto("rutIngresador",rutIngresador);
		    accionweb.agregarObjeto("dvIngresador",dvIngresador);
		    accionweb.agregarObjeto("nomIngresador", nomIngresador);
		    accionweb.agregarObjeto("comentario",comentario);
		    accionweb.agregarObjeto("fechaSolicitud",fecdoc);
		    accionweb.agregarObjeto("estadoSolicitud",estadoSolicitud);
		 	accionweb.agregarObjeto("opcionMenu", tipo);
			accionweb.agregarObjeto("titulo", titulo);
			accionweb.agregarObjeto("control", control);
			accionweb.agregarObjeto("numDoc", numDoc);
			accionweb.agregarObjeto("opcionMenu", opcionMenu);
		
	
			
			
	      	accionweb.agregarObjeto("hayDatoslista", "1"); 
		    } else {
		    		accionweb.agregarObjeto("mensaje", "Lo sentimos, esta Asignaci�n de cuenta no existe.");
					pagina = "solMuestraListaAsignacion.vm";
				}
			accionweb.agregarObjeto("menuPresupSolicitudes", 1); // es para mostrar el menu  de solicitudes

	
		}
		
		
		 synchronized public void apruebaAsignacion(AccionWeb accionweb) throws Exception {
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			String accion = "";
		try{
			
			if(tipo == 1)
					accion = "A"; // aprueba
			if(tipo == 2)
					accion = "R"; // rechaza
				this.rechazaAutCuenta(accionweb, "ARC", accion);
			} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado apruebaAsignacion.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		
			
			
		}
		
		public void imprimeCuenta(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		
			String nomIdentificador = "";
			long valorAPagar = 0;
			long rutnum = 0;
			String dvrut = "";
			String identificadorInterno = "";	
			long sede = 0;
			int identificador = 0; 
			
		    String nomcam = "";
		    long valnu1 = 0;
		    long valnu2 = 0;
		    String valalf = "";
		    String glosa1 = "";
		    String glosa2 = "";
		    String glosa3 = "";
		    String glosa4 = "";
		    String glosa5 = "";
		    String glosa6 = "";
		    String estadoFinal = "";
		    String nomSede = "";
		    String tipoPago = "";
		    String nomTipoPago = "";
			
		    Presw19DTO preswbean19DTO = new Presw19DTO();
			preswbean19DTO.setTippro("DSP");
			preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
			preswbean19DTO.setDigide(dv);
			preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
			
			
			String tituloDetalle1 = "";
			String pagina = "";
			String memorandum = "";
		    long rutIngresador = 0;
		    String dvIngresador = "";
		    String nombreIngresador = "";
		    long rutAutorizador = 0;
		    String dvAutorizador = "";
		    String nombreAutorizador = "";
		    long codUnidad = 0;
		    String descripcionUnidad = "";
		    long rutResponsable = 0;
		    String dvResponsable = "";
		    String nombreResponsable = "";
		    String motivoApertura1 = "";
		    String motivoApertura2 = "";
		    String motivoApertura3 = "";
		    String motivoApertura4 = "";
		    String estadoSolicitud = "";
		    String titulo = "";
		    long fecha = 0;
		 	String diaFecha = "";
        	String mesFecha = "";
        	String annoFecha = "";
        	String fechaSol = "";
		    
		    String readonly = "readonly";

		
			Vector  listaSolicitudesCuenta = new Vector();
			Vector listaHistorial = new Vector();
		
			List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
			CocowBean cocowBean = new CocowBean();
			listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
			//System.out.println("listCocow36DTO : "+ listCocow36DTO.size() );
			String afecto = "";
			   if(accionweb.getSesion().getAttribute("veclistaSolicitudesCuenta") != null){
			    	accionweb.getSesion().setAttribute("veclistaSolicitudesCuenta", null);
			    	accionweb.getSesion().removeAttribute("veclistaSolicitudesCuenta");
			    }
			    if(accionweb.getSesion().getAttribute("veclistaHistorial") != null){
			    	accionweb.getSesion().setAttribute("veclistaHistorial", null);
			    	accionweb.getSesion().removeAttribute("veclistaHistorial");
			    }
		    if(listCocow36DTO != null && listCocow36DTO.size() >0 ){
		    
		    for(Cocow36DTO ss: listCocow36DTO){	   		 
		    	    
		    	if(ss.getNomcam().trim().equals("MEMSOL"))
		    		memorandum = ss.getValalf();	
		  
		    	if(ss.getNomcam().trim().equals("RUTING"))
		    		rutIngresador = ss.getValnu1();
		    		
		     	if(ss.getNomcam().trim().equals("DIGING"))
		     		dvIngresador = ss.getValalf();
		     	
		     	if(ss.getNomcam().trim().equals("NOMING"))
		     		nombreIngresador = ss.getValalf();
		     	
		     	if(ss.getNomcam().trim().equals("RUTAUT"))
		     		rutAutorizador = ss.getValnu1();  
		     	
		     	if(ss.getNomcam().trim().equals("DIGAUT"))
		     		dvAutorizador = ss.getValalf().trim();   
		     	
	            if(ss.getNomcam().trim().equals("NOMAUT"))
	            	nombreAutorizador = ss.getValalf();
	            
	            if(ss.getNomcam().trim().equals("CODUNI"))
	            	codUnidad = ss.getValnu1();
	            
	            if(ss.getNomcam().trim().equals("DESUNI"))
	            	descripcionUnidad = ss.getValalf().trim();
	            
	            if(ss.getNomcam().trim().equals("RUTRES"))
	            	rutResponsable = ss.getValnu1();
		     	
	            if(ss.getNomcam().trim().equals("DIGRES"))
	            	dvResponsable = ss.getValalf().trim();
	            
	            if(ss.getNomcam().trim().equals("NOMRES"))
	            	nombreResponsable = ss.getValalf().trim();
	            
	            if(ss.getNomcam().trim().equals("MOTAP1"))
	            	motivoApertura1 = ss.getValalf().trim();
	            
	            if(ss.getNomcam().trim().equals("MOTAP2"))
	            	motivoApertura2 = ss.getValalf().trim();
	            
	            if(ss.getNomcam().trim().equals("MOTAP3"))
	            	motivoApertura3 = ss.getValalf().trim();
	            
	            if(ss.getNomcam().trim().equals("MOTAP4"))
	            	motivoApertura4 = ss.getValalf().trim();
	            
	            if(ss.getNomcam().trim().equals("ESTADO"))
	            	estadoSolicitud = ss.getValalf().trim();
	            
	            if(ss.getNomcam().trim().equals("FECSOL"))
	            	fecha = ss.getValnu1();
	                 
	             if(ss.getNomcam().trim().equals("DETACC")){
	            	// System.out.println("DETACC");
	            	Vector vec3 = new Vector();
	            	vec3.addElement(ss.getValnu1()); // rut acceso
	            	vec3.addElement(ss.getValalf().trim()); // digito verificador
	            	vec3.addElement(ss.getAccion()); // accion
	            	vec3.addElement(ss.getResval()); // funcionario
	            	listaSolicitudesCuenta.add(vec3);
	      
	            }
	       
	            if(ss.getNomcam().trim().equals("HISTORIAL")){
	            	// System.out.println("HISTORIAL");
	            	String fechaHist = "";	           
	            	String valor = "";
	            	
	            	if(ss.getValnu1()> 0 ) {
	            		fechaHist = ss.getValnu1() + "";
	    				diaFecha = fechaHist.substring(6);
	    				mesFecha = fechaHist.substring(4,6);
	    				annoFecha = fechaHist.substring(0,4);
	    				valor = diaFecha+"/"+mesFecha+"/"+annoFecha;   
	            	} else {
	    				valor = ss.getValnu1()+"";
	            	}  
	            	Vector vec3 = new Vector();
	            	vec3.addElement(ss.getResval()); // es la fecha, 
	            	vec3.addElement(valor); // nombre del funcionario
	            	vec3.addElement(ss.getValalf().trim()); // comentario
	               	listaHistorial.addElement(vec3);
	            }
	          
		    }
		  
		 
		   
		    if(listaSolicitudesCuenta != null && listaSolicitudesCuenta.size() > 0){
		    	accionweb.agregarObjeto("veclistaSolicitudesCuenta", listaSolicitudesCuenta);
		    	accionweb.getSesion().setAttribute("veclistaSolicitudesCuenta", listaSolicitudesCuenta);
		    }
		
		    if(listaHistorial != null && listaHistorial.size() > 0){
		    	accionweb.agregarObjeto("veclistaHistorial", listaHistorial);
		       	accionweb.getSesion().setAttribute("veclistaHistorial", listaHistorial);
		    }
		      
		
			fechaSol = fecha + "";
			diaFecha = fechaSol.substring(6);
			mesFecha = fechaSol.substring(4,6);
			annoFecha = fechaSol.substring(0,4);
			fechaSol = diaFecha+"/"+mesFecha+"/"+annoFecha;  
		    accionweb.getSesion().setAttribute("memorandum",memorandum);
		    accionweb.getSesion().setAttribute("rutIngresador", moduloPresupuesto.formateoNumeroSinDecimales(Double.parseDouble(rutIngresador+"")) + "-" + dvIngresador);
		    accionweb.getSesion().setAttribute("nombreIngresador",nombreIngresador);
		    accionweb.getSesion().setAttribute("rutAutorizador", moduloPresupuesto.formateoNumeroSinDecimales(Double.parseDouble(rutAutorizador+"")) + "-" + dvAutorizador);
		    accionweb.getSesion().setAttribute("nombreAutorizador",nombreAutorizador);
		   	accionweb.getSesion().setAttribute("codUnidad",String.valueOf(codUnidad));
		    accionweb.getSesion().setAttribute("descripcionUnidad",descripcionUnidad);
		    accionweb.getSesion().setAttribute("rutResponsable", moduloPresupuesto.formateoNumeroSinDecimales(Double.parseDouble(rutResponsable+"")) + "-" + dvResponsable);
		    accionweb.getSesion().setAttribute("nombreResponsable",nombreResponsable);
		    accionweb.getSesion().setAttribute("motivoApertura1",motivoApertura1);
		    accionweb.getSesion().setAttribute("motivoApertura2",motivoApertura2);
		    accionweb.getSesion().setAttribute("motivoApertura3",motivoApertura3);
		    accionweb.getSesion().setAttribute("motivoApertura4",motivoApertura4);
		    accionweb.getSesion().setAttribute("estadoSolicitud",estadoSolicitud);
		    accionweb.getSesion().setAttribute("fechaSolicitud",fechaSol);
			accionweb.getSesion().setAttribute("titulo", "Solicitud de Cuenta");	
			accionweb.getSesion().setAttribute("titulo2", "Cuenta");
			accionweb.getSesion().setAttribute("numDoc", String.valueOf(numDoc));
			
			
	
			
			
	      	accionweb.agregarObjeto("hayDatoslista", "1"); 
		    } else {
		    		accionweb.agregarObjeto("mensaje", "Lo sentimos, esta cuenta no existe.");
					pagina = "solMuestraListaCuenta.vm";
				}
			accionweb.agregarObjeto("menuPresupSolicitudes", 1); // es para mostrar el menu  de solicitudes
	 
	           
			 
		}
		public void imprimeAsignacion(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		
			String nomIdentificador = "";
			long valorAPagar = 0;
			long rutnum = 0;
			String dvrut = "";
			String identificadorInterno = "";	
			long sede = 0;
			int identificador = 0; 
			
		    String nomcam = "";
		    long valnu1 = 0;
		    long valnu2 = 0;
		    String valalf = "";
		    String glosa1 = "";
		    String glosa2 = "";
		    String glosa3 = "";
		    String glosa4 = "";
		    String glosa5 = "";
		    String glosa6 = "";
		    String estadoFinal = "";
		    String nomSede = "";
		    String tipoPago = "";
		    String nomTipoPago = "";
			
		    Presw19DTO preswbean19DTO = new Presw19DTO();
			preswbean19DTO.setTippro("DSA");
			preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
			preswbean19DTO.setDigide(dv);
			preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
			
			
			String tituloDetalle1 = "";
			String pagina = "";
			String memorandum = "";
		    long rutIngresador = 0;
		    String dvIngresador = "";
		    String nomIngresador = "";
		    long rutAutorizador = 0;
		    String dvAutorizador = "";
		    String nombreAutorizador = "";
		    long codUnidad = 0;
		    String comentario = "";
		    long rutResponsable = 0;
		    String dvResponsable = "";
		    String nombreResponsable = "";
		    String motivoApertura1 = "";
		    String motivoApertura2 = "";
		    String motivoApertura3 = "";
		    String motivoApertura4 = "";
		    String estadoSolicitud = "";
		    String titulo = "";
		    long fecdoc = 0;
	      	String fechaHist = "";
        	String diaFecha = "";
        	String mesFecha = "";
        	String annoFecha = "";
        	String fechaSol = "";
		    
		    
		    String readonly = "readonly";

				//System.out.println("listCocow36DTO : "+ listCocow36DTO.size() );
			String afecto = "";
			   if(accionweb.getSesion().getAttribute("veclistaSolicitudesCuenta") != null){
			    	accionweb.getSesion().setAttribute("veclistaSolicitudesCuenta", null);
			    	accionweb.getSesion().removeAttribute("veclistaSolicitudesCuenta");
			    }
			    if(accionweb.getSesion().getAttribute("veclistaHistorial") != null){
			    	accionweb.getSesion().setAttribute("veclistaHistorial", null);
			    	accionweb.getSesion().removeAttribute("veclistaHistorial");
			    }
			    Vector  listaSolicitudesAsignacion = new Vector();
				Vector  listaHistorial = new Vector();
			
				List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
				CocowBean cocowBean = new CocowBean();
				listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
				//System.out.println("listCocow36DTO : "+ listCocow36DTO.size() );
				if(listCocow36DTO != null && listCocow36DTO.size() >0 ){
			    
			    for(Cocow36DTO ss: listCocow36DTO){	   		 
			    	    
			    	if(ss.getNomcam().trim().equals("MEMSOL"))
			    		memorandum = ss.getValalf();	
			  
			    	if(ss.getNomcam().trim().equals("RUTING"))
			    		rutIngresador = ss.getValnu1();
			    		
			     	if(ss.getNomcam().trim().equals("DIGING"))
			     		dvIngresador = ss.getValalf();
			     	
			    	if(ss.getNomcam().trim().equals("NOMING"))
			    		nomIngresador = ss.getValalf();
			    	
			     	if(ss.getNomcam().trim().equals("COMENT"))
			     		comentario = ss.getValalf();
			     	
			     	if(ss.getNomcam().trim().equals("FECSOL"))
			     		fecdoc = ss.getValnu1();  
			     	
			     	if(ss.getNomcam().trim().equals("ESTSOL"))
			     		estadoSolicitud = ss.getValalf().trim();   
			     	
		                       
		             if(ss.getNomcam().trim().equals("DETACC")){
		            	// System.out.println("DETACC");
		            	Vector vec = new Vector();
		            	vec.addElement(ss.getValnu1()); // rut acceso
		            	vec.addElement(ss.getValalf().trim()); // digito verificador
		            	vec.addElement(ss.getResval()); // nombre el acceso
		            	vec.addElement(ss.getValnu2()); // cuenta
		            	vec.addElement(ss.getAccion()); // accion
		            	listaSolicitudesAsignacion.add(vec);
		      
		            }
		       
		            if(ss.getNomcam().trim().equals("HISTORIAL")){
		            	// System.out.println("HISTORIAL");
		      
		            	String valor = "";
		            	
		            	if(ss.getValnu1()> 0 ) {
		            		fechaHist = ss.getValnu1() + "";
		    				diaFecha = fechaHist.substring(6);
		    				mesFecha = fechaHist.substring(4,6);
		    				annoFecha = fechaHist.substring(0,4);
		    				valor = diaFecha+"/"+mesFecha+"/"+annoFecha;   
		            	} else {
		    				valor = ss.getValnu1()+"";
		            	}  
		            	Vector vec3 = new Vector();
		             	vec3.addElement(ss.getResval()); // operaci�n
		            	vec3.addElement(valor); //fecha
		              	vec3.addElement(ss.getValalf()); // nombre
		           
		               	listaHistorial.add(vec3);
		            }
		          
			    }
			  
			    if(listaSolicitudesAsignacion != null && listaSolicitudesAsignacion.size() > 0){
			    	accionweb.agregarObjeto("veclistaSolicitudesAsignacion", listaSolicitudesAsignacion);
			    	accionweb.getSesion().setAttribute("veclistaSolicitudesAsignacion", listaSolicitudesAsignacion);
			    }
			
			    if(listaHistorial != null && listaHistorial.size() > 0){
			    	accionweb.agregarObjeto("veclistaHistorial", listaHistorial);
			       	accionweb.getSesion().setAttribute("veclistaHistorial", listaHistorial);
			    }
				fechaSol = fecdoc + "";
				diaFecha = fechaSol.substring(6);
				mesFecha = fechaSol.substring(4,6);
				annoFecha = fechaSol.substring(0,4);
				fechaSol = diaFecha+"/"+mesFecha+"/"+annoFecha;  
			    
			    accionweb.getSesion().setAttribute("memorandum",memorandum);
			    accionweb.getSesion().setAttribute("rutIngresador", moduloPresupuesto.formateoNumeroSinDecimales(Double.parseDouble(rutIngresador+"")) + "-" + dvIngresador);
			    accionweb.getSesion().setAttribute("nomIngresador", nomIngresador);
			    accionweb.getSesion().setAttribute("comentario",comentario);
			    accionweb.getSesion().setAttribute("fechaSolicitud",fechaSol);
			    accionweb.getSesion().setAttribute("estadoSolicitud",estadoSolicitud);
				accionweb.getSesion().setAttribute("titulo", titulo);
				accionweb.getSesion().setAttribute("numDoc", String.valueOf(numDoc));		
			
			    } else {
			    		accionweb.agregarObjeto("mensaje", "Lo sentimos, esta Asignaci�n de cuenta no existe.");
				
					}
	
		}
		
		 synchronized public void ejecucionMovimientoExportar(AccionWeb accionweb) throws Exception {
				/* LMP ejecuci�n por movimiento*/
				Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
				accionweb.agregarObjeto("cuentasPresupuestarias", lista);

				int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
				int mesInicio = Util.validaParametro(accionweb.getParameter("mesInicio"), 0);
				int mesTermino = Util.validaParametro(accionweb.getParameter("mesTermino"), 0);
				String estamento = Util.validaParametro(accionweb.getParameter("estamento"), "");
				String parametroServicio = "";
				int codUnidad = Util.validaParametro(accionweb.getParameter("codUnidad"),0);
				Vector vec_detalle = new Vector();
				Vector vec_datos = new Vector();
                
				int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");

				  try {
					  Collection<Presw18DTO> listaPresw18 = null;
				      if (codUnidad > 0) {					 
				  	  
					 	PreswBean preswbean = new PreswBean("LMP", codUnidad, 0, anno, 0,	0);
					 	preswbean.setSucur(1);// siempre va 1 PP
					 	preswbean.setMespar(mesInicio);
					 	preswbean.setItedoc(mesTermino);
						listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
							
						if (listaPresw18.size() == 0)
							listaPresw18 = null;						
				
				     }
				 
				      if(listaPresw18.size() > 0) {	
				    	  int indice=0;
				    	  String fechaDoc = "";
				    	  String diaFecha = "";
		    			  String mesFecha = "";
		    			  String annoFecha = "";
		    			  String fecha = "";
				    	  for (Presw18DTO ss: listaPresw18){
				    		  if(ss.getFecdoc() > 0) {
				    		    fechaDoc = ss.getFecdoc() + "";
			    				diaFecha = fechaDoc.substring(6);
			    				mesFecha = fechaDoc.substring(4,6);
			    				annoFecha = fechaDoc.substring(0,4);
			    				fecha = diaFecha+"/"+mesFecha+"/"+annoFecha;
				    		  } else fecha = "";
				    		 indice++;
				    		 vec_detalle = new Vector();	
				    		 if(!ss.getTipmov().trim().equals("")) {
							  vec_detalle.addElement(ss.getTipmov().trim()+ "-" + ss.getNomtip().trim());
							  vec_detalle.addElement(ss.getNumdoc());
							  vec_detalle.addElement(fecha);
							  vec_detalle.addElement(ss.getCoduni());
							  vec_detalle.addElement(ss.getItedoc());
							  vec_detalle.addElement(ss.getDesuni());
							  vec_detalle.addElement(ss.getIndprc());
							  vec_detalle.addElement(ss.getPresac());
							  vec_detalle.addElement(ss.getUsadac()); 
							  vec_detalle.addElement(ss.getDesite()); 
							  vec_detalle.addElement(ss.getNompro());
							
		                     
				    	  } else {
				    		  if( indice == listaPresw18.size()){
				    			  vec_detalle.addElement("Total");
				    			  vec_detalle.addElement(" ");
				    			  vec_detalle.addElement(" ");
				    			  vec_detalle.addElement(" ");
				    			  vec_detalle.addElement(" ");
				    			  vec_detalle.addElement(" ");
				    			  vec_detalle.addElement(" ");
				    			  vec_detalle.addElement(ss.getAcumum());
				    			  vec_detalle.addElement(ss.getUsadom());                      
				    			  vec_detalle.addElement(" ");
				    			  vec_detalle.addElement(" ");
				    		  }else {
				    			  vec_detalle.addElement("Total Mes");
				    			  vec_detalle.addElement(" ");
				    			  vec_detalle.addElement(" ");
				    			  vec_detalle.addElement(" ");
				    			  vec_detalle.addElement(" ");
				    			  vec_detalle.addElement(" ");
				    			  vec_detalle.addElement(" ");
				    			  vec_detalle.addElement(ss.getAcumum());
				    			  vec_detalle.addElement(ss.getUsadom());                      
				    			  vec_detalle.addElement(" ");
				    			  vec_detalle.addElement(" ");
				    		  }
				    	  }
						  vec_datos.addElement(vec_detalle); 
					
						 }
					  }
					  
				 	  } catch (Exception E) {
				        System.out.println("excepcion   " + E);
					   }
				  
				  moduloPresupuesto.agregaMovEjecucionExcel(accionweb.getReq(), vec_datos);
				}
			
}

