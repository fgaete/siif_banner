package cl.utfsm.sip;

import java.io.Serializable;
import java.util.Date;

import cl.utfsm.sip.Sedes;
import cl.utfsm.sip.TipoJornadas;

public class PeriodoProcesos implements Serializable{
	private Sedes sede;
	private TipoJornadas jornada;
	private int codigoProceso;
	private int anno;
	private int semestre;
	private Date fechaInicio;
	private Date fechaTermino;
	private int rutUsuario;
	private Date fechaModificacion;
	public Sedes getSede() {
		return sede;
	}
	public void setSede(Sedes sede) {
		this.sede = sede;
	}
	public TipoJornadas getJornada() {
		return jornada;
	}
	public void setJornada(TipoJornadas jornada) {
		this.jornada = jornada;
	}
	public int getCodigoProceso() {
		return codigoProceso;
	}
	public void setCodigoProceso(int codigoProceso) {
		this.codigoProceso = codigoProceso;
	}
	public int getAnno() {
		return anno;
	}
	public void setAnno(int anno) {
		this.anno = anno;
	}
	public int getSemestre() {
		return semestre;
	}
	public void setSemestre(int semestre) {
		this.semestre = semestre;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaTermino() {
		return fechaTermino;
	}
	public void setFechaTermino(Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}
	public int getRutUsuario() {
		return rutUsuario;
	}
	public void setRutUsuario(int rutUsuario) {
		this.rutUsuario = rutUsuario;
	}
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

}
