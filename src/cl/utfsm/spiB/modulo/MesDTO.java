package cl.utfsm.spiB.modulo;

public class MesDTO {
	private int nummes;
	private String nommes;
	public int getNummes() {
		return nummes;
	}
	public void setNummes(int nummes) {
		this.nummes = nummes;
	}
	public String getNommes() {
		return nommes;
	}
	public void setNommes(String nommes) {
		this.nommes = nommes;
	}
	

}
