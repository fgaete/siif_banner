package cl.utfsm.spiB.modulo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.tools.generic.DateTool;

import cl.utfsm.POJO.PRESF200;
import cl.utfsm.base.util.ConexionBanner;
import cl.utfsm.base.util.Util;
import cl.utfsm.conexion.ConexionAs400ValePago;
import cl.utfsm.spiB.datos.PagoIncentivoDao;
import descad.cliente.Ingreso_Documento;
import descad.cliente.Ingreso_Documentos;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw25DTO;

public class ModuloPagoIncentivoB {
	
	PagoIncentivoDao pagoIncentivoDao;
	
	public PagoIncentivoDao getPagoIncentivoDao() {
		return pagoIncentivoDao;
	}

	public void setPagoIncentivoDao(PagoIncentivoDao pagoIncentivoDao) {
		this.pagoIncentivoDao = pagoIncentivoDao;
	}

	public List<PRESF200> getOrganizacion(){
		// Luego cambiar a una conexion as para Contratacion
	   	 ConexionAs400ValePago con = new ConexionAs400ValePago();
	     List<PRESF200> lista = new ArrayList<PRESF200>();
	     
	     try {
	        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODORG, DESORG " +
	                                                                      " FROM USMMBP.PRESF200 "); 
	        ResultSet res = sent.executeQuery();

		    while (res.next()) { 
		    	PRESF200 presf200= new PRESF200();
		    	presf200.setCODORG(res.getString(1).trim()); // 0 codigo de la organizaci�n;
		    	presf200.setDESORG(res.getString(2).trim()); // 1 nombre de la organzaci�n;
		    	lista.add(presf200);
		     }
		     res.close();
		     sent.close();
		     con.getConnection().close();
		    }
		    catch (SQLException e ) {
		      System.out.println("Error en moduloPagoIncentivoB.getDatosOrganizacion : " + e );
		    }
		    return lista;
	}
	
	 public String getMesActual(){
		 DateTool fechaActual = new DateTool();
		 String mesActual = "";
			
		 String mes = (fechaActual.getMonth()+1)+"";
		 if(mes.length()== 1)
					mes = "0" + mes;
		 mesActual = mes;
		 return mesActual;
	}
	public String getAnioActual(){
		 DateTool fechaActual = new DateTool();
		 String a�oActual = String.valueOf(fechaActual.getYear());
			
		 
		 return a�oActual.substring(2,4);
	}
	
	
	public long getBuscaSaldo(String codOrg, String codCuenta){
		  ConexionBanner con = new ConexionBanner();
		  long saldoCuenta = 0;
		  try{
		     String mesActual = this.getMesActual();
			 String a�oActual = this.getAnioActual();
			 String query =   "SELECT sum(fgbbavl_sum_adopt_bud+(fgbbavl_sum_bud_adjt-fgbbavl_sum_ytd_actv-fgbbavl_sum_encumb-fgbbavl_sum_bud_rsrv)) " +
            " FROM fgbbavl t1 " +
            " WHERE t1.fgbbavl_coas_code = 'S'" + 
            " AND t1.fgbbavl_orgn_code =  ? " +
            " AND t1.fgbbavl_acct_code = ? " +
            " AND FGBBAVL_PERIOD <= '" + mesActual+ "'" +
            " AND FGBBAVL_FSYR_CODE = '" + a�oActual+ "'" +
            " ORDER BY FGBBAVL_PERIOD";
			PreparedStatement sent = con.conexion().prepareStatement(query);  
		    sent.setString(1,codOrg);
		    sent.setString(2,codCuenta);
		    ResultSet res = sent.executeQuery();

		     while (res.next()){
		    	saldoCuenta = res.getLong(1); // Saldo
		     }
		     res.close();
		     sent.close(); 
		     con.close();
		     }
		    catch (SQLException e){
		    	System.out.println("Error moduloOrdenCompraRegularizacionB.getBuscaSaldo: " + e);
			} 
		 // para la prueba 
		// saldoCuenta = 1000;
		 return saldoCuenta;
	}
	  
	
	 public String formateoRut(String rut) {
		  int cont = 0;
	        String format;
	        rut = rut.replace(".", "");
	        rut = rut.replace("-", "");
	        format = "-" + rut.substring(rut.length() - 1);
	        for (int i = rut.length() - 2; i >= 0; i--) {
	            format = rut.substring(i, i + 1) + format;
	            cont++;
	            if (cont == 3 && i != 0) {
	                format = "." + format;
	                cont = 0;
	            }
	        }
	       return format;
	}
	 
	 public int getRegistraIncentivo(HttpServletRequest req, int rut, String dv){
			
			PreswBean preswbean = new PreswBean("SAS", 0,0,0,0, rut);
			preswbean.setRutide(rut);
			preswbean.setDigide(dv);
		
		  	int numSol = 0;
			List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
			listCocow36DTO = this.getCreaCocow36(req, rut, dv, "SAS");
				
			Ingreso_Documento ingresoDocumento = new Ingreso_Documento("SAS",rut,dv);
				
			numSol = ingresoDocumento.Ingresar_Documento(listCocow36DTO);
			
			return numSol;
	  }
	 
	 public boolean saveAutRechPagoIncentivo(int numsol, String codOrg, int rutUsuario, String digide, String estado){
			PreswBean preswbean = new PreswBean("SAY",0,0,0,0, rutUsuario);
			preswbean.setDigide(digide);
			preswbean.setNumdoc(numsol);
			preswbean.setCajero(codOrg);
			preswbean.setTipcue(estado);
		return preswbean.ingreso_presw19();
		}
	 
	 public boolean saveVREAAutRechPagoIncentivo(int numsol, String codOrg, int rutUsuario, String digide, String estado){
			PreswBean preswbean = new PreswBean("SA0",0,0,0,0, rutUsuario);
			preswbean.setDigide(digide);
			preswbean.setNumdoc(numsol);
			preswbean.setCajero(codOrg);
			preswbean.setTipcue(estado);
		return preswbean.ingreso_presw19();
		}
	 
	 
	 public boolean getActualizaSolicitud(HttpServletRequest req, int rutide, String digide, int numSol){
			List<Presw18DTO> totalListaAsignaturas = (List<Presw18DTO>) req.getSession().getAttribute("totalListaAsignaturas");
				
			PreswBean preswbean = new PreswBean("SAV", 0,0,0,0, rutide);
			preswbean.setNumdoc(numSol);
			preswbean.setRutide(rutide);
			preswbean.setDigide(digide);
			 
			boolean registra = false;
			List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
			listCocow36DTO = this.getCreaCocow36(req, rutide, digide,"SAV");
			Ingreso_Documento ingresoDocumento = new Ingreso_Documento("SAV",rutide,digide);
			ingresoDocumento.setNumdoc(numSol);
			
			registra = ingresoDocumento.Actualizar_Documento(listCocow36DTO);
			return registra;
		 }
	 
	 public List<Cocow36DTO> getCreaCocow36(HttpServletRequest req, int rutide, String digide, String tipoProceso){
		    long rutInc = Util.validaParametro(req.getParameter("rutnum"),0);
			String dvInc = Util.validaParametro(req.getParameter("dvrut"),"");
			String codOrg = Util.validaParametro(req.getParameter("cuentaPresup"),"");
			long valorIncentivo = Util.validaParametro(req.getParameter("valorIncentivo"),0);
			long numCuotas = Util.validaParametro(req.getParameter("numCuotas"),0);
			long mesPago = Util.validaParametro(req.getParameter("mesPago"),0);
			long a�oPago = Util.validaParametro(req.getParameter("annioPago"),0);
			long codHaber = Util.validaParametro(req.getParameter("codigoHaber"),0);
			String motivo = Util.validaParametro(req.getParameter("motivo"),"").trim();
			
			String motivo1 = "";
			String motivo2 = "";
			String motivo3 = "";
			String motivo4 = "";
			
			if (motivo.length() > 50) {
				 motivo1 = motivo.substring(0,50);
				 motivo = motivo.substring(50);
				 if (motivo.length() > 50) {
					motivo2 = motivo.substring(0,50);
				    motivo = motivo.substring(50);
				    if (motivo.length() > 50) {
				     motivo3 = motivo.substring(0,50);
				     motivo = motivo.substring(50);
				     if (motivo.length() > 50) motivo4 = motivo.substring(0,50);
				     else motivo4 = motivo.substring(0);
				    }
				    else motivo3 = motivo.substring(0);
				 }
				 else motivo2 = motivo.substring(0);
			}
			else motivo1 = motivo.substring(0,motivo.length());
			
			String detalle = Util.validaParametro(req.getParameter("detalle"),"").trim();
			
			String detalle1 = "";
			String detalle2 = "";
			String detalle3 = "";
			String detalle4 = "";
			
			if (detalle.length() > 50) {
				detalle1 = detalle.substring(0,50);
				detalle = detalle.substring(50);
				 if (detalle.length() > 50) {
					 detalle2 = detalle.substring(0,50);
					 detalle = detalle.substring(50);
				    if (detalle.length() > 50) {
				    	detalle3 = detalle.substring(0,50);
				    	detalle = detalle.substring(50);
				     if (detalle.length() > 50) motivo4 = detalle.substring(0,50);
				     else detalle4 = detalle.substring(0);
				    }
				    else detalle3 = detalle.substring(0);
				 }
				 else detalle2 = detalle.substring(0);
			}
			else detalle1 = detalle.substring(0,detalle.length());
					
			List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
			
			String tiping = tipoProceso;
			String nomcam = "";
			long valnu1 = 0;
			long valnu2 = 0;
			String valalf = "";
			
			nomcam = "RUTIDE";
		    valnu1 = rutInc;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "DIGIDE";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = dvInc;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "CODORG";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = codOrg;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "VALINC";
		    valnu1 = valorIncentivo;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "TOTCUO";
		    valnu1 = numCuotas;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "MESINI";
		    valnu1 = mesPago;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "ANOINI";
		    valnu1 = a�oPago;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "CODHDE";
		    valnu1 = codHaber;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "GLODE1";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = motivo1;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "GLODE2";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = motivo2;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "GLODE3";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = motivo3;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "GLODE4";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = motivo4;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "MOTIV1";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = detalle1;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "MOTIV2";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = detalle2;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "MOTIV3";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = detalle3;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "MOTIV4";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = detalle4;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    return listCocow36DTO; 

			}
	 
	 public List<Cocow36DTO> getAgregaLista(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf){
		 Cocow36DTO cocow36DTO = new Cocow36DTO();
		 cocow36DTO.setTiping(tiping);
		 cocow36DTO.setNomcam(nomcam);
		 cocow36DTO.setValnu1(valnu1);
		 cocow36DTO.setValnu2(valnu2);
		 cocow36DTO.setValalf(valalf);
		 lista.add(cocow36DTO);
		 return lista;
 }
	 
	 public String getVerificaRut(String rutnumdv){
		   // System.out.println(rutnumdv);
		    ConexionBanner con = new ConexionBanner();
		    String nombre ="";
		    
		    String rut = rutnumdv.substring(0,rutnumdv.length()-1);
		    String dig = rutnumdv.substring(rutnumdv.length()-1,rutnumdv.length());
		    if (dig.equals("k")) rutnumdv = rut+"K";
		    
		    try{
		      
		      PreparedStatement sent = con.conexion().prepareStatement("SELECT spbpers_legal_name FROM spriden, spbpers " +
		    		                                                   "WHERE spriden_id = ? " +
		    		                                                   "AND spriden_pidm = spbpers_pidm " +
		    		                                                   "AND spriden_change_ind is null");
		      sent.setString(1,rutnumdv);
		      ResultSet res = sent.executeQuery();

		      if (res.next()){
		        nombre = res.getString(1); // nombre mas apellidos
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloValePagoB.getVerificaRut: " + e);
			}
		 return nombre;
	 }
	 
	 public List<Presw18DTO> getListaHaberes(){
		    ConexionAs400ValePago con = new ConexionAs400ValePago();
		    List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
			 String query = " SELECT a1.CODHDE, a1.DOCADM, a2.DESHDE " +
             " FROM REMUNERABT.APREF71 a1, REMUNERABT.APREF04 a2" + 
             " WHERE a1.CODHDE = a2.CODHDE ";
			 	//System.out.println(query);     
		     try {
		    
		       	PreparedStatement sent = con.getConnection().prepareStatement(query); 
		       ResultSet res = sent.executeQuery();
		     

			    while (res.next()) { 
			       Presw18DTO detalle = new Presw18DTO();
			       detalle.setCoduni(res.getInt(1)); // 0 codigo Haber
				   detalle.setDesite(res.getString(2).trim()); // 1 Sigla Haber
				   detalle.setDesuni(res.getString(3).trim()); // 2 Nombre Haber
				   lista.add(detalle);				                   
				   
			     }
			     res.close();
			     sent.close();
			     // con.close();
			     con.getConnection().close();

			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloPagoIncentivoB.getListaHaberes : " + e );
			    }
		    return lista;
		}

}
