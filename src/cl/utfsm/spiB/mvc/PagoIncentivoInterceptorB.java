package cl.utfsm.spiB.mvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;

import org.apache.velocity.tools.generic.DateTool;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.utfsm.POJO.PRESF200;
import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.sivB.modulo.ModuloValePagoB;
import cl.utfsm.spiB.modulo.MesDTO;
import cl.utfsm.spiB.modulo.ModuloPagoIncentivoB;
import descad.cliente.CocowBean;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;

public class PagoIncentivoInterceptorB extends HandlerInterceptorAdapter {
	ModuloPagoIncentivoB moduloPagoIncentivoB;
	ModuloValePagoB moduloValePagoB;
	
	public ModuloPagoIncentivoB getModuloPagoIncentivoB() {
		return moduloPagoIncentivoB;
	}


	public void setModuloPagoIncentivoB(ModuloPagoIncentivoB moduloPagoIncentivoB) {
		this.moduloPagoIncentivoB = moduloPagoIncentivoB;
	}


	public void cargarMenu(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		accionweb.agregarObjeto("esPagoIncentivoB", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		accionweb.agregarObjeto("titulo", "Ingreso");
		
		if(accionweb.getSesion().getAttribute("listaOrg") != null)
		   accionweb.getSesion().removeAttribute("listaOrg");
		
		List<PRESF200> listaOrg = new ArrayList<PRESF200>();
		listaOrg = moduloPagoIncentivoB.getOrganizacion();
		if(listaOrg != null && listaOrg.size() > 0)
	       accionweb.getSesion().setAttribute("listaOrg", listaOrg);
		
		accionweb.agregarObjeto("tipo", tipo);
		if (tipo == 5)
			accionweb.agregarObjeto("titulo", "Autorizar");
		
	}
	
	public void cargaIngresoPagoIncentivo(AccionWeb accionweb) throws Exception {
		verificaRut(accionweb);
		if(accionweb.getReq().getAttribute("mensaje") == null || accionweb.getReq().getAttribute("mensaje").toString().trim().equals(""))
			cargaOpcionMenu(accionweb);
		return;
	}
	
	
	 public void cargaOpcionMenu(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			
			PreswBean preswbean = null;
			Collection<Presw18DTO> listaSolicitudes = null;
			String titulo = "";
			String tituloDetalle1 = "";
			String tituloDetalle2 = "";
			
			switch (tipo) {
			case  2:// lista de Solicitudes para modificar
				{
				preswbean = new PreswBean("SAT",0,0,0,0,rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				titulo = "Modificaci�n";
				break;
			   }
			case  3:// Consulta de ude las solicitudes de un rut Ingresador
				{
				preswbean = new PreswBean("SAW",0,0,0,0,rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				titulo = "Consulta";
				break;
			   }	
			case  4:// lista de solicitudes para autorizar 
				{
				preswbean = new PreswBean("SAX",0,0,0,0,rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				titulo = "Autorizaraci�n de responsable de cuenta";
				tituloDetalle1 = "Autorizar";
				tituloDetalle2 = "Rechazar";
				accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
				accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
				accionweb.agregarObjeto("opcion2", 8);
				
				break;
			   }	
		case  5:// lista de solicitudes para VREA/VRA
				{
				preswbean = new PreswBean("SAZ",0,0,0,0,rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				preswbean.setTipcue("E");
				titulo = "Autorizaci�n VREA";
				break;
			   }
		case  9:// lista de solicitudes para VREA/VRA
			  {
				preswbean = new PreswBean("SAZ",0,0,0,0,rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				preswbean.setTipcue("A");
				titulo = "Autorizaci�n VRA";
				break;
			      }		
		case 11: // lista de Solicitudes de Pago de Incentivo a copiar
				{		
				preswbean = new PreswBean("SBA",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				listaSolicitudes = (Collection<Presw18DTO>) preswbean.consulta_presw18();
				titulo = "Existentes";
				int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"),0);
				String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
				//String nomIdentificador = Util.validaParametro(accionweb.getParameter("nomIdentificador"),"");
				if(rutnum > 0)
				  	 this.verificaRut(accionweb);
			
				accionweb.agregarObjeto("dvrut", dvrut);
				accionweb.agregarObjeto("rutnum", rutnum);
								
				List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
				moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
				listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
				accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
			   	break;
				}			
				
			}
			
			
			if(tipo != 6)
			 	listaSolicitudes = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			//System.out.println("listaSolicitudes.size() " + listaSolicitudes.size());
		    if(listaSolicitudes != null && listaSolicitudes.size() > 0)
	        	accionweb.agregarObjeto("hayDatoslista", "1");
		    
		    for (Presw18DTO lis : listaSolicitudes) {
			   System.out.println(lis.getDesite());
			   System.out.println(lis.getDesuni());
			   System.out.println(lis.getNomtip());
		       System.out.println(lis.getPreano());
		     }
			
		    accionweb.agregarObjeto("titulo", titulo);
		    accionweb.agregarObjeto("opcion", tipo);
	        accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes);
	    	accionweb.agregarObjeto("esPagoIncentivoB", "1");
	   	
		}
	
	public void consultaPagoIncentivo(AccionWeb accionweb) throws Exception {
		 int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		 String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		 int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		 String nomIdentificador = Util.validaParametro(accionweb.getParameter("nomIdentificador"),""); 
		 int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		 String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),""); 
		 
		 int numSol = Util.validaParametro(accionweb.getParameter("numSol"), 0);
	  	 Presw19DTO preswbean19DTO = new Presw19DTO();
	   	 String titulo = "";
		 String tituloDetalle1 = "";
		 preswbean19DTO.setTippro("SAU"); 
	  	 preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
	  	 preswbean19DTO.setDigide(dv);
		 preswbean19DTO.setNumdoc(new BigDecimal(numSol));
		 		 
		 String nomcam = "";
		 long valnu1 = 0;
		 long valnu2 = 0;
		 String valalf = "";
		 long rutInc = 0;
		 String dvInc = "";
		 String codOrg = "";
		 String nomOrg = "";
		 long valorIncentivo = 0;
		 long numCuotas = 0;
		 long mesPago = 0;
		 long a�oPago = 0;
		 long codHaber = 0;
		 String descHaber = "";
		 String motivo1 = "";
		 String motivo2 = "";
		 String motivo3 = "";
		 String motivo4 = "";
		 String detalle1 = "";
		 String detalle2 = "";
		 String detalle3 = "";
		 String detalle4 = "";
		 String nombre = "";
		 String estado = "";
		 List<Presw18DTO>  listaHistorial = new ArrayList<Presw18DTO>();
		 
		 List<Cocow36DTO>  listaSolicitud = new ArrayList<Cocow36DTO>();
		 CocowBean cocowBean = new CocowBean();
		 listaSolicitud = cocowBean.buscar_cocow36(preswbean19DTO);
		 if(listaSolicitud != null && listaSolicitud.size() >0 ){
		    for(Cocow36DTO ss: listaSolicitud){
             if(ss.getNomcam().trim().equals("RUTIDE")) 
              	rutInc = ss.getValnu1();
             if(ss.getNomcam().trim().equals("DIGIDE"))
	            dvInc = ss.getValalf().trim();
             if(ss.getNomcam().trim().equals("CODORG"))
 			   	codOrg = ss.getValalf().trim();
 			 if(ss.getNomcam().trim().equals("DESORG"))
 			   	 nomOrg = ss.getValalf().trim();
 			 if(ss.getNomcam().trim().equals("VALINC"))
 				valorIncentivo = ss.getValnu1();
 			 if(ss.getNomcam().trim().equals("TOTCUO"))
				 numCuotas = ss.getValnu1();
 			 if(ss.getNomcam().trim().equals("MESINI"))
		         mesPago = ss.getValnu1();
		     if(ss.getNomcam().trim().equals("ANOINI"))
		         a�oPago = ss.getValnu1();  
		     if(ss.getNomcam().trim().equals("CODHDE"))
	 			 codHaber = ss.getValnu1();  
	 		 if(ss.getNomcam().trim().equals("DESHDE"))
	 		   	 descHaber = ss.getValalf().trim();
	 		 if(ss.getNomcam().trim().equals("GLODE1"))
		    	 motivo1 = ss.getValalf().substring(0,50);
		     if(ss.getNomcam().trim().equals("GLODE2"))
		    	 motivo2 = ss.getValalf().substring(0,50);
		     if(ss.getNomcam().trim().equals("GLODE3"))
		    	 motivo3 = ss.getValalf().substring(0,50);
		     if(ss.getNomcam().trim().equals("GLODE4"))
		    	 motivo4 = ss.getValalf().substring(0,50);
		     if(ss.getNomcam().trim().equals("MOTIV1"))
		    	 detalle1 = ss.getValalf().substring(0,50);
		     if(ss.getNomcam().trim().equals("MOTIV2"))
		    	 detalle2 = ss.getValalf().substring(0,50);
		     if(ss.getNomcam().trim().equals("MOTIV3"))
		    	 detalle3 = ss.getValalf().substring(0,50);
		     if(ss.getNomcam().trim().equals("MOTIV4"))
		    	 detalle4 = ss.getValalf().substring(0,50);
		     
             if(ss.getNomcam().trim().equals("NOMIDE"))
     	        nombre = ss.getValalf().trim();
             if(ss.getNomcam().trim().equals("ESTADO"))
			    estado = ss.getValalf().trim();
             if(ss.getNomcam().trim().equals("HISTORIAL")){
	           	Presw18DTO presw18DTO = new Presw18DTO();
	           	presw18DTO.setDesite(ss.getResval()); // tipo de operaci�n 
	           	presw18DTO.setDesuni(ss.getValalf()); // Nombre operador    
	           	presw18DTO.setUsadom(ss.getValnu1()); // fecha
   	           	listaHistorial.add(presw18DTO);
	          }
		      
		    }      
	     }

		 String rutFormateado = moduloPagoIncentivoB.formateoRut(rutInc+""+dvInc);
		 accionweb.agregarObjeto("rutSolicitud",rutFormateado);
		 if (tipo != 11) {
			 accionweb.agregarObjeto("rutnum",rutInc);
			 accionweb.agregarObjeto("dvrut",dvInc);
		 }
		 else {
			 accionweb.agregarObjeto("rutnum",rutnum);
			 accionweb.agregarObjeto("dvrut",dvrut);
			 accionweb.agregarObjeto("rutIdentificador",moduloPagoIncentivoB.formateoRut(rutnum+""+dvrut));
			 accionweb.agregarObjeto("titulo","Ingreso");
		 }
		 accionweb.agregarObjeto("codOrg",codOrg);
		 accionweb.agregarObjeto("nomOrg",nomOrg);
		 accionweb.agregarObjeto("valorIncentivo",valorIncentivo);
		 accionweb.agregarObjeto("numCuotas",numCuotas);
		 accionweb.agregarObjeto("nombre",nombre);
		 accionweb.agregarObjeto("mesPago",mesPago);
		 accionweb.agregarObjeto("annioPago",a�oPago);
		 accionweb.agregarObjeto("codHaber",codHaber);
		 accionweb.agregarObjeto("descHaber",descHaber);
		 accionweb.agregarObjeto("motivo",motivo1+""+motivo2+""+motivo3+""+motivo4);
		 accionweb.agregarObjeto("detalle",detalle1+""+detalle2+""+detalle3+""+detalle4);
		 accionweb.agregarObjeto("nombre",nombre);
		 accionweb.agregarObjeto("estado",estado);
		 accionweb.agregarObjeto("listaHistorial",listaHistorial);
		 accionweb.agregarObjeto("numSol",numSol);
		 accionweb.agregarObjeto("nomIdentificador",nomIdentificador);
		 
		 accionweb.agregarObjeto("opcion", tipo); 
		 
		 List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
		 moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
		 listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		    
		 List<Presw18DTO> listaHaberes = moduloPagoIncentivoB.getListaHaberes();
		 accionweb.agregarObjeto("listaHaberes", listaHaberes);
	     accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);  
	 	 
		 if (tipo == 3 || tipo == 4 || tipo == 5 || tipo == 9) {
			 accionweb.agregarObjeto("identificadorConsulta", "1");
			 if (tipo == 3 )
			    accionweb.agregarObjeto("titulo", "Consulta");
			 if (tipo == 4)
				accionweb.agregarObjeto("titulo", "Autorizaci�n Responsable de Cuenta");
			 if (tipo == 5)
				 accionweb.agregarObjeto("titulo", "Autorizaci�n VREA");
			 if (tipo == 9)
				 accionweb.agregarObjeto("titulo", "Autorizaci�n VRA");
		 }
		 else if (tipo == 2)  {
			    accionweb.agregarObjeto("titulo", "Modificaci�n");
			 }
		 
		 
		 String codCuenta = "";
		 for (Presw18DTO lis : listaHaberes) {
			if (codHaber == lis.getCoduni()) {
  		        if (lis.getDesite().trim().equals("ADM"))
			    	codCuenta ="5B0004";
			    else  codCuenta = "5A0007";
			    break;
			}    
	     }
		 
		 long saldoCuenta = moduloValePagoB.getBuscaSaldo(codOrg.trim(),codCuenta.trim());
	     accionweb.agregarObjeto("saldoCuenta", saldoCuenta);
	
	     accionweb.agregarObjeto("esPagoIncentivoB", "1");
		 
	 }
	
	synchronized public void ejecutaAccion(AccionWeb accionweb) throws Exception {
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numSol = Util.validaParametro(accionweb.getParameter("numSol"), 0);
			
		switch (tipo){
		case 4: {
			this.autorizaPagoIncentivo(accionweb);
			break;
		}
		case 5: case 9: {
			this.autorizaPagoIncentivoVREAVRA(accionweb);
		break;
		}
		case 6:{// rechaza autorizaci�n
			this.rechazaAutPagoIncentivo(accionweb);
		break;
			
		}
		}
		
		this.consultaPagoIncentivo(accionweb);
		
	}
	
	
	synchronized public void registraPagoInventivo(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
						
		int numSolicitud = 0;
		String mensaje = "";
        numSolicitud = moduloPagoIncentivoB.getRegistraIncentivo(accionweb.getReq(),rutUsuario,dv);
	    if( numSolicitud > 0){
	       accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa la Solicitud de Pago de incentivo N� Solicitud: "+ numSolicitud);
	       accionweb.agregarObjeto("registra", 1);
	       accionweb.agregarObjeto("titulo","Ingreso");
	
	    } else
	      	accionweb.agregarObjeto("mensaje", "No se registr� la Solicitud de Pago de Incentivo.");
	   
	   	accionweb.agregarObjeto("esPagoIncentivoB", "1");      
	}
	
	synchronized public void autorizaPagoIncentivo(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numSol = Util.validaParametro(accionweb.getParameter("numSol"),0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"),0);
		long pago = Util.validaParametro(accionweb.getParameter("valorIncentivo"),0);
		String codOrg = Util.validaParametro(accionweb.getParameter("codOrg")+ "","");
		String codCuenta = Util.validaParametro(accionweb.getParameter("codCuenta")+ "","");
		long codHaber = Util.validaParametro(accionweb.getParameter("codHaber"),0);
		
		if (!codOrg.equals("")) {
		
		if (codCuenta.equals("")) {
			List<Presw18DTO> listaHaberes = moduloPagoIncentivoB.getListaHaberes();
			for (Presw18DTO lis : listaHaberes) {
				if (codHaber == lis.getCoduni()) {
	  		        if (lis.getDesite().trim().equals("ADM"))
				    	codCuenta ="5B0004";
				    else  codCuenta = "5A0007";
				    break;
				}    
		     }
		}
	
		long saldoOrg = moduloPagoIncentivoB.getBuscaSaldo(codOrg,codCuenta);
		boolean sobrepasa = (saldoOrg>pago?false:true);
		
		try {
			if (!sobrepasa){
				boolean aut = moduloPagoIncentivoB.saveAutRechPagoIncentivo(numSol,codOrg,rutUsuario,dv,"A");
				Thread.sleep(1500);
				if (aut) 
				   accionweb.agregarObjeto("mensaje", "Se autoriz� sin problemas estado de la Organizaci�n a autorizada.");	
		    }
			else {
				boolean aut = moduloPagoIncentivoB.saveAutRechPagoIncentivo(numSol,codOrg,rutUsuario,dv,"B");
				Thread.sleep(1500);
				if (aut) 
				   accionweb.agregarObjeto("mensaje", "Se autoriz� sin problemas estado de la Organizaci�n a Sin Financiamiento.");	
			}
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "Problemas al actulizar el estado de las organizaciones autorizaPagoIncentivo.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		
		cargaOpcionMenu(accionweb);
	
		}
		else accionweb.agregarObjeto("mensaje", "Falta infromaci�n para la autorizaci�n.");
		
	    
		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	}
	
	synchronized public void autorizaPagoIncentivoVREAVRA(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numSol = Util.validaParametro(accionweb.getParameter("numSol"),0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"),0);
		long pago = Util.validaParametro(accionweb.getParameter("valorIncentivo"),0);
		String codOrg = Util.validaParametro(accionweb.getParameter("codOrg")+ "","");
		String codCuenta = Util.validaParametro(accionweb.getParameter("codCuenta")+ "","");
		long codHaber = Util.validaParametro(accionweb.getParameter("codHaber"),0);
		
		if (!codOrg.equals("")) {
		
		if (codCuenta.equals("")) {
			List<Presw18DTO> listaHaberes = moduloPagoIncentivoB.getListaHaberes();
			for (Presw18DTO lis : listaHaberes) {
				if (codHaber == lis.getCoduni()) {
	  		        if (lis.getDesite().trim().equals("ADM"))
				    	codCuenta ="5B0004";
				    else  codCuenta = "5A0007";
				    break;
				}    
		     }
		}
	
		long saldoOrg = moduloPagoIncentivoB.getBuscaSaldo(codOrg,codCuenta);
		boolean sobrepasa = (saldoOrg>pago?false:true);
		
		try {
			if (!sobrepasa){
				boolean aut = moduloPagoIncentivoB.saveVREAAutRechPagoIncentivo(numSol,codOrg,rutUsuario,dv,"T");
				Thread.sleep(1500);
				if (aut) 
				   accionweb.agregarObjeto("mensaje", "Se autoriz� sin problemas estado de la Organizaci�n a autorizada.");	
		    }
			else {
				boolean aut = moduloPagoIncentivoB.saveVREAAutRechPagoIncentivo(numSol,codOrg,rutUsuario,dv,"C");
				Thread.sleep(1500);
				if (aut) 
				   accionweb.agregarObjeto("mensaje", "Se autoriz� sin problemas estado de la Organizaci�n a Sin Financiamiento.");	
			}
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "Problemas al actulizar el estado de las organizaciones autorizaPagoIncentivo.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		
		cargaOpcionMenu(accionweb);
		}
		else accionweb.agregarObjeto("mensaje", "Falta infromaci�n para la autorizaci�n.");
	    
		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	}
	
	synchronized public void actualizaSolicitud(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numSol = Util.validaParametro(accionweb.getParameter("numSol"),0);
		
		boolean registra = false;
	    
	    registra = moduloPagoIncentivoB.getActualizaSolicitud(accionweb.getReq(),rutUsuario,dv,numSol);
	    if(registra){
	   	    accionweb.agregarObjeto("mensaje", "Se actualiz� en forma exitosa la Solicitud de Pago de Incentivo.");
	   	    accionweb.agregarObjeto("registra", 1);
	    } else
	    	accionweb.agregarObjeto("mensaje", "No se modific� la Solicitud de Pago de Incentivo.");
		
	    accionweb.agregarObjeto("esPagoIncentivoB", "1");    
	    accionweb.agregarObjeto("titulo", "Modificaci�n");  
	    Thread.sleep(1000);
	    this.cargaOpcionMenu(accionweb); 
	    
    }
	
	synchronized public void rechazaAutPagoIncentivo(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numSol = Util.validaParametro(accionweb.getParameter("numSol"), 0);	
		String glosa = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "","");
		String codOrg = Util.validaParametro(accionweb.getParameter("codOrg")+ "","");
		
		try { 
		boolean graba = moduloPagoIncentivoB.saveAutRechPagoIncentivo(numSol,codOrg,rutUsuario,dv,"R");
	    if(graba)
	    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el rechazo de la Autorizaci�n.");
	    else
	    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Autorizaci�n.");
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado rechazaAutPagoIncentivo.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		Thread.sleep(500);
		cargaOpcionMenu(accionweb);
		
	}
	 
	 synchronized public void rechazaAutVREAVRAPagoIncentivo(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numSol = Util.validaParametro(accionweb.getParameter("numSol"), 0);	
			String glosa = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			String codOrg = Util.validaParametro(accionweb.getParameter("codOrg")+ "","");
			
			try { 
			String estado = "";
			if (tipo == 5) estado = "L";
			else estado = "H";	
			boolean graba = moduloPagoIncentivoB.saveVREAAutRechPagoIncentivo(numSol,codOrg,rutUsuario,dv,estado);
		    if(graba)
		    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el rechazo de la Autorizaci�n.");
		    else
		    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Autorizaci�n.");
			} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado rechazaAutValePago.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
			Thread.sleep(500);
		    cargaOpcionMenu(accionweb);
			
		} 
	 
	 
	 public void verificaRut(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
			String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
			String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
			int identificador = Util.validaParametro(accionweb.getParameter("identificador"), 0);
			List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
			List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudesPago");
			if(listaSolicitudesPago != null && listaSolicitudesPago.size() > 0)
				accionweb.getSesion().removeAttribute("listaSolicitudesPago");
		
			String nombre = "";
			if(rutnum > 0){
				nombre = moduloPagoIncentivoB.getVerificaRut(rutnum+""+dvrut);
				accionweb.agregarObjeto("rut_aux", rut_aux);  
				accionweb.agregarObjeto("rutnum", rutnum); 
				accionweb.agregarObjeto("dvrut", dvrut); 
				
			}
		   	if(!nombre.trim().equals("")){
		    		accionweb.agregarObjeto("hayDatoslista", 1);
		    		accionweb.agregarObjeto("nomIdentificador", nombre);
		    		this.cargaMes(accionweb);
		   	
		   	} else
		    	accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado. Debe solicitar a FINANZAS su creaci�n.");
			
		   	List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
		    moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
		    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		    
		    List<Presw18DTO> listaHaberes = moduloPagoIncentivoB.getListaHaberes();
   		   
		    
		    
		    
   		    accionweb.agregarObjeto("listaHaberes", listaHaberes);
	     	accionweb.agregarObjeto("identificador", identificador);
	     	accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);  
	     	accionweb.agregarObjeto("esPagoIncentivoB", "1");   
			
		}
	public void cargaMes(AccionWeb accionweb) throws Exception {
		int annioPago = Util.validaParametro(accionweb.getParameter("annioPago"), 0);
		GregorianCalendar hoy = new GregorianCalendar(); 
		int mesActual = hoy.get(Calendar.MONTH) + 1;
		int anioActual = hoy.get(Calendar.YEAR);
		int anioFin = hoy.get(Calendar.YEAR) + 1;
		List<MesDTO> listamesDTO = new ArrayList<MesDTO>();
		int mes = (annioPago==0 || annioPago == anioActual)?mesActual:1;
		int mesFin = (annioPago == 0 || annioPago == anioActual)?12:(mesActual-1);
		int anioVal = (annioPago == 0)?anioActual:annioPago;
		for(int i=mes;i<=mesFin;i++) {
		
		switch (i){
		case 1: {
			MesDTO mesDTO = new MesDTO();
    		mesDTO.setNummes(1);
			mesDTO.setNommes("Enero");
			listamesDTO.add(mesDTO);
			break;
		}
		case 2: {
			MesDTO mesDTO = new MesDTO();
    		mesDTO.setNummes(2);
			mesDTO.setNommes("Febrero");
			listamesDTO.add(mesDTO);
			break;
		}
		case 3: {
			MesDTO mesDTO = new MesDTO();
    		mesDTO.setNummes(3);
			mesDTO.setNommes("Marzo");
			listamesDTO.add(mesDTO);
			break;
		}
		case 4: {
			MesDTO mesDTO = new MesDTO();
    		mesDTO.setNummes(4);
			mesDTO.setNommes("Abril");
			listamesDTO.add(mesDTO);
			break;
		}
		case 5: {
			MesDTO mesDTO = new MesDTO();
    		mesDTO.setNummes(5);
			mesDTO.setNommes("Mayo");
			listamesDTO.add(mesDTO);
			break;
		}
		case 6: {
			MesDTO mesDTO = new MesDTO();
    		mesDTO.setNummes(6);
			mesDTO.setNommes("Junio");
			listamesDTO.add(mesDTO);
			break;
		}
		case 7: {
			MesDTO mesDTO = new MesDTO();
    		mesDTO.setNummes(7);
			mesDTO.setNommes("Julio");
			listamesDTO.add(mesDTO);
			break;
		}
		case 8: {
			MesDTO mesDTO = new MesDTO();
    		mesDTO.setNummes(8);
			mesDTO.setNommes("Agosto");
			listamesDTO.add(mesDTO);
			break;
		}
		case 9: {
			MesDTO mesDTO = new MesDTO();
    		mesDTO.setNummes(9);
			mesDTO.setNommes("Septiembre");
			listamesDTO.add(mesDTO);
			break;
		}
		case 10: {
			MesDTO mesDTO = new MesDTO();
    		mesDTO.setNummes(10);
			mesDTO.setNommes("Octubre");
			listamesDTO.add(mesDTO);
			break;
		}
		case 11: {
			MesDTO mesDTO = new MesDTO();
    		mesDTO.setNummes(11);
			mesDTO.setNommes("Noviembre");
			listamesDTO.add(mesDTO);
			break;
		}
		case 12: {
			MesDTO mesDTO = new MesDTO();
    		mesDTO.setNummes(7);
			mesDTO.setNommes("Diciembre");
			listamesDTO.add(mesDTO);
			break;
		}
		
		}
		
		}
		   		
		accionweb.agregarObjeto("mesActual", mesActual);
		accionweb.agregarObjeto("listamesDTO", listamesDTO);
		accionweb.agregarObjeto("anioActual", anioActual);
		accionweb.agregarObjeto("anioFin", anioFin);
		accionweb.agregarObjeto("anioVal", anioVal);
		
	}
	 synchronized public void revisaSaldoOrganizacion(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		String codOrg = Util.validaParametro(accionweb.getParameter("codOrg")+ "",""); 
		String codCuenta = Util.validaParametro(accionweb.getParameter("codCuenta")+ "",""); 
		 
		
	 	long saldoCuenta = 0;
	 	if (codOrg != null && !codOrg.trim().equals("") && codCuenta != null &&  !codCuenta.trim().equals("") ){
		      saldoCuenta = moduloValePagoB.getBuscaSaldo(codOrg.trim(),codCuenta.trim());
		      accionweb.agregarObjeto("saldoCuenta", saldoCuenta);
	    }    
		
	 }
	 
	 public void imprimePagoIncentivo(AccionWeb accionweb) throws Exception {
		 int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		 String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		 int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		 
		 int numSol = Util.validaParametro(accionweb.getParameter("numSol"), 0);
	  	 Presw19DTO preswbean19DTO = new Presw19DTO();
	   	 String titulo = "";
		 String tituloDetalle1 = "";
		 preswbean19DTO.setTippro("SAU"); 
	  	 preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
	  	 preswbean19DTO.setDigide(dv);
		 preswbean19DTO.setNumdoc(new BigDecimal(numSol));
		 		 
		 String nomcam = "";
		 long valnu1 = 0;
		 long valnu2 = 0;
		 String valalf = "";
		 long rutInc = 0;
		 String dvInc = "";
		 String codOrg = "";
		 String nomOrg = "";
		 long valorIncentivo = 0;
		 long numCuotas = 0;
		 long mesPago = 0;
		 long a�oPago = 0;
		 long codHaber = 0;
		 String descHaber = "";
		 String motivo1 = "";
		 String motivo2 = "";
		 String motivo3 = "";
		 String motivo4 = "";
		 String detalle1 = "";
		 String detalle2 = "";
		 String detalle3 = "";
		 String detalle4 = "";
		 String nombre = "";
		 String estado = "";
		 Vector vecHistorial = new Vector();	
		 
		 List<Cocow36DTO>  listaSolicitud = new ArrayList<Cocow36DTO>();
		 CocowBean cocowBean = new CocowBean();
		 listaSolicitud = cocowBean.buscar_cocow36(preswbean19DTO);
		 if(listaSolicitud != null && listaSolicitud.size() >0 ){
		    for(Cocow36DTO ss: listaSolicitud){
             if(ss.getNomcam().trim().equals("RUTIDE")) 
              	rutInc = ss.getValnu1();
             if(ss.getNomcam().trim().equals("DIGIDE"))
	            dvInc = ss.getValalf().trim();
             if(ss.getNomcam().trim().equals("CODORG"))
 			   	codOrg = ss.getValalf().trim();
 			 if(ss.getNomcam().trim().equals("DESORG"))
 			   	 nomOrg = ss.getValalf().trim();
 			 if(ss.getNomcam().trim().equals("VALINC"))
 				valorIncentivo = ss.getValnu1();
 			 if(ss.getNomcam().trim().equals("TOTCUO"))
				 numCuotas = ss.getValnu1();
 			 if(ss.getNomcam().trim().equals("MESINI"))
		         mesPago = ss.getValnu1();
		     if(ss.getNomcam().trim().equals("ANOINI"))
		         a�oPago = ss.getValnu1();  
		     if(ss.getNomcam().trim().equals("CODHDE"))
	 			 codHaber = ss.getValnu1();  
	 		 if(ss.getNomcam().trim().equals("DESHDE"))
	 		   	 descHaber = ss.getValalf().trim();
	 		 if(ss.getNomcam().trim().equals("GLODE1"))
		    	 motivo1 = ss.getValalf().trim();
		     if(ss.getNomcam().trim().equals("GLODE2"))
		    	 motivo2 = ss.getValalf().trim();
		     if(ss.getNomcam().trim().equals("GLODE3"))
		    	 motivo3 = ss.getValalf().trim();
		     if(ss.getNomcam().trim().equals("GLODE4"))
		    	 motivo4 = ss.getValalf().trim();
		     if(ss.getNomcam().trim().equals("MOTIV1"))
		    	 detalle1 = ss.getValalf().trim();
		     if(ss.getNomcam().trim().equals("MOTIV2"))
		    	 detalle2 = ss.getValalf().trim();
		     if(ss.getNomcam().trim().equals("MOTIV3"))
		    	 detalle3 = ss.getValalf().trim();
		     if(ss.getNomcam().trim().equals("MOTIV4"))
		    	 detalle4 = ss.getValalf().trim();
             if(ss.getNomcam().trim().equals("NOMIDE"))
     	        nombre = ss.getValalf().trim();
             if(ss.getNomcam().trim().equals("ESTADO"))
			    estado = ss.getValalf().trim();
             if(ss.getNomcam().trim().equals("HISTORIAL")){
            	String fecha = "";
	            String diaFecha = "";
	            String mesFecha = "";
	            String annoFecha = "";
	            String valor = "";
	            if(ss.getValnu1()> 0 ) {
    				fecha = ss.getValnu1() + "";
    				diaFecha = fecha.substring(6);
    				mesFecha = fecha.substring(4,6);
    				annoFecha = fecha.substring(0,4);
    				valor = diaFecha+"/"+mesFecha+"/"+annoFecha;   
            	} else {
    				valor = ss.getValnu1()+"";
            	}
	            
	            valor += " por " + ss.getValalf();
	            
	          	Vector vec3 = new Vector();
            	vec3.addElement(ss.getResval()); // es la accion, INGRESO, MODIFICACION, AUTORIZACION, PAGO, AUTORIZA FINANZA				        
            	vec3.addElement(valor); // es la fecha, 
            	vecHistorial.addElement(vec3);
      
	          }
		      
		    }      
	     }
		 
		 System.out.println("vecHistorila " + vecHistorial);
		 String rutFormateado = moduloPagoIncentivoB.formateoRut(rutInc+""+dvInc);
		 accionweb.getSesion().setAttribute("rutSolicitud",rutFormateado);
		 accionweb.getSesion().setAttribute("rutnum",rutInc);
		 accionweb.getSesion().setAttribute("dvrut",dvInc);
		 accionweb.getSesion().setAttribute("codOrg",codOrg);
		 accionweb.getSesion().setAttribute("nomOrg",nomOrg);
		 accionweb.getSesion().setAttribute("valorIncentivo",valorIncentivo+"");
		 accionweb.getSesion().setAttribute("numCuotas",numCuotas+"");
		 accionweb.getSesion().setAttribute("nombre",nombre);
		 accionweb.getSesion().setAttribute("mesPago",mesPago+"");
		 accionweb.getSesion().setAttribute("annioPago",a�oPago+"");
		 accionweb.getSesion().setAttribute("codHaber",codHaber+"");
		 accionweb.getSesion().setAttribute("descHaber",descHaber);
		 accionweb.getSesion().setAttribute("motivo",motivo1+""+motivo2+""+motivo3+""+motivo4);
		 accionweb.getSesion().setAttribute("detalle",detalle1+""+detalle2+""+detalle3+""+detalle4);
		 accionweb.getSesion().setAttribute("nombre",nombre);
		 accionweb.getSesion().setAttribute("estado",estado);
		 accionweb.getSesion().setAttribute("vecHistorial",vecHistorial);
		 accionweb.getSesion().setAttribute("numSol",numSol+"");
		 
		 List<Presw18DTO> listaHaberes = moduloPagoIncentivoB.getListaHaberes();
		
		 String codCuenta = "";
		 for (Presw18DTO lis : listaHaberes) {
			if (codHaber == lis.getCoduni()) {
  		        if (lis.getDesite().trim().equals("ADM"))
			    	codCuenta ="5B0004";
			    else  codCuenta = "5A0007";
			    break;
			}    
	     }
		 
		 long saldoCuenta = moduloValePagoB.getBuscaSaldo(codOrg.trim(),codCuenta.trim());
	     accionweb.getSesion().setAttribute("saldoCuenta", saldoCuenta+"");
		 
		 accionweb.getSesion().setAttribute("opcion", tipo); 
			            	
	}
	
	public ModuloValePagoB getModuloValePagoB() {
		return moduloValePagoB;
	}


	public void setModuloValePagoB(ModuloValePagoB moduloValePagoB) {
		this.moduloValePagoB = moduloValePagoB;
	}
	
	
	
	

}
