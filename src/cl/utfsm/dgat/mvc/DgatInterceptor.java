package cl.utfsm.dgat.mvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.dgat.modulo.ModuloDgat;
import descad.cliente.CocowBean;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw25DTO;

public class DgatInterceptor extends HandlerInterceptorAdapter{
	private ModuloDgat   moduloDgat;
	public void cargarDgat(AccionWeb accionweb) throws Exception {
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
      	accionweb.agregarObjeto("control", control);
  
		accionweb.agregarObjeto("menuDgat", 1); // es para mostrar el menu  de DGAT
	}
	
	public void cargarProyAsociado(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		String titulo = Util.validaParametro(accionweb.getParameter("titulo"),"");
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
	
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
      	accionweb.agregarObjeto("control", control);
      	List<Presw25DTO> listaPRESW25 = new ArrayList<Presw25DTO>();
      	switch(opcionMenu){
      	case 1:// consulta aasociados
      	{
      	  	listaPRESW25 = moduloDgat.getListaprew25("DG1", rutUsuario, dv, 0, 0, "");
      		break;
      	}
    	case 2:// consulta por a�o
      	{
      		int a�o = Util.validaParametro(accionweb.getParameter("annoControl"), 0);
      	   if(a�o == 0){ // a�o actual
             	Date fecActual = new Date();		     
             	a�o = 1900 + fecActual.getYear() ;
         
             }
      	  	listaPRESW25 = moduloDgat.getListaprew25("DG3", rutUsuario, dv,a�o, 0, "");
      	  	accionweb.agregarObjeto("annoControl", a�o);
      		break;
      	}
    	case 3:// consulta por a�o y cuenta
      	{
      		int a�o = Util.validaParametro(accionweb.getParameter("annoControl"), 0);
      		int codUnidad = Util.validaParametro(accionweb.getParameter("codUnidad"), 0);
      	     
      	  	listaPRESW25 = moduloDgat.getListaprew25("DG4", rutUsuario, dv,a�o, codUnidad, "");
      	  	
      	  	accionweb.agregarObjeto("annoControl", a�o);
      	  	accionweb.agregarObjeto("codUnidad", codUnidad);
      		break;
      	}
    	case 5:// consulta por estado
      	{
      		listaPRESW25 = moduloDgat.getListaprew25("DG6", rutUsuario, dv,0, 0, "" );
      	  	
    		break;
      	}
    	case 6:// consulta por un estado
      	{
      		String codigo = Util.validaParametro(accionweb.getParameter("codigo"),"");
      		listaPRESW25 = moduloDgat.getListaprew25("DG7", rutUsuario, dv,0, 0, codigo );
      	  	accionweb.agregarObjeto("codigo", codigo);
    		break;
      	}
    	case 7:// consulta por cuenta 
      	{
      		int codCuenta = Util.validaParametro(accionweb.getParameter("codCuenta"),0);
      		if (codCuenta == 0)
      			codCuenta = 326010;
      		listaPRESW25 = moduloDgat.getListaprew25("DG5", rutUsuario, dv,0, codCuenta, "" );
      	  	accionweb.agregarObjeto("codCuenta", codCuenta);
    		break;
      	}


 
      	}
    	accionweb.agregarObjeto("menuDgat", 1); // es para mostrar el menu  de DGAT
		accionweb.agregarObjeto("listaProyecto", listaPRESW25);
		accionweb.agregarObjeto("titulo", titulo);
	}
	public void cargarProyecto(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			String codigo = Util.validaParametro(accionweb.getParameter("codigo"), "");
			int control = Util.validaParametro(accionweb.getParameter("control"), 0);
			int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
			String titulo = Util.validaParametro(accionweb.getParameter("titulo"),"");
			

			
			accionweb.agregarObjeto("menuDgat", 1); // es para mostrar el menu  de DGAT
			PreswBean preswbean = new PreswBean("DG2", 0, 0, 0, 0, rutUsuario);
			preswbean.setDigide(dv);
			
			Presw18DTO preswbean18DTO = new Presw18DTO();
			preswbean18DTO.setTippro("DG2");
			preswbean18DTO.setNomtip(codigo);
			
			String idSoli = "";
			idSoli = preswbean.ingreso_presw18_presw19(preswbean18DTO);
			List<Presw25DTO> listPresw25 = new ArrayList<Presw25DTO>();
			listPresw25 = preswbean.buscar_presw25_id(idSoli);
			accionweb.agregarObjeto("listaDetalle", listPresw25);
			
			String nomProyecto = "";
		    String codigoServicio = "";
		    String nombreServicio = "";
		    long cuenta = 0;
		    String nombreCuenta = "";
		    long rutCliente = 0;
		    String digCliente = "";
		    String nombreClienteProyecto = "";
		    long unidadProyecto = 0;
		    String nombreUnidadProyecto = "";
		    long rutResponsableProyecto = 0;
		    String digResponsableProyecto = "";
		    String nombreResponsableProyecto = "";
		    long estimacionFact = 0;
		    long fechaApertura = 0;
		    String estadoProyecto = "";
		    long gastosInformados = 0;
		    long facturado = 0;
		    long liquidado = 0;
		    long resultadoProyecto = 0;
		    String pagina = "";
		    
		    
		
			List<Cocow36DTO>  listaDetalleMovimiento= new ArrayList<Cocow36DTO>();

		
			List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
			CocowBean cocowBean = new CocowBean();
			listCocow36DTO = cocowBean.buscar_cocow36_id(idSoli);
			//System.out.println("listCocow36DTO : "+ listCocow36DTO.size() );
		    if(listCocow36DTO != null && listCocow36DTO.size() >0 ){
		    
		    for(Cocow36DTO ss: listCocow36DTO){	   		 
		    	    
		    	if(ss.getNomcam().trim().equals("NOMPRO"))
		    		nomProyecto = ss.getValalf();	
		  
		    	if(ss.getNomcam().trim().equals("CODSER"))
		    		codigoServicio = ss.getValalf();
		    		
		     	if(ss.getNomcam().trim().equals("DESSER"))
		     		nombreServicio = ss.getValalf();
		     	
		     	if(ss.getNomcam().trim().equals("CODUNI"))
		     		cuenta = ss.getValnu1();
		     	
		     	if(ss.getNomcam().trim().equals("DESUNI"))
		     		nombreCuenta = ss.getValalf();  
		     	
		     	if(ss.getNomcam().trim().equals("RUTCLI"))
		     		rutCliente = ss.getValnu1();   
		     	
	            if(ss.getNomcam().trim().equals("DIGCLI"))
	            	digCliente = ss.getValalf();
	            
	            if(ss.getNomcam().trim().equals("NOMCLI"))
	            	nombreClienteProyecto = ss.getValalf();
	            
	            if(ss.getNomcam().trim().equals("UNIPRO"))
	            	unidadProyecto = ss.getValnu1();
	            
	            if(ss.getNomcam().trim().equals("DESPRO"))
	            	nombreUnidadProyecto = ss.getValalf();
		     	
	            if(ss.getNomcam().trim().equals("RUTRES"))
	            	rutResponsableProyecto = ss.getValnu1();
	            
	            if(ss.getNomcam().trim().equals("DIGRES"))
	            	digResponsableProyecto = ss.getValalf();
	            
	            if(ss.getNomcam().trim().equals("NOMRES"))
	            	nombreResponsableProyecto = ss.getValalf();
	            
	            if(ss.getNomcam().trim().equals("ESTFAC"))
	            	estimacionFact = ss.getValnu1();
	            
	            if(ss.getNomcam().trim().equals("FECAPE"))
	            	fechaApertura = ss.getValnu1();
	            
	            if(ss.getNomcam().trim().equals("ESTPRO"))
	            	estadoProyecto = ss.getValalf();
	            
	            if(ss.getNomcam().trim().equals("GASINF"))
	            	gastosInformados = ss.getValnu1();
	            
	            if(ss.getNomcam().trim().equals("VALFAC"))
	            	facturado = ss.getValnu1();
	            
	            if(ss.getNomcam().trim().equals("VALLIQ"))
	            	liquidado = ss.getValnu1();
	            
	            if(ss.getNomcam().trim().equals("RESPRO"))
	            	resultadoProyecto = ss.getValnu1();
		    }
		  
		    } else {
	    		accionweb.agregarObjeto("mensaje", "Lo sentimos, este proyecto no existe.");
				pagina = "proyAsociados.vm";
			}
		   
		    accionweb.agregarObjeto("nomProyecto",nomProyecto);
		    accionweb.agregarObjeto("codigoServicio",codigoServicio);
		    accionweb.agregarObjeto("nombreServicio",nombreServicio);
		    accionweb.agregarObjeto("cuenta",cuenta);
		    accionweb.agregarObjeto("nombreCuenta",nombreCuenta);
		    accionweb.agregarObjeto("rutCliente",rutCliente);
		    accionweb.agregarObjeto("digCliente",digCliente);
		   	accionweb.agregarObjeto("nombreClienteProyecto",nombreClienteProyecto);
		    accionweb.agregarObjeto("unidadProyecto",unidadProyecto);
		    accionweb.agregarObjeto("nombreUnidadProyecto",nombreUnidadProyecto);
		    accionweb.agregarObjeto("rutResponsableProyecto",rutResponsableProyecto);
		    accionweb.agregarObjeto("digResponsableProyecto",digResponsableProyecto);
		    accionweb.agregarObjeto("nombreResponsableProyecto",nombreResponsableProyecto);
		    accionweb.agregarObjeto("estimacionFact",estimacionFact);
		    accionweb.agregarObjeto("fechaApertura",fechaApertura);
		    accionweb.agregarObjeto("estadoProyecto",estadoProyecto);
		    accionweb.agregarObjeto("gastosInformados",gastosInformados);
		    accionweb.agregarObjeto("facturado",facturado);
			accionweb.agregarObjeto("liquidado", liquidado);
			accionweb.agregarObjeto("resultadoProyecto", resultadoProyecto);
			accionweb.agregarObjeto("codigoProyecto", codigo);
			accionweb.agregarObjeto("control", control);
			accionweb.agregarObjeto("opcionMenu", opcionMenu);
			accionweb.agregarObjeto("titulo", titulo);
	
			
			
	      	accionweb.agregarObjeto("hayDatoslista", "1"); 
		
			}

	

	public ModuloDgat getModuloDgat() {
		return moduloDgat;
	}
	public void setModuloDgat(ModuloDgat moduloDgat) {
		this.moduloDgat = moduloDgat;
	}

	

}
