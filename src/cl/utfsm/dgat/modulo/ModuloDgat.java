package cl.utfsm.dgat.modulo;
/**
 * SISTEMA DE CONTROL DE PROYECTOS  POR INTERNET
 * PROGRAMADOR		: 	M�NICA BARRERA FREZ
 * FECHA ULT.MODIF  : 	21/06/2012
 * UNIDAD DE DESARROLLO INSTITUCIONAL(DTI)   
 */
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.tools.generic.NumberTool;
import cl.utfsm.dgat.datos.DgatDao;
import descad.cliente.PreswBean;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw25DTO;


public class ModuloDgat {
	DgatDao dgatDao;


public List<Presw25DTO> getListaprew25(String tipo, int rutide, String digide, int a�o, int codUnidad, String codigo){
	Collection<Presw19DTO> lista = null;
	List<Presw25DTO> listaPRESW25 = new ArrayList<Presw25DTO>();
	PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutide);
	preswbean.setDigide(digide);
	if(a�o > 0)
		preswbean.setAnopar(a�o);
	if(codUnidad > 0)
		preswbean.setCoduni(codUnidad);

	if(tipo.trim().equals("DG6")){
		Presw25DTO presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("EN PROCESO");
		presw25DTO.setPres01(3);
		
		listaPRESW25 = new ArrayList<Presw25DTO>();
		listaPRESW25.add(presw25DTO);
		
	} else {
		if(tipo.trim().equals("DG7")){
			Presw25DTO presw25DTO = new Presw25DTO();
			presw25DTO.setComen1("proyecto");
			presw25DTO.setComen2("proyectito");
			presw25DTO.setPres01(15032013);
			presw25DTO.setPres02(1500);
			presw25DTO.setPres03(0);
			listaPRESW25 = new ArrayList<Presw25DTO>();
			listaPRESW25.add(presw25DTO);
			
		} else{
			if(tipo.trim().equals("DG5")){
				Presw25DTO presw25DTO = new Presw25DTO();
				presw25DTO.setPres01(2013);
				presw25DTO.setPres02(1500);
				presw25DTO.setPres03(0);
				listaPRESW25 = new ArrayList<Presw25DTO>();
				listaPRESW25.add(presw25DTO);
				
			} else
			listaPRESW25 = (List<Presw25DTO>) preswbean.consulta_presw25();
		}
	}	

return listaPRESW25;
}
			 
public DgatDao getDgatDao() {
		return dgatDao;
	}
	public void setDgatDao(DgatDao dgatDao) {
		this.dgatDao = dgatDao;
	}
public String formateoNumeroEntero(int numero){
					/*formatear n�mero */
				     Locale l = new Locale("es","CL");
				     DecimalFormat formatter1 = (DecimalFormat)DecimalFormat.getInstance(l);
				     formatter1.applyPattern("###,###,###");
				 
				   //  formatter1.format(rs.getDouble("RUTNUM"))   /*sin decimales*/
				 return formatter1.format(numero);
				}
public String formateoNumeroLong(long numero){
					/*formatear n�mero */
				     Locale l = new Locale("es","CL");
				     DecimalFormat formatter1 = (DecimalFormat)DecimalFormat.getInstance(l);
				     formatter1.applyPattern("###,###,###");
				 
				   //  formatter1.format(rs.getDouble("RUTNUM"))   /*sin decimales*/
				 return formatter1.format(numero);
				}
public String eliminaBlancosString(String sTexto){
					/*elimina blancos entre medio*/
				   String linea = "";
				    
				    
				    for (int x=0; x < sTexto.length(); x++) {
				    	  if ((sTexto.charAt(x) != ' ' || 
				    		  (x < (sTexto.length() - 1) && sTexto.charAt(x) == ' '  && sTexto.charAt(x+1) != ' ')) &&
				    		  sTexto.charAt(x) != '\'' &&
				    		  sTexto.charAt(x) != '\"')
				    		  linea += sTexto.charAt(x);
				    	}				
				
				 return linea;
				}
public String eliminaAcentosString(String sTexto){
						/*elimina acentos del string*/
					   String linea = "";  
					    if(!sTexto.trim().equals("")){
					    for (int x=0; x < sTexto.length(); x++) {
					    	  if ((sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') || 
					    	      (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
					    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') || 
					    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
					    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�')){
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'a';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'A';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'e';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'E';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'i';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'I';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'o';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'O';
					    		   if(sTexto.charAt(x) == '�' )
					    			  linea += 'u';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'U';
					    	  }
					    	  else  linea += sTexto.charAt(x);
					    	}				
					    }
					
					 return linea;
					}
}
