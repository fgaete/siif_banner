package cl.utfsm.rrhhI2.datos;

import java.sql.CallableStatement;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;


import cl.utfsm.conexion.ConexionAs400RRHHG;
import cl.utfsm.conexion.ConexionBannerRRHH;

import cl.utfsm.rrhhG.datos.IseriesAppef01DAO;
import cl.utfsm.rrhhG.datos.IseriesApref04DAO;
import cl.utfsm.rrhhG.datos.IseriesApref17DAO;
import cl.utfsm.rrhhG.datos.IseriesApref18DAO;



import cl.utfsm.rrhhG.modulo.IseriesAppef01;
import cl.utfsm.rrhhG.modulo.IseriesApref04;
import cl.utfsm.rrhhG.modulo.IseriesApref17;
import cl.utfsm.rrhhG.modulo.IseriesApref18;
import cl.utfsm.rrhhG.modulo.ResumenNomina;

import cl.utfsm.rrhhG.modulo.RegistroProceso;


public class HibernateInterface2Dao extends HibernateDaoSupport implements Interface2Dao{
	
	private static void cargarTablasTmp(int mesPro , int anoPro ,ConexionBannerRRHH con){
		
		DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
		System.out.print("\n comienso del traspaso de datos:"+hourFormat.format(new Date()));		
		
		List<IseriesAppef01> listPer = IseriesAppef01DAO.listPersona(anoPro, mesPro, "INTERFACE1");
		System.out.print("\n resumen per:"+listPer.size());
		List<IseriesApref04> listCodHaDes = IseriesApref04DAO.listCodHabDesc( "INTERFACE1");
		System.out.print("\n resumen cod haber des:"+listCodHaDes.size());
		List<IseriesApref17> listResNomi= IseriesApref17DAO.listResuNomi(anoPro, mesPro, "INTERFACE1");
		System.out.print("\n resumen nominas:"+listResNomi.size());
		List<IseriesApref18> listDetalle= IseriesApref18DAO.listDetalle(anoPro, mesPro, "INTERFACE1");
		System.out.print("\n detalles:"+listDetalle.size());
		
		System.out.print("\n inicio Personas:"+hourFormat.format(new Date()));
		IseriesAppef01DAO.guardarListPer(listPer, con);
		System.out.print("\n inicio codigos de haberes y descuento:"+hourFormat.format(new Date()));
		IseriesApref04DAO.guardarListCodHades(listCodHaDes, con);
		System.out.print("\n inicio resumen:"+hourFormat.format(new Date()));
		IseriesApref17DAO.guardarListResNom(listResNomi, con);
		System.out.print("\n inicio detalle:"+hourFormat.format(new Date()));
		IseriesApref18DAO.guardarListDetalle(listDetalle, con);
		
		System.out.print("\n Fin del traspaso de datos:"+hourFormat.format(new Date()));
		
	}
	public static String generalArchivos(int mes , int anno , RegistroProceso regPro){
		
		
		
		String resp = "";
		String sql = "{call PKG_TRASPASO_NOMINA.P_USM_GENARAL_ARCHIVOS_NOMINAS(?,?,?,?,?,?)}";
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		
		CallableStatement sent;
		try {			
			con.getConexion().setAutoCommit(false);
			cargarTablasTmp(mes, anno, con);
			
			sent = con.conexion().prepareCall(sql);
			sent.setInt(1,mes);
			sent.setInt(2,anno);
			sent.setInt(3,regPro.getRegproIdEjecucion());
			sent.setInt(4,regPro.getCodProceso());
			sent.setInt(5,regPro.getCodEtapaProceso());
			sent.registerOutParameter(6, OracleTypes.VARCHAR);
			sent.executeQuery();			
			resp = sent.getString(6);
			sent.close();
			con.getConexion().close();
		} catch (SQLException e) {
			System.out.print("HibernateInterface2Dao.generalArchivos : "+e.getMessage());
		}		
		return resp ;
	}
	public static ArrayList<String[]> resumenNomina(int mes , int anno ){
		try {
		
			String sql = " SELECT RESU.CODSUC codsuc , COUNT(*) count , MIN(SUC.DESPLA) descd "                      
						+" ,RESU.CODPLA planta "                                                        
						+" FROM REMUNERABT.APREF32 RESU "                                        
						+" LEFT JOIN REMUNERABT.APREF07 SUC  ON SUC.CODPLA = RESU.CODPLA "       
						+" WHERE ANOPRO = ? AND MESPRO = ? "      
						+" GROUP BY RESU.CODSUC , RESU.CODPLA ";
			ConexionAs400RRHHG con = new ConexionAs400RRHHG();
			String[] cantidades = new String[4];
			HashMap<String, String[]> list = new HashMap<String, String[]>();
			ArrayList<String[]> reg = new ArrayList<String[]>();		
			PreparedStatement sent;		
			sent = con.getConnection().prepareStatement(sql);
			sent.setInt(1,anno);	
			sent.setInt(2,mes);				
			ResultSet res = sent.executeQuery();
			while(res.next()){
				//System.out.print(res.getInt("codsuc")+"-"+res.getString("planta"));
				String[] cop = list.get(res.getInt("codsuc")+"-"+res.getString("planta"));
				if(cop != null &&  (res.getInt(1)== 6 || res.getInt(1) == 4)){
					
					cop[1] = (Integer.parseInt(cop[1])+res.getString(3))+"" ;
					list.put(res.getInt("codsuc")+"-"+res.getString("planta"), cop);
					
				}else{
					cantidades = new String[4];
					cantidades[2] = (res.getInt("codsuc")==6 )? ""+4 : ""+res.getInt("codsuc");
					cantidades[0] = res.getString("planta");		
					cantidades[1] = res.getString("count");
					cantidades[3] = res.getString("descd");
					
	 				list.put(res.getInt("codsuc")+"-"+res.getString("planta"), cantidades);
				}
 				
			}
			for (String[] re : list.values()) {
				reg.add(re) ;
			}
			con.getConnection().setAutoCommit(false);
			con.getConnection().commit();
			con.getConnection().close();
			return reg;
		} catch (SQLException e) {
			System.out.print("SQLHibernateInterface2Dao.resumenNomina : "+e.getMessage());
		} catch (Exception e){
			
			System.out.print("HibernateInterface2Dao.resumenNomina : "+e.getMessage());
		}
		
		return null;
	}
	public static ArrayList<ResumenNomina> resumenNomina(int mes , int anno ,boolean sobre ){
		try {
		
			String sql = " SELECT RESU.CODSUC codsuc , COUNT(*) count , MIN(SUC.DESPLA) descd "                      
						+" ,RESU.CODPLA planta "                                                        
						+" FROM REMUNERABT.APREF32 RESU "                                        
						+" LEFT JOIN REMUNERABT.APREF07 SUC  ON SUC.CODPLA = RESU.CODPLA "       
						+" WHERE ANOPRO = ? AND MESPRO = ? "      
						+" GROUP BY RESU.CODSUC , RESU.CODPLA ";
			ConexionAs400RRHHG con = new ConexionAs400RRHHG();
			ResumenNomina cantidades = new ResumenNomina();
			HashMap<String, ResumenNomina> list = new HashMap<String, ResumenNomina>();
			ArrayList<ResumenNomina> reg = new ArrayList<ResumenNomina>();		
			PreparedStatement sent;		
			sent = con.getConnection().prepareStatement(sql);
			sent.setInt(1,anno);	
			sent.setInt(2,mes);				
			ResultSet res = sent.executeQuery();
			while(res.next()){
				//System.out.print(res.getInt("codsuc")+"-"+res.getString("planta"));
				ResumenNomina cop = list.get(res.getInt("codsuc")+"-"+res.getString("planta"));
				if(cop != null &&  (res.getInt(1)== 6 || res.getInt(1) == 4)){
					
					cop.setCount(cop.getCount()+res.getInt(3));
					list.put(res.getInt("codsuc")+"-"+res.getString("planta"), cop);
					
				}else{
					cantidades = new ResumenNomina();
					cantidades.setCodsuc((res.getInt("codsuc")==6 )? 4 : res.getInt("codsuc"));
					cantidades.setPlanta(res.getInt("planta"));		
					cantidades.setCount(res.getInt("count"));
					cantidades.setDescd( res.getString("descd"));
					
	 				list.put(res.getInt("codsuc")+"-"+res.getString("planta"), cantidades);
				}
 				
			}
			for (ResumenNomina re : list.values()) {
				reg.add(re) ;
			}
			con.getConnection().setAutoCommit(false);
			con.getConnection().commit();
			con.getConnection().close();
			return reg;
		} catch (SQLException e) {
			System.out.print("SQLHibernateInterface2Dao.resumenNomina : "+e.getMessage());
		} catch (Exception e){
			
			System.out.print("HibernateInterface2Dao.resumenNomina : "+e.getMessage());
		}
		
		return null;
	}
	
	
}
