package cl.utfsm.rrhhI2.datos;


import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import oracle.jdbc.OracleTypes;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.RegistroProceso;
import cl.utfsm.rrhhI2.modulo.BasePersona;
public class BasePersonaDAO {

	public BasePersonaDAO(){
		
	}
	
	public List<BasePersona> valisarPersona( int mes ,int anno,RegistroProceso regPro ){
		
		  String sql = "{call PKG_TRASPASO_NOMINA.P_validar_rut(?,?,?,?,?,?,?)}";
		  List<BasePersona> list = new ArrayList<BasePersona>();
		  ConexionBannerRRHH con = new ConexionBannerRRHH();
		  CallableStatement sent;
		  BasePersona baPer ;		 
		  try{ 
		       sent = con.conexion().prepareCall(sql);
		       sent.setInt(2, mes);
		       sent.setInt(3, anno);
		       sent.setInt(4, regPro.getRegproIdEjecucion());
		       sent.setInt(5, regPro.getCodProceso());
		       sent.setInt(6, regPro.getCodEtapaProceso());
		       sent.registerOutParameter(1, OracleTypes.CURSOR);
		       sent.registerOutParameter(7, OracleTypes.CURSOR);
		       sent.executeQuery();
		       ResultSet rs;
		       try {
		    	   // modificacion 21-02-2018 por: Yonis Vergara
				   //rs = ((OracleCallableStatement)sent).getCursor(1);
		    	   rs = (ResultSet)sent.getObject(1);
			       while(rs.next()){
			    	   baPer = new BasePersona();
			    	   baPer.setRut(rs.getString("SPRIDEN_ID"));
			    	   baPer.setNombre(rs.getNString("NOMBRE"));
			    	   baPer.setApellidoPat(rs.getString("APEPAT"));
			    	   baPer.setApellidoMat(rs.getString("APEMAT"));
			    	   baPer.setFechaIni(rs.getDate("FECINI"));	
			    	   baPer.setEstado(rs.getString("estado"));
			    	   list.add(baPer);
			       }
			       rs.close();
			       // modificacion 21-02-2018 por: Yonis Vergara
			       //rs = ((OracleCallableStatement)sent).getCursor(7);
			       rs = (ResultSet)sent.getObject(7);
			       while(rs.next()){
			    	  
			       }
			       rs.close();
		       } catch (SQLException e){
		    	   System.out.println("Error BasePersonaDAO.valisarPersona: " + e);
			       System.out.println("cursor" );
			   }
		       
		       sent.close(); 
		       con.close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error BasePersonaDAO.valisarPersona: " + e);
		       System.out.println("antes del cursor " );
		   }	
		return list;			
		
	}
	public List<List<BasePersona>> valisarPersona( int mes ,int anno,RegistroProceso regPro , ArrayList<ArrayList<String>> listTotales ){
		
		  String sql = "{call PKG_TRASPASO_NOMINA.P_validar_rut(P_RESP => ?" +
		  													  ",PI_MES => ?" +
		  													  ",PI_ANNO => ?" +
		  													  ",PI_EJECUTION => ?" +
		  													  ",PI_PROCESO => ?" +
		  													  ",PI_ETAPA =>?" +
		  													  ",P_TOTALES =>?)}";
		   
		  List<List<BasePersona>> listAll = new ArrayList<List<BasePersona>>();
		  List<BasePersona> listE = new ArrayList<BasePersona>();
		  List<BasePersona> listF = new ArrayList<BasePersona>();
		  List<BasePersona> listP = new ArrayList<BasePersona>();
		  listAll.add(listE);
		  listAll.add(listF);
		  listAll.add(listP);
		  ConexionBannerRRHH con = new ConexionBannerRRHH();
		  CallableStatement sent;
		  BasePersona baPer ;
		
		  
		  try{ 
			   con.getConexion().setAutoCommit(false);
		       sent = con.conexion().prepareCall(sql);
		       sent.setInt(2, mes);
		       sent.setInt(3, anno);
		       sent.setInt(4, regPro.getRegproIdEjecucion());
		       sent.setInt(5, regPro.getCodProceso());
		       sent.setInt(6, regPro.getCodEtapaProceso());
		       sent.registerOutParameter(1, OracleTypes.CURSOR);
		       sent.registerOutParameter(7, OracleTypes.CURSOR);
		       sent.execute();
		       ResultSet rs;
		       try {
		    	   // modificacion 21-02-2018 por: Yonis Vergara
				   //rs = ((OracleCallableStatement)sent).getCursor(1);
		    	   rs = (ResultSet)sent.getObject(1);
			       while(rs.next()){
			    	   baPer = new BasePersona();
			    	   baPer.setRut(rs.getString("SPRIDEN_ID"));
			    	   baPer.setNombre(rs.getNString("NOMBRE"));
			    	   baPer.setApellidoPat(rs.getString("APEPAT"));
			    	   baPer.setApellidoMat(rs.getString("APEMAT"));
			    	   baPer.setFechaIni(rs.getDate("FECINI"));	
			    	   baPer.setEstado(rs.getString("estado"));
			    	   if(baPer.getEstado().equals("E")){
			    		   listE.add(baPer);
			    	   }else if (baPer.getEstado().equals("F")) {
			    		   listF.add(baPer);
			    	   }else if (baPer.getEstado().equals("P")){
			    		   listP.add(baPer);
			    	   }
			       }
			       rs.close();
			       // modificacion 21-02-2018 por: Yonis Vergara
			       //rs = ((OracleCallableStatement)sent).getCursor(7);
			       rs = (ResultSet)sent.getObject(7);
			       
			       ArrayList<String> totales ;
			       while(rs.next()){
			    	   totales = new ArrayList<String>();
			    	   totales.add( rs.getNString(2)) ;
			    	   totales.add( rs.getNString(1)) ;
			    	   listTotales.add(totales);			    	   
			       }
			       rs.close();
		       } catch (SQLException e){
		    	   System.out.println("Error BasePersonaDAO.valisarPersona: " + e);
			       System.out.println("cursor" );
			   }
		       
		       sent.close(); 
		       con.getConexion();
		       con.getConexion().close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error BasePersonaDAO.valisarPersona: " + e);
		       System.out.println("antes del cursor " );
		   }	
		return listAll;			
		
	}
}
