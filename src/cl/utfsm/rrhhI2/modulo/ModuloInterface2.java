package cl.utfsm.rrhhI2.modulo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cl.utfsm.rrhhI2.datos.Interface2Dao;
public class ModuloInterface2 {
	Interface2Dao interface2Dao;
	
	public Interface2Dao getInterface2Dao() {
		return interface2Dao;
	}

	public void setInterface2Dao(Interface2Dao interface2Dao) {
		this.interface2Dao = interface2Dao;
	}	
	public static String tablaNominas(ArrayList<String[]> totales){
		Map<String , String> tablas = new HashMap<String , String>();
		
		try {
			
				
		String tabla = "";//"<table id='tabla' style='width:50%;'>";
		/*tabla        += "	<thead>";
		tabla        += "		<tr>";
		tabla        += "			<th>";
		tabla        += "Tipo contrato";
		tabla        += "			</th>";
		tabla        += "			<th>";
		tabla        += "Cantidad de N�mina";
		tabla        += "			</th>";
		tabla        += "		</tr>";
		tabla        += "	</thead>";
		tabla        += "	<tbody>";
		
		String headEjem = tabla;
		*/
		
		String filas = "";
		String head = "";
		String footer = "";
		
		for (String[] total : totales) {
			
			if(tablas.get(total[2]) == null ){
				//head        = " <div  style='width:49%;'>";
				head        = " <table id='tabla' style='width:100%;'>";
				head        += "	<thead>";
				head        += "		<tr>";
				head        += "			<th colspan='2'>";
				switch (Integer.parseInt(total[2])) {
				case 1:
					head        +="CASA CENTRAL";
					break;
				case 2:
					head        +="VITACURA";
					break;
				case 3:
					head        +="VI�A DEL MAR";
					break;
				case 4:
					head        +="CONCEPCION";
					break;
				case 5:
					head        +="RANCAGUA";
					break;	
				case 6:
					head        +="GUAYAQUIL";
					break;
				case 7:
					head        +="SAN JOAQUIN";
					break;
				default:
					break;
				}
				head        += "			</th>";
				head        += "		</tr>";
				head        += "		<tr>";
				head        += "			<th>";
				head        += "				Tipo contrato";
				head        += "			</th>";
				head        += "			<th>";
				head        += "				Cantidad de N�mina";
				head        += "			</th>";
				head        += "		</tr>";
				head        += "	</thead>";
				head        += "	<tbody>"; 
				tablas.put(total[2],head);
			}	
			
			filas        = "		<tr>";
			filas        += "			<td style='text-align: left;'>";
			filas        += total[0]+": "+ total[3];
			filas        += "			</td>";
			filas        += "			<td style='text-align: right;'>";
			filas        += total[1];
			filas        += "			</td>";
			filas        += "		</tr>";
			
		    tabla =	tablas.get(total[2]) ;
		    tabla += filas ;
		    tablas.put(total[2], tabla);
			
		}
		
		footer        = "	</tbody>";
		
	    footer       += "</table>";
	    //footer       += "</div>";
	    // Buscar la tabala con mayor numero de coluna 
	   
		for (String key : tablas.keySet()) {
			
			tabla = tablas.get(key);
			tabla += footer;
			tablas.put(key, tabla);
		}
		/*
		tabla        += "		<tr>";
		tabla        += "			<th class='bordetop' style=' background: #f4f4f4; text-align: right; font-size:125%;'>";
		tabla        += "				<strong>TOTAL</strong>";
		tabla        += "			</th>";
		tabla        += "			<th  class='bordetop' style=' background: #f4f4f4; text-align: right; font-size:125%;'>";
		tabla        += "				<strong>"+totalNomina+"</strong>";
		tabla        += "			</th>";
		tabla        += "		</tr>";	
		tabla        += "	</tbody>";
		
	    tabla       += "</table>";
		*/
		ArrayList<String> orden = new ArrayList<String>();	
		orden.add("1");
		orden.add("3");
		orden.add("4");		
		
		String result = "";
		int numero = 0;
		for (String keyOr : orden) {
			if(tablas.get(keyOr) != null){
				if(numero % 2 == 0 ){
					result += "<div style='width:100%; overflow: hidden; border-style: solid; border-color: #737171;'>";
					result += "<div style='width:49%; float: left; /*border-style: solid; border-color: #737171;*/'> "+tablas.get(keyOr)+"</div>";
				}		
			
				if(numero % 2 > 0 ){
					result += "<div style='width:49%; float: right; /*border-style: solid; border-color: #737171;*/'> "+tablas.get(keyOr)+"</div>";
					result += "</div><br/>";
				}				
				numero ++;			
			  tablas.remove(keyOr);	
			}
		}
		
		for (String keyOr : tablas.keySet()) {
			if(numero % 2 == 0 ){
				result += "<div style='width:100%; overflow: hidden; border-style: solid; border-color: #737171; '>";
				result += "<div style='width:49%; float: left; /*border-style: solid; border-color: #737171;*/'> "+tablas.get(keyOr)+"</div>";
			}			
						
			if(numero % 2 > 0 ){
				result += "<div style='width:49%; float: right;/* border-style: solid; border-color: #737171;*/'> "+tablas.get(keyOr)+"</div>";
				result += " </div><br/>";			
			}
			numero ++;			
		}
		tablas.clear();
		if(numero % 2 > 0 ){
			result += "</div>";
		}
			
		
		
		return result;
		} catch (Exception e) {
			System.out.print("ModuloInterface2.tablaNominas: " + e.getMessage());
		}
		return "";
	}


}
