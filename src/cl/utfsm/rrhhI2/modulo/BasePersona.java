package cl.utfsm.rrhhI2.modulo;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class BasePersona {
	
	public String rut ;
	public String nombre;
	public String apellidoPat;
	public String apellidoMat;
	public Date   fechaIni;
	public String estado;
	
	public BasePersona() {

	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}
	
	
	
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getRutFormat() {
		
		int largoOriginal = this.rut.length();
		int ultimaPosision  = largoOriginal;
		String rutFormato =  this.rut.substring(0,largoOriginal-1)+"-"+this.rut.substring(largoOriginal-1);
		ultimaPosision --;
		
		while(ultimaPosision > 3){
			
			rutFormato = rutFormato.substring(0,ultimaPosision-3)+"."+rutFormato.substring(ultimaPosision-3);
			ultimaPosision -= 3;			
		}
		
		return rutFormato;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPat() {
		return apellidoPat;
	}

	public void setApellidoPat(String apellidoPat) {
		this.apellidoPat = apellidoPat;
	}

	public String getApellidoMat() {
		return apellidoMat;
	}

	public void setApellidoMat(String apellidoMat) {
		this.apellidoMat = apellidoMat;
	}

	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}
	
	public String fechaFormat(){
		
		SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
		
		return formateador.format(this.fechaIni);
	}
	
	
	
}
