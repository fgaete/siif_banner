package cl.utfsm.rrhhI2.mvc;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;



import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.rrhhG.datos.EtapaProcesoDAO;
import cl.utfsm.rrhhG.datos.HibernateGurfeedDao;
import cl.utfsm.rrhhG.datos.RegistroProcesoDAO;
import cl.utfsm.rrhhG.modulo.EstrucuturaHtml;
import cl.utfsm.rrhhG.modulo.EtapaProceso;

import cl.utfsm.rrhhG.modulo.RegistroProceso;
import cl.utfsm.rrhhI2.datos.BasePersonaDAO;
import cl.utfsm.rrhhI2.datos.HibernateInterface2Dao;
import cl.utfsm.rrhhI2.modulo.BasePersona;
import cl.utfsm.rrhhI2.modulo.ModuloInterface2;

public class Interface2Interceptor  extends HandlerInterceptorAdapter {
	private ModuloInterface2 moduloInterface2;

	public ModuloInterface2 getModuloInterface2() {
		return moduloInterface2;
	}

	public void setModuloInterface2(ModuloInterface2 moduloInterface2) {
		this.moduloInterface2 = moduloInterface2;
	}
	
	public void procesoInterfas2(AccionWeb accionweb) throws Exception {
	try {
		
		EstrucuturaHtml strHtml = new EstrucuturaHtml();
		RegistroProceso regPro = new RegistroProceso();
		RegistroProcesoDAO regproDao = new RegistroProcesoDAO();
		HibernateGurfeedDao herGuDAO = new HibernateGurfeedDao();
		herGuDAO.nomina(accionweb.getSesion());
		int anno = Integer.parseInt(""+accionweb.getSesion().getAttribute("anno"));
		int mes = Integer.parseInt(""+accionweb.getSesion().getAttribute("mes"));
		// obtencion de numrero de registro
		regPro.setRegProNombreResponsable(""+accionweb.getSesion().getAttribute("funcionario"));
		regPro.setRegproRutResponsable(""+accionweb.getSesion().getAttribute("rutUsuario")+accionweb.getSesion().getAttribute("dv"));
		//regPro.setRegProFechaNomina(new Date());
		regPro.setRegProFechaRegistro(new Date());
		regPro.setCodEtapaProceso(0);
		regPro.setCodProceso(2);
		regPro.setRegproIdEjecucion(regproDao.getNumeroEjecusion());
		accionweb.agregarObjeto("num", regPro.getRegproIdEjecucion());
		regPro.setRegProFinalizado("F");
		regproDao.insertRegistroProceso(regPro);
		Map<String, RegistroProceso> mapRegPro = new HashMap<String, RegistroProceso>();
		accionweb.agregarObjeto("contidoTabla", mapRegPro);
		mapRegPro.put(""+regPro.getCodEtapaProceso(), new RegistroProceso(regPro));
		
				
		regPro.setCodEtapaProceso(1);
		regPro.setRegProFinalizado("F");
		regproDao.insertRegistroProceso(regPro);
		mapRegPro.put(""+regPro.getCodEtapaProceso(),new RegistroProceso(regPro));
		
		regPro.setCodEtapaProceso(2);
		regPro.setRegProFinalizado("C");
		regproDao.insertRegistroProceso(regPro);
		
		BasePersonaDAO basePerDAO = new BasePersonaDAO();
		ArrayList<ArrayList<String>> totales = new ArrayList<ArrayList<String>>();
		List<List<BasePersona>> listBase = basePerDAO.valisarPersona(mes, anno,regPro,totales);
		accionweb.agregarObjeto("totaleErrores", totales);
		if(listBase == null  || listBase.isEmpty()){
			
			regPro.setRegProFinalizado("F");
			regproDao.updateRegistroProceso(regPro);
			mapRegPro.put(""+regPro.getCodEtapaProceso(),new RegistroProceso(regPro));
			ArrayList<String[]> resutado =  HibernateInterface2Dao.resumenNomina(mes, anno);
			String tablas =  ModuloInterface2.tablaNominas( resutado);
			accionweb.agregarObjeto("countNomina", tablas);
			
			regPro.setCodEtapaProceso(3);
			regPro.setRegProFinalizado("E");	
			regproDao.insertRegistroProceso(regPro);
			
			mapRegPro.put(""+regPro.getCodEtapaProceso(),new RegistroProceso(regPro));
		}else{
			
			regPro.setRegProFinalizado("C");
			mapRegPro.put(""+regPro.getCodEtapaProceso(),new RegistroProceso(regPro));
			//quitar
			//accionweb.agregarObjeto("tablaError", strHtml.errorValidarRut(listBase, totales));
			accionweb.agregarObjeto("tablaError",listBase);
		} 
		regproDao.updateRegistroProceso(regPro);
		EtapaProcesoDAO etaProDAO = new EtapaProcesoDAO();
		ArrayList<EtapaProceso> listEtaPro = etaProDAO.buscarEtapas(1);
		accionweb.agregarObjeto("listEtapa", listEtaPro);		
		String tabla = strHtml.tableEtapa(mapRegPro,accionweb);
		accionweb.agregarObjeto("listPer", listBase);
		accionweb.agregarObjeto("tabla", tabla);
		accionweb.agregarObjeto("session", accionweb.getSesion());
		
	} catch (Exception e) {
		System.out.print("Error Interface2Interceptor.procesoInterfas2() : "+ e.getMessage() );	
	}
		
	}
	public void generalArchivos(AccionWeb accionweb){
		
		try {			
			
			RegistroProceso regPro = new RegistroProceso();
			RegistroProcesoDAO regproDao = new RegistroProcesoDAO();
			
			// obtencion de numrero de registro
			regPro.setRegProNombreResponsable(""+accionweb.getSesion().getAttribute("funcionario"));
			regPro.setRegproRutResponsable(""+accionweb.getSesion().getAttribute("rutUsuario")+accionweb.getSesion().getAttribute("dv"));
			regPro.setRegProFechaNomina(new Date());
			regPro.setRegProFechaRegistro(new Date());
			regPro.setCodEtapaProceso(3);
			regPro.setCodProceso(2);
			regPro.setRegproIdEjecucion(regproDao.getNumeroEjecusion());
			regPro.setRegProFinalizado("C");
			
			regproDao.insertRegistroProceso(regPro);
			int anno = Integer.parseInt(""+accionweb.getSesion().getAttribute("anno"));
			int mes = Integer.parseInt(""+accionweb.getSesion().getAttribute("mes"));
			String mensaje = HibernateInterface2Dao.generalArchivos(mes, anno, regPro);
			int opti = 0;
			//System.out.print("\n "+mensaje);
			if(mensaje == null ){
				
				mensaje = "Archivo generado correctamente";
				opti = 1;
			}
			accionweb.agregarObjeto("opt", opti);
			accionweb.agregarObjeto("mensaje", mensaje);
			
			
		} catch (Exception e) {
			System.out.print("Interface2Interceptor.generalArchivos :" + e.getMessage());
		}
		
	}
	
	public void cargarMenu(AccionWeb accionweb) throws Exception {
		
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);		
		accionweb.agregarObjeto("esInterface2", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		
		switch (tipo) {
		case 1:
			this.procesoInterfas2(accionweb);
			break;
		case 20:
			this.generalArchivos(accionweb);
			break;
		default:
			break;
		}
		
		
	}

}
