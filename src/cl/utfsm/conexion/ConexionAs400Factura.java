package cl.utfsm.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.sql.DataSource;

//Francisco Gonzalez Molina junio/ 2012

public class ConexionAs400Factura implements java.io.Serializable{
	  private static Connection conexionAs400;
	  private Context context = null;
	  private  DataSource dataSource = null;
	  private Connection con = null;
	    
	  public ConexionAs400Factura(){
	  }
	  public Connection getConnection() {
			try {
				if(con == null || con.isClosed() ){
					  
					 this.con = generaConnection(); 
				}
				
				return this.con;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			  
			  return null;
		  }
		public Connection generaConnection(){	
		  //Asignar Driver
			  try{
		  Class.forName("com.ibm.as400.access.AS400JDBCDriver");		  			  
		  //conexionAs400 =  DriverManager.getConnection("jdbc:as400://200.1.29.75/REMUNERABT","SIIFFACTUR","FACTURSIIF");
		  //conexionAs400 =  DriverManager.getConnection("jdbc:as400://10.2.30.104/REMUNERABT","ORACLEQA","ORACLEIBM");
			 conexionAs400 =  DriverManager.getConnection("jdbc:as400://10.2.30.106/TRANSFERBT","SIIFFACTUR","FACTURSIIF");
	
			
	  }catch(ClassNotFoundException e){
		  System.out.println("EXCEPTION AS400: "+e.toString());			  
	  }catch(SQLException sqlE){
		  System.out.println("EXCEPTION SQL AS400: "+sqlE.toString());			  
	  }
	  
	 
/*
		  DataSource ds = null;  
		  try {
			      Context ctx = new InitialContext();
			      ds = (DataSource) ctx.lookup("java:/DB2-400-SIIFFACTUR");
			      System.out.println("obtiene conexion desde pool as400");
			    }
			    catch (NamingException ne) {
			      System.out.println(ne.toString());
			    }			    
			    try {
			      this.conexionAs400 = ds.getConnection();
			    }
			    catch (Exception e) {
			      System.out.println(e.toString());
			    }
		*/	  		    
		    return conexionAs400;
		    
		 
		  } 
}