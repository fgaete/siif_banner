package cl.utfsm.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.sql.DataSource;

//Francisco Gonzalez Molina junio/ 2012

public class ConexionAs400RRHHI1 implements java.io.Serializable{
	  private static Connection conexionAs400;
	  private Context context = null;
	  private  DataSource dataSource = null;
	  private Connection con = null;
	  public ConexionAs400RRHHI1(){
		  
		  
	  }
	  public Connection getConnection() {
		try {
			if(con == null || con.isClosed() ){
				  
				 this.con = generaConnection(); 
			}
			
			return this.con;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		  return null;
	  }
	  public Connection generaConnection(){
    //Asignar Driver
		  DataSource ds = null;  
		  try {
			      Context ctx = new InitialContext();
			      ds = (DataSource) ctx.lookup("java:/DB2-400-SIIFRRHH1");
			      System.out.println("obtiene conexion desde pool as400");
			    }
			    catch (NamingException ne) {
			      System.out.println(ne.toString());
			    }			    
			    try {
			      this.conexionAs400 = ds.getConnection();
			    }
			    catch (Exception e) {
			      System.out.println(e.toString());
			    }
		 		    
		    return conexionAs400;
		    
		
		  } 
}