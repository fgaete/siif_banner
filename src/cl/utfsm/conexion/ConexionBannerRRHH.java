package cl.utfsm.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import oracle.jdbc.driver.OracleDriver;

import java.sql.*;
import java.util.Locale;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import oracle.jdbc.driver.OracleDriver;

public class ConexionBannerRRHH {
	 
	private String driver,url,user,password;
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	private ResultSetMetaData rsmd = null;
	private boolean _escapeProcessing = true;
	Context context = null;
    DataSource dataSource = null;    
    int conexion = 1;
	public ConexionBannerRRHH()
	{
     if (conexion == 2) {

		    this.driver = "oracle.jdbc.driver.OracleDriver";
			
		/*    this.url = "jdbc:oracle:thin:@bannerdbtestdev.dti.usm.cl:1521:TRNG"; // DB_TEST_JOB
			this.user = "MIGRACION_USM";
			this.password = "u_pick_it";*/
			
			/*
		    this.url = "jdbc:oracle:thin:@bannerdbtestdev.dti.usm.cl:1521:PRODCP"; // DB_pprd
			this.user = "USER_RRHH";
			this.password = "";
			
			this.url = "jdbc:oracle:thin:@bannerdbtestdev.dti.usm.cl:1521:MIGR"; // DB_MIGRA
			this.user = "USER_RRHH";
			this.password = "u_pick_it";*/
	
			try
			{
				DriverManager.registerDriver(new OracleDriver());
				Class.forName(driver).newInstance();
	
				Properties connAttr = new Properties();
	
				Properties props = new Properties();
	
				connAttr.put("USER",this.user);
				connAttr.put("PASSWORD",this.password);
				
		
	
				//System.out.println("SIGA2 ConexionSQL3***");
	
				DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver()); 
				//conexion=DriverManager.getConnection(url,connAttr) ;
				con=DriverManager.getConnection(url,this.user ,this.password);
			    stmt = con.createStatement();
			    stmt.setEscapeProcessing(_escapeProcessing);
			    con.clearWarnings();
			}
			catch (Exception exc)
			{
				System.out.println("Error al tratar de abrir la base de Datos cl.utfsm.base.util.ConexionBannerRRHH"+" : "+exc);
				exc.printStackTrace();
			}
		}
		else {
			try{
	            context = new InitialContext();
	            dataSource = (DataSource) context.lookup("java:/BannerDS");
	            con = dataSource.getConnection();
	            stmt = con.createStatement();
	        }
			catch (NamingException e) {
	            System.out.println("NamingException cl.utfsm.base.util.ConexionBannerRRHH1: " + e);
	            e.printStackTrace();
	        }
			catch(SQLException e){
				System.out.println("Error al tratar de abrir cl.utfsm.base.util.ConexionBannerRRHH: "+e);
				e.printStackTrace();
	        }
		}
        
	}

	public ResultSet retornarConsulta(String Rtquery)

	{
		try
		{
			String user=System.getProperty("user.region");
			System.setProperty("user.region","es");
			Statement query = con.createStatement();
			ResultSet nombres = query.executeQuery (Rtquery);
			con.commit();
			if (user!=null)
				System.setProperty("user.region",user);
			return nombres;
		}

		catch (Exception exc)
		{
			System.out.println("Error al hacer la consulta cl.utfsm.base.util.ConexionBannerRRHH "+" : "+exc);
			return null;
		}
	}

	/**
	 * 
	 * @webmethod 
	 */
	public int ejecutarConsulta(String Stquery)
	{
		try
		{
			String user=System.getProperty("user.region");
			System.setProperty("user.region","es");
			Statement query = con.createStatement();
			query.executeUpdate(Stquery);
			con.commit();
			if (user!=null)
				System.setProperty("user.region",user);
			return 0;
		}
		catch (Exception exc)
		{
			System.out.println("Error al hacer la consulta cl.utfsm.base.util.ConexionBannerRRHH "+" : "+exc);
			return -1;
		}
	}


	public Connection getConexion()
	//Permite retornar la conexi�n
	{
		return con;
	}

	/**
	 * 
	 * @webmethod 
	 */
	public int CerrarConexion()
	{
		try
		{
			con.close(); 
			return 0;
		}
		catch(Exception exc)
		{
			System.out.println("Error al cerrar la conexion cl.utfsm.base.util.ConexionBannerRRHH: "+" : "+exc);
			return -1;
		}
	}

	public void close(){
	      try
	      {
	          if (con != null)
	          {
	              stmt.close();
	              con.close();
	              con = null;
	          }
	      }
	      catch (SQLException sqe)
	      {
	          System.out.println("Unexpected exception cl.utfsm.base.util.ConexionBannerRRHH: " +
	              sqe.toString() + ", sqlstate = " + sqe.getSQLState());
	          sqe.printStackTrace();
	      }
	  }

	public Statement sentencia(){
		return stmt;
	}
	
	public Connection conexion(){
		return con;
	}
	
	private void jbInit() throws Exception {

	}
}
