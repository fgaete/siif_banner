package cl.utfsm.sifB.modulo;
/**
 * SISTEMA DE SOLICITUDES DE FACTURACI�N POR INTERNET
 * PROGRAMADOR		: 	M�NICA BARRERA FREZ
 * FECHA ULT.MODIF  : 	21/06/2012
 * UNIDAD DE DESARROLLO INSTITUCIONAL(DTI)   
 */
/*import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;*/
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Vector;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.tools.generic.NumberTool;

import cl.utfsm.POJO.Sede;
import cl.utfsm.base.AccionWeb;
//import cl.utfsm.base.util.ConexionBD;
import cl.utfsm.base.util.ConexionBanner;
import cl.utfsm.base.util.Util;
import cl.utfsm.conexion.ConexionAs400Factura;
import cl.utfsm.sif.datos.FacturacionDao;
import descad.cliente.CocofBean;
import descad.cliente.Ingreso_Documento;
import descad.cliente.MD5;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Cocof17DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw25DTO;

public class ModuloFacturacionB {
	FacturacionDao facturacionDao;

	public FacturacionDao getFacturacionDao() {
		return facturacionDao;
	}

	public void setFacturacionDao(FacturacionDao facturacionDao) {
		this.facturacionDao = facturacionDao;
	} 
	public List<Presw18DTO> getConsultaAutoriza(int rutnum, String dv){
	  	PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		List<Presw18DTO> listaAutoriza =  new ArrayList<Presw18DTO>();
		String nombre = "";
		if(rutnum > 0){
			preswbean = new PreswBean("PAU",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
			lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			   for (Presw18DTO ss : lista){
				   Presw18DTO presw18DTO = new Presw18DTO();
				   presw18DTO.setIndprc(ss.getIndprc());
				   listaAutoriza.add(presw18DTO);
			   }
					
		}
	
	 return listaAutoriza;
	}
	/* public Collection<Presw25DTO> getVerificaRut(int rutnum, String dv){
		  	PreswBean preswbean = null;
			Collection<Presw25DTO> lista = null;
			if(rutnum > 0){
				preswbean = new PreswBean("VCL",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
				lista = (Collection<Presw25DTO>) preswbean.consulta_presw25();
			}
		
		 return lista;
		}
	 */
	
	 public Collection<Presw25DTO> getVerificaRut(String rutnumdv){
		    ConexionBanner con = new ConexionBanner();
		    PreparedStatement sent = null;
		    ResultSet res = null;
		    
		    String rut = rutnumdv.substring(0,rutnumdv.length()-1);
	        String dig = rutnumdv.substring(rutnumdv.length()-1,rutnumdv.length());
	        if (dig.equals("k")) rutnumdv = rut+"K";
		    
		    List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		    try{
		      
		      sent = con.conexion().prepareStatement("SELECT spbpers_legal_name, spriden_pidm, spraddr_street_line1 || spraddr_street_line2 || spraddr_street_line3, " +
		    		                                 "stvcnty_desc, spraddr_city " + 
		    		                                 "FROM spriden, spbpers, spraddr, stvcnty " +
		    		                                 "WHERE spriden_id = ? " +
		    		                                 "AND spriden_pidm = spbpers_pidm " +
		    		                                 "AND spriden_pidm = spraddr_pidm " +
		    		                                 "AND spraddr_cnty_code  = stvcnty_code " + 
		    		                                 "AND spriden_change_ind is null");
		      sent.setString(1,rutnumdv);
		      res = sent.executeQuery();

		      while (res.next()){
		    	Presw25DTO ss = new Presw25DTO();
		        ss.setDesuni(res.getString(1)); // nombre + apellidos
		        ss.setComen1(res.getString(3));  // domicilio
				ss.setComen2(res.getString(4));  // comuna
				ss.setComen3(res.getString(5));            // ciudad
				//giro = ss.getComen4();
				//error = ss.getItedoc();
				//codBan = ss.getPres03();
				//cuentaBanco = ss.getMotiv1();
				//correo = ss.getMotiv2();
		        sent = con.conexion().prepareStatement("SELECT gxrdird_bank_rout_num, gxvdird_desc, gxrdird_bank_acct_num " +
		        		                               "FROM gxrdird, gxvdird " +
                                                    "WHERE gxrdird_pidm = " + res.getString(2) + 
                                                    "AND gxrdird_bank_rout_num = gxvdird_code_bank_rout_num " +
                                                    "AND gxrdird_priority = 1");	        
		        res = sent.executeQuery();

		        if (res.next()) {
		        	ss.setPres03(res.getInt(1)); // codigo del banco
		        	ss.setComen6(res.getString(2)); // Nombre del banco
		        	ss.setMotiv1(res.getString(3)); // cuenta de deposito
		        }
		        else {
		        	ss.setPres01(0); // codigo del banco 
		        	ss.setComen6(""); // Nombre del banco
		        	ss.setComen5(""); // cuenta de deposito
		        }
		        
		        lista.add(ss);
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloFacturacionB.getVerificaRut: " + e);
			}
		 return lista;
	 }
	
	// MAA Este proceso ya no corre con banner
	/* public Collection<Presw25DTO> getVerificaPagoCuenta(int rutnum, String dv, String cuenta){
		  	PreswBean preswbean = null;
			Collection<Presw25DTO> lista = null;
			if(rutnum > 0){
				//preswbean = new PreswBean("VFC",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"",""); se modifica por el proceso siguiente
				// MAA la respuesta de Pedro Peralta fue "NO VA, est� pendiente como hacerlo en Banner "
				preswbean = new PreswBean("VVU",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
			  //preswbean.setCoduni(cuenta);
				preswbean.setCajero(cuenta.trim());
				lista = (Collection<Presw25DTO>) preswbean.consulta_presw25();
			}
		
		 return lista;
		} */
	/* public Collection<Presw25DTO> getVerificaRutFacturacion(int rutnum, String dv){
		  	PreswBean preswbean = null;
			Collection<Presw25DTO> lista = null;
			if(rutnum > 0){
				preswbean = new PreswBean("VCL",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
				lista = (Collection<Presw25DTO>) preswbean.consulta_presw25();
					}
		
		 return lista;
		}*/
	 
	/* public Collection<Presw18DTO> getVerificaRutIngresador(int rutnum, String dv){
		  	PreswBean preswbean = null;
			Collection<Presw18DTO> lista = null;
			if(rutnum > 0){
				preswbean = new PreswBean("VRU",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
				lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			}
		
		 return lista;
		}
	 */
	 
	 
	 public Collection<Presw18DTO> getVerificaRutIngresador(String rutnumdv){
		    ConexionBanner con = new ConexionBanner();
		    PreparedStatement sent = null;
		    ResultSet res = null;
		    
		    String rut = rutnumdv.substring(0,rutnumdv.length()-1);
	        String dig = rutnumdv.substring(rutnumdv.length()-1,rutnumdv.length());
	        if (dig.equals("k")) rutnumdv = rut+"K";
		    		    
		    List<Presw18DTO> lista = new ArrayList<Presw18DTO>();;
		    try{
		      
		      sent = con.conexion().prepareStatement("SELECT spbpers_legal_name, spriden_pidm FROM spriden, spbpers " +
		    		                                                   "WHERE spriden_id = ? " +
		    		                                                   "AND spriden_pidm = spbpers_pidm " +
		    		                                                   "AND spriden_change_ind is null");
		      sent.setString(1,rutnumdv);
		      res = sent.executeQuery();

		      while (res.next()){
		    	Presw18DTO ss = new Presw18DTO();
		        ss.setDesuni(res.getString(1)); // nombre + apellidos
		        
	
		        sent = con.conexion().prepareStatement("SELECT pebempl_pidm FROM pebempl " +
                                                       "WHERE pebempl_pidm = " + res.getString(2));
		                		        
		        res = sent.executeQuery();
		        
		        if (res.next()) ss.setIndprc("F"); // Fde funcionario
                else  ss.setIndprc("");         
		        	
		        lista.add(ss);
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloFacturacionB.getVerificaRutIngresador: " + e);
			}
		 return lista;
	 }
	 
	 public String getVerificaRutFacturacion(String rutnumdv){
		   // System.out.println(rutnumdv);
		    ConexionBanner con = new ConexionBanner();
		    String nombre ="";
		    
		    String rut = rutnumdv.substring(0,rutnumdv.length()-1);
	        String dig = rutnumdv.substring(rutnumdv.length()-1,rutnumdv.length());
	        if (dig.equals("k")) rutnumdv = rut+"K";
		    
		    
		    try{
		      
		      PreparedStatement sent = con.conexion().prepareStatement("SELECT spbpers_legal_name FROM spriden, spbpers " +
		    		                                                   "WHERE spriden_id = ? " +
		    		                                                   "AND spriden_pidm = spbpers_pidm " +
		    		                                                   "AND spriden_change_ind is null");
		      sent.setString(1,rutnumdv);
		      ResultSet res = sent.executeQuery();

		      if (res.next()){
		        nombre = res.getString(1); // nombre mas apellidos
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloFacturacionB.getVerificaRut: " + e);
			}
		 return nombre;
	 }

	 
	 public void getCuentasAutorizadas( HttpServletRequest req, int rutide){
		 HttpSession sesion = req.getSession();
		 Collection<Presw18DTO> listaPresw18 = null;
		// listaPresw18 = this.getConsulta("VC1", 0, 0, 0, 0, rutide); solicita cambio PP segun correo 9/03/2018
		 listaPresw18 = this.getConsulta("VC2", 0, 0, 0, 0, rutide);
	
		 List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
			
		 for (Presw18DTO ss : listaPresw18){
			 Presw18DTO cuentapresupuestaria = new Presw18DTO();
			 //cuentapresupuestaria.setCoduni(ss.getCoduni());
			 cuentapresupuestaria.setNompro(ss.getNompro());
			 cuentapresupuestaria.setNomtip(ss.getNomtip());
			 cuentapresupuestaria.setDesuni(ss.getDesuni());
			 cuentapresupuestaria.setIndprc(ss.getIndprc());
			 lista.add(cuentapresupuestaria);
		 }
		 
		 
		 sesion.setAttribute("cuentasPresupuestarias", lista);
		return ;
		 }
	 
	 public List<Sede> getListaSedeAutorizadas(int rutUsuario) throws Exception {
			List<Sede> lista = new ArrayList<Sede>();
			ConexionAs400Factura con = new ConexionAs400Factura();
		    Sede sede = new Sede();
			try{
				      
				 PreparedStatement sent = con.getConnection().prepareStatement(" SELECT v1.SUCBAN, v1.DESSUC " +
                         													   " FROM USMMBP.PRESF207 v1, USMMBP.PRESF206 v2" +
                         													   " WHERE v1.SUCBAN = v2.SUCBAN " +
						   													   " AND v2.RUTIDE = ? "); 	
					    	
				 sent.setInt(1,rutUsuario);
				 ResultSet res = sent.executeQuery();
		
				 while (res.next()){
					 sede = new Sede();
					 sede.setCodSuc(res.getInt(1));
					 sede.setNomSuc(res.getString(2).trim()); // 
					 lista.add(sede);
				 }
				 res.close();
				 sent.close();
				 con.getConnection().close();
			   }
			    catch (SQLException e){
				    	System.out.println("Error moduloFacturacionB.getListaSedeAutorizadas: " + e);
				}
			 return lista;
			}
		 
	 
	 
	 public Collection<Presw18DTO> getConsulta(String tippro, int codUnidad, int item, int anno, int mes, int rutIde){
			PreswBean preswbean = null;
			Collection<Presw18DTO> listaPresw18 = null;
			if(item >= 0){
				preswbean = new PreswBean(tippro, codUnidad, item, anno, mes, rutIde);
				listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
				}
			return listaPresw18;
		 }
	 
	 // MAA este proceso ya no se usa en BANNER
	 public List<Presw18DTO> getListaSede( HttpServletRequest req) throws Exception {
			/*	esto es en caso de que se muestren las sedes que puede ver el usuario */
				int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
			
				PreswBean preswbean = null;
				Collection<Presw18DTO> lista = null;
				List<Presw18DTO> list = new ArrayList<Presw18DTO>();
				String nombre = "";
				preswbean = new PreswBean("SED",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
					lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
					   for (Presw18DTO ss : lista){
						   Presw18DTO presw18DTO = new Presw18DTO();
						   presw18DTO.setNummes(ss.getNummes());
						   presw18DTO.setDesuni(ss.getDesuni());
						   list.add(presw18DTO);
					   }	
				   
			return list;
			}
	 // 
	 public List<Presw18DTO> getListaBanco( HttpServletRequest req) throws Exception {
			/*	se muestran todos los bancos */
				int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
			
				PreswBean preswbean = null;
				Collection<Presw18DTO> lista = null;
				List<Presw18DTO> list = new ArrayList<Presw18DTO>();
				String nombre = "";
				preswbean = new PreswBean("BAN",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
					lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
					   for (Presw18DTO ss : lista){
						   Presw18DTO presw18DTO = new Presw18DTO();
						   presw18DTO.setItedoc(ss.getItedoc());
						   presw18DTO.setDesuni(ss.getDesuni());
						   list.add(presw18DTO);
					   }	
				   
			return list;
			}
	/* public List<Presw18DTO> getListaServicio( HttpServletRequest req) throws Exception {
			    int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
			
				PreswBean preswbean = null;
				Collection<Presw18DTO> lista = null;
				List<Presw18DTO> list = new ArrayList<Presw18DTO>();
				String nombre = "";
				preswbean = new PreswBean("TS1",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
					lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
					   for (Presw18DTO ss : lista){
						   Presw18DTO presw18DTO = new Presw18DTO();							
						   if(ss.getIddigi().trim().equals("FEC") || ss.getIddigi().trim().equals("FET") || ss.getIddigi().trim().equals("FEX")){
							   presw18DTO.setTipmov(ss.getTipmov().trim());
							   presw18DTO.setDesuni(ss.getDesuni().trim());
							   presw18DTO.setIddigi(ss.getIddigi().trim());
							   presw18DTO.setNummes(ss.getNummes());
							   list.add(presw18DTO);
						   }
					   }	
				   
			return list;
			}
  */ // MAA no se usa en banner ahora se accesas a la tabla USMMBP.COFAF104B
	 
	 public Collection<Presw18DTO> getListaServicio() throws Exception {
		  ConexionAs400Factura con = new ConexionAs400Factura();
		  List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
		    
		  try {
		    PreparedStatement sent = con.getConnection().prepareStatement("SELECT TIPSER, DESSER, TIPFAC, PORIVA " +
                                                                          "FROM USMMBP.COFAF104B " +
                                                                          "WHERE TIPFAC IN ('FEC','FET','FEX')");
		    ResultSet res = sent.executeQuery();

		     while (res.next()){
		        Presw18DTO ss = new Presw18DTO();
		    	ss.setTipmov(res.getString(1).trim());
				ss.setDesuni(res.getString(2).trim());
				ss.setIddigi(res.getString(3).trim());
				ss.setNummes(res.getInt(4));
		        lista.add(ss);
		      }
		      res.close();
		      sent.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloFacturacionB.getListaServicio: " + e);
			}
		 return lista;
	 }
	 /*
	 public Collection<Presw18DTO> getListaPeriodo() throws Exception {
		  ConexionBanner con = new ConexionBanner();
		  List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
		    
		  try {
		    PreparedStatement sent = con.conexion().prepareStatement("SELECT STVTERM_CODE, STVTERM_DESC " +
		    		                                                 "FROM STVTERM " +
		    		                                                 "ORDER BY STVTERM_CODE");
		    ResultSet res = sent.executeQuery();

		     while (res.next()){
		        Presw18DTO ss = new Presw18DTO();
		    	ss.setTipmov(res.getString(1).trim());
				ss.setDesuni(res.getString(2).trim());
				lista.add(ss);
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloFacturacionB.getListaPeriodo: " + e);
			}
		 return lista;
	 }
	*/ public Collection<Presw18DTO> getListaPeriodo(int pidm) throws Exception {
		  ConexionBanner con = new ConexionBanner();
		  List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
		    
		  try {
			  String query = "SELECT DISTINCT STVTERM_CODE, STVTERM_DESC " +
							 "FROM TBRACCD ,STVTERM,TBBDETC, TVRSDDC " +
							 "WHERE TBRACCD_PIDM = " + pidm +
							 "AND tbraccd_balance > 0" +
							 "AND STVTERM_CODE = TBRACCD_TERM_CODE " +
							 "AND TBBDETC_TYPE_IND = 'C' " +
							 "AND TBBDETC_DCAT_CODE IN ('FEE','TUI') " +
							 "AND TBBDETC_DETAIL_CODE = TVRSDDC_DETAIL_CODE " + 
							 "AND TBBDETC_DETAIL_CODE = TBRACCD_DETAIL_CODE " +
							 "AND TVRSDDC_SDOC_CODE = 'SE' ";
			  System.out.println(query);
		    PreparedStatement sent = con.conexion().prepareStatement(query);
		    ResultSet res = sent.executeQuery();

		     while (res.next()){
		        Presw18DTO ss = new Presw18DTO();
		    	ss.setTipmov(res.getString(1).trim());
				ss.setDesuni(res.getString(2).trim());
				lista.add(ss);
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloFacturacionB.getListaPeriodo: " + e);
			}
		 return lista;
	 }
	/* public Collection<Presw18DTO> getListaDetalle() throws Exception {
		  ConexionBanner con = new ConexionBanner();
		  List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
		    
		  try {
		    PreparedStatement sent = con.conexion().prepareStatement(" SELECT TBBDETC_DETAIL_CODE, TBBDETC_DESC " +
		    		                                                 " FROM TBBDETC , TVRSDDC " + 
                                                                     " WHERE TBBDETC_TYPE_IND = 'C' " +   // Para facturas Cargo
                                                                     " AND TBBDETC_DCAT_CODE IN ('FEE','TUI') " +
                                                                     " AND TBBDETC_DETAIL_CODE = TVRSDDC_DETAIL_CODE " +
                                                                     " AND TVRSDDC_SDOC_CODE = 'SE' " + // Servicios Educacionales
                                                                     " ORDER BY TBBDETC_DETAIL_CODE ");
		    ResultSet res = sent.executeQuery();

		     while (res.next()){
		        Presw18DTO ss = new Presw18DTO();
		    	ss.setTipmov(res.getString(1).trim());
				ss.setDesuni(res.getString(2).trim());
				lista.add(ss);
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloFacturacionB.getListaDetalle: " + e);
			}
		 return lista;
	 }*/
	 public Collection<Presw18DTO> getListaDetalle(int pidm, String periodo) throws Exception {
		  ConexionBanner con = new ConexionBanner();
		  List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
		    
		  try {
		    PreparedStatement sent = con.conexion().prepareStatement(" SELECT DISTINCT TBBDETC_DETAIL_CODE, TBBDETC_DESC" + 
		    														" FROM TBRACCD, TBBDETC , TVRSDDC" +  
		    														" WHERE TBRACCD_PIDM = " + pidm + 
		    														" AND TBRACCD_TERM_CODE = '" + periodo + "'" +
																	" AND  TBBDETC_TYPE_IND = 'C'" + 
																	" AND TBBDETC_DCAT_CODE IN ('FEE','TUI')" + 
																	" AND TBBDETC_DETAIL_CODE = TVRSDDC_DETAIL_CODE" + 
																	" AND TBBDETC_DETAIL_CODE = TBRACCD_DETAIL_CODE" +
																	" AND TVRSDDC_SDOC_CODE = 'SE'" + 
																	" AND tbraccd_balance > 0" +
																	" ORDER BY TBBDETC_DETAIL_CODE ");
		  /*  System.out.println("getListaDetalle:  SELECT DISTINCT TBBDETC_DETAIL_CODE, TBBDETC_DESC" + 
		    														" FROM TBRACCD, TBBDETC , TVRSDDC" +  
		    														" WHERE TBRACCD_PIDM = " + pidm + 
		    														" AND TBRACCD_TERM_CODE = '" + periodo + "'" +
																	" AND  TBBDETC_TYPE_IND = 'C'" + 
																	" AND TBBDETC_DCAT_CODE IN ('FEE','TUI')" + 
																	" AND TBBDETC_DETAIL_CODE = TVRSDDC_DETAIL_CODE" + 
																	" AND TBBDETC_DETAIL_CODE = TBRACCD_DETAIL_CODE" +
																	" AND TVRSDDC_SDOC_CODE = 'SE'" + 
																	" AND tbraccd_balance > 0" +
																	" ORDER BY TBBDETC_DETAIL_CODE ");
																	*/
		    ResultSet res = sent.executeQuery();

		     while (res.next()){
		        Presw18DTO ss = new Presw18DTO();
		    	ss.setTipmov(res.getString(1).trim());
				ss.setDesuni(res.getString(2).trim());
				lista.add(ss);
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloFacturacionB.getListaDetalle: " + e);
			}
		 return lista;
	 }
	 public String getObtieneCuentaServicio(String tipoServicio) throws Exception {
		  ConexionAs400Factura con = new ConexionAs400Factura();
		  String cuenta = "";
		    
		  try {
		    PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CUEBAN FROM USMMBP.COFAF104B" +
                                                                          " WHERE TIPSER = ? ");
		    sent.setString(1,tipoServicio);
		    ResultSet res = sent.executeQuery();

		     while (res.next()){
		        cuenta = res.getString(1).trim();
	         }
		      res.close();
		      sent.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloFacturacionB.getObtieneCuentaServicio: " + e);
			}
		 return cuenta;
	 }
	 
	 public String getObtieneServivio(int numdoc) throws Exception {
		  ConexionAs400Factura con = new ConexionAs400Factura();
		  String tipoServ = "";
		    
		  try {
		    PreparedStatement sent = con.getConnection().prepareStatement(" SELECT TIPSER FROM USMMBP.COFAF01B" +
                                                                          " WHERE NUMINT = ? ");
		    sent.setInt(1,numdoc);
		    ResultSet res = sent.executeQuery();

		     while (res.next()){
		    	 tipoServ = res.getString(1).trim();
	         }
		      res.close();
		      sent.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloFacturacionB.getObtieneServivio: " + e);
			}
		 return tipoServ;
	 }
	 
	 public String getObtieneOrganizacion(int numdoc) throws Exception {
		  ConexionAs400Factura con = new ConexionAs400Factura();
		  String codOrg = "";
		    
		  try {
		    PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODORG FROM USMMBP.COCOF04B" +
                                                                          " WHERE NUMDOC = ? ");
		    sent.setInt(1,numdoc);
		    ResultSet res = sent.executeQuery();

		     while (res.next()){
		    	 codOrg = res.getString(1).trim();
	         }
		      res.close();
		      sent.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloFacturacionB.getObtieneOrganizacion: " + e);
			}
		 return codOrg;
	 }
	 
	 public String getObtieneDescServicio(String tipoServicio) throws Exception {
		  ConexionAs400Factura con = new ConexionAs400Factura();
		  String desc = "";
		    
		  try {
		    PreparedStatement sent = con.getConnection().prepareStatement("  SELECT DESSER FROM USMMBP.COFAF104B" +
                                                                          "  WHERE TIPSER = ? ");
		    sent.setString(1,tipoServicio);
		    ResultSet res = sent.executeQuery();

		     while (res.next()){
		        desc = res.getString(1).trim();
	         }
		      res.close();
		      sent.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloFacturacionB.getObtieneDescServicio: " + e);
			}
		 return desc;
	 }
	 
	 public String getObtieneCodigoDetalleFacturaAS(String tipser, String codorg) throws Exception {
		  ConexionAs400Factura con = new ConexionAs400Factura();
		  String codDetFac = "";
		    
		  try {
		    PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODDET FROM USMMBP.COCOF161B" +
                                                                          " WHERE TIPSER = ? " +
                                                                          " AND CODORG = ? ");
		    sent.setString(1,tipser);
		    sent.setString(2,codorg);
		    ResultSet res = sent.executeQuery();

		     while (res.next()){
		    	 codDetFac = res.getString(1).trim();
	         }
		      res.close();
		      sent.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloFacturacionB.getObtieneCodigoDetalleFactura: " + e);
			}
		 return codDetFac;
	 }
	 
	 public Vector getObtieneFondoyPrograma(String codorg) {
		  ConexionAs400Factura con = new ConexionAs400Factura();
		  Vector vec = new Vector();
		    
		  try {
		    PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODFON, CODPRO FROM USMMBP.PRESF200" +
                                                                          " WHERE CODORG = ? ");
		    sent.setString(1,codorg);
		    ResultSet res = sent.executeQuery();

		     while (res.next()){
		      vec.add(res.getString(1).trim());
		      vec.add(res.getString(2).trim());
	         }
		      res.close();
		      sent.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e) { System.out.println("Error moduloFacturacionB.getObtieneFondoYPrograma: " + e);	}
		 return vec;
	 }
	 
		 
	 public Vector getObtieneDatosFactura(int pidm, int numDoc) {
		  ConexionBanner con = new ConexionBanner();
		  PreparedStatement sent;
		  Vector vec = new Vector();
		  Vector v = null;
		    
		  try {
			  sent = con.conexion().prepareStatement(" SELECT TBRACCD_PIDM, TBRACCD_BALANCE, TBRACCD_TRAN_NUMBER, TBRACCD_DETAIL_CODE, TBRAPPL_PAY_TRAN_NUMBER,SUM(TBRAPPL_AMOUNT) " +
                                                     " FROM TBRACCD, TBRAPPL " + 
                                                     " WHERE TBRACCD_PIDM = " + pidm +
                                                     " AND TBRACCD_PIDM = TBRAPPL_PIDM " +
                                                     " AND TBRACCD_TRAN_NUMBER = TBRAPPL_PAY_TRAN_NUMBER " + 
                                                     " AND TBRACCD_INVOICE_NUMBER = " + numDoc);	        
			  ResultSet res = sent.executeQuery();

			  while (res.next()) {
				 v = new Vector(); 
				 v.add(res.getString(2)); // balance 
				 v.add(res.getString(4));	// codigo detalle	 
				 v.add(res.getString(6)); // monto pago
				 vec.addElement(v);
			  }
             res.close();
             sent.close();
             con.close();
          }
          catch (SQLException e){
              System.out.println("Error moduloFacturacionB.getObtieneDatosFactura: " + e);
           }
       return vec;
	 }
	 
	 public Vector getObtieneCodDetCheque(){
		  ConexionBanner con = new ConexionBanner();
		  PreparedStatement sent;
		  Vector vec = new Vector();
		    
		  try {
			  sent = con.conexion().prepareStatement(" SELECT TBBDETC_DETAIL_CODE " +
					  								 " FROM TBBDETC " + 
                                                     " WHERE TBBDETC_DCAT_CODE = 'CHE' " +
                                                     " AND SUBSTR(TBBDETC_DETAIL_CODE,1,2) = 'CO' " +
                                                     " AND TBBDETC_TYPE_IND = 'C' ");	        
			  ResultSet res = sent.executeQuery();

			  while (res.next()) {
				 vec.addElement(res.getString(1)); // codigo detalle
			  }
            res.close();
            sent.close();
            con.close();
         }
         catch (SQLException e){
             System.out.println("Error moduloFacturacionB.getObtieneCodDetCheque: " + e);
          }
      return vec;
	 }
	 
	 public String getObtieneCodigoDetalleFacturaBN(String codOrg, String codFondo, String codProg){
		  ConexionBanner con = new ConexionBanner();
		  PreparedStatement sent;
		  String cod = "";
		    
		  try {
			  sent = con.conexion().prepareStatement(" SELECT TBRACCT_DETAIL_CODE " +
                      								 " FROM TBRACCT, TBBDETC " +
                      								 " WHERE TBRACCT_DETAIL_CODE = TBBDETC_DETAIL_CODE " +
                      								 " AND SUBSTR(TBRACCT_DETAIL_CODE,1,1) = 'F' " +
                      								 " AND TBRACCT_A_ORGN_CODE = '" + codOrg + "'" +
                      								 " AND TBRACCT_A_FUND_CODE = '" + codFondo + "'" +
                      								 " AND TBRACCT_A_PROG_CODE = '" + codProg + "'" +
                      								 " AND TBBDETC_DETC_ACTIVE_IND = 'Y' " +
                      								 " AND TBBDETC_TYPE_IND = 'C' ");	        
			  ResultSet res = sent.executeQuery();

			  while (res.next()) {
				 cod = res.getString(1); // codigo detalle
			  }
           res.close();
           sent.close();
           con.close();
        }
        catch (SQLException e){
            System.out.println("Error moduloFacturacionB.getObtieneCodigoDetalleFacturaBN: " + e);
         }
     return cod;
	 }
	 
	 public String getRevisaExisteCodDetalleBANNER(String codDetalle){
		  ConexionBanner con = new ConexionBanner();
		  PreparedStatement sent;
		  String cod = "";
		    
		  try {
			  sent = con.conexion().prepareStatement(" SELECT TBBDETC_DETAIL_CODE " +
                     								 " FROM TBBDETC " +
                     								 " WHERE TBBDETC_DETAIL_CODE = '" + codDetalle + "'" +
                     								 " AND TBBDETC_TYPE_IND = 'C' ");	        
			  ResultSet res = sent.executeQuery();

			  while (res.next()) {
				 cod = res.getString(1); // codigo detalle
			  }
          res.close();
          sent.close();
          con.close();
       }
       catch (SQLException e){
           System.out.println("Error moduloFacturacionB.getRevisaExisteCodDetalleBANNER: " + e);
        }
    return cod;
	 }
	 
	 public String getRevisaExisteCodDetalleAS(String codDetalle, String tipoServ, String codOrg) throws Exception {
		  ConexionAs400Factura con = new ConexionAs400Factura();
		  String cod = "";
		    
		  try {
		    PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODDET FROM USMMBP.COCOF161B" +
                                                                          " WHERE CODDET =  ? " +
                                                                          " AND TIPSER = ? "+
                                                                          " AND CODORG = ? ");
		    sent.setString(1,codDetalle);
		    sent.setString(2,tipoServ);
		    sent.setString(3,codOrg);
		    ResultSet res = sent.executeQuery();

		     if (res.next()){
		    	cod = res.getString(1).trim();
	         }
		      res.close();
		      sent.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloFacturacionB.getRevisaExisteCodDetalleAS: " + e);
			}
		 return cod;
	 }
	 
	 
	 
	 public String getObtieneCodDetFacBanner() throws Exception {
		 ConexionBanner con = new ConexionBanner();
		 CallableStatement sent;
		 int error = -1;
		 String codDetFac = "";
		 try{
		       sent = con.conexion().prepareCall("{call P_USM_GENERA_CODDETALLE_SIIF(?)}");
		       sent.registerOutParameter(1, Types.VARCHAR);
		         
		       sent.executeQuery();
		       codDetFac = sent.getString(1);
		       sent.close(); 
		       con.close();
		     }
		    catch (SQLException e){
			    System.out.println("Error moduloFacturacionB.getObtieneCodDetFacBanner: " + e);
			}
		    
		    
		 return codDetFac;
	 }
	 
	 public boolean saveCodDetFacBanner(String codDetalle, String desc, String rutDv,String codOrg80, String codCuenta80,String codFondo80, String codProg80) {
		ConexionBanner con = new ConexionBanner();
		CallableStatement sent;
		int error = -1;
		boolean inserta = false;
		int porcentaje = 0;
		String codOrg20 = "";
		String codCuenta20 = ""; 
		String codFondo20 = "";
		String codProg20  = "";
		
		porcentaje = 80;
		codOrg20 = "0A1015";
		codFondo20 = "CA6224";
		codProg20  = "IN0002";
		codCuenta20 = "12A006";
				
		try{
		 System.out.println("p_usm_tbbdetc_tran_siif("+codDetalle+","+desc+","+rutDv+","+codOrg80+","+codCuenta80+","+codFondo80+","+codProg80+","+
		                    porcentaje+","+codOrg20+","+codCuenta20+","+codFondo20+","+codProg20);
			sent = con.conexion().prepareCall("{call p_usm_tbbdetc_tran_siif(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			sent.setString(1,codDetalle);
			sent.setString(2,desc);
			sent.setString(3,rutDv);
			sent.setString(4,codOrg80);
			sent.setString(5,codCuenta80);
			sent.setString(6,codFondo80);
			sent.setString(7,codProg80);
			sent.setInt(8,porcentaje);
			sent.setString(9,codOrg20);
			sent.setString(10,codCuenta20);
			sent.setString(11,codFondo20);
			sent.setString(12,codProg20);
			sent.registerOutParameter(13,Types.INTEGER);
		
			sent.executeQuery();
			error = sent.getInt(13);
			if (error == 0)
				inserta = true;
			else inserta = false;
			 
			sent.close(); 
			con.close();
			}
			catch (SQLException e){
			System.out.println("Error moduloFacturacionB.saveCodDetFacBanner: " + e);
			}
		return inserta;
}
	
	 public String getObtienePrograma(String codOrg, String codFondo){
		   // System.out.println(rutnumdv);
		    ConexionAs400Factura con = new ConexionAs400Factura();
		    String codProg ="";
		    try{
		      
		      PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODPRO FROM USMMBP.PRESF200 " +
                                                                       " WHERE CODORG = ? " +
                                                                       " AND CODFON = ? ");
		      sent.setString(1,codOrg);
		      sent.setString(1,codFondo);
		      ResultSet res = sent.executeQuery();

		      if (res.next()){
		        codProg = res.getString(1); // nombre mas apellidos
		      }
		      res.close();
		      sent.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloFacturacionB.getObtienePrograma: " + e);
			}
		 return codProg;
	 }
	
	 
	 public boolean saveCodDetFacAS(String codOrg, String tipSer, String codDet) throws Exception {
	 ConexionAs400Factura con = new ConexionAs400Factura();
	 boolean inserta = false;
	 Calendar fecha = new GregorianCalendar();
     int a�o = fecha.get(Calendar.YEAR);
     int mes = fecha.get(Calendar.MONTH)+1;
     int dia = fecha.get(Calendar.DAY_OF_MONTH);
     int fechaAc = Integer.parseInt(String.valueOf(a�o) + String.valueOf((mes+1)) + String.valueOf(dia));
     try {
          PreparedStatement sent = con.getConnection().prepareStatement("INSERT INTO USMMBP.COCOF161B( " +
        		                                                        "CODORG, " +
        		                                                        "TIPSER, " +
        		                                                        "CODDET, " +
        		                                                        "FECCRE ) " +
        																"VALUES(?,?,?,?)"); 
          sent.setString(1,codOrg);
          sent.setString(2,tipSer);
          sent.setString(3,codDet);
          sent.setInt(4,fechaAc);
      
          int res = sent.executeUpdate();
          if(res < 0)
         	inserta = false;    	  
          else inserta = true;
	    
	     sent.close();
	     con.getConnection().close();

	    }
	    catch (SQLException e ) {
	      System.out.println("Error en moduloFacturacionB.saveCodDetFacAS COCOF161B : " + e );
	    } 
     return inserta;
	 }  

	 public List<Presw18DTO> getListaCondicionVenta( HttpServletRequest req) throws Exception {
			/*	esto es en caso de que se muestren los plazos de pago */
				int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
			
				PreswBean preswbean = null;
				Collection<Presw18DTO> lista = null;
				List<Presw18DTO> list = new ArrayList<Presw18DTO>();
				String nombre = "";
				preswbean = new PreswBean("GLV",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
					lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
					   for (Presw18DTO ss : lista){
						   Presw18DTO presw18DTO = new Presw18DTO();
						   presw18DTO.setItedoc(ss.getItedoc());
						   presw18DTO.setDesuni(ss.getDesuni().trim());
						   list.add(presw18DTO);
					   }	
				   
			return list;
			}
		 public boolean saveDeleteIngresador( int rut, String dv, String codorg, String tipcue ){
			  	PreswBean preswbean = null;
				Collection<Presw18DTO> lista = null;
				long saldoCuenta = 0;
				//preswbean = new PreswBean("ALI",0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
				// MAA se dejo el mismo proceso que los VALES DE PAGO
				preswbean = new PreswBean("AL1",0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
				preswbean.setRutide(rut);
				preswbean.setDigide(dv);
				preswbean.setCajero(codorg);
				preswbean.setTipcue(tipcue);
				
				return preswbean.ingreso_presw19();	
		 } 
		 
		 
		 
		 
		 public void getAgregaListaSolicitaFactura( HttpServletRequest req){
			 HttpSession sesion = req.getSession();
		     long valor = Util.validaParametro(req.getParameter("valor"), 0);
		     String descripcion = Util.validaParametro(req.getParameter("descripcion"), "");
		     String descLimpia = this.eliminaBlancosString(descripcion);	
		     if(descLimpia.length() > 40)
		    	 descLimpia = descLimpia.substring(0,39);
		 	 List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudes");
			 if(listaSolicitudes != null && listaSolicitudes.size()> 0){
				 sesion.removeAttribute("listaSolicitudes");
			     sesion.setAttribute("listaSolicitudes", null);
					
			 }
			 if(listaSolicitudes == null)
					listaSolicitudes = new ArrayList<Presw18DTO>();
			 if(listaSolicitudes.size() < 20){
				 		Presw18DTO presw18DTO = new Presw18DTO();
				 		presw18DTO.setDesuni(descLimpia.toUpperCase());
				 		presw18DTO.setUsadom(valor);
				 		listaSolicitudes.add(presw18DTO);
			 } 
			 
			 sesion.setAttribute("listaSolicitudes", listaSolicitudes);
				
		 }
		 public void getEliminaListaSolicitaFact( HttpServletRequest req){
			 HttpSession sesion = req.getSession();
		     long valor = Util.validaParametro(req.getParameter("usadom"), 0);
		     String desuni = Util.validaParametro(req.getParameter("desuni"),"");
		     
			 List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudes");
			 List<Presw18DTO> listaFacturas = new ArrayList<Presw18DTO>();
			 if(listaSolicitudes != null && listaSolicitudes.size()> 0){
				 sesion.removeAttribute("listaSolicitudes");
			     sesion.setAttribute("listaSolicitudes", null);
					
			 }
			 if(listaSolicitudes != null && listaSolicitudes.size() > 0){
				 for (Presw18DTO ss: listaSolicitudes){
					 if(ss.getUsadom() == valor && ss.getDesuni().trim().equals(desuni.trim()))
						 continue;
					 else
					 {
						 Presw18DTO presw18DTO = new Presw18DTO();
						 presw18DTO.setDesuni(ss.getDesuni());
						 presw18DTO.setUsadom(ss.getUsadom());
						 listaFacturas.add(presw18DTO);
					 }
				 }
				 
			
			 }
			 //if(listaFacturas.size() > 0)
			   sesion.setAttribute("listaSolicitudes", listaFacturas);
				
		 }
		  public  void getModificaSolicitaFact( HttpServletRequest req){
			 HttpSession sesion = req.getSession();
		     long valor = Util.validaParametro(req.getParameter("usadom"), 0);
		     String desuni = Util.validaParametro(req.getParameter("desuni"),"");
		     long valorNuevo = Util.validaParametro(req.getParameter("usadomNuevo"), 0);
		     String desuniNuevo = Util.validaParametro(req.getParameter("desuniNuevo"),"");
		     String descLimpia = this.eliminaBlancosString(desuniNuevo);	
			 	  
			 List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudes");
			 List<Presw18DTO> listaFacturas = new ArrayList<Presw18DTO>();
			 if(listaSolicitudes != null && listaSolicitudes.size()> 0){
				 sesion.removeAttribute("listaSolicitudes");
			     sesion.setAttribute("listaSolicitudes", null);
			 }
			 if(listaSolicitudes != null && listaSolicitudes.size() > 0){
				 for (Presw18DTO ss: listaSolicitudes){
					 if(ss.getUsadom() == valor && ss.getDesuni().trim().equals(desuni.trim())){
						 Presw18DTO presw18DTO = new Presw18DTO();
						 presw18DTO.setDesuni(descLimpia);
						 presw18DTO.setUsadom(valorNuevo);
						 listaFacturas.add(presw18DTO);
					 } else
					 {
						 Presw18DTO presw18DTO = new Presw18DTO();
						 presw18DTO.setDesuni(ss.getDesuni());
						 presw18DTO.setUsadom(ss.getUsadom());
						 listaFacturas.add(presw18DTO);
					 }
				 }
				 
			
			 }
			 //if(listaFacturas.size() > 0)
			   sesion.setAttribute("listaSolicitudes", listaFacturas);
				
		 }
		 public int getRegistraFactura(HttpServletRequest req, int rutide, String digide){
				int numFactura = 0;
				List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
				listCocow36DTO = this.getCreaCocow36(req, rutide, digide, "GSX");
				Ingreso_Documento ingresoDocumento = new Ingreso_Documento("GSX",rutide,digide);
					
				numFactura = ingresoDocumento.Ingresar_Documento(listCocow36DTO);
				
			
				return numFactura;
			}
		 public List<Cocow36DTO> getCreaCocow36(HttpServletRequest req, int rutide, String digide, String tipoProceso){
				List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) req.getSession().getAttribute("listaSolicitudes");
				String nombreCliente = Util.validaParametro(req.getParameter("nombreCliente"), "");
				long rutnum = Util.validaParametro(req.getParameter("rutnum"), 0);
				String dvrut = Util.validaParametro(req.getParameter("dvrut"),"");
				String direccionCliente = Util.validaParametro(req.getParameter("direccionCliente"),"");
				String comunaCliente = Util.validaParametro(req.getParameter("comunaCliente"),"");
				String ciudadCliente = Util.validaParametro(req.getParameter("ciudadCliente"),"");
				String giroCliente = Util.validaParametro(req.getParameter("giroCliente"),"");
				String referenciaAtt = Util.validaParametro(req.getParameter("referenciaAtt"),"");
				String condicionVenta = Util.validaParametro(req.getParameter("condicionVenta"),"");
				String tipoServicio = Util.validaParametro(req.getParameter("tipoServicio"),"");
				String tipoDocumento = Util.validaParametro(req.getParameter("afecto"),"");
				String desuni = Util.validaParametro(req.getParameter("desuni"),"");
				/*MB agrega 20190423 segun definicion*/
				int fechaInicio = Util.validaParametro(req.getParameter("fechaInicio"),0);
				int fechaTermino = Util.validaParametro(req.getParameter("fechaTermino"),0);
				String numRef = Util.validaParametro(req.getParameter("numRef"),"");
				if(numRef!= null && numRef.trim().length()> 20)
					numRef = numRef.substring(0,20);						
				String numHes = Util.validaParametro(req.getParameter("numHes"),"");
				if(numHes!= null && numHes.trim().length()> 20)
					numHes = numRef.substring(0,20);
				/**/
				long  rutAlu = Util.validaParametro(req.getParameter("rutAlu"),0);
				String digAlu = Util.validaParametro(req.getParameter("digAlu"),"");
				long periodo = Util.validaParametro(req.getParameter("periodoFac"),0);
				String codDetalle = Util.validaParametro(req.getParameter("codDetalleFac"), "");
				nombreCliente = this.eliminaBlancosString(nombreCliente);
				comunaCliente = this.eliminaBlancosString(comunaCliente);
				direccionCliente = this.eliminaBlancosString(direccionCliente);
				giroCliente = this.eliminaBlancosString(giroCliente);
				if(giroCliente.trim().length() > 70)
					giroCliente = giroCliente.substring(0,70);
				ciudadCliente = this.eliminaBlancosString(ciudadCliente);
				referenciaAtt = this.eliminaBlancosString(referenciaAtt);
				condicionVenta = this.eliminaBlancosString(condicionVenta);
				desuni = this.eliminaBlancosString(desuni);
				 
				int diasVencimiento = Util.validaParametro(req.getParameter("diasVencimiento"), 0);
				//int cuentaPresup = Util.validaParametro(req.getParameter("cuentaPresup"), 0);
				String cuentaPresup = Util.validaParametro(req.getParameter("cuentaPresup"),"");
				long totalNeto = Util.validaParametro(req.getParameter("totalNeto"), 0);
				long totalIVA = Util.validaParametro(req.getParameter("totalIVA"), 0);
				long totalFac = Util.validaParametro(req.getParameter("totalFac"), 0);
				long valorIVA = Util.validaParametro(req.getParameter("valorIVA"), 0);
				//long sede = Util.validaParametro(req.getParameter("sede"), 0);
			
				MathTool mathtool = new MathTool();
			    totalIVA = mathtool.mul(totalNeto,valorIVA).longValue();
			    totalIVA = mathtool.round(mathtool.div(totalIVA,100)).longValue();
				List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
				/*mb agrega datos alumno vespertino nov-2014*/
			/*	List<Presw25DTO> datosAlumno = new ArrayList<Presw25DTO>();
				datosAlumno = (List<Presw25DTO>) req.getSession().getAttribute("datosAlumno");
				Presw25DTO presw25DTO = new Presw25DTO();*/
				
				/**para nota de credito*/
				String tipoNota = Util.validaParametro(req.getParameter("tipoNota"),"");
				long numDoc = Util.validaParametro(req.getParameter("numDoc"), 0);
			 /*****/
				
			    String tiping = tipoProceso;
			    String nomcam = "";
			    long valnu1 = 0;
			    long valnu2 = 0;
			    long valorExento = 0;
				if(tipoDocumento.trim().equals("FET")){
					valorExento = totalNeto;
					totalNeto = 0;
				}
			    
			    String valalf = "";
			

		    /* prepara archivo para grabar, llenar listCocow36DTO*/
			    
		    nomcam = "TIPDOC";
		    valnu1 = 0;
		    valnu2 = 0;
		    if(!tipoNota.trim().equals(""))
		    	 valalf = tipoNota;
		    else
		    	valalf = tipoDocumento;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "SUCUR";
		   // valnu1 = sede;
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "RUTIDE";
		    valnu1 = rutnum;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "DIGIDE";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = (dvrut.trim().equals('k')?dvrut.trim().toUpperCase():dvrut.trim());
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "NOMCLI";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = this.eliminaAcentosString(nombreCliente.toUpperCase());
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "DIRCLI";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = this.eliminaAcentosString(direccionCliente.toUpperCase());
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "COMUNA";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = this.eliminaAcentosString(comunaCliente.toUpperCase());
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "CIUDAD";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = this.eliminaAcentosString(ciudadCliente.toUpperCase());
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "GIRCLI";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = this.eliminaAcentosString(giroCliente.toUpperCase());
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

		    nomcam = "TIPSER";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = tipoServicio;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

		    nomcam = "REFCLI";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = this.eliminaAcentosString(referenciaAtt.toUpperCase());
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "CONVEN";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = condicionVenta.toUpperCase();
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

		    nomcam = "CODUNI";
		   // valnu1 = cuentaPresup;
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = cuentaPresup;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

		    nomcam = "DESUNI";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = desuni;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "VALNET";
		    valnu1 = totalNeto;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    

		    nomcam = "VALEXE";
		    valnu1 = valorExento;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "VALIVA";
		    valnu1 = totalIVA;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "VALTOT";
		    valnu1 = totalFac;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		/*    se elimina MB 200190423
		    nomcam = "TIPREF";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = this.eliminaAcentosString(tipoCuenta.toUpperCase());
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );*/
		 
		    nomcam = "NUMREF";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = numRef;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "NUMHES";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = numHes;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    
		    
		    nomcam = "DIAVEN";
		    valnu1 = diasVencimiento;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "FECINI";
		    valnu1 = fechaInicio;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "FECTER";
		    valnu1 = fechaTermino;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    /* datos alumno vespertino que se asocia factura*/
		 /*    if(datosAlumno != null && datosAlumno.size() > 0){
		    	 presw25DTO = datosAlumno.get(0);
		    	 nomcam = "RUTVES";
				 valnu1 = presw25DTO.getRutide();
				 valnu2 = 0;
				 valalf = "";
				 listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
				
				 nomcam = "DIGVES";
				 valnu1 = 0;
				 valnu2 = 0;
				 valalf = presw25DTO.getDigide();
				 listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
			
				 nomcam = "ANOARA";
				 valnu1 = presw25DTO.getAnopre();
				 valnu2 = 0;
				 valalf = "";
				 listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
			
				 nomcam = "CODCAR";
				 valnu1 = presw25DTO.getCoduni();
				 valnu2 = 0;
				 valalf = "";
				 listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
				 
				 nomcam = "CODSUC";
				 valnu1 = presw25DTO.getCanman();
				 valnu2 = 0;
				 valalf = "";
				 listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		     }*/
		    
		    /**/
		    /*  agregar MD5   TIPDOC + RUTIDE + DIGIDE + NOMCLI + VALTOT*/
		    nomcam = "CODMD5";
		    MD5 md5 = new MD5();
		    String texto = md5.getMD5(tipoDocumento.trim() + rutide + dvrut.trim() + nombreCliente.trim() + totalFac) ;
		    if(!tipoNota.trim().equals(""))
		    	texto = md5.getMD5(tipoNota.trim() + rutide + dvrut.trim() + nombreCliente.trim() + totalFac) ;
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = texto;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    /*para nota de credito*/
		    if(!tipoNota.trim().equals("")){
		    nomcam = "TIDORE";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = tipoDocumento.toUpperCase();
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "FACNOT";
		    valnu1 = numDoc;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    }
		  
		    for (Presw18DTO ss : listaSolicitudes){
		    	nomcam = "DETFAC";
		 	    valnu1 = ss.getUsadom();
		 	    valnu2 = 0;
		 	    valalf = ss.getDesuni();
		 	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		 	     
		    }
		   
		    if (rutAlu > 0 && periodo > 0) {
			    nomcam = "RUTALU";
			    valnu1 = rutAlu;
			    valnu2 = 0;
			    valalf = "";
			    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
			    
			    nomcam = "DIGALU";
			    valnu1 = 0;
			    valnu2 = 0;
			    valalf = (digAlu.trim().equals('k')?digAlu.trim().toUpperCase():digAlu.trim());;
			    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
					
			    nomcam = "PERIOD";
			    valnu1 = periodo;
			    valnu2 = 0;
			    valalf = "";
			    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
			    
			    nomcam = "CODDET";
			    valnu1 = 0;
			    valnu2 = 0;
			    valalf = codDetalle;
			    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    }
	
			return listCocow36DTO;
		}
		 
		 
		 public List<Cocow36DTO> getAgregaLista(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf){
			 Cocow36DTO cocow36DTO = new Cocow36DTO();
			 cocow36DTO.setTiping(tiping);
			 cocow36DTO.setNomcam(nomcam);
			 cocow36DTO.setValnu1(valnu1);
			 cocow36DTO.setValnu2(valnu2);
			 cocow36DTO.setValalf(valalf);
			 lista.add(cocow36DTO);
			 return lista;
			 
		 }
		 public List<Cocow36DTO> getAgregaListaMD5(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf, String resval){
			 Cocow36DTO cocow36DTO = new Cocow36DTO();
			 cocow36DTO.setTiping(tiping);
			 cocow36DTO.setNomcam(nomcam);
			 cocow36DTO.setValnu1(valnu1);
			 cocow36DTO.setValnu2(valnu2);
			 cocow36DTO.setValalf(valalf);
			 cocow36DTO.setResval(resval);
			 lista.add(cocow36DTO);
			 return lista;
			 
		 }
			public boolean getActualizaFactura(HttpServletRequest req, int rutide, String digide, int numDoc, String tipoProceso){
				boolean registra = false;
				List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
				listCocow36DTO = getCreaCocow36(req, rutide, digide,tipoProceso);
				Ingreso_Documento ingresoDocumento = new Ingreso_Documento(tipoProceso,rutide,digide);
				ingresoDocumento.setNumdoc(numDoc);
					
				registra = ingresoDocumento.Actualizar_Documento(listCocow36DTO);
				
			
				return registra;
			}
			public boolean saveAutoriza(int numdoc, int rutUsuario, String digide, String nomTipo, String tipCue){
				PreswBean preswbean = new PreswBean(nomTipo, 0,0,0,0, rutUsuario);
				preswbean.setDigide(digide);
				preswbean.setNumdoc(numdoc);
				preswbean.setTipcue(tipCue);
			return preswbean.ingreso_presw19();
			}
			
			public boolean saveRechaza(int numdoc, int rutUsuario, String digide, String tipo){
				PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
				preswbean.setDigide(digide);
				preswbean.setNumdoc(numdoc);
				 return preswbean.ingreso_presw19();
			}
			
			public boolean saveRecepciona(int numdoc, int rutUsuario, String digide, String tipo){
				PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
				preswbean.setDigide(digide);
				preswbean.setNumdoc(numdoc);
			
				
			return preswbean.ingreso_presw19();
			}
			 public boolean getEliminaFactura( int rut, String dv, int numdoc, String nomTipo, String tipCue ){
				  	PreswBean preswbean = null;
					Collection<Presw18DTO> lista = null;
					long saldoCuenta = 0;
					preswbean = new PreswBean(nomTipo,0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
					preswbean.setRutide(rut);
					preswbean.setDigide(dv);
					preswbean.setNumdoc(numdoc);
					preswbean.setTipcue(tipCue);
					
					return preswbean.ingreso_presw19();	
					
			 } 
			 
			 public Collection<Presw18DTO> getVerificaNotaCredito(int rutUsuario, String dv, String tipoDocumento,int numeroFactura, String tipoNota) throws Exception {
					/*	se verifica en nota de credito si existe segun datos enviados */
						PreswBean preswbean = null;
						Collection<Presw18DTO> lista = null;					
						preswbean = new PreswBean("VN1",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
						preswbean.setRutide(rutUsuario);
						preswbean.setDigide(dv);
						//preswbean.setSucur(sede);
						preswbean.setNumdoc(numeroFactura);
						preswbean.setTipdoc(tipoDocumento);
						lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
					
					return lista;
					}
			 
				public void agregaListaFacturacion(HttpServletRequest req, Vector<String> lista){
					HttpSession sesion = req.getSession();
					sesion.setAttribute("listadoFacturacion",lista);

				}
			 
				public String formateoNumeroEntero(int numero){
					/*formatear n�mero */
				     Locale l = new Locale("es","CL");
				     DecimalFormat formatter1 = (DecimalFormat)DecimalFormat.getInstance(l);
				     formatter1.applyPattern("###,###,###");
				 
				   //  formatter1.format(rs.getDouble("RUTNUM"))   /*sin decimales*/
				 return formatter1.format(numero);
				}
				public String formateoNumeroLong(long numero){
					/*formatear n�mero */
				     Locale l = new Locale("es","CL");
				     DecimalFormat formatter1 = (DecimalFormat)DecimalFormat.getInstance(l);
				     formatter1.applyPattern("###,###,###");
				 
				   //  formatter1.format(rs.getDouble("RUTNUM"))   /*sin decimales*/
				 return formatter1.format(numero);
				}
				public String eliminaBlancosString(String sTexto){
					/*elimina blancos entre medio*/
				   String linea = "";
				    
				    
				    for (int x=0; x < sTexto.length(); x++) {
				    	  if ((sTexto.charAt(x) != ' ' || 
				    		  (x < (sTexto.length() - 1) && sTexto.charAt(x) == ' '  && sTexto.charAt(x+1) != ' ')) &&
				    		  sTexto.charAt(x) != '\'' &&
				    		  sTexto.charAt(x) != '\"')
				    		  linea += sTexto.charAt(x);
				    	}				
				
				 return linea;
				}
			
				// MAA no se va usar en BANNER
				public boolean saveCliente(List<Presw25DTO> lista,int rutUsuario){
					PreswBean preswbean = new PreswBean("CLI", 0, 0, 0, 0, rutUsuario);
					
					return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
				} 
				
				 public String eliminaAcentosString(String sTexto){
						/*elimina acentos del string*/
					   String linea = "";  
					    if(!sTexto.trim().equals("")){
					    for (int x=0; x < sTexto.length(); x++) {
					    	  if ((sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') || 
					    	      (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
					    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') || 
					    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
					    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�')){
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'a';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'A';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'e';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'E';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'i';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'I';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'o';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'O';
					    		   if(sTexto.charAt(x) == '�' )
					    			  linea += 'u';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'U';
					    	  }
					    	  else  linea += sTexto.charAt(x);
					    	}				
					    }
					
					 return linea;
					}

public String getVerificaRutAlumno(String rutnumdv){
// System.out.println(rutnumdv);
	ConexionBanner con = new ConexionBanner();
	String nombre ="";
	
	   String rut = rutnumdv.substring(0,rutnumdv.length()-1);
       String dig = rutnumdv.substring(rutnumdv.length()-1,rutnumdv.length());
       if (dig.equals("k")) rutnumdv = rut+"K";
    
	
	try{
					      // SPBPERS con problemas de integraci�n 
	/*	PreparedStatement sent = con.conexion().prepareStatement("SELECT spbpers_legal_name FROM spriden, spbpers " +
					    		                                 "WHERE spriden_id = ? " +
					    		                                 "AND spriden_pidm = spbpers_pidm " +
					    		                                 "AND spriden_change_ind is null");*/
		PreparedStatement sent = con.conexion().prepareStatement(" SELECT SPRIDEN_FIRST_NAME || ' ' || REPLACE(SPRIDEN_LAST_NAME,'/',' ') " +
				                                                 " FROM spriden, sgbstdn " +
				                                                 " WHERE spriden_id = ? " +
				                                                 " AND spriden_pidm = sgbstdn_pidm " + 
				                                                 " AND spriden_change_ind is null ");
		sent.setString(1,rutnumdv);
		ResultSet res = sent.executeQuery();

		if (res.next()){
			nombre = res.getString(1); // nombre mas apellidos
		}
		res.close();
		sent.close();
		con.close();
	}
	catch (SQLException e){
		System.out.println("Error moduloFacturacionB.getVerificaRutAlumnot: " + e);
	}
	return nombre;
}

public long getVerificaCargoServEduc(String rutnumdv, int periodo, String codDetalle){
	ConexionBanner con = new ConexionBanner();
	long balance = -1;
	
	   String rut = rutnumdv.substring(0,rutnumdv.length()-1);
       String dig = rutnumdv.substring(rutnumdv.length()-1,rutnumdv.length());
       if (dig.equals("k")) rutnumdv = rut+"K";
       String condicion = "";
       if(periodo > 0)
    	   condicion = "AND TBRACCD_TERM_CODE = " + periodo +" ";
       if(!codDetalle.trim().equals(""))
    	   condicion = condicion + "AND TBBDETC_DETAIL_CODE = '" + codDetalle + "' ";
	try{
					      
		PreparedStatement sent = con.conexion().prepareStatement("SELECT SUM(TBRACCD_BALANCE) " +
                                                                 "FROM SPRIDEN, TBRACCD,TBBDETC, TVRSDDC " + 
                                                                 "WHERE spriden_id = ? " +
                                                                 "AND spriden_change_ind is null " +
                                                                 "AND spriden_pidm = tbraccd_pidm " +
                                                                 condicion +
                                                                 "AND TBBDETC_TYPE_IND = 'C' " + 
																 "AND TBBDETC_DCAT_CODE IN ('FEE','TUI') " +
																 "AND TBBDETC_DETAIL_CODE = TVRSDDC_DETAIL_CODE " + 
																 "AND TBBDETC_DETAIL_CODE = TBRACCD_DETAIL_CODE " +
																 "AND TVRSDDC_SDOC_CODE = 'SE' " +
                                                                 "AND TBRACCD_BALANCE > 0 ");
		sent.setString(1,rutnumdv);
		ResultSet res = sent.executeQuery();

		if (res.next()){
			balance = res.getLong(1); // existe el cargo
		}
		res.close();
		sent.close();
		con.close();
	}
	catch (SQLException e){
		System.out.println("Error moduloFacturacionB.getVerificaCargoServEduc: " + e);
	}
	return balance;
} 


public int getObtienePIDM(String rutnumdv){
	ConexionBanner con = new ConexionBanner();
	int pidm = 0;
	
	   String rut = rutnumdv.substring(0,rutnumdv.length()-1);
       String dig = rutnumdv.substring(rutnumdv.length()-1,rutnumdv.length());
       if (dig.equals("k")) rutnumdv = rut+"K";
	
	try{
					      
		PreparedStatement sent = con.conexion().prepareStatement("SELECT SPRIDEN_PIDM " +
                                                                 "FROM SPRIDEN " + 
                                                                 "WHERE spriden_id = ? " +
                                                                 "AND spriden_change_ind is null ");
		sent.setString(1,rutnumdv);
		ResultSet res = sent.executeQuery();

		if (res.next()){
			pidm = res.getInt(1); // pidem
		}
		res.close();
		sent.close();
		con.close();
	}
	catch (SQLException e){
		System.out.println("Error moduloFacturacionB.getObtienePIDM: " + e);
	}
	return pidm;
} 

public String formateoRut(String rut) {
	  int cont = 0;
      String format;
      String dig;
      rut = rut.replace(".", "");
      rut = rut.replace("-", "");
      dig = rut.substring(rut.length() - 1);
      if (dig.equals("k")) dig = "K"; 
      
      format = "-" + dig;
      for (int i = rut.length() - 2; i >= 0; i--) {
          format = rut.substring(i, i + 1) + format;
          cont++;
          if (cont == 3 && i != 0) {
              format = "." + format;
              cont = 0;
          }
      }
     return format;
}
				 
		
				 
}
