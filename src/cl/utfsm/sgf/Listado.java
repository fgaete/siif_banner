package cl.utfsm.sgf;

import java.io.Serializable;

public class Listado implements Serializable{
	private int 	CODSUC;     // 2S 0 COLHDG('SEDE PAGO')
	private int 	NUMVAL;     // 8S 0 COLHDG('NUMERO VALE')
	private String 	ESTVAL;		// 1A COLHDG('ESTADO VALE')
	private int 	RUTIDE;		// 8S 0 COLHDG('RUT IDENTIFICADOR') 
	private String 	DIGIDE;		// 1A COLHDG('DIGITO VERIF')
	private String 	NOMPAG; 	// 40A COLHDG('NOMBRE A PAGO') 
	private int 	VALVAL;     // 10S 0 COLHDG('VALOR VALE')
	private String 	GLODE1;		// 40A COLHDG('GLOSA DESCRIPC.1') 
	private String  GLODE2;     // 40A COLHDG('GLOSA DESCRIPC.2')
	private String  GLODE3;		// 40A COLHDG('GLOSA DESCRIPC.3') 
	private String  GLODE4;		// 40A COLHDG('GLOSA DESCRIPC.4')
	private String  GLODE5;		// 40A COLHDG('GLOSA DESCRIPC.5')
	private String  GLODE6;		// 40A COLHDG('GLOSA DESCRIPC.6')
	private String  IDMEMO;		// 20A COLHDG('IDENTIFICA MEMO')
	private String  TIPVAL;		// 3A COLHDG('TIPO VALE PAGO')
	private String  CODMD5;		// 64A COLHDG('CODIGO MD5 SEG')
	
	private int fecha; // para listado

	public int getCODSUC() {
		return CODSUC;
	}

	public void setCODSUC(int codsuc) {
		CODSUC = codsuc;
	}

	public int getNUMVAL() {
		return NUMVAL;
	}

	public void setNUMVAL(int numval) {
		NUMVAL = numval;
	}

	public String getESTVAL() {
		return ESTVAL;
	}

	public void setESTVAL(String estval) {
		ESTVAL = estval;
	}

	public int getRUTIDE() {
		return RUTIDE;
	}

	public void setRUTIDE(int rutide) {
		RUTIDE = rutide;
	}

	public String getDIGIDE() {
		return DIGIDE;
	}

	public void setDIGIDE(String digide) {
		DIGIDE = digide;
	}

	public String getNOMPAG() {
		return NOMPAG;
	}

	public void setNOMPAG(String nompag) {
		NOMPAG = nompag;
	}

	public int getVALVAL() {
		return VALVAL;
	}

	public void setVALVAL(int valval) {
		VALVAL = valval;
	}

	public String getGLODE1() {
		return GLODE1;
	}

	public void setGLODE1(String glode1) {
		GLODE1 = glode1;
	}

	public String getGLODE2() {
		return GLODE2;
	}

	public void setGLODE2(String glode2) {
		GLODE2 = glode2;
	}

	public String getGLODE3() {
		return GLODE3;
	}

	public void setGLODE3(String glode3) {
		GLODE3 = glode3;
	}

	public String getGLODE4() {
		return GLODE4;
	}

	public void setGLODE4(String glode4) {
		GLODE4 = glode4;
	}

	public String getGLODE5() {
		return GLODE5;
	}

	public void setGLODE5(String glode5) {
		GLODE5 = glode5;
	}

	public String getGLODE6() {
		return GLODE6;
	}

	public void setGLODE6(String glode6) {
		GLODE6 = glode6;
	}

	public String getIDMEMO() {
		return IDMEMO;
	}

	public void setIDMEMO(String idmemo) {
		IDMEMO = idmemo;
	}

	public String getTIPVAL() {
		return TIPVAL;
	}

	public void setTIPVAL(String tipval) {
		TIPVAL = tipval;
	}

	public String getCODMD5() {
		return CODMD5;
	}

	public void setCODMD5(String codmd5) {
		CODMD5 = codmd5;
	}

	public int getFecha() {
		return fecha;
	}

	public void setFecha(int fecha) {
		this.fecha = fecha;
	}

	
}
