package cl.utfsm.sgf.datos;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.FlushMode;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cl.utfsm.base.auditoria.AuditoriaServicio;
import cl.utfsm.base.auditoria.DetalleServicio;
import cl.utfsm.base.auditoria.TipoAccionServicio;
//import cl.utfsm.sgf.Listado;
import cl.utfsm.sip.PeriodoProcesos;

public class HibernateFinanzaDao extends HibernateDaoSupport implements FinanzaDao{
	
	public boolean getPeriodoProcesos() throws Exception {
		boolean existe = false;
		List resultado = getHibernateTemplate().find("FROM PeriodoProcesos p " +
				" WHERE p.sede.codigo = 1" + 
				" AND p.jornada.codigo = 1" +
				" AND p.codigoProceso = 46" +
				" AND TO_CHAR(p.fechaInicio, 'yyyymmdd') <=" +
				" TO_CHAR(SYSDATE, 'yyyymmdd')" +
                " AND TO_CHAR(p.fechaTermino, 'yyyymmdd') >=" +
                " TO_CHAR(SYSDATE, 'yyyymmdd') ");

			/*	" AND CONVERT(VARCHAR(8),p.fechaInicio,112) <=" +
                " CONVERT(VARCHAR(8),GETDATE(),112)" +
                " AND CONVERT(VARCHAR(8),p.fechaTermino,112) >=" +
                " CONVERT(VARCHAR(8),GETDATE(),112) ");*/
		if(resultado != null && resultado.size() > 0)
			return existe = true;
		else return existe;
	}
	
	
	
	public DetalleServicio getDetalleServicio(Long id) {
		DetalleServicio detalleServicio = new DetalleServicio();
		List resultado = getHibernateTemplate().find("from DetalleServicio where id = " + id);
		if (resultado.size() == 0)
			detalleServicio = new DetalleServicio();
		else
			detalleServicio = (DetalleServicio) resultado.get(0);
	    return detalleServicio;		
	    }
	public TipoAccionServicio getTipoAccionServicio(int codigo) {
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();
		List resultado = getHibernateTemplate().find("from TipoAccionServicio where codigo = " + codigo);
		if (resultado.size() == 0)
			tipoAccionServicio = new TipoAccionServicio();
		else
			tipoAccionServicio = (TipoAccionServicio) resultado.get(0);
	    return tipoAccionServicio;	
	}
	public void saveAuditoriaServicio(AuditoriaServicio auditoriaServicio) {
		getHibernateTemplate().getSessionFactory().getCurrentSession().setFlushMode(FlushMode.AUTO);
		getHibernateTemplate().save(auditoriaServicio);
		getHibernateTemplate().flush();

	}
	public void savePeriodoProcesos(PeriodoProcesos periodoProcesos) {
		getHibernateTemplate().getSessionFactory().getCurrentSession().setFlushMode(FlushMode.AUTO);
		getHibernateTemplate().saveOrUpdate(periodoProcesos);
		getHibernateTemplate().flush();

	}
		
	public List<PeriodoProcesos> getPeriodosProceso() throws Exception {
		List<PeriodoProcesos> periodosProceso = new ArrayList<PeriodoProcesos>();
		List resultado = getHibernateTemplate().find("FROM PeriodoProcesos p   WHERE p.codigoProceso = 46 ORDER BY p.anno desc" );
		if(resultado != null && resultado.size() > 0){
			for (int i = 0; i < resultado.size(); i++) 
				periodosProceso.add((PeriodoProcesos) resultado.get(i));
			return periodosProceso;
		}	
		return null; 
	}
	public List<PeriodoProcesos> getHabilitaOpcion() throws Exception {
		// mb 31/5/2018 para asi dejar las opciones de men� para ser habilitadas por fecha y hora
	
		List<PeriodoProcesos> periodosProceso = new ArrayList<PeriodoProcesos>(); 
		/* 110 = muestraPresupuesto
		   111 = muestraVAP
		   112 = muestraBHE
		   113 = muestraREC
		   114 = muestraFACT
		   115 = muestraCentralizadas
		   116 = muestraDecentralizadas
		   117 = muestraRegularizacion
		   118 = muestraAyudantia
		   119 = muestraContrato 
		   120 = muestraIncentivo
		   121 = muestraCociliacion
		   
		   *
		   124 = muestraContabilizacion
		   125 = muestraBannerSAC
		   126 = muestraLiquidacion
		   127 = muestraSIIFBanner
		   */
		List resultadoPer = getHibernateTemplate().find("FROM PeriodoProcesos p   WHERE ((p.codigoProceso between  110 and  121) " +
													 " OR (p.codigoProceso between  124 and  127))" +
													 " AND  p.fechaInicio <= sysdate and sysdate <= NVL(p.fechaTermino, sysdate+1)" +												
													 " ORDER BY p.codigoProceso" );
		
		//" AND TRUNC(s.fechaIngreso) BETWEEN TO_DATE('" + fechaInicio + "','YYYY-MM-DD') AND TO_DATE('" + fechaTermino + "','YYYY-MM-DD')";
		
		
		
		if(resultadoPer != null){
			for (int i = 0; i < resultadoPer.size(); i++) 
				periodosProceso.add((PeriodoProcesos) resultadoPer.get(i));
			return periodosProceso;
		}	
		return null; 
	}
	public void deletePeriodoProceso (PeriodoProcesos periodoProcesos)throws Exception {
		getHibernateTemplate().getSessionFactory().getCurrentSession().setFlushMode(FlushMode.AUTO);
		getHibernateTemplate().delete(periodoProcesos);
		getHibernateTemplate().flush();
	}
	public PeriodoProcesos getPeriodoProcesos(int anno) throws Exception {
		PeriodoProcesos periodoProcesos = new PeriodoProcesos();
		List resultado = getHibernateTemplate().find("FROM PeriodoProcesos p " +
				" WHERE p.anno = " + anno +
				" AND p.semestre = 1 " +
				" AND p.sede.codigo = 1" + 
				" AND p.jornada.codigo = 1" +
				" AND p.codigoProceso = 46");
		if(resultado.size() > 0)
			for (int i = 0; i < resultado.size(); i++) 
				periodoProcesos = (PeriodoProcesos) resultado.get(i);
		
	 return periodoProcesos;
	}
/*
	public List<Listado> getvaleSinFinanciamiento(int codUnidad, int a�o)  throws Exception {
		VALES vales = new VALES();
		List<Listado> listado = new  ArrayList<Listado>();
		List resultado = getHibernateTemplate().find("FROM VALES " +
				" WHERE ESTVAL = 'A'" +				
				" AND TIPOPE = 'I'" +
		        " AND SUBSTR(TO_CHAR(FECOPE),1,4) = " + a�o + ")" +
				" ORDER BY NUMVAL");		
		
		if(resultado.size() > 0)
			for (int i = 0; i < resultado.size(); i++) {
				Listado lista = new Listado();
				vales = (VALES) resultado.get(i);
				lista.setCODMD5(vales.getCODMD5());
				lista.setCODSUC(vales.getCODSUC());
				lista.setDIGIDE(vales.getDIGIDE());
				lista.setESTVAL(vales.getESTVAL());
				lista.setFecha(vales.getFECOPE());
				lista.setGLODE1(vales.getGLODE1());
				lista.setGLODE2(vales.getGLODE2());
				lista.setGLODE3(vales.getGLODE3());
				lista.setGLODE4(vales.getGLODE4());
				lista.setGLODE5(vales.getGLODE5());
				lista.setGLODE6(vales.getGLODE6());
				lista.setIDMEMO(vales.getIDMEMO());
				lista.setNOMPAG(vales.getNOMPAG());
				lista.setNUMVAL(vales.getNUMVAL());
				lista.setRUTIDE(vales.getRUTIDE());
				lista.setTIPVAL(vales.getTIPVAL());
				lista.setVALVAL(vales.getVALVAL());
				listado.add(lista);
			}
	 return listado;
	}*/
}

