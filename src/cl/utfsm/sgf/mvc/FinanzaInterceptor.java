
package cl.utfsm.sgf.mvc;
/**
 * SISTEMA GENERAL DE FINANZAS
 * PROGRAMADOR		: 	MñNICA BARRERA FREZ
 * FECHA ULT.MODIF  : 	26/04/2011
 * UNIDAD DE DESARROLLO INSTITUCIONAL(DTI)   
 * migraciñn oracle : 15/06/2013 MB
 * ñltima modificaciñn: 24/05/2016
 */
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.usm.siga.login.LoginLDAPBean;
import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.auditoria.AuditoriaServicio;
import cl.utfsm.base.auditoria.DetalleServicio;
import cl.utfsm.base.auditoria.TipoAccionServicio;
import cl.utfsm.base.perfiles.modulos.ModuloPerfiles;
import cl.utfsm.base.util.Util;
import cl.utfsm.sgf.modulo.ModuloFinanza;
import cl.utfsm.sip.PeriodoProcesos;
import cl.utfsm.sip.Sedes;
import cl.utfsm.sip.TipoJornadas;
import cl.utfsm.sip.modulo.ModuloPresupuesto;
import cl.utfsm.sip.mvc.PresupuestoInterceptor;
import descad.cliente.PreswBean;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw25DTO;
import descad.presupuesto.Presw21DTO;
import java.util.Date;

public class FinanzaInterceptor extends HandlerInterceptorAdapter{
	private ModuloFinanza moduloFinanza;
	private ModuloPerfiles moduloPerfiles;
	static Logger log = Logger.getLogger(PresupuestoInterceptor.class);
	//int rutUsuario = 0;
	String funcionario = "";
	
	public ModuloFinanza getModuloFinanza() {
		return moduloFinanza;
	}

	public void setModuloFinanza(ModuloFinanza moduloFinanza) {
		this.moduloFinanza = moduloFinanza;
	}

	public ModuloPerfiles getModuloPerfiles() {
		return moduloPerfiles;
	}

	public void setModuloPerfiles(ModuloPerfiles moduloPerfiles) {
		this.moduloPerfiles = moduloPerfiles;
	}

	public void preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		System.out.println("Valor HttpServletRequest 1" );
		if (request.getSession().getAttribute("rutUsuario") != null) {
			if (request.getSession().getAttribute("listaUnidad") != null) {
				modelAndView.addObject("inicio", "S");
			}

		}
	}

	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		System.out.println("Valor HttpServletRequest 2" );
			
		/*
		 * perfiles en tabla perfil    	16   Administrador de Presupuesto                                                                                      1          603 27/04/2010 15:31:40   
        								19   Administrador DIPRES                                                                                              1          603 27/09/2013 14:48:30   
        								20   Usuario Prueba SIIF
        								21   Administrador Finanzas 
		 */
		NumberTool numbertool = new NumberTool();
		MathTool mathtool = new MathTool();
		DateTool dateTool = new DateTool();
		modelAndView.addObject("numbertool", numbertool);
		modelAndView.addObject("mathtool", mathtool);
		modelAndView.addObject("datetool", dateTool);
		modelAndView.addObject("date", new DateTool());
		DateTool fechaActual = new DateTool();
		System.out.println("linea 1" );
		try {
			boolean permiteRegistrar = moduloFinanza.getPeriodoProcesos();
			System.out.println("linea 2" );
			List<PeriodoProcesos> listaPeriodo = moduloFinanza.getPeriodosProceso();
			List<PeriodoProcesos> listaPermisoMenu = moduloFinanza.getHabilitaOpcion();
			if(listaPermisoMenu != null && listaPermisoMenu.size() > 0){
				for (PeriodoProcesos ss : listaPermisoMenu){
				 if(ss.getCodigoProceso() == 110)
					 modelAndView.addObject("muestraPresupuesto",1);
				 if(ss.getCodigoProceso() == 111)
					 modelAndView.addObject("muestraVAP",1);
				 if(ss.getCodigoProceso() == 112)
					 modelAndView.addObject("muestraBHE",1);
				 if(ss.getCodigoProceso() == 113)
					 modelAndView.addObject("muestraREC",1);
				 if(ss.getCodigoProceso() == 114)
					 modelAndView.addObject("muestraFACT",1);
				 if(ss.getCodigoProceso() == 115)
					 modelAndView.addObject("muestraCentralizadas",1);
				 if(ss.getCodigoProceso() == 116)
					 modelAndView.addObject("muestraDecentralizadas",1);
				 if(ss.getCodigoProceso() == 117)
					 modelAndView.addObject("muestraRegularizacion",1);
				 if(ss.getCodigoProceso() == 118)
					 modelAndView.addObject("muestraAyudantia",1);
				 if(ss.getCodigoProceso() == 119)
					 modelAndView.addObject("muestraContrato",1);
				 if(ss.getCodigoProceso() == 120)
					 modelAndView.addObject("muestraIncentivo",1);
				 if(ss.getCodigoProceso() == 121)
					 modelAndView.addObject("muestraCociliacion",1);
				 if(ss.getCodigoProceso() == 124)
					 modelAndView.addObject("muestraContabilizacion",1);
				 if(ss.getCodigoProceso() == 125)
					 modelAndView.addObject("muestraBannerSAC",1);
				 if(ss.getCodigoProceso() == 126)
					 modelAndView.addObject("muestraLiquidacion",1);
				 if(ss.getCodigoProceso() == 127)
					 modelAndView.addObject("muestraSIIFBanner",1);
				}
				System.out.println("linea 3" );
				
			}
			if(listaPeriodo != null && listaPeriodo.size() > 0)
				modelAndView.addObject("listaPeriodo",listaPeriodo);
			System.out.println("permiteRegistrar"+permiteRegistrar+" - "+!permiteRegistrar );
			if(!permiteRegistrar){
				modelAndView.addObject("fechaTope", 1);
			}
			System.out.println("linea 4" );
			StringBuffer mensajeInicio = moduloFinanza.getMensaje();
			if(!mensajeInicio.toString().trim().equals(""))
				modelAndView.addObject("mensajeInicio", mensajeInicio);
			System.out.println("linea 5" );
		} catch (Exception e) {
			log.debug("No guardada auditoria cuentasPresupuestarias");
			modelAndView.addObject("mensaje", "No guardado");
			modelAndView.addObject("exception", e);
			e.printStackTrace();
		}
		

//	System.out.println("sys "+ fechaActual.getSystemDate().getDate()+"mi s "+(fechaActual.getSystemDate().getMonth()+1)+"d "+(1900 + fechaActual.getSystemDate().getYear()));
		System.out.println("linea 6" );
		if (request.getSession().getAttribute("rutUsuario") != null) {
			int rutUsuario = Integer.parseInt(Util.validaParametro(request.getSession().getAttribute("rutUsuario")+ "","0"));
			int codigoPerfil = Integer.parseInt(Util.validaParametro(request.getSession().getAttribute("codigoPerfil")+ "","0"));
			List<Presw21DTO> listaMenu = (List<Presw21DTO>) request.getSession().getAttribute("listaMenu");
			if(listaMenu != null && listaMenu.size() > 0){
			modelAndView.addObject("listaMenu", listaMenu);
			System.out.println("linea 7" );
			}
			
			modelAndView.addObject("rutUsuario", rutUsuario);
			modelAndView.addObject("codigoPerfil", codigoPerfil);
			funcionario = Util.validaParametro(request.getSession().getAttribute("funcionario") + "","");
			modelAndView.addObject("funcionario", funcionario);
			System.out.println("linea 8" );
			if(request.getSession().getAttribute("rutOrigen") != null)
				modelAndView.addObject("rutOrigen", Util.validaParametro(request.getSession().getAttribute("rutOrigen"),0));
			if(request.getSession().getAttribute("nomSimulacion") != null)
				modelAndView.addObject("nomSimulacion", request.getSession().getAttribute("nomSimulacion"));

            if(request.getSession().getAttribute("anno") != null)
            	modelAndView.addObject("anno",request.getSession().getAttribute("anno"));
		} else
			modelAndView.addObject("inicio", "S");

	}

	public void cerrar(AccionWeb accionweb) throws Exception {
		System.out.println("linea 9" );
		if (accionweb.getSesion().getAttribute("rutUsuario") != null) {
			accionweb.getSesion().removeAttribute("rutUsuario");
			accionweb.getSesion().setAttribute("rutUsuario",null);
		}
		if (accionweb.getSesion().getAttribute("listaUnidad") != null) {
			accionweb.getSesion().removeAttribute("listaUnidad");
			accionweb.getSesion().setAttribute("listaUnidad",null);
		}
	}
    public void cargarInicio(AccionWeb accionweb) throws Exception {
    	System.out.println("linea 10" );
	 if (accionweb.getSesion().getAttribute("rutUsuario") != null) {
		accionweb.getSesion().removeAttribute("rutUsuario");
		accionweb.getSesion().setAttribute("rutUsuario",null);
		System.out.println("linea 11" );
		if (accionweb.getSesion().getAttribute("listaUnidad") != null) {
			accionweb.getSesion().removeAttribute("listaUnidad");
			accionweb.getSesion().setAttribute("listaUnidad", null);
		}
		System.out.println("linea 12" );
		if (accionweb.getSesion().getAttribute("codigoPerfil") != null) {
			accionweb.getSesion().removeAttribute("codigoPerfil");
			accionweb.getSesion().setAttribute("codigoPerfil", null);
		}
		System.out.println("linea 13" );
	 accionweb.agregarObjeto("esLogin", 1); // 1 verdadero
	}
}
   
    public void cargarHome(AccionWeb accionweb) throws Exception {
    	System.out.println("linea 14" );
    	int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		Collection<Presw18DTO> lista = (Collection<Presw18DTO>) accionweb.getSesion().getAttribute("listaUnidad");
		accionweb.agregarObjeto("cuentasPresupuestarias", lista);
		System.out.println("linea 15" );
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
	/*	descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
		funcionarioBean.buscar_funcionario(rutUsuario);
		String nomSimulacion = funcionarioBean.getNombre();
		accionweb.agregarObjeto("nomSimulacion", nomSimulacion);*/
    }

	public void cargaLogin(AccionWeb accionweb) throws Exception {
		String login = Util.validaParametro(accionweb.getParameter("login"), "");
		String password = Util.validaParametro(accionweb.getParameter("passwd"), "");
		String server = Util.validaParametro(accionweb.getParameter("server"),"");
		StringBuffer mensajeAlerta = moduloFinanza.getMensaje(); 
		System.out.println("linea 16" );
		String rutLdap = "0";
		int rutUsuario = 0;
		String dv = "";
		boolean esLogin = false;
		int resul = 2;
		int codigoPerfil = 0;
		StringBuffer mensajeInicio = moduloFinanza.getMensaje();
		if(!mensajeInicio.toString().trim().equals(""))
			accionweb.agregarObjeto("mensajeInicio", mensajeInicio);
		String Funcionario = "";
		System.out.println("linea 17" );
		if (accionweb.getSesion().getAttribute("rutUsuario") != null) {
			accionweb.getSesion().removeAttribute("rutUsuario");
			accionweb.getSesion().setAttribute("rutUsuario", null);
			if (accionweb.getSesion().getAttribute("listaUnidad") != null) {
				accionweb.getSesion().removeAttribute("listaUnidad");
				accionweb.getSesion().setAttribute("listaUnidad", null);
			}
			if (accionweb.getSesion().getAttribute("codigoPerfil") != null) {
				accionweb.getSesion().removeAttribute("codigoPerfil");
				accionweb.getSesion().setAttribute("codigoPerfil", null);
			}
			if(accionweb.getSesion().getAttribute("rutOrigen") != null){
				accionweb.getSesion().removeAttribute("rutOrigen");
				accionweb.getSesion().setAttribute("rutOrigen",null);
			}
			if(accionweb.getSesion().getAttribute("listaMenu") != null){
				accionweb.getSesion().removeAttribute("listaMenu");
				accionweb.getSesion().setAttribute("listaMenu",null);
			}
			
		}
		System.out.println("linea 18" );
		if (login.trim().length() > 0 && password.trim().length() > 0) {
			int activoSiif = moduloFinanza.getActivacionBannerFacturas(61); // Activaciñn SIIF;
			 LoginLDAPBean loginBeanId = new LoginLDAPBean();
			 System.out.println("linea 19" );	
			if (activoSiif > 0) {
					
			    loginBeanId.setLogin(login);
				loginBeanId.setPasswd(password);
				loginBeanId.setServer(server);
				loginBeanId.setRemoteaddr(accionweb.getReq().getRemoteAddr());
				esLogin = loginBeanId.getUsuario();
				dv = loginBeanId.getDv().toUpperCase();
			}
			
			System.out.println("Valor esLogin " + esLogin);
			if (esLogin) {
				// Obtiene el RUT del usuario
				rutLdap = loginBeanId.getRut();
				rutUsuario = Integer.parseInt(rutLdap);
				descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
				funcionarioBean.buscar_funcionario(rutUsuario);
				if(funcionarioBean != null && funcionarioBean.getNombre() != null && !funcionarioBean.getNombre().equals("null") && !funcionarioBean.getNombre().trim().equals("")) {
					
				Funcionario = funcionarioBean.getNombre();
                codigoPerfil = moduloPerfiles.getCodigoPerfilUsuario(rutUsuario);
				moduloFinanza.agregaUsuario(accionweb.getReq(), rutUsuario,
						Funcionario, codigoPerfil, dv);
				} else {	
					esLogin = false;
					resul = 2;
					System.out.println("finanzas sLogin ************: "+ esLogin+ "  resul = 2; "+resul+ "  rut: "+ rutUsuario);				
			     }
			}
			/*
			 * ************* OJO HABILITAR periodo_proceso 46, 
			 * DE LO CONTRARIO NO APARECE EL BOTON DE REGISTRAR*/
						
			
			rutUsuario = 8149718;
			// 6888256 -4  Darcy
			// 7256370-0   sergio solis
			// 16574426-8  alexis alvarado
		    // 13084685 - 8     director matem    pruebas de plan de desarrollo 
			// 8149718 Pedro Peralta
			// 15690450-2 Vanessa
			// 5310713 Edmundo Venegas
			// 15718253-6 jorge Guajardo
			// 10884302 - 0 JOSE LUIS NAVARRETE
			//  8399195 - K aNGELA lOYOLA
			// 13560745-2   cristian perez
			//9243270-K paulina pereda
			// 9570131-0 Pamela Mella
			// 8833069-2 Lorna
	        //16420427-8  andres rivera
			// 10985358-5
			// 7134199-2 ana maria Oliva UCP14
			// 10090739-9  liliana aguayo
			// 12.642.466-3  karol Trautman
			// 7533585-7   teresita arenas
			// 6881467-7 Alejandro Navarro
			// 9695647-9 Mabel berrios
			// 5231770-3  ricardo cifuentes
			//  14606690 - 9          patricio torres
			
			
			
			esLogin = true;
			descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
			funcionarioBean.buscar_funcionario(rutUsuario);
			Funcionario = funcionarioBean.getNombre();
			codigoPerfil = moduloPerfiles.getCodigoPerfilUsuario(rutUsuario);
				dv = funcionarioBean.getDv();
			moduloFinanza.agregaUsuario(accionweb.getReq(), rutUsuario,	Funcionario, codigoPerfil, dv);
		    dv = "4";
			//dv = "2";
			//comentar todo esto para war 
			
			
			

		} 
		if (esLogin) {
			System.out.println("linea 20" );
			resul = 1;
			int activoFacturas = moduloFinanza.getActivacionBannerFacturas(41); // Integraciñn BANNER FACTURAS;
			if (activoFacturas > 0) {
				this.ejecutaFacturas(accionweb);
			}	
		}
		System.out.println("linea 21" );
		Date fecActual = new Date();		     
		int anno = 1900 + fecActual.getYear() ;
		//System.out.println("anno : "+ anno);
		// Pedro propone si el mes es mayor que 7 que presupueste el año siguiente sino el actual

	
	
	//lo comentariñ solo por prueba
		if((fecActual.getMonth() + 1) > 7)
			anno++;
		System.out.println("linea 22" );
		accionweb.getSesion().setAttribute("anno", anno);
		accionweb.getSesion().setAttribute("dv", dv);
		accionweb.agregarObjeto("anno", anno);
		
		accionweb.agregarObjeto("rutUsuario", rutUsuario);
		accionweb.agregarObjeto("funcionario", Funcionario);
		accionweb.agregarObjeto("dv", dv);
		accionweb.agregarObjeto("esLogin", resul); // 1 verdadero
		accionweb.agregarObjeto("codigoPerfil", codigoPerfil);
		if(accionweb.getSesion().getAttribute("presupItemreq") != null)
			accionweb.getSesion().removeAttribute("presupItemreq");
		if(accionweb.getSesion().getAttribute("resultadoPresup") != null)
			accionweb.getSesion().removeAttribute("resultadoPresup");
		if(accionweb.getSesion().getAttribute("resultadoPresupImport") != null)
			accionweb.getSesion().removeAttribute("resultadoPresupImport");
		if(accionweb.getSesion().getAttribute("Original") != null)
			accionweb.getSesion().removeAttribute("Original");
		if(accionweb.getSesion().getAttribute("resultadoDetalleItemReq") != null)
			accionweb.getSesion().removeAttribute("resultadoDetalleItemReq");
		if(accionweb.getSesion().getAttribute("resultadoPresupNomina") != null)
			accionweb.getSesion().removeAttribute("resultadoPresupNomina");
		if(accionweb.getSesion().getAttribute("resultadoPresupVacante") != null)
			accionweb.getSesion().removeAttribute("resultadoPresupVacante");
		if(accionweb.getSesion().getAttribute("resultadoPresupRemuneracion") != null)
			accionweb.getSesion().removeAttribute("resultadoPresupRemuneracion");
		if(accionweb.getSesion().getAttribute("resultadoPresupIngresos") != null)
			accionweb.getSesion().removeAttribute("resultadoPresupIngresos");
		/*las opciones del menu que tiene permitido el usuario*/
		   List<Presw21DTO> lista = new ArrayList<Presw21DTO>();
		    Presw21DTO presw21DTO = new Presw21DTO();
			PreswBean preswbean = new PreswBean("AUT", 0, 0, 0, 0, rutUsuario);
			preswbean.setDigide(dv);
			preswbean.setAnopar(anno);
			Collection<Presw21DTO> listaPresw21 = (Collection<Presw21DTO>) preswbean.consulta_presw21();
			boolean permisoGurfeed = false;
			for( Presw21DTO ss:listaPresw21){
				if(ss.getNumdoc() == 80)
					permisoGurfeed = true;
			   
			}
		//if(permisoGurfeed)	{
			System.out.println("linea 23" );
			accionweb.getSesion().setAttribute("anno", anno);
			accionweb.getSesion().setAttribute("dv", dv);
			accionweb.agregarObjeto("anno", anno);
			
			accionweb.agregarObjeto("rutUsuario", rutUsuario);
			accionweb.agregarObjeto("funcionario", Funcionario);
			accionweb.agregarObjeto("dv", dv);
			accionweb.agregarObjeto("codigoPerfil", codigoPerfil);
			if(accionweb.getSesion().getAttribute("presupItemreq") != null)
				accionweb.getSesion().removeAttribute("presupItemreq");
			if(accionweb.getSesion().getAttribute("resultadoPresup") != null)
				accionweb.getSesion().removeAttribute("resultadoPresup");
			if(accionweb.getSesion().getAttribute("resultadoPresupImport") != null)
				accionweb.getSesion().removeAttribute("resultadoPresupImport");
			if(accionweb.getSesion().getAttribute("Original") != null)
				accionweb.getSesion().removeAttribute("Original");
			if(accionweb.getSesion().getAttribute("resultadoDetalleItemReq") != null)
				accionweb.getSesion().removeAttribute("resultadoDetalleItemReq");
			if(accionweb.getSesion().getAttribute("resultadoPresupNomina") != null)
				accionweb.getSesion().removeAttribute("resultadoPresupNomina");
			if(accionweb.getSesion().getAttribute("resultadoPresupVacante") != null)
				accionweb.getSesion().removeAttribute("resultadoPresupVacante");
			if(accionweb.getSesion().getAttribute("resultadoPresupRemuneracion") != null)
				accionweb.getSesion().removeAttribute("resultadoPresupRemuneracion");
			if(accionweb.getSesion().getAttribute("resultadoPresupIngresos") != null)
				accionweb.getSesion().removeAttribute("resultadoPresupIngresos");
			
				if(listaPresw21.size() > 0) {
					accionweb.agregarObjeto("listaMenu", listaPresw21);
					accionweb.getSesion().setAttribute("listaMenu", listaPresw21);
					}
			
			//} else { 
			//	      esLogin = false;
			//	      resul = 2;
			//}
				System.out.println("linea 24" );
		accionweb.agregarObjeto("esLogin", resul); // 1 verdadero 2 falso
		
	}

	/*public void cargaLogin2(AccionWeb accionweb) throws Exception {
		String login = Util.validaParametro(accionweb.getParameter("login"), "");
		String password = Util.validaParametro(accionweb.getParameter("passwd"), "");
		String server = Util.validaParametro(accionweb.getParameter("server"),"");
		 para pruebas en ICALMA (pedido por Vanessa) 
		int rutprueba = Util.validaParametro(accionweb.getParameter("rutprueba"),0);
		
		String rutLdap = "0";
		int rutUsuario = 0;
		boolean esLogin = false;
		int resul = 2;
		int codigoPerfil = 0;

		String Funcionario = "";
		if (accionweb.getSesion().getAttribute("rutUsuario") != null) {
			accionweb.getSesion().removeAttribute("rutUsuario");
			if (accionweb.getSesion().getAttribute("listaUnidad") != null) {
				accionweb.getSesion().removeAttribute("listaUnidad");
				accionweb.getSesion().setAttribute("listaUnidad", null);
			}
			if (accionweb.getSesion().getAttribute("codigoPerfil") != null) {
				accionweb.getSesion().removeAttribute("codigoPerfil");
				accionweb.getSesion().setAttribute("codigoPerfil", null);
			}
		}
		if (login.trim().length() > 0 && password.trim().length() > 0) {
			
			if (rutprueba == 0){  para pruebas en ICALMA (pedido por Vanessa) 
			LoginLDAPBean loginBeanId = new LoginLDAPBean();
			loginBeanId.setLogin(login);
			loginBeanId.setPasswd(password);
			loginBeanId.setServer(server);
			loginBeanId.setRemoteaddr(accionweb.getReq().getRemoteAddr());
			esLogin = loginBeanId.getUsuario();

			if (esLogin) {
				// Obtiene el RUT del usuario
				rutLdap = loginBeanId.getRut();
				rutUsuario = Integer.parseInt(rutLdap);
				descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
				funcionarioBean.buscar_funcionario(rutUsuario);
				Funcionario = funcionarioBean.getNombre();

				moduloPresupuesto.agregaUsuario(accionweb.getReq(), rutUsuario,
						Funcionario, codigoPerfil);
				accionweb.agregarObjeto("rutUsuario", rutUsuario);
			}
			} para pruebas en ICALMA (pedido por Vanessa) 
			
			 comentar todo esto para war 
			
			rutUsuario = rutprueba;  para pruebas en ICALMA (pedido por Vanessa) 
			
		    // 4266623 don Horst
			// 8149718 Pedro Peralta
			// 7481952 Cecilia Nova
			// 15690450 Vanessa
			// 5310713 Edmundo Venegas
			Date fecActual = new Date();		     
			int anno = 1900 + fecActual.getYear() + 1;
			
			accionweb.getSesion().setAttribute("anno", anno);
			accionweb.agregarObjeto("anno", anno);
			esLogin = true;
			descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
			funcionarioBean.buscar_funcionario(rutUsuario);
			Funcionario = funcionarioBean.getNombre();
			codigoPerfil = moduloPerfiles.getCodigoPerfilUsuario(rutUsuario);
			moduloPresupuesto.agregaUsuario(accionweb.getReq(), rutUsuario,
					Funcionario, codigoPerfil);
			
			 comentar todo esto para war 

		} 
		if (esLogin) {
			resul = 1;
		}
		accionweb.agregarObjeto("rutUsuario", rutUsuario);
		accionweb.agregarObjeto("funcionario", Funcionario);
		accionweb.agregarObjeto("esLogin", resul); // 1 verdadero
		accionweb.agregarObjeto("codigoPerfil", codigoPerfil);
		if(accionweb.getSesion().getAttribute("presupItemreq") != null)
			accionweb.getSesion().removeAttribute("presupItemreq");
		if(accionweb.getSesion().getAttribute("resultadoPresup") != null)
			accionweb.getSesion().removeAttribute("resultadoPresup");
		if(accionweb.getSesion().getAttribute("Original") != null)
			accionweb.getSesion().removeAttribute("Original");
		if(accionweb.getSesion().getAttribute("resultadoDetalleItemReq") != null)
			accionweb.getSesion().removeAttribute("resultadoDetalleItemReq");
		if(accionweb.getSesion().getAttribute("resultadoPresupNomina") != null)
			accionweb.getSesion().removeAttribute("resultadoPresupNomina");
		if(accionweb.getSesion().getAttribute("resultadoPresupRemuneracion") != null)
			accionweb.getSesion().removeAttribute("resultadoPresupRemuneracion");
		if(accionweb.getSesion().getAttribute("resultadoPresupIngresos") != null)
			accionweb.getSesion().removeAttribute("resultadoPresupIngresos");
			

	}*/
	
    /*
	 * cargaIntranet:
	 * Valida la sesiñn de intranet y sube el rut del usuario
	 */
	public void cargaIntranet(AccionWeb accionweb) throws Exception {
		System.out.println("linea 25" );
		int rutUsuario = 0;
		boolean esLogin = false;
		int resul = 2;
		String Funcionario = "";
		int codigoPerfil = 0;
		String dv = "";
		
		
		String sid = Util.validaParametro(accionweb.getParameter("sid"), "");
		String ts = Util.validaParametro(accionweb.getParameter("ts"), "");
		accionweb.agregarObjeto("esLogin", "0");
		accionweb.agregarObjeto("esIntranet", "0"); 
		
		if (sid.trim().length() > 0 && ts.trim().length() > 0) {
			accionweb.agregarObjeto("esIntranet", "1"); 
			if (accionweb.getSesion().getAttribute("rutUsuario") != null) {
				accionweb.getSesion().removeAttribute("rutUsuario");
				if (accionweb.getSesion().getAttribute("listaUnidad") != null) {
					accionweb.getSesion().removeAttribute("listaUnidad");
					accionweb.getSesion().setAttribute("listaUnidad", null);
				}
				if (accionweb.getSesion().getAttribute("codigoPerfil") != null) {
					accionweb.getSesion().removeAttribute("codigoPerfil");
					accionweb.getSesion().setAttribute("codigoPerfil", null);
				}
				if(accionweb.getSesion().getAttribute("rutOrigen") != null){
					accionweb.getSesion().removeAttribute("rutOrigen");
					accionweb.getSesion().setAttribute("rutOrigen",null);
				}
			}
			LoginLDAPBean loginBeanId = new LoginLDAPBean();
			loginBeanId.setRemoteaddr(accionweb.getReq().getRemoteAddr());
			rutUsuario = loginBeanId.getUsuario_Intranet(sid,ts, accionweb.getReq().getRemoteAddr());
			dv = loginBeanId.getDv();

			Date fecActual = new Date();		     
			int anno = 1900 + fecActual.getYear() ;
			// Pedro propone si el mes es mayor que 7 que presupueste el año siguiente sino el actual
			if(fecActual.getMonth() > 7)
				anno++;
			/* inicio - se asigna fijo */
			// anno = 2010;
			/* fin - se asigna fijo */
			
			accionweb.getSesion().setAttribute("anno", anno);
			accionweb.agregarObjeto("anno", anno);
			if (rutUsuario > 0) {
				codigoPerfil = moduloPerfiles.getCodigoPerfilUsuario(rutUsuario);
				moduloFinanza.agregaUsuario(accionweb.getReq(), rutUsuario,
						Funcionario, codigoPerfil,dv);
				accionweb.agregarObjeto("rutUsuario", rutUsuario);
				esLogin = true;
				resul = 1;

				descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
				funcionarioBean.buscar_funcionario(rutUsuario);
				Funcionario = funcionarioBean.getNombre();
				dv = funcionarioBean.getDv();

				moduloFinanza.agregaUsuario(accionweb.getReq(), rutUsuario,
						Funcionario, codigoPerfil, dv);

			}
		}
		else {
			accionweb.agregarObjeto("esIntranet", "0"); 
		}

		accionweb.agregarObjeto("rutUsuario", rutUsuario);
		accionweb.agregarObjeto("funcionario", Funcionario);
		accionweb.agregarObjeto("esLogin", resul); // 1 verdadero
		accionweb.agregarObjeto("codigoPerfil", codigoPerfil);
		if(accionweb.getSesion().getAttribute("presupItemreq") != null)
			accionweb.getSesion().removeAttribute("presupItemreq");
		if(accionweb.getSesion().getAttribute("resultadoPresup") != null)
			accionweb.getSesion().removeAttribute("resultadoPresup");
		if(accionweb.getSesion().getAttribute("resultadoPresupImport") != null)
			accionweb.getSesion().removeAttribute("resultadoPresupImport");
		if(accionweb.getSesion().getAttribute("Original") != null)
			accionweb.getSesion().removeAttribute("Original");
		if(accionweb.getSesion().getAttribute("resultadoDetalleItemReq") != null)
			accionweb.getSesion().removeAttribute("resultadoDetalleItemReq");
		if(accionweb.getSesion().getAttribute("resultadoPresupNomina") != null)
			accionweb.getSesion().removeAttribute("resultadoPresupNomina");
		if(accionweb.getSesion().getAttribute("resultadoPresupVacante") != null)
			accionweb.getSesion().removeAttribute("resultadoPresupVacante");
		if(accionweb.getSesion().getAttribute("resultadoPresupRemuneracion") != null)
			accionweb.getSesion().removeAttribute("resultadoPresupRemuneracion");
		if(accionweb.getSesion().getAttribute("resultadoPresupIngresos") != null)
			accionweb.getSesion().removeAttribute("resultadoPresupIngresos");
	}	

	/*
	 * esIntranet:
	 * Si el sistema es llamado desde la intranet va a leer un request SID y de un TS
	 */
	public void esIntranet(AccionWeb accionweb) throws Exception {
		System.out.println("linea 26" );
		String sid = Util.validaParametro(accionweb.getParameter("sid"), "");
		String ts = Util.validaParametro(accionweb.getParameter("ts"), "");
		if (sid.trim().length() > 0 && ts.trim().length() > 0) {
			accionweb.agregarObjeto("esIntranet", "1"); // 1 verdadero
			cargaIntranet(accionweb);
		} 
		else 
			accionweb.agregarObjeto("esIntranet", "0"); 
	}			
	public void loginSimulador(AccionWeb accionweb) throws Exception {
		System.out.println("linea 27" );
		if (accionweb.getSesion().getAttribute("listaUnidad") != null)
			accionweb.getSesion().setAttribute("listaUnidad", null);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutOrigen  = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen") +"","0"));
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);	
		String nomSimulacion = "";
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		
		/*hacer este rut rutUsuario*/
		if(rutnum > 0)
		{ 	if(rutOrigen == 0)
				rutOrigen = rutUsuario;
			rutUsuario = rutnum;
		} else {
				if(rutOrigen != 0) // lo vuelve a lo original
				{
					rutUsuario = rutOrigen ;
					rutOrigen = 0;
				}
				} 
			if(rutOrigen > 0) {
			accionweb.agregarObjeto("rutOrigen", rutOrigen);
			descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
			funcionarioBean.buscar_funcionario(rutUsuario);
		    nomSimulacion = funcionarioBean.getNombre();
			accionweb.agregarObjeto("nomSimulacion", nomSimulacion);
			
		}	
		if(accionweb.getSesion().getAttribute("resultadoPresupIngresos") != null)
			accionweb.getSesion().removeAttribute("resultadoPresupIngresos");
		
		accionweb.getSesion().setAttribute("rutUsuario", rutUsuario);
		accionweb.getSesion().setAttribute("rutOrigen", rutOrigen);
		accionweb.getSesion().setAttribute("nomSimulacion", nomSimulacion);
		cuentasPresupuestarias(accionweb);
	}
	
	public void loginRemuneracion(AccionWeb accionweb) throws Exception {
		System.out.println("linea 28" );
    	int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutOrigen  = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen") +"","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv") +"","");
		String passwd = Util.validaParametro(accionweb.getParameter("passwd"),"");
		String nomSimulacion = "";
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		
		/*hacer este rut rutUsuario*/
		if(rutOrigen != 0) // lo vuelve a lo original
		{
			rutUsuario = rutOrigen ;
			rutOrigen = 0;
		}
		String clave = "";
		int fec = 0;
		PreswBean preswbean = new PreswBean("OCL", 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(dv);
		Collection<Presw18DTO> listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		for (Presw18DTO ss : listaPresw18){
			clave = ss.getNompro();
			fec = ss.getFecdoc();
		}
        if(clave.trim().equals(""))
        	accionweb.agregarObjeto("mensaje", "No tiene registrada clave, dirñjase al Jefe de remuneraciones, anexo 4377.");
        else
        	if(!passwd.trim().equals(clave.trim()))
        		accionweb.agregarObjeto("mensaje", "La clave no coincide con la se tiene registrada.");
		accionweb.agregarObjeto("passwd", passwd);
		accionweb.agregarObjeto("esRemuneracion", 1); // quiere decir que estñ correcta la contraseña
		accionweb.getSesion().setAttribute("rutUsuario", rutUsuario);
		accionweb.getSesion().setAttribute("rutOrigen", rutOrigen);
		accionweb.getSesion().setAttribute("nomSimulacion", nomSimulacion);
		
	}
	
	public void cuentasPresupuestarias(AccionWeb accionweb) throws Exception {
		System.out.println("linea 29" );
		if (accionweb.getSesion().getAttribute("listaUnidad") != null)
			accionweb.getSesion().setAttribute("listaUnidad", null);
		// Collection<Presw18> listaPresw18 = (Collection<Presw18>)
		// Presw18Collection.generateCollectionPrr();
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "","0"));
		if(rutUsuario > 0){
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		String tipcue = Util.validaParametro(accionweb.getParameter("cuentas"),"");
		PreswBean preswbean = new PreswBean("PRR", 0, 0, 0, 0, rutUsuario);
		preswbean.setTipcue(tipcue);
		Collection<Presw18DTO> listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		String host = accionweb.getReq().getLocalAddr();
		moduloFinanza.agregaUnidad(accionweb.getReq(), listaPresw18);
		accionweb.agregarObjeto("cuentasPresupuestarias", accionweb.getSesion().getAttribute("listaUnidad"));
		DetalleServicio detalleServicio = new DetalleServicio();
		detalleServicio = moduloFinanza.getDetalleServicio(new Long(30));
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();
		tipoAccionServicio = moduloFinanza.getTipoAccionServicio(1);
		String parametro = "PRR";
		if(rutOrigen > 0)
			parametro = "sim:"+rutUsuario + "@" + parametro;
		
		AuditoriaServicio auditoriaServicio = new AuditoriaServicio();
		auditoriaServicio.setDetalleServicio(detalleServicio);
		auditoriaServicio.setTipoAccionServicio(tipoAccionServicio);
		auditoriaServicio.setFecha(new Date());
		auditoriaServicio.setHost(host);
		auditoriaServicio.setParametro(parametro);
		if(rutOrigen > 0)
			auditoriaServicio.setUsuario(rutOrigen);
		else
			auditoriaServicio.setUsuario(rutUsuario);
		try {
			moduloFinanza.saveAuditoriaServicio(auditoriaServicio);
			if(accionweb.getSesion().getAttribute("rutOrigen") != null){
				accionweb.getSesion().setAttribute("rutOrigen", accionweb.getSesion().getAttribute("rutOrigen"));
				accionweb.agregarObjeto("rutOrigen", accionweb.getSesion().getAttribute("rutOrigen"));
			}
			//System.out.println("origen: "+accionweb.getSesion().getAttribute("rutOrigen"));
			log.debug("Si guardada auditoria cuentasPresupuestarias");
			accionweb.agregarObjeto("control", control);
			accionweb.agregarObjeto("cuentas", tipcue);
		} catch (Exception e) {
			log.debug("No guardada auditoria cuentasPresupuestarias");
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		}
	}
	
	 synchronized public void agregarPeriodo(AccionWeb accionweb) throws Exception {
		 System.out.println("linea 30" );
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "","0"));
		int a�o =Integer.parseInt( Util.validaParametro(accionweb.getSesion().getAttribute("anno")+"","0"));
		
		if(rutUsuario > 0){
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		String fechaInicio = Util.validaParametro(accionweb.getParameter("fechaIni"),"");
		String fechaTermino = Util.validaParametro(accionweb.getParameter("fechaTer"),"");
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		String fechaActual = new SimpleDateFormat("dd/MM/yyyy").format(new Date());

		try {
		
			PeriodoProcesos periodoProceso = moduloFinanza.getPeriodoProcesos(anno);
			if(periodoProceso != null && periodoProceso.getAnno() == anno)
				accionweb.agregarObjeto("mensaje", "Este periodo ya se encuentra registrado");
			else {
				periodoProceso = new PeriodoProcesos();
				Sedes sede = new Sedes();
				sede.setCodigo(1);
				TipoJornadas jornada = new TipoJornadas();
				jornada.setCodigo(1);
				jornada.setNombre("Diurno");
				periodoProceso.setSede(sede);
				periodoProceso.setJornada(jornada);
				periodoProceso.setCodigoProceso(46);
				periodoProceso.setAnno(anno);
				periodoProceso.setSemestre(1);
				periodoProceso.setFechaInicio(new SimpleDateFormat("dd/MM/yyyy").parse(fechaInicio));	
				periodoProceso.setFechaTermino(new SimpleDateFormat("dd/MM/yyyy").parse(fechaTermino));
				periodoProceso.setFechaModificacion(new SimpleDateFormat("dd/MM/yyyy").parse(fechaActual));
				periodoProceso.setRutUsuario(rutUsuario);
				moduloFinanza.savePeriodoProcesos(periodoProceso);
				accionweb.agregarObjeto("mensaje", "Periodo registrado");
				List<PeriodoProcesos> listaPeriodo = moduloFinanza.getPeriodosProceso();
				if(listaPeriodo != null && listaPeriodo.size() > 0)
					accionweb.agregarObjeto("listaPeriodo",listaPeriodo);
				}
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
	    int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		accionweb.getSesion().setAttribute("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		
		accionweb.agregarObjeto("anno", a�o); 
		accionweb.agregarObjeto("control", control);
		}
	
	}
	 synchronized public void modificarPeriodo(AccionWeb accionweb) throws Exception {
		 System.out.println("linea 31" );
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "","0"));
		int a�o =Integer.parseInt( Util.validaParametro(accionweb.getSesion().getAttribute("anno")+"","0"));
		if(rutUsuario > 0){
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		String fechaTermino = Util.validaParametro(accionweb.getParameter("fechaTermino"),"");
		String fechaInicio = Util.validaParametro(accionweb.getParameter("fechaInicio"),"");
		String fechaActual = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
		
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		try {
			int fecTer = 0;
			PeriodoProcesos periodoProceso = moduloFinanza.getPeriodoProcesos(anno);
			if(periodoProceso != null){
			periodoProceso.setFechaTermino(new SimpleDateFormat("dd/MM/yyyy").parse(fechaTermino));
			periodoProceso.setFechaInicio(new SimpleDateFormat("dd/MM/yyyy").parse(fechaInicio));
			periodoProceso.setFechaModificacion(new SimpleDateFormat("dd/MM/yyyy").parse(fechaActual));
			periodoProceso.setRutUsuario(rutUsuario);
			moduloFinanza.savePeriodoProcesos(periodoProceso);
			accionweb.agregarObjeto("mensaje", "Periodo modificado");
			List<PeriodoProcesos> listaPeriodo = moduloFinanza.getPeriodosProceso();
			if(listaPeriodo != null && listaPeriodo.size() > 0)
				accionweb.agregarObjeto("listaPeriodo",listaPeriodo);
	
			} else {
				accionweb.agregarObjeto("mensaje", "No existe este periodo");
			}
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
	    int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		accionweb.getSesion().setAttribute("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		
		accionweb.agregarObjeto("anno", a�o); 
		accionweb.agregarObjeto("control", control);
		}
	}
	 synchronized public void eliminarPeriodo(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "","0"));
		int a�o =Integer.parseInt( Util.validaParametro(accionweb.getSesion().getAttribute("anno")+"","0"));
		if(rutUsuario > 0){
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);// 1 = control presupuestario; 2 = Presupuestacion
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		try {
			PeriodoProcesos periodoProceso = moduloFinanza.getPeriodoProcesos(anno);

			moduloFinanza.deletePeriodoProceso(periodoProceso);
			accionweb.agregarObjeto("mensaje", "Periodo Eliminado");
			accionweb.agregarObjeto("mensaje", "Periodo registrado");
			List<PeriodoProcesos> listaPeriodo = moduloFinanza.getPeriodosProceso();
			if(listaPeriodo != null && listaPeriodo.size() > 0)
				accionweb.agregarObjeto("listaPeriodo",listaPeriodo);
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
	    int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		accionweb.getSesion().setAttribute("opcionMenu", opcionMenu);
		accionweb.agregarObjeto("opcionMenu", opcionMenu);
		
		accionweb.agregarObjeto("anno", a�o); 
		accionweb.agregarObjeto("control", control);
		}
	}
	 synchronized public void ejecutaFacturas(AccionWeb accionweb) throws Exception {
		 try {
		 Vector vec = moduloFinanza.getDatosASFactura();
			//System.out.println("vec " + vec);
			if (vec.size() > 0) { 
				moduloFinanza.saveCuentasPorCobrar(vec);
			}
		 } catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado");
				accionweb.agregarObjeto("exception ejecutaFacturas: ", e.toString());
				e.printStackTrace();
			}
	 }
	
}
