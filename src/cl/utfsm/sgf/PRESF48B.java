package cl.utfsm.sgf;

import java.io.Serializable;

public class PRESF48B implements Serializable{
	private int ANOPRE;         //4S 0       COLHDG('A�O DE PRESUPUEST') 
	private String CODORG;         //10A         COLHDG('CODIGO ORGANIZACI') 
	private long REMVA1;         //9S 0       COLHDG('RE.VARIABLE A�O 1') 
	private long REMVA2;         //9S 0       COLHDG('RE.VARIABLE A�O 2') 
	private long REMVA3;         //9S 0       COLHDG('RE.VARIABLE A�O 3') 
	private long REMVA4;         //9S 0       COLHDG('RE.VARIABLE A�O 4') 
	private long CANPA1;         //9S 0       COLHDG('PARALELOS   SEM 1') 
	private long CANPA2;         //9S 0       COLHDG('PARALELOS   SEM 2') 
	private long ALUAT1;         //9S 0       COLHDG('ALUMNOS ATENSEM 1') 
	private long ALUAT2;         //9S 0       COLHDG('ALUMNOS ATENSEM 2') 
	private long HORPT1;         //9S 0       COLHDG('HORAS PTIME SEM 1') 
	private long HORPT2;         //9S 0       COLHDG('HORAS PTIME SEM 2') 
	private long COSPT1;         //9S 0       COLHDG('COSTO PTIME SEM 1') 
	private long COSPT2;         //9S 0       COLHDG('COSTO PTIME SEM 2') 
	private long HORHO1;         //9S 0       COLHDG('HORAS HONOR SEM 1') 
	private long HORHO2;
	private long COSHO1;
	private long COSHO2;         //9S 0       COLHDG('COSTO HONOR SEM 2') 
	private long HORFT1;         //9S 0       COLHDG('HORAS FTIME SEM 1') 
	private long HORFT2;         //9S 0       COLHDG('HORAS FTIME SEM 2') 
	private long COSFT1;         //9S 0       COLHDG('COSTO FTIME SEM 1') 
	private long COSFT2;         //9S 0       COLHDG('COSTO FTIME SEM 2') 
	private long HORAY1;         //9S 0       COLHDG('HORAS AYUD  SEM 1') 
	private long HORAY2;         //9S 0       COLHDG('HORAS AYUD  SEM 2') 
	private long COSAY1;         //9S 0       COLHDG('COSTO AYUD  SEM 1') 
	private long COSAY2;         //9S 0       COLHDG('COSTO AYUD  SEM 2') 
	private String ESTADO;         //1A         COLHDG('ESTADO A:ACEPTADO') 
	private int FECACE;         //8S 0       COLHDG('FECHA ACEPTACION')  
	public int getANOPRE() {
		return ANOPRE;
	}
	public void setANOPRE(int anopre) {
		ANOPRE = anopre;
	}
	public String getCODORG() {
		return CODORG;
	}
	public long getREMVA1() {
		return REMVA1;
	}
	public void setREMVA1(long remva1) {
		REMVA1 = remva1;
	}
	public long getREMVA2() {
		return REMVA2;
	}
	public void setREMVA2(long remva2) {
		REMVA2 = remva2;
	}
	public long getREMVA3() {
		return REMVA3;
	}
	public void setREMVA3(long remva3) {
		REMVA3 = remva3;
	}
	public long getREMVA4() {
		return REMVA4;
	}
	public void setREMVA4(long remva4) {
		REMVA4 = remva4;
	}
	public long getCANPA1() {
		return CANPA1;
	}
	public void setCANPA1(long canpa1) {
		CANPA1 = canpa1;
	}
	public long getCANPA2() {
		return CANPA2;
	}
	public void setCANPA2(long canpa2) {
		CANPA2 = canpa2;
	}
	public long getALUAT1() {
		return ALUAT1;
	}
	public void setALUAT1(long aluat1) {
		ALUAT1 = aluat1;
	}
	public long getALUAT2() {
		return ALUAT2;
	}
	public void setALUAT2(long aluat2) {
		ALUAT2 = aluat2;
	}
	public long getHORPT1() {
		return HORPT1;
	}
	public void setHORPT1(long horpt1) {
		HORPT1 = horpt1;
	}
	public long getHORPT2() {
		return HORPT2;
	}
	public void setHORPT2(long horpt2) {
		HORPT2 = horpt2;
	}
	public long getCOSPT1() {
		return COSPT1;
	}
	public void setCOSPT1(long cospt1) {
		COSPT1 = cospt1;
	}
	public long getCOSPT2() {
		return COSPT2;
	}
	public void setCOSPT2(long cospt2) {
		COSPT2 = cospt2;
	}
	public long getHORHO1() {
		return HORHO1;
	}
	public void setHORHO1(long horho1) {
		HORHO1 = horho1;
	}
	public long getCOSHO2() {
		return COSHO2;
	}
	public void setCOSHO2(long cosho2) {
		COSHO2 = cosho2;
	}
	public long getHORFT1() {
		return HORFT1;
	}
	public void setHORFT1(long horft1) {
		HORFT1 = horft1;
	}
	public long getHORFT2() {
		return HORFT2;
	}
	public void setHORFT2(long horft2) {
		HORFT2 = horft2;
	}
	public long getCOSFT1() {
		return COSFT1;
	}
	public void setCOSFT1(long cosft1) {
		COSFT1 = cosft1;
	}
	public long getCOSFT2() {
		return COSFT2;
	}
	public void setCOSFT2(long cosft2) {
		COSFT2 = cosft2;
	}
	public long getHORAY1() {
		return HORAY1;
	}
	public void setHORAY1(long horay1) {
		HORAY1 = horay1;
	}
	public long getHORAY2() {
		return HORAY2;
	}
	public void setHORAY2(long horay2) {
		HORAY2 = horay2;
	}
	public long getCOSAY1() {
		return COSAY1;
	}
	public void setCOSAY1(long cosay1) {
		COSAY1 = cosay1;
	}
	public long getCOSAY2() {
		return COSAY2;
	}
	public void setCOSAY2(long cosay2) {
		COSAY2 = cosay2;
	}
	public String getESTADO() {
		return ESTADO;
	}
	public void setESTADO(String estado) {
		ESTADO = estado;
	}
	public int getFECACE() {
		return FECACE;
	}
	public void setFECACE(int fecace) {
		FECACE = fecace;
	}
	public void setCODORG(String codorg) {
		CODORG = codorg;
	}
	public long getHORHO2() {
		return HORHO2;
	}
	public void setHORHO2(long horho2) {
		HORHO2 = horho2;
	}
	public long getCOSHO1() {
		return COSHO1;
	}
	public void setCOSHO1(long cosho1) {
		COSHO1 = cosho1;
	}
	
	
}
