package cl.utfsm.sgf;

import java.io.Serializable;

public class COCOF102 implements Serializable{
	private int SUCUR;  	// NOT NULL NUMBER(2) 
	private int NUMVAL; 	// NOT NULL NUMBER(8) 
	private int FECOPE; 	// NOT NULL NUMBER(8) 
	private int HOROPE; 	// NOT NULL NUMBER(6) 
	private String TIPOPE; 	// NOT NULL CHAR(1)   
	private int RUTIDE;		// NOT NULL NUMBER(8) 
	private String DIGIDE;	// NOT NULL CHAR(1)   
	private int CODUNI;		// NOT NULL NUMBER(6) 
	private String COMENT;	// NOT NULL CHAR(40)  
	public int getSUCUR() {
		return SUCUR;
	}
	public void setSUCUR(int sucur) {
		SUCUR = sucur;
	}
	public int getNUMVAL() {
		return NUMVAL;
	}
	public void setNUMVAL(int numval) {
		NUMVAL = numval;
	}
	public int getFECOPE() {
		return FECOPE;
	}
	public void setFECOPE(int fecope) {
		FECOPE = fecope;
	}
	public int getHOROPE() {
		return HOROPE;
	}
	public void setHOROPE(int horope) {
		HOROPE = horope;
	}
	public String getTIPOPE() {
		return TIPOPE;
	}
	public void setTIPOPE(String tipope) {
		TIPOPE = tipope;
	}
	public int getRUTIDE() {
		return RUTIDE;
	}
	public void setRUTIDE(int rutide) {
		RUTIDE = rutide;
	}
	public String getDIGIDE() {
		return DIGIDE;
	}
	public void setDIGIDE(String digide) {
		DIGIDE = digide;
	}
	public int getCODUNI() {
		return CODUNI;
	}
	public void setCODUNI(int coduni) {
		CODUNI = coduni;
	}
	public String getCOMENT() {
		return COMENT;
	}
	public void setCOMENT(String coment) {
		COMENT = coment;
	}
}
