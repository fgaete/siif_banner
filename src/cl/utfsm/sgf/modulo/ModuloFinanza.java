package cl.utfsm.sgf.modulo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.velocity.tools.generic.DateTool;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;

import cl.utfsm.base.auditoria.AuditoriaServicio;
import cl.utfsm.base.auditoria.DetalleServicio;
import cl.utfsm.base.auditoria.TipoAccionServicio;
import cl.utfsm.base.util.ConexionBD;
import cl.utfsm.base.util.ConexionBanner;
import cl.utfsm.conexion.ConexionAs400Factura;
import cl.utfsm.conexion.ConexionAs400ValePago;
//import cl.utfsm.sgf.COCOF100;
//import cl.utfsm.sgf.Listado;
import cl.utfsm.sgf.datos.FinanzaDao;
import cl.utfsm.sip.PeriodoProcesos;
import descad.presupuesto.Presw18DTO;

public class ModuloFinanza {
	FinanzaDao finanzaDao;
	public void setFinanzaDao(FinanzaDao finanzaDao) {
		this.finanzaDao = finanzaDao;
	}
	public boolean getPeriodoProcesos() throws Exception{
		return finanzaDao.getPeriodoProcesos();
	}
	public void agregaUsuario(HttpServletRequest req, int rutUsuario, String funcionario, int codigoPerfil, String dv){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("rutUsuario",rutUsuario);
		sesion.setAttribute("funcionario",funcionario);
		sesion.setAttribute("codigoPerfil", codigoPerfil);
		sesion.setAttribute("dv", dv);
		
	}
	public void agregaUnidad(HttpServletRequest req, Collection<Presw18DTO> lista){
//		public void agregaUnidad(HttpServletRequest req, Collection<Presw18> lista){
			HttpSession sesion = req.getSession();
		/*esto es para probar 
			Presw18DTO lista2 = new Presw18DTO();
			lista2.setCoduni(340610);
			lista2.setDesuni("DEPARTAMENTO DE INDUSTRIA, OPERACI�N");
			lista.add(lista2);
			/*lista2 = new Presw18DTO();
			lista2.setCoduni(111319);
			lista2.setDesuni("INGRESOS PROPIOS UNID. ACADEMICAS");
			lista.add(lista2);*/
			/*hasta ac�*/
			sesion.setAttribute("listaUnidad",lista);
			
		}
	public DetalleServicio getDetalleServicio(Long id){
		return finanzaDao.getDetalleServicio(id);
	}
	 public TipoAccionServicio getTipoAccionServicio(int codigo){
		 return finanzaDao.getTipoAccionServicio(codigo);
	 }
	 public void saveAuditoriaServicio (AuditoriaServicio auditoriaServicio){
	    	finanzaDao.saveAuditoriaServicio(auditoriaServicio);
	    }
	   
	 public List<PeriodoProcesos> getPeriodosProceso() throws Exception {
			
		return finanzaDao.getPeriodosProceso();
		}
	 public List<PeriodoProcesos> getHabilitaOpcion() throws Exception {
			
			return finanzaDao.getHabilitaOpcion();
			}
	 public void savePeriodoProcesos(PeriodoProcesos periodoProceso){
		    	finanzaDao.savePeriodoProcesos(periodoProceso);
		    }
		public void deletePeriodoProceso (PeriodoProcesos periodoProcesos)throws Exception {
			finanzaDao.deletePeriodoProceso(periodoProcesos);
		}
	 public PeriodoProcesos getPeriodoProcesos(int anno) throws Exception {
			 return finanzaDao.getPeriodoProcesos(anno);
		  }
	/* public List<Listado> getvaleSinFinanciamiento(int codUnidad, int a�o)  throws Exception {
			return finanzaDao.getvaleSinFinanciamiento(codUnidad, a�o);
	 }*/
	 
	  public Vector getDatosASFactura(){
		 ConexionAs400Factura con = new ConexionAs400Factura();
		 Vector vec = new Vector();
		 Vector detalle = null;
		     
		 try {
			String query = " SELECT DISTINCT v1.TIPDOC, v1.NUMDOC, v1.RUTIDE, v1.DIGIDE, v1.FECEMI, v1.VALNET, " + 
						   " v1.VALEXE, v1.VALIVA, v2.RUTIDE, v2.DIGIDE, v3.GLOSA, v4.TIPSER, v5.CUEBAN, " +
						   " v6.CODDET, v1.TIPCUE, v7.RUTALU, v7.DIGALU, v7.PERIOD, v7.CODDET, v1.NUMINT, v1.FACNOT" +
						   " FROM USMMBP.COCOF04B v1, " +
						   " USMMBP.COFAF103B v2, " +
						   " USMMBP.COCOF05B v3, " +
						   " USMMBP.COFAF01B v4, " +
						   " USMMBP.COFAF104B v5, " +
						   " USMMBP.COCOF161B v6, " +
						   " USMMBP.COFAF101B v7 " + 
						   " WHERE v1.ESTDOC = 'R' " +  
						   " AND v1.NUMINT = v2.NUMINT " +  
						   " AND v2.TIPOPE = 'T' " +  
						   " AND v1.NUMINT = v7.NUMINT " +  
						   " AND v1.NUMINT = v3.NUMINT " + 
						   " AND v1.TIPDOC = v3.TIPDOC " + 
						   " AND v3.TIPCUE = 'C' " +  
						   " AND v3.SECUEN = 1 " +
						   " AND v1.NUMINT = v4.NUMINT " +  
						   " AND v4.TIPSER = v5.TIPSER " + 
						   " AND v1.CODORG = v6.CODORG " + 
						   " AND v4.TIPSER = v6.TIPSER ";
			//System.out.println(query);
			
		  	PreparedStatement sent = con.getConnection().prepareStatement(query);
                                                                          //" AND v4.TIPSER = V6.TIPSER"); 
                                                                 
		       ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			     if (!res.getString(14).trim().equals("")){  	
			       detalle = new Vector();
			       detalle.add(res.getString(1).trim()); // tipo Documento; 0
				   detalle.add(res.getString(2).trim()); // numero documento; 1
				   detalle.add(res.getString(3).trim()); // rut del documento 2
				   detalle.add(res.getString(4).trim()); // digito del documento 3
				   detalle.add(res.getString(5).trim()); // Fecha de emision del documento 4
				   detalle.add(res.getLong(6)); // Valor neto 5
				   detalle.add(res.getLong(7)); // valor exento 6
				   detalle.add(res.getLong(8)); // Valor iva 7
				   detalle.add(res.getString(9).trim()); // rut de la usuario 8
				   detalle.add(res.getString(10).trim()); // digito del usuario 9
				   detalle.add(res.getString(11).trim().toUpperCase()); // Glosa 10
				   detalle.add(res.getString(12).trim()); // Tipo de srvicio 11
				   detalle.add(res.getString(13).trim()); // cuenta  12
				   detalle.add(res.getString(14).trim()); // codigo de detalle 13
				   detalle.add(res.getString(15).trim()); // Tipo de cuenta 14
				   detalle.add(res.getString(16).trim()); // Rut del alumno en caso de ser serv. Educ 15
				   detalle.add(res.getString(17).trim()); // dig del alumno en caso de ser serv. Educ. 16
				   detalle.add(res.getString(18).trim()); // periodo del cargo del alumno 17
				   detalle.add(res.getString(19).trim()); // c�digo de detalle del cargo del alumno 18
				   detalle.add(res.getString(20).trim()); // numero interno de la factura 19
				   detalle.add(res.getString(21).trim()); // numero de documento al que se le hace la Nota de credito o debito 20
				   vec.add(detalle);
			     }
			     }
			     res.close();
			     sent.close();
			     // con.close();
			     con.getConnection().close();

			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloFinanza.getDatosASFactura : " + e );
			    }
			    return vec;
		}
	  
	    public int getActivacionBannerFacturas(int tipo){ 	
	     int activo = 0;
      	 String query = " SELECT activo FROM activacion WHERE tipo = '" + tipo +"'";
      	 try{
      	  ConexionBD conORA = new ConexionBD();	 
      	  Statement sent = conORA.getConexion().createStatement();
      	  ResultSet res  = sent.executeQuery(query);
      	  if (res.next()) 
      		  activo = res.getInt(1);
      	  res.close();
      	  sent.close();
      	  conORA.CerrarConexion();
      	}
      	catch (SQLException e){
      	   System.out.println("Error ModuloFinanzas.getActivacionBannerFacturas: "+e );}
      	return activo;
      }
	  
	    
	    
	    synchronized public void saveCuentasPorCobrar(Vector datos) throws SQLException{
			ConexionAs400Factura con = new ConexionAs400Factura();
			Connection conexion = con.getConnection(); 
			
			ConexionBanner conB = new ConexionBanner();
			Connection conexionB = conB.getConexion(); 
      		
			Vector v = null;
			String rutdv = "";
			String rutdvUsuario = "";
			int fechaEmision = 0;
			String codDetalle = "";
			long valorNet = 0;
			long valorExe = 0;
			long valorIva = 0;
			String descDetalle = "";
			int numDoc = 0;
			int numDocNota = 0;
			String tipCue = "";
			String tipDoc = "";
			String rutdvAlumno = "";
			String codDetAlumno = "";
			int perDetAlumno = 0;
			
			
			//System.out.println("datos " + datos);
			
			for (int i = 0; i < datos.size(); i++) {
			   v = (Vector) datos.get(i);
			   rutdv = v.get(2)+ "" + v.get(3);
			   rutdvUsuario = v.get(8)+ "" + v.get(9);
			   fechaEmision = Integer.parseInt(v.get(4)+"");
			   codDetalle = v.get(13)+ "";
			   valorNet = Long.parseLong(v.get(5)+"");
			   valorExe = Long.parseLong(v.get(6)+"");
			   valorIva = Long.parseLong(v.get(7)+"");
			   descDetalle = v.get(10)+ "";
			   numDoc = Integer.parseInt(v.get(1)+"");
			   numDocNota = Integer.parseInt(v.get(20)+"");
			   tipCue = v.get(14)+ "";
			   tipDoc = v.get(0)+ "";	
			   rutdvAlumno = v.get(15)+ "" + v.get(16);
			   perDetAlumno = Integer.parseInt(v.get(17)+"");
			   codDetAlumno = v.get(18)+ "";
			   
			     
			   
				int error = 0;
	       		int tran_number = 0;
	       		try{ // p_rutdv VARCHAR2, p_rutdvUsuario VARCHAR2, p_fechaEmision NUMBER, p_detail_code VARCHAR2, p_valnet NUMBER, p_valexe NUMBER,
	       		    	// p_valiva NUMBER, p_desc VARCHAR2,p_numdoc NUMBER, p_cod_error	OUT NUMBER
	       		      	
	       		    	System.out.println("exec P_USM_TBRACCD_TRAN_SIIF('" + rutdv+"','"+rutdvUsuario+"',"+fechaEmision+",'"+codDetalle+"',"+
	       		    			            valorNet+","+valorExe+","+valorIva+",'"+descDetalle+"',"+numDoc+",'"+rutdvAlumno+"',"+
	       		    			            perDetAlumno+",'"+codDetAlumno+"','"+tipDoc+"',"+numDocNota);
	       		      CallableStatement sentB = conexionB.prepareCall("{call P_USM_TBRACCD_TRAN_SIIF(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			          sentB.setString(1,rutdv);
			          sentB.setString(2,rutdvUsuario);
			          sentB.setInt(3,fechaEmision);
			          sentB.setString(4,codDetalle);
			          sentB.setLong(5,valorNet);
			          sentB.setLong(6,valorExe);
			          sentB.setLong(7,valorIva);
			          sentB.setString(8,descDetalle);
			          sentB.setInt(9,numDoc);
			          sentB.setString(10,rutdvAlumno);
			          sentB.setInt(11,perDetAlumno);
			          sentB.setString(12,codDetAlumno);
			          sentB.setString(13,tipDoc);
			          sentB.setInt(14,numDocNota);
			          sentB.registerOutParameter(15, Types.INTEGER);
			          sentB.registerOutParameter(16, Types.INTEGER);
			         
			          sentB.executeQuery();
			          tran_number = sentB.getInt(15);
	       		      error = sentB.getInt(16);
			          
			          if (error == 0 && tran_number > 0) { 
			        	try {  
			        	 PreparedStatement sent = conexion.prepareStatement(" UPDATE USMMBP.COCOF04B SET ESTDOC = 'T' " +
			                                                                           " WHERE NUMDOC = ? " +
			                                                                           " AND TIPCUE = ? " +
			                                                                           " AND TIPDOC = ? " +
			                                                                           " AND RUTIDE = ? " +
			                                                                           " AND ESTDOC = 'R'");
						  	
						 sent.setInt(1,numDoc);
						 sent.setString(2,tipCue);
						 sent.setString(3,tipDoc);
						 sent.setString(4,v.get(2)+"");
						 int res = sent.executeUpdate();
			             if(res < 0){
			               	error = -1;
			             }   	
						 sent.close();
						 
			        	} 
			        	catch (SQLException e){
						  	System.out.println("Error moduloFinanza.saveCuentasPorCobrar update ISERIES: " + e);
			       		} 
			         }
			        sentB.close();
			       
			      }
	       		  catch (SQLException e){
				   	System.out.println("Error moduloFinanza.saveCuentasPorCobrar procedure BANNER: " + e);
				   } 
	       		  
	             }
			conexionB.close();
			conexion.close();
			  }
	 // 14-03-18 MM Para mostrar un mensaje en la portada del sistema
	    // el campo ACT_NUM_ACTIVIDAD indica el orden de las l�neas a mostrar
	    // puede trabajar con fechas de tiempo para mostrar cada l�nea
	    // si la fecha de t�rmino es NULL se muestra desde la fecha de inicio hacia adelante
	    public StringBuffer getMensaje(){
	            StringBuffer mensaje = new StringBuffer();
	            String query = " SELECT ACT_DESCRIPCION FROM actividad " + 
	                  " WHERE COD_SISTEMA = 128 AND COD_VIGENCIA  = 1" + 
	                  " AND ((ACT_FECHA_TERMINO IS NOT NULL " +
	                  " AND SYSDATE BETWEEN ACT_FECHA_INICIO AND ACT_FECHA_TERMINO) OR " + 
	                  " (ACT_FECHA_TERMINO IS NULL AND ACT_FECHA_INICIO <= SYSDATE)) " + 
	                  " order by ACT_NUM_ACTIVIDAD";
//System.out.println("mensaje: "+query);
	        try {
	        	ConexionBD conORA = new ConexionBD();	 
	        	Statement sent = conORA.getConexion().createStatement();
	        	ResultSet res  = sent.executeQuery(query);
	            
	            while(res.next()) {
	                  mensaje.append(res.getString(1));
	            } 
	            res.close();
	            sent.close();
	            conORA.CerrarConexion();
	        }
	        catch (SQLException e){
	            System.out.println("Error en cl.utfsm.sgf.ModuloFinanza.getMensaje");
	            e.printStackTrace();
	        }

	        return mensaje;
	      }
	  
	    public String getMesActual(){
			 DateTool fechaActual = new DateTool();
			 String mesActual = "";
				
			 String mes = (fechaActual.getMonth()+1)+"";
			 if(mes.length()== 1)
						mes = "0" + mes;
			 mesActual = mes;
			 return mesActual;
		 }
		 public String getAnioActual(){
			 DateTool fechaActual = new DateTool();
			 String a�oActual = String.valueOf(fechaActual.getYear());
				
			 
			 return a�oActual.substring(2,4);
		 }
}

