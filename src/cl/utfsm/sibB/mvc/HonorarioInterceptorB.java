package cl.utfsm.sibB.mvc;
/**
 * SISTEMA DE BOLETAS DE HONORARIOS POR INTERNET
 * PROGRAMADOR		: 	M�NICA BARRERA FREZ
 * FECHA ULT.MODIF  : 	21/06/2012
 * UNIDAD DE DESARROLLO INSTITUCIONAL(DTI)  
 * migraci�n oracle : 15/06/2013 MB  
 */
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.tools.generic.DateTool;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.utfsm.POJO.Sede;
import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.sibB.modulo.ModuloHonorarioB;
import cl.utfsm.sivB.modulo.ModuloValePagoB;
import descad.cliente.CocowBean;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw25DTO;
/**
 * @author vsilva
 * VS.22-11-2016. modificaci�n BANNER. 
 *   ModuloHonorario -> ModuloHonorarioB
 *   ModuloValePago  -> ModuloValePagoB
 *   moduloHonorario -> moduloHonorarioB
 *   moduloValePago  -> moduloValePagoB
 */


public class HonorarioInterceptorB extends HandlerInterceptorAdapter{
	private ModuloHonorarioB moduloHonorarioB;
	private ModuloValePagoB moduloValePagoB;
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
		
	}
	public ModuloHonorarioB getModuloHonorarioB() {
		return moduloHonorarioB;
	}
	public void setModuloHonorarioB(ModuloHonorarioB moduloHonorarioB) {
		this.moduloHonorarioB = moduloHonorarioB;
	}
	

	public void cargarMenu(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
	  	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.getSesion().removeAttribute("autorizaProyecto");
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.getSesion().removeAttribute("autorizaUCP");
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.getSesion().removeAttribute("autorizaDIRPRE");
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.getSesion().removeAttribute("autorizaFinanzas");
      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
			accionweb.getSesion().removeAttribute("autorizaBoletas");
    	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
			accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
      	
	/* se debe buscar segun el usuario si tiene permiso para autorizar*/
	List<Presw18DTO> listaAutorizaciones = new ArrayList<Presw18DTO>();
	listaAutorizaciones = moduloHonorarioB.getConsultaAutoriza(rutUsuario, dv);
	if(listaAutorizaciones != null && listaAutorizaciones.size() > 0){
		 for (Presw18DTO ss : listaAutorizaciones){
			if(ss.getIndprc().trim().equals("Y")){
				accionweb.agregarObjeto("autorizaProyecto", 1);
				accionweb.getSesion().setAttribute("autorizaProyecto", 1);
			}
			if(ss.getIndprc().trim().equals("X")){
				accionweb.agregarObjeto("autorizaUCP", 1);
				accionweb.getSesion().setAttribute("autorizaUCP", 1);
			}
			if(ss.getIndprc().trim().equals("Z")){
				accionweb.agregarObjeto("autorizaDIRPRE", 1);
				accionweb.getSesion().setAttribute("autorizaDIRPRE", 1);
			}
			if(ss.getIndprc().trim().equals("F")){
				accionweb.agregarObjeto("autorizaFinanzas", 1);
				accionweb.getSesion().setAttribute("autorizaFinanzas", 1);
			}
			if(ss.getIndprc().trim().equals("B")){
				accionweb.agregarObjeto("autorizaBoletas", 1);
				accionweb.getSesion().setAttribute("autorizaBoletas", 1);
			}
			if(ss.getIndprc().trim().equals("D")){
				accionweb.agregarObjeto("autorizaDAF", 1);
				accionweb.getSesion().setAttribute("autorizaDAF", 1);
			}
		 }
	}
		if(rutOrigen > 0){
			this.limpiaSimulador(accionweb);
		}
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		if(listaSolicitudes != null && listaSolicitudes.size() > 0)
			accionweb.getSesion().removeAttribute("listaSolicitudes");
		
		accionweb.agregarObjeto("esBoletaB", 1); /* VS.22-11-2016.modificacion BANNER */
		accionweb.agregarObjeto("opcionMenu", tipo);
		
		accionweb.agregarObjeto("listaAutorizaciones", listaAutorizaciones);
	}
	public void limpiaSimulador(AccionWeb accionweb) throws Exception {
		if (accionweb.getSesion().getAttribute("listaUnidad") != null)
			accionweb.getSesion().setAttribute("listaUnidad", null);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutOrigen  = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen") +"","0"));
		String nomSimulacion = "";
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		
		/*hacer este rut rutUsuario*/
		if(rutOrigen != 0) // lo vuelve a lo original
		{
			rutUsuario = rutOrigen ;
			rutOrigen = 0;
		}
		 		
		accionweb.getSesion().setAttribute("rutUsuario", rutUsuario);
		accionweb.getSesion().setAttribute("rutOrigen", rutOrigen);
		accionweb.getSesion().setAttribute("nomSimulacion", nomSimulacion);
		accionweb.agregarObjeto("rutOrigen", rutOrigen);
		accionweb.agregarObjeto("nomSimulacion", nomSimulacion);		
	}
	
	public void getRevisaBoleta(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		int identificador = Util.validaParametro(accionweb.getParameter("identificador"), 0);
		int numeroBoleta = Util.validaParametro(accionweb.getParameter("numeroBoleta"), 0);
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"), "");
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		if(listaSolicitudes != null && listaSolicitudes.size() > 0)
			accionweb.getSesion().removeAttribute("listaSolicitudes");
		List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudesPago");
		if(listaSolicitudesPago != null && listaSolicitudesPago.size() > 0)
			accionweb.getSesion().removeAttribute("listaSolicitudesPago");		
	    int verificaBoleta = 0;
	    String mensaje = "";
		String nombre = "";
		String paisPrestador = "";
		String idSoli = "";
		Vector  v = new Vector();
		if(rutnum > 0){
			v = moduloHonorarioB.getVerificaBoleta(rutnum, dvrut,numeroBoleta,tipoBoleta);
			if(v!= null && v.size() > 0){
				verificaBoleta = Integer.parseInt(v.get(0).toString());
			if (verificaBoleta > 0){					
		    switch (verificaBoleta){
		    case 1:{mensaje = "El emisor es trabajador de la USM, debe comunicarse con Direcci�n de Finanzas, reenv�e correo de la boleta a bhesii@usm.cl.";
		    	break;
		    }                   
		    case 2:{mensaje = "La boleta ya existe en Sistema Administrativo Contable.";
	    	break;
	        }
		    case 3:{mensaje = "Boleta electr�nica no ha sido informada por SII.";
	    	break;
	        }
		    case 4:{mensaje = "El RUT asociado a la boleta est� objetado en Personal.";
	    	break;
	        }
		    case 5:{mensaje = "Ha sido ingresada previamente en el sistema en WEB.";
	    	break;
	        }
		    case 6:{mensaje = "Boleta anulada desde el SII.";
	    	break;
	        }
		    }
		    tipoBoleta = "";
		    numeroBoleta = 0;
			}else {// significa que es una boleta v�lida, 
		    	     // si es BOH ver si existe el 
		    	     // en caso de ser BHE se debe verificar si existe el prestador, si existe se muestran los datos,
		    	     // de lo contrario es necesario mostrar la pantalla que permite el ingreso del prestador
		    	    idSoli =  v.get(2).toString();
		    	    nombre = "";
		    	    String indHay = "";
					Collection<Presw18DTO> lista = moduloHonorarioB.getVerificaRutDocumento(String.valueOf(rutnum)+ dvrut);
					if(lista != null && lista.size() > 0){
						 for (Presw18DTO ss : lista){
							 nombre = ss.getDesuni();
							 paisPrestador = ss.getDesite();// pais prestador
							 indHay = ss.getIndhay();	
						   }
					}					
					
					if(nombre.trim().equals("")){
						mensaje = "Lo sentimos, este RUT no se encuentra registrado, debe solicitar a FINANZAS su creaci�n.";						
					}	
					else {
					 	
			    		 if(indHay.trim().equals("P"))
			    		    accionweb.agregarObjeto("mensaje", "Este RUT No existe en archivo maestro de proveedores.");
			    	     else {		// indHay = "A"					
						  if(moduloHonorarioB.getVerificaBoletaBanner(rutnum, dvrut,numeroBoleta,tipoBoleta) > 0){
							  mensaje = "Lo sentimos, este Documento ya se encuentra registrado en Banner.";	
						
					      } else
					     {
						    accionweb.agregarObjeto("rut_aux", rut_aux);  
							accionweb.agregarObjeto("rutnum", rutnum); 
							accionweb.agregarObjeto("dvrut", dvrut);
							
							accionweb.agregarObjeto("nomIdentificador", nombre);
							accionweb.agregarObjeto("paisIdentificador", nombre); 					
							accionweb.agregarObjeto("numeroBoleta", numeroBoleta);
							List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>(); 
							if(accionweb.getSesion().getAttribute("listCocow36DTO") != null) 
								accionweb.getSesion().removeAttribute("listCocow36DTO"); 
							
							CocowBean cocowBean = new CocowBean(); 
						    listCocow36DTO = cocowBean.buscar_cocow36_id(idSoli); 
					
							if(listCocow36DTO != null && listCocow36DTO.size() >0 ) 
								this.setdatosBoleta(accionweb,listCocow36DTO); 
							if(tipoBoleta.trim().equals("BOH")){ 
								accionweb.agregarObjeto("hayDatosBoleta", 1); 
							} else {							 
									accionweb.agregarObjeto("hayDatosBoleta", 2); 				
							} 
					
						List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
						moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
						listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
						List<Presw18DTO> listaTipoPago = new ArrayList<Presw18DTO>();
			     
				    	accionweb.agregarObjeto("identificador", identificador);
						accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);  
						accionweb.agregarObjeto("tipoBoleta", tipoBoleta);	
					 }
					}		  
			     }	
		    }
		    }			
			}	
		//INICIO	 //VS. 05-12-2016
		List<Presw25DTO> listaTipoGasto = new ArrayList<Presw25DTO>();
		listaTipoGasto = moduloHonorarioB.getListaTipoGasto();
		accionweb.agregarObjeto("listaTipoGasto", listaTipoGasto);
		//FIN	     
		
		accionweb.agregarObjeto("esBoletaB", 1);   
		accionweb.agregarObjeto("opcionMenu", 1);
		if(!mensaje.trim().equals(""))
		{			
			accionweb.agregarObjeto("mensaje", mensaje);
			cargarMenu(accionweb);
		}   
		else {
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
		}	
		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
			accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas")); 	
      	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
			accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
	}
	 synchronized public void setdatosBoleta(AccionWeb accionweb,List<Cocow36DTO>  listCocow36DTO) throws Exception {
		if(listCocow36DTO.size() > 0){
		 for (Cocow36DTO ss : listCocow36DTO){
			if(ss.getNomcam().trim().equals("FECDOC")){
				   String fecha = ss.getValnu1()+"";
				   String diaFecha = "";
	        	   String mesFecha = "";
	        	   String annoFecha = "";
	        	   if(fecha.length() == 8){
	        		annoFecha = fecha.substring(0,4);
	        	    mesFecha = fecha.substring(4,6);
	        	    diaFecha = fecha.substring(6);
	        	   } 
	        	   String fechaBoleta = diaFecha + "/" + mesFecha + "/" + annoFecha;
				
				accionweb.agregarObjeto("fechaBoleta", fechaBoleta);
			}
			if(ss.getNomcam().trim().equals("TOTBOL"))
				accionweb.agregarObjeto("totalBoleta", ss.getValnu1());
			if(ss.getNomcam().trim().equals("RETBOL"))
				accionweb.agregarObjeto("retencion", ss.getValnu1());
			if(ss.getNomcam().trim().equals("VALPAG"))
				accionweb.agregarObjeto("totalAPago", ss.getValnu1());
			if(ss.getNomcam().trim().equals("DESCR1"))
				accionweb.agregarObjeto("glosa1", ss.getValalf());
			if(ss.getNomcam().trim().equals("DESCR2"))
				accionweb.agregarObjeto("glosa2", ss.getValalf());
			if(ss.getNomcam().trim().equals("DESCR3"))
				accionweb.agregarObjeto("glosa3", ss.getValalf());
			if(ss.getNomcam().trim().equals("NOMPRE"))
				accionweb.agregarObjeto("nombrePrestador", ss.getValalf());
			if(ss.getNomcam().trim().equals("DOMPRE"))
				accionweb.agregarObjeto("domicilioPrestador", ss.getValalf());
			if(ss.getNomcam().trim().equals("CIUPRE"))
				accionweb.agregarObjeto("ciudadPrestador", ss.getValalf());		
		
		   }
		 accionweb.getSesion().setAttribute("listCocow36DTO", listCocow36DTO);
		}
	}
	public void verificaRut(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		int identificador = Util.validaParametro(accionweb.getParameter("identificador"), 0);
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		if(listaSolicitudes != null && listaSolicitudes.size() > 0)
			accionweb.getSesion().removeAttribute("listaSolicitudes");
		List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudesPago");
		if(listaSolicitudesPago != null && listaSolicitudesPago.size() > 0)
			accionweb.getSesion().removeAttribute("listaSolicitudesPago");
	
		String nombre = "";
		if(rutnum > 0){
			Collection<Presw18DTO> lista = moduloHonorarioB.getVerificaRut(String.valueOf(rutnum)+ dvrut);
			if(lista != null && lista.size() > 0){
				 for (Presw18DTO ss : lista){
					 nombre = ss.getDesuni();
				   }
			}
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
			
		}
	   	if(!nombre.trim().equals("")){
	    		accionweb.agregarObjeto("hayDatoslista", 1);
	    		accionweb.agregarObjeto("nomIdentificador", nombre);
	    } else
	    	accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado. Debe agregarlo.");
		
	 // MAA ya no se usa la sede
	  /* 	List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
	    listSede = moduloValePagoB.getListaSede(accionweb.getReq());
	    String comboSede = "<select name=\"sede\" id=\"sede\" class=\"estilo_Azul\" onChange=\"\" >"+
	                       "<option value=\"-1\" selected=\"selected\">Seleccione</option> ";
	 
	    for (Presw18DTO ss : listSede){
	    	comboSede += "<option value=" + ss.getNummes() + ">" + ss.getDesuni() + "</option>";
	    	
	    }
	    comboSede += "</select>"; */
	   
	    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
	    moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
	    List<Presw18DTO> listaTipoPago = new ArrayList<Presw18DTO>();
	     
  
	 // MAA ya no se usa la sede
	   // accionweb.agregarObjeto("comboSede", comboSede);
	   // accionweb.agregarObjeto("listSede", listSede);
		accionweb.agregarObjeto("identificador", identificador);		  		  
		accionweb.agregarObjeto("esHonorario", 1);   
		
	
		accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);  
    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
			accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
      	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
			accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
      	
	}
	public void cargaIngresoHonorario(AccionWeb accionweb) throws Exception {
		verificaRut(accionweb);
		if(accionweb.getReq().getAttribute("mensaje") == null || accionweb.getReq().getAttribute("mensaje").toString().trim().equals(""))
			cargaAutorizaHonorario(accionweb);
		return;
	}
	 synchronized public void cargaAutorizaHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
		
		if(accionweb.getSesion().getAttribute("procesoSesion") != null) //MAA.25.02.2018
		   accionweb.getSesion().removeAttribute("procesoSesion"); 
		
		PreswBean preswbean = null;
		Collection<Presw18DTO> listaHonorario = null;
		String titulo = "";
		String tituloDetalle1 = "";
		String tituloDetalle2 = "";
		String tipcue = "";
		
		//System.out.println("rutUsuario: "+rutUsuario);
		//System.out.println("dv: "+dv);
		
		switch (tipo) {
		case  2:// lista de boletas a honorario para modificar
			{preswbean = new PreswBean("LBZ",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"",""); //VS.25-11-2016. Modificacion BANNER -- LBM - LMZ
			titulo = "Modificaci�n";
			break;
		}
		case 3: case 6: case 8: case 9: case 10: // lista de boletas a H para autorizar
			{String nomtipo = "";
			if(tipo == 3 || tipo == 6){
				titulo = "Autorizaci�n Responsable de la Organizaci�n"; // VS.13.12.2016. cuenta -> organizacion
				nomtipo = "LBX"; // VS.25-11-2016. Modificacion BANNER -- LBA -LBX
				accionweb.agregarObjeto("opcion2", 6); // VS.16-12-2016
			}
			if(tipo == 8){
			   nomtipo = "LBW"; /*VS.25-11-2016. Modificacion BANNER, LBE - LBW */
			   titulo = "Autorizaci�n MECESUP VRA";
			   tipcue = "Y";
			   accionweb.agregarObjeto("opcion2", 7); // VS.16-12-2016
			}
			if(tipo == 9){
				   nomtipo = "LBW"; // VS.25-11-2016. Modificacion BANNER -- LBE -LBW
				   titulo = "Autorizaci�n UCP";
				   tipcue = "X";
				   accionweb.agregarObjeto("opcion2", 6); // VS.16-12-2016
				}
			if(tipo == 10){
				   nomtipo = "LBW"; // VS.25-11-2016. Modificacion BANNER -- LBE -LBW
				   titulo = "Autorizaci�n MECESUP DIPRES";
				   tipcue = "Z";
				}
			preswbean = new PreswBean(nomtipo,0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			preswbean.setTipcue(tipcue);
			
			tituloDetalle1 = "Autorizar";
			tituloDetalle2 = "Rechazar";
			if (tipo == 6)
				tituloDetalle1 = "Recepci�n"; //VS.13.12.2016. POR vale
			accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1); //VS.16.12.216
			accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
			//accionweb.agregarObjeto("opcion2", 6); VS.16.12.2016			
			break;
			}
		case 4: // lista de boletas a Honorarios para consulta
			{		
			String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
			int fechaInicio = Util.validaParametro(accionweb.getParameter("fechaInicio"),0);
			int fechaTermino = Util.validaParametro(accionweb.getParameter("fechaTermino"),0);
			String fechaIni = Util.validaParametro(accionweb.getParameter("fechaIni"),"");
			String fechaTer = Util.validaParametro(accionweb.getParameter("fechaTer"),"");
			//int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0); //VS.02-12-2016. Modificacion BANNER
			String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"");
			//int anno = Util.validaParametro(accionweb.getParameter("anno"),0); //VS.16.12.2016. Por copia ...jijij
			/*agrego filtro de a�o VS.16.12.2016. Por copia ...jijij*/
			//DateTool fechaActual = new DateTool(); //VS.16.12.2016. Por copia ...jijij
			if(fechaInicio > 0 && fechaTermino == 0 ){
				DateTool fechaActual = new DateTool();
				String dia = fechaActual.getDay()+"";
				if(dia.length() == 1)
					dia = "0" + dia;
				String mes = (fechaActual.getMonth()+1)+"";
				if(mes.length()== 1)
					mes = "0" + mes;
				fechaTermino = Integer.parseInt(String.valueOf(fechaActual.getYear()) + mes + dia);
				fechaTer = dia + "/" + mes + "/" + String.valueOf(fechaActual.getYear());
				
			}
			//if(unidad >= 0){ //VS.02-12-2016.modificacion BANNER
			if(!organizacion.equals("")){
			preswbean = new PreswBean("LBY",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");  // VS.25-11-2016. Modificacion BANNER -- LBC -LBY
			//preswbean.setCoduni(unidad); //VS.02-12-2016.modificacion BANNER
			preswbean.setCajero(organizacion.trim());			
			preswbean.setTipcue(estado);
			preswbean.setFecmov(fechaInicio);
			preswbean.setNumcom(fechaTermino);
			listaHonorario = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			}
			titulo = "Consulta";	
			List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
			moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
			listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
			accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
		   	accionweb.agregarObjeto("estado", estado);
		   	//accionweb.agregarObjeto("unidad", unidad); //VS.02-12-2016.Modificacion BANNER.
		   	accionweb.agregarObjeto("organizacion", organizacion);
		   	accionweb.agregarObjeto("fechaIni", fechaIni);
		   	accionweb.agregarObjeto("fechaTer", fechaTer);
			break;
			}
		case 5: case 7:// lista de boletas a Honorario para Recepci�n de vales en Finanzas
			{ //VS. 16-12-2016 copia.  MAA este proceso no se usa para BANNER antes LVP ahora LVX.
			//int sede = Util.validaParametro(accionweb.getParameter("sede"),0);	
			//String estadoVales = Util.validaParametro(accionweb.getParameter("estadoVales"),""); VS.16-12-2016
			//preswbean = new PreswBean("LBF",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"",""); VS.16-12-2016
			//preswbean.setSucur(sede);
			//preswbean.setTipcue(estadoVales); VS.16-12-2016
			//titulo = "Recepci�n Finanzas"; VS.16-12-2016
			//tituloDetalle1 = "Recepci�n"; VS.16-12-2016
			//tituloDetalle2 = "Rechazar"; VS.16-12-2016
			//accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2); VS.16-12-2016
			//accionweb.agregarObjeto("opcion2", 7); VS.16-12-2016
			// MAA ya no se usa la sede
		/*	List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
			listSede = moduloValePagoB.getListaSede(accionweb.getReq());
			accionweb.agregarObjeto("listSede", listSede);
			accionweb.agregarObjeto("sede", sede); */
			//accionweb.agregarObjeto("estadoVales", estadoVales); VS.16-12-2016
			break;
			}
		case 11: // lista de boletas a honorarios para consulta
		{		
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
		//int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0); VS.06-12-2016.
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"");
		//if(unidad >= 0){ VS.06-12-2016.
		preswbean = new PreswBean("LB1",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");  // VS.25-11-2016. Modificacion BANNER -- LBR -LB1
		//preswbean.setCoduni(unidad);
		preswbean.setTipcue(estado);
		listaHonorario = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		//}
		titulo = "Consulta";
		if(tipo == 11){
			titulo = "Existentes";
			int volver = Util.validaParametro(accionweb.getParameter("volver"),0);
			if(volver == 1) {
			int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"),0);
			String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
			String nomIdentificador = Util.validaParametro(accionweb.getParameter("nomIdentificador"),"");
			if(rutnum > 0 /*&& nomIdentificador.trim().equals("")*/)
				this.verificaRut(accionweb);
			accionweb.agregarObjeto("dvrut", dvrut);
			accionweb.agregarObjeto("rutnum", rutnum);
			accionweb.agregarObjeto("nomIdentificador", nomIdentificador);			
			}
		}
		List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
		moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
		listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
	   	accionweb.agregarObjeto("estado", estado);
	   	//accionweb.agregarObjeto("unidad", unidad); VS.06-12-2016
	   	accionweb.agregarObjeto("organizacion", organizacion);
		break;
		}	
		case  12:// lista de Ingresadores
		{//int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0);
		    String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"");
		    String accion = Util.validaParametro(accionweb.getParameter("accion"),""); 
		    if(!accion.trim().equals("")){
		    	int rutide = Util.validaParametro(accionweb.getParameter("rutide"), 0);
		    	String digide = Util.validaParametro(accionweb.getParameter("digide"),""); 
		    	//if(rutide > 0){ VS.12.12.2016
			    	try {
			    	boolean graba = moduloValePagoB.saveDeleteIngresador(rutide, digide,organizacion, accion);			        
			    	if(graba)
			    		accionweb.agregarObjeto("mensaje", "registro exitoso.");
			    	else
			    		accionweb.agregarObjeto("mensaje", "problemas al registrar.");
			    	Thread.sleep(500);
			    	} catch (Exception e) {
						accionweb.agregarObjeto("mensaje", "No guardado saveDeleteIngresador.");
						accionweb.agregarObjeto("exception", e);
						e.printStackTrace();
					}
		    	//} else accionweb.agregarObjeto("mensaje", "No guardado saveDeleteIngresador."); VS.12.12.2016
		    }
		
			List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
			moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
			listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
			accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
		   //	accionweb.agregarObjeto("unidad", unidad);
			accionweb.agregarObjeto("organizacion", organizacion);
			if(!organizacion.trim().equals("")){
		    	preswbean = new PreswBean("OL1",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
		    	preswbean.setCajero(organizacion);
		    	Collection<Presw25DTO> listaValePago25 = (Collection<Presw25DTO>) preswbean.consulta_presw25();
		    	accionweb.agregarObjeto("hayDatoslista", 1);
		    	accionweb.agregarObjeto("listaValePago25", listaValePago25);
		    	accionweb.agregarObjeto("hayDatoslista", "1");
			}
			
			titulo = "Ingresadores";
		break;
	     }
		case 16: case 17:// lista de sedeAutorizadas
		{   
			int sedeLista = Util.validaParametro(accionweb.getParameter("sedeLista"),0);
			List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
	        List<Sede> listaSede = new ArrayList<Sede>();
	        listaSede = moduloValePagoB.getListaSedeAutorizadas(rutUsuario);
	        if (tipo == 16) titulo = "Autorizaci�n DAF";
	        else titulo = "Autorizaci�n Sobregiro";
	        if (listaSede != null && listaSede.size() > 0) { 
	          accionweb.agregarObjeto("listaSede", listaSede);
	          accionweb.agregarObjeto("titulo", titulo);
	        }
	        
	        if (tipo == 16) {
		        if (sedeLista > 0){
		          accionweb.agregarObjeto("sedeLista", sedeLista);
		          preswbean = new PreswBean("LBG",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
		          preswbean.setSucur(sedeLista);
		          listaHonorario = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		        }
	        }
	        else {
	        	preswbean = new PreswBean("BSF",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
		        listaHonorario = (Collection<Presw18DTO>) preswbean.consulta_presw18();
	        }
	        
	        List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
			moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
			listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
			accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
	        
		 break;
		}
		}		
				
		if(tipo != 4 && tipo != 12 && tipo != 16 && tipo != 17)
			listaHonorario = (Collection<Presw18DTO>) preswbean.consulta_presw18();		
		
		//CODUNI, NUMMES, TIPPRO, PRESUM, USADOM, INDPRC, DIFERM, ACUMUM, INDHAY, TIPMOV, NOMTIP, ITEDOC, NUMDOC, FECDOC, NOMPRO, INDPRO, VALDOC, IDDIGI, RUTIDE, IDSOLI, PRESAC, USADAC, PREANO, DESUNI, DESITE
		if(listaHonorario != null && listaHonorario.size() > 0)
		{				
	    	accionweb.agregarObjeto("hayDatoslista", "1");
			for (Presw18DTO ss:listaHonorario)
			{	
			 //ss.setDesite(moduloHonorarioB.getVerificaRutNom(ss.getRutide()+""+ss.getIndpro()));			  
				//ss.setTipmov(moduloHonorarioB.getVerificaRutNom(ss.getRutide()+""+ss.getIndpro()));
				ss.setNomtip(moduloHonorarioB.getVerificaRutNom(ss.getRutide()+""+ss.getIndpro()));
			}
		}	    
        accionweb.agregarObjeto("listaHonorario", listaHonorario);
      	accionweb.agregarObjeto("esBoletaB", "1");
      	accionweb.agregarObjeto("opcion", String.valueOf(tipo));
    	accionweb.agregarObjeto("opcionMenu", tipo);
      	accionweb.agregarObjeto("titulo", titulo);
      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
      	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
			accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
      	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
			accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
      	
	}
	public void cargaResultadoSolicitudHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		//int cuentaPresup = Util.validaParametro(accionweb.getParameter("cuentaPresup"), 0);
		String cuentaPresup = Util.validaParametro(accionweb.getParameter("cuentaPresup"), "");
		long valorAPagar = Util.validaParametro(accionweb.getParameter("valorAPagar"), 0);
		long valor = Util.validaParametro(accionweb.getParameter("valor"), 0);
		//String cuentaTipoGasto = Util.validaParametro(accionweb.getParameter("cuentaTipoGasto"),""); MB modifica 24/03/2017
		String tipoGasto = Util.validaParametro(accionweb.getParameter("cuentaTipoGasto"),"");
		String cuentaTipoGasto = moduloHonorarioB.getCuentaTipoGasto(tipoGasto);
		
		long total = 0;
        														// codOrg.            codCuenta		
	    long saldoCuenta =  moduloValePagoB.getBuscaSaldo(cuentaPresup.trim(),cuentaTipoGasto.trim()); //VS.29-11-2016.Modificacion BANNER.saldo
	    moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    
	   // System.out.println("saldoCuenta: "+ saldoCuenta);
	   // System.out.println("cuentaTipoPago: "+ cuentaTipoPago);
	   // System.out.println("cuentaPresup: "+ cuentaPresup);
	    
	    int sobrepasa = 0;
	    sobrepasa = moduloHonorarioB.getAgregaListaSolicitaHonorario(accionweb.getReq(),saldoCuenta);
	    switch (sobrepasa){
	    case 1: {
	    	accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" >Este valor excede el saldo de la cuenta de gasto de la organizaci�n" + cuentaPresup + ", que es de: $ " + moduloHonorarioB.formateoNumeroSinDecimales(Double.parseDouble(saldoCuenta+""))+"</font> .");
	    	break;
	    }
	    case 2: {
	    	accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"destacado\" size=\"12px\">Lo sentimos, este valor no se puede agregar a la cuenta, ya que excede el valor del la Boleta, que es de: $ " + moduloHonorarioB.formateoNumeroSinDecimales(Double.parseDouble(valorAPagar+""))+"</font> .");
	    	break;
	    }
	    case 3: {
	    	accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"destacado\" size=\"12px\">Lo sentimos, esta cuenta ya se encuentra agregada </font> .");
	    	break;
	    }
	    }
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		if (listaSolicitudes != null && listaSolicitudes.size() > 0){
			accionweb.agregarObjeto("registra", 1);
		    accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes); 
		    for (Presw18DTO ss : listaSolicitudes)
				total = total + ss.getUsadom();
            accionweb.agregarObjeto("totalSolicitudHonorario", total);
		}
			
	   	accionweb.agregarObjeto("esBoletaB", 1);   
		accionweb.agregarObjeto("identificador", 1); 
		
	}
	 synchronized public void eliminaResultadoSolicitudHonorario(AccionWeb accionweb) throws Exception {
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
		   moduloHonorarioB.getEliminaListaSolicitaHonorario(accionweb.getReq());
		   	accionweb.agregarObjeto("mensaje", "Se elimin� el cargo a la cuenta.");
		    	
			List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		   	accionweb.agregarObjeto("esBoletaB", 1);   
			accionweb.agregarObjeto("identificador", 1);  
			long total = 0;
			if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				accionweb.agregarObjeto("registra", 1);
			    accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes); 
			    for (Presw18DTO ss : listaSolicitudes)
					total = total + ss.getUsadom();
	            accionweb.agregarObjeto("totalSolicitudHonorario", total);
			}
			if(tipo > 0)
			 accionweb.agregarObjeto("opcion", tipo);
			
		}
	 synchronized public void registraHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
	
		String nomIdentificador = Util.validaParametro(accionweb.getParameter("nomIdentificador"), "");
		long rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		long numeroBoleta = Util.validaParametro(accionweb.getParameter("numeroBoleta"),0);
		long totalBoleta = Util.validaParametro(accionweb.getParameter("totalBoleta"),0);
		long retencion = Util.validaParametro(accionweb.getParameter("retencion"), 0);
		long totalAPago = Util.validaParametro(accionweb.getParameter("totalAPago"), 0); 
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"),"");	
		String fechaBoleta = Util.validaParametro(accionweb.getParameter("fechaBoleta"),"");
		String glosa1 = Util.validaParametro(accionweb.getParameter("glosa1"),"");	
		String glosa2 = Util.validaParametro(accionweb.getParameter("glosa2"),"");
		String glosa3 = Util.validaParametro(accionweb.getParameter("glosa3"),"");
	    String tipoPago = "";
	    String nombrePago = "";		
		//VS.28.11.2016.Modificacion BANNER.la sede no se trabaja
		//long sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
		String tipoGasto = Util.validaParametro(accionweb.getParameter("tipoGasto"),"");//VS.05.12.2016.modificacion banner
					
	    int numHonorario = 0;
	    
	    numHonorario = moduloHonorarioB.getRegistraHonorario(accionweb.getReq(),rutUsuario,dv);
	    if(numHonorario > 0){
	   	    accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa la Boleta de Honorario, el n�mero es: "+ numHonorario+", correspondiente al RUT: " + moduloHonorarioB.formateoNumeroSinDecimales(Double.parseDouble(rutnum+"")) + "-" + dvrut +".");
	   	    if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				accionweb.agregarObjeto("registra", 1);
			    accionweb.getSesion().removeAttribute("listaSolicitudes");
				}
	   	 cargarMenu(accionweb);
	    } else
	    	accionweb.agregarObjeto("mensaje", "No se registr� la Boleta de Honorario.");
		  	accionweb.agregarObjeto("esBoletaB", 1);   
	 
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
				accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
	      	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
				accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
	      	
	}
	public void consultaHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		//int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0); VS.01-12-2016.por organizacion
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"");
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
		String proceso = Util.validaParametro(accionweb.getParameter("proceso")+ "",""); //VS.15.12.2016 .nuevo
		String procesoSesion = Util.validaParametro(accionweb.getSesion().getAttribute("procesoSesion")+ "","");  //VS.19.12.2016 .nuevo
		String tipdoc = Util.validaParametro(accionweb.getParameter("tipoBoleta"),"");
		int sedeLista = Util.validaParametro(accionweb.getParameter("sedeLista"), 0);
		
		//System.out.println("PROCESO" + proceso);
		//VS.23-11-2016.Modificacion BANNER
		//long sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
		//VS.05.12.2016.modificacion banner
		
		String tipoGasto = "";
		//proceso = "DB1";
				
		String estadoVales = Util.validaParametro(accionweb.getParameter("estadoVales"),"");
		
		String nomIdentificador = "";
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		long rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		//System.out.println("numDoc: "+numDoc);
		//VS.23-11-2016.Modificacion BANNER
		//if(sede > 0)
		//	accionweb.agregarObjeto("sedeHonorario", sede);
		//List<Presw18DTO> listaTipoPago = new ArrayList<Presw18DTO>(); //VS.20.12.2016 comentado
	    //listaTipoPago = moduloHonorarioB.getListaTopoPago(accionweb.getReq()); //VS.20.12.2016
		
		if(!estadoVales.trim().equals(""))
			accionweb.agregarObjeto("estadoVales", estadoVales);
				
		Presw19DTO preswbean19DTO = new Presw19DTO();
		String titulo = "";
		String tituloDetalle1 = "";		
		
			
		//preswbean19DTO.setTippro("DB1"); //VS.25-11-2016.Modificacion BANNER - DBH - DB1
		//preswbean19DTO.setTippro((proceso.equals("")?"DB1":proceso)); // VS.15.12.2016.
		preswbean19DTO.setTippro((!procesoSesion.equals("")?procesoSesion:(proceso.equals("")?"DB1":proceso)));// VS.19.12.2016
		//if(procesoSesion.equals("") && proceso.equals("") && tipo == 4)	proceso = "DB1";
		
			
		if(procesoSesion.equals("DB1") || proceso.equals("DB1")) {			 
			preswbean19DTO.setRutide(new BigDecimal(rutnum));
			preswbean19DTO.setDigide(dvrut);
			preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
			preswbean19DTO.setTipdoc(tipdoc.trim()); 
		} else {
			preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
			preswbean19DTO.setDigide(dv); 
			preswbean19DTO.setNumcom(new BigDecimal(rutnum));
			preswbean19DTO.setTipcue(dvrut); 
			preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
			preswbean19DTO.setTipdoc(tipdoc.trim());  // tipoBoleta			
		}	

	//	preswbean19DTO.setIdsoli("9999999");
	
		String readonly = "readonly";
		
/*capturar los datos */		
		if(tipo == 2) // lista de BH para consultar
		{	titulo = "Modifica ";
			tituloDetalle1 = "Modificar";
			if(tipdoc.trim().equals("BOH"))
				readonly = "";
			}
		if(tipo == 3) // lista de BH para autorizar
			{
			titulo = "Autorizaci�n Responsable de la Organizaci�n"; //VS.14.12.2016 Cuenta - Organizacion
			tituloDetalle1 = "Autorizar";		 
			}
		if(tipo == 4) // lista de BH para consulta
			{
			int origenConsulta = Util.validaParametro(accionweb.getParameter("origenConsulta"), 0); // 1 viene de la consulta de boletas
			if(origenConsulta > 0)
				accionweb.agregarObjeto("origenConsulta", origenConsulta);
			titulo = "Consulta ";
			tituloDetalle1 = "Consultar";
			}
		if(tipo == 5) // lista de BH para Recepci�n de vales en Finanzas
			{
			titulo = "Recepci�n ";
			tituloDetalle1 = "Recepci�n";
			}
		if(tipo == 8) // lista de BH para autorizar
		{
		titulo = "Autorizaci�n MECESUP VRA";
		tituloDetalle1 = "Autorizar";		 
		}
		if(tipo == 9) // lista de BH para autorizar
		{
		titulo = "Autorizaci�n UCP";
		tituloDetalle1 = "Autorizar";		 
		}
		if(tipo == 10) // lista de BH para autorizar
		{
		titulo = "Autorizaci�n MECESUP DIPRES";
		tituloDetalle1 = "Autorizar";		 
		}
		if(tipo == 11) // lista de BH para copiar
		{	titulo = "Ingreso";
			tituloDetalle1 = "Ingreso";
			readonly = "";
			nomIdentificador = Util.validaParametro(accionweb.getParameter("nomIdentificador"),"");
			}
		if(tipo == 16) // lista de vales para autorizar
		{
		titulo = "Autorizaci�n DAF";
		accionweb.agregarObjeto("sedeLista", sedeLista);
		}
		if(tipo == 17) // lista de vales para autorizar
		{
		titulo = "Autorizaci�n Sobregiro";
		}
		
		List<Presw18DTO>  listaSolicitudesPago = new ArrayList<Presw18DTO>();		
		List<Presw18DTO>  listaSolicitudes = new ArrayList<Presw18DTO>();
		List<Presw18DTO>  listaHistorial = new ArrayList<Presw18DTO>();
		long valorAPagar = 0;
		//VS.23-11-2016.Modificacion BANNER
		//sede = 0;
		int identificador = 0; 
		
	    String nomcam = "";
	    long valnu1 = 0;
	    long valnu2 = 0;
	    String valalf = "";
	    String glosa1 = "";
	    String glosa2 = "";
	    String glosa3 = "";
	    String tipoPago = "";      // VS.15.12.2016
	    String nombrePago = "";	   // VS.15.12.2016 
	    String nomPrestador = "";
	    String estadoBoleta = "";
	    String codigoEstado = "";
	    String tipoBoleta = "";
	    int ind = 0;
	    long totalSolicitudHonorario = 0;
	    long fechaBoleta = 0;
	    long totalBoleta = 0;
	    long retencion = 0;
	    long totalAPago = 0;	    
	    String muestraSaldo = ""; // VS.15.12.2016
	    long saldoCuenta  = 0; // VS.15.12.2016
	    int permiteAut    = 1; // VS.15.12.2016	    
		List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
		CocowBean cocowBean = new CocowBean();
		//System.out.println("preswbean19DTO : "+ preswbean19DTO.getRutide()+"  - "+preswbean19DTO.getNumdoc()+"--- "+preswbean19DTO.getTippro()+"  --- "+preswbean19DTO.getFecmov());
		listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
		//System.out.println("listCocow36DTO : "+ listCocow36DTO.size() );
		//System.out.println("preswbean19DTO : "+ preswbean19DTO.getRutide()+"  - "+preswbean19DTO.getNumdoc()+"--- "+preswbean19DTO.getTippro()+"  --- "+preswbean19DTO.getFecmov());
//		System.out.println("preswbean19DTO : "+ listCocow36DTO.getRutide()+"  - "+listCocow36DTO.getNumdoc()+"--- "+listCocow36DTO.getTippro()+"  --- "+listCocow36DTO.getFecmov());
	    if(listCocow36DTO != null && listCocow36DTO.size() >0 ){
	    
	    for(Cocow36DTO ss: listCocow36DTO){
	    	if(ss.getNomcam().trim().equals("DESIDE"))
	    		nomIdentificador = ss.getValalf();
	    	//VS.23-11-2016.Modificacion BANNER
	    	//if(ss.getNomcam().trim().equals("CODSUC"))
	    	//	sede = ss.getValnu1();	
	    	if(ss.getNomcam().trim().equals("FECDOC"))
	    		fechaBoleta = ss.getValnu1();
	    /*	if(ss.getNomcam().trim().equals("TIPDOC"))
	    		tipoBoleta = ss.getValalf();*/
	      	if(ss.getNomcam().trim().equals("TOTBOL"))
	    		totalBoleta = ss.getValnu1();
	      	if(ss.getNomcam().trim().equals("RETBOL"))
	    		retencion = ss.getValnu1();
	      	if(ss.getNomcam().trim().equals("VALPAG"))
	    		totalAPago = ss.getValnu1();
	    	if(ss.getNomcam().trim().equals("DESCR1"))
	    		glosa1 = ss.getValalf();
	    	if(ss.getNomcam().trim().equals("DESCR2"))
	    		glosa2 = ss.getValalf();
	     	if(ss.getNomcam().trim().equals("DESCR3"))
	    		glosa3 = ss.getValalf();	     
	     	 if(ss.getNomcam().trim().equals("ESTADO")){
           	   estadoBoleta = ss.getValalf();
           	   codigoEstado = ss.getResval();           	   
	     	 }
		     if(ss.getNomcam().trim().equals("TIPGAS")) //VS.05.12.2016.tipo Gasto
		    		tipoGasto = ss.getValalf().trim();		     	 
	          if(ss.getNomcam().trim().equals("HISTORIAL")){
	            	Presw18DTO presw18DTO = new Presw18DTO();
	            	presw18DTO.setUsadom(ss.getValnu1()); // es la fecha, 
	            	presw18DTO.setUsadac(ss.getValnu2());//en caso de Resval="Autoriza" es la unidad
	            	presw18DTO.setDesite(ss.getValalf()); // es el responsable
	               	presw18DTO.setDesuni(ss.getResval().substring(0,15));
	            	presw18DTO.setNompro(ss.getResval().substring(15)); // es la accion, INGRESO, MODIFICACION, AUTORIZACION, PAGO, AUTORIZA FINANZAS
	            	listaHistorial.add(presw18DTO);
	            }
            if(ss.getNomcam().trim().equals("RUTIDE") && tipo != 11)
             	   rutnum = ss.getValnu1();
            if(ss.getNomcam().trim().equals("DIGIDE") && tipo != 11)
            	dvrut = ss.getValalf();
            if(ss.getNomcam().trim().equals("TIPVAL")){
            	tipoPago = ss.getValalf();
              nombrePago = ss.getResval();
            }                      
            if(ss.getNomcam().trim().equals("CUENTA")){
            	Presw18DTO presw18DTO = new Presw18DTO();
            	//presw18DTO.setCoduni(Integer.parseInt(ss.getValnu1()+"")); //VS.25-11-2016.Modifica BANNER
            	presw18DTO.setNompro(ss.getResval().trim().substring(0,15));
            	presw18DTO.setUsadom(ss.getValnu2()); //lista.usadom
               	presw18DTO.setDesuni(ss.getValalf());  //  $lista.desuni
               	presw18DTO.setDesite(ss.getResval().trim().substring(15)); // $lista.desite //VS.25-11-2016.Modifica BANNER
               	//VS.15.12.2016. saldo. verificaci�n
               	//presw18DTO.setTipmov(tipoPago);
               	//saldoCuenta = moduloValePagoB.getBuscaSaldo(ss.getResval().trim().substring(0,15),tipoPago.trim()); VS.20.12.2016

               //for (Presw18DTO ltp : listaTipoPago) {
			   //		if (tipoPago.trim().equals(ltp.getTipmov().trim())) {
               String cuentaTipoGasto = moduloHonorarioB.getCuentaTipoGasto(tipoGasto);
				  saldoCuenta = moduloValePagoB.getBuscaSaldo(ss.getResval().trim().substring(0,15).trim(),cuentaTipoGasto);
				//	    break;                                       // codOrg.                              codCuenta que se dejo en Nomtip
				//	}    
		       //}               	
            	//System.out.println(ss.getValnu2() +"-" +saldoCuenta + "-" + (ss.getValnu2() > saldoCuenta));                
               	if(ss.getValnu2() > saldoCuenta){
       				 muestraSaldo = "<font class=\"Estilo_Rojo\" >" + saldoCuenta + "</font>";
       			     permiteAut = 2;
       		    }
               	else muestraSaldo = saldoCuenta+"";
               	
               //System.out.println("nmuestraSaldo " + muestraSaldo);
               	presw18DTO.setNomtip(muestraSaldo); 
               	//               	
               	listaSolicitudes.add(presw18DTO);
            	totalSolicitudHonorario += ss.getValnu2();
            }
	    //	 }
	    }	
	    } else {
	    	if(tipo == 4)
	    		accionweb.agregarObjeto("mensaje", "Boleta de Honorario no existe.");
	    }
	   
	    // MAA Sede no se usa BANNER
	    /*String selected = (sede == 0)?"selected=\"selected\"":"";
	    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
	    listSede = moduloValePagoB.getListaSede(accionweb.getReq());*/	    
	    //VS.05.12.2016. tipoGasto	 
	    //System.out.println("tipoGasto: " + tipoGasto);
 	    List<Presw25DTO> listaTipoGasto = new ArrayList<Presw25DTO>();
 	    listaTipoGasto = moduloHonorarioB.getListaTipoGasto();  	    
		//FIN
 	    
 	    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
 	    moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
	    //System.out.println("listaUnidades: "+listaUnidades);
	    if(listaSolicitudes != null && listaSolicitudes.size() > 0){
	    	accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes);
	    	accionweb.getSesion().setAttribute("listaSolicitudes", listaSolicitudes);
	    }
	    //VS.15.12.2016...
	    if(listaSolicitudesPago != null && listaSolicitudesPago.size() > 0){
	    	accionweb.agregarObjeto("listaSolicitudesPago", listaSolicitudesPago);
	       	accionweb.getSesion().setAttribute("listaSolicitudesPago", listaSolicitudesPago);
	    }	    
	    if(listaHistorial != null && listaHistorial.size() > 0){
	    	accionweb.agregarObjeto("listaHistorial", listaHistorial);
	       	accionweb.getSesion().setAttribute("listaHistorial", listaHistorial);
	    }
	    //fin
	    //  accionweb.agregarObjeto("listSede", listSede);   
	    accionweb.agregarObjeto("listaTipoGasto", listaTipoGasto); //VS.05.12.2016
	    accionweb.agregarObjeto("tipoGasto", tipoGasto); //VS.05.12.2016.
	    accionweb.getSesion().setAttribute("tipoGasto", tipoGasto);
	    accionweb.agregarObjeto("rutnum",rutnum);
	    accionweb.agregarObjeto("dvrut",dvrut);
	    accionweb.agregarObjeto("organizacion",organizacion);
	    //accionweb.agregarObjeto("unidad",unidad); VS.01-12-2016.organizacion	
	    
	    accionweb.agregarObjeto("numVale",String.valueOf(numDoc));
	    if(rutnum == 0)
	    	 accionweb.agregarObjeto("identificador","1");
	    else
	    	accionweb.agregarObjeto("identificador","2");
	    accionweb.agregarObjeto("totalSolicitudHonorario", totalSolicitudHonorario);
	    accionweb.agregarObjeto("cuentasPresupuestarias",listaUnidades);
	    //accionweb.agregarObjeto("nomIdentificador",nomIdentificador);
	    accionweb.agregarObjeto("nomIdentificador",moduloHonorarioB.getVerificaRutNom(rutnum+""+dvrut)); //VS.02-12-2016.modificacion BANNER
	    //accionweb.agregarObjeto("sede", sede);
	    if(saldoCuenta == 0) permiteAut = 2;
	    accionweb.agregarObjeto("permiteAut",permiteAut);
	    accionweb.agregarObjeto("valorAPagar", valorAPagar);
		accionweb.agregarObjeto("hayDatoslista", "1");     
      	accionweb.agregarObjeto("esBoletaB", "1");
      	accionweb.agregarObjeto("opcion", tipo);
      	accionweb.agregarObjeto("titulo", titulo);
      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
    	accionweb.agregarObjeto("glosa1",glosa1);
    	accionweb.agregarObjeto("glosa2",glosa2);
    	accionweb.agregarObjeto("glosa3",glosa3);
      	accionweb.agregarObjeto("read",readonly);
    	accionweb.agregarObjeto("nomPrestador", nomPrestador);
      	accionweb.agregarObjeto("fechaBoleta", fechaBoleta);
      	accionweb.agregarObjeto("totalBoleta", totalBoleta);
      	accionweb.agregarObjeto("retencion", retencion);
      	accionweb.agregarObjeto("totalAPago", totalAPago);
      	accionweb.agregarObjeto("estadoBoleta", estadoBoleta);
      	accionweb.agregarObjeto("codigoEstado", codigoEstado);
    	accionweb.agregarObjeto("tipoBoleta", tipdoc);
    	accionweb.agregarObjeto("numeroBoleta", numDoc);
      	accionweb.agregarObjeto("numDoc", numDoc);
    	accionweb.agregarObjeto("estado", estadoBoleta);
    	accionweb.agregarObjeto("proceso",proceso); // VS.19.12.2016. nuevo
       	
    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
			accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
      	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
			accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
      	
      	//System.out.println("termina consulta ");	
	}
	 synchronized public void ejecutaAccion(AccionWeb accionweb) throws Exception {
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		String estadoVales = Util.validaParametro(accionweb.getParameter("estadoVales"),"");
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
		String proceso = Util.validaParametro(accionweb.getParameter("proceso"),"");	
		int sedeLista = Util.validaParametro(accionweb.getParameter("sedeLista"), 0);
		
		switch (tipo){
		case 2: {
			this.actualizaHonorario(accionweb);
			break;
		}
		case 3: case 8: case 9: case 10: {
			this.autorizaHonorario(accionweb);
			break;
		}
		case 5:{
			this.recepcionHonorario(accionweb);
		break;
			
		}
		case 6:{// rechaza autorizaci�n
			this.rechazaAutHonorario(accionweb);
		break;
			
		}
		case 7:{// rechaza Recepci�n
			this.rechazaRecepHonorario(accionweb);
		break;
			
		}
		case 11:{// registra vale
			this.registraHonorario(accionweb);
		break;
			
		}
		case 16: {
			this.autorizaDAFHonorario(accionweb);
			break;
		}
		case 17: {
			this.autorizaSobregiroHonorario(accionweb);
			break;
		}
		}
	//	if(!estadoVales.trim().equals(""))
	//		accionweb.agregarObjeto("estadoVales", estadoVales);
		if(!estado.trim().equals(""))
			accionweb.agregarObjeto("estado", estado);
		if(tipo != 11)
			consultaHonorario(accionweb);
		
	}
	 synchronized public void actualizaHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numeroBoleta"), 0);
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
		String tipdoc = Util.validaParametro(accionweb.getParameter("tipoBoleta"),"");
		//long sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
		String tipoGasto = Util.validaParametro(accionweb.getParameter("tipoGasto"), "");
		
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");

	    boolean registra = false;
	    
	    registra = moduloHonorarioB.getActualizaHonorario(accionweb.getReq(),rutUsuario,dv,numdoc, tipdoc, rutnum, dvrut);
	    if(registra){
	   	    accionweb.agregarObjeto("mensaje", "Se actualiz� en forma exitosa la Boleta de Honorario, n�mero " +numdoc + " correspondiente al RUT: "+moduloHonorarioB.formateoNumeroSinDecimales(Double.parseDouble(rutnum+"")) + "-"+dvrut +".");
	   	    if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				accionweb.agregarObjeto("registra", 1);
			    accionweb.getSesion().removeAttribute("listaSolicitudes");
			    
			}
	   	// cargarMenu(accionweb);
	   	 consultaHonorario(accionweb);
	    } else
	    	accionweb.agregarObjeto("mensaje", "No se modific� la Boleta de Honorario.");
		  	accionweb.agregarObjeto("esBoletaB", 1);   
	 
		 
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
				accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
	      	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
				accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
	      	
	}
	 synchronized public void autorizaHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		//int numeroBoleta = Util.validaParametro(accionweb.getParameter("numeroBoleta"),0);
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"),"");	
		//String cuentaTipoGasto = Util.validaParametro(accionweb.getParameter("tipoGasto"),""); //VS.20.12.2016
		String tipoGasto = Util.validaParametro(accionweb.getParameter("tipoGasto"),""); //VS.20.12.2016
		
		tipoGasto = Util.validaParametro(accionweb.getParameter("tipoGasto"),"");
		String cuentaTipoGasto = ""; //MB agrega cuenta tipo gasto
		cuentaTipoGasto = moduloHonorarioB.getCuentaTipoGasto(tipoGasto);
		//int rutPrestador = rutnum;
		//String dvPrestador = dvrut;
		
		String proceso = Util.validaParametro(accionweb.getParameter("proceso")+ "","");
		
		String nomTipo = "";
		String tipCue = "";
		String nomAut = "";
		switch(tipo){
		 case 3: {
				nomTipo = "AC1"; /*VS.25-11-2016. Modificacion BANNER, ACB - AC1 */
				nomAut = " de Responsable de Organizaci�n. "; // VS.14.12.2016. Cuenta -> Organizaci�n
				break;
			}
			case 8: {
				nomTipo = "ABX"; /*VS.25-11-2016. Modificacion BANNER, ABE - ABX */
				tipCue = "Y";
				nomAut = " de Proyectos. ";
				break;
			}
			case 9: {
				nomTipo = "ABX"; /*VS.25-11-2016. Modificacion BANNER, ABE - ABX */
				tipCue = "X";
				nomAut = " de UCP. ";
				break;
			}
			case 10: {
				nomTipo = "TFB";
				tipCue = "Z";
				nomAut = " de Director de presupuestos. ";
				break;
			}
			}
		
		// Nuevo... VS.10.01.2017
		if(cuentaTipoGasto.equals(""))
			cuentaTipoGasto = Util.validaParametro(accionweb.getParameter("cuentaTipoGasto"),"");
		if(tipo == 3 && cuentaTipoGasto.equals("") && proceso.equals("DB2"))
		{
			accionweb.getSesion().setAttribute("procesoSesion", "DB1"); 
		    consultaHonorario(accionweb);
		    tipoGasto = Util.validaParametro(accionweb.getSesion().getAttribute("tipoGasto")+ "");
			if(cuentaTipoGasto.equals(""))
				cuentaTipoGasto = moduloHonorarioB.getCuentaTipoGasto(tipoGasto);	
				
			accionweb.getSesion().setAttribute("procesoSesion", "DB2");
		}
	    // INICIO NUEVO... VS.14-12-2016 - 16-12-2016		
		
		//accionweb.getMav().addObject("proceso", "DB2"); // VS.15.12.2016.proceso
		consultaHonorario(accionweb);
		//List<Presw18DTO> listaTipoPago = moduloHonorarioB.getListaTopoPago(accionweb.getReq()); VS.20.1.2016
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		
		if (tipo == 3) {	
		boolean sobrepasaOrg = this.revisaSaldoOrganizacion(listaSolicitudes,cuentaTipoGasto); // Se revisan solo los saldos de los autorizadores de sus org. // VS.16.12.2016
			// VS.16.12.2016 MAA para las especilaes no se debe verificar saldo tipo 8,9,10
			// si no estan sobrepasados los saldos 
		if (!sobrepasaOrg){ //entra bien... 1
		  try {	
			boolean aut = moduloHonorarioB.saveRecepciona(numdoc, rutUsuario,dv,"AC1",tipoBoleta, rutnum, dvrut);
			Thread.sleep(2000);
			if (aut) {
				//accionweb.getMav().addObject("proceso", "DB1");
				accionweb.getSesion().setAttribute("procesoSesion", "DB1"); //VS.19.12.2016 db1
				consultaHonorario(accionweb);
				if(accionweb.getSesion().getAttribute("procesoSesion") != null) //VS.19.12.2016
					accionweb.getSesion().removeAttribute("procesoSesion"); //VS.19.12.2016					
			
				listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
				aut = revisaAutOrganizacion(listaSolicitudes,numdoc,rutUsuario,dv,false, tipoBoleta, rutnum, dvrut); // se revisan si todas las org del vale estan autorizadas
				Thread.sleep(2000);
				if (aut ) {
					sobrepasaOrg = this.revisaSaldoOrganizacion(listaSolicitudes,cuentaTipoGasto); // Se revisan los saldos todas las org del vale.
					if (sobrepasaOrg) accionweb.agregarObjeto("mensaje", "No se puede Autorizar la Boleta de Honorario por Organizaciones, sin saldo suficiente");
					else accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n de la Boleta de Honorario N� " + numdoc +".");
				}	
				else accionweb.agregarObjeto("mensaje", "Hay organizaciones de la Boleta de Honorario sin autorizar.");
			}
			else accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n" + nomAut+".");
		  } catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "Error al autorizar la Boleta de Honorario.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
		 }	
	  }
	else accionweb.agregarObjeto("mensaje", "Organizaciones sin Saldo suficiente.");
	}
	else {
	  	 boolean aut = moduloHonorarioB.saveRecepciona(numdoc, rutUsuario,dv,nomTipo,tipoBoleta, rutnum, dvrut);
		 Thread.sleep(1500);
		 if (aut) accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n de la Boleta de Honorario N� " + numdoc);
		 else accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n" + nomAut);
	  }	
	  
	// MAA 28/12/2017 ya no es necesario este proceso por que el proceso que autoriza las cunetas autoriza el documento si estan todas las organizaciones autorizadas	
	/*boolean estadoHonorario = false;
	if (listaSolicitudes != null && listaSolicitudes.size() > 0){
		if (sobrepasa == 0) {
			estadoHonorario = moduloHonorarioB.saveEstadoHonorario(rutUsuario,dv,numdoc, tipoBoleta, rutnum, dvrut); //
			Thread.sleep(2000);
		}
	}
	*/
		
       // FIN		
	  	Thread.sleep(1500);
	    cargaAutorizaHonorario(accionweb);
	    
	    if(accionweb.getSesion().getAttribute("tipoGasto") != null) 
			accionweb.getSesion().removeAttribute("tipoGasto");
	    
		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
			accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
      	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
			accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
      	
	}
	 
	 synchronized public void autorizaDAFHonorario(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
			int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
			//int numeroBoleta = Util.validaParametro(accionweb.getParameter("numeroBoleta"),0);
			String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"),"");	
			//String cuentaTipoGasto = Util.validaParametro(accionweb.getParameter("tipoGasto"),""); //VS.20.12.2016
			String tipoGasto = Util.validaParametro(accionweb.getParameter("tipoGasto"),""); //VS.20.12.2016
			
			tipoGasto = Util.validaParametro(accionweb.getParameter("tipoGasto"),"");
			String cuentaTipoGasto = ""; //MB agrega cuenta tipo gasto
			cuentaTipoGasto = moduloHonorarioB.getCuentaTipoGasto(tipoGasto);
			//int rutPrestador = rutnum;
			//String dvPrestador = dvrut;
			
			String proceso = Util.validaParametro(accionweb.getParameter("proceso")+ "","");
			int sobrepasa = 0;
			boolean estadoHonorario = false;
			
			// Nuevo... VS.10.01.2017
			if(cuentaTipoGasto.equals(""))
				cuentaTipoGasto = Util.validaParametro(accionweb.getParameter("cuentaTipoGasto"),"");
			
			accionweb.getSesion().setAttribute("procesoSesion", "DB1"); 
			consultaHonorario(accionweb);
			if(accionweb.getSesion().getAttribute("procesoSesion") != null)
				  accionweb.getSesion().removeAttribute("procesoSesion");
			
			tipoGasto = Util.validaParametro(accionweb.getSesion().getAttribute("tipoGasto")+ "");
			if(cuentaTipoGasto.equals(""))
			   cuentaTipoGasto = moduloHonorarioB.getCuentaTipoGasto(tipoGasto);	
			
		    List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
			boolean sobrepasaOrg = this.revisaSaldoOrganizacion(listaSolicitudes,cuentaTipoGasto);
			boolean aut = false;
			if (!sobrepasaOrg){ //entra bien... 1
				aut = revisaAutOrganizacion(listaSolicitudes,numdoc,rutUsuario,dv,false, tipoBoleta, rutnum, dvrut); // se autorizan las org del autorizador
				Thread.sleep(2000);
				sobrepasa = 0;
				if (!aut) {
					 try {
					 	aut = actualizaConFinOrganizacion(listaSolicitudes,cuentaTipoGasto,numdoc,rutUsuario,dv, tipoBoleta,rutnum, dvrut); //se dejan en estado A las org de la Boleta  que estan con saldo
						Thread.sleep(1500);
						listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
						if (!aut) accionweb.agregarObjeto("mensaje", "Problemas al modificar estado de la Organizaci�n a Autorizada.");
					
					} catch (Exception e) {
						accionweb.agregarObjeto("mensaje", "Error al actualizar estado a Autorizada autorizaDAFHonorario.");
						accionweb.agregarObjeto("exception", e);
						e.printStackTrace();
					}	
				  }
				}
			else {
				aut = revisaAutOrganizacion(listaSolicitudes,numdoc,rutUsuario,dv,false, tipoBoleta, rutnum, dvrut); // se autorizan las org del autorizador 
				Thread.sleep(1500);
			  	sobrepasa = 1;
			  	if (aut) {
				   try {
						aut = actualizaSinFinOrganizacion(listaSolicitudes,cuentaTipoGasto,numdoc,rutUsuario,dv, tipoBoleta,rutnum, dvrut); //se dejan en estado B las org de la boleta que estan sin saldo
						Thread.sleep(2000);
						listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
						if (!aut) accionweb.agregarObjeto("mensaje", "Problemas al modificar estado de la Organizaci�n Sin Financiamiento.");
				   } catch (Exception e) {
						accionweb.agregarObjeto("mensaje", "Error al actualizar estado sin Financiamiento autorizaDAFHonorario.");
						accionweb.agregarObjeto("exception", e);
						e.printStackTrace();
				 }	
			}
		 }			
		String valor_vale = moduloHonorarioB.getValorDeBHE(numdoc, rutnum, dvrut, tipoBoleta);
		
		String rutDvIngresador = moduloHonorarioB.getRutOperacion(numdoc,rutnum, dvrut, tipoBoleta,"I");
   	    String rutDvDAF = rutUsuario + dv;
	   	
   	    boolean existeGoremal =  moduloValePagoB.getExisteGoremal(rutDvIngresador);
		if (existeGoremal) 
		    existeGoremal = moduloValePagoB.getExisteGoremal(rutDvDAF);
		 
		if (existeGoremal) {
	   	    boolean graba = moduloHonorarioB.saveRecepciona(numdoc, rutUsuario,dv,"TFG",tipoBoleta, rutnum, dvrut);
			if(graba) {
			  if (sobrepasa == 0) 	
				  accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n de la Boleta de Honorario N� " + numdoc+".");
			  else accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n de la Boleta de Honorario N� " + numdoc+", pero no se traspasa a BANNER por Organizaci�n sin saldo.");
				 estadoHonorario = true;
			 }
			 else {
			    accionweb.agregarObjeto("mensaje","No se grab� la autorizaci�n de la Boleta de Honorario N� " + numdoc+".");
			    estadoHonorario = false;
			}
		}
		
		try {
				
				if (estadoHonorario && sobrepasa == 0 && existeGoremal) {	
				         //String rutdv,String numvale, String fecha_invoice, String fecha_pmt_due, String fecha_trans, String cod_doct, String datos		   
					
			    		 String fecha = moduloHonorarioB.getDatoCOHOF102B(numdoc, rutnum, dvrut, tipoBoleta);
			    		 String rutDvAutorizador = moduloHonorarioB.getRutOperacion(numdoc,rutnum, dvrut, tipoBoleta,"A"); // el procedimeinto no lo usa
			    					    		
			    		 Vector vec_datos = new Vector();
			    		 vec_datos = moduloHonorarioB.getDatosAS(numdoc, rutnum, dvrut, tipoBoleta);
			    		 String datos_parametro = "";
			    		 Vector vec = null;
			    		 String comen_vale = "";
			    		 String tip_val   = "";
			    		 String desc_val  = "";
			    		 int rutHon      = 0;
			    		 String dvHon    = "";
			    		 String rutProd  = "";
			    		 String codOrg     = "";
			    		 
			    		 for (int i = 0; i < vec_datos.size(); i++) {
			    			 vec = (Vector) vec_datos.get(i);
			    			 comen_vale =  vec.get(6)+"";
		                     tip_val    = vec.get(7)+""; //VS.20.12.2016
		                     desc_val   = vec.get(8)+"";
		                     rutHon    = Integer.parseInt(vec.get(9)+"");
		                     dvHon     = vec.get(10)+"";
		                     rutProd    = rutHon + dvHon;
		                     codOrg     = vec.get(1)+"";
		                     
			    			 for (int y = 0; y < vec.size(); y++) { // para que rescate hasta valval
						     if (y == 11) datos_parametro = datos_parametro + vec.get(y) + "&" + moduloHonorarioB.getBancoOperacion(codOrg) + "@"; // Cunado llegue a VALVAL agrgue @			    				else if(y <= 4)datos_parametro = datos_parametro + vec.get(y) + "&";
							 else if(y <= 4)datos_parametro = datos_parametro + vec.get(y) + "&";	 }
			    		 }
			    		//System.out.println("datos_parametro " + datos_parametro); 
			    		//int error = moduloHonorarioB.saveCuentasPorPagar(rutUsuario, dv,numdoc, rutnum, dvrut, tipoBoleta,fecha,comen_vale,tip_val,desc_val,valor_vale,datos_parametro ); // VS.19.12.2016
			    		                                           // 26/04/2017  MAA rutUsuario, dv se debiera sacar de la tabla de COHOF100B en este caso para que muestre rut de pago
			    		int error = moduloHonorarioB.saveCuentasPorPagar(rutUsuario, dv,numdoc, rutnum, dvrut, tipoBoleta,fecha,comen_vale,
			    				                                         tip_val,desc_val,valor_vale,datos_parametro,tipoGasto,rutDvIngresador,rutDvAutorizador,rutProd, rutDvDAF, tipo); // MB 24/03 modifica
			    		if(error == -12 ) accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n del archivo Historial del documento.");
			    		else if(error == -13) accionweb.agregarObjeto("mensaje", "Problemas en la actualizaci�n del estado del Documento.");
			    		else if (error < 0) accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n a Autorizar. C�digo Banner.");//B=Banner
			            else  accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n con Traspaso a Banner.");
				 
		     } 
			} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado en BANNER autorizaDAFHonorario.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}	
			
			if (!existeGoremal) accionweb.agregarObjeto("mensaje", "Existe alg�n funcionario ingresador o autoizador no ingresado en GOREMAL."); 

		  	Thread.sleep(1500);
		    cargaAutorizaHonorario(accionweb);
		    
		    if(accionweb.getSesion().getAttribute("tipoGasto") != null) 
				accionweb.getSesion().removeAttribute("tipoGasto");
		    
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
				accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
	      	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
				accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
	      	
		}
	 
	 synchronized public void autorizaSobregiroHonorario(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
			int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
			//int numeroBoleta = Util.validaParametro(accionweb.getParameter("numeroBoleta"),0);
			String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"),"");	
			//String cuentaTipoGasto = Util.validaParametro(accionweb.getParameter("tipoGasto"),""); //VS.20.12.2016
			String tipoGasto = Util.validaParametro(accionweb.getParameter("tipoGasto"),""); //VS.20.12.2016
			
			tipoGasto = Util.validaParametro(accionweb.getParameter("tipoGasto"),"");
			String cuentaTipoGasto = ""; //MB agrega cuenta tipo gasto
			cuentaTipoGasto = moduloHonorarioB.getCuentaTipoGasto(tipoGasto);
			//int rutPrestador = rutnum;
			//String dvPrestador = dvrut;
			
			String proceso = Util.validaParametro(accionweb.getParameter("proceso")+ "","");
			
			// Nuevo... VS.10.01.2017
			if(cuentaTipoGasto.equals(""))
				cuentaTipoGasto = Util.validaParametro(accionweb.getParameter("cuentaTipoGasto"),"");
			
			accionweb.getSesion().setAttribute("procesoSesion", "DB1"); 
			consultaHonorario(accionweb);
			if(accionweb.getSesion().getAttribute("procesoSesion") != null)
				  accionweb.getSesion().removeAttribute("procesoSesion");
			
			tipoGasto = Util.validaParametro(accionweb.getSesion().getAttribute("tipoGasto")+ "");
			if(cuentaTipoGasto.equals(""))
			   cuentaTipoGasto = moduloHonorarioB.getCuentaTipoGasto(tipoGasto);	
			
		    List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		    boolean aut = false;
			aut = this.revisaSaldoOrganizacion(listaSolicitudes,cuentaTipoGasto);
			Thread.sleep(1500);
			if (!aut) {
				 try {
					if (listaSolicitudes != null && listaSolicitudes.size() > 0){
					    for (Presw18DTO ss : listaSolicitudes) {
							if (!ss.getDesite().trim().equals("AUTORIZADA")) 
								aut = moduloHonorarioB.saveEstadoCuenta(numdoc,ss.getNompro().trim(),rutUsuario,dv,"A", tipoBoleta, rutnum, dvrut);
							if (!aut) {
					  	    	 accionweb.agregarObjeto("mensaje", "Problemas al modificar estado de la Organizaci�n a Autorizada.");
					  	    	 break;
					  	   	 }
						   }
					  } 
					} catch (Exception e) {
						accionweb.agregarObjeto("mensaje", "Error al actualizar estado a Autorizada autorizaSobregiroHonorario.");
						accionweb.agregarObjeto("exception", e);
						e.printStackTrace();
					}	
			}
			
		try {
			//String rutdv,String numvale, String fecha_invoice, String fecha_pmt_due, String fecha_trans, String cod_doct, String datos		   
					
		    String fecha = moduloHonorarioB.getDatoCOHOF102B(numdoc, rutnum, dvrut, tipoBoleta);
			String valor_vale = moduloHonorarioB.getValorDeBHESobregiro(numdoc, rutnum, dvrut, tipoBoleta);
			String rutDvIngresador = moduloHonorarioB.getRutOperacion(numdoc,rutnum, dvrut, tipoBoleta, "I");
			String rutDvAutorizador = moduloHonorarioB.getRutOperacion(numdoc,rutnum, dvrut, tipoBoleta,"A");
			String rutDvDAF = rutUsuario + dv;
			    		
			Vector vec_datos = new Vector();
			vec_datos = moduloHonorarioB.getDatosAS(numdoc, rutnum, dvrut, tipoBoleta);
			String datos_parametro = "";
			Vector vec = null;
			String comen_vale = "";
			String tip_val   = "";
			String desc_val  = "";
			int rutHon      = 0;
			String dvHon    = "";
			String rutProd  = "";
			String codOrg     = "";
			    		 
			for (int i = 0; i < vec_datos.size(); i++) {
			  vec = (Vector) vec_datos.get(i);
			  comen_vale =  vec.get(6)+"";
		      tip_val    = vec.get(7)+""; //VS.20.12.2016
		      desc_val   = vec.get(8)+"";
		      rutHon    = Integer.parseInt(vec.get(9)+"");
		      dvHon     = vec.get(10)+"";
		      rutProd    = rutHon + dvHon;
		      codOrg     = vec.get(1)+"";
		                     
			  for (int y = 0; y < vec.size(); y++) { // para que rescate hasta valval
			     if (y == 11) datos_parametro = datos_parametro + vec.get(y) + "&" + moduloHonorarioB.getBancoOperacion(codOrg) + "@"; // Cunado llegue a VALVAL agrgue @			    				else if(y <= 4)datos_parametro = datos_parametro + vec.get(y) + "&";
			 	 else if(y <= 4)datos_parametro = datos_parametro + vec.get(y) + "&";	 }
			  }
			  //System.out.println("datos_parametro " + datos_parametro); 
			  //int error = moduloHonorarioB.saveCuentasPorPagar(rutUsuario, dv,numdoc, rutnum, dvrut, tipoBoleta,fecha,comen_vale,tip_val,desc_val,valor_vale,datos_parametro ); // VS.19.12.2016
			    		                                           // 26/04/2017  MAA rutUsuario, dv se debiera sacar de la tabla de COHOF100B en este caso para que muestre rut de pago
			  int error = moduloHonorarioB.saveCuentasPorPagar(rutUsuario, dv,numdoc, rutnum, dvrut, tipoBoleta,fecha,comen_vale,
			    				                                         tip_val,desc_val,valor_vale,datos_parametro,tipoGasto,rutDvIngresador,rutDvAutorizador,rutProd, rutDvDAF, tipo ); // MB 24/03 modifica
			  if(error == -12 || tipo == -14) accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n del archivo Historial del documento.");
			  else if(error == -13) accionweb.agregarObjeto("mensaje", "Problemas en la actualizaci�n del estado del Documento.");
			  else if (error < 0) accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n a Autorizar. C�digo Banner.");//B=Banner
			  else  accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n con Traspaso a Banner.");
				 
		  } catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado en BANNER autorizaSobregiroHonorario.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}	

		  Thread.sleep(1500);
		  cargaAutorizaHonorario(accionweb);
		    
		  if(accionweb.getSesion().getAttribute("tipoGasto") != null) 
			accionweb.getSesion().removeAttribute("tipoGasto");
		    
		  if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      	accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      	accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      	accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	      if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
			accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
	      	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
				accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
	      	
		}
	 
	 
	 
	 synchronized public void recepcionHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"), "");		
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);	
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
		int numeroBoleta = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		if(rutUsuario > 0 && !dv.trim().equals("")){
		try { 
			boolean graba = moduloHonorarioB.saveRecepciona(numeroBoleta, rutUsuario,dv,"TFB",tipoBoleta, rutnum, dvrut);
		    if(graba)
		    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la Recepci�n.");
		    else
		    	accionweb.agregarObjeto("mensaje", "No se Grab� la Recepci�n.");
	   	} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado saveRecepciona.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		} else accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n.(p�rdida de sesi�n)" );
		Thread.sleep(500);
	    cargaAutorizaHonorario(accionweb);
		
	}
	 synchronized public void recepcionaHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numeroBoleta = Util.validaParametro(accionweb.getParameter("numeroBoleta"), 0);
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"), "");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
		if(rutUsuario > 0 && !dv.trim().equals("")){
		try {	
		boolean graba = moduloHonorarioB.saveRecepciona(numeroBoleta, rutUsuario,dv,"TFB", tipoBoleta, rutnum, dvrut);
		
	    if(graba)
	    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la Recepci�n.");
	    else
	    	accionweb.agregarObjeto("mensaje", "No se Grab� la Recepci�n.");
	   	} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado saveRecepciona.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		} else accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n.(p�rdida de sesi�n)" );
		
		Thread.sleep(500);
	    cargaAutorizaHonorario(accionweb);
		
	}
	
	 synchronized public void rechazaAutHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numeroBoleta = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"), "");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
		String glosa1 = Util.validaParametro(accionweb.getParameter("glosa1"), "");
	    if(rutUsuario > 0 && !dv.trim().equals("")){
	    try {
			boolean graba = moduloHonorarioB.saveAutHonorario(numeroBoleta, rutUsuario,dv,"RB1",tipoBoleta, rutnum, dvrut, glosa1); /* VS.25-11-2016. Modificacion BANNER RBR RB1 */
		    if(graba)
		    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el rechazo de la Autorizaci�n.");
		    else
		    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Autorizaci�n.");
		   	} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado rechazaAutHonorario.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
		} else accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n.(p�rdida de sesi�n)" );
		
	   	Thread.sleep(500);
	    cargaAutorizaHonorario(accionweb);
		
	}
	 
	 synchronized public void rechazaAutDAFHonorario(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numeroBoleta = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"), "");
			int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
			String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
			String glosa1 = Util.validaParametro(accionweb.getParameter("glosa1"), "");
			int sedeLista = Util.validaParametro(accionweb.getParameter("sedeLista"), 0);	
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			
			if(rutUsuario > 0 && !dv.trim().equals("")){
				try{ 
				boolean graba = moduloHonorarioB.saveRechazoDirFinanza(numeroBoleta, rutUsuario,dv,"RBG",tipoBoleta, rutnum, dvrut, glosa1);
			    if(graba)
			    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el Rechazo de la Recepci�n.");
			    else
			    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Recepci�n.");
			   	} catch (Exception e) {
					accionweb.agregarObjeto("mensaje", "No guardado rechazaRecepHonorario.");
					accionweb.agregarObjeto("exception", e);
					e.printStackTrace();
				}
			} else accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n.(p�rdida de sesi�n)" );
			
			Thread.sleep(500);
		    cargaAutorizaHonorario(accionweb);
			
		}
	 
	 
	 synchronized public void rechazaRecepHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numeroBoleta = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"), "");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
		String glosa1 = Util.validaParametro(accionweb.getParameter("glosa1"), "");

		
		
		if(rutUsuario > 0 && !dv.trim().equals("")){
			try{ 
			boolean graba = moduloHonorarioB.saveRechazoDirFinanza(numeroBoleta, rutUsuario,dv,"RBF",tipoBoleta, rutnum, dvrut, glosa1);
		    if(graba)
		    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el Rechazo de la Recepci�n.");
		    else
		    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Recepci�n.");
		   	} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado rechazaRecepHonorario.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
		} else accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n.(p�rdida de sesi�n)" );
		
		Thread.sleep(500);
	    cargaAutorizaHonorario(accionweb);
		
	}
	 synchronized public void cargaRechazo(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numeroBoleta = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"), "");
			int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
			String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			String tipDoc = Util.validaParametro(accionweb.getParameter("tipDoc"), "");
		
		    accionweb.agregarObjeto("numDoc", numeroBoleta);
		    accionweb.agregarObjeto("tipoBoleta", tipoBoleta);
		    accionweb.agregarObjeto("rutnum", rutnum);
		    accionweb.agregarObjeto("dvrut", dvrut);
		    accionweb.agregarObjeto("tipo", tipo);
		    accionweb.agregarObjeto("tipDoc", tipDoc);
		}
	 
	public void imprimeHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numeroBoleta = Util.validaParametro(accionweb.getParameter("numeroBoleta"), 0);
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"),"");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
		//VS.06.12.2016.modificacion banner
		String tipoGasto = "";
		String nomTipoGasto = "";
		
		long valorAPagar = 0;
		//long sede = 0;
		int identificador = 0; 
		
	    String nomcam = "";
	    long valnu1 = 0;
	    long valnu2 = 0;
	    String valalf = "";
	    String glosa1 = "";
	    String glosa2 = "";
	    String glosa3 = "";

	    String estadoFinal = "";
	    //String nomSede = "";
	    String estado = "";
	    String codigoEstado ="";
		
		Presw19DTO preswbean19DTO = new Presw19DTO();
		String titulo = "";
		String tituloDetalle1 = "";
		preswbean19DTO.setTippro("DB1"); /*VS.25-11-2016.modificacion BANNER. DBH - DB1*/
		preswbean19DTO.setRutide(new BigDecimal(rutnum));
		preswbean19DTO.setDigide(dvrut);
		preswbean19DTO.setNumdoc(new BigDecimal(numeroBoleta));
		preswbean19DTO.setTipdoc(tipoBoleta);
		
		Vector vecSolicitudes = new Vector();	
		Vector vecHistorial = new Vector();	
		String nombrePrestador = "";
		long fechaBoleta = 0;
		long totalBoleta = 0;
		long retencion = 0;
		long totalAPago = 0;
	 	String fecha = "";
    	String diaFecha = "";
    	String mesFecha = "";
    	String annoFecha = "";
    	String valor = "";
    	String valor2 = "";
    	String nomTipoBoleta = "";
    	if(tipoBoleta.trim().equals("BOH"))
    		nomTipoBoleta = "Boleta de Honorario Manual";
    	else
    		nomTipoBoleta = "Boleta de Honorario Electr�nica";
    	String valorfecha = "";
		List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
		CocowBean cocowBean = new CocowBean();
		listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
		 if(listCocow36DTO != null && listCocow36DTO.size() >0 ) {
			    for(Cocow36DTO ss: listCocow36DTO){
			   //  if(ss.getTiping().trim().equals("VAP")){
			    	//if(ss.getNomcam().trim().equals("CODSUC")) VS.01-12-2016.no se utiliza
			    	//	sede = ss.getValnu1();
			    	//if(ss.getNomcam().trim().equals("DESIDE")) VS.02-12-2016 se saca de spriden
			    	//	nombrePrestador = ss.getValalf();
			    	if(ss.getNomcam().trim().equals("FECDOC")){
			    		fechaBoleta = ss.getValnu1();
			    		diaFecha = String.valueOf(fechaBoleta).substring(6);
	    				mesFecha = String.valueOf(fechaBoleta).substring(4,6);
	    				annoFecha = String.valueOf(fechaBoleta).substring(0,4);
	    				valorfecha = diaFecha+"/"+mesFecha+"/"+annoFecha; 
			    	}
				    if(ss.getNomcam().trim().equals("TIPGAS")) //VS.06.12.2016.tipo Gasto
				    	tipoGasto = ss.getValalf().trim();	
			    	if(ss.getNomcam().trim().equals("TOTBOL"))
			    		totalBoleta = ss.getValnu1();
			    	if(ss.getNomcam().trim().equals("RETBOL"))
			    		retencion = ss.getValnu1();
			    	if(ss.getNomcam().trim().equals("VALPAG"))
			    		totalAPago = ss.getValnu1();

			    	if(ss.getNomcam().trim().equals("DESCR1"))
			    		glosa1 = ss.getValalf();
			    	if(ss.getNomcam().trim().equals("DESCR2"))
			    		glosa2 = ss.getValalf();
			     	if(ss.getNomcam().trim().equals("DESCR3"))
			    		glosa3 = ss.getValalf();
			        if(ss.getNomcam().trim().equals("ESTADO")){
		            	estado = ss.getValalf();
		            	codigoEstado = ss.getResval();
			        }
			        //V.S.01-12-2016.modificacion banner
		           // if(ss.getNomcam().trim().equals("CUENTA")){
		           // 	Vector vec = new Vector();
		           // 	vec.addElement(ss.getValnu1());  // cuenta
		           // 	vec.addElement(ss.getValalf()); // unidad
		           // 	vec.addElement(ss.getValnu2()); // valor		            
		           //    	vec.addElement(ss.getResval()); // estado imputacion
		           // 	vecSolicitudes.addElement(vec);		            	
		           // }
		            if(ss.getNomcam().trim().equals("CUENTA")){
		            	Vector vec = new Vector();
		            	vec.addElement(ss.getResval().trim().substring(0,15).trim()+ " - " + ss.getValalf());
		            	vec.addElement(ss.getValalf()); // unidad
		            	vec.addElement(ss.getValnu2());
		               	vec.addElement(ss.getResval().trim().substring(15));	
		               	vecSolicitudes.addElement(vec);		            	
		            }		            
		            if(ss.getNomcam().trim().equals("HISTORIAL")){
	                   	if(ss.getValnu1()> 0 ) {
	    				fecha = ss.getValnu1() + "";
	    				diaFecha = fecha.substring(6);
	    				mesFecha = fecha.substring(4,6);
	    				annoFecha = fecha.substring(0,4);
	    				valor = diaFecha+"/"+mesFecha+"/"+annoFecha;   
	            	} else {
	    				valor = ss.getValnu1()+"";
	            	} 
	            	if (ss.getResval().trim().equals("AUTORIZACION"))// se ocupa de la 1-15 el resto se ocupa la observacion del rechazo
	            	   valor +=  " por " + ss.getValalf() + " Cuenta " + ss.getValnu2() ;
	            	else
	            		valor += " por " + ss.getValalf();
	            	
	            	Vector vec3 = new Vector();
	            	vec3.addElement(ss.getResval()); // es la accion, INGRESO, MODIFICACION, AUTORIZACION, PAGO, AUTORIZA FINANZA				        
	            	vec3.addElement(valor); // es la fecha, 
	            	vec3.addElement(ss.getValalf()); // nombre usuario
	            	vec3.addElement(ss.getValnu2()); //nombre unidad
	            	vec3.addElement(ss.getResval()); // glosa operaci�n
	            	vecHistorial.addElement(vec3);
	            }		           
			    // }
			    }	
			   
			 // MAA Sede no se usa BANNER
			    /*List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
			    listSede = moduloValePagoB.getListaSede(accionweb.getReq());
			    for( Presw18DTO ss:listSede){
			    	if(ss.getNummes() == sede)
			    		nomSede = ss.getDesuni();
			    }*/
			   //VS.06-12-2016.
			    List<Presw25DTO> listaTipoGasto = new ArrayList<Presw25DTO>();
			    listaTipoGasto = moduloHonorarioB.getListaTipoGasto();			    	
			    if(listaTipoGasto != null && listaTipoGasto.size() > 0){
					for (Presw25DTO ss : listaTipoGasto){
						//System.out.println(ss.getComen1().trim());
						if(ss.getComen1().trim().equals(tipoGasto)){
					    nomTipoGasto = ss.getComen2().trim(); 
						//System.out.println(ss.getComen1().trim()+"-"+ss.getComen2().trim());
						}	
					}
			    }
			    accionweb.getSesion().setAttribute("tipoGasto", tipoGasto); 
			    accionweb.getSesion().setAttribute("nomTipoGasto", nomTipoGasto);
				//fin			    
               accionweb.getSesion().setAttribute("vecSolicitudes", vecSolicitudes);
               accionweb.getSesion().setAttribute("vecHistorial", vecHistorial);
              // accionweb.getSesion().setAttribute("sede", sede); VS.01-12-2016.no se utiliza
               //accionweb.getSesion().setAttribute("nombrePrestador", nombrePrestador); //VS.02-12-2016.modificacion BANNER
               
       		   accionweb.getSesion().setAttribute("nombrePrestador", moduloHonorarioB.getVerificaRutNom(rutnum+""+dvrut)); 
               accionweb.getSesion().setAttribute("fechaBoleta", valorfecha);
               accionweb.getSesion().setAttribute("totalBoleta", moduloHonorarioB.formateoNumeroSinDecimales(Double.parseDouble(totalBoleta+"")));
               accionweb.getSesion().setAttribute("retencion", moduloHonorarioB.formateoNumeroSinDecimales(Double.parseDouble(retencion+"")));
               accionweb.getSesion().setAttribute("totalAPago", moduloHonorarioB.formateoNumeroSinDecimales(Double.parseDouble(totalAPago+"")));
               accionweb.getSesion().setAttribute("glosa1", glosa1);
               accionweb.getSesion().setAttribute("glosa2", glosa2);
               accionweb.getSesion().setAttribute("glosa3", glosa3);
               accionweb.getSesion().setAttribute("nomTipoBoleta", nomTipoBoleta);
               if(rutnum > 0)
            	   accionweb.getSesion().setAttribute("rutnum", moduloHonorarioB.formateoNumeroSinDecimales(Double.parseDouble(rutnum+"")) + "-" + dvrut);
               //accionweb.getSesion().setAttribute("nomSede", nomSede); VS.01-12-2016.no se utiliza
               accionweb.getSesion().setAttribute("numeroBoleta", String.valueOf(numeroBoleta));
               accionweb.getSesion().setAttribute("estadoFinal", estadoFinal);
               
		 }
	}
	 synchronized public void eliminaHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numeroBoleta"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"),"");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"),0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		
		String nomAut = "";
		switch(tipo){
		case 3:case 2: {
			// tipo == 2 es para los ingresadores
			if(tipo == 3)
				nomAut = " de Responsable de Organizaci�n. "; //VS.14.12.2016 Cuenta - Organizacion
			break;
		}
		case 8: {
			nomAut = " de Proyectos. ";
			break;
		}
		case 9: {
			nomAut = " de UCP. ";
			break;
		}
		case 10: {
			nomAut = " de Director de presupuestos. ";
			break;
		}
		}
		    boolean resultado = moduloHonorarioB.getEliminaHonorario(rutUsuario,dv,numdoc, tipoBoleta, rutnum, dvrut );
		    if(resultado)
		    	accionweb.agregarObjeto("mensaje", "Qued� anulada la Boleta de Honorario " + nomAut);
		    else
		    	accionweb.agregarObjeto("mensaje", "Problemas al anular la Boleta de Honorario" + nomAut);
			this.cargaAutorizaHonorario(accionweb);
			//accionweb.agregarObjeto("titulo","Modificaci�n");
			
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
				accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
	      	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
				accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
	      	
		}
	
	public void limpiaSolicitudHonorario(AccionWeb accionweb) throws Exception {
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		listaSolicitudes = null;
		accionweb.getSesion().removeAttribute("listaSolicitudes");
		accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes);
	
	}
	public ModuloValePagoB getModuloValePagoB() {
		return moduloValePagoB;
	}
	public void setModuloValePagoB(ModuloValePagoB moduloValePagoB) {
		this.moduloValePagoB = moduloValePagoB;
	}
	
	 synchronized public void registraPrestador(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
	    int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
		String nomPrestador = Util.validaParametro(accionweb.getParameter("nomPrestador"), "");
		String direccion = Util.validaParametro(accionweb.getParameter("direccion"),"");
		String ciudad = Util.validaParametro(accionweb.getParameter("ciudad"),"");	
		String pais = Util.validaParametro(accionweb.getParameter("pais"),"");
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"),"");
		int numeroBoleta = Util.validaParametro(accionweb.getParameter("numeroBoleta"), 0);
		String idSoli = Util.validaParametro(accionweb.getParameter("idSoli"),"");
		
		
		String nomP = moduloHonorarioB.eliminaAcentosString(nomPrestador);
		String dir  = moduloHonorarioB.eliminaAcentosString(direccion);
		String ciu = moduloHonorarioB.eliminaAcentosString(ciudad);
		/*segun petici�n se deben colocar todos los datos sin acento y con may�scula para ser registrado*/
		  
		List<Presw25DTO>  lista = new ArrayList<Presw25DTO>();
		Presw25DTO presw25 = new Presw25DTO();
		presw25.setTippro("PRX"); /*VS.25-11-2016. Modificacion BANNER. PRS - PRX*/
		presw25.setRutide(rutnum);
		presw25.setDigide(dvrut);
		presw25.setComen1(nomP.toUpperCase());
		presw25.setComen2(dir.toUpperCase());
		presw25.setComen3(ciu.toUpperCase());
		presw25.setComen4(pais.toUpperCase());			
		lista.add(presw25);
		if(rutUsuario > 0 && !dv.trim().equals("")){
		try{
		 	 boolean graba = moduloHonorarioB.savePrestador(lista,rutUsuario,dv);
		 	 if(graba){
		   	    accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa el Prestador de Servicios");
				accionweb.agregarObjeto("hayDatosBoleta", 2);
				accionweb.agregarObjeto("rutnum", 2);
				getRevisaBoleta(accionweb);
		    } else
		    	accionweb.agregarObjeto("mensaje", "No se registr� el Prestador de Servicios.");
		 	Thread.sleep(500);
		   	} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado savePrestador.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
		} else accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n.(p�rdida de sesi�n)" );
		
		  	accionweb.agregarObjeto("esBoletaB", 1);   
	 
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
				accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
	      	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
				accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
	      	
	}
	public void cargaIngresoIngresador(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		//int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0); VS.07.12.2016
		String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"").trim();
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
		//String nomUnidad = ""; VS.07.12.2016
		String nomOrganizacion = "";
		String titulo = "Nuevo Ingresador";
	
		 moduloHonorarioB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
		 List<Presw18DTO>    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		 if(listaUnidades != null && listaUnidades.size() > 0){
			 for (Presw18DTO ss : listaUnidades){
				 //if(ss.getCoduni() == unidad) VS.07.12.2016			
				//	 nomUnidad = ss.getDesuni();			 
				 
				 //if(organizacion.trim().equals(ss.getNompro().trim()))
				 if(organizacion.trim().equals(ss.getNompro().trim()))
					 nomOrganizacion = ss.getDesuni();				 
			 }
		 }		 
		//accionweb.agregarObjeto("unidad", unidad); VS.07.12.2016	
		//accionweb.agregarObjeto("nomUnidad", nomUnidad);
		accionweb.agregarObjeto("organizacion", organizacion);
		accionweb.agregarObjeto("nomOrganizacion", nomOrganizacion);		
		accionweb.agregarObjeto("esBoletaB", 1);
      	accionweb.agregarObjeto("opcion", tipo);
    	accionweb.agregarObjeto("opcionMenu", tipo);
      	accionweb.agregarObjeto("titulo", titulo);
	}
	 synchronized public void recepcionaMasivo(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			String linea = Util.validaParametro(accionweb.getParameter("linea"),"");
			String lineaRut = Util.validaParametro(accionweb.getParameter("lineaRut"),"");
			String lineaDV = Util.validaParametro(accionweb.getParameter("lineaDV"),"");
			String lineaTipDoc = Util.validaParametro(accionweb.getParameter("lineaTipDoc"),"");
			
			if(rutUsuario > 0){ // significa que hay sesi�n ah� graba	
			int numeroBoleta = 0;
			int rutnum = 0;
			String dvrut = "";
			String tipoBoleta = "";
			try { 
				int index = 0;
				int indexRut = 0;
				int indexDV = 0;
				int indexTipDoc = 0;
				boolean graba = false;
				String mensaje = "";
				while(linea.length() > 0){
					index = linea.indexOf("@");
					indexRut = lineaRut.indexOf("@");
					indexDV = lineaDV.indexOf("@");
					indexTipDoc = lineaTipDoc.indexOf("@");
					if(index > 0){
						numeroBoleta = Integer.parseInt(linea.substring(0,index));
						rutnum =  Integer.parseInt(lineaRut.substring(0,indexRut));
						dvrut = lineaDV.substring(0,indexDV);
						tipoBoleta = lineaTipDoc.substring(0,indexTipDoc);
					}else{
						numeroBoleta = Integer.parseInt(linea);
						rutnum =  Integer.parseInt(lineaRut);
						dvrut = lineaDV;
						tipoBoleta = lineaTipDoc;
						lineaRut = "";
						linea = "";
					}
					graba = true;
					graba = moduloHonorarioB.saveRecepciona(numeroBoleta, rutUsuario,dv,"TFB", tipoBoleta, rutnum, dvrut);
					if(graba)
					    	mensaje += "Se grab&oacute; en forma exitosa la Recepci&oacute;n de la Boleta N " + numeroBoleta + " RUT: "+rutnum + "-"+dvrut+"Tipo Boleta: "+tipoBoleta+"<br>";
					    else
					    	mensaje += "No se Grab� la Recepci&oacute;n de la Boleta  N " + numeroBoleta + " RUT: "+rutnum + "-"+dvrut+"Tipo Boleta: "+tipoBoleta;
					 linea = linea.substring(index+1);
					 lineaRut = lineaRut.substring(indexRut+1);
					 lineaDV = lineaDV.substring(indexDV+1);
					 lineaTipDoc = lineaTipDoc.substring(indexTipDoc+1);
					
				}
			   	accionweb.agregarObjeto("mensaje", mensaje);
				Thread.sleep(500);
				} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado recepcionaMasivo.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
			} else 	accionweb.agregarObjeto("mensaje", "No guardado recepcionaMasivo.(P&eacute;rdida de sesi&oacute;n)");			
			
		}
/* NUEVOS PARA BANNER */
public void verificaRutIngresador(AccionWeb accionweb) throws Exception {  //VS.07-12-2016 .
	int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
	String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
	int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
	String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
	String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
	//int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
	String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"").trim();
	//String nomUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"),"");
	String nomOrganizacion = Util.validaParametro(accionweb.getParameter("nomOrganizacion"),"").trim();	

	String nombre = "";
	String indprc = "";
	if(rutnum > 0){
		
	Collection<Presw18DTO> lista = moduloHonorarioB.getVerificaRutIngresador(rutnum+""+dvrut);
	if(lista != null && lista.size() > 0){
	  for (Presw18DTO ss : lista){
			 nombre = ss.getDesuni();
			 indprc = ss.getIndprc();
	   }
	}
	accionweb.agregarObjeto("rut_aux", rut_aux);  
	accionweb.agregarObjeto("rutnum", rutnum); 
	accionweb.agregarObjeto("dvrut", dvrut); 
	//accionweb.agregarObjeto("unidad", unidad); 
	//accionweb.agregarObjeto("nomUnidad", nomUnidad); 
	accionweb.agregarObjeto("organizacion", organizacion); 
	accionweb.agregarObjeto("nomOrganizacion", nomOrganizacion);	
	}
   	if(!nombre.trim().equals("")){
   		if(indprc.trim().equals("F")){
    		accionweb.agregarObjeto("hayDatoslista", 1);
    		accionweb.agregarObjeto("nomIngresador", nombre);
   		} else {
   			accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado como Funcionario.");
   			accionweb.agregarObjeto("organizacion", organizacion); 
   			accionweb.agregarObjeto("nomOrganizacion", nomOrganizacion);
   		}
    } else
    	accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado. Debe solicitar a FINANZAS su creaci�n.");
   	
   	accionweb.agregarObjeto("esBoletaB", 1);
  	accionweb.agregarObjeto("titulo", "Nuevo Ingresador");	
}
synchronized public boolean revisaSaldoOrganizacion(List<Presw18DTO>  listaSolicitudes, String cuentaTipoGasto) throws Exception { // VS.16.12.2016
 	
	long total = 0;
 	long saldoCuenta = 0;
 	boolean sobrepasa = false;
	if (listaSolicitudes != null && listaSolicitudes.size() > 0){
	    for (Presw18DTO ss : listaSolicitudes) {
     	   total = ss.getUsadom();
		   saldoCuenta = moduloValePagoB.getBuscaSaldo(ss.getNompro().trim(),cuentaTipoGasto.trim());
					
		   if(total > saldoCuenta ) {
			   sobrepasa = true;
			   break;
		   }
		 }	
      }
	return sobrepasa;
 
 }

synchronized public boolean revisaAutOrganizacion(List<Presw18DTO>  listaSolicitudes, int numdoc, int rutUsuario, String dv, boolean modifica, String tipoBoleta, int rutprestador, String dvprestador) throws Exception { //VS.16.12.2016
 	boolean aut = true;
	if (listaSolicitudes != null && listaSolicitudes.size() > 0){
	  if (modifica) {	
	      for (Presw18DTO ss : listaSolicitudes) {	
	    	  System.out.println(ss.getDesite() + "-" + ss.getDesite().trim().equals("AUTORIZADA") ); 
	    	  if (!ss.getDesite().trim().equals("AUTORIZADA")) {
	    		  aut = moduloHonorarioB.saveEstadoCuenta(numdoc,ss.getNompro().trim(),rutUsuario,dv,"A", tipoBoleta,rutprestador, dvprestador );
	    		  if (!aut) break;
	    	  }	  
		  }
 	 }
	 else {
		   for (Presw18DTO ss : listaSolicitudes) {	
			 // System.out.println(ss.getDesite() + "-" + ss.getDesite().trim().equals("AUTORIZADA") ); 
		   	  if (!ss.getDesite().trim().equals("AUTORIZADA")) { 
		   		  aut = false;
		   		  break;
		   	  } 	  
		   }
	 }
	}	  
   return aut;
}	

synchronized public boolean actualizaSinFinOrganizacion(List<Presw18DTO> listaSolicitudes, String cuentaTipoGasto, int numdoc, int rutUsuario, String dv, String tipoBoleta, int rutprestador, String dvprestador) throws Exception {  //VS.16.12.2016
 	long saldoCuenta = 0;
 	boolean sf = true;  // Sin Financiamiento
	if (listaSolicitudes != null && listaSolicitudes.size() > 0){
	    for (Presw18DTO ss : listaSolicitudes) {
          saldoCuenta = moduloValePagoB.getBuscaSaldo(ss.getNompro().trim(),cuentaTipoGasto.trim());
		  if(ss.getUsadom() > saldoCuenta ) {
	    	if (!ss.getDesite().trim().equals("APROB.S/FINANCIAM")) 
			        sf = moduloHonorarioB.saveEstadoCuenta(numdoc,ss.getNompro().trim(),rutUsuario,dv,"B", tipoBoleta, rutprestador, dvprestador);
		   }	
	     }
      }
	return sf;
 }

synchronized public boolean actualizaConFinOrganizacion(List<Presw18DTO> listaSolicitudes, String cuentaTipoGasto, int numdoc, int rutUsuario, String dv, String tipoBoleta, int rutprestador, String dvprestador) throws Exception {  //VS.16.12.2016
 	long saldoCuenta = 0;
 	boolean cf = true;  // Con Financiamiento
	if (listaSolicitudes != null && listaSolicitudes.size() > 0){
	    for (Presw18DTO ss : listaSolicitudes) {
  		     saldoCuenta = moduloValePagoB.getBuscaSaldo(ss.getNompro().trim(),cuentaTipoGasto.trim());
			 if(saldoCuenta > ss.getUsadom()) {
	    		if (!ss.getDesite().trim().equals("AUTORIZADA")) 
			        cf = moduloHonorarioB.saveEstadoCuenta(numdoc,ss.getNompro().trim(),rutUsuario,dv,"A", tipoBoleta, rutprestador, dvprestador);
		   }	
	     }
      }
	return cf;
 }
/* FIN BANNER*/

}