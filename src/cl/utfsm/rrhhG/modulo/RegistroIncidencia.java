package cl.utfsm.rrhhG.modulo;


import java.util.ArrayList;
import java.util.Collection;

import cl.utfsm.rrhhG.datos.PreGurfeedDAO;

public class RegistroIncidencia {
	
	private Collection<PreGurfeed> ListPreGurfeed = new ArrayList<PreGurfeed>();
	private RegistroProceso regPro;
	private Incidencia incidencia;	
	private String regincLocalizacion;
	
	public RegistroIncidencia() {
	
	}
	
	public RegistroProceso getRegPro() {
		return regPro;
	}

	public void setRegPro(RegistroProceso regPro) {
		this.regPro = regPro;
	}

	public Collection<PreGurfeed> getListPreGurfeed() {
		return ListPreGurfeed;
	}

	public void setListPreGurfeed(Collection<PreGurfeed> listPreGurfeed) {
		ListPreGurfeed = listPreGurfeed;
	}

	public Incidencia getIncidencia() {
		return incidencia;
	}

	public void setIncidencia(Incidencia incidencia) {
		this.incidencia = incidencia;
	}

	public String getRegincLocalizacion() {
		return regincLocalizacion;
	}

	public void setRegincLocalizacion(String regincLocalizacion) {
		this.regincLocalizacion = regincLocalizacion;
	}
	
	public void agregarPreGurfeed(PreGurfeed preGur){
		this.ListPreGurfeed.add(preGur);		
	}
	
	public void cargarListPreGurfeed(){
		PreGurfeedDAO preGurDAO = new PreGurfeedDAO();		
		preGurDAO.cargarLisrPreGurfeed(this);		
	}
	
	
	
	
	

}
