package cl.utfsm.rrhhG.modulo;

public class CuentaDescuento implements TablaAutomatica,Comparable<CuentaDescuento>{
	private int codigo;
	private String planta;
	private String nivel;
	private String cuenta;
	private String organizacion;
	private String programa;
	private String fondo;
	private String id;
	
	public CuentaDescuento(){
		
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getPlanta() {
		return planta;
	}

	public void setPlanta(String planta) {
		this.planta = planta;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getOrganizacion() {
		return organizacion;
	}

	public void setOrganizacion(String organicion) {
		this.organizacion = organicion;
	}

	public String getFondo() {
		return fondo;
	}

	public void setFondo(String fondo) {
		this.fondo = fondo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getPrograma() {
		return programa;
	}

	public void setPrograma(String programa) {
		this.programa = programa;
	}

	@Override
	public String getId(int id) {
		switch (id) {
		case 0:
			return ""+this.codigo;
		case 1:
			return  ""+this.codigo;			
		case 2:
			return this.planta;			
		case 3:
			return this.nivel;			
		case 4:
			return this.cuenta;			
		case 5:
			return this.organizacion;			
		case 6:
			return this.programa;
		case 7:
			return this.fondo;	
		default:
			return "";
		}
	}

	@Override
	public int compareTo(CuentaDescuento cuentaDescuento) {
		int byAge = Integer.valueOf(this.codigo).compareTo(Integer.valueOf(cuentaDescuento.getCodigo()));
		if ( byAge != 0 ) {
            return byAge;
        }
		
		byAge =  this.planta.compareToIgnoreCase(cuentaDescuento.getPlanta());
		if (byAge != 0){
			return byAge;
		}
		
		byAge = this.nivel.compareToIgnoreCase(cuentaDescuento.getNivel());
		if (byAge != 0){
			return byAge;
		}
		return 0;
	}
	
	
	
}
