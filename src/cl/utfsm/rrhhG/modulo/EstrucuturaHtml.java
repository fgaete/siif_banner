package cl.utfsm.rrhhG.modulo;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cl.utfsm.base.AccionWeb;
import cl.utfsm.rrhhG.datos.EtapaProcesoDAO;
import cl.utfsm.rrhhG.datos.PreGurfeedDAO;
import cl.utfsm.rrhhI2.modulo.BasePersona;

public class EstrucuturaHtml {

	public EstrucuturaHtml() {
		
	}
	/* Tabla de estado de los procesos es utilisadas en los proceso:
	 * GURFEED
	 * INTERFACE 2
	 * Interface 1
	 */
	
	public String tableEtapa( Map<String, RegistroProceso> mapRegPro , AccionWeb accionWeb ) throws Exception {
		
		EtapaProcesoDAO etaProDAO = new EtapaProcesoDAO();
		ArrayList<EtapaProceso> listEtaPro = etaProDAO.buscarEtapas(mapRegPro.get("1").getCodProceso());
		
		String html = "<div id='list' class='list'>"; // replesentate  tabla 
		RegistroProceso regPRO ;
		String home = (String)accionWeb.getMav().getModel().get("home");
		System.out.print("\n"+home+"\n");
		for (EtapaProceso etapaProceso : listEtaPro) {
			regPRO = mapRegPro.get(""+etapaProceso.getCodEtapaProceso());
			if(regPRO == null){
				regPRO = new RegistroProceso();
				regPRO.setRegProFinalizado("0");
			}
			if(etapaProceso.getDesEtapaProceso() == null || etapaProceso.getDesEtapaProceso().equals("")){
				etapaProceso.setDesEtapaProceso(etapaProceso.getNomEtapaProceso());
			}
			if(etapaProceso.getCodEtapaProceso() > 0){
				
				if(regPRO.getRegProFinalizado().equals("E")){
					html +=       	"<div class='actual' title='"+etapaProceso.getDesEtapaProceso()+"'>";// replesentante tr
				}else{
					html +=       	"<div class='tr' title='"+etapaProceso.getDesEtapaProceso()+"'>";// replesentante tr
				}
				html +=       		"<div id='textEtapa"+etapaProceso.getCodEtapaProceso()+"' class='textEtap' >";// replesante a td para texto
				html +=  					etapaProceso.getCodEtapaProceso()+". "+ etapaProceso.getNomEtapaProceso()  ;
				html +=       		"</div>";				
				if( regPRO.getRegProFinalizado().equals("A")){
					html +=       		"<div id='imgEtapa"+etapaProceso.getCodEtapaProceso()+"' class='imgEta' >";
					html +=              		"<img  style='float: left; width:18px' src='"+home+"/sgf/img/RRHH/alerta.gif'  />";
					html +=       		"</div>";
				} else if (regPRO.getRegProFinalizado().equals("F")){
					html +=       		"<div id='imgEtapa"+etapaProceso.getCodEtapaProceso()+"' class='imgEta' >";
					html +=              		"<img  style='float: left; width:18px' src='"+home+"/sgf/img/RRHH/24X24-sign2-check.png'  />";
					html +=       		"</div>";	
				} else if (regPRO.getRegProFinalizado().equals("C")){
					html +=       		"<div id='imgEtapa"+etapaProceso.getCodEtapaProceso()+"' class='imgEta' >";
					html +=              		"<img  style='float: left; width:18px' src='"+home+"/sgf/img/RRHH/borrar2.gif'  />";
					html +=       		"</div>";	
				}else if (regPRO.getRegProFinalizado().equals("E")){
					html +=       		"<div id='imgEtapa"+etapaProceso.getCodEtapaProceso()+"' class='imgEta' >";
					html +=              		"<img  style='float: left; width:32px' src='"+home+"/sgf/img/RRHH/engr.gif'  />";
					html +=       		"</div>";	
				}else {					
					html +=       		"<div id='imgEtapa"+etapaProceso.getCodEtapaProceso()+"' class='imgEta' >";
					html +=       		"</div>";					
				}			
				html +=       	"</div>";
			}
		}
		html +=       "</div>";		
		return html;
	}
	/* 
	 * tabla de Error estado de los contratos
	 * 
	 * 
	 */ /*
	public String errorValidarRut(List<BasePersona> personas,ArrayList<ArrayList<String>> totales){		
		String tablaPersonasP = " <div id='P' name='P' class='tabla_err'  style='width: 100%; display: none'>";
		tablaPersonasP += "<div id='volver' style='margin-right: 110px;'> <ul style='list-style:none;' > <li> <a href='#' onclick='visibleSolo(\"error\")' return false;'> <img src='/sgf/img/undo.gif' align='absmiddle' alt='Volver' border='0'> Volver</a></li></ul></div> ";
		tablaPersonasP += " <table id='tabla'   style='width: 100%; '>";
		
		String tablaPersonasF = "<div id='F' name='F' class='tabla_err' style='width: 100%; display: none'>";
	    tablaPersonasF += "<table id='tabla'   style='width: 100%; '>";
	    tablaPersonasF += "<div id='volver' style='margin-right: 110px;'> <ul style='list-style:none;' > <li> <a href='#' onclick='visibleSolo(\"error\")' return false;'> <img src='/sgf/img/undo.gif' align='absmiddle' alt='Volver' border='0'> Volver</a></li></ul></div> ";
	    
	    String tablaPersonasE = "<div id='E' name='E' class='tabla_err' style='width: 100%; display: none'>";
		tablaPersonasE += "<table id='tabla'   style='width: 100%;'>";
		tablaPersonasE += "<div id='volver' style='margin-right: 110px;'> <ul style='list-style:none;' > <li> <a href='#' onclick='visibleSolo(\"error\")' return false;'> <img src='/sgf/img/undo.gif' align='absmiddle' alt='Volver' border='0'> Volver</a></li></ul></div>";
		
		String cabesera = "<thead>"+
						  	"<tr>"+
						  		"<th>RUT</th>"+
						  		"<th>Nombre</th>"+
						  		"<th>Apellidos</th>"+
		                        "<th>Fecha Contrato</th>"+
		                        "<th>Estado</th>"+
		                    "</tr>"+
		                  "</thead>"+
		                  "<tbody>";
		
		tablaPersonasP += cabesera ;
		tablaPersonasF += cabesera ;
		tablaPersonasE += cabesera ;
		String filaComun;
		for (BasePersona persona : personas) {
			filaComun =  "<tr>";
			filaComun += "<td>"+persona.getRutFormat()+"</td>";
			filaComun += "<td>"+persona.getNombre()+"</td>";
			filaComun += "<td>"+persona.getApellidoPat()+"/"+persona.getApellidoMat()+"</td>";
			filaComun += "<td>"+persona.fechaFormat()+"</td>";
			filaComun += "<td>"+persona.getEstado()+"</td>";
			filaComun += "</tr>";
			
			if (persona.getEstado().equals("P")){
				tablaPersonasP += filaComun;
			}
			if(persona.getEstado().equals("E")){
				tablaPersonasE += filaComun;
			}
			if(persona.getEstado().equals("F")){
				tablaPersonasF += filaComun;
			}
		}		
		String footer = "</tbody>";
		footer += "</table>";
		footer += "</div>";
		tablaPersonasP += footer;
		tablaPersonasF += footer;
		tablaPersonasE += footer;
		
		String tablasTotales  = "<div id='Pricipal' class='tabla_err' name='error' style='width: 100%;'>";
		tablasTotales        += "<table id='tabla'   style='width: 100%;'>";		
		tablasTotales        += "<thead><tr>";
		tablasTotales        += "<th>Tipo</th><th>Cantidad</th><th>Detalle</th>";
		tablasTotales        += "</tr></thead><tbody>";		
		String detalle ="";
		for (ArrayList<String> total : totales) {
			if(total.get(0).equals("P")){
				detalle = "No se encuentra en SPRIDEN";
			}
			if(total.get(0).equals("E")){
				detalle = "No se encuentra en PEAEMPL";
			}
			if(total.get(0).equals("F")){
				detalle = "No se encuentra en NBAJOBS";
			}
			
			tablasTotales        += "<tr>";
			tablasTotales        += "<td>"+total.get(0)+": "+detalle+"</td>";
			tablasTotales        += "<td>"+total.get(1)+"</td>";
			tablasTotales        += "<td >";
			tablasTotales        +=      "<div id='menu_row' style='width: 100%;'>";
			tablasTotales        +=           "<ul style='float: right;'>";
			tablasTotales        +=               "<li>";
			tablasTotales        +=                   "<a href='#' onclick='return false;' class='nivel'></a>";
			tablasTotales        += 		          "<ul>";
			tablasTotales        +=                      "<li>";
			tablasTotales        +=                         "<a href=\"#\" onclick=\"visibleSolo(\'"+total.get(0)+"\');return false; \" >Ver Detalle</a>";
			tablasTotales        +=                      "</li>";
			tablasTotales        +=                   "</ul>";
			tablasTotales        +=               "</li>";
			tablasTotales        +=           "</ul>";
			tablasTotales        +=       "</div>";
			tablasTotales        += "</td>";
			tablasTotales        += "</tr>";		
		}		
		tablasTotales        += "</tbody></table>";
		tablasTotales        += "</div>";	
		
		
		return tablasTotales+tablaPersonasE+tablaPersonasF+tablaPersonasP;
	}
	/*
	 * Tabla resumen del resultado de GURFEED  
	 *//*
    public static String tablaTotales(int numEjec){
		
		PreGurfeedDAO preGurDAO = new PreGurfeedDAO();
		
		List<ResumenPorCuenta> totales = preGurDAO.totales(numEjec);
		DecimalFormat form = new DecimalFormat("###,###.##");
		Long totalHab = 0L;
		Long totalDes = 0L;
		Long totalSuma = 0L;
		String tabla = "<table id='tabla'   style='width: 100%;'>";
		tabla       += "<caption>Resumen</caption>";
		tabla       += "<thead>";
		tabla       += "		<tr>";
		tabla       += "			<th class='bordetop' >";
		tabla       += "		  		<span style='float: left'>CUENTA </span>";
		tabla       += "			</th>";
		tabla       += "			<th class='bordetop'>";
		tabla       += "		  		<span style='float: left'>SUMA DEBE</span> ";
		tabla       += "			</th>";
		tabla       += "			<th class='bordetop'>";
		tabla       += "		  		<span style='float: left'>SUMA HABER</span> ";
		tabla       += "			</th>";
		tabla       += "			<th class='bordetop'>";
		tabla       += "		  		<span style='float: left'>SUMA SALDO</span> ";
		tabla       += "			</th>";
		tabla       += "		</tr>";
		tabla       += "	</thead>";
		tabla       += "	<tbody>";
		for (ResumenPorCuenta total : totales) {
			tabla       += "	<tr>";
			tabla       += "		<td> ";
			tabla       += "           <span style='float: left'>"+ total.getCuenta()+" "+total.getDesc() +"</span>";
			tabla       += "		</td>";
			tabla       += "		<td>";
			tabla       += "           <span style='float: right'>"+form.format(total.getDebe())+"</span>";			
			totalHab += total.getHaber();
			tabla       += "		</td>";
			tabla       += "		<td>";
			tabla       += "           <span style='float: right'>"+form.format(total.getHaber())+"</span>";
			totalDes += total.getDebe();
			tabla       += "		</td>";
			tabla       += "		<td>";
			tabla       += "           <span style='float: right'>"+form.format(total.getTotal())+"</span>";
			totalSuma += total.getTotal();
			tabla       += "		</td>";
			tabla       += "	</tr>";			
		}
		tabla       += "	<tr>";
		tabla       += "		<th class='bordetop' style=' background: #f4f4f4;'>";
		tabla       += "			<span style='float: right;'><strong>Total</strong></span>";	
		tabla       += "		</th>";
		tabla       += "		<th class='bordetop' style=' background: #f4f4f4;'>";
		tabla       += "			<span style='float: right'><strong>"+form.format(totalHab)+"</strong></span>";	
		tabla       += "		</th>";	
		tabla       += "		<th class='bordetop' style=' background: #f4f4f4;'>";
		tabla       += "			<span style='float: right'><strong>"+form.format(totalDes)+"</strong></span>";	
		tabla       += "		</th>";
		tabla       += "		<th class='bordetop' style=' background: #f4f4f4;'>";
		tabla       += "			<span style='float: right'><strong>"+form.format(totalSuma)+"</strong></span>";	
		tabla       += "		</th>";
		tabla       += "	</tr>";		
		tabla       += "	</tbody>";		
	    tabla       += "</table>";	
		return tabla;
	}
	/*
	 * tabla de registro errores de gurfeed 
	 *//*
    public static String impriTabla(RegistroIncidencia regInc , AccionWeb accionWeb){
    String home = (String)accionWeb.getMav().getModel().get("home");
    System.out.print("\n"+home+"\n");
	String columnas= "";
	/* columnas PreGurfeed	 * 
	 *pregurId
     *pregurValpes
	 *pregurTipo
	 *pregurFondo
	 *pregurOrganizacion
	 *pregurCuenta
	 *pregurPrograma
	 *pregurCodhde
	 *pregurPidm
	 *pregurRut
	 *pregurCodpla
	 *pregurNumniv
	 *pregurCoduni 
	 * *//*
	switch (regInc.getIncidencia().getIncId()) {
	case 30:
		// error para registros que no se les encontro cuenta por la conbinacion en tabla tmp_cuenta_haberes o tabla Tmp_cuenta_Descuentos
		// columnas que para mostrar 
		columnas+="pregurCuenta-pregurCodpla-pregurNumniv-pregurCodhde-pregurRut";
		break;
	case 32:
		// error para registros que el coduni no tenga una organizacion acignada en tabla de as400 tabla  USMMBP.PRESF201@ASLINK 
		// columnas que para mostrar 
		columnas+="pregurRut-pregurCodhde-pregurOrganizacion-pregurCoduni";
		break;
	case 33:
		// error para registros que el coduni no tenga una organizacion acignada en tabla de as400 tabla  USMMBP.PRESF201@ASLINK 
		// columnas que para mostrar 
		columnas+="pregurRut-pregurCodhde";
		break;
	case 36:
		columnas+="pregurOrganizacion-pregurPrograma-pregurFondo";
	default:
		break;
	}
	String table = "<div class='tabla_err' name='"+regInc.getIncidencia().getIncId()+"' style='width: 100%; height:220px; overflow-y: scroll; display:none;' >";
	table += "<div id='volver' style='margin-right: 110px;'> <ul style='list-style:none;' > <li> <a href='#' onclick='visibleSolo(\"error\")' return false;'> <img src='"+home+"/sgf/img/undo.gif' align='absmiddle' alt='Volver' border='0'> Volver</a></li></ul></div><table id='tabla'  style='width: 100%; ' ><thead><tr>";
	if(columnas.indexOf("pregurId")> -1 ){
		table +=	      "<th class='bordetop'>ID PRE_GURFEED<th>";
	}
	if(columnas.indexOf("pregurValpes")> -1 ){
		table +=	      "<th class='bordetop'>VALOR</th>";
	}
	if(columnas.indexOf("pregurFondo")> -1 ){
		table +=	      "<th class='bordetop'>FONDO</th>";
	}
	if(columnas.indexOf("pregurOrganizacion")> -1 ){
		table +=	      "<th class='bordetop'>ORGANIZACI�N</th>";
	}
	if(columnas.indexOf("pregurCuenta")> -1 ){
		table +=	      "<th class='bordetop'>CUENTA</th>";
	}
	if(columnas.indexOf("pregurPrograma")> -1 ){
		table +=	      "<th class='bordetop'>PROGRAMA</th>";
	}
	if(columnas.indexOf("pregurCodhde")> -1 ){
		table +=	      "<th class='bordetop'>HABER / DESCUENTO</th>";
	}
	if(columnas.indexOf("pregurPidm")> -1 ){
		table +=	      "<th class='bordetop'>PIDM DE LA PERSONA</th>";
	}
	if(columnas.indexOf("pregurRut")> -1 ){
		table +=	      "<th class='bordetop'>RUT</th>";
	}
	if(columnas.indexOf("pregurCodpla")> -1 ){
		table +=	      "<th class='bordetop'>PLANTA</th>";
	}
	if(columnas.indexOf("pregurNumniv")> -1 ){
		table +=	      "<th class='bordetop'>NIVEL</th>";
	}
	if(columnas.indexOf("pregurCoduni")> -1 ){
		table +=	      "<th class='bordetop'>C�DIGO UNIDAD</th>";
	}
	table +=	      "</tr></thead><tbody>";
	for (PreGurfeed preGur : regInc.getListPreGurfeed()) {	
		table +=	      "<tr>";
		if(columnas.indexOf("pregurId")> -1 ){
			table +=	      "<th class='bordetop'>"+preGur.getPregurId()+"</th>";
		}		
		if(columnas.indexOf("pregurValpes")> -1 ){
			table +=	      "<th class='bordetop'>"+preGur.getPregurValpes()+"</th>";
		}
		if(columnas.indexOf("pregurFondo")> -1 ){
			table +=	      "<th class='bordetop'>"+preGur.getPregurFondo()+"</th>";
		}
		if(columnas.indexOf("pregurOrganizacion")> -1 ){
			table +=	      "<th class='bordetop'>"+preGur.getPregurOrganizacion()+"</th>";
		}
		if(columnas.indexOf("pregurCuenta")> -1 ){
			table +=	      "<th class='bordetop'>"+preGur.getPregurCuenta()+"</th>";
		}
		if(columnas.indexOf("pregurPrograma")> -1 ){
			table +=	      "<th class='bordetop'>"+preGur.getPregurPrograma()+"</th>";
		}
		if(columnas.indexOf("pregurCodhde")> -1 ){
			table +=	      "<th class='bordetop'>"+preGur.getPregurCodhde()+"</th>";
		}
		if(columnas.indexOf("pregurPidm")> -1 ){
			table +=	      "<th class='bordetop'>"+preGur.getPregurPidm()+"</th>";
		}
		if(columnas.indexOf("pregurRut")> -1 ){
			table +=	      "<th class='bordetop'>"+preGur.getRutFormato()+"</th>";
		}
		if(columnas.indexOf("pregurCodpla")> -1 ){
			table +=	      "<th class='bordetop'>"+preGur.getPregurCodpla()+"</th>";
		}
		if(columnas.indexOf("pregurNumniv")> -1 ){
			table +=	      "<th class='bordetop'>"+preGur.getPregurNumniv()+"</th>";
		}
		if(columnas.indexOf("pregurCoduni")> -1 ){
			table +=	      "<th class='bordetop'>"+preGur.getPregurCoduni()+"</th>";
		}
		table +=	      "</tr>";
	}
	table +=	      "</tbody></table></div>";	
	return table;
}
	/*
	 * estring columnas
	 * primera dimecion de es para el orden de columnas de izquierda a derecha
	 * sengunda dimencion es el contenido de la columna posision 1 encabesado de columna y 
	 * segundo id de variables del OBJTO.
	 * ejemplo la primera columna sera el "CODHDE"  del objeto con el encabezado "Codigo de haber"
	 * 			columnas[0][0]= "Codigo de haber";
	 * 			columnas[0][1]= "1";
	 * 			la segunda coluna sera "nivel" del objeto y el cabesera dira "Nivel del trabajador"
	 * 			columnas[1][0]= "Nivel del trabajador";
	 * 			columnas[1][1]= "3";
	 * columnas especciales:
	 * 			columnas[2][0]= "Nivel del trabajador";
	 * 			columnas[2][1]= "DET";
	 * etc...;
	 */ /*
    public static String tablaCuentaHabDes(ArrayList<TablaAutomatica> lisHab,String[][] columnas ){
    	String tabla ="";
    	// generacion de cabesera de la tabla 
    	tabla +="<table id='tabla' style='width: 100%;'>";// apertura de la tabla
    	tabla +=	"<thead>";// apertura de cabesera
    	tabla +=		"<tr>";// apertura de fila
    	for (int i = 0; i < columnas.length; i++) {
    		tabla +=	"<th>"+columnas[i][0]+"</th>";//columna
		}
    	tabla +=		"</tr>";// sierre de fila 
    	tabla +=	"</thead>";// Sierre de cabesera
    	tabla +=	"<tbody>";// Sierre de cuerpo
    	for (TablaAutomatica registro : lisHab) {
    		
    		tabla +=	"<tr >";//  Fila
    		for (int x = 0; x < columnas.length; x++) {
    			tabla +=	"<td>";//  Columna
    			if(columnas[x][1].equals("Det")){
    				tabla += 	"<div id='menu_row' style='width: 100%;'>";
    				tabla += 		"<ul style='float: right;'>";
    				tabla += 			"<li>";
    				tabla += 				"<a href='#' onclick='return false;' class='nivel'></a>";
    				tabla += 				"<ul>\n";
    				tabla += 					"<li>\n";
    				tabla += 						"<a href=\"#\" onclick=\"getId(\""+registro.getId(0)+"\"); \" >Ver Detalle</a>\n";
    				tabla += 					"</li>\n";
    				tabla += 				"</ul>\n";
    				tabla += 			"</li>\n";
    				tabla += 		"</ul>\n";
    				tabla += 	"</div>\n";
    			}else if(columnas[x][1].equals("SEL")){
    				//tabla += "<input class=\"botton\"  type=\"button\" value=\"Seleccionar\" onclick=\"filaSeleccion('"+registro.getId(0)+"')\" />";
    			}else{
    				tabla +=registro.getId(Integer.parseInt(columnas[x][1]));
    			}
    			tabla +="</td>";// Sierre de Columna
			}
    		tabla +="</tr>";// Sierre de fila
		}
    	tabla +="</tbody></table>";// sierre de la tabala 
    	return tabla;
    }
    public static String tablaCuentaHabDes(ArrayList<TablaAutomatica> lisHab ,String[][] columnas ,int cantidadFilas ){
    	
    	int count = 0;
    	// generacion de cabesera de la tabla 
    	String tabla ="<table id='tabla' style='width: 100%;'><thead><tr>";// apertura de fila
    	for (int i = 0; i < columnas.length; i++) {
    		tabla +="		<th>"+columnas[i][0]+"</th>";
    	}
    	tabla +="		</tr></thead><tbody>";// Sierre de cuerpo
    	for (TablaAutomatica registro : lisHab) {
    		tabla +="	<tr>";// Sierre de Fila
    		for (int x = 0; x < columnas.length; x++) {
    			tabla +="	<td>";// Sierre de Columna
    			if(columnas[x][1].equals("Det")){
    				tabla += "	<div id='menu_row' style='width: 100%; float left;'>\n";
    				tabla += "		<ul style='float: right;'>\n";
    				tabla += "			<li>\n";
    				tabla += "				<a href='#' onclick='return false;' class='nivel'>\n";
    				tabla += "				</a>\n";
    				tabla += "				<ul>\n";
    				tabla += "					<li>\n";
    				tabla += "						<a href=\"#\" onclick=\"getId('"+registro.getId(0)+"');\" >Ver Detalle</a>\n";
    				tabla += "					</li>\n";
    				tabla += "    			</ul>\n";
    				tabla += "			</li>\n";
    				tabla += "		</ul>\n";
    				tabla += "	</div>\n";
    			}else if(columnas[x][1].equals("SEL")){
        				tabla += " <input type=\"button\" class=\"botton\" value=\"Seleccionar\" onclick=\"filaSeleccion('"+registro.getId(0)+"')\" />";
    			}else{
        				tabla += registro.getId(Integer.parseInt(columnas[x][1]));
        	    }	
    			
    			tabla +="	</td>";// Sierre de Columna
			}
    		tabla +="	</tr>";// Sierre de fila
    		count ++;
    		if(count == cantidadFilas){
    			break;
    		}
		}
    	tabla +="	</tbody><table>";// sierre de la tabala 
    	return tabla;
    }
    public static String tablaCuentaHabDes(List<TablaAutomatica> lisHab,String[][] columnas,boolean dobleClickTr ){
    	String tabla ="";
    	// generacion de cabesera de la tabla 
    	tabla +="<table id='tabla' style='width: 100%;'>";// apertura de la tabla
    	tabla +=	"<thead>";// apertura de cabesera
    	tabla +=		"<tr>";// apertura de fila
    	for (int i = 0; i < columnas.length; i++) {
    		tabla +=	"<th>"+columnas[i][0]+"</th>";//columna
		}
    	tabla +=		"</tr>";// sierre de fila 
    	tabla +=	"</thead>";// Sierre de cabesera
    	tabla +=	"<tbody>";// Sierre de cuerpo
    	for (TablaAutomatica registro : lisHab) {
    		
    		tabla +=	"<tr ondblclick=\"filaSeleccion('"+registro.getId(0)+"')\">";//  Fila
    		for (int x = 0; x < columnas.length; x++) {
    			tabla +=	"<td>";//  Columna
    			if(columnas[x][1].equals("Det")){
    				tabla += 	"<div id='menu_row' style='width: 100%;'>";
    				tabla += 		"<ul style='float: right;'>";
    				tabla += 			"<li>";
    				tabla += 				"<a href='#' onclick='return false;' class='nivel'></a>";
    				tabla += 				"<ul>\n";
    				tabla += 					"<li>\n";
    				tabla += 						"<a href=\"#\" onclick=\"getId(\""+registro.getId(0)+"\"); \" >Ver Detalle</a>\n";
    				tabla += 					"</li>\n";
    				tabla += 				"</ul>\n";
    				tabla += 			"</li>\n";
    				tabla += 		"</ul>\n";
    				tabla += 	"</div>\n";
    			}else if(columnas[x][1].equals("SEL")){
    				//tabla += "<input class=\"botton\"  type=\"button\" value=\"Seleccionar\" onclick=\"filaSeleccion('"+registro.getId(0)+"')\" />";
    			}else{
    				tabla +=registro.getId(Integer.parseInt(columnas[x][1]));
    			}
    			tabla +="</td>";// Sierre de Columna
			}
    		tabla +="</tr>";// Sierre de fila
		}
    	tabla +="</tbody></table>";// sierre de la tabala 
    	return tabla;
    }
    public static String tablaCuentaHabDes(List<TablaAutomatica> lisHab ,String[][] columnas ,int cantidadFilas,boolean dobleClickTr  ){
    	
    	int count = 0;
    	// generacion de cabesera de la tabla 
    	String tabla ="<table id='tabla' style='width: 100%;'><thead><tr>";// apertura de fila
    	for (int i = 0; i < columnas.length; i++) {
    		tabla +="		<th>"+columnas[i][0]+"</th>";
    	}
    	tabla +="		</tr></thead><tbody>";// Sierre de cuerpo
    	for (TablaAutomatica registro : lisHab) {
    		tabla +="<tr ondblclick=\"filaSeleccion('"+registro.getId(0)+"')\">";// Sierre de Fila
    		for (int x = 0; x < columnas.length; x++) {
    			tabla +="	<td>";// Sierre de Columna
    			if(columnas[x][1].equals("Det")){
    				tabla += "	<div id='menu_row' style='width: 100%; float left;'>\n";
    				tabla += "		<ul style='float: right;'>\n";
    				tabla += "			<li>\n";
    				tabla += "				<a href='#' onclick='return false;' class='nivel'>\n";
    				tabla += "				</a>\n";
    				tabla += "				<ul>\n";
    				tabla += "					<li>\n";
    				tabla += "						<a href=\"#\" onclick=\"getId('"+registro.getId(0)+"');\" >Ver Detalle</a>\n";
    				tabla += "					</li>\n";
    				tabla += "    			</ul>\n";
    				tabla += "			</li>\n";
    				tabla += "		</ul>\n";
    				tabla += "	</div>\n";
    			}else if(columnas[x][1].equals("SEL")){
        				tabla += " <input type=\"button\" class=\"botton\" value=\"Seleccionar\" onclick=\"filaSeleccion('"+registro.getId(0)+"')\" />";
    			}else{
        				tabla += registro.getId(Integer.parseInt(columnas[x][1]));
        	    }	
    			
    			tabla +="	</td>";// Sierre de Columna
			}
    		tabla +="	</tr>";// Sierre de fila
    		count ++;
    		if(count == cantidadFilas){
    			break;
    		}
		}
    	tabla +="	</tbody><table>";// sierre de la tabala 
    	return tabla;
    }*/
}
