package cl.utfsm.rrhhG.modulo;

public class TipoProceso {
	
	private int codProceso;
	private String nomProceso;
	
	public TipoProceso (){
			
	}
	public TipoProceso (TipoProceso tipPro){
		this.setCodProceso(tipPro.getCodProceso());
		this.setNomProceso(tipPro.getNomProceso());
	}
	public int getCodProceso() {
		return codProceso;
	}

	public void setCodProceso(int codProceso) {
		this.codProceso = codProceso;
	}

	public String getNomProceso() {
		return nomProceso;
	}

	public void setNomProceso(String nomProceso) {
		this.nomProceso = nomProceso;
	}
	
	
	
}
