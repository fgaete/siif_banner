package cl.utfsm.rrhhG.modulo;

public class IseriesApref18 {

	private int anoPro;
	private int mesPro;
	private int rutide;
	private String digide;
	private int tipCon;
	private int numCom;
	private int codHde;
	private int valHde;
	private int codUni;
	private int valApl;
	private int valPes;
	private String desHde;
	private int insApv;
	private String codOrg;
	
	public IseriesApref18(){
		
	}

	public int getAnoPro() {
		return anoPro;
	}

	public void setAnoPro(int anoPro) {
		this.anoPro = anoPro;
	}

	public int getMesPro() {
		return mesPro;
	}

	public void setMesPro(int mesPro) {
		this.mesPro = mesPro;
	}

	public int getRutide() {
		return rutide;
	}

	public void setRutide(int rutide) {
		this.rutide = rutide;
	}

	public String getDigide() {
		return digide;
	}

	public void setDigide(String digide) {
		this.digide = digide;
	}

	public int getTipCon() {
		return tipCon;
	}

	public void setTipCon(int tipCon) {
		this.tipCon = tipCon;
	}

	public int getNumCom() {
		return numCom;
	}

	public void setNumCom(int numCom) {
		this.numCom = numCom;
	}

	public int getCodHde() {
		return codHde;
	}

	public void setCodHde(int codHde) {
		this.codHde = codHde;
	}

	public int getValHde() {
		return valHde;
	}

	public void setValHde(int valHde) {
		this.valHde = valHde;
	}

	public int getCodUni() {
		return codUni;
	}

	public void setCodUni(int codUni) {
		this.codUni = codUni;
	}

	public int getValApl() {
		return valApl;
	}

	public void setValApl(int valApl) {
		this.valApl = valApl;
	}

	public int getValPes() {
		return valPes;
	}

	public void setValPes(int valPes) {
		this.valPes = valPes;
	}

	public String getDesHde() {
		return desHde;
	}

	public void setDesHde(String desHde) {
		this.desHde = desHde;
	}

	public int getInsApv() {
		return insApv;
	}

	public void setInsApv(int insApv) {
		this.insApv = insApv;
	}

	public String getCodOrg() {
		return codOrg;
	}

	public void setCodOrg(String codOrg) {
		this.codOrg = codOrg;
	}
	
	
	
}
