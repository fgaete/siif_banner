package cl.utfsm.rrhhG.modulo;

public class IseriesApref02 {
	
	private int anoPro;
	private int mesPro;
	private String codPra;
	
	public IseriesApref02(){
		
	}

	public int getAnoPro() {
		return anoPro;
	}

	public void setAnoPro(int anoPro) {
		this.anoPro = anoPro;
	}

	public int getMesPro() {
		return mesPro;
	}

	public void setMesPro(int mesPro) {
		this.mesPro = mesPro;
	}

	public String getCodPra() {
		return codPra;
	}

	public void setCodPra(String codPra) {
		this.codPra = codPra;
	}
	
	
	
}
