package cl.utfsm.rrhhG.modulo;

public class IseriesAppef06 {
	
	private int numInt;
	private int rutide;
	private String digide;
	private String codAyu;
	private String codNiv;
	private int valHor;
	private int codUni;
	private int sucur;
	private int fecSol;
	private int fecIni;
	private int fecFin;
	private String nomTra;
	private int rutPro;
	private String digPro;
	private String proRes;
	private int canHor;
	private int valMes;
	private int valCon;
	private String estSol;
	private int tipCon;
	private int semest;
	private int anoAyu;
	private String asiAlf1;
	private int asiNum1;
	private String asiAlf2;
	private int asinum2;
	private String asiAlf3;
	private int asiNum3;
	private String prin01;
	private String prin02;
	private String prin03;
	private String idDigi;
	private int fecIng;
	private int feeFin;
	private int feeFfi;
	private int rutPro2;
	private String digPro2;
	private int rutPro3;
	private String digPro3;
	private String parale1;
	private String parale2;
	private String parale3;
	private String codOrg;
	
	public IseriesAppef06() {
	
	}
	public int getNumInt() {
		return numInt;
	}
	public void setNumInt(int numInt) {
		this.numInt = numInt;
	}
	public int getRutide() {
		return rutide;
	}
	public void setRutide(int rutide) {
		this.rutide = rutide;
	}
	public String getDigide() {
		return digide;
	}
	public void setDigide(String digide) {
		this.digide = digide;
	}
	public String getCodAyu() {
		return codAyu;
	}
	public void setCodAyu(String codAyu) {
		this.codAyu = codAyu;
	}
	public String getCodNiv() {
		return codNiv;
	}
	public void setCodNiv(String codNiv) {
		this.codNiv = codNiv;
	}
	public int getValHor() {
		return valHor;
	}
	public void setValHor(int valHor) {
		this.valHor = valHor;
	}
	public int getCodUni() {
		return codUni;
	}
	public void setCodUni(int codUni) {
		this.codUni = codUni;
	}
	public int getSucur() {
		return sucur;
	}
	public void setSucur(int sucur) {
		this.sucur = sucur;
	}
	public int getFecSol() {
		return fecSol;
	}
	public void setFecSol(int fecSol) {
		this.fecSol = fecSol;
	}
	public int getFecIni() {
		return fecIni;
	}
	public void setFecIni(int fecIni) {
		this.fecIni = fecIni;
	}
	public int getFecFin() {
		return fecFin;
	}
	public void setFecFin(int fecFin) {
		this.fecFin = fecFin;
	}
	public String getNomTra() {
		return nomTra;
	}
	public void setNomTra(String nomTra) {
		this.nomTra = nomTra;
	}
	public int getRutPro() {
		return rutPro;
	}
	public void setRutPro(int rutPro) {
		this.rutPro = rutPro;
	}
	public String getDigPro() {
		return digPro;
	}
	public void setDigPro(String digPro) {
		this.digPro = digPro;
	}
	public String getProRes() {
		return proRes;
	}
	public void setProRes(String proRes) {
		this.proRes = proRes;
	}
	public int getCanHor() {
		return canHor;
	}
	public void setCanHor(int canHor) {
		this.canHor = canHor;
	}
	public int getValMes() {
		return valMes;
	}
	public void setValMes(int valMes) {
		this.valMes = valMes;
	}
	public int getValCon() {
		return valCon;
	}
	public void setValCon(int valCon) {
		this.valCon = valCon;
	}
	public String getEstSol() {
		return estSol;
	}
	public void setEstSol(String estSol) {
		this.estSol = estSol;
	}
	public int getTipCon() {
		return tipCon;
	}
	public void setTipCon(int tipCon) {
		this.tipCon = tipCon;
	}
	public int getSemest() {
		return semest;
	}
	public void setSemest(int semest) {
		this.semest = semest;
	}
	public int getAnoAyu() {
		return anoAyu;
	}
	public void setAnoAyu(int anoAyu) {
		this.anoAyu = anoAyu;
	}
	public String getAsiAlf1() {
		return asiAlf1;
	}
	public void setAsiAlf1(String asiAlf1) {
		this.asiAlf1 = asiAlf1;
	}
	public int getAsiNum1() {
		return asiNum1;
	}
	public void setAsiNum1(int asiNum1) {
		this.asiNum1 = asiNum1;
	}
	public String getAsiAlf2() {
		return asiAlf2;
	}
	public void setAsiAlf2(String asiAlf2) {
		this.asiAlf2 = asiAlf2;
	}
	public int getAsinum2() {
		return asinum2;
	}
	public void setAsinum2(int asinum2) {
		this.asinum2 = asinum2;
	}
	public String getAsiAlf3() {
		return asiAlf3;
	}
	public void setAsiAlf3(String asiAlf3) {
		this.asiAlf3 = asiAlf3;
	}
	public int getAsiNum3() {
		return asiNum3;
	}
	public void setAsiNum3(int asiNum3) {
		this.asiNum3 = asiNum3;
	}
	public String getPrin01() {
		return prin01;
	}
	public void setPrin01(String prin01) {
		this.prin01 = prin01;
	}
	public String getPrin02() {
		return prin02;
	}
	public void setPrin02(String prin02) {
		this.prin02 = prin02;
	}
	public String getPrin03() {
		return prin03;
	}
	public void setPrin03(String prin03) {
		this.prin03 = prin03;
	}
	public String getIdDigi() {
		return idDigi;
	}
	public void setIdDigi(String idDigi) {
		this.idDigi = idDigi;
	}
	public int getFecIng() {
		return fecIng;
	}
	public void setFecIng(int fecIng) {
		this.fecIng = fecIng;
	}
	public int getFeeFin() {
		return feeFin;
	}
	public void setFeeFin(int feeFin) {
		this.feeFin = feeFin;
	}
	public int getFeeFfi() {
		return feeFfi;
	}
	public void setFeeFfi(int feeFfi) {
		this.feeFfi = feeFfi;
	}
	public int getRutPro2() {
		return rutPro2;
	}
	public void setRutPro2(int rutPro2) {
		this.rutPro2 = rutPro2;
	}
	public String getDigPro2() {
		return digPro2;
	}
	public void setDigPro2(String digPro2) {
		this.digPro2 = digPro2;
	}
	public int getRutPro3() {
		return rutPro3;
	}
	public void setRutPro3(int rutPro3) {
		this.rutPro3 = rutPro3;
	}
	public String getDigPro3() {
		return digPro3;
	}
	public void setDigPro3(String digPro3) {
		this.digPro3 = digPro3;
	}
	public String getParale1() {
		return parale1;
	}
	public void setParale1(String parale1) {
		this.parale1 = parale1;
	}
	public String getParale2() {
		return parale2;
	}
	public void setParale2(String parale2) {
		this.parale2 = parale2;
	}
	public String getParale3() {
		return parale3;
	}
	public void setParale3(String parale3) {
		this.parale3 = parale3;
	}
	public String getCodOrg() {
		return codOrg;
	}
	public void setCodOrg(String codOrg) {
		this.codOrg = codOrg;
	}
	
	
	
	
}
