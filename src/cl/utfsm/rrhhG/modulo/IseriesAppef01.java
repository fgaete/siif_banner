package cl.utfsm.rrhhG.modulo;

import java.text.DecimalFormat;

public class IseriesAppef01 {

	private int rutide;
	private String digide;
	private String apePat;
	private String apeMat;
	private String nombre;
	private int estCiv;
	private int sexo;
	private int fecNac;
	private String lugNac;
	private String paiNac;
	private int nacion;
	private String detNac;
	private String domici;
	private int comuna;
	private int ciudad;
	private int region;
	private int codGra;
	private int graTit;
	private int codTit;
	private int fecIng;
	private String ingPor;
	private String telefo;
	private int rolCap;
	private String nivAca;
	private int semAnt;
	private String claInt;
	private String indAct;
	private int fecCam;
	private String celula;
	private String corele;
	private String domele;
	
	public IseriesAppef01() {
		
	}

	public int getRutide() {
		return rutide;
	}
	public String getRutideFormat() {
		DecimalFormat form = new DecimalFormat("###,###.##");
		
		return form.format(this.rutide);
	}

	public void setRutide(int rutide) {
		this.rutide = rutide;
	}

	public String getDigide() {
		return digide;
	}

	public void setDigide(String digide) {
		this.digide = digide;
	}

	public String getApePat() {
		return apePat;
	}

	public void setApePat(String apePat) {
		this.apePat = apePat;
	}

	public String getApeMat() {
		return apeMat;
	}

	public void setApeMat(String apeMat) {
		this.apeMat = apeMat;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEstCiv() {
		return estCiv;
	}

	public void setEstCiv(int estCiv) {
		this.estCiv = estCiv;
	}

	public int getSexo() {
		return sexo;
	}

	public void setSexo(int sexo) {
		this.sexo = sexo;
	}

	public int getFecNac() {
		return fecNac;
	}

	public void setFecNac(int fecNac) {
		this.fecNac = fecNac;
	}

	public String getLugNac() {
		return lugNac;
	}

	public void setLugNac(String lugNac) {
		this.lugNac = lugNac;
	}

	public String getPaiNac() {
		return paiNac;
	}

	public void setPaiNac(String paiNac) {
		this.paiNac = paiNac;
	}

	public int getNacion() {
		return nacion;
	}

	public void setNacion(int nacion) {
		this.nacion = nacion;
	}

	public String getDetNac() {
		return detNac;
	}

	public void setDetNac(String detNac) {
		this.detNac = detNac;
	}

	public String getDomici() {
		return domici;
	}

	public void setDomici(String domici) {
		this.domici = domici;
	}

	public int getComuna() {
		return comuna;
	}

	public void setComuna(int comuna) {
		this.comuna = comuna;
	}

	public int getCiudad() {
		return ciudad;
	}

	public void setCiudad(int ciudad) {
		this.ciudad = ciudad;
	}

	public int getRegion() {
		return region;
	}

	public void setRegion(int region) {
		this.region = region;
	}

	public int getCodGra() {
		return codGra;
	}

	public void setCodGra(int codGra) {
		this.codGra = codGra;
	}

	public int getGraTit() {
		return graTit;
	}

	public void setGraTit(int graTit) {
		this.graTit = graTit;
	}

	public int getCodTit() {
		return codTit;
	}

	public void setCodTit(int codTit) {
		this.codTit = codTit;
	}

	public int getFecIng() {
		return fecIng;
	}

	public void setFecIng(int fecIng) {
		this.fecIng = fecIng;
	}

	public String getIngPor() {
		return ingPor;
	}

	public void setIngPor(String ingPor) {
		this.ingPor = ingPor;
	}

	public String getTelefo() {
		return telefo;
	}

	public void setTelefo(String telefo) {
		this.telefo = telefo;
	}

	public int getRolCap() {
		return rolCap;
	}

	public void setRolCap(int rolCap) {
		this.rolCap = rolCap;
	}

	public String getNivAca() {
		return nivAca;
	}

	public void setNivAca(String nivAca) {
		this.nivAca = nivAca;
	}

	public int getSemAnt() {
		return semAnt;
	}

	public void setSemAnt(int semAnt) {
		this.semAnt = semAnt;
	}

	public String getClaInt() {
		return claInt;
	}

	public void setClaInt(String claInt) {
		this.claInt = claInt;
	}

	public String getIndAct() {
		return indAct;
	}

	public void setIndAct(String indAct) {
		this.indAct = indAct;
	}

	public int getFecCam() {
		return fecCam;
	}

	public void setFecCam(int fecCam) {
		this.fecCam = fecCam;
	}

	public String getCelula() {
		return celula;
	}

	public void setCelula(String celula) {
		this.celula = celula;
	}

	public String getCorele() {
		return corele;
	}

	public void setCorele(String corele) {
		this.corele = corele;
	}

	public String getDomele() {
		return domele;
	}

	public void setDomele(String domele) {
		this.domele = domele;
	}
	
	
	
}
