package cl.utfsm.rrhhG.modulo;

import java.sql.Timestamp;

public class ProcesoIncidencia {

	int regProIdEjecucion ;
	int codEtapaProceso;
	int incId;
	int codProceso	;
	String proIncLocalizacion;
	Timestamp proIncFechaRegistro;
	
	public ProcesoIncidencia() {
		
	}
	public int getRegProIdEjecucion() {
		return regProIdEjecucion;
	}
	public void setRegProIdEjecucion(int regProIdEjecucion) {
		this.regProIdEjecucion = regProIdEjecucion;
	}
	public int getCodEtapaProceso() {
		return codEtapaProceso;
	}
	public void setCodEtapaProceso(int codEtapaProceso) {
		this.codEtapaProceso = codEtapaProceso;
	}
	public int getIncId() {
		return incId;
	}
	public void setIncId(int incId) {
		this.incId = incId;
	}
	public int getCodProceso() {
		return codProceso;
	}
	public void setCodProceso(int codProceso) {
		this.codProceso = codProceso;
	}
	public String getProIncLocalizacion() {
		return proIncLocalizacion;
	}
	public void setProIncLocalizacion(String proIncLocalizacion) {
		this.proIncLocalizacion = proIncLocalizacion;
	}
	public Timestamp getProIncFechaRegistro() {
		return proIncFechaRegistro;
	}
	public void setProIncFechaRegistro(Timestamp proIncFechaRegistro) {
		this.proIncFechaRegistro = proIncFechaRegistro;
	}
	
	
	
	
}
