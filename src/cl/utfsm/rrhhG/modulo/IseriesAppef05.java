package cl.utfsm.rrhhG.modulo;

public class IseriesAppef05 {
	
	private int anoPro;
	private int mesPro;
	private int rutide;
	private String digide;
	private int tipCon;
	private int numCon;
	private int escFon;
	private int codUni;
	private int codPla;
	private int numNiv;
	private int codSuc;
	private int codCar;
	private int sumFij;
	private int jorNad;
	private int canHor; 
	private int fecIni;
	private int fecFin;
	private int fecAnt;
	private int fecTer;
	private int indCol;
	private int indMov; 
	private int codAFP;
	private int indJub;
	private int indSal;
	private int valAho; 
	private String uniAho;
	private int codIsa;
	private String uniIsa;
	private int valIsa;
	private int antQui;
	private int indDep;
	private int serMed;
	private int cuoMor;
	private int sindi1;
	private int sindi2;
	private int sindi3;
	private String tipJer; 
	private int horIni;
	private int horFin;
	private int resolu;
	private int fecRes;
	private String impCon;
	private int fecIng;
	private int fecMod;
	private String ingPor;
	private String serPr1;
	private String serPr2;
	private int segCes;
	private int indBon;
	private int mTipo;
	private int mRazon;
	private String motivo;
	private int valeSuper;
	private int codBan;
	private int sucBan;
	private String cueBan;
	private int mRetencion;
	private int fecCon;
	private int fecPas;
	private String uniAug;
	private int valAug;
	private int fecInt;
	private int numNeg;
	private String codOrg;
	private String tipPue;
	private String codPue;
	private String baner1;
	private String baner2;
	private String baner3;
	
	public IseriesAppef05() {
			
	}

	public int getAnoPro() {
		return anoPro;
	}

	public void setAnoPro(int anoPro) {
		this.anoPro = anoPro;
	}

	public int getMesPro() {
		return mesPro;
	}

	public void setMesPro(int mesPro) {
		this.mesPro = mesPro;
	}

	public int getRutide() {
		return rutide;
	}

	public void setRutide(int rutide) {
		this.rutide = rutide;
	}

	public String getDigide() {
		return digide;
	}

	public void setDigide(String digide) {
		this.digide = digide;
	}

	public int getTipCon() {
		return tipCon;
	}

	public void setTipCon(int tipCon) {
		this.tipCon = tipCon;
	}

	public int getNumCon() {
		return numCon;
	}

	public void setNumCon(int numCon) {
		this.numCon = numCon;
	}

	public int getEscFon() {
		return escFon;
	}

	public void setEscFon(int escFon) {
		this.escFon = escFon;
	}

	public int getCodUni() {
		return codUni;
	}

	public void setCodUni(int codUni) {
		this.codUni = codUni;
	}

	public int getCodPla() {
		return codPla;
	}

	public void setCodPla(int codPla) {
		this.codPla = codPla;
	}

	public int getNumNiv() {
		return numNiv;
	}

	public void setNumNiv(int numNiv) {
		this.numNiv = numNiv;
	}

	public int getCodSuc() {
		return codSuc;
	}

	public void setCodSuc(int codSuc) {
		this.codSuc = codSuc;
	}

	public int getCodCar() {
		return codCar;
	}

	public void setCodCar(int codCar) {
		this.codCar = codCar;
	}

	public int getSumFij() {
		return sumFij;
	}

	public void setSumFij(int sumFij) {
		this.sumFij = sumFij;
	}

	public int getJorNad() {
		return jorNad;
	}

	public void setJorNad(int jorNad) {
		this.jorNad = jorNad;
	}

	public int getCanHor() {
		return canHor;
	}

	public void setCanHor(int canHor) {
		this.canHor = canHor;
	}

	public int getFecIni() {
		return fecIni;
	}

	public void setFecIni(int fecIni) {
		this.fecIni = fecIni;
	}

	public int getFecFin() {
		return fecFin;
	}

	public void setFecFin(int fecFin) {
		this.fecFin = fecFin;
	}

	public int getFecAnt() {
		return fecAnt;
	}

	public void setFecAnt(int fecAnt) {
		this.fecAnt = fecAnt;
	}

	public int getFecTer() {
		return fecTer;
	}

	public void setFecTer(int fecTer) {
		this.fecTer = fecTer;
	}

	public int getIndCol() {
		return indCol;
	}

	public void setIndCol(int indCol) {
		this.indCol = indCol;
	}

	public int getIndMov() {
		return indMov;
	}

	public void setIndMov(int indMov) {
		this.indMov = indMov;
	}

	public int getCodAFP() {
		return codAFP;
	}

	public void setCodAFP(int codAFP) {
		this.codAFP = codAFP;
	}

	public int getIndJub() {
		return indJub;
	}

	public void setIndJub(int indJub) {
		this.indJub = indJub;
	}

	public int getIndSal() {
		return indSal;
	}

	public void setIndSal(int indSal) {
		this.indSal = indSal;
	}

	public int getValAho() {
		return valAho;
	}

	public void setValAho(int valAho) {
		this.valAho = valAho;
	}

	public String getUniAho() {
		return uniAho;
	}

	public void setUniAho(String uniAho) {
		this.uniAho = uniAho;
	}

	public int getCodIsa() {
		return codIsa;
	}

	public void setCodIsa(int codIsa) {
		this.codIsa = codIsa;
	}

	public String getUniIsa() {
		return uniIsa;
	}

	public void setUniIsa(String uniIsa) {
		this.uniIsa = uniIsa;
	}

	public int getValIsa() {
		return valIsa;
	}

	public void setValIsa(int valIsa) {
		this.valIsa = valIsa;
	}

	public int getAntQui() {
		return antQui;
	}

	public void setAntQui(int antQui) {
		this.antQui = antQui;
	}

	public int getIndDep() {
		return indDep;
	}

	public void setIndDep(int indDep) {
		this.indDep = indDep;
	}

	public int getSerMed() {
		return serMed;
	}

	public void setSerMed(int serMed) {
		this.serMed = serMed;
	}

	public int getCuoMor() {
		return cuoMor;
	}

	public void setCuoMor(int cuoMor) {
		this.cuoMor = cuoMor;
	}

	public int getSindi1() {
		return sindi1;
	}

	public void setSindi1(int sindi1) {
		this.sindi1 = sindi1;
	}

	public int getSindi2() {
		return sindi2;
	}

	public void setSindi2(int sindi2) {
		this.sindi2 = sindi2;
	}

	public int getSindi3() {
		return sindi3;
	}

	public void setSindi3(int sindi3) {
		this.sindi3 = sindi3;
	}

	public String getTipJer() {
		return tipJer;
	}

	public void setTipJer(String tipJer) {
		this.tipJer = tipJer;
	}

	public int getHorIni() {
		return horIni;
	}

	public void setHorIni(int horIni) {
		this.horIni = horIni;
	}

	public int getHorFin() {
		return horFin;
	}

	public void setHorFin(int horFin) {
		this.horFin = horFin;
	}

	public int getResolu() {
		return resolu;
	}

	public void setResolu(int resolu) {
		this.resolu = resolu;
	}

	public int getFecRes() {
		return fecRes;
	}

	public void setFecRes(int fecRes) {
		this.fecRes = fecRes;
	}

	public String getImpCon() {
		return impCon;
	}

	public void setImpCon(String impCon) {
		this.impCon = impCon;
	}

	public int getFecIng() {
		return fecIng;
	}

	public void setFecIng(int fecIng) {
		this.fecIng = fecIng;
	}

	public int getFecMod() {
		return fecMod;
	}

	public void setFecMod(int fecMod) {
		this.fecMod = fecMod;
	}

	public String getIngPor() {
		return ingPor;
	}

	public void setIngPor(String ingPor) {
		this.ingPor = ingPor;
	}

	public String getSerPr1() {
		return serPr1;
	}

	public void setSerPr1(String serPr1) {
		this.serPr1 = serPr1;
	}

	public String getSerPr2() {
		return serPr2;
	}

	public void setSerPr2(String serPr2) {
		this.serPr2 = serPr2;
	}

	public int getSegCes() {
		return segCes;
	}

	public void setSegCes(int segCes) {
		this.segCes = segCes;
	}

	public int getIndBon() {
		return indBon;
	}

	public void setIndBon(int indBon) {
		this.indBon = indBon;
	}

	public int getMTipo() {
		return mTipo;
	}

	public void setMTipo(int tipo) {
		mTipo = tipo;
	}

	public int getMRazon() {
		return mRazon;
	}

	public void setMRazon(int razon) {
		mRazon = razon;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public int getValeSuper() {
		return valeSuper;
	}

	public void setValeSuper(int valeSuper) {
		this.valeSuper = valeSuper;
	}

	public int getCodBan() {
		return codBan;
	}

	public void setCodBan(int codBan) {
		this.codBan = codBan;
	}

	public int getSucBan() {
		return sucBan;
	}

	public void setSucBan(int sucBan) {
		this.sucBan = sucBan;
	}

	public String getCueBan() {
		return cueBan;
	}

	public void setCueBan(String cueBan) {
		this.cueBan = cueBan;
	}

	public int getMRetencion() {
		return mRetencion;
	}

	public void setMRetencion(int retencion) {
		mRetencion = retencion;
	}

	public int getFecCon() {
		return fecCon;
	}

	public void setFecCon(int fecCon) {
		this.fecCon = fecCon;
	}

	public int getFecPas() {
		return fecPas;
	}

	public void setFecPas(int fecPas) {
		this.fecPas = fecPas;
	}

	public String getUniAug() {
		return uniAug;
	}

	public void setUniAug(String uniAug) {
		this.uniAug = uniAug;
	}

	public int getValAug() {
		return valAug;
	}

	public void setValAug(int valAug) {
		this.valAug = valAug;
	}

	public int getFecInt() {
		return fecInt;
	}

	public void setFecInt(int fecInt) {
		this.fecInt = fecInt;
	}

	public int getNumNeg() {
		return numNeg;
	}

	public void setNumNeg(int numNeg) {
		this.numNeg = numNeg;
	}

	public String getCodOrg() {
		return codOrg;
	}

	public void setCodOrg(String codOrg) {
		this.codOrg = codOrg;
	}

	public String getTipPue() {
		return tipPue;
	}

	public void setTipPue(String tipPue) {
		this.tipPue = tipPue;
	}

	public String getCodPue() {
		return codPue;
	}

	public void setCodPue(String codPue) {
		this.codPue = codPue;
	}

	public String getBaner1() {
		return baner1;
	}

	public void setBaner1(String baner1) {
		this.baner1 = baner1;
	}

	public String getBaner2() {
		return baner2;
	}

	public void setBaner2(String baner2) {
		this.baner2 = baner2;
	}

	public String getBaner3() {
		return baner3;
	}

	public void setBaner3(String baner3) {
		this.baner3 = baner3;
	}
	
	
	
	
}
