package cl.utfsm.rrhhG.modulo;
/**
 *Nombre classe: Planta
 *Reprenta la tabla: APREF07
 *biblioteca AS400: REMUNERABT
 *Base datos : AS400
 *Programado por: Yonis Vergara
 *@author yonis.vergara
 *Fecha Creacion:20-09-2017
 *Vercion: 1.0
 *Ultima Verci�n: 20-09-2017
 *Modificado por :Yonis Vergara
 *Razon de cambio:Creacion de la clase
 *Descripci�n: esta clase esta creada para representar la tabla  "APREF04" pero solo los campos
 *				CODPLA codigo de planta
 *				DESPLA Descripci�n de planta 
 */
public class Planta implements TablaAutomatica{
	//columna: CODPLA , id = 1;
	private String codigoPlanta;
	//columna: DESPLA , id = 2;
	private String descPlanta; 
	
	public Planta(){
		
	}
	
	public String getCodigoPlanta() {
		return codigoPlanta;
	}



	public void setCodigoPlanta(String codigoPlanta) {
		this.codigoPlanta = codigoPlanta;
	}



	public String getDescPlanta() {
		return descPlanta;
	}



	public void setDescPlanta(String descPlanta) {
		this.descPlanta = descPlanta;
	}



	@Override
	public String getId(int id) {
		String respuesta = "";
		switch (id) {
		case 0:
			respuesta = this.codigoPlanta+"";
			break;
		case 1:
			respuesta = this.codigoPlanta+"";
			break;
		case 2:
			respuesta = this.descPlanta;
			break;			

		default:
			respuesta = "id incorecto";
			break;
		}		
		return respuesta;
	}
}
