package cl.utfsm.rrhhG.modulo;

public class IseriesApref67 {

	private int mesPro;
	private int anoPro;
	private int rutide;
	private String digide;
	private String tipPro;
	private int valPag;
	private int numVal;
	private int fecAut;
	private int fecEnv;
	private int numDoc;
	
	public IseriesApref67(){
		
	}

	public int getMesPro() {
		return mesPro;
	}

	public void setMesPro(int mesPro) {
		this.mesPro = mesPro;
	}

	public int getAnoPro() {
		return anoPro;
	}

	public void setAnoPro(int anoPro) {
		this.anoPro = anoPro;
	}

	public int getRutide() {
		return rutide;
	}

	public void setRutide(int rutide) {
		this.rutide = rutide;
	}

	public String getDigide() {
		return digide;
	}

	public void setDigide(String digide) {
		this.digide = digide;
	}

	public String getTipPro() {
		return tipPro;
	}

	public void setTipPro(String tipPro) {
		this.tipPro = tipPro;
	}

	public int getValPag() {
		return valPag;
	}

	public void setValPag(int valPag) {
		this.valPag = valPag;
	}

	public int getNumVal() {
		return numVal;
	}

	public void setNumVal(int numVal) {
		this.numVal = numVal;
	}

	public int getFecAut() {
		return fecAut;
	}

	public void setFecAut(int fecAut) {
		this.fecAut = fecAut;
	}

	public int getFecEnv() {
		return fecEnv;
	}

	public void setFecEnv(int fecEnv) {
		this.fecEnv = fecEnv;
	}

	public int getNumDoc() {
		return numDoc;
	}

	public void setNumDoc(int numDoc) {
		this.numDoc = numDoc;
	}
	
}
