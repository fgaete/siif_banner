package cl.utfsm.rrhhG.modulo;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
	/**
	 *Nombre classe: UsmCuentaHabDes
	 *Reprenta la tabla: USM_CUENTA_HAB_DES
	 *Base datos : Banner
	 *Programado por: Yonis Vergara
	 *Fecha Creacion:30-08-2017
	 *Vercion: 1.0
	 *Ultima Verci�n: 30-08-2017
	 *Modificado por :Yonis Vergara
	 *Razon de cambio:Creacion de la clase
	 *Descripci�n: esta clase esta creada para representar la tabla  "USM_CUENTA_HAB_DES"
	 */
	// tabla USM_CUENTA_HAB_DES
public class UsmCuentaHabDes implements Comparable<UsmCuentaHabDes>,TablaAutomatica {
	
	// ROWID ID 0
	private String rowid;
	// columna:USMHABDES_CODHDE ID 1
	private int usmHabDesCodhde;
	// columna:USMHABDES_CODPLA ID 2
	private String usmHabDesCodpla;
	// columna:USMHABDES_NUMNIV ID 3
	private String usmHabDesNumniv;
	// columna:USMHABDES_CUENTA_BANNER ID 4
	private String usmHabDesCuentaBanner;
	// columna:USMHABDES_ORGN_BANNER ID 5
	private String usmHabDesOrgnBanner;
	// columna:USMHABDES_OBS ID 6
	private String usmHabDesObs;
	// columna:USMHABDES_COD_VIGENCIA ID 7
	private int usmHabDesCodVigencia;
	// columna:USMHABDES_FECH_INI ID 8
	private Date usmHabDesFechIni;
	// columna:USMHABDES_FECH_FIN ID 9
	private Date usmHabDesFechFin;
	// columna:USMHABDES_RUTIDE ID 10
	private int usmHabDesRutide;
	// columna:USMHABDES_PIDM ID 11
	private int usmHabDesPidm;
	
	public UsmCuentaHabDes (){
		
		
	}

	public int getUsmHabDesCodhde() {
		return usmHabDesCodhde;
	}

	public void setUsmHabDesCodhde(int usmHabDesCodhde) {
		this.usmHabDesCodhde = usmHabDesCodhde;
	}

	public String getUsmHabDesCodpla() {
		return usmHabDesCodpla;
	}

	public void setUsmHabDesCodpla(String usmHabDesCodpla) {
		this.usmHabDesCodpla = usmHabDesCodpla;
	}

	public String getUsmHabDesNumniv() {
		return usmHabDesNumniv;
	}

	public void setUsmHabDesNumniv(String usmHabDesNumniv) {
		this.usmHabDesNumniv = usmHabDesNumniv;
	}

	public String getUsmHabDesCuentaBanner() {
		return usmHabDesCuentaBanner;
	}

	public void setUsmHabDesCuentaBanner(String usmHabDesCuentaBanner) {
		this.usmHabDesCuentaBanner = usmHabDesCuentaBanner;
	}

	public String getUsmHabDesOrgnBanner() {
		return usmHabDesOrgnBanner;
	}

	public void setUsmHabDesOrgnBanner(String usmHabDesOrgnBanner) {
		this.usmHabDesOrgnBanner = usmHabDesOrgnBanner;
	}

	public String getUsmHabDesObs() {
		return usmHabDesObs;
	}

	public void setUsmHabDesObs(String usmHabDesObs) {
		this.usmHabDesObs = usmHabDesObs;
	}

	public int getUsmHabDesCodVigencia() {
		return usmHabDesCodVigencia;
	}

	public void setUsmHabDesCodVigencia(int usmHabDesCodVigencia) {
		this.usmHabDesCodVigencia = usmHabDesCodVigencia;
	}

	public Date getUsmHabDesFechIni() {
		return usmHabDesFechIni;
	}

	public void setUsmHabDesFechIni(Date usmHabDesFechIni) {
		this.usmHabDesFechIni = usmHabDesFechIni;
	}

	public Date getUsmHabDesFechFin() {
		return usmHabDesFechFin;
	}

	public void setUsmHabDesFechFin(Date usmHabDesFechFin) {
		this.usmHabDesFechFin = usmHabDesFechFin;
	}

	public int getUsmHabDesRutide() {
		return usmHabDesRutide;
	}
	
	 public String getRowid() {
		return rowid;
	}

	public void setRowid(String rowid) {
		this.rowid = rowid;
	}

	/*
	 *Nombre metodo: getUsmHabDesRutideFormat
	 *Programado por: Yonis Vergara
	 *Fecha Creacion:30-08-2017
	 *Vercion: 1.0
	 *Ultima Verci�n: 30-08-2017
	 *Modificado por :Yonis Vergara
	 *Razon de cambio:generar metodo
	 *descripci�n: entregar el rut enformato con su digito verificador
	 *Opservaci�n:el rut deve ser string y contener el DV al final
	 */
	public String getUsmHabDesRutideFormat() {		
		
		return getRutFormat(this.usmHabDesRutide+this.calculoDV(this.usmHabDesRutide+""));
	}
	public void setUsmHabDesRutide(int usmHabDesRutide) {
		this.usmHabDesRutide = usmHabDesRutide;
	}
	
	public int getUsmHabDesPidm() {
		return usmHabDesPidm;
	}

	public void setUsmHabDesPidm(int usmHabDesPidm) {
		this.usmHabDesPidm = usmHabDesPidm;
	}
	 /*
	 *Nombre metodo: getRutFormat
	 *Programado por: Yonis Vergara
	 *Fecha Creacion:30-08-2017
	 *Vercion: 1.0
	 *Ultima Verci�n: 30-08-2017
	 *Modificado por :Yonis Vergara
	 *Razon de cambio:generacion de Metodo
	 *descripci�n: le da formato al rut  dejandolo en XX.XXX.XXX-X 
	 *Opservaci�n:el rut deve ser string y contener el DV al final
	 */
    private String getRutFormat(String rut) {
		
		int largoOriginal = rut.length();
		int ultimaPosision  = largoOriginal;
		String rutFormato =  rut.substring(0,largoOriginal-1)+"-"+rut.substring(largoOriginal-1);
		ultimaPosision --;
		
		while(ultimaPosision > 3){
			
			rutFormato = rutFormato.substring(0,ultimaPosision-3)+"."+rutFormato.substring(ultimaPosision-3);
			ultimaPosision -= 3;			
		}
		
		return rutFormato;
	}
    /*
	 *Nombre metodo: calculoDV
	 *Programado por: Yonis Vergara
	 *Fecha Creacion:30-08-2017
	 *Vercion: 1.0
	 *Ultima Verci�n: 30-08-2017
	 *Modificado por :Yonis Vergara
	 *Razon de cambio:Calculo del digito verificador del rut 
	 *
	 */
	private  String calculoDV(String vrut) 
    { 
        
        String rut = vrut.trim();        
        int cantidad = rut.length(); 
        int factor = 2; 
        int suma = 0; 
        String verificador = ""; 

        for(int i = cantidad; i > 0; i--) 
        { 
            if(factor > 7) 
            { 
                factor = 2; 
            } 
            suma += (Integer.parseInt(rut.substring((i-1), i)))*factor; 
            factor++; 
        } 
        verificador = String.valueOf(11 - suma%11); 
        
            if((verificador.equals("10")) ) 
            { 
            	verificador = "K"; 
            } 
             
            if((verificador.equals("11"))) 
            { 
            	verificador = "0"; 
            }                 
                   
        return verificador;         
    }	
	
	

	@Override
	public int compareTo(UsmCuentaHabDes o) {
		int byAge = Integer.valueOf(usmHabDesCodhde).compareTo(Integer.valueOf(o.getUsmHabDesCodhde()));
		if ( byAge != 0 ) {
            return byAge;
        }
		
		byAge =  this.usmHabDesCodpla.compareToIgnoreCase(o.getUsmHabDesCodpla());
		if (byAge != 0){
			return byAge;
		}
		
		byAge = this.usmHabDesNumniv.compareToIgnoreCase(o.getUsmHabDesNumniv());
		if (byAge != 0){
			return byAge;
		}
        
        
		return 0;
	}

	@Override
	public String getId(int id) {
		String respuesta = null;
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//formato por defecto
		switch (id) {
		case 0:
			return this.rowid;
		case 1:
			DecimalFormat twodigits = new DecimalFormat("000");
			respuesta = twodigits.format(this.usmHabDesCodhde);			
			break;
		case 2:
			respuesta = this.usmHabDesCodpla+"";
			break;
		case 3:
			respuesta = this.usmHabDesNumniv+"";
			break;
		case 4:
			respuesta = this.usmHabDesCuentaBanner;
			break;
		case 5:
			if(this.usmHabDesOrgnBanner == null){
				respuesta = "Oganizaci�n nomina";
			}else{
				respuesta = this.usmHabDesOrgnBanner;
			}
			
			break;
		case 6:
			respuesta = this.usmHabDesObs;
			break;
		case 7:
			respuesta = this.usmHabDesCodVigencia+"";
			break;
		case 8:		
			respuesta = sdfDate.format(this.usmHabDesFechIni);			
			break;
		case 9:			
			respuesta = sdfDate.format(this.usmHabDesFechFin);				
			break;
		case 10:
			respuesta = this.getUsmHabDesRutideFormat();	
			break;
		case 11:
			if(this.usmHabDesCodVigencia == 0){
				respuesta = "Emplador";
			}else{
				respuesta = "Empleado";				
			}
			break;
				
		default:
			respuesta = null;
		}	
		return respuesta;
	}
	
	
	
	
	
}
