package cl.utfsm.rrhhG.modulo;
/*
 *Nombre classe: Organizacion
 *Reprenta la tabla: FTVORGN solo 4 columnas de esta 
 *Programado por: Yonis Vergara
 *Fecha Creacion:30-08-2017
 *Vercion: 1.0
 *Ultima Verción: 30-08-2017
 *Modificado por :Yonis Vergara
 *Razon de cambio:Creacion de la clase
 *Descripción: Esta clase esta creada para representar la tabla  "FTVORGN" pero
 *					solo 4 columnas por razones de que banner maneja muchos indicadores
 */
// tabla FTVORGN solo 4 columnas de esta 
public class Organizacion implements Comparable<UsmCuentaHabDes>,TablaAutomatica {
	// columnas: FTVORGN_ORGN_CODE, codigo de la organización ,id 1
	private String codOrgn;
	// columnas: FTVORGN_TITLE, descripcion de la organizacion ,id 2
	private String descOreg; 
	// columnas: FTVORGN_FUND_CODE_DEF, fondo asosiada a esta organizacion ,id 3
	private String codFondo;
	// columnas: FTVORGN_PROG_CODE_DEF, programa asosiada a esta organizacion ,id 4
	private String codPrograma;
	public Organizacion() {
		
	}
	public String getCodOrgn() {
		return codOrgn;
	}
	public void setCodOrgn(String codOrgn) {
		this.codOrgn = codOrgn;
	}
	public String getDescOreg() {
		return descOreg;
	}
	public void setDescOreg(String descOreg) {
		this.descOreg = descOreg;
	}
	public String getCodFondo() {
		return codFondo;
	}
	public void setCodFondo(String codFondo) {
		this.codFondo = codFondo;
	}
	public String getCodPrograma() {
		return codPrograma;
	}
	public void setCodPrograma(String codPrograma) {
		this.codPrograma = codPrograma;
	}
	
	/*
	 *Nombre metodo: getPorId
	 *Programado por: Yonis Vergara
	 *Fecha Creacion:30-08-2017
	 *Vercion: 1.0
	 *Ultima Verción: 30-08-2017
	 *Modificado por :Yonis Vergara
	 *Razon de cambio:generar metodo
	 *descripción: entregar una variable depediendo por la id entregada
	 *Obserbación: esto es usado por el generador de tabla 
	 */
	
	@Override
	public int compareTo(UsmCuentaHabDes o) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public String getId(int id) {
		String respuesta;
		switch (id) {
		case 0:
			respuesta = this.getCodOrgn();
			break;
		case 1:
			respuesta = this.getCodOrgn();
			break;
		case 2:
			respuesta = this.getDescOreg();
			break;
		case 3:
			respuesta = this.getCodFondo();
			break;
		case 4:
			respuesta = this.getCodPrograma();
			break;
		default:
			respuesta = "id no valido Para la classe Organizacion";
			break;
		}
		
		return respuesta;
		
	}
	
	
}
