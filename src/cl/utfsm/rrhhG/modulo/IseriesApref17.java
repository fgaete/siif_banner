package cl.utfsm.rrhhG.modulo;

public class IseriesApref17 {

	private int mesPro;
	private int anoPro;
	private int rutide;
	private String digide;
	private int tipCon;
	private int numCon;
	private int codGra;
	private int codTit;
	private int codBan;
	private int sucBan;
	private String cueBan;
	private int escFon;
	private int codUni;
	private int codPla;
	private int numNiv;
	private int codSuc;
	private int codCar;
	private int sumFij;
	private int jornad;
	private int canHor;
	private int codAFP;
	private int indJub;
	private int codIsa;
	private int totImp;
	private int totTri;
	private int valImp;
	private int totLiq;
	private int afeExt;
	private int habOrd;
	private int numche;
	private int diatra;
	private int segCes;
	private int segTra;
	private int liqLiq;
	private int horExt;
	private int valExt;
	private int canCar;
	private int carRet;
	private int carDup;
	private int bonHij;
	private int totHab;
	private int totDes;
	private int totExe;
	private int totLeg;
	private int tDesva;
	private int apoPat;
	private int afeLso;
	private int afeImp;
	private int afeAPV;
	private int afeSeg;
	private int mTipo;
	private int mRazon;
	private int dilIpa;
	private int vaSuIn;
	private String codorg;
	
	public IseriesApref17() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getMesPro() {
		return mesPro;
	}

	public void setMesPro(int mesPro) {
		this.mesPro = mesPro;
	}

	public int getAnoPro() {
		return anoPro;
	}

	public void setAnoPro(int anoPro) {
		this.anoPro = anoPro;
	}

	public int getRutide() {
		return rutide;
	}

	public void setRutide(int rutide) {
		this.rutide = rutide;
	}

	public String getDigide() {
		return digide;
	}

	public void setDigide(String digide) {
		this.digide = digide;
	}

	public int getTipCon() {
		return tipCon;
	}

	public void setTipCon(int tipCon) {
		this.tipCon = tipCon;
	}

	public int getNumCon() {
		return numCon;
	}

	public void setNumCon(int numCon) {
		this.numCon = numCon;
	}

	public int getCodGra() {
		return codGra;
	}

	public void setCodGra(int codGra) {
		this.codGra = codGra;
	}

	public int getCodTit() {
		return codTit;
	}

	public void setCodTit(int codTit) {
		this.codTit = codTit;
	}

	public int getCodBan() {
		return codBan;
	}

	public void setCodBan(int codBan) {
		this.codBan = codBan;
	}

	public int getSucBan() {
		return sucBan;
	}

	public void setSucBan(int cucBan) {
		this.sucBan = cucBan;
	}

	public String getCueBan() {
		return cueBan;
	}

	public void setCueBan(String curBan) {
		this.cueBan = curBan;
	}

	public int getEscFon() {
		return escFon;
	}

	public void setEscFon(int escFon) {
		this.escFon = escFon;
	}

	public int getCodUni() {
		return codUni;
	}

	public void setCodUni(int codUni) {
		this.codUni = codUni;
	}

	public int getCodPla() {
		return codPla;
	}

	public void setCodPla(int codPla) {
		this.codPla = codPla;
	}

	public int getNumNiv() {
		return numNiv;
	}

	public void setNumNiv(int numNiv) {
		this.numNiv = numNiv;
	}

	public int getCodSuc() {
		return codSuc;
	}

	public void setCodSuc(int codSuc) {
		this.codSuc = codSuc;
	}

	public int getCodCar() {
		return codCar;
	}

	public void setCodCar(int codCar) {
		this.codCar = codCar;
	}

	public int getSumFij() {
		return sumFij;
	}

	public void setSumFij(int sumFij) {
		this.sumFij = sumFij;
	}

	public int getJornad() {
		return jornad;
	}

	public void setJornad(int jornad) {
		this.jornad = jornad;
	}

	public int getCanHor() {
		return canHor;
	}

	public void setCanHor(int canHor) {
		this.canHor = canHor;
	}

	public int getCodAFP() {
		return codAFP;
	}

	public void setCodAFP(int codAFP) {
		this.codAFP = codAFP;
	}

	public int getIndJub() {
		return indJub;
	}

	public void setIndJub(int indJub) {
		this.indJub = indJub;
	}

	public int getCodIsa() {
		return codIsa;
	}

	public void setCodIsa(int codIsa) {
		this.codIsa = codIsa;
	}

	public int getTotImp() {
		return totImp;
	}

	public void setTotImp(int totImp) {
		this.totImp = totImp;
	}

	public int getTotTri() {
		return totTri;
	}

	public void setTotTri(int totTri) {
		this.totTri = totTri;
	}

	public int getValImp() {
		return valImp;
	}

	public void setValImp(int valImp) {
		this.valImp = valImp;
	}

	public int getTotLiq() {
		return totLiq;
	}

	public void setTotLiq(int totLiq) {
		this.totLiq = totLiq;
	}

	public int getAfeExt() {
		return afeExt;
	}

	public void setAfeExt(int afeExt) {
		this.afeExt = afeExt;
	}

	public int getHabOrd() {
		return habOrd;
	}

	public void setHabOrd(int habOrd) {
		this.habOrd = habOrd;
	}

	public int getNumche() {
		return numche;
	}

	public void setNumche(int numche) {
		this.numche = numche;
	}

	public int getDiatra() {
		return diatra;
	}

	public void setDiatra(int diatra) {
		this.diatra = diatra;
	}

	public int getSegCes() {
		return segCes;
	}

	public void setSegCes(int segCes) {
		this.segCes = segCes;
	}

	public int getSegTra() {
		return segTra;
	}

	public void setSegTra(int segTra) {
		this.segTra = segTra;
	}

	public int getLiqLiq() {
		return liqLiq;
	}

	public void setLiqLiq(int liqLiq) {
		this.liqLiq = liqLiq;
	}

	public int getHorExt() {
		return horExt;
	}

	public void setHorExt(int horExt) {
		this.horExt = horExt;
	}

	public int getValExt() {
		return valExt;
	}

	public void setValExt(int valExt) {
		this.valExt = valExt;
	}

	public int getCanCar() {
		return canCar;
	}

	public void setCanCar(int canCar) {
		this.canCar = canCar;
	}

	public int getCarRet() {
		return carRet;
	}

	public void setCarRet(int carRet) {
		this.carRet = carRet;
	}

	public int getCarDup() {
		return carDup;
	}

	public void setCarDup(int carDup) {
		this.carDup = carDup;
	}

	public int getBonHij() {
		return bonHij;
	}

	public void setBonHij(int bonHij) {
		this.bonHij = bonHij;
	}

	public int getTotHab() {
		return totHab;
	}

	public void setTotHab(int totHab) {
		this.totHab = totHab;
	}

	public int getTotDes() {
		return totDes;
	}

	public void setTotDes(int totDes) {
		this.totDes = totDes;
	}

	public int getTotExe() {
		return totExe;
	}

	public void setTotExe(int totExe) {
		this.totExe = totExe;
	}

	public int getTotLeg() {
		return totLeg;
	}

	public void setTotLeg(int totLeg) {
		this.totLeg = totLeg;
	}

	public int getTDesva() {
		return tDesva;
	}

	public void setTDesva(int desva) {
		tDesva = desva;
	}

	public int getApoPat() {
		return apoPat;
	}

	public void setApoPat(int apoPat) {
		this.apoPat = apoPat;
	}

	public int getAfeLso() {
		return afeLso;
	}

	public void setAfeLso(int afeLso) {
		this.afeLso = afeLso;
	}

	public int getAfeImp() {
		return afeImp;
	}

	public void setAfeImp(int afeImp) {
		this.afeImp = afeImp;
	}

	public int getAfeAPV() {
		return afeAPV;
	}

	public void setAfeAPV(int afeAPV) {
		this.afeAPV = afeAPV;
	}

	public int getAfeSeg() {
		return afeSeg;
	}

	public void setAfeSeg(int afeSeg) {
		this.afeSeg = afeSeg;
	}

	public int getMTipo() {
		return mTipo;
	}

	public void setMTipo(int tipo) {
		mTipo = tipo;
	}

	public int getMRazon() {
		return mRazon;
	}

	public void setMRazon(int razon) {
		mRazon = razon;
	}

	public int getDilIpa() {
		return dilIpa;
	}

	public void setDilIpa(int dilIpa) {
		this.dilIpa = dilIpa;
	}

	public int getVaSuIn() {
		return vaSuIn;
	}

	public void setVaSuIn(int vaSuIn) {
		this.vaSuIn = vaSuIn;
	}

	public String getCodorg() {
		return codorg;
	}

	public void setCodorg(String codorg) {
		this.codorg = codorg;
	}
	
	
		
}
