package cl.utfsm.rrhhG.modulo;



public class PreGurfeed {

	
	
	private int pregurId;
	private RegistroProceso regPro;
	private int pregurValpes;
	private String pregurTipo;
	private String pregurFondo;
	private String pregurOrganizacion;
	private String pregurCuenta;
	private String pregurPrograma;
	private String pregurCodhde;
	private String pregurPidm;
	private String pregurRut;
	private String pregurCodpla;
	private String pregurNumniv;
	private int pregurCoduni;
	
	public PreGurfeed() {
		 
	}	
	
	public int getPregurCoduni() {
		return pregurCoduni;
	}

	public void setPregurCoduni(int pregurCoduni) {
		this.pregurCoduni = pregurCoduni;
	}

	public int getPregurId() {
		return pregurId;
	}


	public void setPregurId(int pregurId) {
		this.pregurId = pregurId;
	}


	


	public RegistroProceso getRegPro() {
		return regPro;
	}


	public void setRegPro(RegistroProceso regPro) {
		this.regPro = regPro;
	}


	public int getPregurValpes() {
		return pregurValpes;
	}


	public void setPregurValpes(int pregurValpes) {
		this.pregurValpes = pregurValpes;
	}


	public String getPregurTipo() {
		return pregurTipo;
	}


	public void setPregurTipo(String pregurTipo) {
		this.pregurTipo = pregurTipo;
	}


	public String getPregurFondo() {
		return pregurFondo;
	}


	public void setPregurFondo(String pregurFondo) {
		this.pregurFondo = pregurFondo;
	}


	public String getPregurOrganizacion() {
		return pregurOrganizacion;
	}


	public void setPregurOrganizacion(String pregurOrganizacion) {
		this.pregurOrganizacion = pregurOrganizacion;
	}


	public String getPregurCuenta() {
		return pregurCuenta;
	}


	public void setPregurCuenta(String pregurCuenta) {
		this.pregurCuenta = pregurCuenta;
	}


	public String getPregurPrograma() {
		return pregurPrograma;
	}


	public void setPregurPrograma(String pregurPrograma) {
		this.pregurPrograma = pregurPrograma;
	}


	public String getPregurCodhde() {
		return pregurCodhde;
	}


	public void setPregurCodhde(String pregurCodhde) {
		this.pregurCodhde = pregurCodhde;
	}


	public String getPregurPidm() {
		return pregurPidm;
	}


	public void setPregurPidm(String pregurPidm) {
		this.pregurPidm = pregurPidm;
	}


	public String getPregurRut() {
		return pregurRut;
	}


	public void setPregurRut(String pregurRut) {
		this.pregurRut = pregurRut;
	}


	public String getPregurCodpla() {
		return pregurCodpla;
	}


	public void setPregurCodpla(String pregurCodpla) {
		this.pregurCodpla = pregurCodpla;
	}


	public String getPregurNumniv() {
		return pregurNumniv;
	}


	public void setPregurNumniv(String pregurNumniv) {
		this.pregurNumniv = pregurNumniv;
	}	
	
	public String getRutFormato(){
		
		int largoOriginal = this.pregurRut.length();
		int ultimaPosision  = largoOriginal;
		String rutFormato =  this.pregurRut.substring(0,largoOriginal-1)+"-"+this.pregurRut.substring(largoOriginal-1);
		ultimaPosision --;
		
		while(ultimaPosision > 3){
			
			rutFormato = rutFormato.substring(0,ultimaPosision-3)+"."+rutFormato.substring(ultimaPosision-3);
			ultimaPosision -= 3;			
		}		
		return rutFormato;
	}
	
		
	
}
