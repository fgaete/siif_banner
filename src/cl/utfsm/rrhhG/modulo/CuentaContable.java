package cl.utfsm.rrhhG.modulo;
/**
 *Nombre classe: CuentaContable
 *Reprenta la tabla: FTVACCT
 *DB: banner
 *Programado por: Yonis Vergara
 *@author yonis.vergara
 *Fecha Creacion:22-09-2017
 *Vercion: 1.0
 *Ultima Verci�n: 22-09-2017
 *Creado por :Yonis Vergara
 *Razon de cambio:Creacion de la clase
 *Descripci�n: clase repensentativa de la tabla de FTVACCT pero dilimitando los campos a los nesesarios 
 *				Codigo de cuenta = FTVACCT_ACCT_CODE
 *				Descripci�n de cuenta = FTVACCT_ACCT_CODE
 */
public class CuentaContable implements TablaAutomatica {
	//columna representando: FTVACCT_ACCT_CODE , id = 1
	private String codigoCuenta;
	//columna representado: FTVACCT_ACCT_CODE , id = 2
	private String descCuenta;
	
	public CuentaContable(){
		
	}

	public String getCodigoCuenta() {
		return codigoCuenta;
	}

	public void setCodigoCuenta(String codigoCuenta) {
		this.codigoCuenta = codigoCuenta;
	}

	public String getDescCuenta() {
		return descCuenta;
	}

	public void setDescCuenta(String descCuenta) {
		this.descCuenta = descCuenta;
	}

	@Override
	public String getId(int id) {
		String respuesta = "";
		
		switch (id) {
		case 0:
			respuesta = this.codigoCuenta;
			break;
		case 1:
			respuesta = this.codigoCuenta;
			break;
		case 2:
			respuesta = this.descCuenta;
			break;

		default:
			respuesta = "ID no valido";
			break;
		}
		
		return respuesta;
	}
	
	
}
