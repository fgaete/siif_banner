package cl.utfsm.rrhhG.modulo;

import java.io.Serializable;
import java.util.Date;

public class GurfeedNomina implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String GurfeedSystemId;
	private String GurfeedSystemTimeStamp;
	private String GurfeedDocCode;
	private String GurfeedRecType;
	private Integer GurfeedSubmissionNumber;
	private Integer GurfeedItemNum;
	private Integer GurfeedSeqNum;
	private Date GurfeedActivityDate;
	private String GurfeedUserId;
	private String GurfeedRuclCode;
	private String GurfeedDocRefNum;
	private Date GurfeedTransDate;
	private Long GurfeedTransAmt;
	private String GurfeedTransDesc;
	private String GurfeedDrCrInd;
	private String GurfeedBankCode;
	private String GurfeedObudCode;
	private String GurfeedObphCode;
	private String GurfeedBudgDurCode;
	private String GurfeedCoasCode;
	private String GurfeedAcciCode;
	private String GurfeedFundCode;
	private String GurfeedOrgnCode;
	private String GurfeedAcctCode;
	private String GurfeedProgCode;
	private String GurfeedActvCode;
	private String GurfeedLocnCode;
	private String GurfeedDepNum;
	private String GurfeedEncdNum;
	private Integer GurfeedEncdItemNum;
	private Integer GurfeedEncdSeqNum;
	private String GurfeedEncdActionInd;
	private String GurfeedPrjdCode;
	private Integer GurfeedDistPct;
	private String GurfeedBudDispn;
	private String GurfeedCmtType;
	private Integer GurfeedCmtPct;
	private String GurfeedEncbType;
	private Integer GurfeedVendorPidm;
	private String GurfeedOneTimeVendCode;
	private String GurfeedBudgetPeriod;
	private String GurfeedAccrualInd;
	private String GurfeedAbalOverride;
	private String GurfeedId;
	private String GurfeedTranNumber;
	private String GurfeedDetailCode;
	private String GurfeedTermCode;
	private String GurfeedAccount;
	private String GurfeedSrceCode;
	private String GurfeedEffectiveDate;
	private String GurfeedPaytCode;
	private String GurfeedDetailCode2;
	private String GurfeedPaytCode2;
	private String GurfeedCurrCode;
	private Integer GurfeedForeignAmount;
	private String GurfeedPtotStatus;
	private String GurfeedLedcCode;
	
	public GurfeedNomina(){
		
		
	}

	public String getGurfeedSystemId() {
		return GurfeedSystemId;
	}

	public void setGurfeedSystemId(String gurfeedSystemId) {
		GurfeedSystemId = gurfeedSystemId;
	}

	public String getGurfeedSystemTimeStamp() {
		return GurfeedSystemTimeStamp;
	}

	public void setGurfeedSystemTimeStamp(String gurfeedSystemTimeStamp) {
		GurfeedSystemTimeStamp = gurfeedSystemTimeStamp;
	}

	public String getGurfeedDocCode() {
		return GurfeedDocCode;
	}

	public void setGurfeedDocCode(String gurfeedDocCode) {
		GurfeedDocCode = gurfeedDocCode;
	}

	public String getGurfeedRecType() {
		return GurfeedRecType;
	}

	public void setGurfeedRecType(String gurfeedRecType) {
		GurfeedRecType = gurfeedRecType;
	}

	public Integer getGurfeedSubmissionNumber() {
		return GurfeedSubmissionNumber;
	}

	public void setGurfeedSubmissionNumber(Integer gurfeedSubmissionNumber) {
		GurfeedSubmissionNumber = gurfeedSubmissionNumber;
	}

	public Integer getGurfeedItemNum() {
		return GurfeedItemNum;
	}

	public void setGurfeedItemNum(Integer gurfeedItemNum) {
		GurfeedItemNum = gurfeedItemNum;
	}

	public Integer getGurfeedSeqNum() {
		return GurfeedSeqNum;
	}

	public void setGurfeedSeqNum(Integer gurfeedSeqNum) {
		GurfeedSeqNum = gurfeedSeqNum;
	}

	public Date getGurfeedActivityDate() {
		return GurfeedActivityDate;
	}

	public void setGurfeedActivityDate(Date gurfeedActivityDate) {
		GurfeedActivityDate = gurfeedActivityDate;
	}

	public String getGurfeedUserId() {
		return GurfeedUserId;
	}

	public void setGurfeedUserId(String gurfeedUserId) {
		GurfeedUserId = gurfeedUserId;
	}

	public String getGurfeedRuclCode() {
		return GurfeedRuclCode;
	}

	public void setGurfeedRuclCode(String gurfeedRuclCode) {
		GurfeedRuclCode = gurfeedRuclCode;
	}

	public String getGurfeedDocRefNum() {
		return GurfeedDocRefNum;
	}

	public void setGurfeedDocRefNum(String gurfeedDocRefNum) {
		GurfeedDocRefNum = gurfeedDocRefNum;
	}

	public Date getGurfeedTransDate() {
		return GurfeedTransDate;
	}

	public void setGurfeedTransDate(Date gurfeedTransDate) {
		GurfeedTransDate = gurfeedTransDate;
	}

	public Long getGurfeedTransAmt() {
		return GurfeedTransAmt;
	}

	public void setGurfeedTransAmt(Long gurfeedTransAmt) {
		GurfeedTransAmt = gurfeedTransAmt;
	}

	public String getGurfeedTransDesc() {
		return GurfeedTransDesc;
	}

	public void setGurfeedTransDesc(String gurfeedTransDesc) {
		GurfeedTransDesc = gurfeedTransDesc;
	}

	public String getGurfeedDrCrInd() {
		return GurfeedDrCrInd;
	}

	public void setGurfeedDrCrInd(String gurfeedDrCrInd) {
		GurfeedDrCrInd = gurfeedDrCrInd;
	}

	public String getGurfeedBankCode() {
		return GurfeedBankCode;
	}

	public void setGurfeedBankCode(String gurfeedBankCode) {
		GurfeedBankCode = gurfeedBankCode;
	}

	public String getGurfeedObudCode() {
		return GurfeedObudCode;
	}

	public void setGurfeedObudCode(String gurfeedObudCode) {
		GurfeedObudCode = gurfeedObudCode;
	}

	public String getGurfeedObphCode() {
		return GurfeedObphCode;
	}

	public void setGurfeedObphCode(String gurfeedObphCode) {
		GurfeedObphCode = gurfeedObphCode;
	}

	public String getGurfeedBudgDurCode() {
		return GurfeedBudgDurCode;
	}

	public void setGurfeedBudgDurCode(String gurfeedBudgDurCode) {
		GurfeedBudgDurCode = gurfeedBudgDurCode;
	}

	public String getGurfeedCoasCode() {
		return GurfeedCoasCode;
	}

	public void setGurfeedCoasCode(String gurfeedCoasCode) {
		GurfeedCoasCode = gurfeedCoasCode;
	}

	public String getGurfeedAcciCode() {
		return GurfeedAcciCode;
	}

	public void setGurfeedAcciCode(String gurfeedAcciCode) {
		GurfeedAcciCode = gurfeedAcciCode;
	}

	public String getGurfeedFundCode() {
		return GurfeedFundCode;
	}

	public void setGurfeedFundCode(String gurfeedFundCode) {
		GurfeedFundCode = gurfeedFundCode;
	}

	public String getGurfeedOrgnCode() {
		return GurfeedOrgnCode;
	}

	public void setGurfeedOrgnCode(String gurfeedOrgnCode) {
		GurfeedOrgnCode = gurfeedOrgnCode;
	}

	public String getGurfeedAcctCode() {
		return GurfeedAcctCode;
	}

	public void setGurfeedAcctCode(String gurfeedAcctCode) {
		GurfeedAcctCode = gurfeedAcctCode;
	}

	public String getGurfeedProgCode() {
		return GurfeedProgCode;
	}

	public void setGurfeedProgCode(String gurfeedProgCode) {
		GurfeedProgCode = gurfeedProgCode;
	}

	public String getGurfeedActvCode() {
		return GurfeedActvCode;
	}

	public void setGurfeedActvCode(String gurfeedActvCode) {
		GurfeedActvCode = gurfeedActvCode;
	}

	public String getGurfeedLocnCode() {
		return GurfeedLocnCode;
	}

	public void setGurfeedLocnCode(String gurfeedLocnCode) {
		GurfeedLocnCode = gurfeedLocnCode;
	}

	public String getGurfeedDepNum() {
		return GurfeedDepNum;
	}

	public void setGurfeedDepNum(String gurfeedDepNum) {
		GurfeedDepNum = gurfeedDepNum;
	}

	public String getGurfeedEncdNum() {
		return GurfeedEncdNum;
	}

	public void setGurfeedEncdNum(String gurfeedEncdNum) {
		GurfeedEncdNum = gurfeedEncdNum;
	}

	public Integer getGurfeedEncdItemNum() {
		return GurfeedEncdItemNum;
	}

	public void setGurfeedEncdItemNum(Integer gurfeedEncdItemNum) {
		GurfeedEncdItemNum = gurfeedEncdItemNum;
	}

	public Integer getGurfeedEncdSeqNum() {
		return GurfeedEncdSeqNum;
	}

	public void setGurfeedEncdSeqNum(Integer gurfeedEncdSeqNum) {
		GurfeedEncdSeqNum = gurfeedEncdSeqNum;
	}

	public String getGurfeedEncdActionInd() {
		return GurfeedEncdActionInd;
	}

	public void setGurfeedEncdActionInd(String gurfeedEncdActionInd) {
		GurfeedEncdActionInd = gurfeedEncdActionInd;
	}

	public String getGurfeedPrjdCode() {
		return GurfeedPrjdCode;
	}

	public void setGurfeedPrjdCode(String gurfeedPrjdCode) {
		GurfeedPrjdCode = gurfeedPrjdCode;
	}

	public Integer getGurfeedDistPct() {
		return GurfeedDistPct;
	}

	public void setGurfeedDistPct(Integer gurfeedDistPct) {
		GurfeedDistPct = gurfeedDistPct;
	}

	public String getGurfeedBudDispn() {
		return GurfeedBudDispn;
	}

	public void setGurfeedBudDispn(String gurfeedBudDispn) {
		GurfeedBudDispn = gurfeedBudDispn;
	}

	public String getGurfeedCmtType() {
		return GurfeedCmtType;
	}

	public void setGurfeedCmtType(String gurfeedCmtType) {
		GurfeedCmtType = gurfeedCmtType;
	}

	public Integer getGurfeedCmtPct() {
		return GurfeedCmtPct;
	}

	public void setGurfeedCmtPct(Integer gurfeedCmtPct) {
		GurfeedCmtPct = gurfeedCmtPct;
	}

	public String getGurfeedEncbType() {
		return GurfeedEncbType;
	}

	public void setGurfeedEncbType(String gurfeedEncbType) {
		GurfeedEncbType = gurfeedEncbType;
	}

	public Integer getGurfeedVendorPidm() {
		return GurfeedVendorPidm;
	}

	public void setGurfeedVendorPidm(Integer gurfeedVendorPidm) {
		GurfeedVendorPidm = gurfeedVendorPidm;
	}

	public String getGurfeedOneTimeVendCode() {
		return GurfeedOneTimeVendCode;
	}

	public void setGurfeedOneTimeVendCode(String gurfeedOneTimeVendCode) {
		GurfeedOneTimeVendCode = gurfeedOneTimeVendCode;
	}

	public String getGurfeedBudgetPeriod() {
		return GurfeedBudgetPeriod;
	}

	public void setGurfeedBudgetPeriod(String gurfeedBudgetPeriod) {
		GurfeedBudgetPeriod = gurfeedBudgetPeriod;
	}

	public String getGurfeedAccrualInd() {
		return GurfeedAccrualInd;
	}

	public void setGurfeedAccrualInd(String gurfeedAccrualInd) {
		GurfeedAccrualInd = gurfeedAccrualInd;
	}

	public String getGurfeedAbalOverride() {
		return GurfeedAbalOverride;
	}

	public void setGurfeedAbalOverride(String gurfeedAbalOverride) {
		GurfeedAbalOverride = gurfeedAbalOverride;
	}

	public String getGurfeedId() {
		return GurfeedId;
	}

	public void setGurfeedId(String gurfeedId) {
		GurfeedId = gurfeedId;
	}

	public String getGurfeedTranNumber() {
		return GurfeedTranNumber;
	}

	public void setGurfeedTranNumber(String gurfeedTranNumber) {
		GurfeedTranNumber = gurfeedTranNumber;
	}

	public String getGurfeedDetailCode() {
		return GurfeedDetailCode;
	}

	public void setGurfeedDetailCode(String gurfeedDetailCode) {
		GurfeedDetailCode = gurfeedDetailCode;
	}

	public String getGurfeedTermCode() {
		return GurfeedTermCode;
	}

	public void setGurfeedTermCode(String gurfeedTermCode) {
		GurfeedTermCode = gurfeedTermCode;
	}

	public String getGurfeedAccount() {
		return GurfeedAccount;
	}

	public void setGurfeedAccount(String gurfeedAccount) {
		GurfeedAccount = gurfeedAccount;
	}

	public String getGurfeedSrceCode() {
		return GurfeedSrceCode;
	}

	public void setGurfeedSrceCode(String gurfeedSrceCode) {
		GurfeedSrceCode = gurfeedSrceCode;
	}

	public String getGurfeedEffectiveDate() {
		return GurfeedEffectiveDate;
	}

	public void setGurfeedEffectiveDate(String gurfeedEffectiveDate) {
		GurfeedEffectiveDate = gurfeedEffectiveDate;
	}

	public String getGurfeedPaytCode() {
		return GurfeedPaytCode;
	}

	public void setGurfeedPaytCode(String gurfeedPaytCode) {
		GurfeedPaytCode = gurfeedPaytCode;
	}

	public String getGurfeedDetailCode2() {
		return GurfeedDetailCode2;
	}

	public void setGurfeedDetailCode2(String gurfeedDetailCode2) {
		GurfeedDetailCode2 = gurfeedDetailCode2;
	}

	public String getGurfeedPaytCode2() {
		return GurfeedPaytCode2;
	}

	public void setGurfeedPaytCode2(String gurfeedPaytCode2) {
		GurfeedPaytCode2 = gurfeedPaytCode2;
	}

	public String getGurfeedCurrCode() {
		return GurfeedCurrCode;
	}

	public void setGurfeedCurrCode(String gurfeedCurrCode) {
		GurfeedCurrCode = gurfeedCurrCode;
	}

	public Integer getGurfeedForeignAmount() {
		return GurfeedForeignAmount;
	}

	public void setGurfeedForeignAmount(Integer gurfeedForeignAmount) {
		GurfeedForeignAmount = gurfeedForeignAmount;
	}

	public String getGurfeedPtotStatus() {
		return GurfeedPtotStatus;
	}

	public void setGurfeedPtotStatus(String gurfeedPtotStatus) {
		GurfeedPtotStatus = gurfeedPtotStatus;
	}

	public String getGurfeedLedcCode() {
		return GurfeedLedcCode;
	}

	public void setGurfeedLedcCode(String gurfeedLedcCode) {
		GurfeedLedcCode = gurfeedLedcCode;
	}
	
	
	
}
