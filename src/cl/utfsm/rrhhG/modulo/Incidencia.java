package cl.utfsm.rrhhG.modulo;

public class Incidencia {

 	private int     incId;	
	private String  incDescripcion;	
	private String  incDescripcionCorta;
	public Incidencia() {
		
	}
	
	public int getIncId() {
		return incId;
	}
	public void setIncId(int incId) {
		this.incId = incId;
	}
	public String getIncDescripcion() {
		return incDescripcion;
	}
	public void setIncDescripcion(String incDescripcion) {
		this.incDescripcion = incDescripcion;
	}
	public String getIncDescripcionCorta() {
		return incDescripcionCorta;
	}
	public void setIncDescripcionCorta(String incDescripcionCorta) {
		this.incDescripcionCorta = incDescripcionCorta;
	}
	
	
	
	
	
}
