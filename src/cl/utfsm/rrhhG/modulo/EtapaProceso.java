package cl.utfsm.rrhhG.modulo;

public class EtapaProceso {
	private int codEtapaProceso;
	private TipoProceso tipoProceso = new TipoProceso();
	private String nomEtapaProceso;
	private String desEtapaProceso;

	public EtapaProceso(){
		
	}
	
	public EtapaProceso(EtapaProceso etaPr){
		this.setNomEtapaProceso(etaPr.getNomEtapaProceso());
		this.setCodEtapaProceso(etaPr.getCodEtapaProceso());
		this.setTipoProceso(new TipoProceso(etaPr.getTipoProceso()));
		this.desEtapaProceso = etaPr.getDesEtapaProceso();
	}
	
	public String getDesEtapaProceso() {
		return desEtapaProceso;
	}

	public void setDesEtapaProceso(String desEtapaProceso) {
		this.desEtapaProceso = desEtapaProceso;
	}

	public int getCodEtapaProceso() {
		return codEtapaProceso;
	}

	public void setCodEtapaProceso(int codEtapaProceso) {
		this.codEtapaProceso = codEtapaProceso;
	}	

	public TipoProceso getTipoProceso() {
		return tipoProceso;
	}

	public void setTipoProceso(TipoProceso tipoProceso) {
		this.tipoProceso = tipoProceso;
	}

	public String getNomEtapaProceso() {
		return nomEtapaProceso;
	}

	public void setNomEtapaProceso(String nomEtapaProceso) {
		this.nomEtapaProceso = nomEtapaProceso;
	}	
}
