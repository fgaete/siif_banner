package cl.utfsm.rrhhG.modulo;

public class IseriesApref08 {
	
	private int codPla;
	private int codCar;
	private String desCar;
	private int nivGra;
	private String famCarCod;
	private String pueBan;
	
	public IseriesApref08() {
		
	}

	public int getCodPla() {
		return codPla;
	}

	public void setCodPla(int codPla) {
		this.codPla = codPla;
	}

	public int getCodCar() {
		return codCar;
	}

	public void setCodCar(int codCar) {
		this.codCar = codCar;
	}

	public String getDesCar() {
		return desCar;
	}

	public void setDesCar(String desCar) {
		this.desCar = desCar;
	}

	public int getNivGra() {
		return nivGra;
	}

	public void setNivGra(int nivGra) {
		this.nivGra = nivGra;
	}

	public String getFamCarCod() {
		return famCarCod;
	}

	public void setFamCarCod(String famCarCod) {
		this.famCarCod = famCarCod;
	}

	public String getPueBan() {
		return pueBan;
	}

	public void setPueBan(String pueBan) {
		this.pueBan = pueBan;
	}
	
	
	
	
}
