package cl.utfsm.rrhhG.modulo;
/**
 *Nombre classe: HaberDescuento
 *Reprenta la tabla: APREF04
 *biblioteca AS400: REMUNERABT
 *Base datos : AS00
 *Programado por: Yonis Vergara
 *@author yonis.vergara
 *Fecha Creacion:30-08-2017
 *Vercion: 1.0
 *Ultima Verci�n: 30-08-2017
 *Modificado por :Yonis Vergara
 *Razon de cambio:Creacion de la clase
 *Descripci�n: esta clase esta creada para representar la tabla  "APREF04" pero solo los campos
 *				CODHDE codigo de haber o descuento
 *				DESHDE Descripci�n del haber o descuento 
 */
public class HaberDescuento implements Comparable<HaberDescuento>,TablaAutomatica {
	// columna: CODHDE ,ID 1 
	private int codHabDes;
	// columna: DESHDE , ID 2	
	private String descHabDes;
	
	public HaberDescuento() {
		
	}

	public int getCodHabDes() {
		return codHabDes;
	}

	public void setCodHabDes(int codHabDes) {
		this.codHabDes = codHabDes;
	}

	public String getDescHabDes() {
		return descHabDes;
	}

	public void setDescHabDes(String descHabDes) {
		this.descHabDes = descHabDes;
	}

	@Override
	public int compareTo(HaberDescuento o) {

		int byAge = Integer.valueOf(this.codHabDes).compareTo(Integer.valueOf(o.getCodHabDes()));
		if ( byAge != 0 ) {
            return byAge;
        }	
		
		return 0;
	}

	@Override
	public String getId(int id) {
		String respuesta;
		
		switch (id) {
		case 0:
			respuesta = this.getCodHabDes()+"";
			break;
		case 1:
			respuesta = this.getCodHabDes()+"";
			break;
		case 2:
			respuesta = this.getDescHabDes();
			break;
		default:
			respuesta = "ID invalido";
			break;
		}
		
		return respuesta;
	}
	
	
	
}
