package cl.utfsm.rrhhG.modulo;

public class ResumenNomina {
	private int codsuc;
	private int planta;	
	private int count;
	private String descd;
	public ResumenNomina(){
		
	}
	public int getCodsuc() {
		return codsuc;
	}
	public void setCodsuc(int codsuc) {
		this.codsuc = codsuc;
	}
	public int getPlanta() {
		return planta;
	}
	public void setPlanta(int planta) {
		this.planta = planta;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getDescd() {
		return descd;
	}
	public void setDescd(String descd) {
		this.descd = descd;
	}
	
}
