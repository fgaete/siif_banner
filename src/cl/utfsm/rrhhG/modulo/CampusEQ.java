package cl.utfsm.rrhhG.modulo;

public class CampusEQ {
	
	private int stvCampCode;
	private String stvCampDesc;
	private int as400;
	
	public CampusEQ(){
		
	}

	public int getStvCampCode() {
		return stvCampCode;
	}

	public void setStvCampCode(int stvCampCode) {
		this.stvCampCode = stvCampCode;
	}

	public String getStvCampDesc() {
		return stvCampDesc;
	}

	public void setStvCampDesc(String stvCampDesc) {
		this.stvCampDesc = stvCampDesc;
	}

	public int getAs400() {
		return as400;
	}

	public void setAs400(int as400) {
		this.as400 = as400;
	}		
}
