package cl.utfsm.rrhhG.modulo;

public class IseriesPresf200 {

	private String codOrg;
	private String desOrg;
	private String indAut;
	private String codFon;
	private String codPro;
	
	public IseriesPresf200() {
		
	}

	public String getCodOrg() {
		return codOrg;
	}

	public void setCodOrg(String codOrg) {
		this.codOrg = codOrg;
	}

	public String getDesOrg() {
		return desOrg;
	}

	public void setDesOrg(String desOrg) {
		this.desOrg = desOrg;
	}

	public String getIndAut() {
		return indAut;
	}

	public void setIndAut(String indAut) {
		this.indAut = indAut;
	}

	public String getCodFon() {
		return codFon;
	}

	public void setCodFon(String codFon) {
		this.codFon = codFon;
	}

	public String getCodPro() {
		return codPro;
	}

	public void setCodPro(String codPro) {
		this.codPro = codPro;
	}
		
}
