package cl.utfsm.rrhhG.modulo;

public class ResumenPorCuenta {
			
	private String cuenta;
	private String desc;
	private long debe;
	private long haber;
	private long total;
	
	public ResumenPorCuenta(){
		
		
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}	
	
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public long getDebe() {
		return debe;
	}

	public void setDebe(long debe) {
		this.debe = debe;
	}

	public long getHaber() {
		return haber;
	}

	public void setHaber(long haber) {
		this.haber = haber;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}
	
}
