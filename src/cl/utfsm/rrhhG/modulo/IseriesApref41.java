package cl.utfsm.rrhhG.modulo;

public class IseriesApref41 {

	private int anoPro;
	private int mesPro;
	private int rutide;
	private String digide;
	private int tipCon;
	private int numCom;
	private int CodHde;
	private String uniCar;
	private String uniAbo;
	private int valCar;
	private int valAbo;
	private int canHor;
	private String nomIde;
	
	public IseriesApref41() {
		
	}

	public int getAnoPro() {
		return anoPro;
	}

	public void setAnoPro(int anoPro) {
		this.anoPro = anoPro;
	}

	public int getMesPro() {
		return mesPro;
	}

	public void setMesPro(int mesPro) {
		this.mesPro = mesPro;
	}

	public int getRutide() {
		return rutide;
	}

	public void setRutide(int rutide) {
		this.rutide = rutide;
	}

	public String getDigide() {
		return digide;
	}

	public void setDigide(String digide) {
		this.digide = digide;
	}

	public int getTipCon() {
		return tipCon;
	}

	public void setTipCon(int tipCon) {
		this.tipCon = tipCon;
	}

	public int getNumCom() {
		return numCom;
	}

	public void setNumCom(int numCom) {
		this.numCom = numCom;
	}

	public int getCodHde() {
		return CodHde;
	}

	public void setCodHde(int codHde) {
		CodHde = codHde;
	}

	public String getUniCar() {
		return uniCar;
	}

	public void setUniCar(String uniCar) {
		this.uniCar = uniCar;
	}

	public String getUniAbo() {
		return uniAbo;
	}

	public void setUniAbo(String uniAbo) {
		this.uniAbo = uniAbo;
	}

	public int getValCar() {
		return valCar;
	}

	public void setValCar(int valCar) {
		this.valCar = valCar;
	}

	public int getValAbo() {
		return valAbo;
	}

	public void setValAbo(int valAbo) {
		this.valAbo = valAbo;
	}

	public int getCanHor() {
		return canHor;
	}

	public void setCanHor(int canHor) {
		this.canHor = canHor;
	}

	public String getNomIde() {
		return nomIde;
	}

	public void setNomIde(String nomIde) {
		this.nomIde = nomIde;
	}
	
	
	
	
}
