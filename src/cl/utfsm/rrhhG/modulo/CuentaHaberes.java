package cl.utfsm.rrhhG.modulo;

public class CuentaHaberes implements TablaAutomatica,Comparable<CuentaHaberes>{
	private int codigo;
	private String planta;
	private String nivel;
	private String cuenta;
	private String organizacion;
	private String programa;
	private String fondo;
	
	public CuentaHaberes(){}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getPlanta() {
		return planta;
	}

	public void setPlanta(String planta) {
		this.planta = planta;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getOrganizacion() {
		return organizacion;
	}

	public void setOrganizacion(String organizacion) {
		this.organizacion = organizacion;
	}

	public String getPrograma() {
		return programa;
	}

	public void setPrograma(String programa) {
		this.programa = programa;
	}

	public String getFondo() {
		return fondo;
	}

	public void setFondo(String fondo) {
		this.fondo = fondo;
	}

	@Override
	public String getId(int id) {
		
		switch (id) {
		case 0:
			return this.codigo+"-"+this.planta+"-"+this.nivel;
			
		case 1:
			return String.format("%0" + 3 + "d", this.codigo);
		case 2:
			return ""+this.planta;	
		case 3:
			return ""+this.nivel;	
		case 4:
			return ""+this.cuenta;	
		case 5:
			return ""+this.organizacion;
		case 6:
			return ""+this.fondo;
		case 7:
			return ""+this.programa;
		default:
			return "";
		}
		
		
		
	}

	@Override
	public int compareTo(CuentaHaberes cuenta) {
		int byAge = Integer.valueOf(this.codigo).compareTo(Integer.valueOf(cuenta.getCodigo()));
		if ( byAge != 0 ) {
            return byAge;
        }
		
		byAge =  this.planta.compareToIgnoreCase(cuenta.getPlanta());
		if (byAge != 0){
			return byAge;
		}
		
		byAge = this.nivel.compareToIgnoreCase(cuenta.getNivel());
		if (byAge != 0){
			return byAge;
		}
		return 0;
	}
	
	
	
}
