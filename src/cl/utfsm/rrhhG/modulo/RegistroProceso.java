package cl.utfsm.rrhhG.modulo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RegistroProceso {
	
	private int    regproIdEjecucion;
	private int    codEtapaProceso;
	private int    codProceso;
	private EtapaProceso etapaPro = new EtapaProceso();
	private String regproRutResponsable;
	private String regProNombreResponsable;
	private Date   regProFechaRegistro;	
	private Date   regProFechaNomina;
	private String regProFinalizado;
	
	
	public RegistroProceso() {
	
	}
	
	public RegistroProceso(RegistroProceso regPr) {
		
		this.regproIdEjecucion = regPr.getRegproIdEjecucion();
		this.codEtapaProceso = regPr.getCodEtapaProceso();
		this.codProceso = regPr.getCodProceso();		
		this.etapaPro = new EtapaProceso(regPr.getEtapaPro());
		this.regproRutResponsable = regPr.getRegproRutResponsable();
		this.regProNombreResponsable = regPr.getRegProNombreResponsable();
		this.regProFechaRegistro = regPr.getRegProFechaRegistro();
		this.regProFechaNomina = regPr.getRegProFechaNomina();
		this.regProFinalizado = regPr.getRegProFinalizado();
	
	}
	
	public void cargarEtapa(){
		
		EtapaProceso etaPro = new EtapaProceso();
		etaPro.setCodEtapaProceso(this.getCodEtapaProceso());
		etaPro.setTipoProceso(new TipoProceso());
		etaPro.getTipoProceso().setCodProceso(this.getCodProceso());
		this.etapaPro = etaPro;
		
	}
	
	public EtapaProceso getEtapaPro() {
		return etapaPro;
	}


	public void setEtapaPro(EtapaProceso etapaPro) {
		this.etapaPro = etapaPro;
	}


	public int getRegproIdEjecucion() {
		return regproIdEjecucion;
	}

	public void setRegproIdEjecucion(int regproIdEjecucion) {
		this.regproIdEjecucion = regproIdEjecucion;
	}

	public int getCodEtapaProceso() {
		return codEtapaProceso;
	}

	public void setCodEtapaProceso(int codEtapaProceso) {
		this.codEtapaProceso = codEtapaProceso;
	}

	public int getCodProceso() {
		return codProceso;
	}

	public void setCodProceso(int codProceso) {
		this.codProceso = codProceso;
	}

	public String getRegproRutResponsable() {
		return regproRutResponsable;
	}

	public void setRegproRutResponsable(String regproRutResponsable) {
		this.regproRutResponsable = regproRutResponsable;
	}

	public String getRegProNombreResponsable() {
		return regProNombreResponsable;
	}

	public void setRegProNombreResponsable(String regProCuentaUsuario) {
		this.regProNombreResponsable = regProCuentaUsuario;
	}

	public Date getRegProFechaRegistro() {
		return regProFechaRegistro;
	}
	public String getRegProFechaRegistroFormato() {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		
		String reportDate = df.format(this.regProFechaRegistro);
		
		return reportDate;
	}

	public void setRegProFechaRegistro(Date regProFechaRegistro) {
		this.regProFechaRegistro = regProFechaRegistro;
	}

	public Date getRegProFechaNomina() {
		return regProFechaNomina;
	}
	public String getRegProFechaNominaFormato() {
		
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		
		String reportDate = df.format(regProFechaNomina);
		
		return reportDate;
	}

	public void setRegProFechaNomina(Date regProFechaNomina) {
		this.regProFechaNomina = regProFechaNomina;
	}
	

	public String getRegProFinalizado() {
		return regProFinalizado;
	}

	public void setRegProFinalizado(String regProFinalizado) {
		this.regProFinalizado = regProFinalizado;
	}
	
	
	
	

}
