 package cl.utfsm.rrhhG.mvc;
 
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.velocity.tools.generic.NumberTool;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import cl.utfsm.base.AccionWeb;

import cl.utfsm.base.util.Util;

import cl.utfsm.rrhhG.datos.CuentaContableDAO;
import cl.utfsm.rrhhG.datos.CuentaDescuentoDAO;
import cl.utfsm.rrhhG.datos.CuentaHaberesDAO;
import cl.utfsm.rrhhG.datos.EtapaProcesoDAO;
import cl.utfsm.rrhhG.datos.GurfeedNominaDAO;
import cl.utfsm.rrhhG.datos.HaberDescuentoDAO;
import cl.utfsm.rrhhG.datos.HibernateGurfeedDao;
import cl.utfsm.rrhhG.datos.IseriesAppef01DAO;
import cl.utfsm.rrhhG.datos.NivelAS400DAO;
import cl.utfsm.rrhhG.datos.OrganizacionDAO;
import cl.utfsm.rrhhG.datos.PlantaDAO;
import cl.utfsm.rrhhG.datos.PreGurfeedDAO;
import cl.utfsm.rrhhG.datos.ProcesoIncidenciaDAO;
import cl.utfsm.rrhhG.datos.RegistroIncidenciaDAO;
import cl.utfsm.rrhhG.datos.RegistroProcesoDAO;

import cl.utfsm.rrhhG.modulo.CampusEQ;
import cl.utfsm.rrhhG.modulo.CuentaDescuento;
import cl.utfsm.rrhhG.modulo.CuentaHaberes;
import cl.utfsm.rrhhG.modulo.EtapaProceso;
import cl.utfsm.rrhhG.modulo.GurfeedNomina;
import cl.utfsm.rrhhG.modulo.IseriesAppef01;
import cl.utfsm.rrhhG.modulo.ModuloGurfeed;
import cl.utfsm.rrhhG.modulo.CuentaContable;
import cl.utfsm.rrhhG.modulo.EstrucuturaHtml;
import cl.utfsm.rrhhG.modulo.HaberDescuento;
import cl.utfsm.rrhhG.modulo.Organizacion;
import cl.utfsm.rrhhG.modulo.Planta;
import cl.utfsm.rrhhG.modulo.ProcesoIncidencia;
import cl.utfsm.rrhhG.modulo.RegistroIncidencia;
import cl.utfsm.rrhhG.modulo.RegistroProceso;
import cl.utfsm.rrhhG.modulo.ResumenNomina;
import cl.utfsm.rrhhG.modulo.ResumenPorCuenta;
import cl.utfsm.rrhhG.modulo.TablaAutomatica;

import cl.utfsm.rrhhI2.datos.HibernateInterface2Dao;
import cl.utfsm.rrhhI2.modulo.ModuloInterface2;

public class GurfeedInterceptor extends HandlerInterceptorAdapter {
	ModuloGurfeed moduloGurfeed;

	public ModuloGurfeed getModuloGurfeed() {
		return moduloGurfeed;
	}

	public void setModuloGurfeed(ModuloGurfeed moduloGurfeed) {
		this.moduloGurfeed = moduloGurfeed;
	}

	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
	}

	// mantenedor
	public void tablaHaberesDescuento(AccionWeb accionweb) throws Exception {
		String codigo = accionweb.getParameter("codigo");
		String tipoM = accionweb.getParameter("tipoM");
		
		List<CuentaHaberes> listCuentaHarDes = new ArrayList<CuentaHaberes>();
		int codInt = Util.validaParametro(accionweb.getParameter("codigo"), 0);
		boolean datos = true;
		if (codigo != null && codigo.trim().length() > 0) {
			System.out.print(codInt);
			if (codInt > 0 && codInt < 200) {
				listCuentaHarDes = CuentaHaberesDAO.listHaberesPorCodigo(codInt);
			}else{
				
			}
		} else {
			listCuentaHarDes = CuentaHaberesDAO.listHaberes();
		}

		if (listCuentaHarDes.size() > 0) {
			Collections.sort(listCuentaHarDes);
			/*String[][] columnas = new String[6][2];
			// 1 columna
			if (tipoM.equals("HAB")) {
				columnas[0][0] = "C�digo de Haber";
				columnas[0][1] = "1";
			} else if (tipoM.equals("DES")) {
				columnas[0][0] = "C�digo de Descuento";
				columnas[0][1] = "1";
			}
			// 2 columna
			columnas[1][0] = "Planta";
			columnas[1][1] = "2";
			// 3 columna
			columnas[2][0] = "Nivel";
			columnas[2][1] = "3";
			// 4 columna
			columnas[3][0] = "Cuenta";
			columnas[3][1] = "4";
			// 5 columna
			columnas[4][0] = "Organizaci�n";
			columnas[4][1] = "5";
			// 6 columna
			columnas[5][0] = "Editar";
			columnas[5][1] = "Det";
			*/
			/*ArrayList<TablaAutomatica> paraTabla = new ArrayList<TablaAutomatica>();
			for (CuentaHaberes registro : listCuentaHarDes) {
				paraTabla.add(registro);
			}*/
			//String tabla = EstrucuturaHtml.tablaCuentaHabDes(paraTabla,columnas, 100);
			//accionweb.agregarObjeto("tabla", listCuentaHarDes);
		} else {
			datos = false;			
		}
		
		accionweb.agregarObjeto("tipoM",tipoM);
		accionweb.agregarObjeto("datos",datos);
		accionweb.agregarObjeto("Cuentas",listCuentaHarDes);
	}
	
	public void tablaDescuento(AccionWeb accionweb) throws Exception {
		String codigo = accionweb.getParameter("codigo");
		String tipoM = accionweb.getParameter("tipoM");
		List<CuentaDescuento> listCuentaHarDes = new ArrayList<CuentaDescuento>();
		boolean datos = true;
		if (codigo != null && codigo.trim().length() > 0) {		
			
				listCuentaHarDes = CuentaDescuentoDAO.getListDescuentos(Util.validaParametro(codigo,0));
			
		} else {
			listCuentaHarDes = CuentaDescuentoDAO.getListDescuentos();
		}

		if (listCuentaHarDes.size() > 0) {
			Collections.sort(listCuentaHarDes);
			String[][] columnas = new String[6][2];
			// 1 columna
			if (tipoM.equals("HAB")) {
				columnas[0][0] = "C�digo de Haber";
				columnas[0][1] = "1";
			} else if (tipoM.equals("DES")) {
				columnas[0][0] = "C�digo de Descuento";
				columnas[0][1] = "1";
			}			
			// 2 columna
			columnas[1][0] = "Cuenta";
			columnas[1][1] = "4";
			// 3 columna
			columnas[2][0] = "Organizaci�n";
			columnas[2][1] = "5";
			// 4 columna
			columnas[3][0] = "Programa";
			columnas[3][1] = "6";
			// 4 columna
			columnas[4][0] = "Fondo";
			columnas[4][1] = "7";
			// 6 columna
			columnas[5][0] = "Editar";
			columnas[5][1] = "Det";

			ArrayList<TablaAutomatica> paraTabla = new ArrayList<TablaAutomatica>();
			for (CuentaDescuento registro : listCuentaHarDes) {
				paraTabla.add(registro);
			}
			//String tabla = EstrucuturaHtml.tablaCuentaHabDes(paraTabla,columnas, 100);
			//accionweb.agregarObjeto("tabla", tabla);
		} else {
			datos = false;
		}
		accionweb.agregarObjeto("tipoM",tipoM);
		accionweb.agregarObjeto("datos",datos);
		accionweb.agregarObjeto("Cuentas",listCuentaHarDes);
	}

	// matenedor
	public void formulario(AccionWeb accionweb) {

		String tipoM = accionweb.getParameter("tipoM");

		String tipo = accionweb.getParameter("tipo");
		accionweb.agregarObjeto("tipoF", tipo);
		accionweb.agregarObjeto("tipoM", tipoM);
		HashMap<String, Planta> mapaPlanta;
		try {
			mapaPlanta = (HashMap<String, Planta>) accionweb.getSesion().getAttribute("listPlantas");

		} catch (Exception e) {
			mapaPlanta = null;
		}
		if (mapaPlanta == null) {
			PlantaDAO plantaDAO = new PlantaDAO();
			mapaPlanta = plantaDAO.mapaPlanta();
			accionweb.getSesion().setAttribute("listPlantas", mapaPlanta);
		}
		if (!tipo.equals("CREATE")) {
			String rowID = accionweb.getParameter("codigoPK");
			if(tipoM.equals("DES")){				
				CuentaDescuento cuenta = CuentaDescuentoDAO.findOneByCodigo(Integer.parseInt(rowID));
				accionweb.agregarObjeto("reglaCuentaModificar", cuenta);				
			}else{
				
				rowID.substring(rowID.indexOf("-",rowID.indexOf("-",1)+1)+1);
				System.out.print(rowID+"\n"+rowID.indexOf("-")
						+" / " +rowID.indexOf("-",1)
						+" / " +rowID.indexOf("-",rowID.indexOf("-")+1));
				System.out.print("\n 1:"+rowID);
				System.out.print("\n CODHDE:"+ rowID.substring(0,rowID.indexOf("-")));
				System.out.print("\n CODPLA:" +rowID.substring(rowID.indexOf("-")+1,rowID.indexOf("-",rowID.indexOf("-",1)+1)));				
				System.out.print("\n NUMNIV:" +rowID.substring(rowID.indexOf("-",rowID.indexOf("-",1)+1)+1));
						
				int codigo = Integer.parseInt( rowID.substring(0,rowID.indexOf("-")));
				String planta = rowID.substring(rowID.indexOf("-")+1,rowID.indexOf("-",rowID.indexOf("-",1)+1));
				String nivel  = rowID.substring(rowID.indexOf("-",rowID.indexOf("-",1)+1)+1);
				CuentaHaberes haberes = CuentaHaberesDAO.reglaCuentaHaber(codigo, planta, nivel);
				accionweb.agregarObjeto("reglaCuentaModificar", haberes);
				
			}			
			
			accionweb.agregarObjeto("edit", "disabled");
			accionweb.agregarObjeto("ayuda", "display: none;");
		}
		
		accionweb.agregarObjeto("plantas", mapaPlanta);
	}

	// mantenedor
	public void ventanaBuscador(AccionWeb accionweb) {
		String tipoBusqueda = accionweb.getParameter("tipoBus");//tipoMantenedor
	    String tipoMantenedor = accionweb.getParameter("tipoMantenedor");
		if (tipoBusqueda.equals("CODHDE")) {
			if(tipoMantenedor.equals("DES")){
				accionweb.agregarObjeto("titulo", "C�digo de descuento");
			}else{
				accionweb.agregarObjeto("titulo", "C�digo de Haber ");
			}
			accionweb.agregarObjeto("tipoMantenedor", tipoMantenedor);
			accionweb.agregarObjeto("tipoBus", tipoBusqueda);
		} else if (tipoBusqueda.equals("CODPLA")) {

		} else if (tipoBusqueda.equals("NUMNIV")) {

		} else if (tipoBusqueda.equals("Cuenta")) {
			accionweb.agregarObjeto("titulo", "Cuenta");
			accionweb.agregarObjeto("tipoBus", tipoBusqueda);
		} else if (tipoBusqueda.equals("CODORG")) {
			accionweb.agregarObjeto("titulo", "Organizaci�n");
			accionweb.agregarObjeto("tipoBus", tipoBusqueda);
		}
	}

	// mantenedor
	public void buscador(AccionWeb accionweb) {
		String tipo = accionweb.getParameter("tipoDesc");
		String tipoMantenedor = accionweb.getParameter("tipoMantenedor");
		String bus = accionweb.getParameter("bus");
		String tabla = "";
		List<? extends TablaAutomatica> paraTabla = new ArrayList<TablaAutomatica>();
		String[][] columnas = new String[1][1];

		if (tipo.equals("CODHDE")) {
			HashMap<Integer, HaberDescuento> tablaMap = (HashMap<Integer, HaberDescuento>) accionweb.getSesion().getAttribute("listHaberDes");
			int busInt = Util.validaParametro(bus, 0);
			if (tablaMap == null) {
				HaberDescuentoDAO habDesDAO = new HaberDescuentoDAO();
				tablaMap = habDesDAO.tablaCompleta();
				accionweb.getSesion().setAttribute("listHaberDes", tablaMap);
			}
			HaberDescuento paso = new HaberDescuento();
			List<HaberDescuento> listBase = new ArrayList<HaberDescuento>();
			if (tipoMantenedor != null && tipoMantenedor.equals("HAB")) {
				for (Integer key : tablaMap.keySet()) {
					if(key < 200){
						if(busInt > 0){
							if((""+key).indexOf(bus) > -1){
								paso = tablaMap.get(key);
							}else{
								paso = null;
							}
						}else{
							paso = tablaMap.get(key);
						}
						if (paso != null) {							
							listBase.add(paso);
						}
					}
				}
				columnas = new String[2][2];

				columnas[0][0] = "C�digo";
				columnas[0][1] = "1";

				columnas[1][0] = "Descripci�n";
				columnas[1][1] = "2";
				 
				Collections.sort(listBase);
				accionweb.agregarObjeto("datos",  listBase);
			}else{
				for (Integer key : tablaMap.keySet()) {
					if(key > 200){
						if(busInt > 0){
							if((""+key).indexOf(bus) > -1){
								paso = tablaMap.get(key);
							}else{
								paso = null;
							}
						}else{
							paso = tablaMap.get(key);
						}
						if (paso != null) {
							
							listBase.add(paso);
						}
					}
				}
				
				columnas = new String[2][2];

				columnas[0][0] = "C�digo";
				columnas[0][1] = "1";

				columnas[1][0] = "Descripci�n";
				columnas[1][1] = "2";

				Collections.sort(listBase);
				accionweb.agregarObjeto("datos", listBase);
			}
		} else if (tipo.equals("CODPLA")) {

		} else if (tipo.equals("NUMNIV")) {
			tipo = "Nivel";
		} else if (tipo.equals("Cuenta")) {
			tipo = "Cuenta";
			
			CuentaContableDAO cuentConDAO = new CuentaContableDAO();
			ArrayList<CuentaContable> listCuen = cuentConDAO.busquedaCuenta(bus, 100);
			columnas = new String[2][2];
			// 2 columna
			columnas[0][0] = "C�digo Cuenta";
			columnas[0][1] = "1";
			// 3 columna
			columnas[1][0] = "Descripci�n Cuenta";
			columnas[1][1] = "2";

			
			accionweb.agregarObjeto("datos", listCuen) ;
			

		} else if (tipo.equals("CODORG")) {
			//tipo = "Organizaci�n";
			OrganizacionDAO orgnDAO = new OrganizacionDAO();
			ArrayList<Organizacion> listOrg = orgnDAO.buscarOrgnList(bus);

			columnas = new String[4][2];
			// 2 columna
			columnas[0][0] = "C�digo Organizaci�n";
			columnas[0][1] = "1";
			// 3 columna
			columnas[1][0] = "Descripci�n Organizacion";
			columnas[1][1] = "2";
			// 4 columna
			columnas[2][0] = "C�digo Fondo";
			columnas[2][1] = "3";
			// 5 columna
			columnas[3][0] = "C�digo Programa";
			columnas[3][1] = "4";
			
			accionweb.agregarObjeto("datos", listOrg);
			
		}
		/*
		if (paraTabla.size() > 0) {
			tabla = EstrucuturaHtml.tablaCuentaHabDes((List<TablaAutomatica>)paraTabla , columnas,true);
		} else {
			tabla = "<h3> No hay similitud con alguna " + tipo + " </h3>";
		}*/
		accionweb.agregarObjeto("tipo", tipo);
		
		accionweb.agregarObjeto("tabla", tabla);
	}

	// mantenedor guardar cambio
	public void guardarReglaCuenta(AccionWeb accionweb) {
		String respuesta = "";
		String accion = accionweb.getParameter("tipo").trim();//
		String tipoM = accionweb.getParameter("tipoM").trim();
		
		if(tipoM.equals("DES")){
			CuentaDescuento cuentaDescuento = new CuentaDescuento();
			cuentaDescuento.setCodigo(Util.validaParametro(accionweb.getParameter("CODHDE"), 0));
			cuentaDescuento.setPlanta(accionweb.getParameter("CODPLA").trim());
			cuentaDescuento.setNivel(accionweb.getParameter("NUMNIV").trim());
			cuentaDescuento.setCuenta(accionweb.getParameter("Cuenta"));
			cuentaDescuento.setOrganizacion(accionweb.getParameter("CODORG"));
			cuentaDescuento.setId(accionweb.getParameter("ID"));
			Organizacion org = OrganizacionDAO.buscarOrgnPorCod(cuentaDescuento.getOrganizacion());
			cuentaDescuento.setPrograma(org.getCodPrograma());
			cuentaDescuento.setFondo(org.getCodFondo());
			if(accion.equals("CREATE")){
				
					if(CuentaDescuentoDAO.crearReglaCuentaDescuento(cuentaDescuento)){
						respuesta = "{\"mensaje\":\"creaci�n correcta\",\"estado\":true}";
					}else{
						respuesta = "{\"mensaje\":\"creaci�n cancelad\",\"estado\":false}";
					}
				
			}else if (accion.equals("EDIT")){
				
				if(CuentaDescuentoDAO.actualizaReglaCuentaDescuento(cuentaDescuento)){
					respuesta = "{\"mensaje\":\"actualizaci�n correcta\",\"estado\":true}";
					
				}else {
					respuesta = "{\"mensaje\":\"actualizaci�n cancelada\",\"estado\":false}";
				}
			}else if (accion.equals("ELIM")){
				// por decidir
			}
		}else{
			CuentaHaberes cuentaHaberes = new CuentaHaberes();
			cuentaHaberes.setCodigo(Util.validaParametro(accionweb.getParameter("CODHDE"), 0));
			cuentaHaberes.setPlanta(accionweb.getParameter("CODPLA").trim());
			cuentaHaberes.setNivel(accionweb.getParameter("NUMNIV").trim());
			cuentaHaberes.setCuenta(accionweb.getParameter("Cuenta"));
			cuentaHaberes.setOrganizacion(accionweb.getParameter("CODORG"));
			if(cuentaHaberes.getOrganizacion() != null && !cuentaHaberes.getOrganizacion().equals("")){
				Organizacion org = OrganizacionDAO.buscarOrgnPorCod(cuentaHaberes.getOrganizacion());
				cuentaHaberes.setPrograma(org.getCodPrograma());
				cuentaHaberes.setFondo(org.getCodFondo());
			}else{
				cuentaHaberes.setOrganizacion("FUNCIONARIO");
				cuentaHaberes.setFondo("FUNCIONARIO");
				cuentaHaberes.setPrograma("FUNCIONARIO");
			}
			
		if(accion.equals("CREATE")){
				
				if(CuentaHaberesDAO.guardarReglaCuentaHaberesDescuento(cuentaHaberes)){
					respuesta = "{\"mensaje\":\"creaci�n correcta\",\"estado\":true}";
				}else{
					respuesta = "{\"mensaje\":\"creaci�n cancelad\",\"estado\":false}";
				}
			
		}else if (accion.equals("EDIT")){
			
			if(CuentaHaberesDAO.actualizarReglaCuentaHaberesDescuento(cuentaHaberes)){
				respuesta = "{\"mensaje\":\"actualizaci�n correcta\",\"estado\":true}";
				
			}else {
				respuesta = "{\"mensaje\":\"actualizaci�n cancelada\",\"estado\":false}";
			}
		}else if (accion.equals("ELIM")){
			// por decidir
		}
			
		}
		accionweb.agregarObjeto("mensaje", respuesta);
		
	}

	public void validarPK(AccionWeb accionweb){
		
		String tipoM = accionweb.getParameter("tipoMantenedor");
		String mensaje = "";
		if(tipoM.equals("DES")){
			
			List<CuentaDescuento> cuen = CuentaDescuentoDAO.getListDescuentos(Util.validaParametro(accionweb.getParameter("CODHDE"),-1));
			if(cuen.size()>0){
				mensaje ="{\"desc\":\"clave primaria ya existe\",\"existe\":true}";
			}else{
				mensaje ="{\"desc\":\"clave primaria disponible\",\"existe\":false}";
			}
		}else{
			int codigo = Util.validaParametro(accionweb.getParameter("CODHDE"),-1);
			String planta = accionweb.getParameter("CODPLA");
			String nivel = accionweb.getParameter("NUMNIV");
			List<CuentaHaberes> cuen;
			if(codigo > 0 && planta != null && !planta.equals("")&& nivel != null && !nivel.equals("")){
				cuen = CuentaHaberesDAO.listHaberesPorPlantaYPlantaYNivel(codigo, planta, nivel);
				if(cuen.size() == 0){
					mensaje ="{\"desc\":\"clave primaria disponible\",\"existe\":false}";
				}else{
					mensaje ="{\"desc\":\"clave primaria ya existe\",\"existe\":true}";
				}
			}else{
				mensaje ="{\"desc\":\"\",\"existe\":true}";
			}
 			
			
		}
		accionweb.agregarObjeto("mensaje", mensaje);
	}
	
	// mantenedor
	public void descripcion(AccionWeb accionweb) {
		String tipo = accionweb.getParameter("tipoDesc");
		
		String codigo = accionweb.getParameter("codigo");
		String tipoMantenedor = accionweb.getParameter("tipoMantenedor");

		String repuesta = null;
		if (tipo.equals("CODHDE")) {
			if (codigo != null && !codigo.trim().equals("")) {
				int cod = Util.validaParametro(codigo, 0);
				if (cod > 0
						&& ((tipoMantenedor.equals("HAB") && cod < 200) || (tipoMantenedor
								.equals("DES") && cod >= 200))) {

					HashMap<Integer, HaberDescuento> tablaMap = (HashMap<Integer, HaberDescuento>) accionweb.getSesion().getAttribute("listHaberDes");
					if (tablaMap == null) {
						HaberDescuentoDAO habDesDAO = new HaberDescuentoDAO();
						tablaMap = habDesDAO.tablaCompleta();
						accionweb.getSesion().setAttribute("listHaberDes",tablaMap);
					}
					HaberDescuento harDes = tablaMap.get(cod);
					if (harDes != null) {
						
						repuesta = "{\"desc\":\"" + harDes.getDescHabDes().replace("\"", "\\\"")
								+ "\",\"existe\":true}";
					} else {
						repuesta = "{\"desc\":\"EL c�digo no existe\",\"existe\":false}";
					}

				} else {
					repuesta = "{\"desc\":\"EL c�digo no existe\",\"existe\":false}";
				}
			} else {
				repuesta = "{\"desc\":\"Debe ingresar dato\",\"existe\":false}";
			}
			if (repuesta == null) {
				repuesta = "{\"desc\":\"EL c�digo no existe\",\"existe\":false}";
			}
			accionweb.agregarObjeto("mensaje", repuesta);
		} else if (tipo.equals("CODPLA")) {

		} else if (tipo.equals("NUMNIV")) {
			String cod = codigo;
			HashMap<String, String> listNivel = (HashMap<String, String>) accionweb.getSesion().getAttribute("listNivel");
			if (listNivel == null) {
				NivelAS400DAO nivelDAO = new NivelAS400DAO();
				listNivel = nivelDAO.mapNivel();
			}
			if (cod != null && !cod.equals("")) {
				String nivel = listNivel.get(cod);
				if (nivel == null) {
					repuesta = "{\"desc\":\"Nivel Incorrecto\",\"existe\":false}";
				} else {
					repuesta = "{\"desc\":\"Nivel Correcto\",\"existe\":true}";
				}
			} else {
				repuesta = "{\"desc\":\"Debe ingresar dato\",\"existe\":false}";
			}
		} else if (tipo.equals("Cuenta")) {
			if (codigo != null && !codigo.trim().equals("")) {
				CuentaContableDAO cuenConDAO = new CuentaContableDAO();
				CuentaContable cuen = cuenConDAO.cuentaPorCodigo(codigo);
				if (cuen != null) {

					repuesta = "{\"desc\":\"" + cuen.getDescCuenta()
							+ "\",\"existe\":true}";
				} else {
					repuesta = "{\"desc\":\"EL c�digo no existe\",\"existe\":false}";
				}
				accionweb.agregarObjeto("mensaje", repuesta);
			} else if (codigo.trim().equals("")) {
				repuesta = "{\"desc\":\"Debe ingresar este campo\",\"existe\":false}";
			}
			if (repuesta == null) {
				repuesta = "{\"desc\":\"EL c�digo no existe\",\"existe\":false}";
			}
			accionweb.agregarObjeto("mensaje", repuesta);
		} else if (tipo.equals("CODORG")) {
			if (codigo != null && !codigo.trim().equals("")) {
				OrganizacionDAO orgnDAO = new OrganizacionDAO();
				Organizacion orgn;
				orgn = orgnDAO.buscarOrgnPorCod(codigo);
				if (orgn != null) {
					repuesta = "{\"desc\":\"" + orgn.getDescOreg()
							+ "\",\"existe\":true}";
				} else {
					repuesta = "{\"desc\":\"EL c�digo no existe\",\"existe\":false}";
				}

			} else {
				repuesta = "{\"desc\":\"\",\"existe\":false}";
			}
			if (repuesta == null) {
				repuesta = "{\"desc\":\"\",\"existe\":false}";
			}
			accionweb.agregarObjeto("mensaje", repuesta);
		}
		if (repuesta == null) {
			repuesta = "\"desc\":\"EL c�digo no existe\",\"existe\":false}";
		}		
		accionweb.agregarObjeto("mensaje", repuesta);
	}

	// mantenedor
	public void cargarMenu(AccionWeb accionweb) throws Exception {

		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);

		accionweb.agregarObjeto("esGurfeed", 1);
		accionweb.agregarObjeto("opcionMenu", tipo);
		switch (tipo) {
		case 1: 
			this.procesoNominaGurfeed(accionweb);
			break;
		case 2:
			this.mantenedorHaber(accionweb);
			break;
		case 3:
			this.mantenedorDescuento(accionweb);
			break;
		case 4:
			this.aprobarGurfeed(accionweb);
			break;
		default:
			break;
		}
	}

	public void aprobarGurfeed(AccionWeb accionweb) {
		RegistroProceso reg = RegistroProcesoDAO.ultimoGurfeedEjecutado();
		accionweb.agregarObjeto("session", accionweb.getSesion());
		if (reg != null) {
			accionweb.agregarObjeto("session", accionweb.getSesion());
			accionweb.agregarObjeto("gurFin", reg);
			accionweb.agregarObjeto("hayGurfeed", true);

			accionweb.agregarObjeto("neJecusion", reg.getRegproIdEjecucion());
			SimpleDateFormat formateador = new SimpleDateFormat("MM",
					new Locale("ES"));

			String mes = formateador.format(reg.getRegProFechaNomina());
			
			formateador = new SimpleDateFormat("yyyy", new Locale("ES"));
			String anno = formateador.format(reg.getRegProFechaNomina());
			formateador = new SimpleDateFormat(" MMMM 'de' yyyy", new Locale("ES"));
			
			String fecha = formateador.format(reg.getRegProFechaNomina());
			accionweb.agregarObjeto("session", accionweb.getSesion());
			accionweb.getSesion().setAttribute("nomina", fecha);
			accionweb.getSesion().setAttribute("mes", mes);
			accionweb.getSesion().setAttribute("anno", anno);
			accionweb.getSesion().setAttribute("registroProceso", reg);
		} else {
			accionweb.agregarObjeto("hayGurfeed", false);
		}
	}
	// mantenedor
	public void mantenedorDescuento(AccionWeb accionweb) throws Exception {
		

		//Date fecha = new Date();
		//SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// formato
																				 // por
																				 // defecto
		//System.out.print(sdfDate.format(fecha) + "\n");
		//List<CuentaDescuento> listadoHberes = CuentaDescuentoDAO.getListDescuentos();
		//fecha = new Date();
		//System.out.print(sdfDate.format(fecha) + "\n");
		//String[][] columnas = new String[4][2];
		/*
		// 1 columna
		columnas[0][0] = "C�digo de Descuento";
		columnas[0][1] = "1";
		
		// 4 columna
		columnas[1][0] = "Cuenta";
		columnas[1][1] = "4";
		// 5 columna
		columnas[2][0] = "Organizaci�n";
		columnas[2][1] = "5";
		// 6 columna
		columnas[3][0] = "Editar";
		columnas[3][1] = "Det";
		
		Collections.sort(listadoHberes);
		System.out.print(" " + listadoHberes.size() + "\n");
		fecha = new Date();
		System.out.print("creacion de tabla " + sdfDate.format(fecha) + "\n");
		fecha = new Date();
		System.out.print(sdfDate.format(fecha) + "\n");
		fecha = new Date();
		System.out.print(sdfDate.format(fecha) + "\n");
		//*/
	}

	// mantenedor
	public void mantenedorHaber(AccionWeb accionweb) throws Exception {
		

		Date fecha = new Date();
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// formato por defecto		
		System.out.print(sdfDate.format(fecha) + "\n");		
		List<CuentaHaberes> list = CuentaHaberesDAO.listHaberes();
		fecha = new Date();
		System.out.print(sdfDate.format(fecha) + "\n");
		String[][] columnas = new String[6][2];
		// 1 columna
		columnas[0][0] = "C�digo de Haber";
		columnas[0][1] = "1";
		// 2 columna
		columnas[1][0] = "Planta";
		columnas[1][1] = "2";
		// 3 columna
		columnas[2][0] = "Nivel";
		columnas[2][1] = "3";
		// 4 columna
		columnas[3][0] = "Cuenta";
		columnas[3][1] = "4";
		// 5 columna
		columnas[4][0] = "Organizaci�n";
		columnas[4][1] = "5";
		// 6 columna
		columnas[5][0] = "Editar";
		columnas[5][1] = "Det";

		Collections.sort(list);
		System.out.print(" " + list.size() + "\n");
		fecha = new Date();
		System.out.print("creacion de tabla " + sdfDate.format(fecha) + "\n");
		// columnas);
		fecha = new Date();
		System.out.print(sdfDate.format(fecha) + "\n");
		fecha = new Date();
		System.out.print(sdfDate.format(fecha) + "\n");
	}

	// exportacion a excel
	public void exportExcelGurfeed(AccionWeb accionweb) {
		List<GurfeedNomina> list = GurfeedNominaDAO.listGurfedNomina();
		accionweb.getSesion().setAttribute("mesAnno",
				accionweb.getParameter("mesAnno"));
		accionweb.getSesion().setAttribute("numeroEjecucion",
				accionweb.getParameter("numeroEjecucion"));

		Vector vecHead = new Vector();
		vecHead.addElement("GURFEED_SYSTEM_ID");
		vecHead.addElement("GURFEED_SYSTEM_TIME_STAMP");
		vecHead.addElement("GURFEED_DOC_CODE");
		vecHead.addElement("GURFEED_REC_TYPE");
		vecHead.addElement("GURFEED_SUBMISSION_NUMBER");
		vecHead.addElement("GURFEED_ITEM_NUM");
		vecHead.addElement("GURFEED_SEQ_NUM");
		vecHead.addElement("GURFEED_ACTIVITY_DATE");
		vecHead.addElement("GURFEED_USER_ID");
		vecHead.addElement("GURFEED_RUCL_CODE");
		vecHead.addElement("GURFEED_DOC_REF_NUM");
		vecHead.addElement("GURFEED_TRANS_DATE");
		vecHead.addElement("GURFEED_TRANS_AMT");
		vecHead.addElement("GURFEED_TRANS_DESC");
		vecHead.addElement("GURFEED_DR_CR_IND");
		vecHead.addElement("GURFEED_BANK_CODE");
		vecHead.addElement("GURFEED_OBUD_CODE");
		vecHead.addElement("GURFEED_OBPH_CODE");
		vecHead.addElement("GURFEED_BUDG_DUR_CODE");
		vecHead.addElement("GURFEED_COAS_CODE");
		vecHead.addElement("GURFEED_ACCI_CODE");
		vecHead.addElement("GURFEED_FUND_CODE");
		vecHead.addElement("GURFEED_ORGN_CODE");
		vecHead.addElement("GURFEED_ACCT_CODE");
		vecHead.addElement("GURFEED_PROG_CODE");
		vecHead.addElement("GURFEED_ACTV_CODE");
		vecHead.addElement("GURFEED_LOCN_CODE");
		vecHead.addElement("GURFEED_DEP_NUM");
		vecHead.addElement("GURFEED_ENCD_NUM");
		vecHead.addElement("GURFEED_ENCD_ITEM_NUM");
		vecHead.addElement("GURFEED_ENCD_SEQ_NUM");
		vecHead.addElement("GURFEED_ENCD_ACTION_IND");
		vecHead.addElement("GURFEED_PRJD_CODE");
		vecHead.addElement("GURFEED_DIST_PCT");
		vecHead.addElement("GURFEED_BUD_DISPN");
		vecHead.addElement("GURFEED_CMT_TYPE");
		vecHead.addElement("GURFEED_CMT_PCT");
		vecHead.addElement("GURFEED_ENCB_TYPE");
		vecHead.addElement("GURFEED_VENDOR_PIDM");
		vecHead.addElement("GURFEED_ONE_TIME_VEND_CODE");
		vecHead.addElement("GURFEED_BUDGET_PERIOD");
		vecHead.addElement("GURFEED_ACCRUAL_IND");
		vecHead.addElement("GURFEED_ABAL_OVERRIDE");
		vecHead.addElement("GURFEED_ID");
		vecHead.addElement("GURFEED_TRAN_NUMBER");
		vecHead.addElement("GURFEED_DETAIL_CODE");
		vecHead.addElement("GURFEED_TERM_CODE");
		vecHead.addElement("GURFEED_ACCOUNT");
		vecHead.addElement("GURFEED_SRCE_CODE");
		vecHead.addElement("GURFEED_EFFECTIVE_DATE");
		vecHead.addElement("GURFEED_PAYT_CODE");
		vecHead.addElement("GURFEED_DETAIL_CODE2");
		vecHead.addElement("GURFEED_PAYT_CODE2");
		vecHead.addElement("GURFEED_CURR_CODE");
		vecHead.addElement("GURFEED_FOREIGN_AMOUNT");
		vecHead.addElement("GURFEED_PTOT_STATUS");
		vecHead.addElement("GURFEED_LEDC_CODE");

		accionweb.getSesion().setAttribute("head", vecHead);

		Vector vecRegDatos = new Vector();
		Vector vecDatos = null;

		for (GurfeedNomina gur : list) {
			vecDatos = new Vector();
			vecDatos.addElement((gur.getGurfeedSystemId() == null) ? "" : gur.getGurfeedSystemId());
			vecDatos.addElement((gur.getGurfeedSystemTimeStamp() == null) ? "": gur.getGurfeedSystemTimeStamp());
			vecDatos.addElement((gur.getGurfeedDocCode() == null) ? "" : gur.getGurfeedDocCode());
			vecDatos.addElement((gur.getGurfeedRecType() == null) ? "" : gur.getGurfeedRecType());
			vecDatos.addElement((gur.getGurfeedSubmissionNumber() == null) ? "": gur.getGurfeedSubmissionNumber());
			vecDatos.addElement((gur.getGurfeedItemNum() == null) ? "" : gur.getGurfeedItemNum());
			vecDatos.addElement((gur.getGurfeedSeqNum() == null) ? "" : gur.getGurfeedSeqNum());
			vecDatos.addElement((gur.getGurfeedActivityDate() == null) ? "": gur.getGurfeedActivityDate());
			vecDatos.addElement((gur.getGurfeedUserId() == null) ? "" : gur.getGurfeedUserId());
			vecDatos.addElement((gur.getGurfeedRuclCode() == null) ? "" : gur.getGurfeedRuclCode());
			vecDatos.addElement((gur.getGurfeedDocRefNum() == null) ? "" : gur.getGurfeedDocRefNum());
			vecDatos.addElement((gur.getGurfeedTransDate() == null) ? "" : gur.getGurfeedTransDate());
			vecDatos.addElement((gur.getGurfeedTransAmt() == null) ? "" : gur.getGurfeedTransAmt());
			vecDatos.addElement((gur.getGurfeedTransDesc() == null) ? "" : gur.getGurfeedTransDesc());
			vecDatos.addElement((gur.getGurfeedDrCrInd() == null) ? "" : gur.getGurfeedDrCrInd());
			vecDatos.addElement((gur.getGurfeedBankCode() == null) ? "" : gur.getGurfeedBankCode());
			vecDatos.addElement((gur.getGurfeedObudCode() == null) ? "" : gur.getGurfeedObudCode());
			vecDatos.addElement((gur.getGurfeedObphCode() == null) ? "" : gur.getGurfeedObphCode());
			vecDatos.addElement((gur.getGurfeedBudgDurCode() == null) ? "": gur.getGurfeedBudgDurCode());
			vecDatos.addElement((gur.getGurfeedCoasCode() == null) ? "" : gur.getGurfeedCoasCode());
			vecDatos.addElement((gur.getGurfeedAcciCode() == null) ? "" : gur.getGurfeedAcciCode());
			vecDatos.addElement((gur.getGurfeedFundCode() == null) ? "" : gur.getGurfeedFundCode());
			vecDatos.addElement((gur.getGurfeedOrgnCode() == null) ? "" : gur.getGurfeedOrgnCode());
			vecDatos.addElement((gur.getGurfeedAcctCode() == null) ? "" : gur.getGurfeedAcctCode());
			vecDatos.addElement((gur.getGurfeedProgCode() == null) ? "" : gur.getGurfeedProgCode());
			vecDatos.addElement((gur.getGurfeedActvCode() == null) ? "" : gur.getGurfeedActvCode());
			vecDatos.addElement((gur.getGurfeedLocnCode() == null) ? "" : gur.getGurfeedLocnCode());
			vecDatos.addElement((gur.getGurfeedDepNum() == null) ? "" : gur.getGurfeedDepNum());
			vecDatos.addElement((gur.getGurfeedEncdNum() == null) ? "" : gur.getGurfeedEncdNum());
			vecDatos.addElement((gur.getGurfeedEncdItemNum() == null) ? "": gur.getGurfeedEncdItemNum());
			vecDatos.addElement((gur.getGurfeedEncdSeqNum() == null) ? "" : gur.getGurfeedEncdSeqNum());
			vecDatos.addElement((gur.getGurfeedEncdActionInd() == null) ? "": gur.getGurfeedEncdActionInd());
			vecDatos.addElement((gur.getGurfeedPrjdCode() == null) ? "" : gur.getGurfeedPrjdCode());
			vecDatos.addElement((gur.getGurfeedDistPct() == null) ? "" : gur.getGurfeedDistPct());
			vecDatos.addElement((gur.getGurfeedBudDispn() == null) ? "" : gur.getGurfeedBudDispn());
			vecDatos.addElement((gur.getGurfeedCmtType() == null) ? "" : gur.getGurfeedCmtType());
			vecDatos.addElement((gur.getGurfeedCmtPct() == null) ? "" : gur.getGurfeedCmtPct());
			vecDatos.addElement((gur.getGurfeedEncbType() == null) ? "" : gur.getGurfeedEncbType());
			vecDatos.addElement((gur.getGurfeedVendorPidm() == null) ? "" : gur.getGurfeedVendorPidm());
			vecDatos.addElement((gur.getGurfeedOneTimeVendCode() == null) ? "": gur.getGurfeedOneTimeVendCode());
			vecDatos.addElement((gur.getGurfeedBudgetPeriod() == null) ? "": gur.getGurfeedBudgetPeriod());
			vecDatos.addElement((gur.getGurfeedAccrualInd() == null) ? "" : gur.getGurfeedAccrualInd());
			vecDatos.addElement((gur.getGurfeedAbalOverride() == null) ? "": gur.getGurfeedAbalOverride());
			vecDatos.addElement((gur.getGurfeedId() == null) ? "" : gur.getGurfeedId());
			vecDatos.addElement((gur.getGurfeedTranNumber() == null) ? "" : gur.getGurfeedTranNumber());
			vecDatos.addElement((gur.getGurfeedDetailCode() == null) ? "" : gur.getGurfeedDetailCode());
			vecDatos.addElement((gur.getGurfeedTermCode() == null) ? "" : gur.getGurfeedTermCode());
			vecDatos.addElement((gur.getGurfeedAccount() == null) ? "" : gur.getGurfeedAccount());
			vecDatos.addElement((gur.getGurfeedSrceCode() == null) ? "" : gur.getGurfeedSrceCode());
			vecDatos.addElement((gur.getGurfeedEffectiveDate() == null) ? "": gur.getGurfeedEffectiveDate());
			vecDatos.addElement((gur.getGurfeedPaytCode() == null) ? "" : gur.getGurfeedPaytCode());
			vecDatos.addElement((gur.getGurfeedDetailCode2() == null) ? "": gur.getGurfeedDetailCode2());
			vecDatos.addElement((gur.getGurfeedPaytCode2() == null) ? "" : gur.getGurfeedPaytCode2());
			vecDatos.addElement((gur.getGurfeedCurrCode() == null) ? "" : gur.getGurfeedCurrCode());
			vecDatos.addElement((gur.getGurfeedForeignAmount() == null) ? "": gur.getGurfeedForeignAmount());
			vecDatos.addElement((gur.getGurfeedPtotStatus() == null) ? "" : gur.getGurfeedPtotStatus());
			vecDatos.addElement((gur.getGurfeedLedcCode() == null) ? "" : gur.getGurfeedLedcCode());
			vecRegDatos.addElement(vecDatos);
		}
		accionweb.getSesion().setAttribute("head", vecHead);
		accionweb.getSesion().setAttribute("contenido", vecRegDatos);

	}

	public void procesoNominaGurfeed(AccionWeb accionweb) throws Exception {
		System.out.print("\n inicio procesoNominaGurfeed \n");
		HibernateGurfeedDao hiberGur2DAO = new HibernateGurfeedDao();
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int mesE = Util.validaParametro(accionweb.getParameter("mesE"), 0);
		int anoE = Util.validaParametro(accionweb.getParameter("anoE"), 0);
		DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");

		RegistroProceso regPro = new RegistroProceso();
		accionweb.agregarObjeto("esGurfeed", 1);
		accionweb.agregarObjeto("opcionMenu", tipo);
		EtapaProcesoDAO etaProDAO = new EtapaProcesoDAO();
		ArrayList<EtapaProceso> listEtaPro = etaProDAO.buscarEtapas(1);
		accionweb.agregarObjeto("listEtapa", listEtaPro);
		
		Map<String, RegistroProceso> listRegPro = new HashMap<String, RegistroProceso>();
		accionweb.agregarObjeto("contidoTabla", listRegPro);
		// Etapa 0 de proceso de GURFEED
 
		if (mesE > 0) {
			hiberGur2DAO.nomina(accionweb.getSesion(), mesE, anoE);
		} else {
			hiberGur2DAO.nomina(accionweb.getSesion());
		}

		regPro.setRegproIdEjecucion(hiberGur2DAO.getNumeroEjecusion());
		accionweb.agregarObjeto("neJecusion", regPro.getRegproIdEjecucion());
		regPro.setCodProceso(1);
		regPro.setCodEtapaProceso(0);
		regPro.setRegProNombreResponsable(""+ accionweb.getSesion().getAttribute("funcionario"));
		regPro.setRegproRutResponsable(""+ accionweb.getSesion().getAttribute("rutUsuario")+ accionweb.getSesion().getAttribute("dv"));
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String mes = "" + accionweb.getSesion().getAttribute("mes");
		mes = "00".substring(mes.length()) + mes;
		String anno = "" + accionweb.getSesion().getAttribute("anno");
		Date date = sdf.parse("28-" + mes + "-" + accionweb.getSesion().getAttribute("anno"));
		regPro.setRegProFechaNomina(date);
		regPro.setRegProFinalizado("F");
		regPro.cargarEtapa();
		RegistroProcesoDAO regDao = new RegistroProcesoDAO();
		regDao.insertRegistroProceso(regPro);
		String numDoc = HibernateGurfeedDao.mesUsado(Integer.parseInt(mes),Integer.parseInt(anno));
		regPro.setCodEtapaProceso(1);
		System.out.print("\n punto de control 1:");
		if (!numDoc.equals("true")) {
			System.out.print("\n punto de control 1.2.1:");
			regPro.setRegProFinalizado("C");
			//listRegPro.get(1).getRegProFinalizado();
			listRegPro.put("" + regPro.getCodEtapaProceso(),new RegistroProceso(regPro));
			regDao.insertRegistroProceso(regPro);
			EstrucuturaHtml etHtml = new EstrucuturaHtml();
			//String Tabla = etHtml.tableEtapa(listRegPro, accionweb);
			RegistroProceso regProGurFin = regDao.gurfeedFinalisado(
					Integer.parseInt("" + accionweb.getSesion().getAttribute("mes")),
					Integer.parseInt(""+ accionweb.getSesion().getAttribute("anno")));
			accionweb.agregarObjeto("gurFin", regProGurFin);
			//accionweb.agregarObjeto("tabla", Tabla);
			accionweb.agregarObjeto("contidoTabla", listRegPro);
			accionweb.agregarObjeto("integrado", 2);
			accionweb.agregarObjeto("session", accionweb.getSesion());
			accionweb.agregarObjeto("doc", numDoc);
			System.out.print("\n punto de control 1.2.2:");
		} else {
			System.out.print("\n punto de control 1.3.1:");
			regPro.setRegProFinalizado("F");
			listRegPro.put("" + regPro.getCodEtapaProceso(),new RegistroProceso(regPro));
			regDao.insertRegistroProceso(regPro);
			// Etapa 2 de proceso de GURFEED
			regPro.setCodEtapaProceso(2);
			regPro.setRegProFinalizado("c");
			regDao.insertRegistroProceso(regPro);
			accionweb.agregarObjeto("session", accionweb.getSesion());
			Collection<String> l = hiberGur2DAO.integrado(accionweb.getSesion());
			List<IseriesAppef01> datosFaltantes = IseriesAppef01DAO.listPersona(Integer.parseInt(anno), Integer.parseInt(mes),l);
			accionweb.agregarObjeto("listRut", datosFaltantes);
			System.out.print("\n punto de control 1.3.2:");
			System.out.print("\n largo de la lista: " +l.isEmpty()+"/"+l.size());
			if (l.isEmpty() || l.size()== 0) {
				System.out.print("\n punto de control 1.3.2.1:");
				regPro.setRegProFinalizado("F");
				accionweb.agregarObjeto("integrado", 1);
				ArrayList<String[]> resuta = HibernateInterface2Dao.resumenNomina(Integer.parseInt(mes), Integer.parseInt(anno));
				ArrayList<ResumenNomina> resutado = HibernateInterface2Dao.resumenNomina(Integer.parseInt(mes), Integer.parseInt(anno),true);
				HashMap<Integer, CampusEQ> eq = HibernateGurfeedDao.campusEq();
				HashMap<Integer, List<ResumenNomina>> resul = new HashMap<Integer, List<ResumenNomina>>();
				
				this.subDividirResumenNomina(resul,resutado);
				
				accionweb.agregarObjeto("equivalencia", eq);
				accionweb.agregarObjeto("resultado", resul.values());
				String tablas = ModuloInterface2.tablaNominas(resuta);			
				
				accionweb.agregarObjeto("countNomina", tablas);
				System.out.print("\n punto de control 1.3.2.2:");
			} else {
				System.out.print("\n punto de control 1.3.3.1:");
				regPro.setRegProFinalizado("C");
				ProcesoIncidencia proInc = new ProcesoIncidencia();
				proInc.setRegProIdEjecucion(regPro.getRegproIdEjecucion());
				proInc.setCodProceso(regPro.getCodProceso());
				proInc.setCodEtapaProceso(regPro.getCodEtapaProceso());
				proInc.setIncId(51);
				proInc.setProIncLocalizacion("error proceso etapa 2 (gurfeed)");
				ProcesoIncidenciaDAO proIncDAO = new ProcesoIncidenciaDAO();
				proIncDAO.insert(proInc);
				accionweb.agregarObjeto("integrado", 0);
				System.out.print("\n punto de control 1.3.3.2:");
			}
			System.out.print("\n punto de control 1.3.4.2:");
			regPro.setCodEtapaProceso(2);
			listRegPro.put("" + regPro.getCodEtapaProceso(),new RegistroProceso(regPro));
			regPro.cargarEtapa();
			regDao.updateRegistroProceso(regPro);

			accionweb.getSesion().setAttribute("registroProceso", regPro);
			System.out.print("\n punto de control 1.3.4.3:");
			if (regPro.getRegProFinalizado().equals("F")) {
				System.out.print("\n punto de control f1.3.2.1:");
				regPro.setCodEtapaProceso(regPro.getCodEtapaProceso() + 1);
				regPro.setRegProFinalizado("E");
				listRegPro.put("" + regPro.getCodEtapaProceso(),new RegistroProceso(regPro));
				System.out.print("\n punto de control f1.3.2.2:");
			}
			System.out.print("\n punto de control final 1:");
			EstrucuturaHtml etHtml = new EstrucuturaHtml();
			//String Tabla = etHtml.tableEtapa(listRegPro, accionweb);
			//accionweb.agregarObjeto("tabla", Tabla);
		}
		System.out.print("\n fin procesoNominaGurfeed \n");
	}
	
	private void subDividirResumenNomina(HashMap<Integer, List<ResumenNomina>> retorno,ArrayList<ResumenNomina> datos){
		
		for (ResumenNomina strings : datos) {
			if(retorno.get(Util.validaParametro(strings.getCodsuc(), 0))==null){
				
				retorno.put(Util.validaParametro(strings.getCodsuc(), 0), new ArrayList<ResumenNomina>());
				retorno.get(Util.validaParametro(strings.getCodsuc(), 0)).add(strings);
				
			}else{
				retorno.get(Util.validaParametro(strings.getCodsuc(), 0)).add(strings);				
			}
		}
		
	}
	
	public void ejctGurfeed(AccionWeb accionweb) {
		System.out.print("\n inicio ejctGurfeed \n");
		HibernateGurfeedDao hiberGur2DAO = new HibernateGurfeedDao();
		RegistroProceso regPro = (RegistroProceso) accionweb.getSesion().getAttribute("registroProceso");
		RegistroProcesoDAO regDAO = new RegistroProcesoDAO();
		regPro.setCodEtapaProceso(3);
		regPro.setRegProFinalizado("C");
		regDAO.insertRegistroProceso(regPro);
		boolean fin = hiberGur2DAO.gurfeed(accionweb.getSesion());
		accionweb.agregarObjeto("fin", fin);
		RegistroIncidenciaDAO regIncDAO = new RegistroIncidenciaDAO();
		Collection<RegistroIncidencia> l = regIncDAO.listRegInc(regPro);
		// this.moduloGurfeed.CargaPreGurfeedNoCumple();
		accionweb.agregarObjeto("Errores", l);
		if (l.isEmpty()) {
			regPro.setRegProFinalizado("F");
			PreGurfeedDAO preDao = new PreGurfeedDAO();
			List<ResumenPorCuenta> totales = preDao.totales(regPro.getRegproIdEjecucion());
			
			//String tabla = EstrucuturaHtml.tablaTotales(regPro.getRegproIdEjecucion());
			accionweb.agregarObjeto("toolNumber", new NumberTool());
			//System.out.print((new NumberTool()).format("currency",452167,new Locale("es","CL")));
			accionweb.agregarObjeto("locale", new Locale("es","CL"));
			accionweb.agregarObjeto("totales", totales);
			//accionweb.agregarObjeto("tabla", tabla);
		} else {
			regPro.setRegProFinalizado("C");
			// aqui se colocara la insersion a REGISTRO_INCIDENCIA
			ProcesoIncidencia proInc = new ProcesoIncidencia();
			proInc.setRegProIdEjecucion(regPro.getRegproIdEjecucion());
			proInc.setCodProceso(regPro.getCodProceso());
			proInc.setCodEtapaProceso(regPro.getCodEtapaProceso());
			proInc.setIncId(50);
			proInc.setProIncLocalizacion("error proceso etapa 3 (gurfeed)");
			ProcesoIncidenciaDAO proIncDAO = new ProcesoIncidenciaDAO();
			proIncDAO.insert(proInc);
			String ErrorTablas = "";
			System.out.print("antes de generar tablas ");
			/*
			for (RegistroIncidencia registroIncidencia : l) {
				ErrorTablas += EstrucuturaHtml.impriTabla(registroIncidencia,accionweb);
			}*/
			System.out.print("antes de generar tablas ");
			accionweb.agregarObjeto("erroresTablas", ErrorTablas);
		}
		regDAO.updateRegistroProceso(regPro);
		System.out.print("\n final ejctGurfeed \n");
	}

	public void pasarGurfeed(AccionWeb accionweb) {
		int numEje = ((RegistroProceso) accionweb.getSesion().getAttribute(	"registroProceso")).getRegproIdEjecucion();
		String mes = "" + accionweb.getSesion().getAttribute("mes");
		mes = "00".substring(mes.length()) + mes;
		// String anno = ""+accionweb.getSesion().getAttribute("anno");
		HibernateGurfeedDao hiberGur2DAO = new HibernateGurfeedDao();
		String fin[] = hiberGur2DAO.pasarGurfeed(Integer.parseInt(""
				+ accionweb.getSesion().getAttribute("mes")), Integer
				.parseInt("" + accionweb.getSesion().getAttribute("anno")),
				numEje);

		RegistroProceso regPro = (RegistroProceso) accionweb.getSesion()
				.getAttribute("registroProceso");
		regPro.setCodEtapaProceso(4);
		regPro.setRegProFinalizado("C");
		RegistroProcesoDAO regProDAO = new RegistroProcesoDAO();
		regProDAO.insertRegistroProceso(regPro);

		accionweb.agregarObjeto("fin", fin[0]);

		if (fin[0].equals("0")) {
			ProcesoIncidencia proInc = new ProcesoIncidencia();
			proInc.setRegProIdEjecucion(regPro.getRegproIdEjecucion());
			proInc.setCodProceso(regPro.getCodProceso());
			proInc.setCodEtapaProceso(regPro.getCodEtapaProceso());
			proInc.setIncId(52);
			proInc.setProIncLocalizacion("error proceso etapa 3 (gurfeed)");
		} else if (fin[0].equals("3")) {
			ProcesoIncidencia proInc = new ProcesoIncidencia();
			proInc.setRegProIdEjecucion(regPro.getRegproIdEjecucion());
			proInc.setCodProceso(regPro.getCodProceso());
			proInc.setCodEtapaProceso(regPro.getCodEtapaProceso());
			proInc.setIncId(53);
			proInc.setProIncLocalizacion("error proceso etapa 3 (gurfeed)");
		} else {
			regPro.setRegProFinalizado("F");
			accionweb.agregarObjeto("nomina", fin[1]);
			accionweb.agregarObjeto("ajuste", fin[2]);
		}
		regProDAO.updateRegistroProceso(regPro);
	}
}
