package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.ProcesoIncidencia;
public class ProcesoIncidenciaDAO {

	

	public ProcesoIncidenciaDAO() {
		
	}
	
	public boolean insert(ProcesoIncidencia proInc){
		ConexionBannerRRHH con =   new ConexionBannerRRHH();
		CallableStatement sent = null;
	   
	    boolean finalizado = false;
		
	    String sql = "{ call PKG_CONSULTA_USUARIO_RRHH.INSERT_PROCESO_INCIDENCIA( "
																		    +" ? ," //PI_REGPRO_ID_EJECUCION => PI_REGPRO_ID_EJECUCION,
																		    +" ? ," //PI_COD_ETAPA_PROCESO => PI_COD_ETAPA_PROCESO,
																		    +" ? ," //PI_INC_ID => PI_INC_ID,
																		    +" ? ," //PI_COD_PROCESO => PI_COD_PROCESO,
																		    +" ? " //PI_PROINC_LOCALIZACION => PI_PROINC_LOCALIZACION
																		  +")}";
		
		try {
			
			sent = con.getConexion().prepareCall(sql);
			sent.setInt(1, proInc.getRegProIdEjecucion());
			sent.setInt(2, proInc.getCodEtapaProceso());
			sent.setInt(3, proInc.getIncId());
			sent.setInt(4, proInc.getCodProceso());
			sent.setString(5, proInc.getProIncLocalizacion());			
			sent.execute();
			finalizado = true;
			
		    sent.close();
		    con.close();
			
		} catch (Exception e) {
			System.out.println("Error ProcesoIncidenciaDAO.insert: " + e);
			finalizado = false;
		}
		
		
		return finalizado;
	}
	
	
}
