package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import oracle.jdbc.OracleTypes;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.TipoProceso;

public class TipoProcesoDAO {

		public TipoProcesoDAO(){
			
			
		}
		//optener Tipo de proceso por ID
		public boolean buscarID(TipoProceso idTipoProceso){
			
			boolean respuesta = true;
			ConexionBannerRRHH con = new ConexionBannerRRHH();
			CallableStatement sent = null;
		    ResultSet res ;
			String sql = "{call PKG_CONSULTA_USUARIO_RRHH.BUSCAR_TIPO_PROCESO_ID("
																		    +" ? , " //PI_COD_PROCESO => PI_COD_PROCESO,
																		    +" ?  " //PO_PROCESO => PO_PROCESO
																		    +" )} "; 
			
			try {
				
				sent = con.getConexion().prepareCall(sql);
				sent.setInt   (1, idTipoProceso.getCodProceso());
				sent.registerOutParameter(2, OracleTypes.CURSOR);
				sent.execute();
				/** modificación 21-02-2018 por: Yonis Vergara */
		    	//rs = ((OracleCallableStatement)sent).getCursor(2);
				res = (ResultSet)sent.getObject(2);
				if(res.next()){
					
					idTipoProceso.setNomProceso(res.getString(1));					
				}
				respuesta = true;
				res.close();
			    sent.close();
			    con.close();
				
			} catch (Exception e) {
				System.out.println("Error TipoPrecesoDAO.buscarID("+idTipoProceso.getCodProceso()+"): " + e);
				respuesta = false;
			}
			
			return respuesta;
		}
}
