package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import oracle.jdbc.OracleTypes;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.EtapaProceso;
import cl.utfsm.rrhhG.modulo.TipoProceso;

public class EtapaProcesoDAO {

		public EtapaProcesoDAO(){
			
		}
		public boolean buscarProEtapa(EtapaProceso etapaPro){
			boolean respuesta = true;
			ConexionBannerRRHH con = new ConexionBannerRRHH();
			CallableStatement sent = null;
		    ResultSet res ;
		    TipoProcesoDAO tipProDAO = new TipoProcesoDAO();
		    tipProDAO.buscarID(etapaPro.getTipoProceso());
			String sql = " { call PKG_CONSULTA_USUARIO_RRHH.BUSCAR_ETAPA_PROCESO_ID("
																			   +" ? ,"// PI_COD_PROCESO => PI_COD_PROCESO,
																			   +" ? ,"// PI_COD_ETAPA_PROCESO => PI_COD_ETAPA_PROCESO,
																			   +" ? ,"// PO_ETAPA_PROCESO => PO_ETAPA_PROCESO
																			   +");"; 
			
			try {
				
				sent = con.getConexion().prepareCall(sql);
				sent.setInt   (1, etapaPro.getCodEtapaProceso());
				sent.setInt   (2, etapaPro.getTipoProceso().getCodProceso());
				sent.registerOutParameter(3, OracleTypes.CURSOR);				
				sent.executeQuery();
				/** modificación 21-02-2018 por: Yonis Vergara */
				// res = ((OracleCallableStatement)sent).getCursor(3);
				res = (ResultSet)sent.getObject(3);
				if(res.next()){
					
					etapaPro.setNomEtapaProceso(res.getString(1));		
					//etapaPro.setDesEtapaProceso(res.getString(2));
				}
				respuesta = true;
				res.close();
			    sent.close();
			    con.close();
				
			} catch (Exception e) {
				System.out.println("Error EtapaProcesoDAO.buscarProEtapa("+etapaPro.getCodEtapaProceso()+","+etapaPro.getTipoProceso().getCodProceso()+"): " + e);
				respuesta = false;
			}
			
			return respuesta;
		}
		public ArrayList<EtapaProceso> buscarEtapas(int idTipoProceso){
			
			ConexionBannerRRHH con = new ConexionBannerRRHH();
			CallableStatement sent = null;
		    ResultSet res ;
		   
			//String sql = "select COD_ETAPA_PROCESO,NOM_ETAPA_PROCESO,DES_ETAPA_PROCESO from ETAPA_PROCESO where COD_PROCESO = ? order by COD_ETAPA_PROCESO "; 
		    String sql = "{call PKG_CONSULTA_USUARIO_RRHH.BUSCAR_ETAPA_PROCESO("
																		      +" ? ,"//PI_COD_PROCESO => PI_COD_PROCESO,
																		      +" ? "//PO_ETAPAS_PROCESO => PO_ETAPAS_PROCESO
																		      +")} ";
		    ArrayList<EtapaProceso> lisEtaPro = new ArrayList<EtapaProceso>();
			try {
				TipoProcesoDAO tipProDAO = new TipoProcesoDAO();
				sent = con.getConexion().prepareCall(sql);
				sent.setInt   (1, idTipoProceso);
				sent.registerOutParameter(2, OracleTypes.CURSOR);				
				EtapaProceso etaPr = new EtapaProceso();
				TipoProceso tipPro = new TipoProceso();
				tipPro.setCodProceso(idTipoProceso);
				tipProDAO.buscarID(tipPro);
				sent.executeQuery();
				/** modificación 21-02-2018 por: Yonis Vergara */
				//res = ((OracleCallableStatement)sent).getCursor(2);
				res = (ResultSet)sent.getObject(2);
				while(res.next()){
					etaPr = new EtapaProceso();
					
					etaPr.setCodEtapaProceso(res.getInt(1));
					etaPr.setNomEtapaProceso(res.getString(2));
					//etaPr.setDesEtapaProceso(res.getString(3));
					etaPr.setTipoProceso( new TipoProceso(tipPro));	
					lisEtaPro.add(etaPr);
				}
				
				res.close();
			    sent.close();
			    con.close();
				
			} catch (Exception e) {
				System.out.println("Error EtapaProcesoDAO.buscarEtapas("+idTipoProceso+"): " + e);
				
			}
			
			return lisEtaPro;
		}
}
