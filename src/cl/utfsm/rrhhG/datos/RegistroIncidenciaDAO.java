package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import oracle.jdbc.OracleTypes;


import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.Incidencia;
import cl.utfsm.rrhhG.modulo.RegistroIncidencia;
import cl.utfsm.rrhhG.modulo.RegistroProceso;

public class RegistroIncidenciaDAO {

	public RegistroIncidenciaDAO() {
		
	}

	public RegistroIncidencia opRegInc(RegistroProceso regPro , Incidencia inc){
		
		String sql = "select PREGUR_ID, REGINC_LOCALIZACION from REGISTRO_INCIDENCIA where INC_ID = ? and REGPRO_ID_EJECUCION = ? and COD_PROCESO = ? and COD_ETAPA_PROCESO = ?";
		
		RegistroIncidencia regInc = new RegistroIncidencia();
		regInc.setIncidencia(inc);
		
		ConexionBannerRRHH con =   new ConexionBannerRRHH();
		PreparedStatement sent = null;
	    ResultSet res = null;
	    
	    
	    
		try {
			
			sent = con.getConexion().prepareStatement(sql);
			sent.setInt(1, inc.getIncId());
			sent.setInt(2, regPro.getRegproIdEjecucion());		
			sent.setInt(3, regPro.getCodProceso());	
			sent.setInt(4, regPro.getCodEtapaProceso());	
			
			
			
			res = sent.executeQuery();
			while(res.next()){
				
				//regInc.agregarPreGurfeed(preGur.getPreGurfeed(res.getInt(1), regPro));
				regInc.setRegPro(regPro);
				regInc.setRegincLocalizacion(res.getString(2));				
				
			}
			
			res.close();
		    sent.close();
		    con.close();
			
		} catch (Exception e) {
			System.out.println("Error RegistroIncidencia.opRegInc: " + e);
			
		}
		
			
		
		return regInc;
	}
	
	
	
	
    public Collection<RegistroIncidencia> listRegInc (RegistroProceso regPro){
		
		
		Collection<RegistroIncidencia> l = new ArrayList<RegistroIncidencia>();
		
		String sql = "{call PKG_CONSULTA_USUARIO_RRHH.REGISTRO_INCIDENCIA_LISTA("
																		    +" ? , "//PI_COD_PROCESO => PI_COD_PROCESO,
																		    +" ? , "//PI_COD_ETAPA_PROCESO => PI_COD_ETAPA_PROCESO,
																		    +" ? , "//PI_ID_EJECUCION => PI_ID_EJECUCION,
																		    +" ?  "//PO_REGISTROS_INC => PO_REGISTROS_INC
																		    +" )} ";				
		ConexionBannerRRHH con =   new ConexionBannerRRHH();
		CallableStatement sent = null;
	    ResultSet res = null;
	    IncidenciaDAO incDAO =new IncidenciaDAO ();
	    
	    try {
			
			sent = con.getConexion().prepareCall(sql);
			
			sent.setInt(1, regPro.getCodProceso());		
			sent.setInt(2, regPro.getCodEtapaProceso());
			sent.setInt(3, regPro.getRegproIdEjecucion());
			sent.registerOutParameter(4, OracleTypes.CURSOR);
			Incidencia inc = new Incidencia();
			sent.executeQuery();
			/** modificación 21-02-2018 por: Yonis Vergara */	    	
			res = (ResultSet)sent.getObject(4);
			RegistroIncidencia regInc = null;
			while(res.next()){
				
				inc = incDAO.incidencia(res.getInt(1));
				regInc = new RegistroIncidencia();
				regInc.setIncidencia(inc);
				regInc.setRegPro(regPro);
				regInc.cargarListPreGurfeed();
				l.add(regInc);				
			}
			
			res.close();
		    sent.close();
		    con.close();
			
		} catch (Exception e) {
			System.out.println("Error RegistroIncidenciaDAO.listRegInc: " + e);			
		}	
		return l;
	}
	
	
	
	
}
