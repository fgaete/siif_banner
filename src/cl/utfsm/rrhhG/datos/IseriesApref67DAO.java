package cl.utfsm.rrhhG.datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cl.utfsm.conexion.ConexionAs400RRHHG;
import cl.utfsm.conexion.ConexionAs400RRHHI1;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.IseriesApref67;

public class IseriesApref67DAO {

	public static List<IseriesApref67> todosPorMes(int anoPro , int mesPro ,String nomConeccion){
		
		Connection conIseries ;
		if(nomConeccion.equals("GURFEED")){
			conIseries  = (new ConexionAs400RRHHG()).generaConnection();
		}else if (nomConeccion.equals("INTERFACE1")) {
			conIseries  = (new ConexionAs400RRHHI1()).generaConnection();
		}else{
			return null;
		}
		
		List<IseriesApref67> list = new ArrayList<IseriesApref67>();
		String sql = "select * from remunerabt.apref67 where anopro = ? and mespro = ? ";
		
		try {
			PreparedStatement sent = conIseries.prepareStatement(sql);
			sent.setInt(1, anoPro);
			sent.setInt(2, mesPro);
			ResultSet rs = sent.executeQuery();
			while (rs.next()) {
				IseriesApref67 boletasParadas = new IseriesApref67();
				boletasParadas.setMesPro(rs.getInt("MESPRO"));
				boletasParadas.setAnoPro(rs.getInt("ANOPRO"));
				boletasParadas.setRutide(rs.getInt("RUTIDE"));
				boletasParadas.setDigide(rs.getString("DIGIDE"));
				boletasParadas.setTipPro(rs.getString("TIPPRO"));
				boletasParadas.setValPag(rs.getInt("VALPAG"));
				boletasParadas.setNumVal(rs.getInt("NUMVAL"));
				boletasParadas.setFecAut(rs.getInt("FECAUT"));
				boletasParadas.setFecEnv(rs.getInt("FECENV"));
				boletasParadas.setNumDoc(rs.getInt("NUMDOC"));
				list.add(boletasParadas);
			}
			
		} catch (SQLException e) {
			System.out.print("SQLError : IseriesApref67DAO.todosPorMes :"+e.getMessage());
		} catch (Exception e) {
			System.out.print("Error : IseriesApref67DAO.todosPorMes :"+e.getMessage());
		}	
		try {
			conIseries.close();
		} catch (SQLException e) {
			System.out.print("SQLError : IseriesApref67DAO.todosPorMes :"+e.getMessage());
		} catch (Exception e) {
			System.out.print("Error : IseriesApref67DAO.todosPorMes :"+e.getMessage());
		}
		return list;
	}
	/*************************************************************************************
	 *******************************IMPORTANTE********************************************
	 *Este metodo no cierra la conexcion para no perder la session esta tabla es Temporal*
	 *******************************IMPORTANTE********************************************
	 *************************************************************************************
	 */
	public static void guardarBanner(List<IseriesApref67> list ,ConexionBannerRRHH con){
		
		String sql = "insert into TMP_APREF67(MESPRO,ANOPRO,RUTIDE,DIGIDE,TIPPRO,VALPAG,NUMVAL,FECAUT,FECENV,NUMDOC)values(?,?,?,?,?,?,?,?,?,?)";
		
		try {
			PreparedStatement sent = con.getConexion().prepareStatement(sql);
			for (IseriesApref67 bole : list) {
				sent.setInt(1,bole.getMesPro());
				sent.setInt(2,bole.getAnoPro());
				sent.setInt(3,bole.getRutide());
				sent.setString(4,bole.getDigide());
				sent.setString(5,bole.getTipPro());
				sent.setInt(6,bole.getValPag());
				sent.setInt(7,bole.getNumVal());
				sent.setInt(8,bole.getFecAut());
				sent.setInt(9,bole.getFecEnv());
				sent.setInt(10,bole.getNumDoc());
				sent.addBatch();
			}
			sent.executeBatch();
			sent.clearBatch();
			sent.close();
			
		} catch (SQLException e) {
			System.out.print("SQLError : IseriesApref67DAO.todosPorMes :"+e.getMessage());
		} catch (Exception e) {
			System.out.print("Error : IseriesApref67DAO.todosPorMes :"+e.getMessage());
		}
		
	}
	
}
