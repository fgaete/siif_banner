package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


import oracle.jdbc.OracleTypes;


import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.Incidencia;

public class IncidenciaDAO {

	public IncidenciaDAO(){
		
	}
	
	public Incidencia incidencia(int id){
		
		Incidencia inc = new Incidencia();
		
		ConexionBannerRRHH con =   new ConexionBannerRRHH();
		CallableStatement sent = null;
	    ResultSet res = null;	    
		
	    String sql = "{call PKG_CONSULTA_USUARIO_RRHH.BUSCAR_INCIDENCIA_ID( "
																	   +" ? , "// PI_ID_INCIDENCIA => PI_ID_INCIDENCIA,
																	   +" ?  "// PO_INCIDENCIA => PO_INCIDENCIA
																	   +" )} ";
		try {
			
			sent = con.getConexion().prepareCall(sql);
			sent.setInt(1, id);	
			sent.registerOutParameter(2, OracleTypes.CURSOR);
			sent.executeQuery();
			/** modificación 21-02-2018 por: Yonis Vergara */
			//res = ((OracleCallableStatement)sent).getCursor(2);
			res = (ResultSet)sent.getObject(2);
			
			if(res.next()){
				inc.setIncId(id);
				inc.setIncDescripcion(res.getString(2));
				inc.setIncDescripcionCorta(res.getString(3));
			}
			
			res.close();
		    sent.close();
		    con.close();
			
		} catch (Exception e) {
			System.out.println("Error IncidenciaDAO.incidencia: " + e);
			
		}
		
		return inc ;
		
	}
	public Incidencia incidencia(int id , ConexionBannerRRHH con2){
		
		Incidencia inc = new Incidencia();
		
		ConexionBannerRRHH con =   con2;
		PreparedStatement sent = null;
	    ResultSet res = null;	    
		
		String sql = "select INC_ID , INC_DESCRIPCION , INC_DESCRIPCION_CORTA from INCIDENCIA where INC_ID = ?";
		
		try {
			
			sent = con.getConexion().prepareStatement(sql);
			sent.setInt(1, id);		
			res = sent.executeQuery();
			if(res.next()){
				inc.setIncId(id);
				inc.setIncDescripcion(res.getString(2));
				inc.setIncDescripcionCorta(res.getString(3));
			}			
		} catch (Exception e) {
			System.out.println("Error IncidenciaDAO.incidencia: " + e);
			
		}
		
		return inc ;
		
	}
}
