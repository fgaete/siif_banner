package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cl.utfsm.conexion.ConexionAs400RRHHG;
import cl.utfsm.conexion.ConexionAs400RRHHI1;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.IseriesApref08;
import cl.utfsm.rrhhG.modulo.IseriesApref41;

public class IseriesApref41DAO {

	public static List<IseriesApref41> listAjustePresupuesto(int anoPro,int mesPro,String nomConeccion){
		Connection conIseries ;
		if(nomConeccion.equals("GURFEED")){
			conIseries  = (new ConexionAs400RRHHG()).generaConnection();
		}else if (nomConeccion.equals("INTERFACE1")) {
			conIseries  = (new ConexionAs400RRHHI1()).generaConnection();
		}else{
			return null;
		}
		List<IseriesApref41> list = new ArrayList<IseriesApref41>();
		String sql = "select * from remunerabt.apref41B where anoPro = ? and mesPro = ? ";
		
		try {
			PreparedStatement sent = conIseries.prepareStatement(sql);
			sent.setInt(1, anoPro);
			sent.setInt(2, mesPro);
			ResultSet rs = sent.executeQuery();
			IseriesApref41 ajustesPre ;
			while (rs.next()) {
				ajustesPre = new IseriesApref41();
				ajustesPre.setAnoPro(rs.getInt("ANOPRO"));
				ajustesPre.setMesPro(rs.getInt("MESPRO"));
				ajustesPre.setRutide(rs.getInt("RUTIDE"));
				ajustesPre.setDigide(rs.getString("DIGIDE"));
				ajustesPre.setTipCon(rs.getInt("TIPCON"));
				ajustesPre.setNumCom(rs.getInt("NUMCON"));
				ajustesPre.setCodHde(rs.getInt("CODHDE"));
				ajustesPre.setUniCar(rs.getString("UNICAR"));
				ajustesPre.setUniAbo(rs.getString("UNIABO"));
				ajustesPre.setValCar(rs.getInt("VALCAR"));
				ajustesPre.setValAbo(rs.getInt("VALABO"));
				ajustesPre.setCanHor(rs.getInt("CANHOR"));
				ajustesPre.setNomIde(rs.getString("NOMIDE"));
				list.add(ajustesPre);
			}
		} catch (SQLException e) {
			System.out.print("SQLError : IseriesApref41DAO.listAjustePresupuesto :"+e.getMessage());
		} catch (Exception e) {
			System.out.print("Error : IseriesApref41DAO.listAjustePresupuesto :"+e.getMessage());
		}
		try {
			conIseries.close();
		} catch (SQLException e) {
			System.out.print("SQLError : IseriesApref41DAO.listAjustePresupuesto :"+e.getMessage());
		} catch (Exception e) {
			System.out.print("Error : IseriesApref41DAO.listAjustePresupuesto :"+e.getMessage());
		}
		return list;
	}
	/*************************************************************************************
	 *******************************IMPORTANTE********************************************
	 *Este metodo no cierra la conexcion para no perder la session esta tabla es Temporal*
	 *******************************IMPORTANTE********************************************
	 *************************************************************************************
	 */
	
	public static void guardarListAjusPres(List<IseriesApref41> listAjusPres,ConexionBannerRRHH con){
		
		String sql = "INSERT INTO TMP_APREF41(ANOPRO,MESPRO,RUTIDE,DIGIDE,TIPCON,NUMCON,CODHDE,UNICAR,UNIABO,VALCAR,VALABO,CANHOR,NOMIDE) "+
                     "values(?,?,?,?,?,?,?,?,?,?,?,?,?)"; 
	try {
		CallableStatement sent = con.getConexion().prepareCall(sql);
		
		for (IseriesApref41 ajusPres : listAjusPres) {
			sent.setInt(1,ajusPres.getAnoPro());
			sent.setInt(2,ajusPres.getMesPro());
			sent.setInt(3,ajusPres.getRutide());
			sent.setString(4,ajusPres.getDigide());
			sent.setInt(5,ajusPres.getTipCon());
			sent.setInt(6,ajusPres.getNumCom());
			sent.setInt(7,ajusPres.getCodHde());
			sent.setString(8,ajusPres.getUniCar());
			sent.setString(9,ajusPres.getUniAbo());
			sent.setInt(10,ajusPres.getValCar());
			sent.setInt(11,ajusPres.getValAbo());
			sent.setInt(12,ajusPres.getCanHor());
			sent.setString(13,ajusPres.getNomIde());
			sent.addBatch();
		}
		sent.executeBatch();
		sent.clearBatch();
		sent.close();
	} catch (SQLException e) {
		System.out.print("SQLERROR en : IsriesApref08DAO.guardarListSolAyu :\n" +e.getMessage());
	} catch (Exception e) {
		System.out.print("ERROR en : IsriesApref08DAO.guardarListSolAyu :\n"+e.getMessage());
	}
				
	}
}
