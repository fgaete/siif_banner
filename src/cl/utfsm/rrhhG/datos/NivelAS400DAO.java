package cl.utfsm.rrhhG.datos;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import cl.utfsm.conexion.ConexionAs400RRHHG;


/**
 *Nombre classe: NivelAS400DAO
 *Programado por: Yonis Vergara
 *Fecha Creacion:25-09-2017
 *Vercion: 1.0
 *Ultima Verci�n: 25-09-2017
 *Modificado por :Yonis Vergara
 *Razon de cambio:Creacion de la clase
 *Descripci�n: para hacer consulta a la tabla 
 */
public class NivelAS400DAO {

	public NivelAS400DAO(){
		
	}
	
	public HashMap<String, String> mapNivel(){
		
		  String sql = "select NUMNIV from REMUNERABT.APREF11 where CODESC <= 2";
		  
		  ConexionAs400RRHHG con = new ConexionAs400RRHHG();
		  PreparedStatement sent;	  
		  ResultSet rs;
		  HashMap<String, String> mapNivel = new HashMap<String, String>();
		  try{ 
		       sent = con.getConnection().prepareStatement(sql);
		       rs = sent.executeQuery();		       
		       try {		    	   
		    	   
		    	   while (rs.next()) {
		    		   mapNivel.put(rs.getString("NUMNIV"), rs.getString("NUMNIV"));				
		    	   }
		    	   mapNivel.put("0", "0");	
		    	   mapNivel.put("AC", "AC");	
		    	   mapNivel.put("AD", "AD");
			       rs.close();
		       } catch (SQLException e){
		    	   System.out.println("Error UsmCuentaHabDesDAO.descOrgn: " + e);
			       System.out.println("cursor" );
			   }		       
		       sent.close(); 
		       con.getConnection().close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error UsmCuentaHabDesDAO.descOrgn: " + e);
		       System.out.println("antes del cursor " );
		   }
		
		return mapNivel;
	}
}
