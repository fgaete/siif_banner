package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cl.utfsm.conexion.ConexionAs400RRHHG;
import cl.utfsm.conexion.ConexionAs400RRHHI1;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.IseriesAppef06;
import cl.utfsm.rrhhG.modulo.IseriesApref08;

public class IseriesApref08DAO {

	public static List<IseriesApref08> listCargos(String nomConeccion){
		
		Connection conIseries ;
		if(nomConeccion.equals("GURFEED")){
			conIseries  = (new ConexionAs400RRHHG()).generaConnection();
		}else if (nomConeccion.equals("INTERFACE1")) {
			conIseries  = (new ConexionAs400RRHHI1()).generaConnection();
		}else{
			return null;
		}
		
		List<IseriesApref08> list = new ArrayList<IseriesApref08>();
		String sql = "select * From remunerabt.apref08";
		try {
			IseriesApref08 cargo;
			PreparedStatement sent = conIseries.prepareStatement(sql);
			ResultSet rs = sent.executeQuery();
			while (rs.next()) {
				cargo = new IseriesApref08();
				cargo.setCodPla(rs.getInt("CODPLA"));
				cargo.setCodCar(rs.getInt("CODCAR"));
				cargo.setDesCar(rs.getString("DESCAR"));
				cargo.setNivGra(rs.getInt("NIVGRA"));
				cargo.setFamCarCod(rs.getString("FAMCARCOD"));
				cargo.setPueBan(rs.getString("PUEBAN"));
				list.add(cargo);
			}
			
			
		} catch (SQLException e) {
			System.out.print("SQLError : IseriesApref08DAO.listCargos :" +e.getMessage());
		} catch (Exception e) {
			System.out.print("Error : IseriesApref08DAO.listCargos :" +e.getMessage());
		}
		try {
			conIseries.close();
		} catch (SQLException e) {
			System.out.print("SQLError : IseriesApref08DAO.listCargos :" +e.getMessage());
		} catch (Exception e) {
			System.out.print("Error : IseriesApref08DAO.listCargos :" +e.getMessage());
		}
		return list;
	}
	/*************************************************************************************
	 *******************************IMPORTANTE********************************************
	 *Este metodo no cierra la conexcion para no perder la session esta tabla es Temporal*
	 *******************************IMPORTANTE********************************************
	 *************************************************************************************
	 */
	
	public static void guardarListPlanta(List<IseriesApref08> listPlanta,ConexionBannerRRHH con){
		
		String sql = "INSERT INTO TMP_APREF08(CODPLA,CODCAR,DESCAR,NIVGRA,FAMCARCOD,PUEBAN) "+
                      " values(?,?,?,?,?,?)"; 
	try {
		CallableStatement sent = con.getConexion().prepareCall(sql);
		for (IseriesApref08 planta : listPlanta) {
			sent.setInt(1,planta.getCodPla());
			sent.setInt(2,planta.getCodCar());
			sent.setString(3,planta.getDesCar());
			sent.setInt(4,planta.getNivGra());
			sent.setString(5,planta.getFamCarCod());
			sent.setString(6,planta.getPueBan());
			sent.addBatch();
		}
		sent.executeBatch();
		sent.clearBatch();
		sent.close();
	} catch (SQLException e) {
		System.out.print("SQLERROR en : IsriesApref08DAO.guardarListSolAyu :\n" +e.getMessage());
	} catch (Exception e) {
		System.out.print("ERROR en : IsriesApref08DAO.guardarListSolAyu :\n"+e.getMessage());
	}
				
	}
}
