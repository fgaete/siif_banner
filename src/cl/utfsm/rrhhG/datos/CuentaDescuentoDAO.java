package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.internal.OracleTypes;

import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.CuentaDescuento;
import cl.utfsm.rrhhG.modulo.CuentaHaberes;

public class CuentaDescuentoDAO {
	private static List<CuentaDescuento> buscarHaberes(int codigo ){
		
		String sql = "{call PKG_CONSULTA_USUARIO_RRHH.CONSULTA_CUENTA_DESCUENTO( "+
						    " PI_CODIGO => ? , "+
						    " PO_CUENTA_DESCUENTO => ? "+
						   " )}";
		List<CuentaDescuento> listHaberes = new ArrayList<CuentaDescuento>();
		try {
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		CallableStatement sent = con.getConexion().prepareCall(sql);
		
		sent.setInt(1, codigo);		
		sent.registerOutParameter(2,OracleTypes.CURSOR);		
		sent.execute();		
		ResultSet rs = (ResultSet)sent.getObject(2);
		CuentaDescuento cuentaDescuento = null;
		
		while (rs.next()) {
			cuentaDescuento = new CuentaDescuento(); 
			cuentaDescuento.setCodigo(rs.getInt("CODIGO"));
			cuentaDescuento.setPlanta(rs.getString("PLANTA"));
			cuentaDescuento.setNivel(rs.getString("NIVEL"));
			cuentaDescuento.setCuenta(rs.getString("CUENTA"));
			cuentaDescuento.setOrganizacion(rs.getString("ORGANIZACION"));
			cuentaDescuento.setFondo(rs.getString("FONDO"));
			cuentaDescuento.setPrograma(rs.getString("PROGRAMA"));
			cuentaDescuento.setId(rs.getString("ID"));
			listHaberes.add(cuentaDescuento);
		}		
		con.getConexion().close();
		} catch (SQLException e) {
			System.out.print("SQLError CuentaHaberesDAO.buscarHaberes: "+e.getMessage());
		} catch (Exception e) {
			System.out.print("Error CuentaHaberesDAO.buscarHaberes: "+e.getMessage());
		}	
		return listHaberes;
		
	}
	public static List<CuentaDescuento> getListDescuentos(){
		return buscarHaberes(0);
	}
	public static List<CuentaDescuento> getListDescuentos(int codigo){
		return buscarHaberes(codigo);
	}
	public static CuentaDescuento findOneByCodigo(int codigo){
		
		List<CuentaDescuento> list = buscarHaberes(codigo);
		
		if(list.size()== 1){
			return list.get(0);
		}		
		return null;
	}
	/**
	 * fecha de creacion:03-09-2018
	 * @author yonis.vergara 
	 * descripcion : crear o actualizar una regla de calculo de cuenta para descuento
	 * @param tipo
	 * @param descu
	 */
	private static boolean guardarCambios(String tipo,CuentaDescuento descu ){
		
		String sql = "{ CALL PKG_CONSULTA_USUARIO_RRHH.GUADAR_REGLA_DESCUENTO( "+
												"PI_CODIGO => ? ,"+ //1
												"PI_PLANTA => ? ,"+ //2
												"PI_NIVEL => ? ,"+ //3
												"PI_CUENTA => ?,"+ //4
												"PI_ORGANIZACION => ? ,"+//5
												"PI_PROGRAMA => ? ,"+ //6
												"PI_FONDO => ? ,"+ //7
												"PI_ID => ?,"+ //8
												"PI_TIPO => ?"+ //9
												")}";
		ConexionBannerRRHH con = new ConexionBannerRRHH(); 
		try {
			CallableStatement sent = con.getConexion().prepareCall(sql);
			sent.setInt(1, descu.getCodigo());
			sent.setString(2, descu.getPlanta());
			sent.setString(3, descu.getNivel());
			sent.setString(4, descu.getCuenta());
			sent.setString(5, descu.getOrganizacion());
			sent.setString(6, descu.getPrograma());
			sent.setString(7, descu.getFondo());
			sent.setString(8, descu.getId());
			sent.setString(9, tipo);
			
			sent.execute();
			
			con.getConexion().close();
		} catch (SQLException e) {
			System.out.print("SQLError CuentaHaberesDAO.guardarCambios: "+e.getMessage());
			return false;
		} catch (Exception e) {
			System.out.print("Error CuentaHaberesDAO.guardarCambios: "+e.getMessage());
			return false;
		}
		return true;
    }

	public static boolean crearReglaCuentaDescuento(CuentaDescuento descu){
		return guardarCambios("INSERT", descu);
	}
	public static boolean actualizaReglaCuentaDescuento(CuentaDescuento descu){
		return guardarCambios("UPDATE", descu);
	}
	
}
