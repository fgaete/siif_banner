package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import oracle.jdbc.OracleTypes;
import cl.utfsm.conexion.ConexionAs400RRHHG;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.UsmCuentaHabDes;

public class UsmCuentaHabDesDAO {
	
	public UsmCuentaHabDesDAO (){}
	
	public UsmCuentaHabDes optenerPorID(String reglaCuentaRowID){
		
		String sql = "{ call PKG_CONSULTA_USUARIO_RRHH.BUSCAR_REGLA_CUENTA_POR_ROWID(" +
																			 " ?," +// P_ROWID IN VARCHAR2
																			 " ? " +// PO_REGLA_CUENTA OUT CURSOR
																			 ")}";
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		CallableStatement sent = null;
		UsmCuentaHabDes cuentaHabDes = null ;	
		try {
			sent = con.getConexion().prepareCall(sql);
			sent.setString(1, reglaCuentaRowID);
			sent.registerOutParameter(2, OracleTypes.CURSOR);
			sent.execute();
			/** modificaci�n 26-02-2018 por: Yonis Vergara */
	    	//rs = ((OracleCallableStatement)sent).getCursor(2);
			ResultSet rs = (ResultSet)sent.getObject(2);
			while (rs.next()) {
				
				   cuentaHabDes = new UsmCuentaHabDes();
		    	   cuentaHabDes.setUsmHabDesCodhde(rs.getInt("USMHABDES_CODHDE"));
		    	   cuentaHabDes.setUsmHabDesCodpla(rs.getString("USMHABDES_CODPLA"));
		    	   cuentaHabDes.setUsmHabDesNumniv(rs.getString("USMHABDES_NUMNIV"));
		    	   cuentaHabDes.setUsmHabDesCuentaBanner(rs.getString("USMHABDES_CUENTA_BANNER"));
		    	   cuentaHabDes.setUsmHabDesOrgnBanner(rs.getString("USMHABDES_ORGN_BANNER"));
		    	   cuentaHabDes.setUsmHabDesObs(rs.getString("USMHABDES_OBS"));
		    	   cuentaHabDes.setUsmHabDesCodVigencia(rs.getInt("USMHABDES_COD_VIGENCIA"));
		    	   cuentaHabDes.setUsmHabDesFechIni(rs.getDate("USMHABDES_FECH_INI"));
		    	   cuentaHabDes.setUsmHabDesFechFin(rs.getDate("USMHABDES_FECH_FIN"));
		    	   cuentaHabDes.setUsmHabDesRutide(rs.getInt("USMHABDES_RUTIDE"));
		    	   cuentaHabDes.setUsmHabDesPidm(rs.getInt("USMHABDES_PIDM"));
		    	   cuentaHabDes.setRowid(rs.getString("ROWID"));
			}
		}catch (SQLException e) {
			System.out.println("Error SQL UsmCuentaHabDesDAO.optenerPorID: " + e);
		} catch (Exception e) {
			System.out.println("Error UsmCuentaHabDesDAO.optenerPorID: " + e);
		}		
		
		return cuentaHabDes;
	}
	
	/**
	 *Nombre m�todo: insertarUsmCuentaHabDes
	 *Programado por: Yonis Vergara
	 *Fecha Creacion:11-10-2017
	 *Vercion: 1.0
	 *Ultima Verci�n: 11-10-2017
	 *Modificado por :Yonis Vergara
	 *Razon de cambio:Gurdar la regla nueva en la base de datos
	 *Descripci�n: se encarga de guardar la regla de cuenta nueva 
	 *               en la base de datos 
	 *
	 */
	
	public void insertarUsmCuentaHabDes(UsmCuentaHabDes reglaCuenta){
		
		String sql = "{ call PKG_CONSULTA_USUARIO_RRHH.INSERTREGLACUENTA( " +
																		  " ? " +// P_CODHDE Number
																		  ",? " +// P_CODPLA Varchar
																		  ",? " +// P_NUMNIV Varchar
																		  ",? " +// P_CUENTA varchar
																		  ",? " +// P_CODORG varchar
																		  ",? " +// P_OBS varchar
																		  ",? " +// P_COD_PIDM number
																		  ",? " +// P_RUTIDE number
																		  ")}"; 
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		try {
			CallableStatement sent = con.getConexion().prepareCall(sql);
			sent.setInt(1, reglaCuenta.getUsmHabDesCodhde());
			sent.setString(2, reglaCuenta.getUsmHabDesCodpla());
			sent.setString(3, reglaCuenta.getUsmHabDesNumniv());
			sent.setString(4, reglaCuenta.getUsmHabDesCuentaBanner());
			sent.setString(5, reglaCuenta.getUsmHabDesOrgnBanner());
			sent.setString(6, reglaCuenta.getUsmHabDesObs());
			sent.setInt(7, reglaCuenta.getUsmHabDesPidm());
			sent.setInt(8, reglaCuenta.getUsmHabDesRutide());			
			sent.execute();
		} catch (SQLException e) {
			System.out.println("Error SQL UsmCuentaHabDesDAO.insertarUsmCuentaHabDes: " + e);
		}catch (Exception e) {
			System.out.println("Error UsmCuentaHabDesDAO.insertarUsmCuentaHabDes: " + e);
		}
		
		
		
	}
	
	/**
	 *Nombre m�todo: updateUsmCuentaHabDes
	 *Programado por: Yonis Vergara
	 *Fecha Creacion:11-10-2017
	 *Vercion: 1.0
	 *Ultima Verci�n: 12-10-2017
	 *Modificado por: Yonis Vergara
	 *Razon de cambio: Actualiza la regla nueva en la base de datos
	 *Descripci�n: esta deja el registro con el estodo viginte en 2 esto siginifica que ya no se utilizara
	 *				y le coloca una fecha de fin luego crea un registro nuevo 
	 *               
	 *
	 */
	
	public void updateUsmCuentaHabDes(UsmCuentaHabDes reglaCuenta){
		
		String sql = "{ call PKG_CONSULTA_USUARIO_RRHH.UPDATE_RRGLA_CUENTA( " +
																		  " ? " +// P_ROWID VARCHAR2
																		  ",? " +// P_CODHDE Number
																		  ",? " +// P_CODPLA Varchar
																		  ",? " +// P_NUMNIV Varchar
																		  ",? " +// P_CUENTA varchar
																		  ",? " +// P_CODORG varchar
																		  ",? " +// P_OBS varchar
																		  ",? " +// P_COD_PIDM number
																		  ",? " +// P_RUTIDE number
																		  ")}"; 
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		try {
			CallableStatement sent = con.getConexion().prepareCall(sql);
			sent.setString(1, reglaCuenta.getRowid());
			sent.setInt(2, reglaCuenta.getUsmHabDesCodhde());
			sent.setString(3, reglaCuenta.getUsmHabDesCodpla());
			sent.setString(4, reglaCuenta.getUsmHabDesNumniv());
			sent.setString(5, reglaCuenta.getUsmHabDesCuentaBanner());
			sent.setString(6, reglaCuenta.getUsmHabDesOrgnBanner());
			sent.setString(7, reglaCuenta.getUsmHabDesObs());
			sent.setInt(8, reglaCuenta.getUsmHabDesPidm());
			sent.setInt(9, reglaCuenta.getUsmHabDesRutide());			
			sent.execute();
		} catch (SQLException e) {
			System.out.println("Error SQL UsmCuentaHabDesDAO.updateUsmCuentaHabDes: " + e);
		}catch (Exception e) {
			System.out.println("Error UsmCuentaHabDesDAO.updateUsmCuentaHabDes: " + e);
		}
		
		
		
	}
	
	/**
	 *Nombre m�todo  : deleteUsmCuentaHabDes
	 *Programado por : Yonis Vergara
	 *Fecha Creacion : 11-10-2017
	 *Vercion        : 1.0
	 *Ultima Verci�n : 12-10-2017
	 *Modificado por : Yonis Vergara
	 *Razon de cambio: cambia el estado de vigencia del registro
	 *Descripci�n    : cambia el estado de vigencia del registro cambiadolo a un 2 y poniendo una fecha de termino 
	 *					esto establece que el registro registo esta desabilitado como regla pero 
	 *				    no se pierde podiendo verce como estava en un momento de terminado 
	 *               
	 *
	 */
	
	public void deleteUsmCuentaHabDes(UsmCuentaHabDes reglaCuenta){
		
		String sql = "{ call PKG_CONSULTA_USUARIO_RRHH.DELETE_REGLA_CUENTA( " +
																		  " ? " +// P_ROWID VARCHAR2
																		  ")}"; 
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		try {
			CallableStatement sent = con.getConexion().prepareCall(sql);
			sent.setString(1, reglaCuenta.getRowid());
			sent.execute();
		} catch (SQLException e) {
			System.out.println("Error SQL UsmCuentaHabDesDAO.deleteUsmCuentaHabDes: " + e);
		}catch (Exception e) {
			System.out.println("Error UsmCuentaHabDesDAO.deleteUsmCuentaHabDes: " + e);
		}
		
		
		
	}
	
	/**
	 *Nombre m�todo: listadoHabODesVigente
	 *Programado por: Yonis Vergara
	 *Fecha Creacion:30-08-2017
	 *Vercion: 1.0
	 *Ultima Verci�n: 30-08-2017
	 *Modificado por :Yonis Vergara
	 *Razon de cambio:Creacion Metodos consulta de listado 
	 *Descripci�n: este es el metodo que se escarga de entregar
	 *			   las lista de haberes o descuentos VIGENTE
	 *			   (valor 1 en la columna USMHABDES_COD_VIGENCIA )
	 *
	 */
	public ArrayList<UsmCuentaHabDes> listadoHabODesVigente(String tipo){
		
		 
		  /**
		   * El procedimiento almacenado PKG_TRASPASO_NOMINA.listado_Hab_o_des_V(1)?,2)?)  
		   * lo llamamos solisita 2 campos :
		   * 							1) Este dato es de entrada y es un varchar2 
		   * 							   pero solo acepta 2 posibilidades que son:
		   * 																		A)"HAB" :esto significa que 
		   * 																		   traera la lista de haberes Vigentes
		   * 																		B)"DES" : esto significa que 
		   * 																		  traera la lista de descuentos Vigentes
		   * 							2)este dato es de salida esto devolvera el cursor 
		   * 							  con lam respuesta  
		   */
		  String sql = "{call PKG_CONSULTA_USUARIO_RRHH.listado_Hab_o_des_V(?,?)}";
		  ArrayList<UsmCuentaHabDes> list = new ArrayList<UsmCuentaHabDes>();
		  ConexionBannerRRHH con = new ConexionBannerRRHH();
		  CallableStatement sent;
		  UsmCuentaHabDes cuentaHabDes ;		 
		  try{ 
		       sent = con.conexion().prepareCall(sql);
		       sent.setString(1, tipo);
		       sent.registerOutParameter(2, OracleTypes.CURSOR);
		       sent.executeQuery();
		       ResultSet rs;
		       try {
		    	   /** modificaci�n 21-02-2018 por: Yonis Vergara */
		    	   //rs = ((OracleCallableStatement)sent).getCursor(2);
				   rs = (ResultSet)sent.getObject(2);
			       
			       while(rs.next()){
			    	   cuentaHabDes = new UsmCuentaHabDes();
			    	   cuentaHabDes.setUsmHabDesCodhde(rs.getInt(1));
			    	   cuentaHabDes.setUsmHabDesCodpla(rs.getString(2));
			    	   cuentaHabDes.setUsmHabDesNumniv(rs.getString(3));
			    	   cuentaHabDes.setUsmHabDesCuentaBanner(rs.getString(4));
			    	   cuentaHabDes.setUsmHabDesOrgnBanner(rs.getString(5));
			    	   cuentaHabDes.setUsmHabDesObs(rs.getString(6));
			    	   cuentaHabDes.setUsmHabDesCodVigencia(rs.getInt(7));
			    	   cuentaHabDes.setUsmHabDesFechIni(rs.getDate(8));
			    	   cuentaHabDes.setUsmHabDesFechFin(rs.getDate(9));
			    	   cuentaHabDes.setUsmHabDesRutide(rs.getInt(10));
			    	   cuentaHabDes.setUsmHabDesPidm(rs.getInt(11));
			    	   cuentaHabDes.setRowid(rs.getString("ROWID"));
			    	   list.add(cuentaHabDes);
			       }
			       rs.close();	       
			       
		       } catch (SQLException e){
		    	   System.out.println("Error UsmCuentaHabDesDAO.listadoHabODesVigente: " + e);
			       System.out.println("cursor" );
			   }
		       
		       sent.close(); 
		       con.close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error UsmCuentaHabDesDAO.listadoHabODesVigente: " + e);
		       System.out.println("antes del cursor " );
		   }
		
		return list;
	}
	/**
	 *Nombre metodo: existePK
	 *Programado por: Yonis Vergara
	 *Fecha Creacion:30-08-2017
	 *Vercion: 1.0
	 *Ultima Verci�n: 30-08-2017
	 *Modificado por :Yonis Vergara
	 *Razon de cambio:Creacion Metodos consulta de listado 
	 *Descripci�n: para saber si esta si la primari Key ya ocupada*			   
	 *
	 */
	public boolean existePK(UsmCuentaHabDes reglaCuenta){
		boolean existe = true;
		String sql = "{ call  PKG_CONSULTA_USUARIO_RRHH.EXISTPK( "
														  +"?, "// PI_CODORG 
														  +"?, "// PI_CODPLA 
														  +"?, "// PI_NUMNIV 
														  +"?  " // PO_EXISTE
														  +") } ";
		CallableStatement sent ;
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		try{ 
		       sent = con.conexion().prepareCall(sql);
		       sent.setInt   (1, reglaCuenta.getUsmHabDesCodhde() );
		       sent.setString(2, reglaCuenta.getUsmHabDesCodpla());
		       sent.setString(3, reglaCuenta.getUsmHabDesNumniv());
		       sent.registerOutParameter(4, OracleTypes.VARCHAR);
		       sent.executeQuery();
		       
		       try {
		    	   /** modificaci�n 26-02-2018 por: Yonis Vergara */
			       //rs = ((OracleCallableStatement)sent).getCursor(2);
				   String resp = sent.getNString(4);			       
			       if(resp.equals("FALSE")){
			    	   existe= false;
			       }
			              
			       
		       } catch (SQLException e){
		    	   System.out.println("Error UsmCuentaHabDesDAO.existePK: " + e);
			       System.out.println("cursor" );
			   }
		       
		       sent.close(); 
		       con.close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error UsmCuentaHabDesDAO.existePK: " + e);
		       System.out.println("antes del cursor " );
		   }
		
		
		return existe;
	}
	
	/**
	 *Nombre metodo: habODesVigentePorCodigo
	 *Programado por: Yonis Vergara
	 *Fecha Creacion: 30-08-2017
	 *Vercion: 1.0
	 *Ultima Verci�n: 30-08-2017
	 *Modificado por : Yonis Vergara
	 *Razon de cambio: Creacion Metodos consulta de busqueda 
	 *Descripci�n: Busqueda por CODHDE 
	 *
	 */
	public ArrayList<UsmCuentaHabDes> habODesVigentePorCodigo(String codigo,String tipo){
		
		 
		  /*
		   * El procedimiento almacenado PKG_TRASPASO_NOMINA.listado_Hab_o_des_V_codigo(1)?,2)?)  
		   * lo llamamos solisita 2 campos :
		   * 							1)CODHDE(Codigo de haber o descuento) para la busqueda 
		   * 							2)Este dato es de salida esto devolvera el cursor 
		   * 							  con lam respuesta  
		   */
		  String sql = "{call PKG_CONSULTA_USUARIO_RRHH.listado_Hab_o_des_V_codigo(?,?,?)}";
		  ArrayList<UsmCuentaHabDes> list = new ArrayList<UsmCuentaHabDes>();
		  ConexionBannerRRHH con = new ConexionBannerRRHH();
		  CallableStatement sent;
		  UsmCuentaHabDes cuentaHabDes ;		 
		  try{ 
		       sent = con.conexion().prepareCall(sql);
		       sent.setString(1, tipo);
		       sent.setString(2, codigo);
		       sent.registerOutParameter(3, OracleTypes.CURSOR);
		       sent.executeQuery();
		       ResultSet rs;
		       try {
		    	   /** modificaci�n 26-02-2018 por: Yonis Vergara */
			    	//rs = ((OracleCallableStatement)sent).getCursor(2);
				   rs = (ResultSet)sent.getObject(3);
			       
			       while(rs.next()){
			    	   cuentaHabDes = new UsmCuentaHabDes();
			    	   cuentaHabDes.setUsmHabDesCodhde(rs.getInt(1));
			    	   cuentaHabDes.setUsmHabDesCodpla(rs.getString(2));
			    	   cuentaHabDes.setUsmHabDesNumniv(rs.getString(3));
			    	   cuentaHabDes.setUsmHabDesCuentaBanner(rs.getString(4));
			    	   cuentaHabDes.setUsmHabDesOrgnBanner(rs.getString(5));
			    	   cuentaHabDes.setUsmHabDesObs(rs.getString(6));
			    	   cuentaHabDes.setUsmHabDesCodVigencia(rs.getInt(7));
			    	   cuentaHabDes.setUsmHabDesFechIni(rs.getDate(8));
			    	   cuentaHabDes.setUsmHabDesFechFin(rs.getDate(9));
			    	   cuentaHabDes.setUsmHabDesRutide(rs.getInt(10));
			    	   cuentaHabDes.setUsmHabDesPidm(rs.getInt(11));
			    	   cuentaHabDes.setRowid(rs.getString("ROWID"));
			    	   list.add(cuentaHabDes);
			       }
			       rs.close();	       
			       
		       } catch (SQLException e){
		    	   System.out.println("Error UsmCuentaHabDesDAO.habODesVigentePorCodigo: " + e);
			       System.out.println("cursor" );
			   }		       
		       sent.close(); 
		       con.close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error UsmCuentaHabDesDAO.habODesVigentePorCodigo: " + e);
		       System.out.println("antes del cursor " );
		   }
		
		return list;
	}
	
	public String descOrgn(String codigoOrgn){
		
		  String sql = "{call PKG_CONSULTA_USUARIO_RRHH.descripcionORGN(?,?)}";
		  
		  ConexionBannerRRHH con = new ConexionBannerRRHH();
		  CallableStatement sent;
		  
		  String descOrgn = null;
		  try{ 
		       sent = con.conexion().prepareCall(sql);
		       sent.setString(1, codigoOrgn);		     
		       sent.registerOutParameter(2, OracleTypes.VARCHAR);
		       sent.executeQuery();
		       
		       try {
		    	   
		    	   descOrgn = sent.getNString(2);		              
			       
		       } catch (SQLException e){
		    	   System.out.println("Error UsmCuentaHabDesDAO.descOrgn: " + e);
			       System.out.println("cursor" );
			   }		       
		       sent.close(); 
		       con.close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error UsmCuentaHabDesDAO.descOrgn: " + e);
		       System.out.println("antes del cursor " );
		   }
		
		return descOrgn;
	}
	public String descCuenta(String codigoCuenta){
		
		  String sql = "{call PKG_CONSULTA_USUARIO_RRHH.descripcionCUENTA(?,?)}";
		  
		  ConexionBannerRRHH con = new ConexionBannerRRHH();
		  CallableStatement sent;
		  
		  String descOrgn = null;
		  try{ 
		       sent = con.conexion().prepareCall(sql);
		       sent.setString(1, codigoCuenta);		     
		       sent.registerOutParameter(2, OracleTypes.VARCHAR);
		       sent.executeQuery();
		       
		       try {
		    	   /** modificaci�n 26-02-2018 por: Yonis Vergara */
			       //rs = ((OracleCallableStatement)sent).getCursor(2);
		    	   descOrgn = sent.getNString(2);		              
			       
		       } catch (SQLException e){
		    	   System.out.println("Error UsmCuentaHabDesDAO.descCuenta: " + e);
			       System.out.println("cursor" );
			   }		       
		       sent.close(); 
		       con.close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error UsmCuentaHabDesDAO.descCuenta: " + e);
		       System.out.println("antes del cursor " );
		   }
		
		return descOrgn;
	}
	
	public String descCODHDE(int codigoCODHDE){
		
		  String sql = "SELECT DESHDE FROM REMUNERABT.APREF04 where CODHDE = ?  ";
		  
		  ConexionAs400RRHHG con = new ConexionAs400RRHHG();
		  PreparedStatement sent ;
		  
		  String descOrgn = null;
		  try{ 
		       sent = con.getConnection().prepareCall(sql);
		       sent.setInt(1, codigoCODHDE);	       
		       ResultSet rs = sent.executeQuery();
		       
		       try {
		    	   
		    	   while (rs.next()) {
					
		    		   descOrgn =  rs.getString(1);
		    	   }		              
			       
		       } catch (SQLException e){
		    	   System.out.println("Error UsmCuentaHabDesDAO.descCODHDE: " + e);
			       System.out.println("cursor" );
			   }		       
		       sent.close(); 
		       con.getConnection().close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error UsmCuentaHabDesDAO.descCODHDE: " + e);
		       System.out.println("antes del cursor " );
		   }
		
		return descOrgn;
	}
}
