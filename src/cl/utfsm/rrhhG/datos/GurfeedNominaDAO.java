package cl.utfsm.rrhhG.datos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.GurfeedNomina;

public class GurfeedNominaDAO {
	
	public static List<GurfeedNomina> listGurfedNomina (){
		
		List<GurfeedNomina> list = new ArrayList<GurfeedNomina>();
		String sql = "select * from GURFEED_NOMINA";
		try {
			ConexionBannerRRHH con  = new ConexionBannerRRHH();
			PreparedStatement sent = con.getConexion().prepareStatement(sql);
			ResultSet rs = sent.executeQuery();
			GurfeedNomina gur = null;
			while (rs.next()) {
				gur = new GurfeedNomina();
				gur.setGurfeedSystemId(rs.getString("GURFEED_SYSTEM_ID"));
				gur.setGurfeedSystemTimeStamp(rs.getString("GURFEED_SYSTEM_TIME_STAMP"));
				gur.setGurfeedDocCode(rs.getString("GURFEED_DOC_CODE"));
				gur.setGurfeedRecType(rs.getString("GURFEED_REC_TYPE"));
				gur.setGurfeedSubmissionNumber(rs.getInt("GURFEED_SUBMISSION_NUMBER"));
				if(rs.wasNull()){
					gur.setGurfeedSubmissionNumber(null);
				}
				gur.setGurfeedItemNum(rs.getInt("GURFEED_ITEM_NUM"));
				if(rs.wasNull()){
					gur.setGurfeedItemNum(null);
				}
				gur.setGurfeedSeqNum(rs.getInt("GURFEED_SEQ_NUM"));
				if(rs.wasNull()){
					gur.setGurfeedSeqNum(null);
				}
				gur.setGurfeedActivityDate(rs.getDate("GURFEED_ACTIVITY_DATE"));
				gur.setGurfeedUserId(rs.getString("GURFEED_USER_ID"));
				gur.setGurfeedRuclCode(rs.getString("GURFEED_RUCL_CODE"));
				gur.setGurfeedDocRefNum(rs.getString("GURFEED_DOC_REF_NUM"));
				gur.setGurfeedTransDate(rs.getDate("GURFEED_TRANS_DATE"));
				gur.setGurfeedTransAmt(rs.getLong("GURFEED_TRANS_AMT"));
				if(rs.wasNull()){
					gur.setGurfeedTransAmt(null);
				}
				gur.setGurfeedTransDesc(rs.getString("GURFEED_TRANS_DESC"));
				gur.setGurfeedDrCrInd(rs.getString("GURFEED_DR_CR_IND"));
				gur.setGurfeedBankCode(rs.getString("GURFEED_BANK_CODE"));
				gur.setGurfeedObudCode(rs.getString("GURFEED_OBUD_CODE"));
				gur.setGurfeedObphCode(rs.getString("GURFEED_OBPH_CODE"));
				gur.setGurfeedBudgDurCode(rs.getString("GURFEED_BUDG_DUR_CODE"));
				gur.setGurfeedCoasCode(rs.getString("GURFEED_COAS_CODE"));
				gur.setGurfeedAcciCode(rs.getString("GURFEED_ACCI_CODE"));
				gur.setGurfeedFundCode(rs.getString("GURFEED_FUND_CODE"));
				gur.setGurfeedOrgnCode(rs.getString("GURFEED_ORGN_CODE"));
				gur.setGurfeedAcctCode(rs.getString("GURFEED_ACCT_CODE"));
				gur.setGurfeedProgCode(rs.getString("GURFEED_PROG_CODE"));
				gur.setGurfeedActvCode(rs.getString("GURFEED_ACTV_CODE"));
				gur.setGurfeedLocnCode(rs.getString("GURFEED_LOCN_CODE"));
				gur.setGurfeedDepNum(rs.getString("GURFEED_DEP_NUM"));
				gur.setGurfeedEncdNum(rs.getString("GURFEED_ENCD_NUM"));
				gur.setGurfeedEncdItemNum(rs.getInt("GURFEED_ENCD_ITEM_NUM"));
				if(rs.wasNull()){
					gur.setGurfeedEncdItemNum(null);
				}
				gur.setGurfeedEncdSeqNum(rs.getInt("GURFEED_ENCD_SEQ_NUM"));
				if(rs.wasNull()){
					gur.setGurfeedEncdSeqNum(null);
				}
				gur.setGurfeedEncdActionInd(rs.getString("GURFEED_ENCD_ACTION_IND"));
				gur.setGurfeedPrjdCode(rs.getString("GURFEED_PRJD_CODE"));
				gur.setGurfeedDistPct(rs.getInt("GURFEED_DIST_PCT"));
				if(rs.wasNull()){
					gur.setGurfeedDistPct(null);
				}
				gur.setGurfeedBudDispn(rs.getString("GURFEED_BUD_DISPN"));
				gur.setGurfeedCmtType(rs.getString("GURFEED_CMT_TYPE"));
				gur.setGurfeedCmtPct(rs.getInt("GURFEED_CMT_PCT"));
				if(rs.wasNull()){
					gur.setGurfeedCmtPct(null);
				}
				gur.setGurfeedEncbType(rs.getString("GURFEED_ENCB_TYPE"));
				gur.setGurfeedVendorPidm(rs.getInt("GURFEED_VENDOR_PIDM"));
				if(rs.wasNull()){
					gur.setGurfeedVendorPidm(null);
				}
				gur.setGurfeedOneTimeVendCode(rs.getString("GURFEED_ONE_TIME_VEND_CODE"));
				gur.setGurfeedBudgetPeriod(rs.getString("GURFEED_BUDGET_PERIOD"));
				gur.setGurfeedAccrualInd(rs.getString("GURFEED_ACCRUAL_IND"));
				gur.setGurfeedAbalOverride(rs.getString("GURFEED_ABAL_OVERRIDE"));
				gur.setGurfeedId(rs.getString("GURFEED_ID"));
				gur.setGurfeedTranNumber(rs.getString("GURFEED_TRAN_NUMBER"));
				gur.setGurfeedDetailCode(rs.getString("GURFEED_DETAIL_CODE"));
				gur.setGurfeedTermCode(rs.getString("GURFEED_TERM_CODE"));
				gur.setGurfeedAccount(rs.getString("GURFEED_ACCOUNT"));
				gur.setGurfeedSrceCode(rs.getString("GURFEED_SRCE_CODE"));
				gur.setGurfeedEffectiveDate(rs.getString("GURFEED_EFFECTIVE_DATE"));
				gur.setGurfeedPaytCode(rs.getString("GURFEED_PAYT_CODE"));
				gur.setGurfeedDetailCode2(rs.getString("GURFEED_DETAIL_CODE2"));
				gur.setGurfeedPaytCode2(rs.getString("GURFEED_PAYT_CODE2"));
				gur.setGurfeedCurrCode(rs.getString("GURFEED_CURR_CODE"));
				gur.setGurfeedForeignAmount(rs.getInt("GURFEED_FOREIGN_AMOUNT"));
				if(rs.wasNull()){
					gur.setGurfeedForeignAmount(null);
				}
				gur.setGurfeedPtotStatus(rs.getString("GURFEED_PTOT_STATUS"));
				gur.setGurfeedLedcCode(rs.getString("GURFEED_LEDC_CODE"));
				list.add(gur);
			}
		} catch (SQLException e) {
			System.out.print("SQLERROR GurfeedNominaDAO.listGurfedNomina: "+e.getMessage());
		} catch (Exception e) {
			System.out.print("ERROR GurfeedNominaDAO.listGurfedNomina: "+e.getMessage());
		}
		
		return list;
	}
}
