package cl.utfsm.rrhhG.datos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import cl.utfsm.conexion.ConexionAs400RRHHG;
import cl.utfsm.rrhhG.modulo.HaberDescuento;
/*
 *Clase: HaberDescuentoDAO
 *Programado por: Yonis Vergara
 *Fecha Creacion:07-09-2017
 *Vercion: 1.0
 *Ultima Verci�n: 07-09-2017
 *Modificado por :Yonis Vergara
 *Razon de cambio:creacion de clase y sus metodods
 *Descripci�n: esta clase hace la union entre la clase HaberDescuento y la base de datos de AS400
 *
 */
public class HaberDescuentoDAO {
	
	public HaberDescuentoDAO(){
		
	}
	/**
	 *Metodo: habDesPorCod
	 *Programado por: Yonis Vergara
	 *Fecha Creacion:07-09-2017
	 *Vercion: 1.0
	 *Ultima Verci�n: 07-09-2017
	 *Modificado por :Yonis Vergara
	 *Razon de cambio:optener HaberDecuento en base a su codigo
	 *Descripci�n: optener HaberDecuento en base a su codigo recibiendo un CODHDE que es un int 
	 *
	 */
	public HaberDescuento habDesPorCod(int codhde){
		
		  String sql = " SELECT CODHDE,DESHDE FROM REMUNERABT.APREF04 where CODHDE = ? ";
		  HaberDescuento habDec = new HaberDescuento();
		  ConexionAs400RRHHG con = new ConexionAs400RRHHG();
		  PreparedStatement sent ;
		  
		  
		  try{ 
		       sent = con.getConnection().prepareCall(sql);
		       sent.setInt(1, codhde);	       
		       ResultSet rs = sent.executeQuery();
		       
		       try {
		    	   
		    	   while (rs.next()) {
		    		   habDec = new HaberDescuento();
		    		   habDec.setDescHabDes(rs.getString("DESHDE"));
		    		   habDec.setCodHabDes(rs.getInt("CODHDE"));
		    	   }		              
			       
		       } catch (SQLException e){
		    	   System.out.println("Error UsmCuentaHabDesDAO.descCODHDE: " + e);
			       System.out.println("cursor" );
			   }		       
		       sent.close(); 
		       con.getConnection().close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error UsmCuentaHabDesDAO.descCODHDE: " + e);
		       System.out.println("antes del cursor " );
		   }
		
		   
		return habDec;
		
		
	}
	/**
	 *Clase: listaBusHabDec
	 *Programado por: Yonis Vergara
	 *Fecha Creacion:07-09-2017
	 *Vercion: 1.0
	 *Ultima Verci�n: 07-09-2017
	 *Modificado por :Yonis Vergara
	 *Razon de cambio:para optener un listado de HAberDec basado aBuscar 
	 *Descripci�n: para optener un listado de HAberDec basado aBuscar 
	 *				tambien se espesifica en que tipo buscar en haberes o decuento:		
	 *							1) tipo es igual "HAR" solo busca entre los haberes que la regla dice que 
	 *										su codigo es menor a 200
	 *							2) tipo es igual "DESC" solo busca entre los descuentos que la regla dice
	 *										que codigo es igual o mayor a 200
	 *							3) cualquier otra cosa buscara en todo por igual .nota tambien se le puede pasar null   
	 *
	 */
	
	public ArrayList<HaberDescuento> listaBusHabDec(String aBuscar,String tipo) {
		
		String buscarCod = aBuscar+"%";
		String busDesc = "%"+buscarCod;
		ArrayList<HaberDescuento> list = new ArrayList<HaberDescuento>();
		String sql = "";
		if(tipo != null && tipo.equals("HAB")){
			 sql = " SELECT CODHDE,DESHDE FROM REMUNERABT.APREF04 " +
							" where (CODHDE LIKE ? OR DESHDE LIKE ?) and CODHDE < 200  ";
		}else if (tipo != null && tipo.equals("DESC")){
			 sql = " SELECT CODHDE,DESHDE FROM REMUNERABT.APREF04 " +
							" where (CODHDE like ? OR DESHDE LIKE ?) and CODHDE >= 200  ";
		}else{
			 sql = " SELECT CODHDE,DESHDE FROM REMUNERABT.APREF04 where CODHDE like ? or DESHDE LIKE ? ";
		}
		HaberDescuento habDec = new HaberDescuento();
		ConexionAs400RRHHG con = new ConexionAs400RRHHG();
		PreparedStatement sent ;	  
		  
		  try{ 
		       sent = con.getConnection().prepareCall(sql);
		       sent.setString(1, buscarCod);
		       sent.setString(2, busDesc);
		       ResultSet rs = sent.executeQuery();
		       
		       try {
		    	   
		    	   while (rs.next()) {
		    		   habDec = new HaberDescuento();
		    		   habDec.setDescHabDes(rs.getString("DESHDE"));
		    		   habDec.setCodHabDes(rs.getInt("CODHDE"));
		    		   list.add(habDec);
		    	   }		              
			       
		       } catch (SQLException e){
		    	   System.out.println("Error UsmCuentaHabDesDAO.descCODHDE: " + e);
			       System.out.println("cursor" );
			   }		       
		       sent.close(); 
		       con.getConnection().close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error UsmCuentaHabDesDAO.descCODHDE: " + e);
		       System.out.println("antes del cursor " );
		   }
		
		return list;
	}
	/**
	 *Clase: tablaCompleta
	 *Programado por: Yonis Vergara
	 *Fecha Creacion:07-09-2017
	 *Vercion: 1.0
	 *Ultima Verci�n: 07-09-2017
	 *Modificado por :Yonis Vergara
	 *Razon de cambio:para optener un listado de HAberDec
	 *Descripci�n: 
	 *			es para optener la tabla de as400 y dejarla 
	 *				en session para evitar hacer varias consultas   
	 *
	 */
	public HashMap<Integer,HaberDescuento> tablaCompleta() {
		
		System.out.print("consulta Apref04 \n");
		String sql = " SELECT CODHDE,DESHDE FROM REMUNERABT.APREF04 ";
		
		HaberDescuento habDec = new HaberDescuento();
		ConexionAs400RRHHG con = new ConexionAs400RRHHG();
		PreparedStatement sent ;	  
		HashMap<Integer,HaberDescuento> map = new HashMap<Integer, HaberDescuento>(); 
		  try{ 
		       sent = con.getConnection().prepareCall(sql);
		       
		       ResultSet rs = sent.executeQuery();
		       
		       try {
		    	   
		    	   while (rs.next()) {
		    		   habDec = new HaberDescuento();
		    		   habDec.setDescHabDes(rs.getString("DESHDE"));
		    		   habDec.setCodHabDes(rs.getInt("CODHDE"));
		    		   map.put(habDec.getCodHabDes(),habDec);
		    	   }		              
			       
		       } catch (SQLException e){
		    	   System.out.println("Error UsmCuentaHabDesDAO.descCODHDE: " + e);
			       System.out.println("cursor" );
			   }		       
		       sent.close(); 
		       con.getConnection().close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error UsmCuentaHabDesDAO.descCODHDE: " + e);
		       System.out.println("antes del cursor " );
		   }
		
		return map;
	}
}
