package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cl.utfsm.conexion.ConexionAs400RRHHG;
import cl.utfsm.conexion.ConexionAs400RRHHI1;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.IseriesApref08;
import cl.utfsm.rrhhG.modulo.IseriesApref18;

public class IseriesApref18DAO {

	public static List<IseriesApref18> listDetalle(int anoPro ,int mesPro ,String nomConeccion){
		
		Connection conIseries ;
		if(nomConeccion.equals("GURFEED")){
			conIseries  = (new ConexionAs400RRHHG()).generaConnection();
		}else if (nomConeccion.equals("INTERFACE1")) {
			conIseries  = (new ConexionAs400RRHHI1()).generaConnection();
		}else{
			return null;
		}
		
		List<IseriesApref18> list = new ArrayList<IseriesApref18>();
		String sql = "select * from remunerabt.apref33 where anoPro = ? and mesPro = ? ";
		
		try {
			PreparedStatement sent = conIseries.prepareStatement(sql);
			sent.setInt(1, anoPro);
			sent.setInt(2, mesPro);
			ResultSet rs = sent.executeQuery();
			IseriesApref18 det ;
			while (rs.next() ){
				det = new IseriesApref18(); 
				det.setAnoPro(rs.getInt("ANOPRO"));
				det.setMesPro(rs.getInt("MESPRO"));
				det.setRutide(rs.getInt("RUTIDE"));
				det.setDigide(rs.getString("DIGIDE"));
				det.setTipCon(rs.getInt("TIPCON"));
				det.setNumCom(rs.getInt("NUMCON"));
				det.setCodHde(rs.getInt("CODHDE"));
				det.setValHde(rs.getInt("VALHDE"));
				det.setCodUni(rs.getInt("CODUNI"));
				det.setValApl(rs.getInt("VALAPL"));
				det.setValPes(rs.getInt("VALPES"));
				det.setDesHde(rs.getString("DESHDE"));
				det.setInsApv(rs.getInt("INSAPV"));
				det.setCodOrg(rs.getString("CODORG"));
				list.add(det);
				
			}
			
		} catch (SQLException e) {
			System.out.print("SQLError : IseriesApref18DAO.listDetalle :" +e.getMessage());
		} catch (Exception e) {
			System.out.print("Error : IseriesApref18DAO.listDetalle :" +e.getMessage());
		}
		try {
			conIseries.close();
		} catch (SQLException e) {
			System.out.print("SQLError : IseriesApref18DAO.listDetalle :" +e.getMessage());
		} catch (Exception e) {
			System.out.print("Error : IseriesApref18DAO.listDetalle :" +e.getMessage());
		}
		return list;
	}
	
	/*************************************************************************************
	 *******************************IMPORTANTE********************************************
	 *Este metodo no cierra la conexcion para no perder la session esta tabla es Temporal*
	 *******************************IMPORTANTE********************************************
	 *************************************************************************************
	 */
	
	public static void guardarListDetalle(List<IseriesApref18> listDetalle,ConexionBannerRRHH con){
		
		String sql = "INSERT INTO TMP_APREF18(ANOPRO,MESPRO,RUTIDE,DIGIDE,TIPCON,NUMCON,CODHDE,VALHDE,CODUNI,VALAPL,VALPES,DESHDE,INSAPV,CODORG)"+
                     					     "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; 
	try {
		PreparedStatement sent = con.getConexion().prepareStatement(sql);
		for (IseriesApref18 detalle : listDetalle) {
			sent.setInt(1,detalle.getAnoPro());
			sent.setInt(2,detalle.getMesPro());
			sent.setInt(3,detalle.getRutide());
			sent.setString(4,detalle.getDigide());
			sent.setInt(5,detalle.getTipCon());
			sent.setInt(6,detalle.getNumCom());
			sent.setInt(7,detalle.getCodHde());
			sent.setInt(8,detalle.getValHde());
			sent.setInt(9,detalle.getCodUni());
			sent.setInt(10,detalle.getValApl());
			sent.setInt(11,detalle.getValPes());
			sent.setString(12,detalle.getDesHde());
			sent.setInt(13,detalle.getInsApv());
			sent.setString(14,detalle.getCodOrg());
			sent.addBatch();
		}
		sent.executeBatch();
		sent.clearBatch();
		sent.close();
	} catch (SQLException e) {
		System.out.print("SQLERROR en : IsriesApref18DAO.guardarListDetalle :\n" +e.getMessage());
	} catch (Exception e) {
		System.out.print("ERROR en : IsriesApref18DAO.guardarListDetalle :\n"+e.getMessage());
	}
				
	}
	
}
