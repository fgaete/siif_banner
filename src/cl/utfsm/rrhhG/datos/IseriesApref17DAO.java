package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cl.utfsm.conexion.ConexionAs400RRHHG;
import cl.utfsm.conexion.ConexionAs400RRHHI1;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.IseriesApref08;
import cl.utfsm.rrhhG.modulo.IseriesApref17;

public class IseriesApref17DAO {
	
	public static List<IseriesApref17> listResuNomi(int anoPro , int mesPro,String nomConeccion){
		Connection conIseries ;
		if(nomConeccion.equals("GURFEED")){
			conIseries  = (new ConexionAs400RRHHG()).generaConnection();
		}else if (nomConeccion.equals("INTERFACE1")) {
			conIseries  = (new ConexionAs400RRHHI1()).generaConnection();
		}else{
			return null;
		}
		
		List<IseriesApref17> list = new ArrayList<IseriesApref17>();
		String sql = "select * from remunerabt.apref32 where anoPro = ? and mesPro = ?";
		try {
			PreparedStatement sent = conIseries.prepareStatement(sql);
			sent.setInt(1, anoPro);
			sent.setInt(2, mesPro);
			ResultSet rs = sent.executeQuery();
			IseriesApref17  det ;
			while (rs.next()) {
				det = new IseriesApref17();
				det.setMesPro(rs.getInt("MESPRO"));
				det.setAnoPro(rs.getInt("ANOPRO"));
				det.setRutide(rs.getInt("RUTIDE"));
				det.setDigide(rs.getString("DIGIDE"));
				det.setTipCon(rs.getInt("TIPCON"));
				det.setNumCon(rs.getInt("NUMCON"));
				det.setCodGra(rs.getInt("CODGRA"));
				det.setCodTit(rs.getInt("CODTIT"));
				det.setCodBan(rs.getInt("CODBAN"));
				det.setSucBan(rs.getInt("SUCBAN"));
				det.setCueBan(rs.getString("CUEBAN"));
				det.setEscFon(rs.getInt("ESCFON"));
				det.setCodUni(rs.getInt("CODUNI"));
				det.setCodPla(rs.getInt("CODPLA"));
				det.setNumNiv(rs.getInt("NUMNIV"));
				det.setCodSuc(rs.getInt("CODSUC"));
				det.setCodCar(rs.getInt("CODCAR"));
				det.setSumFij(rs.getInt("SUMFIJ"));
				det.setJornad(rs.getInt("JORNAD"));
				det.setCanHor(rs.getInt("CANHOR"));
				det.setCodAFP(rs.getInt("CODAFP"));
				det.setIndJub(rs.getInt("INDJUB"));
				det.setCodIsa(rs.getInt("CODISA"));
				det.setTotImp(rs.getInt("TOTIMP"));
				det.setTotTri(rs.getInt("TOTTRI"));
				det.setValImp(rs.getInt("VALIMP"));
				det.setTotLiq(rs.getInt("TOTLIQ"));
				det.setAfeExt(rs.getInt("AFEEXT"));
				det.setHabOrd(rs.getInt("HABORD"));
				det.setNumche(rs.getInt("NUMCHE"));
				det.setDiatra(rs.getInt("DIATRA"));
				det.setSegCes(rs.getInt("SEGCES"));
				det.setSegTra(rs.getInt("SEGTRA"));
				det.setLiqLiq(rs.getInt("LIQLIQ"));
				det.setHorExt(rs.getInt("HOREXT"));
				det.setValExt(rs.getInt("VALEXT"));
				det.setCanCar(rs.getInt("CANCAR"));
				det.setCarRet(rs.getInt("CARRET"));
				det.setCarDup(rs.getInt("CARDUP"));
				det.setBonHij(rs.getInt("BONHIJ"));
				det.setTotHab(rs.getInt("TOTHAB"));
				det.setTotDes(rs.getInt("TOTDES"));
				det.setTotExe(rs.getInt("TOTEXE"));
				det.setTotLeg(rs.getInt("TOTLEG"));
				det.setTDesva(rs.getInt("TDESVA"));
				det.setApoPat(rs.getInt("APOPAT"));
				det.setAfeLso(rs.getInt("AFELSO"));
				det.setAfeImp(rs.getInt("AFEIMP"));
				det.setAfeAPV(rs.getInt("AFEAPV"));
				det.setAfeSeg(rs.getInt("AFESEG"));
				det.setMTipo(rs.getInt("MTIPO"));
				det.setMRazon(rs.getInt("MRAZON"));
				det.setDilIpa(rs.getInt("DILIPA"));
				det.setVaSuIn(rs.getInt("VASUIN"));
				det.setCodorg(rs.getString("CODORG"));
				list.add(det);
				
			}
		} catch (SQLException e) {
			System.out.print("SQLError : IseriesApref17DAO.listDetalle :" +e.getMessage());
		} catch (Exception e) {
			System.out.print("Error : IseriesApref17DAO.listDetalle :" +e.getMessage());
		}
		try {
			conIseries.close();
		} catch (SQLException e) {
			System.out.print("SQLError : IseriesApref17DAO.listDetalle :" +e.getMessage());
		} catch (Exception e) {
			System.out.print("Error : IseriesApref17DAO.listDetalle :" +e.getMessage());
		}
		return list;
	}
	public static void guardarListResNom(List<IseriesApref17> listResuNom,ConexionBannerRRHH con){
		
		String sql = "INSERT INTO TMP_APREF17(MESPRO,ANOPRO,RUTIDE,DIGIDE,TIPCON,NUMCON,CODGRA,CODTIT,CODBAN,SUCBAN,CUEBAN,ESCFON,CODUNI, "+
		                    "CODPLA,NUMNIV,CODSUC,CODCAR,SUMFIJ,JORNAD,CANHOR,CODAFP,INDJUB,CODISA,TOTIMP,TOTTRI,VALIMP,TOTLIQ, "+
		                    "AFEEXT,HABORD,NUMCHE,DIATRA,SEGCES,SEGTRA,LIQLIQ,HOREXT,VALEXT,CANCAR,CARRET,CARDUP,BONHIJ,TOTHAB, "+
		                    "TOTDES,TOTEXE,TOTLEG,TDESVA,APOPAT,AFELSO,AFEIMP,AFEAPV,AFESEG,MTIPO,MRAZON,DILIPA,VASUIN,CODORG) "+
		             "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, "+
		                    "?,?,?,?,?)";  
	try {		
		PreparedStatement sent = con.getConexion().prepareStatement(sql);
		for (IseriesApref17 resuNomi : listResuNom) {
			sent.setInt(1,resuNomi.getMesPro());
			sent.setInt(2,resuNomi.getAnoPro());
			sent.setInt(3,resuNomi.getRutide());
			sent.setString(4,resuNomi.getDigide());
			sent.setInt(5,resuNomi.getTipCon());
			sent.setInt(6,resuNomi.getNumCon());
			sent.setInt(7,resuNomi.getCodGra());
			sent.setInt(8,resuNomi.getCodTit());
			sent.setInt(9,resuNomi.getCodBan());
			sent.setInt(10,resuNomi.getSucBan());
			sent.setString(11,resuNomi.getCueBan());
			sent.setInt(12,resuNomi.getEscFon());
			sent.setInt(13,resuNomi.getCodUni());
			sent.setInt(14,resuNomi.getCodPla());
			sent.setInt(15,resuNomi.getNumNiv());
			sent.setInt(16,resuNomi.getCodSuc());
			sent.setInt(17,resuNomi.getCodCar());
			sent.setInt(18,resuNomi.getSumFij());
			sent.setInt(19,resuNomi.getJornad());
			sent.setInt(20,resuNomi.getCanHor());
			sent.setInt(21,resuNomi.getCodAFP());
			sent.setInt(22,resuNomi.getIndJub());
			sent.setInt(23,resuNomi.getCodIsa());
			sent.setInt(24,resuNomi.getTotImp());
			sent.setInt(25,resuNomi.getTotTri());
			sent.setInt(26,resuNomi.getValImp());
			sent.setInt(27,resuNomi.getTotLiq());
			sent.setInt(28,resuNomi.getAfeExt());
			sent.setInt(29,resuNomi.getHabOrd());
			sent.setInt(30,resuNomi.getNumche());
			sent.setInt(31,resuNomi.getDiatra());
			sent.setInt(32,resuNomi.getSegCes());
			sent.setInt(33,resuNomi.getSegTra());
			sent.setInt(34,resuNomi.getLiqLiq());
			sent.setInt(35,resuNomi.getHorExt());
			sent.setInt(36,resuNomi.getValExt());
			sent.setInt(37,resuNomi.getCanCar());
			sent.setInt(38,resuNomi.getCarRet());
			sent.setInt(39,resuNomi.getCarDup());
			sent.setInt(40,resuNomi.getBonHij());
			sent.setInt(41,resuNomi.getTotHab());
			sent.setInt(42,resuNomi.getTotDes());
			sent.setInt(43,resuNomi.getTotExe());
			sent.setInt(44,resuNomi.getTotLeg());
			sent.setInt(45,resuNomi.getTDesva());
			sent.setInt(46,resuNomi.getApoPat());
			sent.setInt(47,resuNomi.getAfeLso());
			sent.setInt(48,resuNomi.getAfeImp());
			sent.setInt(49,resuNomi.getAfeAPV());
			sent.setInt(50,resuNomi.getAfeSeg());
			sent.setInt(51,resuNomi.getMTipo());
			sent.setInt(52,resuNomi.getMRazon());
			sent.setInt(53,resuNomi.getDilIpa());
			sent.setInt(54,resuNomi.getVaSuIn());
			sent.setString(55,resuNomi.getCodorg());			
			sent.addBatch();
		}		
		sent.executeBatch();
		sent.clearBatch();
		sent.close();
	} catch (SQLException e) {
		System.out.print("SQLERROR en : IsriesApref17DAO.guardarListResNom :\n" +e.getMessage());
	} catch (Exception e) {
		System.out.print("ERROR en : IsriesApref17DAO.guardarListResNom :\n"+e.getMessage());
	}
				
	}
}
