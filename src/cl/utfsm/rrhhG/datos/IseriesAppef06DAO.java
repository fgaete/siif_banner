package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cl.utfsm.conexion.ConexionAs400RRHHG;
import cl.utfsm.conexion.ConexionAs400RRHHI1;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.IseriesAppef05;
import cl.utfsm.rrhhG.modulo.IseriesAppef06;

public class IseriesAppef06DAO {

	public IseriesAppef06DAO(){
		
	}
	
	public static List<IseriesAppef06> listaSoliciAyudante(String nomConeccion){
		
		Connection conIseries ;
		if(nomConeccion.equals("GURFEED")){
			conIseries  = (new ConexionAs400RRHHG()).generaConnection();
		}else if (nomConeccion.equals("INTERFACE1")) {
			conIseries  = (new ConexionAs400RRHHI1()).generaConnection();
		}else{
			return null;
		}
		
		List<IseriesAppef06> list = new ArrayList<IseriesAppef06>();
		String sql = "select * From remunerabt.appef06 where FECINI > ?";
		Date hoy = new Date();
		Calendar cal  = Calendar.getInstance();
		cal.setTime(hoy);
		int year = (cal.get(Calendar.YEAR)-1)*10000 ;
		try {
			PreparedStatement sent = conIseries.prepareStatement(sql);
			sent.setInt(1, year);
			ResultSet rs = sent.executeQuery();
			IseriesAppef06 solAyu ;
			while (rs.next()) {
				solAyu = new IseriesAppef06();
				solAyu.setNumInt(rs.getInt("NUMINT"));
				solAyu.setRutide(rs.getInt("RUTIDE"));
				solAyu.setDigide(rs.getString("DIGIDE"));
				solAyu.setCodAyu(rs.getString("CODAYU"));
				solAyu.setCodNiv(rs.getString("CODNIV"));
				solAyu.setValHor(rs.getInt("VALHOR"));
				solAyu.setCodUni(rs.getInt("CODUNI"));
				solAyu.setSucur(rs.getInt("SUCUR"));
				solAyu.setFecSol(rs.getInt("FECSOL"));
				solAyu.setFecIni(rs.getInt("FECINI"));
				solAyu.setFecFin(rs.getInt("FECFIN"));
				solAyu.setNomTra(rs.getString("NOMTRA"));
				solAyu.setRutPro(rs.getInt("RUTPRO"));
				solAyu.setDigPro(rs.getString("DIGPRO"));
				solAyu.setProRes(rs.getString("PRORES"));
				solAyu.setCanHor(rs.getInt("CANHOR"));
				solAyu.setValMes(rs.getInt("VALMES"));
				solAyu.setValCon(rs.getInt("VALCON"));
				solAyu.setEstSol(rs.getString("ESTSOL"));
				solAyu.setTipCon(rs.getInt("TIPCON"));
				solAyu.setSemest(rs.getInt("SEMEST"));
				solAyu.setAnoAyu(rs.getInt("ANOAYU"));
				solAyu.setAsiAlf1(rs.getString("ASIALF1"));
				solAyu.setAsiNum1(rs.getInt("ASINUM1"));
				solAyu.setAsiAlf2(rs.getString("ASIALF2"));
				solAyu.setAsinum2(rs.getInt("ASINUM2"));
				solAyu.setAsiAlf3(rs.getString("ASIALF3"));
				solAyu.setAsiNum3(rs.getInt("ASINUM3"));
				solAyu.setPrin01(rs.getString("PRIN01"));
				solAyu.setPrin02(rs.getString("PRIN02"));
				solAyu.setPrin03(rs.getString("PRIN03"));
				solAyu.setIdDigi(rs.getString("IDDIGI"));
				solAyu.setFecIng(rs.getInt("FECING"));
				solAyu.setFeeFin(rs.getInt("FEEFIN"));
				solAyu.setFeeFfi(rs.getInt("FEEFFI"));
				solAyu.setRutPro2(rs.getInt("RUTPRO2"));
				solAyu.setDigPro2(rs.getString("DIGPRO2"));
				solAyu.setRutPro3(rs.getInt("RUTPRO3"));
				solAyu.setDigPro3(rs.getString("DIGPRO3"));
				solAyu.setParale1(rs.getString("PARALE1"));
				solAyu.setParale2(rs.getString("PARALE2"));
				solAyu.setParale3(rs.getString("PARALE3"));
				solAyu.setCodOrg(rs.getString("CODORG"));
				list.add(solAyu);
			}
			
			
		} catch (SQLException e) {
			System.out.print("SQLERROR IseriesAppef06DAO.listaSoliciAyudante :" +e.getMessage());
		} catch (Exception e) {
			System.out.print("ERROR IseriesAppef06DAO.listaSoliciAyudante :" +e.getMessage());
		}
		
		try {
			conIseries.close();
		} catch (SQLException e) {
			System.out.print("SQLERROR IseriesAppef06DAO.listaSoliciAyudante :" +e.getMessage());
		} catch (Exception e) {
			System.out.print("ERROR IseriesAppef06DAO.listaSoliciAyudante :" +e.getMessage());
		}
		
		return list;
	}
	
	/*************************************************************************************
	 *******************************IMPORTANTE********************************************
	 *Este metodo no cierra la conexcion para no perder la session esta tabla es Temporal*
	 *******************************IMPORTANTE********************************************
	 *************************************************************************************
	 */
	
	public static void guardarListSolAyu(List<IseriesAppef06> listSolAyu,ConexionBannerRRHH con){
		
		String sql = "INSERT INTO TMP_APPEF06( "+
										        "NUMINT,RUTIDE,DIGIDE,CODAYU,CODNIV,VALHOR,CODUNI,SUCUR,FECSOL,FECINI,FECFIN,NOMTRA,RUTPRO,DIGPRO, "+
										        "PRORES,CANHOR,VALMES,VALCON,ESTSOL,TIPCON,SEMEST,ANOAYU,ASIALF1,ASINUM1,ASIALF2,ASINUM2,ASIALF3, "+
										        "ASINUM3,PRIN01,PRIN02,PRIN03,IDDIGI,FECING,FEEFIN,FEEFFI,RUTPRO2,DIGPRO2,RUTPRO3,DIGPRO3,PARALE1, "+
										        "PARALE2,PARALE3,CODORG) "+
										      "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	try {
		CallableStatement sent = con.getConexion().prepareCall(sql);
		for (IseriesAppef06 solAyu : listSolAyu) {
			sent.setInt(1,solAyu.getNumInt());
			sent.setInt(2,solAyu.getRutide());
			sent.setString(3,solAyu.getDigide());
			sent.setString(4,solAyu.getCodAyu());
			sent.setString(5,solAyu.getCodNiv());
			sent.setInt(6,solAyu.getValHor());
			sent.setInt(7,solAyu.getCodUni());
			sent.setInt(8,solAyu.getSucur());
			sent.setInt(9,solAyu.getFecSol());
			sent.setInt(10,solAyu.getFecIni());
			sent.setInt(11,solAyu.getFecFin());
			sent.setString(12,solAyu.getNomTra());
			sent.setInt(13,solAyu.getRutPro());
			sent.setString(14,solAyu.getDigPro());
			sent.setString(15,solAyu.getProRes());
			sent.setInt(16,solAyu.getCanHor());
			sent.setInt(17,solAyu.getValMes());
			sent.setInt(18,solAyu.getValCon());
			sent.setString(19,solAyu.getEstSol());
			sent.setInt(20,solAyu.getTipCon());
			sent.setInt(21,solAyu.getSemest());
			sent.setInt(22,solAyu.getAnoAyu());
			sent.setString(23,solAyu.getAsiAlf1());
			sent.setInt(24,solAyu.getAsiNum1());
			sent.setString(25,solAyu.getAsiAlf2());
			sent.setInt(26,solAyu.getAsinum2());
			sent.setString(27,solAyu.getAsiAlf3());
			sent.setInt(28,solAyu.getAsiNum3());
			sent.setString(29,solAyu.getPrin01());
			sent.setString(30,solAyu.getPrin02());
			sent.setString(31,solAyu.getPrin03());
			sent.setString(32,solAyu.getIdDigi());
			sent.setInt(33,solAyu.getFecIng());
			sent.setInt(34,solAyu.getFeeFin());
			sent.setInt(35,solAyu.getFeeFfi());
			sent.setInt(36,solAyu.getRutPro2());
			sent.setString(37,solAyu.getDigPro2());
			sent.setInt(38,solAyu.getRutPro3());
			sent.setString(39,solAyu.getDigPro3());
			sent.setString(40,solAyu.getParale1());
			sent.setString(41,solAyu.getParale2());
			sent.setString(42,solAyu.getParale3());
			sent.setString(43,solAyu.getCodOrg());
			sent.addBatch();
		}
		sent.executeBatch();
		sent.clearBatch();
		sent.close();
	} catch (SQLException e) {
		System.out.print("SQLERROR en : IsriesAppef06DAO.guardarListSolAyu :\n" +e.getMessage());
	} catch (Exception e) {
		System.out.print("ERROR en : IsriesAppef06DAO.guardarListSolAyu :\n"+e.getMessage());
	}
				
	}
	
}
