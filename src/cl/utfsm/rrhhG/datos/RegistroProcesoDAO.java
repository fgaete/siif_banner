package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import oracle.jdbc.OracleTypes;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.RegistroProceso;

public class RegistroProcesoDAO {
	
	

	public RegistroProcesoDAO() {
		
		
	}
	// para insertar proceso 
	public boolean insertRegistroProceso( RegistroProceso registro){
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		CallableStatement sent = null;
	    
	    boolean finalizado = false;
	    String sql = "{ call PKG_CONSULTA_USUARIO_RRHH.insert_registroProce( "
	    	                                                         +" ? , " //P_REGPRO_ID_EJECUCION 
	    	                                                         +" ? , " //P_COD_ETAPA_PROCESO => P_COD_ETAPA_PROCESO, "
	    	                                                         +" ? , " //P_COD_PROCESO => P_COD_PROCESO, "
	    	                                                         +" ? , " //P_REGPRO_RUT_RESPONSABLE => P_REGPRO_RUT_RESPONSABLE, "
	    	                                                         +" ? , " //P_REGPRO_NOMBRE_RESPONSABLE => P_REGPRO_NOMBRE_RESPONSABLE, "
	    	                                                         +" ? , " //P_REGPRO_FECHA_NOMINA => P_REGPRO_FECHA_NOMINA, "
	    	                                                         +" ?  " //P_REGPRO_FINALIZADO => P_REGPRO_FINALIZADO "
	    	          +")} ";
		
		try {
			
			sent = con.getConexion().prepareCall(sql);
			sent.setInt   (1, registro.getRegproIdEjecucion());
			sent.setInt   (2, registro.getCodEtapaProceso());
			sent.setInt   (3, registro.getCodProceso());
			sent.setString(4, registro.getRegproRutResponsable());
			sent.setString(5, registro.getRegProNombreResponsable());
			if(registro.getRegProFechaNomina() != null){
				sent.setDate  (6, new java.sql.Date(registro.getRegProFechaNomina().getTime()));
			}else{
				sent.setDate  (6, null);
			}
			sent.setString(7, registro.getRegProFinalizado());
			
		    sent.execute();
			
			finalizado = true;
			
		    sent.close();
		    con.close();
			
		} catch (Exception e) {
			System.out.println("Error RegistroProcesoDAO.insertRegistroProceso: " + e);
			finalizado = false;
		}
		
		return finalizado;
	
	
	}
	//buscar proceso terminado y que es de tipo de gurfeed
	public static RegistroProceso ultimoGurfeedEjecutado(){
		
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		CallableStatement sent = null;
	    ResultSet res ;
	    RegistroProceso regPro = null;
		
	    String sql = "{call PKG_CONSULTA_USUARIO_RRHH.ULTIMO_GURFEED_GENERADO(PO_DATOS => ?)}";
	    try {
			
	    	sent = con.getConexion().prepareCall(sql);			
			sent.registerOutParameter(1, OracleTypes.CURSOR);
			
			sent.executeQuery();
			/** modificación 21-02-2018 por: Yonis Vergara */
	    	//rs = ((OracleCallableStatement)sent).getCursor(3);
			ResultSet rs = (ResultSet)sent.getObject(1);
			/** */
			if (rs.next()){
				regPro = new RegistroProceso();
				regPro.setRegproIdEjecucion( rs.getInt("REGPRO_ID_EJECUCION"));
				regPro.setCodProceso( rs.getInt("COD_PROCESO"));
				regPro.setCodEtapaProceso( rs.getInt("COD_ETAPA_PROCESO"));
				regPro.setRegProFechaNomina( rs.getDate("REGPRO_FECHA_NOMINA"));
				regPro.setRegProFechaRegistro( rs.getDate("REGPRO_FECHA_REGISTRO"));
				regPro.setRegProFinalizado( rs.getString("REGPRO_FINALIZADO"));
				regPro.setRegProNombreResponsable( rs.getString("NOMBRE"));
				regPro.setRegproRutResponsable( rs.getString("REGPRO_RUT_RESPONSABLE"));				
			 }			
			res = sent.executeQuery();			
			res.close();
		    sent.close();
		    con.close();
	    	
		} catch (Exception e) {
			// TODO: handle exception
		}
		return regPro;
	}
	public RegistroProceso gurfeedFinalisado(int mes , int anno){
		
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		CallableStatement sent = null;
	    ResultSet res ;
	    RegistroProceso regPro = null;
	    
		
		String sql =  "{ call PKG_CONSULTA_USUARIO_RRHH.gurfeedFinalisado(   " +
																			"?, " + // P_MES
																			"?, " + // P_ANNO
																			"?  " + // PO_DATOS
																			")}"; 
		try {
			
			sent = con.getConexion().prepareCall(sql);			
			sent.setInt(1,mes);
			sent.setInt(2,anno);
			sent.registerOutParameter(3, OracleTypes.CURSOR);
			
			sent.executeQuery();
			/** modificación 21-02-2018 por: Yonis Vergara */
	    	//rs = ((OracleCallableStatement)sent).getCursor(3);
			ResultSet rs = (ResultSet)sent.getObject(3);
			/** */
			if (rs.next()){
				regPro = new RegistroProceso();
				regPro.setRegproIdEjecucion( rs.getInt("REGPRO_ID_EJECUCION"));
				regPro.setCodProceso( rs.getInt("COD_PROCESO"));
				regPro.setCodEtapaProceso( rs.getInt("COD_ETAPA_PROCESO"));
				regPro.setRegProFechaNomina( rs.getDate("REGPRO_FECHA_NOMINA"));
				regPro.setRegProFechaRegistro( rs.getDate("REGPRO_FECHA_REGISTRO"));
				regPro.setRegProFinalizado( rs.getString("REGPRO_FINALIZADO"));
				regPro.setRegProNombreResponsable( rs.getString("NOMBRE"));
				regPro.setRegproRutResponsable( rs.getString("REGPRO_RUT_RESPONSABLE"));				
			 }
			
			
			res = sent.executeQuery();
			
			
			res.close();
		    sent.close();
		    con.close();
			
		} catch (Exception e) {
			System.out.println("Error RegistroProcesoDAO.gurfeedFinalisado: " + e);
			regPro = null;
		}
		
		return regPro;
	}
	
	// para actualizar el proceso con los datos actuales
	public boolean updateRegistroProceso( RegistroProceso registro){
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		CallableStatement sent = null;
	    
	    boolean finalizado = false;
		
		String sql = " { call PKG_CONSULTA_USUARIO_RRHH.update_registroProce( "
																		+" ? , "// P_REGPRO_ID_EJECUCION 
																		+" ? , "//,P_COD_ETAPA_PROCESO
																		+" ? , "//,P_COD_PROCESO 
																		+" ? , "//,P_REGPRO_RUT_RESPONSABLE   
																		+" ? , "//,P_REGPRO_NOMBRE_RESPONSABLE     
																		+" ? , "// P_REGPRO_FECHA_NOMINA    
																		+" ?  )}";// P_REGPRO_FINALIZADO  															       
		
       try {
			
			sent = con.getConexion().prepareCall(sql);
			
			sent.setInt   (1,registro.getRegproIdEjecucion());
			sent.setInt   (2,registro.getCodEtapaProceso());
			sent.setInt   (3,registro.getCodProceso());
			sent.setString(4,registro.getRegproRutResponsable());
			sent.setString(5,registro.getRegProNombreResponsable());
			if(registro.getRegProFechaNomina() != null){
				sent.setDate  (6, new java.sql.Date(registro.getRegProFechaNomina().getTime()));
			}else{
				sent.setDate  (6, null);
			}
			sent.setString(7,registro.getRegProFinalizado());
			
			sent.execute();		
			
			finalizado = true;
			
		    sent.close();
		    con.close();
			
		} catch (SQLException e) {
			System.out.println("ErrorSQL RegistroProcesoDAO.updateRegistroProceso: " + e);
			finalizado = false;
		}catch (Exception e){
			System.out.println("Error RegistroProcesoDAO.updateRegistroProceso: " + e);
			finalizado = false;
		}
		
		
		
		return finalizado;		
		
	}
	//para optener el nuevo numero de ejecucion nuevo controlado por una secuencia
	public int getNumeroEjecusion(){
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		int ejecucion = 0;
		String sql = "{ call PKG_CONSULTA_USUARIO_RRHH.NUMEROEJECUCION(?) }";
		CallableStatement sent = null;
	    try {
			 sent = con.conexion().prepareCall(sql);
			 sent.registerOutParameter(1, OracleTypes.NUMBER);
			 sent.execute();
			 
			 ejecucion = sent.getInt(1);				 
			 		 
		     sent.close();
		     con.close();
			 
		} catch (SQLException e) {
			System.out.print("HibernateGurfeedDao.getNumeroEjecusion : " +e.getMessage());
		}
		
		return ejecucion ;
	}
	
}
