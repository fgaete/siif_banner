package cl.utfsm.rrhhG.datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import cl.utfsm.conexion.ConexionAs400RRHHG;
import cl.utfsm.conexion.ConexionAs400RRHHI1;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.IseriesAppef01;

public class IseriesAppef01DAO {
	
	public static List<IseriesAppef01> listPersona(int anoPro ,int mesPro ,Collection<String>  lisRut){
		
		String sql ="select * from remunerabt.appef01 " +
				    "where rutide in (select rutide from remunerabt.apref32 " +
								    " where anoPro = ? and mespro = ?)";
		if(lisRut.size() > 0){
			sql += " and rutide in ( " ;
			int count = 0;
			
			for (int i = 0; i < lisRut.size(); i++ ) {
				if(count > 0){
					sql += " , ";
				}
				sql += "?";				
				count++;
			}
			sql += " ) " ;
		}
		List<IseriesAppef01> list = new ArrayList<IseriesAppef01>();
		
		Connection con = new ConexionAs400RRHHG().generaConnection();
		try {
			PreparedStatement sent = con.prepareStatement(sql);
			sent.setInt(1, anoPro);
			sent.setInt(2, mesPro);
			int count = 3;
			for (String rut : lisRut) {
				int rutide = Integer.parseInt(rut.substring(0,rut.length()-1));
				
				sent.setInt(count, rutide);
				count ++;
			}
			ResultSet rs = sent.executeQuery();
			while (rs.next()) {
				IseriesAppef01 per = new IseriesAppef01();
				per.setRutide(rs.getInt("RUTIDE"));
				per.setDigide(rs.getString("DIGIDE"));
				per.setApePat(rs.getString("APEPAT"));
				per.setApeMat(rs.getString("APEMAT"));
				per.setNombre(rs.getString("NOMBRE"));
				per.setEstCiv(rs.getInt("ESTCIV"));
				per.setSexo(rs.getInt("SEXO"));
				per.setFecNac(rs.getInt("FECNAC"));
				per.setLugNac(rs.getString("LUGNAC"));
				per.setPaiNac(rs.getString("PAINAC"));
				per.setNacion(rs.getInt("NACION"));
				per.setDetNac(rs.getString("DETNAC"));
				per.setDomici(rs.getString("DOMICI"));
				per.setComuna(rs.getInt("COMUNA"));
				per.setCiudad(rs.getInt("CIUDAD"));
				per.setRegion(rs.getInt("REGION"));
				per.setCodGra(rs.getInt("CODGRA"));
				per.setGraTit(rs.getInt("GRATIT"));
				per.setCodTit(rs.getInt("CODTIT"));
				per.setFecIng(rs.getInt("FECING"));
				per.setIngPor(rs.getString("INGPOR"));
				per.setTelefo(rs.getString("TELEFO"));
				per.setRolCap(rs.getInt("ROLCAP"));
				per.setNivAca(rs.getString("NIVACA"));
				per.setSemAnt(rs.getInt("SEMANT"));
				per.setClaInt(rs.getString("CLAINT"));
				per.setIndAct(rs.getString("INDACT"));
				per.setFecCam(rs.getInt("FECCAM"));
				per.setCelula(rs.getString("CELULA"));
				per.setCorele(rs.getString("CORELE"));
				per.setDomele(rs.getString("DOMELE"));
				list.add(per);
				
			}
			
		} catch (SQLException e) {
			System.out.print("SQLERROR IseriesAppef01DAO.guardarListPer :"+e.getMessage());
		} catch (Exception e) {
			System.out.print("ERROR IseriesAppef01DAO.guardarListPer :"+e.getMessage());
		}	
		try {
			con.close();
		} catch (SQLException e) {
			System.out.print("SQLERROR IseriesAppef01DAO.guardarListPer :"+e.getMessage());
		}
		
		return list;
	}




	public static List<IseriesAppef01> listPersona(int anoPro ,int mesPro , String nomConecion){
		
		List<IseriesAppef01> list = new ArrayList<IseriesAppef01>();
		Connection con ;
		
		if(nomConecion.equals("GURFEED")){
			con = (new ConexionAs400RRHHG()).generaConnection();
			
		}else if (nomConecion.equals("INTERFACE1")) {
			con = (new ConexionAs400RRHHI1()).generaConnection();
		}else{
			return null;
		}
		
		
		String sql = " select * from remunerabt.appef01 " +
				" where rutide in (select rutide from remunerabt.apref32 " +
								  " where anoPro = ? and mespro = ?)";
		try {
			PreparedStatement sent = con.prepareStatement(sql);
			sent.setInt(1, anoPro);
			sent.setInt(2, mesPro);
			ResultSet rs = sent.executeQuery();
			while (rs.next()) {
				IseriesAppef01 per = new IseriesAppef01();
				per.setRutide(rs.getInt("RUTIDE"));
				per.setDigide(rs.getString("DIGIDE"));
				per.setApePat(rs.getString("APEPAT"));
				per.setApeMat(rs.getString("APEMAT"));
				per.setNombre(rs.getString("NOMBRE"));
				per.setEstCiv(rs.getInt("ESTCIV"));
				per.setSexo(rs.getInt("SEXO"));
				per.setFecNac(rs.getInt("FECNAC"));
				per.setLugNac(rs.getString("LUGNAC"));
				per.setPaiNac(rs.getString("PAINAC"));
				per.setNacion(rs.getInt("NACION"));
				per.setDetNac(rs.getString("DETNAC"));
				per.setDomici(rs.getString("DOMICI"));
				per.setComuna(rs.getInt("COMUNA"));
				per.setCiudad(rs.getInt("CIUDAD"));
				per.setRegion(rs.getInt("REGION"));
				per.setCodGra(rs.getInt("CODGRA"));
				per.setGraTit(rs.getInt("GRATIT"));
				per.setCodTit(rs.getInt("CODTIT"));
				per.setFecIng(rs.getInt("FECING"));
				per.setIngPor(rs.getString("INGPOR"));
				per.setTelefo(rs.getString("TELEFO"));
				per.setRolCap(rs.getInt("ROLCAP"));
				per.setNivAca(rs.getString("NIVACA"));
				per.setSemAnt(rs.getInt("SEMANT"));
				per.setClaInt(rs.getString("CLAINT"));
				per.setIndAct(rs.getString("INDACT"));
				per.setFecCam(rs.getInt("FECCAM"));
				per.setCelula(rs.getString("CELULA"));
				per.setCorele(rs.getString("CORELE"));
				per.setDomele(rs.getString("DOMELE"));
				list.add(per);
				
			}
			
		} catch (SQLException e) {
			System.out.print("SQLERROR IseriesAppef01DAO.guardarListPer :"+e.getMessage());
		} catch (Exception e) {
			System.out.print("ERROR IseriesAppef01DAO.guardarListPer :"+e.getMessage());
		}	
		try {
			con.close();
		} catch (SQLException e) {
			System.out.print("SQLERROR IseriesAppef01DAO.guardarListPer :"+e.getMessage());
		}
		return list;
	}
	
	public static void guardarListPer (List<IseriesAppef01> listPer, ConexionBannerRRHH con){
		
		String sql = " INSERT INTO TMP_APPEF01(" +
								"RUTIDE,DIGIDE,APEPAT,APEMAT,NOMBRE,ESTCIV,SEXO,FECNAC,LUGNAC,PAINAC, "+
								"NACION,DETNAC,DOMICI,COMUNA,CIUDAD,REGION,CODGRA,GRATIT,CODTIT,FECING, "+
								"INGPOR,TELEFO,ROLCAP,NIVACA,SEMANT,CLAINT,INDACT,FECCAM,CELULA,CORELE, "+
								"DOMELE)" +
					"VALUES(?,?,?,?,?,?,?,?,?,?," +
					       "?,?,?,?,?,?,?,?,?,?," +
					       "?,?,?,?,?,?,?,?,?,?,?)";
		try {
			
			PreparedStatement sent = con.getConexion().prepareStatement(sql);
			for (IseriesAppef01 per : listPer) {
				sent.setInt(1,per.getRutide());
				sent.setString(2,per.getDigide());
				sent.setString(3,per.getApePat());
				sent.setString(4,per.getApeMat());
				sent.setString(5,per.getNombre());
				sent.setInt(6,per.getEstCiv());
				sent.setInt(7,per.getSexo());
				sent.setInt(8,per.getFecNac());
				sent.setString(9,per.getLugNac());
				sent.setString(10,per.getPaiNac());
				sent.setInt(11,per.getNacion());
				sent.setString(12,per.getDetNac());
				sent.setString(13,per.getDomici());
				sent.setInt(14,per.getComuna());
				sent.setInt(15,per.getCiudad());
				sent.setInt(16,per.getRegion());
				sent.setInt(17,per.getCodGra());
				sent.setInt(18,per.getGraTit());
				sent.setInt(19,per.getCodTit());
				sent.setInt(20,per.getFecIng());
				sent.setString(21,per.getIngPor());
				sent.setString(22,per.getTelefo());
				sent.setInt(23,per.getRolCap());
				sent.setString(24,per.getNivAca());
				sent.setInt(25,per.getSemAnt());
				sent.setString(26,per.getClaInt());
				sent.setString(27,per.getIndAct());
				sent.setInt(28,per.getFecCam());
				sent.setString(29,per.getCelula());
				sent.setString(30,per.getCorele());
				sent.setString(31,per.getDomele());
				sent.addBatch();
			}			
			sent.executeBatch();
			sent.clearBatch();
			sent.close();
			
		} catch (SQLException e) {
			System.out.print("SQLERROR IseriesAppef01DAO.guardarListPer :"+e.getMessage());
		} catch (Exception e) {
			System.out.print("ERROR IseriesAppef01DAO.guardarListPer :"+e.getMessage());
		}
		
	}

	

}
