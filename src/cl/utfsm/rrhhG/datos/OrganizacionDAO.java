package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import oracle.jdbc.OracleTypes;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.Organizacion;

/**
 *Nombre classe: OrganizacionDAO
 *Programado por: Yonis Vergara
 *Fecha Creacion:06-09-2017
 *Vercion: 1.0
 *Ultima Verción: 06-09-2017
 *Modificado por :Yonis Vergara
 *Razon de cambio:Creacion de la clase
 *Descripción: esta clase para el control del el objeto Organizacion con DB 
 */
public class OrganizacionDAO {
	
	
	
	public OrganizacionDAO() {
		
	}

	public static Organizacion buscarOrgnPorCod(String codigoOrgn){
		
		  String sql = "{call PKG_CONSULTA_USUARIO_RRHH.ORGN_por_CODORGN(?,?)}";
		  
		  ConexionBannerRRHH con = new ConexionBannerRRHH();
		  CallableStatement sent;	  
		  ResultSet rs;
		  Organizacion orgn  = null;
		  try{ 
		       sent = con.conexion().prepareCall(sql);
		       sent.setString(1, codigoOrgn);		     
		       sent.registerOutParameter(2, OracleTypes.CURSOR);
		       sent.executeQuery();
		       
		       try {
		    	   /** modificación 21-02-2018 por: Yonis Vergara */
		    	   //rs = ((OracleCallableStatement)sent).getCursor(2);
		    	   rs = (ResultSet)sent.getObject(2);
		    	   while (rs.next()) {
		    		   orgn = new Organizacion();
		    		   orgn.setCodOrgn(rs.getNString("FTVORGN_ORGN_CODE"));
		    		   orgn.setCodFondo(rs.getNString("FTVORGN_FUND_CODE_DEF"));
		    		   orgn.setCodPrograma(rs.getNString("FTVORGN_PROG_CODE_DEF"));
		    		   orgn.setDescOreg(rs.getNString("FTVORGN_TITLE"));
					
		    	   }
			       rs.close();
		       } catch (SQLException e){
		    	   System.out.println("Error UsmCuentaHabDesDAO.descOrgn: " + e);
			       System.out.println("cursor" );
			   }		       
		       sent.close(); 
		       con.close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error UsmCuentaHabDesDAO.descOrgn: " + e);
		       System.out.println("antes del cursor " );
		   }
		
		return orgn;
	}
	public ArrayList<Organizacion> buscarOrgnList(String buscar){
		
		  String sql = "{call PKG_CONSULTA_USUARIO_RRHH.buscarORGN(?,?)}";
		  ArrayList<Organizacion> list = new ArrayList<Organizacion>();
		  ConexionBannerRRHH con = new ConexionBannerRRHH();
		  CallableStatement sent;		  
		
		  try{ 
		       sent = con.conexion().prepareCall(sql);
		       sent.setString(1, buscar);		     
		       sent.registerOutParameter(2, OracleTypes.CURSOR);
		       sent.executeQuery();
		       
		       try {
		    	   /** modificación 21-02-2018 por: Yonis Vergara */
		    	   // ResultSet rs = ((OracleCallableStatement)sent).getCursor(2);	
		    	   ResultSet rs = (ResultSet)sent.getObject(2);	
		    	   while (rs.next()) {
					Organizacion orgn = new Organizacion();
					orgn.setCodOrgn    (rs.getString("FTVORGN_ORGN_CODE"));
					orgn.setDescOreg   (rs.getString("FTVORGN_TITLE"));
					orgn.setCodFondo   (rs.getString("FTVORGN_FUND_CODE_DEF"));
					orgn.setCodPrograma(rs.getString("FTVORGN_PROG_CODE_DEF"));
					list.add(orgn);
		    	   }
			       
		       } catch (SQLException e){
		    	   System.out.println("Error UsmCuentaHabDesDAO.buscarOrgnList: " + e);
			       System.out.println("cursor" );
			   }		       
		       sent.close(); 
		       con.close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error UsmCuentaHabDesDAO.buscarOrgnList: " + e);
		       System.out.println("antes del cursor " );
		   }
		
		return list;
	}
}
