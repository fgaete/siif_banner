package cl.utfsm.rrhhG.datos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import cl.utfsm.conexion.ConexionAs400RRHHG;
import cl.utfsm.rrhhG.modulo.Planta;

/**
 *Clase: PlantaDAO
 *Programado por: Yonis Vergara
 *Fecha Creacion:07-09-2017
 *Vercion: 1.0
 *Ultima Verci�n: 07-09-2017
 *Modificado por :Yonis Vergara
 *Razon de cambio:creacion de clase y sus metodods
 *Descripci�n: esta clase hace la union entre la clase Planta y la base de datos de AS400
 *
 */

public class PlantaDAO {
	
	public PlantaDAO() {
		
	}
	/**
	 *Clase: mapaPlanta
	 *Programado por: Yonis Vergara
	 *Fecha Creacion:07-09-2017
	 *Vercion: 1.0
	 *Ultima Verci�n: 07-09-2017
	 *Modificado por :Yonis Vergara
	 *Razon de cambio:optener planta 
	 *Descripci�n:optener las plantas que existen en AS400 
	 *
	 */
	public HashMap<String, Planta> mapaPlanta(){
		
		  String sql = " SELECT CODPLA,DESPLA FROM REMUNERABT.APREF07 ";
		  Planta planta = new Planta();
		  ConexionAs400RRHHG con = new ConexionAs400RRHHG();
		  PreparedStatement sent ;
		  HashMap<String, Planta> mapaPlanta = new HashMap<String, Planta>();		  
		  try{ 
		       sent = con.getConnection().prepareCall(sql);
		             
		       ResultSet rs = sent.executeQuery();
		       
		       try {
		    	   
		    	   while (rs.next()) {
		    		   planta = new Planta();
		    		   planta.setDescPlanta(rs.getString("DESPLA").trim());
		    		   planta.setCodigoPlanta(rs.getString("CODPLA"));
		    		   if(planta.getCodigoPlanta().equals("3")){
		    			   planta.setCodigoPlanta("AY");
		    		   }
		    		   mapaPlanta.put(planta.getCodigoPlanta(), planta);
		    	   }		              
		    	   planta = new Planta();
	    		   planta.setDescPlanta("HONORARIOS");
	    		   planta.setCodigoPlanta("HO");
	    		   
	    		   mapaPlanta.put(planta.getCodigoPlanta(), planta);
		       } catch (SQLException e){
		    	   System.out.println("Error UsmCuentaHabDesDAO.descCODHDE: " + e);
			       System.out.println("cursor" );
			   }		       
		       sent.close(); 
		       con.getConnection().close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error UsmCuentaHabDesDAO.descCODHDE: " + e);
		       System.out.println("antes del cursor " );
		   }
		
		return mapaPlanta;
	}
}
