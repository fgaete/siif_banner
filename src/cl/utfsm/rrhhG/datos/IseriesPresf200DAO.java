package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cl.utfsm.conexion.ConexionAs400RRHHG;
import cl.utfsm.conexion.ConexionAs400RRHHI1;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.IseriesApref08;
import cl.utfsm.rrhhG.modulo.IseriesPresf200;
public class IseriesPresf200DAO {

	public static List<IseriesPresf200> listOrg(String nomConeccion){
		Connection conIseries ;
		if(nomConeccion.equals("GURFEED")){
			conIseries  = (new ConexionAs400RRHHG()).generaConnection();
		}else if (nomConeccion.equals("INTERFACE1")) {
			conIseries  = (new ConexionAs400RRHHI1()).generaConnection();
		}else{
			return null;
		}
		List<IseriesPresf200> list = new ArrayList<IseriesPresf200>();
		String sql = "select * from USMMBP.presf200";
		
		try {
			PreparedStatement sent = conIseries.prepareStatement(sql);
			ResultSet rs = sent.executeQuery();
			IseriesPresf200 org;
			while (rs.next()) {
				org = new IseriesPresf200();
				org.setCodOrg(rs.getString("CODORG"));
				org.setDesOrg(rs.getString("DESORG"));
				org.setIndAut(rs.getString("INDAUT"));
				org.setCodFon(rs.getString("CODFON"));
				org.setCodPro(rs.getString("CODPRO"));
				list.add(org);
			}
			
		} catch (SQLException e) {
			System.out.print("SQLError : IseriesPresf200DAO:listOrg :" +e.getMessage());
		} catch (Exception e) { 
			System.out.print("Error : IseriesPresf200DAO:listOrg :" +e.getMessage());
		}
		try {
			conIseries.close();
		} catch (SQLException e) {
			System.out.print("SQLError : IseriesPresf200DAO:listOrg :" +e.getMessage());
		} catch (Exception e) { 
			System.out.print("Error : IseriesPresf200DAO:listOrg :" +e.getMessage());
		}
		return list;
	}
	/*
	 * ********************************IMPORTANTE*************************************************
	 * este metodo no sierra la conecion para no persder la sescion ya que esta tabla es temporal
	 * ********************************IMPORTANTE*************************************************
	 */
	/*************************************************************************************
	 *******************************IMPORTANTE********************************************
	 *Este metodo no cierra la conexcion para no perder la session esta tabla es Temporal*
	 *******************************IMPORTANTE********************************************
	 *************************************************************************************
	 */
	
	public static void guardarListOrg(List<IseriesPresf200> listOrg,ConexionBannerRRHH con){
		
		String sql = "INSERT INTO TMP_PRESF200(CODORG,DESORG,INDAUT,CODFON,CODPRO) "+
                      "VALUES(?,?,?,?,?)"; 
	try {
		CallableStatement sent = con.getConexion().prepareCall(sql);
		for (IseriesPresf200 org : listOrg) {
			sent.setString(1,org.getCodOrg());
			sent.setString(2,org.getDesOrg());
			sent.setString(3,org.getIndAut());
			sent.setString(4,org.getCodFon());
			sent.setString(5,org.getCodPro());
			sent.addBatch();
		}
		sent.executeBatch();
		sent.clearBatch();
		sent.close();
	} catch (SQLException e) {
		System.out.print("SQLERROR en : IsriesPresf200DAO.guardarListOrg :\n" +e.getMessage());
	} catch (Exception e) {
		System.out.print("ERROR en : IsriesPresf200DAO.guardarListOrg :\n"+e.getMessage());
	}
				
	}
}
