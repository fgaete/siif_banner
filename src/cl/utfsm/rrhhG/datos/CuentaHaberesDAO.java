package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.internal.OracleTypes;
import oracle.jdbc.oracore.OracleType;

import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.CuentaHaberes;

public class CuentaHaberesDAO {
	private static List<CuentaHaberes> buscarHaberes(int codigo ,String planta,String nivel){
		System.out.print(codigo+"-"+planta+"-"+nivel);
		String sql = "{call PKG_CONSULTA_USUARIO_RRHH.CONSULTA_CUENTA_HABERES( "+
											    " PI_CODIGO => ?, "+
											    " PI_PLANTA => ?, "+
											    " PI_NIVEL  => ?, "+
											    " PO_CUENTA_HABERES => ? "+
											    ")}";
		List<CuentaHaberes> listHaberes = new ArrayList<CuentaHaberes>();
		try {
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		CallableStatement sent = con.getConexion().prepareCall(sql);
		
		sent.setInt(1, codigo);
		sent.setString(2, planta);
		sent.setString(3,nivel);
		sent.registerOutParameter(4,OracleTypes.CURSOR);		
		sent.execute();		
		ResultSet rs = (ResultSet)sent.getObject(4);
		CuentaHaberes cuentaHaberes = null;
		
		while (rs.next()) {
			cuentaHaberes = new CuentaHaberes(); 
			cuentaHaberes.setCodigo(rs.getInt("CODIGO"));
			cuentaHaberes.setPlanta(rs.getString("PLANTA"));
			cuentaHaberes.setNivel(rs.getString("NIVEL"));
			cuentaHaberes.setCuenta(rs.getString("CUENTA"));
			cuentaHaberes.setOrganizacion(rs.getString("ORGANIZACION"));
			cuentaHaberes.setFondo(rs.getString("FONDO"));
			cuentaHaberes.setPrograma(rs.getString("PROGRAMA"));
			listHaberes.add(cuentaHaberes);
		}		
		con.getConexion().close();
		} catch (SQLException e) {
			System.out.print("SQLError CuentaHaberesDAO.buscarHaberes: "+e.getMessage());
		} catch (Exception e) {
			System.out.print("Error CuentaHaberesDAO.buscarHaberes: "+e.getMessage());
		}		
		return listHaberes;		
	}
	public static List<CuentaHaberes> listHaberes(){
		return buscarHaberes( 0 , "-1", "-1");		
	}
	public static List<CuentaHaberes> listHaberesPorCodigo(int codigo ){
		return buscarHaberes( codigo , "-1", "-1");		
	}
	public static List<CuentaHaberes> listHaberesPorPlanta(String planta ){
		return buscarHaberes( 0 , planta, "-1");		
	}
	public static List<CuentaHaberes> listHaberesPorNivel(String nivel ){
		return buscarHaberes( 0 , "-1", nivel);		
	}
	public static List<CuentaHaberes> listHaberesPorCodigoYPlanta(int codigo,String planta ){
		return buscarHaberes( codigo , planta, "-1");		
	}
	public static List<CuentaHaberes> listHaberesPorCodigoYNivel(int codigo,String nivel ){
		return buscarHaberes( codigo , "-1", nivel);		
	}
	public static List<CuentaHaberes> listHaberesPorPlantaYNivel(String planta,String nivel ){
		return buscarHaberes( 0 , planta, nivel);		
	}
	public static List<CuentaHaberes> listHaberesPorPlantaYPlantaYNivel(int codigo ,String planta,String nivel ){
		return buscarHaberes( codigo , planta, nivel);		
	}
	public static CuentaHaberes reglaCuentaHaber(int codigo ,String planta,String nivel ){
		List<CuentaHaberes> list = listHaberesPorPlantaYPlantaYNivel( codigo , planta, nivel );
		if(list.size() == 1){
			return list.get(0);
		}
		return null;
	}
	private static boolean guardarReglaCuentaHaberes(String tipo,CuentaHaberes cuenHab){
		boolean repuesta = true;
		String sql = "{call PKG_CONSULTA_USUARIO_RRHH.GU_REG_CUENTA_HAVERES( "+
														    " PI_CODIGO => ?, "+
														    " PI_PLANTA => ?, "+
														    " PI_NIVEL => ?, "+
														    " PI_CUENTA => ?, "+
														    " PI_ORGANIZACION => ?, "+
														    " PI_PROGRAMA => ?, "+
														    " PI_FONDO => ?, "+
														    " PI_TIPO => ? "+
														    " ) }";
		try {
			ConexionBannerRRHH con = new ConexionBannerRRHH();
			CallableStatement sent = con.getConexion().prepareCall(sql);
			sent.setInt(1, cuenHab.getCodigo());
			sent.setString(2, cuenHab.getPlanta());
			sent.setString(3, cuenHab.getNivel());
			sent.setString(4, cuenHab.getCuenta());
			sent.setString(5, cuenHab.getOrganizacion());
			sent.setString(6, cuenHab.getPrograma());
			sent.setString(7, cuenHab.getFondo());
			sent.setString(8, tipo);
			sent.execute();
			con.getConexion().close();
		} catch (SQLException e) {
			repuesta = false;
			System.out.print("SQLError CuentaHaberesDAO.guardarReglaCuentaHaberes: "+e.getMessage());
		} catch (Exception e) {
			System.out.print("Error CuentaHaberesDAO.guardarReglaCuentaHaberes: "+e.getMessage());
			repuesta = false;
		}
		
		return repuesta;
	}
	public static boolean guardarReglaCuentaHaberesDescuento(CuentaHaberes cuenta){
		
		return guardarReglaCuentaHaberes("INSERT", cuenta);
		
	}
	public static boolean actualizarReglaCuentaHaberesDescuento(CuentaHaberes cuenta){
		
		return guardarReglaCuentaHaberes("UPDATE", cuenta);
		
	}
	
}