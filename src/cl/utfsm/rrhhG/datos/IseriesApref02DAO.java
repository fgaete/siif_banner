package cl.utfsm.rrhhG.datos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import cl.utfsm.conexion.ConexionAs400RRHHG;
import cl.utfsm.conexion.ConexionBannerRRHH;

public class IseriesApref02DAO {
	
	public static int valorAporteServicioMedicoOperario(int anopro , int mespro){
		int valor = 0;
		String sql = "select * from remunerabt.apref02 where anopro = ? and mespro = ? and codpar = ?";
		String codpar = "SMO";
		try {
			ConexionAs400RRHHG con = new ConexionAs400RRHHG();
			PreparedStatement sent = con.getConnection().prepareStatement(sql);
			
			sent.setInt(1, anopro);
			sent.setInt(2, mespro);
			sent.setString(3, codpar);
			ResultSet rs = sent.executeQuery();
			while (rs.next()) {
				valor = rs.getInt("VALNUM");				
			}
			con.getConnection().close();
		} catch (SQLException e){
			System.out.print("SQLError IseriesApref02DAO.valorAporteServicioMedicoOperario: "+ e.getMessage());
		} catch (Exception e) {
			System.out.print("Error IseriesApref02DAO.valorAporteServicioMedicoOperario: "+ e.getMessage());
		}
		
		return valor;
	}
	
}
