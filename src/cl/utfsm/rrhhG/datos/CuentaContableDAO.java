package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import oracle.jdbc.OracleTypes;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.CuentaContable;


/**
 *Clase: CuentaContableDAO
 *Programado por: Yonis Vergara
 *Fecha Creacion:22-09-2017
 *Vercion: 1.0
 *Ultima Verción: 22-09-2017
 *Modificado por :Yonis Vergara
 *Razon de cambio:creacion de clase y sus metodods
 *Descripción: Esta clase hace la union entre la clase CuentaContable 
 *					y la base de datos de BANNER tabla FTVACCT
 *
 */
public class CuentaContableDAO {
	
	public CuentaContableDAO (){
		
	}
	public CuentaContable cuentaPorCodigo(String codigoCuenta){
			
			  String sql = " { call PKG_CONSULTA_USUARIO_RRHH.CUENTA_POR_CODIGO( ? ,"//PO_CODCUENTA codigo de cuenta a buscar
																		    + " ? "//cursor de salida
																			+ " ) } " ;
			  CuentaContable cuentaContable = null;				  
			  ConexionBannerRRHH con = new ConexionBannerRRHH();
			  CallableStatement sent ;
			  ResultSet rs = null;
			  
			  try{ 
			       sent = con.getConexion().prepareCall(sql);
			       sent.setString(1, codigoCuenta); 
			       sent.registerOutParameter(2, OracleTypes.CURSOR);
			       sent.executeQuery();
			       
			       try {
			    	   /** modificación 21-02-2018 por: Yonis Vergara */
			    	   //rs = ((OracleCallableStatement)sent).getCursor(2);
			    	   rs = (ResultSet)sent.getObject(2);
			    	   while (rs.next()) {
			    		   cuentaContable = new CuentaContable();
			    		   cuentaContable.setCodigoCuenta(rs.getString("FTVACCT_ACCT_CODE").trim());
			    		   cuentaContable.setDescCuenta(rs.getString("FTVACCT_TITLE"));		    		   
			    	   }		              
				       
			       } catch (SQLException e){
			    	   System.out.println("Error UsmCuentaHabDesDAO.descCODHDE: " + e);
				       System.out.println("cursor" );
				   }
			       rs.close();
			       sent.close(); 
			       con.close();	       
			   }
			   catch (SQLException e){
			       System.out.println("Error UsmCuentaHabDesDAO.descCODHDE: " + e);
			       System.out.println("antes del cursor " );
			   
			   }
			
			
		
		
		return cuentaContable;
	}
	
	public ArrayList<CuentaContable> busquedaCuenta(String Bus){
		
		  String sql = " { call PKG_CONSULTA_USUARIO_RRHH.BUSCAR_CUENTAS(   ? ,"//PI_CODCUENTA codigo de cuenta a buscar
																	    + " ? , " //PI_NUM_COL cantidad de columnas a traer como maximo de la columna 0 significa que traiga todas 
																		+ " ? ) } " ;//cursor de salida
		  CuentaContable cuentaContable = new CuentaContable();				  
		  ConexionBannerRRHH con = new ConexionBannerRRHH();
		  CallableStatement sent ;
		  ArrayList<CuentaContable> list = new ArrayList<CuentaContable>();
		  ResultSet rs = null;
		  
		  try{ 
		       sent = con.getConexion().prepareCall(sql);
		       sent.setString(1, Bus);
		       sent.setInt(2, 0); 
		       sent.registerOutParameter(3, OracleTypes.CURSOR);
		       sent.executeQuery();
		       
		       try {
		    	   /** modificación 21-02-2018 por: Yonis Vergara */
		    	   //rs = ((OracleCallableStatement)sent).getCursor(3);
		    	   rs = (ResultSet)sent.getObject(3);
		    	   while (rs.next()) {
		    		   cuentaContable = new CuentaContable();
		    		   cuentaContable.setCodigoCuenta(rs.getString("FTVACCT_ACCT_CODE").trim());
		    		   cuentaContable.setDescCuenta(rs.getString("FTVACCT_TITLE"));
		    		   list.add(cuentaContable);
		    	   }		              
			       
		       } catch (SQLException e){
		    	   System.out.println("Error UsmCuentaHabDesDAO.descCODHDE: " + e);
			       System.out.println("cursor" );
			   }
		       rs.close();
		       sent.close(); 
		       con.close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error UsmCuentaHabDesDAO.descCODHDE: " + e);
		       System.out.println("antes del cursor " );
		   
		   }	
		   return list;
	}
	
	public ArrayList<CuentaContable> busquedaCuenta(String Bus, int catColumnas){
		
		  String sql = " { call PKG_CONSULTA_USUARIO_RRHH.BUSCAR_CUENTAS(   ? ,"//PI_CODCUENTA codigo de cuenta a buscar
																	    + " ? , " //PI_NUM_COL cantidad de columnas a traer como maximo de la columna 0 significa que traiga todas 
																		+ " ? ) } " ;//cursor de salida
		  CuentaContable cuentaContable = new CuentaContable();				  
		  ConexionBannerRRHH con = new ConexionBannerRRHH();
		  CallableStatement sent ;
		  ArrayList<CuentaContable> list = new ArrayList<CuentaContable>();
		  ResultSet rs = null;
		  
		  try{ 
		       sent = con.getConexion().prepareCall(sql);
		       sent.setString(1, Bus);
		       sent.setInt(2, catColumnas); 
		       sent.registerOutParameter(3, OracleTypes.CURSOR);
		       sent.executeQuery();
		       
		       try {
		    	   /** modificación 21-02-2018 por: Yonis Vergara */
		    	   // rs = ((OracleCallableStatement)sent).getCursor(3);
		    	   rs = (ResultSet)sent.getObject(3);
		    	   while (rs.next()) {
		    		   cuentaContable = new CuentaContable();
		    		   cuentaContable.setCodigoCuenta(rs.getString("FTVACCT_ACCT_CODE").trim());
		    		   cuentaContable.setDescCuenta(rs.getString("FTVACCT_TITLE"));
		    		   list.add(cuentaContable);
		    	   }		              
			       
		       } catch (SQLException e){
		    	   System.out.println("Error UsmCuentaHabDesDAO.descCODHDE: " + e);
			       System.out.println("cursor" );
			   }
		       rs.close();
		       sent.close(); 
		       con.close();	       
		   }
		   catch (SQLException e){
		       System.out.println("Error UsmCuentaHabDesDAO.descCODHDE: " + e);
		       System.out.println("antes del cursor " );
		   
		   }	
		   return list;
	}

}