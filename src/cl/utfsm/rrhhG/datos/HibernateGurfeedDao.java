package cl.utfsm.rrhhG.datos;

import java.sql.CallableStatement;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;
import oracle.jdbc.OracleTypes;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import cl.utfsm.base.util.Util;
import cl.utfsm.conexion.ConexionAs400RRHHG;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.CampusEQ;
import cl.utfsm.rrhhG.modulo.IseriesAppef05;
import cl.utfsm.rrhhG.modulo.IseriesAppef06;
import cl.utfsm.rrhhG.modulo.IseriesApref02;
import cl.utfsm.rrhhG.modulo.IseriesApref08;
import cl.utfsm.rrhhG.modulo.IseriesApref17;
import cl.utfsm.rrhhG.modulo.IseriesApref18;
import cl.utfsm.rrhhG.modulo.IseriesApref41;
import cl.utfsm.rrhhG.modulo.IseriesApref67;
import cl.utfsm.rrhhG.modulo.IseriesPresf200;
import cl.utfsm.rrhhG.modulo.RegistroProceso;

public class HibernateGurfeedDao extends HibernateDaoSupport implements GurfeedDao{
	
	public static HashMap<Integer,CampusEQ> campusEq(){
		
		String sql = "{call PKG_CONSULTA_USUARIO_RRHH.P_CAMPUS_EQ( ? )}";
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		HashMap<Integer,CampusEQ> eq = new HashMap<Integer, CampusEQ>();
		try {
			CallableStatement sent = con.getConexion().prepareCall(sql);
			sent.registerOutParameter(1, OracleTypes.CURSOR);
			sent.execute();
			ResultSet rs = (ResultSet)sent.getObject(1);
			CampusEQ eqCampus ;
			while (rs.next()) {
				eqCampus = new CampusEQ();
				eqCampus.setStvCampCode(rs.getInt("STVCAMP_CODE"));
				eqCampus.setStvCampDesc(rs.getString("STVCAMP_DESC"));
				eqCampus.setAs400(rs.getInt("AS400"));
				eq.put(Util.validaParametro(rs.getInt("AS400"),0), eqCampus);
			}
		} catch (SQLException e) {
			System.out.println("SQLError HibernateGurfeedDao.campusEq: " + e);			
		} catch (Exception e) {
			System.out.println("Error HibernateGurfeedDao.campusEq: " + e);			
		}
		
		return eq;
	}
	
	public static String mesUsado(int mes , int anno ){
		String usado = "true";
		try {
		
		ConexionBannerRRHH con =   new ConexionBannerRRHH();
		CallableStatement sent = null;	
	    String sql = "{call PKG_CONSULTA_USUARIO_RRHH.MES_USADO( "
														    +" ?,"// PI_MES => PI_MES,
														    +" ?,"//  PI_ANNO => PI_ANNO,
														    +" ? "//  PO_USADO => PO_USADO
														    +" )}";		
			sent = con.getConexion().prepareCall(sql);;
			sent.setInt(2, anno);
			sent.setInt(1, mes);		
			sent.registerOutParameter(3, OracleTypes.VARCHAR);	
			sent.execute();
			usado = sent.getString(3); 		
			
		    sent.close();
		    con.close();
			
		} catch (SQLException e) {
			System.out.println("Error HibernateGurfeedDao.mesUsado: " + e);			
		}	
		return usado;
	}
	/********************Cambio**********************************************/
	
	public  Collection<String> integrado(HttpSession session){ 
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		
		CallableStatement sent = null;
	    ResultSet res = null;
	    
	    List<String> lista = new ArrayList<String>();
	    int mes = (Integer)session.getAttribute("mes");
		int anno = (Integer)session.getAttribute("anno");
	    
	    try{
	    	  con.getConexion().setAutoCommit(false);
	    	  
	    	  IseriesAppef05DAO.guardarListCon(IseriesAppef05DAO.contratoPorMes(anno, mes, "GURFEED"), con);
	    	  
	    	  sent = con.getConexion().prepareCall("{call PKG_CONSULTA_USUARIO_RRHH.INTEGRADO( "
																					    +" ? ," //PI_MES => PI_MES,
																					    +" ? ," //PI_ANNO => PI_ANNO,
																					    +" ? " //PO_USADO => PO_USADO
																					    +")} ");		      
		      sent.setInt(1, Integer.parseInt(""+session.getAttribute("mes")));
		      sent.setInt(2, Integer.parseInt(""+session.getAttribute("anno")));
		      sent.registerOutParameter(3, OracleTypes.CURSOR);		      
		       sent.executeQuery();
		       /** modificación 21-02-2018 por: Yonis Vergara */
		       //res = ((OracleCallableStatement)sent).getCursor(3);
		       res = (ResultSet)sent.getObject(3);
		      while (res.next()){
			    	String ss = "";
			        ss = res.getString(1); // rut que no estan en spaiden
			        lista.add(ss);
		      }
		      res.close();
		      sent.close();
		      con.getConexion().commit();
		     
		      con.getConexion().close();
		    }catch (SQLException e){
		    	System.out.println("SQLError HibernateGurfeedDao.integrado: " + e);
			}catch(Exception e){
				System.out.println("Error HibernateGurfeedDao.integrado: " + e);
				
			}
	    
	    
		return lista;
	}
	
	public void ultimoGurfeed(){
		
		
		
	}
	
	public void nomina(HttpSession session){
		
		//ConexionBannerRRHH con = new ConexionBannerRRHH();
		ConexionAs400RRHHG con = new ConexionAs400RRHHG();
		PreparedStatement sent = null;
	    
		
	    try{	    	 
	    	  sent = con.getConnection().prepareCall(" select ANOPRO , MESPRO "
													+" from remunerabt.apref16 " 
													+" where (ANOPRO *100) + MESPRO =(select max((ANOPRO *100) + MESPRO) from remunerabt.apref16 where ESTPRO = 'C' ) ");	    	
		      ResultSet rs = sent.executeQuery();	      
			  while (rs.next()) {
				
				  session.setAttribute("mes"   , rs.getInt(2));
		    	  session.setAttribute("anno"  , rs.getInt(1));
		    	  String fecha = ("00".substring( rs.getString(2).length()) + rs.getString(2)+"/"+rs.getString(1));
		    	  //session.setAttribute("nomina", sent.getString(3));		    	  
		    	  Date date1 = new SimpleDateFormat("MM/yyyy").parse(fecha); 
		    	  SimpleDateFormat formateador = new SimpleDateFormat(" MMMM 'de' yyyy", new Locale("ES"));
		    	  session.setAttribute("nomina", formateador.format(date1));
			  }
	    	  	        
		      
		      //res.close();
		      sent.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error HibernateGurfeedDao.nomina: " + e);
			}catch (Exception e){
				System.out.println("Error HibernateGurfeedDao.nomina: " + e);
			}		
	}
    public void nomina(HttpSession session,int mesE , int anoE){
		
	ConexionBannerRRHH con = new ConexionBannerRRHH();
	CallableStatement sent = null;
	System.out.print("\n ");
    
	
    try{	 	  
    	  sent = con.conexion().prepareCall("{ call PKG_CONSULTA_USUARIO_RRHH.NOMINA(?,?,?) }");
    	  sent.setInt(1, mesE);
    	  sent.setInt(2, anoE);
    	 
    	  sent.registerOutParameter(1,OracleTypes.NUMBER);
	      sent.registerOutParameter(2,OracleTypes.NUMBER);
	      sent.registerOutParameter(3,OracleTypes.VARCHAR);
	      
	      sent.execute();	      
		    	
    	  session.setAttribute("mes"   , sent.getInt(1));
    	  session.setAttribute("anno"  , sent.getInt(2));
    	  session.setAttribute("nomina", sent.getString(3));	        
	      
	      //res.close();
	      sent.close();
	      con.close();
	    }
	    catch (SQLException e){
	    	System.out.println("Error HibernateGurfeedDao.nomina: " + e);
		}catch (Exception e){
			System.out.println("Error HibernateGurfeedDao.nomina: " + e);
		}
	}
    /*
    public void contadorReg(ConexionBannerRRHH con){
    	
    	String sql = "{call PKG_CONSULTA_USUARIO_RRHH.CONTADOR_REGISTROS( ? )}";
    	
    	
    	
    	try {
			CallableStatement sent = con.getConexion().prepareCall(sql);
			
			sent.registerOutParameter(1, OracleTypes.CURSOR);
			sent.execute();
			ResultSet rs = (ResultSet)sent.getObject(1);
			
			while (rs.next()) {
				
				System.out.print("\n Appef01: "+rs.getInt("APPEF01")+"\n");
				System.out.print("\n APPEF05: "+rs.getInt("APPEF05")+"\n");
				System.out.print("\n APPEF06: "+rs.getInt("APPEF06")+"\n");
				System.out.print("\n APPEF09: "+rs.getInt("APPEF09")+"\n");
				System.out.print("\n APPEF13: "+rs.getInt("APPEF13")+"\n");
				System.out.print("\n APREF06: "+rs.getInt("APREF06")+"\n");
				System.out.print("\n APREF07: "+rs.getInt("APREF07")+"\n");
				System.out.print("\n APREF08: "+rs.getInt("APREF08")+"\n");
				System.out.print("\n APREF11: "+rs.getInt("APREF11")+"\n");
				System.out.print("\n APREF17: "+rs.getInt("APREF17")+"\n");
				System.out.print("\n APREF18: "+rs.getInt("APREF18")+"\n");
				System.out.print("\n APREF41: "+rs.getInt("APREF41")+"\n");
				System.out.print("\n APREF67: "+rs.getInt("APREF67")+"\n");
				
			}
    		
    		
		} catch (SQLException e) {
			System.out.print("\n SQLError HibernateGurfeedDao.contadorReg "+e.getMessage()+"\n");
		} catch (Exception e) {
			System.out.print("\n SQLError HibernateGurfeedDao.contadorReg "+e.getMessage()+"\n");
		}
    	
    	
    }
    */
    
	private void cargarTablaTemp(int anoPro,int mesPro,ConexionBannerRRHH con){
		
		
		List<IseriesAppef05> listCon = IseriesAppef05DAO.contratoPorMes(anoPro, mesPro, "GURFEED");
		List<IseriesAppef06> listSolAyu = IseriesAppef06DAO.listaSoliciAyudante("GURFEED");
		List<IseriesApref08> listPlanta = IseriesApref08DAO.listCargos("GURFEED");
		List<IseriesApref17> listResNomi= IseriesApref17DAO.listResuNomi(anoPro, mesPro, "GURFEED");
		List<IseriesApref18> listDetalle= IseriesApref18DAO.listDetalle(anoPro, mesPro, "GURFEED");
		List<IseriesApref41> listAjusPre= IseriesApref41DAO.listAjustePresupuesto(anoPro, mesPro, "GURFEED");
		List<IseriesPresf200> listOrg = IseriesPresf200DAO.listOrg("GURFEED");
		List<IseriesApref67> listBol = IseriesApref67DAO.todosPorMes(anoPro, mesPro, "GURFEED");
		
		IseriesAppef05DAO.guardarListCon(listCon, con);
		IseriesAppef06DAO.guardarListSolAyu(listSolAyu, con);
		IseriesApref08DAO.guardarListPlanta(listPlanta, con);
		IseriesApref17DAO.guardarListResNom(listResNomi, con);
		IseriesApref18DAO.guardarListDetalle(listDetalle, con);
		IseriesApref41DAO.guardarListAjusPres(listAjusPre, con);
		IseriesPresf200DAO.guardarListOrg(listOrg, con);	
		IseriesApref67DAO.guardarBanner(listBol, con);
		
		
	}
    
	public boolean gurfeed(HttpSession session){
		  //, int valoServMediOpe
		  ConexionBannerRRHH con = new ConexionBannerRRHH();
		 
		  CallableStatement sent;
		  boolean fin  = true; 
		  
		  //System.out.println(rutdv+"-"+rutnum+"-"+numdoc+"-"+tipoBoleta+"-"+fecha_invoice+"-"+comen_vale+"-"+tip_val+"-"+desc_val+"-"+datos);
		  try{ //invoice_dateDATE, pmt_due_dateDATE, trans_dateDATE,doct_codeVARCHAR2,datos_farinvaVARCHAR2
			  
			  con.getConexion().setAutoCommit(false);			  
			  RegistroProceso regpro = (RegistroProceso)session.getAttribute("registroProceso");
			  int mes = (Integer)session.getAttribute("mes");
			  int anno = (Integer)session.getAttribute("anno");
			  int valor = IseriesApref02DAO.valorAporteServicioMedicoOperario(anno, mes);
			  this.cargarTablaTemp(anno,mes,con);
		      sent = con.conexion().prepareCall("{call PKG_NOMINA_GURFEED.P_NOMINA_GURFEED_2(?,?,?,?,?,?)}");
		       
		      sent.setInt(1, regpro.getRegproIdEjecucion());
		      sent.setInt(2, regpro.getCodProceso());
		      sent.setInt(3, regpro.getCodEtapaProceso());
		      sent.setInt(4, mes);
		      sent.setInt(5, anno);
		      sent.setInt(6, valor);
		      sent.executeQuery();		         
		      sent.close(); 		      
		      con.getConexion().close();
		       
		    } catch (SQLException e){
		    	System.out.println("Error HibernateGurfeedDao.gurfeed: " + e);
		    	fin  = false;
			}
		    
		    
		
		return fin;
	}
	
	public String[] pasarGurfeed(  int mes , int anno,int numEje){
		  
		  ConexionBannerRRHH con = new ConexionBannerRRHH();
		  CallableStatement sent;		  
		  String opcion[] = new String[3];
		  opcion[0] = "0";
		  //System.out.println(rutdv+"-"+rutnum+"-"+numdoc+"-"+tipoBoleta+"-"+fecha_invoice+"-"+comen_vale+"-"+tip_val+"-"+desc_val+"-"+datos);
		  try{ //invoice_dateDATE, pmt_due_dateDATE, trans_dateDATE,doct_codeVARCHAR2,datos_farinvaVARCHAR2
		       sent = con.conexion().prepareCall("{call PKG_NOMINA_GURFEED.P_NOMINA_GURFEED_P2(?,?,?,?,?,?)}");
		       sent.setInt(2, mes);
		       sent.setInt(3, anno);
		       sent.registerOutParameter(1, Types.NUMERIC);
		       sent.registerOutParameter(4, Types.VARCHAR);
		       sent.registerOutParameter(5, Types.VARCHAR);
		       sent.setInt(6, numEje);
		       sent.executeQuery();	
		       opcion[0] = ""+sent.getInt(1);
		       opcion[1] = sent.getString(4);
		       opcion[2] = sent.getString(5);
		       sent.close(); 
		       con.close();		       
		   }
		   catch (SQLException e){
		       System.out.println("Error HibernateGurfeedDao.saveCuentasPorPagar: " + e);
		       opcion[0]  = "3";
		   }		
		return opcion;
	}	
	
	public int getNumeroEjecusion(){
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		int ejecucion = 0;
		String sql = "{ call PKG_CONSULTA_USUARIO_RRHH.NUMEROEJECUCION(?) }";
		CallableStatement sent = null;
	    try {
			 sent = con.conexion().prepareCall(sql);
			 sent.registerOutParameter(1, OracleTypes.NUMBER);
			 sent.execute();
			 
			 ejecucion = sent.getInt(1);				 
			 		 
		     sent.close();
		     con.close();
			 
		} catch (SQLException e) {
			System.out.print("HibernateGurfeedDao.getNumeroEjecusion : " +e.getMessage());
		}
		
		return ejecucion ;
	}
	/********************Cambio**********************************************/
}
