package cl.utfsm.rrhhG.datos;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import cl.utfsm.conexion.ConexionAs400RRHHG;
import cl.utfsm.conexion.ConexionAs400RRHHI1;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.IseriesAppef05;

public class IseriesAppef05DAO {
	
	public IseriesAppef05DAO(){
		
	}
	
	public static List<IseriesAppef05> contratoPorMes(int anoPro,int mesPro,String nomConeccion){
		Connection conIseries ;
		if(nomConeccion.equals("GURFEED")){
			conIseries  = (new ConexionAs400RRHHG()).generaConnection();
		}else if (nomConeccion.equals("INTERFACE1")) {
			conIseries  = (new ConexionAs400RRHHI1()).generaConnection();
		}else{
			return null;
		}
		
		String sql = "select * from remunerabt.appef46 where anoPro = ? and mesPro = ?";
		List<IseriesAppef05> list = new ArrayList<IseriesAppef05>();
		try {
					
			PreparedStatement sent = conIseries.prepareStatement(sql);
			sent.setInt(1, anoPro);
			sent.setInt(2, mesPro);			
			ResultSet rs = sent.executeQuery();
			IseriesAppef05 contr = null;
			while(rs.next()){
				contr = new IseriesAppef05();
				contr.setAnoPro(rs.getInt("ANOPRO"));
				contr.setMesPro(rs.getInt("MESPRO"));
				contr.setRutide(rs.getInt("RUTIDE"));
				contr.setDigide(rs.getString("DIGIDE"));
				contr.setTipCon(rs.getInt("TIPCON"));
				contr.setNumCon(rs.getInt("NUMCON"));
				contr.setEscFon(rs.getInt("ESCFON"));
				contr.setCodUni(rs.getInt("CODUNI"));
				contr.setCodPla(rs.getInt("CODPLA"));
				contr.setNumNiv(rs.getInt("NUMNIV"));
				contr.setCodSuc(rs.getInt("CODSUC"));
				contr.setCodCar(rs.getInt("CODCAR"));
				contr.setSumFij(rs.getInt("SUMFIJ"));
				contr.setJorNad(rs.getInt("JORNAD"));
				contr.setCanHor(rs.getInt("CANHOR"));
				contr.setFecIni(rs.getInt("FECINI"));
				contr.setFecFin(rs.getInt("FECFIN"));
				contr.setFecAnt(rs.getInt("FECANT"));
				contr.setFecTer(rs.getInt("FECTER"));
				contr.setIndCol(rs.getInt("INDCOL"));
				contr.setIndMov(rs.getInt("INDMOV"));
				contr.setCodAFP(rs.getInt("CODAFP"));
				contr.setIndJub(rs.getInt("INDJUB"));
				contr.setIndSal(rs.getInt("INDSAL"));
				contr.setValAho(rs.getInt("VALAHO"));
				contr.setUniAho(rs.getString("UNIAHO"));
				contr.setCodIsa(rs.getInt("CODISA"));
				contr.setUniIsa(rs.getString("UNIISA"));
				contr.setValIsa(rs.getInt("VALISA"));
				contr.setAntQui(rs.getInt("ANTQUI"));
				contr.setIndDep(rs.getInt("INDDEP"));
				contr.setSerMed(rs.getInt("SERMED"));
				contr.setCuoMor(rs.getInt("CUOMOR"));
				contr.setSindi1(rs.getInt("SINDI1"));
				contr.setSindi2(rs.getInt("SINDI2"));
				contr.setSindi3(rs.getInt("SINDI3"));
				contr.setTipJer(rs.getString("TIPJER"));
				contr.setHorIni(rs.getInt("HORINI"));
				contr.setHorFin(rs.getInt("HORFIN"));
				contr.setResolu(rs.getInt("RESOLU"));
				contr.setFecRes(rs.getInt("FECRES"));
				contr.setImpCon(rs.getString("IMPCON"));
				contr.setFecIng(rs.getInt("FECING"));
				contr.setFecMod(rs.getInt("FECMOD"));
				contr.setIngPor(rs.getString("INGPOR"));
				contr.setSerPr1(rs.getString("SERPR1"));
				contr.setSerPr2(rs.getString("SERPR2"));
				contr.setSegCes(rs.getInt("SEGCES"));
				contr.setIndBon(rs.getInt("INDBON"));
				contr.setMTipo(rs.getInt("MTIPO"));
				contr.setMRazon(rs.getInt("MRAZON"));
				contr.setMotivo(rs.getString("MOTIVO"));
				contr.setValeSuper(rs.getInt("VALESUPER"));
				contr.setCodBan(rs.getInt("CODBAN"));
				contr.setSucBan(rs.getInt("SUCBAN"));
				contr.setCueBan(rs.getString("CUEBAN"));
				contr.setMRetencion(rs.getInt("MRETENCION"));
				contr.setFecCon(rs.getInt("FECCON"));
				contr.setFecPas(rs.getInt("FECPAS"));
				contr.setUniAug(rs.getString("UNIAUG"));
				contr.setValAug(rs.getInt("VALAUG"));
				contr.setFecInt(rs.getInt("FECINT"));
				contr.setNumNeg(rs.getInt("NUMNEG"));
				contr.setCodOrg(rs.getString("CODORG"));
				contr.setTipPue(rs.getString("TIPPUE"));
				contr.setCodPue(rs.getString("CODPUE"));
				contr.setBaner1(rs.getString("BANER1"));
				contr.setBaner2(rs.getString("BANER2"));
				contr.setBaner3(rs.getString("BANER3"));
				list.add(contr);
			}
			
		} catch (SQLException e) {
			System.out.print("SQLERROR en : IsriesAppef05DAO.contratoPorMes :\n" +e.getMessage());
		} catch (Exception e) {
			System.out.print("ERROR en : IsriesAppef05DAO.contratoPorMes :\n"+e.getMessage());
		}
		try {
			conIseries.close();
		} catch (SQLException e) {
			System.out.print("SQLERROR en : IsriesAppef05DAO.contratoPorMes :\n" +e.getMessage());
		} catch (Exception e) {
			System.out.print("ERROR en : IsriesAppef05DAO.contratoPorMes :\n"+e.getMessage());
		}
		
		return list;
	}
	
	/*************************************************************************************
	 *******************************IMPORTANTE********************************************
	 *Este metodo no cierra la conexcion para no perder la session esta tabla es Temporal*
	 *******************************IMPORTANTE********************************************
	 *************************************************************************************
	 */
	
	public static void guardarListCon(List<IseriesAppef05> listCon,ConexionBannerRRHH con){
		
		String sql = "insert into TMP_APPEF05 (ANOPRO,MESPRO,RUTIDE,DIGIDE,TIPCON,NUMCON,ESCFON,CODUNI,CODPLA,NUMNIV,CODSUC,CODCAR,SUMFIJ "+
                            ",JORNAD,CANHOR,FECINI,FECFIN,FECANT,FECTER,INDCOL,INDMOV,CODAFP,INDJUB,INDSAL,VALAHO,UNIAHO "+
                            ",CODISA,UNIISA,VALISA,ANTQUI,INDDEP,SERMED,CUOMOR,SINDI1,SINDI2,SINDI3,TIPJER,HORINI,HORFIN "+
                            ",RESOLU,FECRES,IMPCON,FECING,FECMOD,INGPOR,SERPR1,SERPR2,SEGCES,INDBON,MTIPO,MRAZON,MOTIVO "+
                            ",VALESUPER,CODBAN,SUCBAN,CUEBAN,MRETENCION,FECCON,FECPAS,UNIAUG,VALAUG,FECINT,NUMNEG,CODORG "+
                            ",TIPPUE,CODPUE,BANER1,BANER2,BANER3) "+
                      "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, "+
                             "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	try {
		CallableStatement sent = con.getConexion().prepareCall(sql);
		for (IseriesAppef05 contr : listCon) {
			sent.setInt(1,contr.getAnoPro());
			sent.setInt(2,contr.getMesPro());
			sent.setInt(3,contr.getRutide());
			sent.setString(4,contr.getDigide());
			sent.setInt(5,contr.getTipCon());
			sent.setInt(6,contr.getNumCon());
			sent.setInt(7,contr.getEscFon());
			sent.setInt(8,contr.getCodUni());
			sent.setInt(9,contr.getCodPla());
			sent.setInt(10,contr.getNumNiv());
			sent.setInt(11,contr.getCodSuc());
			sent.setInt(12,contr.getCodCar());
			sent.setInt(13,contr.getSumFij());
			sent.setInt(14,contr.getJorNad());
			sent.setInt(15,contr.getCanHor());
			sent.setInt(16,contr.getFecIni());
			sent.setInt(17,contr.getFecFin());
			sent.setInt(18,contr.getFecAnt());
			sent.setInt(19,contr.getFecTer());
			sent.setInt(20,contr.getIndCol());
			sent.setInt(21,contr.getIndMov());
			sent.setInt(22,contr.getCodAFP());
			sent.setInt(23,contr.getIndJub());
			sent.setInt(24,contr.getIndSal());
			sent.setInt(25,contr.getValAho());
			sent.setString(26,contr.getUniAho());
			sent.setInt(27,contr.getCodIsa());
			sent.setString(28,contr.getUniIsa());
			sent.setInt(29,contr.getValIsa());
			sent.setInt(30,contr.getAntQui());
			sent.setInt(31,contr.getIndDep());
			sent.setInt(32,contr.getSerMed());
			sent.setInt(33,contr.getCuoMor());
			sent.setInt(34,contr.getSindi1());
			sent.setInt(35,contr.getSindi2());
			sent.setInt(36,contr.getSindi3());
			sent.setString(37,contr.getTipJer());
			sent.setInt(38,contr.getHorIni());
			sent.setInt(39,contr.getHorFin());
			sent.setInt(40,contr.getResolu());
			sent.setInt(41,contr.getFecRes());
			sent.setString(42,contr.getImpCon());
			sent.setInt(43,contr.getFecIng());
			sent.setInt(44,contr.getFecMod());
			sent.setString(45,contr.getIngPor());
			sent.setString(46,contr.getSerPr1());
			sent.setString(47,contr.getSerPr2());
			sent.setInt(48,contr.getSegCes());
			sent.setInt(49,contr.getIndBon());
			sent.setInt(50,contr.getMTipo());
			sent.setInt(51,contr.getMRazon());
			sent.setString(52,contr.getMotivo());
			sent.setInt(53,contr.getValeSuper());
			sent.setInt(54,contr.getCodBan());
			sent.setInt(55,contr.getSucBan());
			sent.setString(56,contr.getCueBan());
			sent.setInt(57,contr.getMRetencion());
			sent.setInt(58,contr.getFecCon());
			sent.setInt(59,contr.getFecPas());
			sent.setString(60,contr.getUniAug());
			sent.setInt(61,contr.getValAug());
			sent.setInt(62,contr.getFecInt());
			sent.setInt(63,contr.getNumNeg());
			sent.setString(64,contr.getCodOrg());
			sent.setString(65,contr.getTipPue());
			sent.setString(66,contr.getCodPue());
			sent.setString(67,contr.getBaner1());
			sent.setString(68,contr.getBaner2());
			sent.setString(69,contr.getBaner3());
			sent.addBatch();
			
		}
		sent.executeBatch();
		sent.clearBatch();
		sent.close();
	} catch (SQLException e) {
		System.out.print("SQLERROR en : IsriesAppef05DAO.guardarListCon :\n" +e.getMessage());
	} catch (Exception e) {
		System.out.print("ERROR en : IsriesAppef05DAO.guardarListCon :\n"+e.getMessage());
	}
				
	}
	
}
