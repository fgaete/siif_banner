package cl.utfsm.rrhhG.datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cl.utfsm.conexion.ConexionAs400RRHHG;
import cl.utfsm.conexion.ConexionAs400RRHHI1;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhG.modulo.IseriesApref04;

public class IseriesApref04DAO {
	
	public static List<IseriesApref04> listCodHabDesc(String nomConec){
		
		List<IseriesApref04> list = new ArrayList<IseriesApref04>();
		Connection con ;
		if(nomConec.equals("GURFEED")){
			con = (new ConexionAs400RRHHG()).generaConnection();
			
		}else if (nomConec.equals("INTERFACE1")) {
			con = (new ConexionAs400RRHHI1()).generaConnection();
		}else{
			return null;
		}
		String sql = "select * from remunerabt.apref04";
	try {
		
		PreparedStatement sent = con.prepareStatement(sql);
		ResultSet rs = sent.executeQuery();
		while (rs.next()){
			IseriesApref04 codHabDesc = new IseriesApref04();
			codHabDesc.setCodHde(rs.getInt("CODHDE"));
			codHabDesc.setDesHde(rs.getString("DESHDE"));
			codHabDesc.setIndHde(rs.getString("INDHDE"));
			codHabDesc.setIndImp(rs.getString("INDIMP"));
			codHabDesc.setIndTri(rs.getString("INDTRI"));
			codHabDesc.setUniMed(rs.getString("UNIMED"));
			codHabDesc.setIndQui(rs.getString("INDQUI"));
			codHabDesc.setIndOrd(rs.getString("INDORD"));
			codHabDesc.setIndJor(rs.getString("INDJOR"));
			codHabDesc.setIndDia(rs.getString("INDDIA"));
			codHabDesc.setIndHex(rs.getString("INDHEX"));
			codHabDesc.setIndInv(rs.getString("INDINV"));
			codHabDesc.setHdeBan(rs.getString("HDEBAN"));
			list.add(codHabDesc);			
		}
		
		con.close();
	} catch (SQLException e) {
		System.out.print("SQLError IseriesApref04DAO.listCodHabDesc: "+e.getMessage());
	} catch (Exception e) {
		System.out.print("Error IseriesApref04DAO.listCodHabDesc: "+e.getMessage());
	}	
		return list;
	}
	
	public static void guardarListCodHades(List<IseriesApref04> listCodHades, ConexionBannerRRHH con){
		
		String sql = "INSERT INTO TMP_APREF04 (CODHDE,DESHDE,INDHDE,INDIMP,INDTRI,UNIMED,INDQUI,INDORD,INDJOR,INDDIA,INDHEX,INDINV,HDEBAN)" +
							"VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			PreparedStatement sent = con.getConexion().prepareStatement(sql);
			for (IseriesApref04 codHeDes : listCodHades) {
				sent.setInt(1,codHeDes.getCodHde());
				sent.setString(2,codHeDes.getDesHde());
				sent.setString(3,codHeDes.getIndHde());
				sent.setString(4,codHeDes.getIndImp());
				sent.setString(5,codHeDes.getIndTri());
				sent.setString(6,codHeDes.getUniMed());
				sent.setString(7,codHeDes.getIndQui());
				sent.setString(8,codHeDes.getIndOrd());
				sent.setString(9,codHeDes.getIndJor());
				sent.setString(10,codHeDes.getIndDia());
				sent.setString(11,codHeDes.getIndHex());
				sent.setString(12,codHeDes.getIndInv());
				sent.setString(13,codHeDes.getHdeBan());
				sent.addBatch();
			}
			sent.executeBatch();
			sent.clearBatch();
			sent.close();
		} catch (SQLException e) {
			System.out.print("SQLError IseriesApref04DAO.listCodHabDesc: "+e.getMessage());
		} catch (Exception e) {
			System.out.print("Error IseriesApref04DAO.listCodHabDesc: "+e.getMessage());
		}
	}
}
