package cl.utfsm.sib.mvc;
/**
 * SISTEMA DE BOLETAS DE HONORARIOS POR INTERNET
 * PROGRAMADOR		: 	M�NICA BARRERA FREZ
 * FECHA ULT.MODIF  : 	21/06/2012
 * UNIDAD DE DESARROLLO INSTITUCIONAL(DTI)  
 * migraci�n oracle : 15/06/2013 MB  
 */
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.tools.generic.DateTool;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.sib.modulo.ModuloHonorario;
import cl.utfsm.siv.modulo.ModuloValePago;
import descad.cliente.CocowBean;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw25DTO;



public class HonorarioInterceptor extends HandlerInterceptorAdapter{
	private ModuloHonorario moduloHonorario;
	private ModuloValePago moduloValePago;
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
		
	}
	public ModuloHonorario getModuloHonorario() {
		return moduloHonorario;
	}
	public void setModuloHonorario(ModuloHonorario moduloHonorario) {
		this.moduloHonorario = moduloHonorario;
	}
	

	public void cargarMenu(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
	  	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.getSesion().removeAttribute("autorizaProyecto");
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.getSesion().removeAttribute("autorizaUCP");
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.getSesion().removeAttribute("autorizaDIRPRE");
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.getSesion().removeAttribute("autorizaFinanzas");
      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
			accionweb.getSesion().removeAttribute("autorizaBoletas");
      	
	/* se debe buscar segun el usuario si tiene permiso para autorizar*/
	List<Presw18DTO> listaAutorizaciones = new ArrayList<Presw18DTO>();
	listaAutorizaciones = moduloHonorario.getConsultaAutoriza(rutUsuario, dv);
	if(listaAutorizaciones != null && listaAutorizaciones.size() > 0){
		 for (Presw18DTO ss : listaAutorizaciones){
			if(ss.getIndprc().trim().equals("Y")){
				accionweb.agregarObjeto("autorizaProyecto", 1);
				accionweb.getSesion().setAttribute("autorizaProyecto", 1);
			}
			if(ss.getIndprc().trim().equals("X")){
				accionweb.agregarObjeto("autorizaUCP", 1);
				accionweb.getSesion().setAttribute("autorizaUCP", 1);
			}
			if(ss.getIndprc().trim().equals("Z")){
				accionweb.agregarObjeto("autorizaDIRPRE", 1);
				accionweb.getSesion().setAttribute("autorizaDIRPRE", 1);
			}
			if(ss.getIndprc().trim().equals("F")){
				accionweb.agregarObjeto("autorizaFinanzas", 1);
				accionweb.getSesion().setAttribute("autorizaFinanzas", 1);
			}
			if(ss.getIndprc().trim().equals("B")){
				accionweb.agregarObjeto("autorizaBoletas", 1);
				accionweb.getSesion().setAttribute("autorizaBoletas", 1);
			}
		 }
	}
		if(rutOrigen > 0){
			this.limpiaSimulador(accionweb);
		}
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		if(listaSolicitudes != null && listaSolicitudes.size() > 0)
			accionweb.getSesion().removeAttribute("listaSolicitudes");
		
		accionweb.agregarObjeto("esBoleta", 1);
		accionweb.agregarObjeto("opcionMenu", tipo);
		
		accionweb.agregarObjeto("listaAutorizaciones", listaAutorizaciones);
	}
	public void limpiaSimulador(AccionWeb accionweb) throws Exception {
		if (accionweb.getSesion().getAttribute("listaUnidad") != null)
			accionweb.getSesion().setAttribute("listaUnidad", null);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutOrigen  = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen") +"","0"));
		String nomSimulacion = "";
		if(accionweb.getSesion().getAttribute("opcionMenu") != null)
			accionweb.getSesion().removeAttribute("opcionMenu");
		
		/*hacer este rut rutUsuario*/
		if(rutOrigen != 0) // lo vuelve a lo original
		{
			rutUsuario = rutOrigen ;
			rutOrigen = 0;
		}
		 
		
		accionweb.getSesion().setAttribute("rutUsuario", rutUsuario);
		accionweb.getSesion().setAttribute("rutOrigen", rutOrigen);
		accionweb.getSesion().setAttribute("nomSimulacion", nomSimulacion);
		accionweb.agregarObjeto("rutOrigen", rutOrigen);
		accionweb.agregarObjeto("nomSimulacion", nomSimulacion);
		
	}
	
	public void getRevisaBoleta(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		int identificador = Util.validaParametro(accionweb.getParameter("identificador"), 0);
		int numeroBoleta = Util.validaParametro(accionweb.getParameter("numeroBoleta"), 0);
		int opcionMenu = Util.validaParametro(accionweb.getParameter("opcionMenu"), 0);
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"), "");
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		if(listaSolicitudes != null && listaSolicitudes.size() > 0)
			accionweb.getSesion().removeAttribute("listaSolicitudes");
		List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudesPago");
		if(listaSolicitudesPago != null && listaSolicitudesPago.size() > 0)
			accionweb.getSesion().removeAttribute("listaSolicitudesPago");
	    int verificaBoleta = 0;
	    String mensaje = "";
		String nombre = "";
		String paisPrestador = "";
		String idSoli = "";
		Vector  v = new Vector();
		if(rutnum > 0){
			v = moduloHonorario.getVerificaBoleta(rutnum, dvrut,numeroBoleta,tipoBoleta);
			if(v!= null && v.size() > 0){
				verificaBoleta = Integer.parseInt(v.get(0).toString());
			if (verificaBoleta > 0){	
		    switch (verificaBoleta){
		    case 1:{mensaje = "El emisor es trabajador de la USM, debe comunicarse con Direcci�n de Finanzas, reenv�e correo de la boleta a bhesii@usm.cl.";
		    	break;
		    }
		    case 2:{mensaje = "La boleta ya existe en Sistema Administrativo Contable.";
	    	break;
	        }
		    case 3:{mensaje = "Boleta electr�nica no ha sido informada por SII.";
	    	break;
	        }
		    case 4:{mensaje = "El RUT asociado a la boleta est� objetado en Personal.";
	    	break;
	        }
		    case 5:{mensaje = "Ha sido ingresada previamente en el sistema en WEB.";
	    	break;
	        }
		    case 6:{mensaje = "Boleta anulada desde el SII.";
	    	break;
	        }
		    }
		    tipoBoleta = "";
		    numeroBoleta = 0;
			}else {// significa que es una boleta v�lida, 
		    	     // si es BOH ver si existe el 
		    	     // en caso de ser BHE se debe verificar si existe el prestador, si existe se muestran los datos,
		    	     // de lo contrario es necesario mostrar la pantalla que permite el ingreso del prestador
		    	    idSoli =  v.get(2).toString();
					Collection<Presw18DTO> lista = moduloHonorario.getVerificaRut(rutnum, dvrut);
					if(lista != null && lista.size() > 0){
						 for (Presw18DTO ss : lista){
							 nombre = ss.getDesuni();
							 paisPrestador = ss.getDesite();// pais prestador
							
						   }
					}
					
					accionweb.agregarObjeto("rut_aux", rut_aux);  
					accionweb.agregarObjeto("rutnum", rutnum); 
					accionweb.agregarObjeto("dvrut", dvrut); 
					
					if(!nombre.trim().equals("") || tipoBoleta.trim().equals("BHE")){
						accionweb.agregarObjeto("nomIdentificador", nombre);
						accionweb.agregarObjeto("paisIdentificador", nombre);						
						accionweb.agregarObjeto("numeroBoleta", numeroBoleta);
						List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
						if(accionweb.getSesion().getAttribute("listCocow36DTO") != null)
							accionweb.getSesion().removeAttribute("listCocow36DTO");
						
						CocowBean cocowBean = new CocowBean();
					/*	Presw19DTO preswbean19DTO = new Presw19DTO();
						preswbean19DTO.setTippro("DBH");
						preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
						preswbean19DTO.setDigide(dv);
						preswbean19DTO.setNumdoc(new BigDecimal(numeroBoleta));
						preswbean19DTO.setTipdoc(tipoBoleta);
						listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);*/
						listCocow36DTO = cocowBean.buscar_cocow36_id(idSoli);
				
						if(listCocow36DTO != null && listCocow36DTO.size() >0 )
							this.setdatosBoleta(accionweb,listCocow36DTO);
						if(tipoBoleta.trim().equals("BOH")){
							accionweb.agregarObjeto("hayDatosBoleta", 1);
						} else {// significa que es BHE		
							if(nombre.trim().equals("") ){
								accionweb.agregarObjeto("hayDatosBoleta", 3); // significa que se tiene que agregar los datos del prestador
								accionweb.agregarObjeto("idSoli", idSoli);
							}else
								accionweb.agregarObjeto("hayDatosBoleta", 2);				
						}
				
					} else
							accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado. Debe agregarlo.");
			
					List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
					listSede = moduloValePago.getListaSede(accionweb.getReq());
					String comboSede = "<select name=\"sede\" id=\"sede\" class=\"estilo_Azul\" onChange=\"\" >"+
		                       "<option value=\"-1\" selected=\"selected\">Seleccione</option> ";
					//  if(identificador == 1)
					for (Presw18DTO ss : listSede){
						comboSede += "<option value=" + ss.getNummes() + ">" + ss.getDesuni() + "</option>";
		    	
					}
					comboSede += "</select>";
		   
					List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
					moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
					listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
					List<Presw18DTO> listaTipoPago = new ArrayList<Presw18DTO>();
		     
					accionweb.agregarObjeto("comboSede", comboSede);
					accionweb.agregarObjeto("listSede", listSede);
					accionweb.agregarObjeto("identificador", identificador);
					accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);  
					accionweb.agregarObjeto("tipoBoleta", tipoBoleta);
		    
		    
		    }
		    }
		
			
		
			}
		  		  
	
		accionweb.agregarObjeto("esBoleta", 1);   
		accionweb.agregarObjeto("opcionMenu", 1);
		if(!mensaje.trim().equals(""))
			accionweb.agregarObjeto("mensaje", mensaje);
		else {
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
		}
	
		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
			accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
      	
      	
      	
	}
	 synchronized public void setdatosBoleta(AccionWeb accionweb,List<Cocow36DTO>  listCocow36DTO) throws Exception {
		if(listCocow36DTO.size() > 0){
		 for (Cocow36DTO ss : listCocow36DTO){
			if(ss.getNomcam().trim().equals("FECDOC")){
				   String fecha = ss.getValnu1()+"";
				   String diaFecha = "";
	        	   String mesFecha = "";
	        	   String annoFecha = "";
	        	   if(fecha.length() == 8){
	        		annoFecha = fecha.substring(0,4);
	        	    mesFecha = fecha.substring(4,6);
	        	    diaFecha = fecha.substring(6);
	        	   } 
	        	   String fechaBoleta = diaFecha + "/" + mesFecha + "/" + annoFecha;
				
				accionweb.agregarObjeto("fechaBoleta", fechaBoleta);
			}
			if(ss.getNomcam().trim().equals("TOTBOL"))
				accionweb.agregarObjeto("totalBoleta", ss.getValnu1());
			if(ss.getNomcam().trim().equals("RETBOL"))
				accionweb.agregarObjeto("retencion", ss.getValnu1());
			if(ss.getNomcam().trim().equals("VALPAG"))
				accionweb.agregarObjeto("totalAPago", ss.getValnu1());
			if(ss.getNomcam().trim().equals("DESCR1"))
				accionweb.agregarObjeto("glosa1", ss.getValalf());
			if(ss.getNomcam().trim().equals("DESCR2"))
				accionweb.agregarObjeto("glosa2", ss.getValalf());
			if(ss.getNomcam().trim().equals("DESCR3"))
				accionweb.agregarObjeto("glosa3", ss.getValalf());
			if(ss.getNomcam().trim().equals("NOMPRE"))
				accionweb.agregarObjeto("nombrePrestador", ss.getValalf());
			if(ss.getNomcam().trim().equals("DOMPRE"))
				accionweb.agregarObjeto("domicilioPrestador", ss.getValalf());
			if(ss.getNomcam().trim().equals("CIUPRE"))
				accionweb.agregarObjeto("ciudadPrestador", ss.getValalf());		
		
		   }
		 accionweb.getSesion().setAttribute("listCocow36DTO", listCocow36DTO);
		}
	}
	public void verificaRut(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		int identificador = Util.validaParametro(accionweb.getParameter("identificador"), 0);
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		if(listaSolicitudes != null && listaSolicitudes.size() > 0)
			accionweb.getSesion().removeAttribute("listaSolicitudes");
		List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudesPago");
		if(listaSolicitudesPago != null && listaSolicitudesPago.size() > 0)
			accionweb.getSesion().removeAttribute("listaSolicitudesPago");
	
		String nombre = "";
		if(rutnum > 0){
			Collection<Presw18DTO> lista = moduloHonorario.getVerificaRut(rutnum, dvrut);
			if(lista != null && lista.size() > 0){
				 for (Presw18DTO ss : lista){
					 nombre = ss.getDesuni();
				   }
			}
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
			
		}
	   	if(!nombre.trim().equals("")){
	    		accionweb.agregarObjeto("hayDatoslista", 1);
	    		accionweb.agregarObjeto("nomIdentificador", nombre);
	    } else
	    	accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado. Debe agregarlo.");
		
	    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
	    listSede = moduloValePago.getListaSede(accionweb.getReq());
	    String comboSede = "<select name=\"sede\" id=\"sede\" class=\"estilo_Azul\" onChange=\"\" >"+
	                       "<option value=\"-1\" selected=\"selected\">Seleccione</option> ";
	  //  if(identificador == 1)
	    for (Presw18DTO ss : listSede){
	    	comboSede += "<option value=" + ss.getNummes() + ">" + ss.getDesuni() + "</option>";
	    	
	    }
	    comboSede += "</select>";
	   
	    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
	    moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
	    List<Presw18DTO> listaTipoPago = new ArrayList<Presw18DTO>();
	     
  
	    
	    accionweb.agregarObjeto("comboSede", comboSede);
	    accionweb.agregarObjeto("listSede", listSede);
		accionweb.agregarObjeto("identificador", identificador);		  		  
		accionweb.agregarObjeto("esHonorario", 1);   
		
	
		accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);  
    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
			accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
      	
	}
	public void cargaIngresoHonorario(AccionWeb accionweb) throws Exception {
		verificaRut(accionweb);
		if(accionweb.getReq().getAttribute("mensaje") == null || accionweb.getReq().getAttribute("mensaje").toString().trim().equals(""))
			cargaAutorizaHonorario(accionweb);
		return;
	}
	 synchronized public void cargaAutorizaHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
	
		
		PreswBean preswbean = null;
		Collection<Presw18DTO> listaHonorario = null;
		String titulo = "";
		String tituloDetalle1 = "";
		String tituloDetalle2 = "";
		String tipcue = "";
		//System.out.println("rutUsuario: "+rutUsuario);
		//System.out.println("dv: "+dv);
		switch (tipo) {
		case  2:// lista de boletas a honorario para modificar
			{preswbean = new PreswBean("LBM",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			titulo = "Modificaci�n";
			break;
		}
		case 3: case 6: case 8: case 9: case 10: // lista de boletas a H para autorizar
			{String nomtipo = "";
			if(tipo == 3 || tipo == 6){
				titulo = "Autorizaci�n Responsable de la Cuenta";
				nomtipo = "LBA";
			}
			if(tipo == 8){
			   nomtipo = "LBE";
			   titulo = "Autorizaci�n MECESUP VRA";
			   tipcue = "Y";
			}
			if(tipo == 9){
				   nomtipo = "LBE";
				   titulo = "Autorizaci�n UCP";
				   tipcue = "X";
				}
			if(tipo == 10){
				   nomtipo = "LBE";
				   titulo = "Autorizaci�n MECESUP DIPRES";
				   tipcue = "Z";
				}
			preswbean = new PreswBean(nomtipo,0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			preswbean.setTipcue(tipcue);
			tituloDetalle1 = "Autorizar";
			tituloDetalle2 = "Rechazar";
			accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
			accionweb.agregarObjeto("opcion2", 6);
			
			break;
			}
		case 4: // lista de boletas a Honorarios para consulta
			{		
			String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
			int fechaInicio = Util.validaParametro(accionweb.getParameter("fechaInicio"),0);
			int fechaTermino = Util.validaParametro(accionweb.getParameter("fechaTermino"),0);
			String fechaIni = Util.validaParametro(accionweb.getParameter("fechaIni"),"");
			String fechaTer = Util.validaParametro(accionweb.getParameter("fechaTer"),"");
			int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0);
			if(fechaInicio > 0 && fechaTermino == 0 ){
				DateTool fechaActual = new DateTool();
				String dia = fechaActual.getDay()+"";
				if(dia.length() == 1)
					dia = "0" + dia;
				String mes = (fechaActual.getMonth()+1)+"";
				if(mes.length()== 1)
					mes = "0" + mes;
				fechaTermino = Integer.parseInt(String.valueOf(fechaActual.getYear()) + mes + dia);
				fechaTer = dia + "/" + mes + "/" + String.valueOf(fechaActual.getYear());
				
			}
			if(unidad >= 0){
			preswbean = new PreswBean("LBC",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			preswbean.setCoduni(unidad);
			preswbean.setTipcue(estado);
			preswbean.setFecmov(fechaInicio);
			preswbean.setNumcom(fechaTermino);
			listaHonorario = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			}
			titulo = "Consulta";	
			List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
			moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
			listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
			accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
		   	accionweb.agregarObjeto("estado", estado);
		   	accionweb.agregarObjeto("unidad", unidad);
		   	accionweb.agregarObjeto("fechaIni", fechaIni);
		   	accionweb.agregarObjeto("fechaTer", fechaTer);
			break;
			}
		case 5: case 7:// lista de boletas a Honorario para Recepci�n de vales en Finanzas
			{
			int sede = Util.validaParametro(accionweb.getParameter("sede"),0);	
			String estadoVales = Util.validaParametro(accionweb.getParameter("estadoVales"),"");
			preswbean = new PreswBean("LBF",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			preswbean.setSucur(sede);
			preswbean.setTipcue(estadoVales);
			titulo = "Recepci�n Finanzas";
			tituloDetalle1 = "Recepci�n";
			tituloDetalle2 = "Rechazar";
			accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
			accionweb.agregarObjeto("opcion2", 7);
			List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
			listSede = moduloValePago.getListaSede(accionweb.getReq());
			accionweb.agregarObjeto("listSede", listSede);
			accionweb.agregarObjeto("sede", sede);
			accionweb.agregarObjeto("estadoVales", estadoVales);
			break;
			}
		case 11: // lista de boletas a honorarios para consulta
		{		
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0);
		if(unidad >= 0){
		preswbean = new PreswBean("LBR",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
		preswbean.setCoduni(unidad);
		preswbean.setTipcue(estado);
		listaHonorario = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		}
		titulo = "Consulta";
		if(tipo == 11){
			titulo = "Existentes";
			int volver = Util.validaParametro(accionweb.getParameter("volver"),0);
			if(volver == 1) {
			int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"),0);
			String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
			String nomIdentificador = Util.validaParametro(accionweb.getParameter("nomIdentificador"),"");
			if(rutnum > 0 /*&& nomIdentificador.trim().equals("")*/)
				this.verificaRut(accionweb);
			accionweb.agregarObjeto("dvrut", dvrut);
			accionweb.agregarObjeto("rutnum", rutnum);
			accionweb.agregarObjeto("nomIdentificador", nomIdentificador);
			}
		}
		List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
		moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
		listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
	   	accionweb.agregarObjeto("estado", estado);
	   	accionweb.agregarObjeto("unidad", unidad);
		break;
		}	
		case  12:// lista de Ingresadores
		{	int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0);
		    String accion = Util.validaParametro(accionweb.getParameter("accion"),""); 
		    if(!accion.trim().equals("")){
		    	int rutide = Util.validaParametro(accionweb.getParameter("rutide"), 0);
		    	String digide = Util.validaParametro(accionweb.getParameter("digide"),""); 
		    	if(rutide > 0){
			    	try {
			    	boolean graba = moduloValePago.saveDeleteIngresador(rutide, digide, unidad, accion);
			    	if(graba)
			    		accionweb.agregarObjeto("mensaje", "registro exitoso.");
			    	else
			    		accionweb.agregarObjeto("mensaje", "problemas al registrar.");
			    	Thread.sleep(500);
			    	} catch (Exception e) {
						accionweb.agregarObjeto("mensaje", "No guardado saveDeleteIngresador.");
						accionweb.agregarObjeto("exception", e);
						e.printStackTrace();
					}
		    	} else accionweb.agregarObjeto("mensaje", "No guardado saveDeleteIngresador.");
		    }
		
			List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
			moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
			listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
			accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
		   	accionweb.agregarObjeto("unidad", unidad);
			if(unidad > 0){
		    	preswbean = new PreswBean("OLI",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
		    	preswbean.setCoduni(unidad);
		    	Collection<Presw25DTO> listaValePago25 = (Collection<Presw25DTO>) preswbean.consulta_presw25();
		    	accionweb.agregarObjeto("hayDatoslista", 1);
		    	accionweb.agregarObjeto("listaValePago25", listaValePago25);
		    	accionweb.agregarObjeto("hayDatoslista", "1");
			}
			titulo = "Ingresadores";
		break;
	}
		
		}
		if(tipo != 4 && tipo != 12)
			listaHonorario = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		
		
		//System.out.println("listaValePago.size(): "+listaValePago.size());
	    if(listaHonorario != null && listaHonorario.size() > 0)
        	accionweb.agregarObjeto("hayDatoslista", "1");
	    
	    
        accionweb.agregarObjeto("listaHonorario", listaHonorario);
      	accionweb.agregarObjeto("esBoleta", "1");
      	accionweb.agregarObjeto("opcion", String.valueOf(tipo));
    	accionweb.agregarObjeto("opcionMenu", tipo);
      	accionweb.agregarObjeto("titulo", titulo);
      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
      	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
			accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
      	
	}
	public void cargaResultadoSolicitudHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int cuentaPresup = Util.validaParametro(accionweb.getParameter("cuentaPresup"), 0);
		long valorAPagar = Util.validaParametro(accionweb.getParameter("valorAPagar"), 0);
		long valor = Util.validaParametro(accionweb.getParameter("valor"), 0);
		long total = 0;
	    

		
	    long saldoCuenta = moduloHonorario.getBuscaSaldo(cuentaPresup);
	    moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    int sobrepasa = 0;
	    sobrepasa = moduloHonorario.getAgregaListaSolicitaHonorario(accionweb.getReq(),saldoCuenta);
	    switch (sobrepasa){
	    case 1: {
	    	accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" >Este valor EXCEDE el saldo de la cuenta " + cuentaPresup + ", que es de: $ " + moduloHonorario.formateoNumeroSinDecimales(Double.parseDouble(saldoCuenta+""))+"</font> .");
	    	break;
	    }
	    case 2: {
	    	accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"destacado\" size=\"12px\">Lo sentimos, este valor no se puede agregar a la cuenta, ya que excede el valor del la Boleta, que es de: $ " + moduloHonorario.formateoNumeroSinDecimales(Double.parseDouble(valorAPagar+""))+"</font> .");
	    	break;
	    }
	    case 3: {
	    	accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"destacado\" size=\"12px\">Lo sentimos, esta cuenta ya se encuentra agregada </font> .");
	    	break;
	    }
	    }
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		if (listaSolicitudes != null && listaSolicitudes.size() > 0){
			accionweb.agregarObjeto("registra", 1);
		    accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes); 
		    for (Presw18DTO ss : listaSolicitudes)
				total = total + ss.getUsadom();
            accionweb.agregarObjeto("totalSolicitudHonorario", total);
		}
			
	   	accionweb.agregarObjeto("esBoleta", 1);   
		accionweb.agregarObjeto("identificador", 1);  
	 
	
		
	}
	 synchronized public void eliminaResultadoSolicitudHonorario(AccionWeb accionweb) throws Exception {
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
		   moduloHonorario.getEliminaListaSolicitaHonorario(accionweb.getReq());
		   	accionweb.agregarObjeto("mensaje", "Se elimin� el cargo a la cuenta.");
		    	
			List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		   	accionweb.agregarObjeto("esBoleta", 1);   
			accionweb.agregarObjeto("identificador", 1);  
			long total = 0;
			if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				accionweb.agregarObjeto("registra", 1);
			    accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes); 
			    for (Presw18DTO ss : listaSolicitudes)
					total = total + ss.getUsadom();
	            accionweb.agregarObjeto("totalSolicitudHonorario", total);
			}
			if(tipo > 0)
			 accionweb.agregarObjeto("opcion", tipo);
			
		}
	 synchronized public void registraHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
	
		String nomIdentificador = Util.validaParametro(accionweb.getParameter("nomIdentificador"), "");
		long rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		long numeroBoleta = Util.validaParametro(accionweb.getParameter("numeroBoleta"),0);
		long totalBoleta = Util.validaParametro(accionweb.getParameter("totalBoleta"),0);
		long retencion = Util.validaParametro(accionweb.getParameter("retencion"), 0);
		long totalAPago = Util.validaParametro(accionweb.getParameter("totalAPago"), 0); 
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"),"");	
		String fechaBoleta = Util.validaParametro(accionweb.getParameter("fechaBoleta"),"");
		String glosa1 = Util.validaParametro(accionweb.getParameter("glosa1"),"");	
		String glosa2 = Util.validaParametro(accionweb.getParameter("glosa2"),"");
		String glosa3 = Util.validaParametro(accionweb.getParameter("glosa3"),"");
		long sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
		
		
		
	    int numHonorario = 0;
	    
	    numHonorario = moduloHonorario.getRegistraHonorario(accionweb.getReq(),rutUsuario,dv);
	    if(numHonorario > 0){
	   	    accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa la Boleta de Honorario, el n�mero es: "+ numHonorario+", correspondiente al RUT: " + moduloHonorario.formateoNumeroSinDecimales(Double.parseDouble(rutnum+"")) + "-" + dvrut +".");
	   	    if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				accionweb.agregarObjeto("registra", 1);
			    accionweb.getSesion().removeAttribute("listaSolicitudes");
				}
	   	 cargarMenu(accionweb);
	    } else
	    	accionweb.agregarObjeto("mensaje", "No se registr� la Boleta de Honorario.");
		  	accionweb.agregarObjeto("esBoleta", 1);   
	 
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
				accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
	      	
	}
	public void consultaHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
		String tipdoc = Util.validaParametro(accionweb.getParameter("tipoBoleta"),"");
		long sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
		String estadoVales = Util.validaParametro(accionweb.getParameter("estadoVales"),"");
	
		String nomIdentificador = "";
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		long rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		//System.out.println("numDoc: "+numDoc);
		if(sede > 0)
			accionweb.agregarObjeto("sedeHonorario", sede);
		if(!estadoVales.trim().equals(""))
			accionweb.agregarObjeto("estadoVales", estadoVales);
		Presw19DTO preswbean19DTO = new Presw19DTO();
		String titulo = "";
		String tituloDetalle1 = "";
		preswbean19DTO.setTippro("DBH");
		preswbean19DTO.setRutide(new BigDecimal(rutnum));
		preswbean19DTO.setDigide(dvrut);
		preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
		preswbean19DTO.setTipdoc(tipdoc.trim());  // tipoBoleta

	//	preswbean19DTO.setIdsoli("9999999");
	
		String readonly = "readonly";
		

		
/*capturar los datos */
		
		if(tipo == 2) // lista de BH para consultar
		{	titulo = "Modifica ";
			tituloDetalle1 = "Modificar";
			if(tipdoc.trim().equals("BOH"))
				readonly = "";
			}
		if(tipo == 3) // lista de BH para autorizar
			{
			titulo = "Autorizaci�n Responsable de la Cuenta";
			tituloDetalle1 = "Autorizar";		 
			}
		if(tipo == 4) // lista de BH para consulta
			{
			int origenConsulta = Util.validaParametro(accionweb.getParameter("origenConsulta"), 0); // 1 viene de la consulta de boletas
			if(origenConsulta > 0)
				accionweb.agregarObjeto("origenConsulta", origenConsulta);
			titulo = "Consulta ";
			tituloDetalle1 = "Consultar";
			}
		if(tipo == 5) // lista de BH para Recepci�n de vales en Finanzas
			{
			titulo = "Recepci�n ";
			tituloDetalle1 = "Recepci�n";
			}
		if(tipo == 8) // lista de BH para autorizar
		{
		titulo = "Autorizaci�n MECESUP VRA";
		tituloDetalle1 = "Autorizar";		 
		}
		if(tipo == 9) // lista de BH para autorizar
		{
		titulo = "Autorizaci�n UCP";
		tituloDetalle1 = "Autorizar";		 
		}
		if(tipo == 10) // lista de BH para autorizar
		{
		titulo = "Autorizaci�n MECESUP DIPRES";
		tituloDetalle1 = "Autorizar";		 
		}
		if(tipo == 11) // lista de BH para copiar
		{	titulo = "Ingreso";
			tituloDetalle1 = "Ingreso";
			readonly = "";
			nomIdentificador = Util.validaParametro(accionweb.getParameter("nomIdentificador"),"");
	
		
			}
		List<Presw18DTO>  listaSolicitudes = new ArrayList<Presw18DTO>();
		List<Presw18DTO>  listaHistorial = new ArrayList<Presw18DTO>();
		long valorAPagar = 0;
		sede = 0;
		int identificador = 0; 
		
	    String nomcam = "";
	    long valnu1 = 0;
	    long valnu2 = 0;
	    String valalf = "";
	    String glosa1 = "";
	    String glosa2 = "";
	    String glosa3 = "";
	    String nomPrestador = "";
	    String estadoBoleta = "";
	    String codigoEstado = "";
	    String tipoBoleta = "";
	    int ind = 0;
	    long totalSolicitudHonorario = 0;
	    long fechaBoleta = 0;
	    long totalBoleta = 0;
	    long retencion = 0;
	    long totalAPago = 0;
	   
		List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
		CocowBean cocowBean = new CocowBean();
		//System.out.println("preswbean19DTO : "+ preswbean19DTO.getRutide()+"  - "+preswbean19DTO.getNumdoc()+"--- "+preswbean19DTO.getTippro()+"  --- "+preswbean19DTO.getFecmov());
		listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
		//System.out.println("listCocow36DTO : "+ listCocow36DTO.size() );
	    if(listCocow36DTO != null && listCocow36DTO.size() >0 ){
	    
	    for(Cocow36DTO ss: listCocow36DTO){
	    	if(ss.getNomcam().trim().equals("DESIDE"))
	    		nomIdentificador = ss.getValalf();
	    	if(ss.getNomcam().trim().equals("CODSUC"))
	    		sede = ss.getValnu1();	
	    	if(ss.getNomcam().trim().equals("FECDOC"))
	    		fechaBoleta = ss.getValnu1();
	    /*	if(ss.getNomcam().trim().equals("TIPDOC"))
	    		tipoBoleta = ss.getValalf();*/
	      	if(ss.getNomcam().trim().equals("TOTBOL"))
	    		totalBoleta = ss.getValnu1();
	      	if(ss.getNomcam().trim().equals("RETBOL"))
	    		retencion = ss.getValnu1();
	      	if(ss.getNomcam().trim().equals("VALPAG"))
	    		totalAPago = ss.getValnu1();
	    	if(ss.getNomcam().trim().equals("DESCR1"))
	    		glosa1 = ss.getValalf();
	    	if(ss.getNomcam().trim().equals("DESCR2"))
	    		glosa2 = ss.getValalf();
	     	if(ss.getNomcam().trim().equals("DESCR3"))
	    		glosa3 = ss.getValalf();	     
	     	 if(ss.getNomcam().trim().equals("ESTADO")){
           	   estadoBoleta = ss.getValalf();
           	   codigoEstado = ss.getResval();
	     	 }
	          if(ss.getNomcam().trim().equals("HISTORIAL")){
	            	Presw18DTO presw18DTO = new Presw18DTO();
	            	presw18DTO.setUsadom(ss.getValnu1()); // es la fecha, 
	            	presw18DTO.setUsadac(ss.getValnu2());//en caso de Resval="Autoriza" es la unidad
	            	presw18DTO.setDesite(ss.getValalf()); // es el responsable
	            	presw18DTO.setDesuni(ss.getResval()); // es la accion, INGRESO, MODIFICACION, AUTORIZACION, PAGO, AUTORIZA FINANZAS
	            	listaHistorial.add(presw18DTO);
	            }
            if(ss.getNomcam().trim().equals("RUTIDE") && tipo != 11)
             	   rutnum = ss.getValnu1();
            if(ss.getNomcam().trim().equals("DIGIDE") && tipo != 11)
            	dvrut = ss.getValalf();
          
            if(ss.getNomcam().trim().equals("CUENTA")){
            	Presw18DTO presw18DTO = new Presw18DTO();
            	presw18DTO.setCoduni(Integer.parseInt(ss.getValnu1()+""));
            	presw18DTO.setUsadom(ss.getValnu2());
               	presw18DTO.setDesuni(ss.getValalf());
               	presw18DTO.setDesite(ss.getResval());
            	listaSolicitudes.add(presw18DTO);
            	totalSolicitudHonorario += ss.getValnu2();
            }
     
     
	    //	 }
	    }	
	    } else {
	    	if(tipo == 4)
	    		accionweb.agregarObjeto("mensaje", "Boleta de Honorario no existe.");
	    }
	    String selected = (sede == 0)?"selected=\"selected\"":"";
	    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
	    listSede = moduloValePago.getListaSede(accionweb.getReq());
	
	   // System.out.println("listaTipoPago: "+listaTipoPago);
 	    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
 	    moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
	    //System.out.println("listaUnidades: "+listaUnidades);
	    if(listaSolicitudes != null && listaSolicitudes.size() > 0){
	    	accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes);
	    	accionweb.getSesion().setAttribute("listaSolicitudes", listaSolicitudes);
	    }
	    if(listaHistorial != null && listaHistorial.size() > 0){
	    	accionweb.agregarObjeto("listaHistorial", listaHistorial);
	       	accionweb.getSesion().setAttribute("listaHistorial", listaHistorial);
	    }
	
	    accionweb.agregarObjeto("listSede", listSede);
	    
	 
	    accionweb.agregarObjeto("rutnum",rutnum);
	    accionweb.agregarObjeto("dvrut",dvrut);
	    accionweb.agregarObjeto("unidad",unidad);
	
	    
	    accionweb.agregarObjeto("numVale",String.valueOf(numDoc));
	    if(rutnum == 0)
	    	 accionweb.agregarObjeto("identificador","1");
	    else
	    	accionweb.agregarObjeto("identificador","2");
	    accionweb.agregarObjeto("totalSolicitudHonorario", totalSolicitudHonorario);
	    accionweb.agregarObjeto("cuentasPresupuestarias",listaUnidades);
	    accionweb.agregarObjeto("nomIdentificador",nomIdentificador);
	    accionweb.agregarObjeto("sede", sede);
	    accionweb.agregarObjeto("valorAPagar", valorAPagar);
		accionweb.agregarObjeto("hayDatoslista", "1");     
      	accionweb.agregarObjeto("esBoleta", "1");
      	accionweb.agregarObjeto("opcion", tipo);
      	accionweb.agregarObjeto("titulo", titulo);
      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
    	accionweb.agregarObjeto("glosa1",glosa1);
    	accionweb.agregarObjeto("glosa2",glosa2);
    	accionweb.agregarObjeto("glosa3",glosa3);
      	accionweb.agregarObjeto("read",readonly);
    	accionweb.agregarObjeto("nomPrestador", nomPrestador);
      	accionweb.agregarObjeto("fechaBoleta", fechaBoleta);
      	accionweb.agregarObjeto("totalBoleta", totalBoleta);
      	accionweb.agregarObjeto("retencion", retencion);
      	accionweb.agregarObjeto("totalAPago", totalAPago);
      	accionweb.agregarObjeto("estadoBoleta", estadoBoleta);
      	accionweb.agregarObjeto("codigoEstado", codigoEstado);
    	accionweb.agregarObjeto("tipoBoleta", tipdoc);
    	accionweb.agregarObjeto("numeroBoleta", numDoc);
      	accionweb.agregarObjeto("numDoc", numDoc);
    	accionweb.agregarObjeto("estado", estadoBoleta);
       	
    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
			accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
      	
     // 	System.out.println("termina consulta ");	
	}
	 synchronized public void ejecutaAccion(AccionWeb accionweb) throws Exception {
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
	//	String estadoVales = Util.validaParametro(accionweb.getParameter("estadoVales"),"");
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
		
		
		switch (tipo){
		case 2: {
			this.actualizaHonorario(accionweb);
			break;
		}
		case 3: case 8: case 9: case 10: {
			this.autorizaHonorario(accionweb);
			break;
		}
		case 5:{
			this.recepcionHonorario(accionweb);
		break;
			
		}
		case 6:{// rechaza autorizaci�n
			this.rechazaAutHonorario(accionweb);
		break;
			
		}
		case 7:{// rechaza Recepci�n
			this.rechazaRecepHonorario(accionweb);
		break;
			
		}
		case 11:{// registra vale
			this.registraHonorario(accionweb);
		break;
			
		}
		}
	//	if(!estadoVales.trim().equals(""))
	//		accionweb.agregarObjeto("estadoVales", estadoVales);
		if(!estado.trim().equals(""))
			accionweb.agregarObjeto("estado", estado);
		if(tipo != 11)
			consultaHonorario(accionweb);
		
	}
	 synchronized public void actualizaHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numeroBoleta"), 0);
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
		String tipdoc = Util.validaParametro(accionweb.getParameter("tipoBoleta"),"");
		long sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
		
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");

	    boolean registra = false;
	    
	    registra = moduloHonorario.getActualizaHonorario(accionweb.getReq(),rutUsuario,dv,numdoc, tipdoc, rutnum, dvrut);
	    if(registra){
	   	    accionweb.agregarObjeto("mensaje", "Se actualiz� en forma exitosa la Boleta de Honorario, n�mero " +numdoc + " correspondiente al RUT: "+moduloHonorario.formateoNumeroSinDecimales(Double.parseDouble(rutnum+"")) + "-"+dvrut +".");
	   	    if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				accionweb.agregarObjeto("registra", 1);
			    accionweb.getSesion().removeAttribute("listaSolicitudes");
			    
			}
	   	// cargarMenu(accionweb);
	   	 consultaHonorario(accionweb);
	    } else
	    	accionweb.agregarObjeto("mensaje", "No se modific� la Boleta de Honorario.");
		  	accionweb.agregarObjeto("esBoleta", 1);   
	 
		 
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
				accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
	      	
	}
	 synchronized public void autorizaHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		//int numeroBoleta = Util.validaParametro(accionweb.getParameter("numeroBoleta"),0);
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"),"");	
	

		String nomTipo = "";
		String tipCue = "";
		String nomAut = "";
		switch(tipo){
		case 3: {
			nomTipo = "ACB";
			nomAut = " de Responsable de Cuenta. ";
			break;
		}
		case 8: {
			nomTipo = "ABE";
			tipCue = "Y";
			nomAut = " de Proyectos. ";
			break;
		}
		case 9: {
			nomTipo = "ABE";
			tipCue = "X";
			nomAut = " de UCP. ";
			break;
		}
		case 10: {
			nomTipo = "TFB";
			tipCue = "Z";
			nomAut = " de Director de presupuestos. ";
			break;
		}
		}
		if(rutUsuario > 0){
		try { 
			boolean graba = false;	
			if(tipo != 9 && tipo != 10)
				graba = moduloHonorario.saveAutoriza(numdoc, rutUsuario,dv,nomTipo, tipoBoleta, rutnum, dvrut);
			else
				graba = moduloHonorario.saveAutorizaEspecial(numdoc, rutUsuario, dv, nomTipo, tipoBoleta, rutnum, dvrut, tipCue);

	    if(graba)
	    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n" + nomAut);
	    else
	    	accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n" + nomAut);
	   	} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado saveAutoriza.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		} else 	accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n.(p�rdida de sesi�n)" );

	  	Thread.sleep(500);
	    cargaAutorizaHonorario(accionweb);
	    
		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
			accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
      	
	}
	 synchronized public void recepcionHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"), "");		
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);	
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
		int numeroBoleta = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		if(rutUsuario > 0 && !dv.trim().equals("")){
		try { 
			boolean graba = moduloHonorario.saveRecepciona(numeroBoleta, rutUsuario,dv,"TFB",tipoBoleta, rutnum, dvrut);
		    if(graba)
		    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la Recepci�n.");
		    else
		    	accionweb.agregarObjeto("mensaje", "No se Grab� la Recepci�n.");
	   	} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado saveRecepciona.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		} else accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n.(p�rdida de sesi�n)" );
		Thread.sleep(500);
	    cargaAutorizaHonorario(accionweb);
		
	}
	 synchronized public void recepcionaHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numeroBoleta = Util.validaParametro(accionweb.getParameter("numeroBoleta"), 0);
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"), "");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
		if(rutUsuario > 0 && !dv.trim().equals("")){
		try {	
		boolean graba = moduloHonorario.saveRecepciona(numeroBoleta, rutUsuario,dv,"TFB", tipoBoleta, rutnum, dvrut);
		
	    if(graba)
	    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la Recepci�n.");
	    else
	    	accionweb.agregarObjeto("mensaje", "No se Grab� la Recepci�n.");
	   	} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado saveRecepciona.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		} else accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n.(p�rdida de sesi�n)" );
		
		Thread.sleep(500);
	    cargaAutorizaHonorario(accionweb);
		
	}
	
	 synchronized public void rechazaAutHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numeroBoleta = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"), "");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
		String glosa1 = Util.validaParametro(accionweb.getParameter("glosa1"), "");
	    if(rutUsuario > 0 && !dv.trim().equals("")){
	    try {
			boolean graba = moduloHonorario.saveAutHonorario(numeroBoleta, rutUsuario,dv,"RBR",tipoBoleta, rutnum, dvrut, glosa1);
		    if(graba)
		    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el rechazo de la Autorizaci�n.");
		    else
		    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Autorizaci�n.");
		   	} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado rechazaAutHonorario.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
		} else accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n.(p�rdida de sesi�n)" );
		
	   	Thread.sleep(500);
	    cargaAutorizaHonorario(accionweb);
		
	}
	 synchronized public void rechazaRecepHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numeroBoleta = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"), "");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
		String glosa1 = Util.validaParametro(accionweb.getParameter("glosa1"), "");

		
		
		if(rutUsuario > 0 && !dv.trim().equals("")){
			try{ 
			boolean graba = moduloHonorario.saveRechazoDirFinanza(numeroBoleta, rutUsuario,dv,"RBF",tipoBoleta, rutnum, dvrut, glosa1);
		    if(graba)
		    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el Rechazo de la Recepci�n.");
		    else
		    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Recepci�n.");
		   	} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado rechazaRecepHonorario.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
		} else accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n.(p�rdida de sesi�n)" );
		
		Thread.sleep(500);
	    cargaAutorizaHonorario(accionweb);
		
	}
	 synchronized public void cargaRechazo(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numeroBoleta = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"), "");
			int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
			String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			String tipDoc = Util.validaParametro(accionweb.getParameter("tipDoc"), "");
		
		    accionweb.agregarObjeto("numDoc", numeroBoleta);
		    accionweb.agregarObjeto("tipoBoleta", tipoBoleta);
		    accionweb.agregarObjeto("rutnum", rutnum);
		    accionweb.agregarObjeto("dvrut", dvrut);
		    accionweb.agregarObjeto("tipo", tipo);
		    accionweb.agregarObjeto("tipDoc", tipDoc);
		}
	 
	public void imprimeHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numeroBoleta = Util.validaParametro(accionweb.getParameter("numeroBoleta"), 0);
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"),"");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
		
	
		long valorAPagar = 0;
		long sede = 0;
		int identificador = 0; 
		
	    String nomcam = "";
	    long valnu1 = 0;
	    long valnu2 = 0;
	    String valalf = "";
	    String glosa1 = "";
	    String glosa2 = "";
	    String glosa3 = "";

	    String estadoFinal = "";
	    String nomSede = "";
	    String estado = "";
	    String codigoEstado ="";
		
		Presw19DTO preswbean19DTO = new Presw19DTO();
		String titulo = "";
		String tituloDetalle1 = "";
		preswbean19DTO.setTippro("DBH");
		preswbean19DTO.setRutide(new BigDecimal(rutnum));
		preswbean19DTO.setDigide(dvrut);
		preswbean19DTO.setNumdoc(new BigDecimal(numeroBoleta));
		preswbean19DTO.setTipdoc(tipoBoleta);
		
		Vector vecSolicitudes = new Vector();	
		Vector vecHistorial = new Vector();	
		String nombrePrestador = "";
		long fechaBoleta = 0;
		long totalBoleta = 0;
		long retencion = 0;
		long totalAPago = 0;
	 	String fecha = "";
    	String diaFecha = "";
    	String mesFecha = "";
    	String annoFecha = "";
    	String valor = "";
    	String valor2 = "";
    	String nomTipoBoleta = "";
    	if(tipoBoleta.trim().equals("BOH"))
    		nomTipoBoleta = "Boleta de Honorario Manual";
    	else
    		nomTipoBoleta = "Boleta de Honorario Electr�nica";
    	String valorfecha = "";
		List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
		CocowBean cocowBean = new CocowBean();
		listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
		 if(listCocow36DTO != null && listCocow36DTO.size() >0 ) {
			    for(Cocow36DTO ss: listCocow36DTO){
			   //  if(ss.getTiping().trim().equals("VAP")){
			    	if(ss.getNomcam().trim().equals("CODSUC"))
			    		sede = ss.getValnu1();
			    	if(ss.getNomcam().trim().equals("DESIDE"))
			    		nombrePrestador = ss.getValalf();
			    	if(ss.getNomcam().trim().equals("FECDOC")){
			    		fechaBoleta = ss.getValnu1();
			    		diaFecha = String.valueOf(fechaBoleta).substring(6);
	    				mesFecha = String.valueOf(fechaBoleta).substring(4,6);
	    				annoFecha = String.valueOf(fechaBoleta).substring(0,4);
	    				valorfecha = diaFecha+"/"+mesFecha+"/"+annoFecha; 
			    	}
			    	if(ss.getNomcam().trim().equals("TOTBOL"))
			    		totalBoleta = ss.getValnu1();
			    	if(ss.getNomcam().trim().equals("RETBOL"))
			    		retencion = ss.getValnu1();
			    	if(ss.getNomcam().trim().equals("VALPAG"))
			    		totalAPago = ss.getValnu1();

			    	if(ss.getNomcam().trim().equals("DESCR1"))
			    		glosa1 = ss.getValalf();
			    	if(ss.getNomcam().trim().equals("DESCR2"))
			    		glosa2 = ss.getValalf();
			     	if(ss.getNomcam().trim().equals("DESCR3"))
			    		glosa3 = ss.getValalf();
			        if(ss.getNomcam().trim().equals("ESTADO")){
		            	estado = ss.getValalf();
		            	codigoEstado = ss.getResval();
			        }
		            if(ss.getNomcam().trim().equals("CUENTA")){
		            	Vector vec = new Vector();
		            	vec.addElement(ss.getValnu1());  // cuenta
		            	vec.addElement(ss.getValalf()); // unidad
		            	vec.addElement(ss.getValnu2()); // valor		            
		               	vec.addElement(ss.getResval()); // estado imputacion
		            	vecSolicitudes.addElement(vec);
		            	
		            }
		            if(ss.getNomcam().trim().equals("HISTORIAL")){
		                   	if(ss.getValnu1()> 0 ) {
		    				fecha = ss.getValnu1() + "";
		    				diaFecha = fecha.substring(6);
		    				mesFecha = fecha.substring(4,6);
		    				annoFecha = fecha.substring(0,4);
		    				valor = diaFecha+"/"+mesFecha+"/"+annoFecha;   
		            	} else {
		    				valor = ss.getValnu1()+"";
		            	} 
		            	if (ss.getResval().trim().equals("AUTORIZACION"))// se ocupa de la 1-15 el resto se ocupa la observacion del rechazo
		            	   valor +=  " por " + ss.getValalf() + " Cuenta " + ss.getValnu2() ;
		            	else
		            		valor += " por " + ss.getValalf();
		            	
		            	Vector vec3 = new Vector();
		            	vec3.addElement(ss.getResval()); // es la accion, INGRESO, MODIFICACION, AUTORIZACION, PAGO, AUTORIZA FINANZA				        
		            	vec3.addElement(valor); // es la fecha, 
		            	vec3.addElement(ss.getValalf()); // nombre usuario
		            	vec3.addElement(ss.getValnu2()); //nombre unidad
		            	vec3.addElement(ss.getResval()); // glosa operaci�n
		            	vecHistorial.addElement(vec3);
		            }
		           
			    // }
			    }	
			    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
			    listSede = moduloValePago.getListaSede(accionweb.getReq());
			    for( Presw18DTO ss:listSede){
			    	if(ss.getNummes() == sede)
			    		nomSede = ss.getDesuni();
			    }
			 
               accionweb.getSesion().setAttribute("vecSolicitudes", vecSolicitudes);
               accionweb.getSesion().setAttribute("vecHistorial", vecHistorial);
               accionweb.getSesion().setAttribute("sede", sede);
               accionweb.getSesion().setAttribute("nombrePrestador", nombrePrestador);
               accionweb.getSesion().setAttribute("fechaBoleta", valorfecha);
               accionweb.getSesion().setAttribute("totalBoleta", moduloHonorario.formateoNumeroSinDecimales(Double.parseDouble(totalBoleta+"")));
               accionweb.getSesion().setAttribute("retencion", moduloHonorario.formateoNumeroSinDecimales(Double.parseDouble(retencion+"")));
               accionweb.getSesion().setAttribute("totalAPago", moduloHonorario.formateoNumeroSinDecimales(Double.parseDouble(totalAPago+"")));
               accionweb.getSesion().setAttribute("glosa1", glosa1);
               accionweb.getSesion().setAttribute("glosa2", glosa2);
               accionweb.getSesion().setAttribute("glosa3", glosa3);
               accionweb.getSesion().setAttribute("nomTipoBoleta", nomTipoBoleta);
               if(rutnum > 0)
            	   accionweb.getSesion().setAttribute("rutnum", moduloHonorario.formateoNumeroSinDecimales(Double.parseDouble(rutnum+"")) + "-" + dvrut);
               accionweb.getSesion().setAttribute("nomSede", nomSede);
               accionweb.getSesion().setAttribute("numeroBoleta", String.valueOf(numeroBoleta));
               accionweb.getSesion().setAttribute("estadoFinal", estadoFinal);
               
		 }
	}
	 synchronized public void eliminaHonorario(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numeroBoleta"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"),"");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"),0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		
		String nomAut = "";
		switch(tipo){
		case 3:case 2: {
			// tipo == 2 es para los ingresadores
			if(tipo == 3)
				nomAut = " de Responsable de Cuenta. ";
			break;
		}
		case 8: {
			nomAut = " de Proyectos. ";
			break;
		}
		case 9: {
			nomAut = " de UCP. ";
			break;
		}
		case 10: {
			nomAut = " de Director de presupuestos. ";
			break;
		}
		}
		    boolean resultado = moduloHonorario.getEliminaHonorario(rutUsuario,dv,numdoc, tipoBoleta, rutnum, dvrut );
		    if(resultado)
		    	accionweb.agregarObjeto("mensaje", "Qued� anulada la Boleta de Honorario " + nomAut);
		    else
		    	accionweb.agregarObjeto("mensaje", "Problemas al anular la Boleta de Honorario" + nomAut);
			this.cargaAutorizaHonorario(accionweb);
			//accionweb.agregarObjeto("titulo","Modificaci�n");
			
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
				accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
	      	
		}
	
	public void limpiaSolicitudHonorario(AccionWeb accionweb) throws Exception {
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		listaSolicitudes = null;
		accionweb.getSesion().removeAttribute("listaSolicitudes");
		accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes);
	
	}
	public ModuloValePago getModuloValePago() {
		return moduloValePago;
	}
	public void setModuloValePago(ModuloValePago moduloValePago) {
		this.moduloValePago = moduloValePago;
	}
	
	 synchronized public void registraPrestador(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
	    int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
		String nomPrestador = Util.validaParametro(accionweb.getParameter("nomPrestador"), "");
		String direccion = Util.validaParametro(accionweb.getParameter("direccion"),"");
		String ciudad = Util.validaParametro(accionweb.getParameter("ciudad"),"");	
		String pais = Util.validaParametro(accionweb.getParameter("pais"),"");
		String tipoBoleta = Util.validaParametro(accionweb.getParameter("tipoBoleta"),"");
		int numeroBoleta = Util.validaParametro(accionweb.getParameter("numeroBoleta"), 0);
		String idSoli = Util.validaParametro(accionweb.getParameter("idSoli"),"");
		
		
		String nomP = moduloHonorario.eliminaAcentosString(nomPrestador);
		String dir  = moduloHonorario.eliminaAcentosString(direccion);
		String ciu = moduloHonorario.eliminaAcentosString(ciudad);
		/*segun petici�n se deben colocar todos los datos sin acento y con may�scula para ser registrado*/
		  
		List<Presw25DTO>  lista = new ArrayList<Presw25DTO>();
		Presw25DTO presw25 = new Presw25DTO();
		presw25.setTippro("PRS");
		presw25.setRutide(rutnum);
		presw25.setDigide(dvrut);
		presw25.setComen1(nomP.toUpperCase());
		presw25.setComen2(dir.toUpperCase());
		presw25.setComen3(ciu.toUpperCase());
		presw25.setComen4(pais.toUpperCase());		
		lista.add(presw25);
		if(rutUsuario > 0 && !dv.trim().equals("")){
		try{
		 	 boolean graba = moduloHonorario.savePrestador(lista,rutUsuario,dv);
		 	 if(graba){
		   	    accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa el Prestador de Servicios");
				accionweb.agregarObjeto("hayDatosBoleta", 2);
				accionweb.agregarObjeto("rutnum", 2);
				getRevisaBoleta(accionweb);
		    } else
		    	accionweb.agregarObjeto("mensaje", "No se registr� el Prestador de Servicios.");
		 	Thread.sleep(500);
		   	} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado savePrestador.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
		} else accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n.(p�rdida de sesi�n)" );
		
		  	accionweb.agregarObjeto("esBoleta", 1);   
	 
			if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	      	if(accionweb.getSesion().getAttribute("autorizaBoletas") != null)
				accionweb.agregarObjeto("autorizaBoletas", accionweb.getSesion().getAttribute("autorizaBoletas"));
	      	
	}
	public void cargaIngresoIngresador(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
		String nomUnidad = "";
		String titulo = "Nuevo Ingresador";
	
		 moduloHonorario.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
		 List<Presw18DTO>    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		 if(listaUnidades != null && listaUnidades.size() > 0){
			 for (Presw18DTO ss : listaUnidades){
				 if(ss.getCoduni() == unidad)
					 nomUnidad = ss.getDesuni();
			 }
		 }
		accionweb.agregarObjeto("unidad", unidad);
		accionweb.agregarObjeto("nomUnidad", nomUnidad);
		accionweb.agregarObjeto("esBoleta", 1);
      	accionweb.agregarObjeto("opcion", tipo);
    	accionweb.agregarObjeto("opcionMenu", tipo);
      	accionweb.agregarObjeto("titulo", titulo);
	}
	 synchronized public void recepcionaMasivo(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			String linea = Util.validaParametro(accionweb.getParameter("linea"),"");
			String lineaRut = Util.validaParametro(accionweb.getParameter("lineaRut"),"");
			String lineaDV = Util.validaParametro(accionweb.getParameter("lineaDV"),"");
			String lineaTipDoc = Util.validaParametro(accionweb.getParameter("lineaTipDoc"),"");
			
			if(rutUsuario > 0){ // significa que hay sesi�n ah� graba	
			int numeroBoleta = 0;
			int rutnum = 0;
			String dvrut = "";
			String tipoBoleta = "";
			try { 
				int index = 0;
				int indexRut = 0;
				int indexDV = 0;
				int indexTipDoc = 0;
				boolean graba = false;
				String mensaje = "";
				while(linea.length() > 0){
					index = linea.indexOf("@");
					indexRut = lineaRut.indexOf("@");
					indexDV = lineaDV.indexOf("@");
					indexTipDoc = lineaTipDoc.indexOf("@");
					if(index > 0){
						numeroBoleta = Integer.parseInt(linea.substring(0,index));
						rutnum =  Integer.parseInt(lineaRut.substring(0,indexRut));
						dvrut = lineaDV.substring(0,indexDV);
						tipoBoleta = lineaTipDoc.substring(0,indexTipDoc);
					}else{
						numeroBoleta = Integer.parseInt(linea);
						rutnum =  Integer.parseInt(lineaRut);
						dvrut = lineaDV;
						tipoBoleta = lineaTipDoc;
						lineaRut = "";
						linea = "";
					}
					graba = true;
					graba = moduloHonorario.saveRecepciona(numeroBoleta, rutUsuario,dv,"TFB", tipoBoleta, rutnum, dvrut);
					if(graba)
					    	mensaje += "Se grab&oacute; en forma exitosa la Recepci&oacute;n de la Boleta N " + numeroBoleta + " RUT: "+rutnum + "-"+dvrut+"Tipo Boleta: "+tipoBoleta+"<br>";
					    else
					    	mensaje += "No se Grab� la Recepci&oacute;n de la Boleta  N " + numeroBoleta + " RUT: "+rutnum + "-"+dvrut+"Tipo Boleta: "+tipoBoleta;
					 linea = linea.substring(index+1);
					 lineaRut = lineaRut.substring(indexRut+1);
					 lineaDV = lineaDV.substring(indexDV+1);
					 lineaTipDoc = lineaTipDoc.substring(indexTipDoc+1);
					
				}
			   	accionweb.agregarObjeto("mensaje", mensaje);
				Thread.sleep(500);
				} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado recepcionaMasivo.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
			} else 	accionweb.agregarObjeto("mensaje", "No guardado recepcionaMasivo.(P&eacute;rdida de sesi&oacute;n)");
			
			
		}
}
