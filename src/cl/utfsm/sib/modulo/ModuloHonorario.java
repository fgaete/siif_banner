package cl.utfsm.sib.modulo;
/**
 * SISTEMA DE BOLETAS DE HONORARIOS POR INTERNET
 * PROGRAMADOR		: 	M�NICA BARRERA FREZ
 * FECHA ULT.MODIF  : 	21/06/2012
 * UNIDAD DE DESARROLLO INSTITUCIONAL(DTI)   
 */
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.velocity.tools.generic.NumberTool;

import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.sib.datos.HonorarioDao;
import descad.cliente.CocofBean;
import descad.cliente.Ingreso_Documento;
import descad.cliente.MD5;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Cocof17DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw25DTO;

public class ModuloHonorario {
	HonorarioDao honorarioDao;

	public HonorarioDao getHonorarioDao() {
		return honorarioDao;
	}

	public void setHonorarioDao(HonorarioDao honorarioDao) {
		this.honorarioDao = honorarioDao;
	}
	
	 public List<Presw18DTO> getConsultaAutoriza(int rutnum, String dv){
		  	PreswBean preswbean = null;
			Collection<Presw18DTO> lista = null;
			List<Presw18DTO> listaAutoriza =  new ArrayList<Presw18DTO>();
			String nombre = "";
			if(rutnum > 0){
				preswbean = new PreswBean("PAU",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
				lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
				   for (Presw18DTO ss : lista){
					   Presw18DTO presw18DTO = new Presw18DTO();
					   presw18DTO.setIndprc(ss.getIndprc());
					   listaAutoriza.add(presw18DTO);
				   }
						
			}
		
		 return listaAutoriza;
		}
	 public Collection<Presw18DTO> getVerificaRut(int rutnum, String dv){
		  	PreswBean preswbean = null;
			Collection<Presw18DTO> lista = null;
			if(rutnum > 0){
				preswbean = new PreswBean("VRS",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
				lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			}
		
		 return lista;
		}
	 public Vector getVerificaBoleta(int rutnum, String dv, int numeroBoleta,String tipoBoleta) throws Exception{
		  	PreswBean preswbean = null;
			Collection<Presw18DTO> lista = null;
			Vector v = new Vector();
			int verificaBoleta = -1;
			if(rutnum > 0){
				preswbean = new PreswBean("VBH",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
				preswbean.setNumdoc(numeroBoleta);
				preswbean.setTipdoc(tipoBoleta);
				lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
				   for (Presw18DTO ss : lista){
					  v.addElement(ss.getNummes());
					  v.addElement(ss.getDesite());
					  v.addElement(ss.getIdsoli());
					
				   }
			}
		
		 return v;
		}
	 

		 public Collection<Presw18DTO> getConsulta(String tippro, int codUnidad, int item, int anno, int mes, int rutIde){
				PreswBean preswbean = new PreswBean(tippro, codUnidad, item, anno, mes, rutIde);
				Collection<Presw18DTO> listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
				return listaPresw18;
			 }
	
		 public long getBuscaSaldo(int codUni){
			  	PreswBean preswbean = null;
				Collection<Presw18DTO> lista = null;
				long saldoCuenta = 0;
				preswbean = new PreswBean("SCU",0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
				preswbean.setCoduni(codUni);
					lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
					   for (Presw18DTO ss : lista){
						   saldoCuenta = ss.getUsadom();
					   }	
			 return saldoCuenta;
			}
		 public int getAgregaListaSolicitaHonorario( HttpServletRequest req, long saldoCuenta){
			 HttpSession sesion = req.getSession();
		     long valor = Util.validaParametro(req.getParameter("valor"), 0);
		     long valorAPagar = Util.validaParametro(req.getParameter("valorAPagar"), 0);
		     int cuentaPresup = Util.validaParametro(req.getParameter("cuentaPresup"), 0);
		 	 String desuni = Util.validaParametro(req.getParameter("desuni"),"");
		    // String desuni = this.eliminaBlancosString(des);
		     
			 List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudes");
			 if(listaSolicitudes != null && listaSolicitudes.size()> 0)
				 sesion.removeAttribute("listaSolicitudes");
			 long total = 0;
			 long totalTotal = 0;
			 int sobrepasa = 0;
			 String muestraSaldo = "";
			 NumberTool numbertool = new NumberTool();
			 muestraSaldo = String.valueOf(numbertool.format(saldoCuenta));
			 if(listaSolicitudes != null && listaSolicitudes.size() > 0){
				 for (Presw18DTO ss: listaSolicitudes){
					 if(ss.getCoduni() == cuentaPresup)
						 sobrepasa = 3;
					 totalTotal += ss.getUsadom();
				 }
				 totalTotal += valor;
			 } else total = valor;
			 if(sobrepasa == 0) {
			 if(total > saldoCuenta){
				 sobrepasa = 1;
				 muestraSaldo = "<font class=\"Estilo_Rojo\" >" + muestraSaldo + "</font>";
			 }
			// else { lo dej� comentado, ya que la petici�n fue solo enviar mensaje en caso de que el saldo no alcance
				 if(totalTotal <= valorAPagar) {
				if(listaSolicitudes == null)
					listaSolicitudes = new ArrayList<Presw18DTO>();
				 Presw18DTO presw18DTO = new Presw18DTO();
				 presw18DTO.setCoduni(cuentaPresup);
				 presw18DTO.setUsadom(valor);
				 presw18DTO.setDesuni(desuni);
				 presw18DTO.setNompro(muestraSaldo);
				 listaSolicitudes.add(presw18DTO);
				 } else sobrepasa = 2;
			 //}
			 }
			 sesion.setAttribute("listaSolicitudes", listaSolicitudes);
			return sobrepasa;	
		 }
			public String formateoNumeroSinDecimales(Double numero){
				/*formatear n�mero */
			     Locale l = new Locale("es","CL");
			     DecimalFormat formatter1 = (DecimalFormat)DecimalFormat.getInstance(l);
			     formatter1.applyPattern("###,###,###");
			 
			   //  formatter1.format(rs.getDouble("RUTNUM"))   /*sin decimales*/
			 return formatter1.format(numero);
			}
public void getEliminaListaSolicitaHonorario( HttpServletRequest req){
				 HttpSession sesion = req.getSession();

				 long valor = Util.validaParametro(req.getParameter("usadom"), 0);
			     int cuentaPresup = Util.validaParametro(req.getParameter("coduni"), 0);
			     String desuni = Util.validaParametro(req.getParameter("desuni"),"");
			     
				 List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudes");
				 List<Presw18DTO> listaBoletas = new ArrayList<Presw18DTO>();
				 if(listaSolicitudes != null && listaSolicitudes.size()> 0)
					 sesion.removeAttribute("listaSolicitudes");
				 if(listaSolicitudes != null && listaSolicitudes.size() > 0){
					 for (Presw18DTO ss: listaSolicitudes){
						 if(ss.getCoduni() == cuentaPresup && ss.getUsadom() == valor && ss.getDesuni().trim().equals(desuni.trim()))
							 continue;
						 else
						 {
							 Presw18DTO presw18DTO = new Presw18DTO();
							 presw18DTO.setCoduni(ss.getCoduni());
							 presw18DTO.setUsadom(ss.getUsadom());
							 presw18DTO.setDesuni(ss.getDesuni());
							 presw18DTO.setDesite(ss.getDesite());
							 presw18DTO.setNompro(ss.getNompro());
							 listaBoletas.add(presw18DTO);
						 }
					 }
					 
				
				 }
				 if(listaBoletas.size() > 0)
				   sesion.setAttribute("listaSolicitudes", listaBoletas);
				 
					
			 }
public int getRegistraHonorario(HttpServletRequest req, int rutide, String digide){
	int numVale = 0;
	List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
	listCocow36DTO = getCreaCocow36(req, rutide, digide, "GBH");
	Ingreso_Documento ingresoDocumento = new Ingreso_Documento("GBH",rutide,digide);
		
	numVale = ingresoDocumento.Ingresar_Documento(listCocow36DTO);
	

	return numVale;
}
public boolean savePrestador(List<Presw25DTO> lista,int rutUsuario, String dv){
	PreswBean preswbean = new PreswBean("PRS", 0, 0, 0, 0, rutUsuario);
	preswbean.setDigide(dv);
	
	return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
} 

public List<Cocow36DTO> getCreaCocow36(HttpServletRequest req, int rutide, String digide, String tiping){
	List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) req.getSession().getAttribute("listaSolicitudes");
	String nomIdentificador = Util.validaParametro(req.getParameter("nomIdentificador"), "");
	long rutnum = Util.validaParametro(req.getParameter("rutnum"), 0);
	long numeroBoleta = Util.validaParametro(req.getParameter("numeroBoleta"),0);
	long totalBoleta = Util.validaParametro(req.getParameter("totalBoleta"),0);
	long retencion = Util.validaParametro(req.getParameter("retencion"), 0);
	long totalAPago = Util.validaParametro(req.getParameter("totalAPago"), 0); 
	String dvrut = Util.validaParametro(req.getParameter("dvrut"),"");
	String tipoBoleta = Util.validaParametro(req.getParameter("tipoBoleta"),"");	
	String fechaBoleta = Util.validaParametro(req.getParameter("fechaBoleta"),"");
	String gl1 = Util.validaParametro(req.getParameter("glosa1"),"");	
	String gl2 = Util.validaParametro(req.getParameter("glosa2"),"");
	String gl3 = Util.validaParametro(req.getParameter("glosa3"),"");
	String glo1 = this.eliminaAcentosString(gl1.trim());
	String glo2 = this.eliminaAcentosString(gl2.trim());
	String glo3 = this.eliminaAcentosString(gl3.trim());
	String glosa1 = this.eliminaBlancosString(glo1.trim());
	String glosa2 = this.eliminaBlancosString(glo2.trim());
	String glosa3 = this.eliminaBlancosString(glo3.trim());

	long sede = Util.validaParametro(req.getParameter("sede"), 0);
	List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
	
   // String tiping = "GBH";
    String nomcam = "";
    long valnu1 = 0;
    long valnu2 = 0;
    String valalf = "";


/* prepara archivo para grabar, llenar listCocow36DTO*/
    nomcam = "CODSUC";
    valnu1 = sede;
    valnu2 = 0;
    valalf = "";
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );    

	nomcam = "RUTIDE";
	valnu1 = rutnum;
	valnu2 = 0;
	valalf = "";
	listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

	nomcam = "DIGIDE";
	valnu1 = 0;
	valnu2 = 0;
	valalf = dvrut;
	listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

	nomcam = "NUMDOC";
	valnu1 = numeroBoleta;
	valnu2 = 0;
	valalf = "";
	listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

	nomcam = "TIPDOC";
	valnu1 = 0;
	valnu2 = 0;
	valalf = (tipoBoleta.length()>80)?tipoBoleta.substring(0,79):tipoBoleta;
	listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

	int fecha = Integer.parseInt(fechaBoleta.substring(6)+fechaBoleta.substring(3,5)+fechaBoleta.substring(0,2));
	nomcam = "FECDOC";
	valnu1 = fecha;
	valnu2 = 0;
	valalf = "";
	listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

	nomcam = "TOTBOL";
	valnu1 = totalBoleta;
	valnu2 = 0;
	valalf = "";
	listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	
	nomcam = "RETBOL";
	valnu1 = retencion;
	valnu2 = 0;
	valalf = "";
	listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

	nomcam = "VALPAG";
	valnu1 = totalAPago;
	valnu2 = 0;
	valalf = "";
	listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	
	nomcam = "DESCR1";
	valnu1 = 0;
	valnu2 = 0;
	valalf = (glosa1.length()>80)?glosa1.substring(0,79):glosa1;
	listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	
	nomcam = "DESCR2";
	valnu1 = 0;
	valnu2 = 0;
	valalf = (glosa2.length()>80)?glosa2.substring(0,79):glosa2;
	listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	
	nomcam = "DESCR3";
	valnu1 = 0;
	valnu2 = 0;
	valalf = (glosa3.length()>80)?glosa3.substring(0,79):glosa3;
	listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	
	
	/* 25/05/2012 agregar MD5*/
	nomcam = "CODMD5";
	MD5 md5 = new MD5();
	String texto =md5.getMD5(tipoBoleta + numeroBoleta + rutnum + dvrut.trim() + nomIdentificador.trim() + totalAPago) ;
	valnu1 = 0;
	valnu2 = 0;
	valalf = (texto.length() > 80)?texto.substring(0, 79):texto;
	listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

for (Presw18DTO ss : listaSolicitudes){
	nomcam = "CUENTA";
	    valnu1 = ss.getCoduni();
	    valnu2 = ss.getUsadom();
	    valalf = "";
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
}
	

return listCocow36DTO;
}
public List<Cocow36DTO> getAgregaLista(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf){
	 Cocow36DTO cocow36DTO = new Cocow36DTO();
	 cocow36DTO.setTiping(tiping);
	 cocow36DTO.setNomcam(nomcam);
	 cocow36DTO.setValnu1(valnu1);
	 cocow36DTO.setValnu2(valnu2);
	 cocow36DTO.setValalf(valalf);
	 lista.add(cocow36DTO);
	 return lista;
	 
}
public List<Cocow36DTO> getAgregaListaMD5(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf, String resval){
	 Cocow36DTO cocow36DTO = new Cocow36DTO();
	 cocow36DTO.setTiping(tiping);
	 cocow36DTO.setNomcam(nomcam);
	 cocow36DTO.setValnu1(valnu1);
	 cocow36DTO.setValnu2(valnu2);
	 cocow36DTO.setValalf(valalf);
	 cocow36DTO.setResval(resval);
	 lista.add(cocow36DTO);
	 return lista;
	 
}
public boolean getActualizaHonorario(HttpServletRequest req, int rutide, String digide, int numDoc,String tipdoc, int rutnum, String dvrut){
	boolean registra = false;
	List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
	listCocow36DTO = getCreaCocow36(req, rutide, digide, "MBH" );
	Ingreso_Documento ingresoDocumento = new Ingreso_Documento("MBH",rutide,digide);
	ingresoDocumento.setNumdoc(numDoc);
	ingresoDocumento.setTipdoc(tipdoc);
	ingresoDocumento.setNumcom(rutnum);
	ingresoDocumento.setTipcue(dvrut);
	
		
	registra = ingresoDocumento.Actualizar_Documento(listCocow36DTO);
	

	return registra;
}
public boolean saveAutoriza(int numdoc, int rutUsuario, String digide, String nomTipo, String tipdoc, int rutnum, String dvrut){
	PreswBean preswbean = new PreswBean(nomTipo, 0,0,0,0, rutUsuario);
	preswbean.setDigide(digide);
	preswbean.setNumdoc(numdoc);
	preswbean.setTipdoc(tipdoc);
	preswbean.setNumcom(rutnum);
	preswbean.setTipcue(dvrut);
return preswbean.ingreso_presw19();
}
public boolean saveAutorizaEspecial(int numdoc, int rutUsuario, String digide, String nomTipo, String tipdoc, int rutnum, String dvrut,  String cajero){
	PreswBean preswbean = new PreswBean(nomTipo, 0,0,0,0, rutUsuario);
	preswbean.setDigide(digide);
	preswbean.setNumdoc(numdoc);
	preswbean.setTipdoc(tipdoc);
	preswbean.setNumcom(rutnum);
	preswbean.setTipcue(dvrut);
	preswbean.setCajero(cajero);
return preswbean.ingreso_presw19();
}
public boolean saveRecepciona(int numeroBoleta, int rutUsuario, String digide, String tipo, String tipoBoleta, int rutnum, String dvrut){
	PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
	preswbean.setDigide(digide);
	preswbean.setNumdoc(numeroBoleta);
	preswbean.setTipdoc(tipoBoleta);
	preswbean.setNumcom(rutnum);
	preswbean.setTipcue(dvrut);

	
return preswbean.ingreso_presw19();
}
public boolean saveAutHonorario(int numeroBoleta, int rutUsuario, String digide, String tipo, String tipoBoleta, int rutnum, String dvrut, String glosa1){
	PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
	preswbean.setDigide(digide);
	preswbean.setNumdoc(numeroBoleta);
	preswbean.setTipdoc(tipoBoleta);
	preswbean.setNumcom(rutnum);
	preswbean.setTipcue(dvrut);
	List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
	//if(preswbean.ingreso_presw19()){
		
		Presw25DTO presw25DTO = new Presw25DTO();
		presw25DTO.setPres01(numeroBoleta);
		presw25DTO.setComen1(glosa1);
		presw25DTO.setRutide(rutnum);
		presw25DTO.setDigide(dvrut);
		lista.add(presw25DTO);
	
	//}
	return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
}
public boolean saveRechazoDirFinanza(int numeroBoleta, int rutUsuario, String digide, String tipo, String tipoBoleta, int rutnum, String dvrut, String glosa1){
	PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
	preswbean.setDigide(digide);
	preswbean.setNumdoc(numeroBoleta);
	preswbean.setTipdoc(tipoBoleta);
	preswbean.setNumcom(rutnum);
	preswbean.setTipcue(dvrut);
	List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
	//if(preswbean.ingreso_presw19()){
		
		Presw25DTO presw25DTO = new Presw25DTO();
		presw25DTO.setPres01(rutnum);
		presw25DTO.setIndexi(dvrut);
		presw25DTO.setCodigo(tipoBoleta);
		presw25DTO.setPres02(numeroBoleta);		
		presw25DTO.setComen1(glosa1);
		presw25DTO.setRutide(rutUsuario);
		presw25DTO.setDigide(digide);
		lista.add(presw25DTO);
	
	//}
	return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
}
public boolean getEliminaHonorario( int rut, String dv, int numdoc, String tipDoc, int numCom , String tipCue ){
  	PreswBean preswbean = null;
	Collection<Presw18DTO> lista = null;
	long saldoCuenta = 0;
	preswbean = new PreswBean("ABH",0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
	preswbean.setRutide(rut);
	preswbean.setDigide(dv);
	preswbean.setNumdoc(numdoc);
	preswbean.setTipdoc(tipDoc);
	preswbean.setNumcom(numCom);
	preswbean.setTipcue(tipCue);
	
	return preswbean.ingreso_presw19();	
	
} 


public String eliminaAcentosString(String sTexto){
	/*elimina acentos del string*/
   String linea = "";  
    if(!sTexto.trim().equals("")){
    for (int x=0; x < sTexto.length(); x++) {
    	  if ((sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') || 
    	      (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') || 
    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�')){
    		  if(sTexto.charAt(x) == '�' )
    			  linea += 'a';
    		  if(sTexto.charAt(x) == '�' )
    			  linea += 'A';
    		  if(sTexto.charAt(x) == '�' )
    			  linea += 'e';
    		  if(sTexto.charAt(x) == '�' )
    			  linea += 'E';
    		  if(sTexto.charAt(x) == '�' )
    			  linea += 'i';
    		  if(sTexto.charAt(x) == '�' )
    			  linea += 'I';
    		  if(sTexto.charAt(x) == '�' )
    			  linea += 'o';
    		  if(sTexto.charAt(x) == '�' )
    			  linea += 'O';
    		   if(sTexto.charAt(x) == '�' )
    			  linea += 'u';
    		  if(sTexto.charAt(x) == '�' )
    			  linea += 'U';
    	  }
    	  else  linea += sTexto.charAt(x);
    	}				
    }

 return linea;
}
public void getCuentasAutorizadas( HttpServletRequest req, int rutide){
	 HttpSession sesion = req.getSession();
	 Collection<Presw18DTO> listaPresw18 = null;
	 
	 listaPresw18 = this.getConsulta("VCA", 0, 0, 0, 0, rutide);
	 List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
		
	 for (Presw18DTO ss : listaPresw18){
		 Presw18DTO cuentapresupuestaria = new Presw18DTO();
		 cuentapresupuestaria.setCoduni(ss.getCoduni());
		 cuentapresupuestaria.setNomtip(ss.getNomtip());
		 cuentapresupuestaria.setDesuni(ss.getDesuni());
		 cuentapresupuestaria.setIndprc(ss.getIndprc());
		 lista.add(cuentapresupuestaria);
	 }
	 
	 
	 sesion.setAttribute("cuentasPresupuestarias", lista);
	return ;
	 }
public Collection<Presw25DTO> getVerificaRutHonorario(int rutnum, String dv){
  	PreswBean preswbean = null;
	Collection<Presw25DTO> lista = null;
	if(rutnum > 0){
		preswbean = new PreswBean("VCL",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
		lista = (Collection<Presw25DTO>) preswbean.consulta_presw25();
			}

 return lista;
}
public String eliminaBlancosString(String sTexto){
	/*elimina blancos entre medio*/
   String linea = "";
    
    
    for (int x=0; x < sTexto.length(); x++) {
    	  if ((sTexto.charAt(x) != ' ' || 
    		  (x < sTexto.length() && sTexto.charAt(x) == ' '  && sTexto.charAt(x+1) != ' ')) &&
    		  sTexto.charAt(x) != '\'' &&
    		  sTexto.charAt(x) != '\"')
    		  linea += sTexto.charAt(x);
    	}				

 return linea;
}
}
