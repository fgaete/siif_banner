package cl.utfsm.srgB.mvc;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.utfsm.POJO.Sede;
import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.sivB.modulo.ModuloValePagoB;
import cl.utfsm.srgB.modulo.ModuloRecuperacionGastoB;
import descad.cliente.CocowBean;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.ItemDTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;

public class RecuperacionGastoInterceptorB extends HandlerInterceptorAdapter{
	private ModuloRecuperacionGastoB moduloRecuperacionGastoB;
	private ModuloValePagoB moduloValePagoB;
	
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		
	}	
	public void cargarRecuperacion(AccionWeb accionweb) throws Exception {
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
      	accionweb.agregarObjeto("control", control);
      	if(accionweb.getSesion().getAttribute("listaSolicitudDistribucion") != null)
      		accionweb.getSesion().setAttribute("listaSolicitudDistribucion", null);
  		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
   		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
   		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
   	  	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
          		accionweb.getSesion().removeAttribute("autorizaProyecto");
          	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
          		accionweb.getSesion().removeAttribute("autorizaUCP");
          	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
          		accionweb.getSesion().removeAttribute("autorizaDIRPRE");
          	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
    			accionweb.getSesion().removeAttribute("autorizaFinanzas");
         	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
    			accionweb.getSesion().removeAttribute("autorizaDAF");
    	/* se debe buscar segun el usuario si tiene permiso para autorizar*/
    	List<Presw18DTO> listaAutorizaciones = new ArrayList<Presw18DTO>();
    	listaAutorizaciones = moduloValePagoB.getConsultaAutoriza(rutUsuario, dv);
    	if(listaAutorizaciones != null && listaAutorizaciones.size() > 0){
    		 for (Presw18DTO ss : listaAutorizaciones){
    			if(ss.getIndprc().trim().equals("Y")){
    				accionweb.agregarObjeto("autorizaProyecto", 1);
    				accionweb.getSesion().setAttribute("autorizaProyecto", 1);
    			}
    			if(ss.getIndprc().trim().equals("X")){
    				accionweb.agregarObjeto("autorizaUCP", 1);
    				accionweb.getSesion().setAttribute("autorizaUCP", 1);
    			}
    			if(ss.getIndprc().trim().equals("Z")){
    				accionweb.agregarObjeto("autorizaDIRPRE", 1);
    				accionweb.getSesion().setAttribute("autorizaDIRPRE", 1);
    			}
    			if(ss.getIndprc().trim().equals("F")){
    				accionweb.agregarObjeto("autorizaFinanzas", 1);
    				accionweb.getSesion().setAttribute("autorizaFinanzas", 1);
    			}
    			if(ss.getIndprc().trim().equals("D")){
    				accionweb.agregarObjeto("autorizaDAF", 1);
    				accionweb.getSesion().setAttribute("autorizaDAF", 1);
    			}
    		 }
    	}
    		if(rutOrigen > 0){
    			this.limpiaSimulador(accionweb);
    		}
    	 		    			
    	accionweb.agregarObjeto("listaAutorizaciones", listaAutorizaciones);
    	accionweb.agregarObjeto("esRecuperacionGastoB", 1); // es para mostrar el menu  de Recuperacion
	}
	
	public void verificaRut(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		int identificador = Util.validaParametro(accionweb.getParameter("identificador"), 0);
		if(accionweb.getSesion().getAttribute("listaSolicitudDistribucion") != null)
      		accionweb.getSesion().setAttribute("listaSolicitudDistribucion", null);
		if(accionweb.getSesion().getAttribute("listaSolicitudDocumento") != null)
			accionweb.getSesion().setAttribute("listaSolicitudDocumento", null);
		if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		
		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
			accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
      		accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));

		
		String nombre = "";
		if(rutnum > 0 && String.valueOf(rutnum).length() <= 8){
			Collection<Presw18DTO> lista = moduloRecuperacionGastoB.getVerificaRut(rutnum+dvrut);// VS.03.01.2017(rutnum, dvrut,"VRF");
			if(lista != null && lista.size() > 0){
				 for (Presw18DTO ss : lista){
					 nombre = ss.getDesuni();
				   }
			}
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
			
		}  
			
	   	if(!nombre.trim().equals("")){
	    		accionweb.agregarObjeto("hayDatoslista", 1);
	    		accionweb.agregarObjeto("nomIdentificador", nombre);
	    } else{
	    	if(String.valueOf(rutnum).length() > 8)
	    		accionweb.agregarObjeto("mensaje", "Este RUT est� inv�lido.");
	    	else
	    		//accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado. Debe agregarlo.");//VS.21.12.2016.
	    		accionweb.agregarObjeto("mensaje", "Lo sentimos, este RUT no se encuentra registrado, debe solicitar a FINANZAS su creaci�n.");
	    }
	   	//VS.21.12.2016. sede no se usa
	    //List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
	    //listSede = moduloValePagoB.getListaSede(accionweb.getReq());
	    //String comboSede = "<select name=\"sede\" id=\"sede\" class=\"estilo_Azul\" onChange=\"\" >"+
	    //                   "<option value=\"-1\" selected=\"selected\">Seleccione</option> ";
	    //  if(identificador == 1)
	    //for (Presw18DTO ss : listSede){
	    //	comboSede += "<option value=" + ss.getNummes() + ">" + ss.getDesuni() + "</option>";
	    //	
	   // }
	    //comboSede += "</select>";
	   
	    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
	    moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
	    accionweb.agregarObjeto("listaUnidades", listaUnidades);
	    
	    List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
	    listaTipoDocumento = moduloRecuperacionGastoB.getTipoDocumento();
	    accionweb.agregarObjeto("listaTipoDocumento", listaTipoDocumento);
	    
		/*Item items = new Item();
		Collection<ItemDTO> lista = (Collection<ItemDTO>) items.todos_items();
		List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
		for(ItemDTO pp: lista){
			//System.out.println("incren: "+ pp.getIncren()+" "+ pp.getNomite()+"  "+ pp.getItepre());
			if(pp.getIncren().trim().equals("S"))
				listaItem.add(pp);
			
		}*/
	//	este acceso cambia a partir de 27/01/2015 pr instrucciones de correo PP accediendo a partir de ahora a la tabla cocof139 en lugar de cocof23
		List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
		listaItem = moduloRecuperacionGastoB.getItemCocof139B();
		accionweb.agregarObjeto("listaItem", listaItem);
	  
		DateTool fechaActual = new DateTool();
		String fecha = "";
		String dia = fechaActual.getDay()+"";
		if(dia.length() == 1)
			dia = "0" + dia;
		String mes = (fechaActual.getMonth()+1)+"";
		if(mes.length()== 1)
			mes = "0" + mes;
		fecha = dia + "/" + mes + "/" + String.valueOf(fechaActual.getYear());
  
	    accionweb.agregarObjeto("fecha", fecha);
	    accionweb.agregarObjeto("anioActual", String.valueOf(fechaActual.getYear()));
	    //accionweb.agregarObjeto("comboSede", comboSede); VS.21.12.2016
	    //accionweb.agregarObjeto("listSede", listSede); VS.21.12.2016
		accionweb.agregarObjeto("identificador", identificador);		  		  
		accionweb.agregarObjeto("esRecuperacionGastoB", 1);   
		
	}
	public void verificaRutDocumento(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		String tipoDocumento = Util.validaParametro(accionweb.getParameter("tipoDocumento"),"");
		
		String nombre = "";
		String indproc = "";
		String indHay = "";
		if(rutnum > 0 && String.valueOf(rutnum).length() <= 8){
			Collection<Presw18DTO> lista = moduloRecuperacionGastoB.getVerificaRutDocumento(rutnum+dvrut); //VS.03.01.2017 (rutnum, dvrut, "VRD");
			if(lista != null && lista.size() > 0){
				 for (Presw18DTO ss : lista){
					 nombre = ss.getDesuni();
					 indproc = ss.getIndprc();
					 indHay = ss.getIndhay();					 
				   }
			}
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
			
		}
		if(indproc.trim().equals("1") || String.valueOf(rutnum).length() > 8) {
			accionweb.agregarObjeto("mensaje", "Este RUT es inv�lido.");
		    accionweb.agregarObjeto("indHay", "N");
		}   
		else {
			if(!nombre.trim().equals("")){	    		
	    		accionweb.agregarObjeto("nomRut", nombre);
	    		if(indHay.trim().equals("P"))
	    			accionweb.agregarObjeto("mensaje", "Proveedor no se encuentra creado en Banner. Favor enviar correo a proyecto.ERP@usm.cl con la siguiente informaci�n: Rut, Nombre Proveedor, Direcci�n,Tel�fono.");


	    		else accionweb.agregarObjeto("mensaje", nombre);
	    		
	    		accionweb.agregarObjeto("indHay", indHay.trim());
	    		/* VS.03.01.2017. no existe validaciones ya que solo valida que el rut este en spriden
	    		 if(indHay.trim().equals("A")) // datos OK
	    			accionweb.agregarObjeto("hayDatoslista", 1);
	    		else
	    			accionweb.agregarObjeto("mensaje", nombre);
	    		if(indHay.trim().equals("P") && tipoDocumento.trim().equals("H"))
	    			accionweb.agregarObjeto("mensaje", "Este RUT No existe en archivo maestro de proveedores.");
	    		else
	    			accionweb.agregarObjeto("mensaje", nombre);
	    		if(indHay.trim().equals("H") &&  !tipoDocumento.trim().equals("BHE") &&  tipoDocumento.trim().equals("BOH"))
	    			accionweb.agregarObjeto("mensaje", "Este RUT No existe en archivo maestro de prestadores de servicio.");
	    		else
	    			accionweb.agregarObjeto("mensaje", nombre);*/
	    			
		    } else {
		    	if(rutnum > 0) {
		    	  accionweb.agregarObjeto("mensaje", "Este RUT no se encuentra registrado.Favor enviar correo a proyecto.ERP@usm.cl");
		    	  accionweb.agregarObjeto("indHay", "N"); 
		    	}
		    	else {
		    		 accionweb.agregarObjeto("mensaje", "");
			    	 accionweb.agregarObjeto("indHay", ""); 
		    	}
		    	
		    }	
	}
		//VS.29.12.2016. sede no va
	    //List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
	    //listSede = moduloValePagoB.getListaSede(accionweb.getReq());
	    //String comboSede = "<select name=\"sede\" id=\"sede\" class=\"estilo_Azul\" onChange=\"\" >"+
	    //                  "<option value=\"-1\" selected=\"selected\">Seleccione</option> ";
	    //  if(identificador == 1)
	    //for (Presw18DTO ss : listSede){
	    //	comboSede += "<option value=" + ss.getNummes() + ">" + ss.getDesuni() + "</option>";	    	
	    //}
	    //comboSede += "</select>";
	   
	    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
	    moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
	    
	    List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
	    listaTipoDocumento = moduloRecuperacionGastoB.getTipoDocumento();
	    accionweb.agregarObjeto("listaTipoDocumento", listaTipoDocumento);
	    
		/*Item items = new Item();
		Collection<ItemDTO> lista = (Collection<ItemDTO>) items.todos_items();
		List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
		for(ItemDTO pp: lista){
			//System.out.println("incren: "+ pp.getIncren()+" "+ pp.getNomite()+"  "+ pp.getItepre());
			if(pp.getIncren().trim().equals("S"))
				listaItem.add(pp);
			
		}*/
		//	este acceso cambia a partir de 27/01/2015 pr instrucciones de correo PP accediendo a partir de ahora a la tabla cocof139 en lugar de cocof23
		List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
		listaItem = moduloRecuperacionGastoB.getItemCocof139B();
		
		accionweb.agregarObjeto("listaItem", listaItem);
	  
		DateTool fechaActual = new DateTool();
		String fecha = "";
		String dia = fechaActual.getDay()+"";
		if(dia.length() == 1)
			dia = "0" + dia;
		String mes = (fechaActual.getMonth()+1)+"";
		if(mes.length()== 1)
			mes = "0" + mes;
		fecha = dia + "/" + mes + "/" + String.valueOf(fechaActual.getYear());
  
	    accionweb.agregarObjeto("fecha", fecha);
	   // accionweb.agregarObjeto("comboSede", comboSede);
	    accionweb.agregarObjeto("esRecuperacionGastoB", 1);   
	}
	public void verificaRutPago(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		int identificador = Util.validaParametro(accionweb.getParameter("identificador"), 0);
			
		String nombre = "";
		if(rutnum > 0 && String.valueOf(rutnum).length() <= 8){
			nombre = moduloValePagoB.getVerificaRut(rutnum+""+dvrut);
			
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
			
		}
	   	if(!nombre.trim().equals("")){
	    		accionweb.agregarObjeto("hayDatoslista", 1);
	    		accionweb.agregarObjeto("nomIdentificador", nombre);
	    } else{
	    	if(String.valueOf(rutnum).length() > 8)
	    		accionweb.agregarObjeto("mensaje", "Este RUT es inv�lido.");
	    	else
	    		accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado. Debe agregarlo.");
	    }
	    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
	    listSede = moduloValePagoB.getListaSede(accionweb.getReq());
	    String comboSede = "<select name=\"sede\" id=\"sede\" class=\"estilo_Azul\" onChange=\"\" >"+
	                       "<option value=\"-1\" selected=\"selected\">Seleccione</option> ";
	  //  if(identificador == 1)
	    for (Presw18DTO ss : listSede){
	    	comboSede += "<option value=" + ss.getNummes() + ">" + ss.getDesuni() + "</option>";
	    	
	    }
	    comboSede += "</select>";
	   
	    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
	    moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
	    
	    List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
	    listaTipoDocumento = moduloRecuperacionGastoB.getTipoDocumento();
	    accionweb.agregarObjeto("listaTipoDocumento", listaTipoDocumento);
	    
		/*Item items = new Item();
		Collection<ItemDTO> lista = (Collection<ItemDTO>) items.todos_items();
		List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
		for(ItemDTO pp: lista){
			//System.out.println("incren: "+ pp.getIncren()+" "+ pp.getNomite()+"  "+ pp.getItepre());
			if(pp.getIncren().trim().equals("S"))
				listaItem.add(pp);
			
		}
		accionweb.agregarObjeto("listaItem", listaItem);*/
		
		//	este acceso cambia a partir de 27/01/2015 pr instrucciones de correo PP accediendo a partir de ahora a la tabla cocof139 en lugar de cocof23
		List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
		listaItem = moduloRecuperacionGastoB.getItemCocof139B();
		accionweb.agregarObjeto("listaItem", listaItem);
	  
		DateTool fechaActual = new DateTool();
		String fecha = "";
		String dia = fechaActual.getDay()+"";
		if(dia.length() == 1)
			dia = "0" + dia;
		String mes = (fechaActual.getMonth()+1)+"";
		if(mes.length()== 1)
			mes = "0" + mes;
		fecha = dia + "/" + mes + "/" + String.valueOf(fechaActual.getYear());
  
	    accionweb.agregarObjeto("fecha", fecha);
	    accionweb.agregarObjeto("comboSede", comboSede);
	    accionweb.agregarObjeto("listSede", listSede);
		accionweb.agregarObjeto("identificador", identificador);		  		  
		accionweb.agregarObjeto("esRecuperacionGastoB", 1);   
		
	}
	
	public void cargaResultadoSolicitudDocumento(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numeroDocumento = Util.validaParametro(accionweb.getParameter("numeroDocumento"),0);
   	  	String tipoDocumento = Util.validaParametro(accionweb.getParameter("tipoDocumento"),"");
   	  	int rutnumDoc = Util.validaParametro(accionweb.getParameter("rutnumDoc"), 0);
   	  	String dvrutDoc = Util.validaParametro(accionweb.getParameter("dvrutDoc"),"");
   	  	long iva = Util.validaParametro(accionweb.getParameter("iva"), 0);
   	  	long totalDocumento = Util.validaParametro(accionweb.getParameter("totalDocumento"), 0);
   	  	String glosa = Util.validaParametro(accionweb.getParameter("glosa"),"");
   	  	String rubroGasto = Util.validaParametro(accionweb.getParameter("rubroGasto"),""); // VS.03.01.2017. long -- string -- ""   	  	
   	    String cuentaPresup = Util.validaParametro(accionweb.getParameter("cuentaPresup"), ""); //VS.06.01.2017.
  	    String cuentaPresupMod = Util.validaParametro(accionweb.getParameter("cuentaPresupMod"), ""); //VS.06.01.2017.
  	    String cuentaPresupModCam = Util.validaParametro(accionweb.getParameter("cuentaPresupModCam"), ""); 
 	    String desuni = Util.validaParametro(accionweb.getParameter("desuni"),""); //VS.06.01.2017.
 	    int modifica = Util.validaParametro(accionweb.getParameter("modifica"),0);
 	    List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumento");
 		List<Cocow36DTO>  listaSolicitudDocumOrig = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumOrig");

   		//List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumento"); 
   		//List<Cocow36DTO>  lista = new ArrayList<Cocow36DTO>();
   		String validaDocumento = "";
   		long total = 0;   		
        if(cuentaPresup.trim().equals(""))
        {        	
        	if(cuentaPresupModCam.trim().equals(""))
        	{
        	  cuentaPresup = cuentaPresupMod;	
        	}
        	else cuentaPresup = cuentaPresupModCam;
        }
        
        DecimalFormat formato = new DecimalFormat("00000000");
		String rutFormateado = formato.format(rutnumDoc);
		formato = new DecimalFormat("00000000");
		String numeroFormateado = formato.format(numeroDocumento);
		String dvrutDocFormato = dvrutDoc.trim();
		if(dvrutDocFormato.trim().equals(""))
			dvrutDocFormato = " ";
		
		if(String.valueOf(rutnumDoc).length() <= 8 && String.valueOf(numeroDocumento).length() <= 8)
			validaDocumento = moduloRecuperacionGastoB.getValidaDocumento("VD1", rutnumDoc, dvrutDoc, numeroDocumento, tipoDocumento); //VS.29.12.2016. VDR - VD1
	   
        if(modifica == 1 && !validaDocumento.trim().equals("")){
   			/*comparar si es el mismo documento*/
		
			
			if (listaSolicitudDocumOrig != null && listaSolicitudDocumOrig.size() > 0){
				for (Cocow36DTO ss : listaSolicitudDocumOrig){
					if(ss.getResval().trim().equals(rutFormateado.trim() + dvrutDocFormato + numeroFormateado.trim() ) )
						validaDocumento = "";					
			        
				}
				
			} 
			
   		}
   	 	 //System.out.println("String.valueOf(numeroDocumento).length() "+String.valueOf(numeroDocumento).length());
		 if(!validaDocumento.trim().equals("") || String.valueOf(rutnumDoc).length() > 8 || String.valueOf(numeroDocumento).length() > 8) 
	     	{
	    	if(String.valueOf(rutnumDoc).length() > 8)
	    		accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > RUT Inv�lido.</font> ");
		    else {
		    	   if(String.valueOf(numeroDocumento).length() > 8)
		    		   accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > N�mero de Documento Inv�lido.</font>");
		    	   else {
	    			if(validaDocumento.trim().equals("1"))
	    				accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > Documento existe en cuentas Corrientes.</font>");
	    			if(validaDocumento.trim().equals("2"))
	    				accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > Documento existe en otra rendici�n.</font>");
	    			if(validaDocumento.trim().equals("3"))
	    				accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > Documento en proceso en otros sistemas.</font>");
		    	   }
	        	}
	    } else {
	   		//VS.06.01.2017. nuevo
        	if( moduloRecuperacionGastoB.getVerificaRecuperacionBanner(rutnumDoc, dvrutDoc, numeroDocumento, tipoDocumento) > 0){
        		accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" >Documento existe en Banner.</font> .");
    			
        	}else {
        		
        		// codOrg.            codCuenta		
			long saldoCuenta =  moduloValePagoB.getBuscaSaldo(cuentaPresup.trim(),rubroGasto.trim()); //VS.29-11-2016.Modificacion BANNER.saldo
			
			int sobrepasa = 0;
			sobrepasa = moduloRecuperacionGastoB.getAgregaListaSolicitudDocumento(accionweb.getReq(),saldoCuenta, rutUsuario, dv);
			switch (sobrepasa){
				case 1: {
				accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" >Este valor excede el saldo de la cuenta de gasto de la organizaci�n " + cuentaPresup + ", que es de: $ " + moduloRecuperacionGastoB.formateoNumeroSinDecimales(Double.parseDouble(saldoCuenta+""))+"</font> .");
				break;
				}
				case 2: {
				accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"destacado\" size=\"12px\">Lo sentimos, este valor no se puede agregar a la cuenta, ya que excede el valor del la Boleta, que es de: $ " + moduloRecuperacionGastoB.formateoNumeroSinDecimales(Double.parseDouble(totalDocumento+""))+"</font> .");
				break;
				}
				case 3: {
				accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"destacado\" size=\"12px\">Lo sentimos, el documento ya se encuentra agregada </font> .");
				break;
				}
				case 4: {
					accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"destacado\" size=\"12px\">Este RUT no se encuentra registrado.Favor enviar correo a proyecto.ERP@usm.cl</font> .");
					break;
					}
			}
			
			listaSolicitudDocumento = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumento");
			
        	}
			//FIN nuevo
	    }
	    
		accionweb.agregarObjeto("listaSolicitudDocumento",listaSolicitudDocumento ); //lista
	//	if (listaSolicitudes != null && listaSolicitudes.size() > 0){
	//		accionweb.agregarObjeto("registra", 1);
	//	    accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes); 
	//	    for (Presw18DTO ss : listaSolicitudes)
	//			total = total + ss.getUsadom();
     //       accionweb.agregarObjeto("totalSolicitudHonorario", total);
	//	}
	    listaSolicitudDocumento = (List<Cocow36DTO>) listaSolicitudDocumento;
    	if(listaSolicitudDocumento != null && listaSolicitudDocumento.size() > 0){
    		for (Cocow36DTO ss : listaSolicitudDocumento){
    			total = total + ss.getValnu2();	 
    		}
       }
	    accionweb.agregarObjeto("totalSolDocumento", total);
	    //11.01.2017
	    long totalrubro = 0;
	    List<Cocow36DTO> listaSolicitudDistribucion = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
	    accionweb.agregarObjeto("listaSolicitudDistribucion",listaSolicitudDistribucion ); //lista
	    
	    listaSolicitudDistribucion = (List<Cocow36DTO>) listaSolicitudDistribucion;
    	if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
    		for (Cocow36DTO ss : listaSolicitudDistribucion){
    			totalrubro = totalrubro + ss.getValnu2();	 
    		}
       }	   
    	accionweb.agregarObjeto("totalSolDistribucion", totalrubro);


	    //listaSolicitudDocumento = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("lista");
	   	
	    //accionweb.getSesion().setAttribute("listaSolicitudDocumento", lista); //lista
	    
        //accionweb.agregarObjeto("listaSolicitudDocumento", lista); //lista
		
		List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
	    listaTipoDocumento = moduloRecuperacionGastoB.getTipoDocumento();
	    accionweb.agregarObjeto("listaTipoDocumento", listaTipoDocumento);
	    
		/*Item items = new Item();
		Collection<ItemDTO> listaI = (Collection<ItemDTO>) items.todos_items();
		List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
		for(ItemDTO pp: listaI){
			//System.out.println("incren: "+ pp.getIncren()+" "+ pp.getNomite()+"  "+ pp.getItepre());
			if(pp.getIncren().trim().equals("S"))
				listaItem.add(pp);
			
		}
		accionweb.agregarObjeto("listaItem", listaItem);*/

		//	este acceso cambia a partir de 27/01/2015 pr instrucciones de correo PP accediendo a partir de ahora a la tabla cocof139 en lugar de cocof23
		List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
		listaItem = moduloRecuperacionGastoB.getItemCocof139B();
		accionweb.agregarObjeto("listaItem", listaItem);
		accionweb.agregarObjeto("listaItemRubro", listaItem);
		
		accionweb.getSesion().setAttribute("rubroGasto", rubroGasto);
		/*cargaResultadoTablaResumen(accionweb,listaSolicitudDocumento);*/
		
	}
	
/*	public void cargaResultadoTablaResumen(AccionWeb accionweb, List<Cocow36DTO>  listaSolicitudDocumento) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		//int cuentaPresup = Util.validaParametro(accionweb.getParameter("cuentaPresup"),0);
		String cuentaPresup = Util.validaParametro(accionweb.getParameter("cuentaPresup"),"");
   	  	String desuni = Util.validaParametro(accionweb.getParameter("desuni"),"");
   	    String rubroGasto = Util.validaParametro(accionweb.getParameter("rubroGasto"),"");
   	    long totalSolDocumento = Util.validaParametro(accionweb.getParameter("totalSolDocumento"), 0); 
   	    long valor = Util.validaParametro(accionweb.getParameter("valor"), 0);
		
	    //VS.11.01.2017.- listaSolicitudDistribucion guarda los datos de tabla resumen
   	    List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
   	    
    	if(listaSolicitudDocumento != null && listaSolicitudDocumento.size() > 0){
    		if (listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0)
    		{
	    		for (Cocow36DTO ss : listaSolicitudDocumento){	    			
	    			//total = total + ss.getValnu2();
	 			   for (Cocow36DTO ltp : listaSolicitudDistribucion) {
	 				  if (ss.getValalf().trim().substring(3,9).equals(ltp.getResval().trim())) // Pregunta si rubro gasto son iguales
	 					{
	 					 ltp.setValnu2(ltp.getValnu2() + ss.getValnu2());
					     break;   
						}    
			       }	    			
	    			
	    		}
    		}
    		else
    		{
    			listaSolicitudDistribucion = new ArrayList<Cocow36DTO>();
    			Cocow36DTO cocow36DTO = new Cocow36DTO();
				cocow36DTO.setNomcam("CUENTA");
				cocow36DTO.setResval(rubroGasto.trim());
				cocow36DTO.setValnu2(valor);
				cocow36DTO.setValalf(cuentaPresup.trim());
				listaSolicitudDistribucion.add(cocow36DTO);
    		}
       }
	  		
			boolean agrega = true;
			long total = 0;
			if (listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
				for (Cocow36DTO ss : listaSolicitudDistribucion){
					//getNomcam("CUENTA");
					//cocow36DTO.setResval --> rubro gasto
					//cocow36DTO.setValnu2 --> total Valor
					//cocow36DTO.setValalf --> cuenta presupuestari
					
					//if(ss.getValnu1() == cuentaPresup) VS.29.12.2016.
					//System.out.println(ss.getValalf());
					//System.out.println(ss.getResval());
					//System.out.println(ss.getTiping());
					//System.out.println(ss.getValnu1());
					//System.out.println(ss.getAccion());
					//System.out.println(ss.getIdsoli());
					//System.out.println(ss.getNomcam());
					
					if(ss.getValalf().trim().equals(cuentaPresup.trim())) 
						
						agrega = false;
					total = total + ss.getValnu2();
			        
				}
			} else listaSolicitudDistribucion = new ArrayList<Cocow36DTO>();
			
			if(agrega){
				if(String.valueOf(valor).length() > 8)
					accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > El valor ingresado es muy grande.</font> .");
				else {
				if(totalSolDocumento <= (total + valor))
					{
						Cocow36DTO cocow36DTO = new Cocow36DTO();
						cocow36DTO.setNomcam("CUENTA");
						//cocow36DTO.setValnu1(cuentaPresup); VS.29.12.2016. 
						cocow36DTO.setResval(cuentaPresup.trim()); // VS.04.01.2017
						cocow36DTO.setValnu2(valor);
						cocow36DTO.setValalf(desuni);
						listaSolicitudDistribucion.add(cocow36DTO);
						total = total + valor;
				        
				} else 	accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > El Valor excede el valor total de la Solicitud.</font> .");
				}
			} else 	accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > Esta cuenta ya se encuentra agregada.</font> .");
			
			
            accionweb.agregarObjeto("listaSolicitudDistribucion", listaSolicitudDistribucion);
			accionweb.getSesion().setAttribute("listaSolicitudDistribucion", listaSolicitudDistribucion);
		    accionweb.agregarObjeto("totalSolDistribucion", total);
	}
	*/
	public void cargaResultadoSolicitudDistribucion(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		//int cuentaPresup = Util.validaParametro(accionweb.getParameter("cuentaPresup"),0);
		String cuentaPresup = Util.validaParametro(accionweb.getParameter("cuentaPresup"),"");
   	  	String desuni = Util.validaParametro(accionweb.getParameter("desuni"),"");
   	    long totalSolDocumento = Util.validaParametro(accionweb.getParameter("totalSolDocumento"), 0); 
   	    long valor = Util.validaParametro(accionweb.getParameter("valor"), 0);
		
	  		List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
			boolean agrega = true;
			long total = 0;
			if (listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
				for (Cocow36DTO ss : listaSolicitudDistribucion){
					//if(ss.getValnu1() == cuentaPresup) VS.29.12.2016.
					//System.out.println(ss.getValalf());
					//System.out.println(ss.getResval());
					//System.out.println(ss.getTiping());
					//System.out.println(ss.getValnu1());
					//System.out.println(ss.getAccion());
					//System.out.println(ss.getIdsoli());
					//System.out.println(ss.getNomcam());
					
					if(ss.getValalf().trim().equals(cuentaPresup.trim())) 
						agrega = false;
					total = total + ss.getValnu2();
			        
				}
			} else listaSolicitudDistribucion = new ArrayList<Cocow36DTO>();
			
			if(agrega){
				if(String.valueOf(valor).length() > 8)
					accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > El valor ingresado es muy grande.</font> .");
				else {
				if(totalSolDocumento <= (total + valor))
					{
						Cocow36DTO cocow36DTO = new Cocow36DTO();
						cocow36DTO.setNomcam("CUENTA");
						//cocow36DTO.setValnu1(cuentaPresup); VS.29.12.2016. 
						cocow36DTO.setResval(cuentaPresup.trim()); // VS.04.01.2017
						cocow36DTO.setValnu2(valor);
						cocow36DTO.setValalf(desuni);
						listaSolicitudDistribucion.add(cocow36DTO);
						total = total + valor;
				        
				} else 	accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > El Valor excede el valor total de la Solicitud.</font> .");
				}
			} else 	accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > Esta cuenta ya se encuentra agregada.</font> .");
			
			
            accionweb.agregarObjeto("listaSolicitudDistribucion", listaSolicitudDistribucion);
			accionweb.getSesion().setAttribute("listaSolicitudDistribucion", listaSolicitudDistribucion);
		    accionweb.agregarObjeto("totalSolDistribucion", total);
	 
	    		
	}
		
	 synchronized public void eliminaTodoSolicitudDocumento(AccionWeb accionweb) throws Exception {
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
		   moduloRecuperacionGastoB.getEliminaTodoListaSolicitaDocumento(accionweb.getReq());
		   
		    	
			List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumento");
			List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
		    // accionweb.agregarObjeto("listaSolicitudDocumento", listaSolicitudDocumento); 
		    //accionweb.agregarObjeto("listaSolicitudDistribucion", listaSolicitudDistribucion);
		    
		    List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
			listaItem = moduloRecuperacionGastoB.getItemCocof139B();
			accionweb.agregarObjeto("listaItem", listaItem);
			accionweb.agregarObjeto("listaItemRubro", listaItem);
			
			List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
		    listaTipoDocumento = moduloRecuperacionGastoB.getTipoDocumento();
		    accionweb.agregarObjeto("listaTipoDocumento", listaTipoDocumento);
			if(tipo > 0)
			 accionweb.agregarObjeto("opcion", tipo);
			
		}
	 
/*	 synchronized public void actualizaSaldoTablaResumen(AccionWeb accionweb) throws Exception {
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			//moduloRecuperacionGastoB.getactualizaSaldoTablaResumen(accionweb.getReq());
			 
			 List<Cocow36DTO>  listaSolicitudDistribucion = new ArrayList<Cocow36DTO>();
			 
			 if (listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0)
				{
					for (Cocow36DTO ss2 : listaSolicitudDistribucion)
					{						  									 //org.				rubro
					  long saldoCuenta =  moduloRecuperacionGastoB.getBuscaSaldo(ss2.getValalf(),ss2.getResval());
					  NumberTool numbertool = new NumberTool();
					  String muestraSaldo = String.valueOf(numbertool.format(saldoCuenta));
							 
					  if( ss2.getValnu2() > saldoCuenta)
					  {						  
					    accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" >Este valor EXCEDE el saldo de la cuenta " + ss2.getValalf() + ", que es de: $ " + moduloRecuperacionGastoB.formateoNumeroSinDecimales(Double.parseDouble(saldoCuenta+""))+"</font> .");
						muestraSaldo = "<font class=\"Estilo_Rojo\" >" + muestraSaldo + "</font>";
						ss2.setTiping(muestraSaldo);
					  }
					}
					listaSolicitudDistribucion = (List<Cocow36DTO>) listaSolicitudDistribucion;	
					accionweb.agregarObjeto("hayDatoslista", "1"); 
				}
				
			   //if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
		    	//	for (Cocow36DTO ss : listaSolicitudDistribucion){
		    	//		totalSolDistribucion = totalSolDistribucion + ss.getValnu2();	 
		    	//	}
		    	//}
			    accionweb.agregarObjeto("listaSolicitudDistribucion", listaSolicitudDistribucion);
			   
			    List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
				listaItem = moduloRecuperacionGastoB.getItemCocof139B();
				accionweb.agregarObjeto("listaItem", listaItem);
				accionweb.agregarObjeto("listaItemRubro", listaItem);
				
				List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
			    listaTipoDocumento = moduloRecuperacionGastoB.getTipoDocumento();
			    accionweb.agregarObjeto("listaTipoDocumento", listaTipoDocumento);
				if(tipo > 0)
				 accionweb.agregarObjeto("opcion", tipo);
				  
				
			}		*/ 
	 
	 synchronized public void actualizaSaldoTablaResumen(AccionWeb accionweb) throws Exception {
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			String cuentaPresup = Util.validaParametro(accionweb.getParameter("cuentaPresup"),"");
			String cuentaPresupModCam = Util.validaParametro(accionweb.getParameter("cuentaPresupModCam"),"");
			
			//String cuentaPresup = Util.validaParametro(accionweb.getSesion().getAttribute("cuentaPresup")+ "","");
			//String cuentaPresupMod = Util.validaParametro(accionweb.getSesion().getAttribute("cuentaPresupMod")+ "","");
			
	   	    
			   List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumento");
			  List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");

				 if (listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0)
					{
						for (Cocow36DTO ss2 : listaSolicitudDistribucion)
						{						  									 //org.				rubro
						  long saldoCuenta =  moduloValePagoB.getBuscaSaldo(cuentaPresup.trim(),ss2.getResval());
						  NumberTool numbertool = new NumberTool();
						  String muestraSaldo = String.valueOf(numbertool.format(saldoCuenta));
								 
						  if( ss2.getValnu2() > saldoCuenta)
						  {						  
						    accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" >Este valor EXCEDE el saldo de la cuenta " + ss2.getValalf() + ", que es de: $ " + moduloRecuperacionGastoB.formateoNumeroSinDecimales(Double.parseDouble(saldoCuenta+""))+"</font> .");
							muestraSaldo = "<font class=\"Estilo_Rojo\" >" + muestraSaldo + "</font>";
							ss2.setTiping(muestraSaldo);
						  }
						}
						listaSolicitudDistribucion = (List<Cocow36DTO>) listaSolicitudDistribucion;	
						accionweb.agregarObjeto("hayDatoslista", "1"); 
					}
				 
				long total = 0;
				long totalrubro = 0;
				if (listaSolicitudDocumento != null && listaSolicitudDocumento.size() > 0){
					accionweb.agregarObjeto("registra", 1);
				    accionweb.agregarObjeto("listaSolicitudDocumento", listaSolicitudDocumento); 
				    accionweb.agregarObjeto("listaSolicitudDistribucion", listaSolicitudDistribucion);
				    accionweb.agregarObjeto("cuentaPresupMod", cuentaPresup.trim());
				    accionweb.agregarObjeto("cuentaPresupModCam", cuentaPresup.trim());
				    
				    for (Cocow36DTO ss : listaSolicitudDocumento)
						total = total + ss.getValnu2();
		            accionweb.agregarObjeto("totalSolDocumento", total);
		            
		            for (Cocow36DTO ss : listaSolicitudDistribucion)
						totalrubro = totalrubro + ss.getValnu2();
		            accionweb.agregarObjeto("totalSolDistribucion", totalrubro);
				}
				/*Item items = new Item();
				Collection<ItemDTO> lista = (Collection<ItemDTO>) items.todos_items();
				List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
				for(ItemDTO pp: lista){
					//System.out.println("incren: "+ pp.getIncren()+" "+ pp.getNomite()+"  "+ pp.getItepre());
					if(pp.getIncren().trim().equals("S"))
						listaItem.add(pp);
					
				}*/
				//	este acceso cambia a partir de 27/01/2015 pr instrucciones de correo PP accediendo a partir de ahora a la tabla cocof139 en lugar de cocof23
				List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
				listaItem = moduloRecuperacionGastoB.getItemCocof139B();
				accionweb.agregarObjeto("listaItem", listaItem);
				accionweb.agregarObjeto("listaItemRubro", listaItem);
				
				List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
			    listaTipoDocumento = moduloRecuperacionGastoB.getTipoDocumento();
			    accionweb.agregarObjeto("listaTipoDocumento", listaTipoDocumento);
				if(tipo > 0)
				 accionweb.agregarObjeto("opcion", tipo);
				
			}	 
		
	 synchronized public void eliminaResultadoSolicitudDocumento(AccionWeb accionweb) throws Exception {
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			
			   moduloRecuperacionGastoB.getEliminaListaSolicitaDocumento(accionweb.getReq());
			   	accionweb.agregarObjeto("mensaje", "Se elimin� el Documento.");
			    	
				List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumento");
				List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
		  
				long total = 0;
				long totalrubro = 0;
				if (listaSolicitudDocumento != null && listaSolicitudDocumento.size() > 0){
					accionweb.agregarObjeto("registra", 1);
				    accionweb.agregarObjeto("listaSolicitudDocumento", listaSolicitudDocumento); 
				    accionweb.agregarObjeto("listaSolicitudDistribucion", listaSolicitudDistribucion);
				    for (Cocow36DTO ss : listaSolicitudDocumento)
						total = total + ss.getValnu2();
		            accionweb.agregarObjeto("totalSolDocumento", total);
		            
		            for (Cocow36DTO ss : listaSolicitudDistribucion)
						totalrubro = totalrubro + ss.getValnu2();
		            accionweb.agregarObjeto("totalSolDistribucion", totalrubro);
				}
				/*Item items = new Item();
				Collection<ItemDTO> lista = (Collection<ItemDTO>) items.todos_items();
				List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
				for(ItemDTO pp: lista){
					//System.out.println("incren: "+ pp.getIncren()+" "+ pp.getNomite()+"  "+ pp.getItepre());
					if(pp.getIncren().trim().equals("S"))
						listaItem.add(pp);
					
				}*/
				//	este acceso cambia a partir de 27/01/2015 pr instrucciones de correo PP accediendo a partir de ahora a la tabla cocof139 en lugar de cocof23
				List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
				listaItem = moduloRecuperacionGastoB.getItemCocof139B();
				accionweb.agregarObjeto("listaItem", listaItem);
				accionweb.agregarObjeto("listaItemRubro", listaItem);
				
				List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
			    listaTipoDocumento = moduloRecuperacionGastoB.getTipoDocumento();
			    accionweb.agregarObjeto("listaTipoDocumento", listaTipoDocumento);
				if(tipo > 0)
				 accionweb.agregarObjeto("opcion", tipo);
				
			}
	 synchronized public void eliminaResultadoSolicitudDistribucion(AccionWeb accionweb) throws Exception {
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			
			   moduloRecuperacionGastoB.getEliminaListaSolicitaDistribucion(accionweb.getReq());
			   	accionweb.agregarObjeto("mensaje", "Se elimin� la Organizaci�n de a Distribuci�n."); //VS.03.01.2016. Cuenta - organizaci�n
			    	
				List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
		  
				long total = 0;
				if (listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
					accionweb.agregarObjeto("registra", 1);
				    accionweb.agregarObjeto("listaSolicitudDistribucion", listaSolicitudDistribucion); 
				    for (Cocow36DTO ss : listaSolicitudDistribucion)
						total = total + ss.getValnu2();
		            accionweb.agregarObjeto("totalSolDistribucion", total);
				}
				
				if(tipo > 0)
				 accionweb.agregarObjeto("opcion", tipo);
				
			}
	 
	 synchronized public void registraRecuperacionGasto(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			//List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
			List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumento");

		    
			long rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
			String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
			String identificadorInterno = Util.validaParametro(accionweb.getParameter("identificadorInterno"),"");	
			// mb agrega validaci�n
			String cuentaPresup = Util.validaParametro(accionweb.getParameter("cuentaPresup"), "");	
	
			//long sede = Util.validaParametro(accionweb.getParameter("sede"), 0); VS.03.01.2016.
			if(String.valueOf(rutnum).length() > 8 || identificadorInterno.trim().length() > 40 || cuentaPresup.trim().equals(""))
				accionweb.agregarObjeto("mensaje", "Datos Inv�lidos");
			else {
			
			int numSolicitud = 0;
		    try {
		  
		    numSolicitud = moduloRecuperacionGastoB.getRegistraRecuperacion(accionweb.getReq(),rutUsuario,dv);
		    if(numSolicitud > 0){
		   	    accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa la solicitud de Recuperacion de gasto. Debe enviar documentos de respaldo a la Direcci�n de Finanzas haciendo referencia al n�mero de solicitud: "+ numSolicitud);
		   	    
		   	 if(listaSolicitudDocumento != null)
				accionweb.getSesion().removeAttribute("listaSolicitudDocumento");	
		    }
		   	   // if (listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
				//	accionweb.agregarObjeto("registra", 1);
				//    accionweb.getSesion().removeAttribute("listaSolicitudDistribucion");
				//    if(listaSolicitudDocumento != null)
				//    	accionweb.getSesion().removeAttribute("listaSolicitudDocumento");
				//}		   	 
		    //} else
		    //	accionweb.agregarObjeto("mensaje", "No se registr� la solicitud de recuperaci�n de gasto.");	
		    }
		    catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado getRegistraRecuperacion.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
			   this.cargarRecuperacion(accionweb);
			}
		} 
	 synchronized public void actualizaRecuperacionGasto(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
			List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumento");
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			String cuentaPresup = Util.validaParametro(accionweb.getParameter("cuentaPresup"), ""); //VS.06.01.2017.
			boolean registra = false;
		    
		    try {
			registra = moduloRecuperacionGastoB.getActualizaRecuperacion(accionweb.getReq(),rutUsuario,dv, numDoc);
		    if(registra){
		   	    accionweb.agregarObjeto("mensaje", "Se actualiz� en forma exitosa la solicitud de Recuperacion de gasto.");
		   	   accionweb.agregarObjeto("cuentaPresupMod", cuentaPresup.trim());
		   	    if (listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
					accionweb.agregarObjeto("registra", 1);
				    accionweb.getSesion().removeAttribute("listaSolicitudDistribucion");
				    if(listaSolicitudDocumento != null)
				    	accionweb.getSesion().removeAttribute("listaSolicitudDocumento");
				}
		   	 Thread.sleep(500);
		   	 this.consultaRecuperacion(accionweb);
		    } else
		    	accionweb.agregarObjeto("mensaje", "No se modific� la solicitud de recuperaci�n de gasto.");	
		    } catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado actualizaRecuperacionGasto.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
		} 
	 public void cargaAutorizaRecuperacion(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
			
			PreswBean preswbean = null;
			Collection<Presw18DTO> listaRecuperacion = null;
			String titulo = "";
			String tituloDetalle1 = "";
			String tituloDetalle2 = "";
			String tipcue = "";
			//System.out.println("rutUsuario: "+rutUsuario);
			//System.out.println("dv: "+dv);
			switch (tipo) {
			case  2:// lista de vales para modificar
				{preswbean = new PreswBean("LM1",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"",""); // VS.28-12-2016 LMR - LM1
				titulo = "Modificaci�n";
				break;
			}
			// case 3: case 6: case 8: case 9: case 10: // lista de vales para autorizar
			case 3: case 18: case 19: case 20: case 8: // lista de SOLICITUDES para autorizar
				{String nomtipo = "";
				if(tipo == 3 || tipo == 8 ){
					titulo = "Autorizaci�n Presupuestaria de Recuperaci�n de Gastos";
					nomtipo = "LR1"; // VS.28.12.2016 LRA - LR1
					tituloDetalle1 = "Autorizar";
					tituloDetalle2 = "Rechazar";
					tipo = 3; // autoriza responsable de cuenta
					accionweb.agregarObjeto("opcion2", 8); // rechazar responsable de cuenta
					accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
					accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
				
				}
				if(tipo == 18){
					   nomtipo = "LRE";
					   titulo = "Autorizaci�n MECESUP VRA";
					   tipcue = "Y";
					   accionweb.agregarObjeto("opcion2", 21);
					}
				if(tipo == 19){
						nomtipo = "LRE";
						titulo = "Autorizaci�n UCP";
						tipcue = "X";
						accionweb.agregarObjeto("opcion2", 22);
					}
				if(tipo == 20){
						nomtipo = "LRE";
						titulo = "Autorizaci�n MECESUP DIPRES";
						tipcue = "Z";
						accionweb.agregarObjeto("opcion2", 23);
					}
				preswbean = new PreswBean(nomtipo,0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				preswbean.setTipcue(tipcue);
				
				tituloDetalle1 = "Autorizar";
				tituloDetalle2 = "Rechazar";
				accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
				accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
				break;
				}
				
			case 4: // lista de consulta de solicitudes de recuperacion de gastos
				{		
				int fechaInicio = Util.validaParametro(accionweb.getParameter("fechaInicio"),0);
				int fechaTermino = Util.validaParametro(accionweb.getParameter("fechaTermino"),0);
				String fechaIni = Util.validaParametro(accionweb.getParameter("fechaIni"),"");
				String fechaTer = Util.validaParametro(accionweb.getParameter("fechaTer"),"");
				//int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0); VS.28/12/2016
				String organizacion = Util.validaParametro(accionweb.getParameter("organizacion"),"");
				int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
				String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
				
				if(fechaInicio > 0 && fechaTermino == 0 ){
					DateTool fechaActual = new DateTool();
					String dia = fechaActual.getDay()+"";
					if(dia.length() == 1)
						dia = "0" + dia;
					String mes = (fechaActual.getMonth()+1)+"";
					if(mes.length()== 1)
						mes = "0" + mes;
					fechaTermino = Integer.parseInt(String.valueOf(fechaActual.getYear()) + mes + dia);
					fechaTer = dia + "/" + mes + "/" + String.valueOf(fechaActual.getYear());
					
				}
				if(!organizacion.equals("") || rutnum != 0){ //if(unidad >= 0){ VS.28.12.2016
				preswbean = new PreswBean("CR1",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"",""); //CRG - CR1
				preswbean.setCajero(organizacion.trim());			
				//preswbean.setCoduni(unidad);				
				preswbean.setFecmov(fechaInicio);
				preswbean.setNumche(fechaTermino);
				preswbean.setRutide(rutnum);
				preswbean.setDigide(dvrut);
				preswbean.setNumcom(rutUsuario);
				preswbean.setTipcue(dv);				
				
				listaRecuperacion = (Collection<Presw18DTO>) preswbean.consulta_presw18();
				}
				
			    
				
				titulo = "Consulta";	
				List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
				moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
				listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
				accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
			   	//accionweb.agregarObjeto("unidad", unidad); VS.28-12-2016
				accionweb.agregarObjeto("organizacion", organizacion);
			   	accionweb.agregarObjeto("fechaIni", fechaIni);
			   	accionweb.agregarObjeto("fechaTer", fechaTer);
			   	accionweb.agregarObjeto("rutnum", rutnum);
			   	accionweb.agregarObjeto("dvrut", dvrut);	
				break;
				}
			case 5: // lista de solicitudes para revisi�n de documentaci�n
				{
			    //int sede = Util.validaParametro(accionweb.getParameter("sede"), -1);
			    //if(sede >= 0){
			    preswbean = new PreswBean("FL1",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"",""); // VS.28-12-2016 FLR - FL1			    
			    listaRecuperacion = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			    
			    //}
				titulo = "Revisi�n de Documentaci�n en Finanzas";
				tipo = 5;// para que cambie opcion en caso de que sea 7
				//List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
				//listSede = moduloValePagoB.getListaSede(accionweb.getReq());
				//accionweb.agregarObjeto("listSede", listSede);
				//accionweb.agregarObjeto("sedeOpc", sede);
				break;
				}
			case 6: case 9: // lista de solicitudes para recepci�n en finanzas   // MAA 26/02/2018 ya no se usa
			{
		    int sede = Util.validaParametro(accionweb.getParameter("sede"), -1);
		    if(sede >= 0){
		    	preswbean = new PreswBean("LDR",0,0,0,0, rutUsuario,0,"",sede,dv, "",0,0,0,0,"","");
		    	listaRecuperacion = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		    }
			titulo = "Recepci�n en Finanzas";
			tituloDetalle1 = "Autorizar";
			tituloDetalle2 = "Rechazar";
			accionweb.agregarObjeto("opcion2", 9); // rechazar en DF
			accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
			accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
            tipo = 6;
            List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
			listSede = moduloValePagoB.getListaSede(accionweb.getReq());
			accionweb.agregarObjeto("listSede", listSede);
			accionweb.agregarObjeto("sedeOpc", sede);
			break;
			}
			case 16: case 17: case 21:// lista de sedeAutorizadas
			{   
				int sedeLista = Util.validaParametro(accionweb.getParameter("sedeLista"),0);
				List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
		        List<Sede> listaSede = new ArrayList<Sede>();
		        listaSede = moduloValePagoB.getListaSedeAutorizadas(rutUsuario);
		        switch (tipo){
		        case 16: {
		        	     titulo = "Autorizaci�n DAF";
		        	     break;
		        }
		        case 17: {
		        	     titulo = "Autorizaci�n Sobregiro";
		        	     break;
		        }
		        case 21:{ titulo = "Consulta Sin Financiamiento";
	   	     			  break;
		        	
		        }
		        }
		        if (listaSede != null && listaSede.size() > 0) { 
		          accionweb.agregarObjeto("listaSede", listaSede);
		          accionweb.agregarObjeto("titulo", titulo);
		        }
		         
		        if (tipo == 16) {
			        if (sedeLista > 0){
			          accionweb.agregarObjeto("sedeLista", sedeLista);	
			          preswbean = new PreswBean("LD1",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			          preswbean.setSucur(sedeLista);
			          listaRecuperacion = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			        }
		        }    
			    else  { // tipo ==  17 || tipo ==  21
			    	preswbean = new PreswBean("RFF",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");	
			        listaRecuperacion = (Collection<Presw18DTO>) preswbean.consulta_presw18();	
			    }
		        
		        List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
				moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
				listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
				accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
		        
			 break;
			}
		
			}
			if(tipo != 4 && tipo != 12 && tipo != 5 && tipo != 6 && tipo != 9 && tipo != 16 && tipo != 17 && tipo != 21)
				listaRecuperacion = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			
			
			//System.out.println("listaValePago.size(): "+listaValePago.size());
		    if(listaRecuperacion != null && listaRecuperacion.size() > 0)
		    {
	        	accionweb.agregarObjeto("hayDatoslista", "1");		    

		    //for (Presw18DTO ss : listaRecuperacion){
			//	//if(ss.getValnu1() == cuentaPresup) VS.29.12.2016.
			//	System.out.println("getDesite: " + ss.getDesite());
			//	System.out.println("Desuni: " + ss.getDesuni());
			//	System.out.println("Iddigi: " + ss.getIddigi());
			//	System.out.println("Idsoli: " + ss.getIdsoli());
			//	System.out.println("Indhay: " + ss.getIndhay());
			//	System.out.println("tIndprc: " + ss.getIndprc());
			//	System.out.println("Indpro: " + ss.getIndpro());    
			//	System.out.println("Nompro: " + ss.getNompro());
			//	System.out.println("Nomtip: " + ss.getNomtip());
			//	System.out.println("Tipmov: " + ss.getTipmov());
			//	System.out.println("Tippro: " + ss.getTippro());	}	
			}	    
	        accionweb.agregarObjeto("listaRecuperacion", listaRecuperacion);
	      	accionweb.agregarObjeto("esRecuperacionGastoB", "1");
	      	accionweb.agregarObjeto("opcion", String.valueOf(tipo));
	    	accionweb.agregarObjeto("opcionMenu", tipo);
	      	accionweb.agregarObjeto("titulo", titulo);
	      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
	      	
	      	/*for (Presw18DTO ss : listaRecuperacion){	
		    	System.out.println("getAcumum: " + ss.getAcumum());
		    	System.out.println("getClass: " + ss.getClass());
		    	System.out.println("getCodUni: " + ss.getCoduni());			    	
				System.out.println("getDesite: " + ss.getDesite());
				System.out.println("Desuni: " + ss.getDesuni());
				System.out.println("Diferm: " + ss.getDiferm());
				System.out.println("Diferm: " + ss.getFecdoc());
				System.out.println("Iddigi: " + ss.getIddigi());
				System.out.println("Idsoli: " + ss.getIdsoli());
				System.out.println("Indhay: " + ss.getIndhay());
				System.out.println("tIndprc: " + ss.getIndprc());
				System.out.println("Indpro: " + ss.getIndpro());
				System.out.println("Itedoc: " + ss.getItedoc());
				System.out.println("Nompro: " + ss.getNompro());
				System.out.println("Nomtip: " + ss.getNomtip());
				System.out.println("Numdoc: " + ss.getNumdoc());
				System.out.println("Nommes: " + ss.getNummes());
				System.out.println("Preano: " + ss.getPreano());
				System.out.println("Presac: " + ss.getPresac());
				System.out.println("Presum: " + ss.getPresum());
				System.out.println("Rutide: " + ss.getRutide());					
				System.out.println("Tipmov: " + ss.getTipmov());
				System.out.println("Tippro: " + ss.getTippro());
				System.out.println("Usadac: " + ss.getUsadac());
				System.out.println("Usadom: " + ss.getUsadom());
				System.out.println("Valdoc: " + ss.getValdoc());
				
				}	
	      	*/
	      	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	    	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
				accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
		}
	 
	 public void consultaRecuperacion(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			String cuentaPresupAut = Util.validaParametro(accionweb.getParameter("cuentaPresupAut"),"");
			int organizacion = Util.validaParametro(accionweb.getParameter("organizacion"), 0); //unidad - organizacion VS.28-12-2016
			String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
			int sedeLista = Util.validaParametro(accionweb.getParameter("sedeLista"), 0);
		
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			if(numDoc == 0)
				numDoc = Util.validaParametro(accionweb.getSesion().getAttribute("numDoc"), 0);
			if(numDoc > 0){
							
			//String cuentaPresup = = Util.validaParametro(accionweb.getParameter("cuentaPresup"),"");
			
			//long sede = Util.validaParametro(accionweb.getParameter("sede"), 0); VS.28-12-2016
			//String estadoVales = Util.validaParametro(accionweb.getParameter("estadoVales"),"");
			String nomIdentificador = "";
			long rutnum = 0;
			String dvrut = "";
			//System.out.println("numDoc: "+numDoc);
			//if(sede > 0) VS.28-12-2016
			//	accionweb.agregarObjeto("sedeSolicitud", sede);
			//if(!estadoVales.trim().equals(""))
			//	accionweb.agregarObjeto("estadoVales", estadoVales);
			Presw19DTO preswbean19DTO = new Presw19DTO();
			String titulo = "";
			String tituloDetalle1 = "";
			preswbean19DTO.setTippro("OD1"); //VS.28-12-2016 ODR - OD1
			preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
			preswbean19DTO.setDigide(dv);
			preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
		//	preswbean19DTO.setIdsoli("9999999");
		
			String readonly = "readonly";
						
	/*capturar los datos */
			switch (tipo) {
			case  2: // lista de solicitudes de recuperaci�n para consultar
			{	titulo = "Modifica ";
				tituloDetalle1 = "Modificar";
				readonly = "";
				break;
				}
			case 3: // lista de solicitudes de recuperaci�n para autorizar
				{
				titulo = "Autorizaci�n Responsable de la Cuenta";
				tituloDetalle1 = "Autorizar";	
				break;
				}
			case 4: // lista de solicitudes de recuperaci�n para consulta
				{
				titulo = "Consulta ";
				tituloDetalle1 = "Consultar";
				break;
				}
			case 5: // lista de solicitudes de recuperaci�n para Recepci�n de solicitudes de recuperaci�n en Finanzas
				{
				titulo = "Revisi�n de Documentaci�n";
				tituloDetalle1 = "Recepci�n";
				break;
				}
			case 6: // lista de solicitudes recepci�n en Finanzas
			{
			titulo = "Recepci�n en Finanzas";
			break;
			}
			case 7: // Documento Rechazado
			{
			titulo = "Documento Rechazado";
			break;
			}
			case 16: // lista de Recuperaciones para autorizar DAF
			{
			titulo = "Autorizaci�n DAF";
			accionweb.agregarObjeto("sedeLista",sedeLista);
			break;
			}
			case 17: // lista de Recuperaciones para autorizar sobregiro
			{
			titulo = "Autorizaci�n Sobregiro";
			break;
			}
			
			}
			List<Cocow36DTO>  listaSolicitudDocumento = new ArrayList<Cocow36DTO>();
			List<Cocow36DTO>  listaSolicitudDocumOrig = new ArrayList<Cocow36DTO>();
			
			List<Cocow36DTO>  listaSolicitudDistribucion = new ArrayList<Cocow36DTO>();
			List<Cocow36DTO>  listaHistorial = new ArrayList<Cocow36DTO>();
			long valorAPagar = 0;
			String identificadorInterno = "";	
			//sede = 0; VS.28-12-2016
			int identificador = 0; 
			
		    String nomcam = "";
		    long valnu1 = 0;
		    long valnu2 = 0;
		    String valalf = "";
		    String glosa1 = "";
		    String glosa2 = "";
		    String glosa3 = "";
		    String glosa4 = "";
		    String glosa5 = "";
		    String glosa6 = "";
		    String tipoPago = "";
		    String nombrePago = "";
		    String cuentaPresupMod = "";
		    String nivelAut = "";
		    //String nomSede = ""; VS.28-12-2016
		    int ind = 0;
		    String estadoFinal = "";
		    long fecha = 0;
		    long totalSolDistribucion = 0;
		    long totalSolDocumento = 0;
		  	List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
			CocowBean cocowBean = new CocowBean();
			//System.out.println("preswbean19DTO : "+ preswbean19DTO.getRutide()+"  - "+preswbean19DTO.getNumdoc()+"--- "+preswbean19DTO.getTippro()+"  --- "+preswbean19DTO.getFecmov());
			listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
			//System.out.println("listCocow36DTO : "+ listCocow36DTO.size() );
		    if(listCocow36DTO != null && listCocow36DTO.size() >0 ){
		    
		    for(Cocow36DTO ss: listCocow36DTO){
		   	
		    	if(ss.getNomcam().trim().equals("IDMEMO"))
	            	identificadorInterno = ss.getValalf();
	         	//if(ss.getNomcam().trim().equals("CODSUC")){ VS.28-12-2016
	            	//sede = ss.getValnu1(); 
	            //	nomSede = ss.getValalf();
	            //}
			    if(ss.getNomcam().trim().equals("RUTIDE") && tipo != 11)
	             	   rutnum = ss.getValnu1();
	            if(ss.getNomcam().trim().equals("DIGIDE") && tipo != 11)
	            	dvrut = ss.getValalf();
	            if(ss.getNomcam().trim().equals("NOMPAG") && tipo != 11)
	                nombrePago = ss.getValalf();
	 		   
		    	if(ss.getNomcam().trim().equals("DESCR1"))
		    		glosa1 = ss.getValalf();
		    	if(ss.getNomcam().trim().equals("DESCR2"))
		    		glosa2 = ss.getValalf();
		     	if(ss.getNomcam().trim().equals("DESCR3"))
		    		glosa3 = ss.getValalf();
		     	if(ss.getNomcam().trim().equals("DESCR4"))
		    		glosa4 = ss.getValalf();
		     	if(ss.getNomcam().trim().equals("DESCR5"))
		    		glosa5 = ss.getValalf();   
		     	if(ss.getNomcam().trim().equals("DESCR6"))
		    		glosa6 = ss.getValalf();
	            
	            if(ss.getNomcam().trim().equals("VALREC"))
	            	valorAPagar = ss.getValnu1();
	            if(ss.getNomcam().trim().equals("ESTADO"))
	            	estadoFinal = ss.getValalf();
	            
	            if(ss.getNomcam().trim().equals("DOCASO")){
	            	Cocow36DTO cocow36DTO = new Cocow36DTO();
	            	cocow36DTO.setNomcam(ss.getNomcam());
	            	cocow36DTO.setValnu1(ss.getValnu1());
	            	cocow36DTO.setValnu2(ss.getValnu2());
	            	cocow36DTO.setResval(ss.getResval()); // rut + dig + nombre
	            	cocow36DTO.setValalf(ss.getValalf());  // tipo + rubro + glosa
	            	listaSolicitudDocumento.add(cocow36DTO);
	            	listaSolicitudDocumOrig.add(cocow36DTO);
	            	totalSolDocumento += ss.getValnu2();
	           
	            }
	            
	            if(ss.getNomcam().trim().equals("CUENTA")){
	            	//Cocow36DTO cocow36DTO = new Cocow36DTO();
	            	//cocow36DTO.setNomcam(ss.getNomcam());
	            	//cocow36DTO.setValnu1(ss.getValnu1());
	            	//cocow36DTO.setValnu2(ss.getValnu2());
	              	//cocow36DTO.setResval(ss.getResval()); VS.04.01.2016
	            	//cocow36DTO.setValalf(ss.getValalf()); VS.04.01.2016
	            	//System.out.println(ss.getResval());
	            	//System.out.println(ss.getValalf());
	            	//cocow36DTO.setResval(ss.getResval().substring(0,6)); // VS.04.01.2016 codigo organizacion 
	            	//cocow36DTO.setValalf(ss.getValalf()); // VS.04.01.2016 descripcion. Organizacion
	            	cuentaPresupMod = ss.getResval().substring(0,6); // Cambiado
	            	nivelAut = ss.getAccion();
	            	if(cuentaPresupAut.trim().equals("")) cuentaPresupAut = cuentaPresupMod;	            	 
	            	//listaSolicitudDistribucion.add(cocow36DTO);
	            	//totalSolDistribucion +=  ss.getValnu2();
	            }
	         int i = 0;
	            if(ss.getNomcam().trim().equals("HISTORIAL")){
	            	i++;
	            	if(i==1)
	            		fecha = ss.getValnu1();
	            	Cocow36DTO cocow36DTO = new Cocow36DTO();
	            	cocow36DTO.setValnu1(ss.getValnu1()); // es la fecha, 
	            	cocow36DTO.setValalf(ss.getValalf());//nombre del funcionario
	            	cocow36DTO.setResval(ss.getResval()); // comentario
	            	//cocow36DTO.setNompro(ss.getResval().substring(26));//en caso de Resval="Autoriza" es la unidad 
	            	//cocow36DTO.setIndpro(ss.getResval().substring(16,25));// comentario 
	            	//cocow36DTO.setDesite(ss.getValalf()); // es el responsable
	            	//cocow36DTO.setDesuni(ss.getResval().substring(0,15)); // es la accion, INGRESO, MODIFICACION, AUTORIZACION, PAGO, AUTORIZA FINANZAS // Tipo Movimeinto
	              		            	
	               	listaHistorial.add(cocow36DTO);
	            }
	       
		    //	 }
		    }
		    if(tipo == 7 && !estadoFinal.trim().equals("REC.REV.FINANZA") && !estadoFinal.trim().equals("RECHAZADO UCP"))
				   accionweb.agregarObjeto("mensaje", "Esta Solicitud no ha sido Rechazada en la Revisi�n de Documentos." );
		    else {
				    //List<Presw18DTO> listSede = new ArrayList<Presw18DTO>(); VS.28-12-2016
				    //listSede = moduloValePagoB.getListaSede(accionweb.getReq());
				    //String comboSede = "<select name=\"sede\" id=\"sede\" class=\"estilo_Azul\" onChange=\"\" >"+
				    //                   "<option value=\"-1\" selected=\"selected\">Seleccione</option> ";
				  //  if(identificador == 1)
				    //for (Presw18DTO ss : listSede){
				    //	comboSede += "<option value=" + ss.getNummes() + ">" + ss.getDesuni() + "</option>";				    	
				    //}
				    //comboSede += "</select>";
				   
				    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
				    moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
				    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
				    accionweb.agregarObjeto("listaUnidades", listaUnidades);
				    
			      	/*for (Presw18DTO ss : listaUnidades ){	
			      		System.out.println("Acumum: " + ss.getAcumum());			      		
			      		System.out.println("Coduni: " + ss.getCoduni());
			      		System.out.println("Desite: " + ss.getDesite());			      		
			      		System.out.println("Desuni: " + ss.getDesuni());			      		
			      		System.out.println("Diferm: " + ss.getDiferm());			      		
			      		System.out.println("Fecdoc: " + ss.getFecdoc());			      		
			      		System.out.println("Iddigi: " + ss.getIddigi());			      		
			      		System.out.println("Idsoli: " + ss.getIdsoli());			      		
			      		System.out.println("Indhay: " + ss.getIndhay());
			      		System.out.println("Indprc: " + ss.getIndprc());
			      		System.out.println("Indpro: " + ss.getIndpro());
			      		System.out.println("Itedoc: " + ss.getItedoc());
			      		System.out.println("Nompro: " + ss.getNompro());
			      		System.out.println("Nomtip: " + ss.getNomtip());
			      		System.out.println("Numdoc: " + ss.getNumdoc());
			      		System.out.println("Nummes: " + ss.getNummes());
			      		System.out.println("Preano: " + ss.getPreano());
			      		System.out.println("Presac: " + ss.getPresac());
			      		System.out.println("Presum: " + ss.getPresum());
			      		System.out.println("Rutide: " + ss.getRutide());
			      		System.out.println("Tipmov: " + ss.getTipmov());
			      		System.out.println("Tippro: " + ss.getTippro());
			      		System.out.println("Usadac: " + ss.getUsadac());
			      		System.out.println("Usadom: " + ss.getUsadom());
			      		System.out.println("Valdoc: " + ss.getValdoc());
						}	*/				    
				    List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
				    listaTipoDocumento = moduloRecuperacionGastoB.getTipoDocumento();
				    accionweb.agregarObjeto("listaTipoDocumento", listaTipoDocumento);
				    
					/*Item items = new Item();
					Collection<ItemDTO> lista = (Collection<ItemDTO>) items.todos_items();
					List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
					for(ItemDTO pp: lista){
						//System.out.println("incren: "+ pp.getIncren()+" "+ pp.getNomite()+"  "+ pp.getItepre());
						if(pp.getIncren().trim().equals("S"))
							listaItem.add(pp);
						
					}
					accionweb.agregarObjeto("listaItem", listaItem);*/
					//	este acceso cambia a partir de 27/01/2015 pr instrucciones de correo PP accediendo a partir de ahora a la tabla cocof139 en lugar de cocof23
					List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
					listaItem = moduloRecuperacionGastoB.getItemCocof139B();
					
					accionweb.agregarObjeto("listaItem", listaItem);
					accionweb.agregarObjeto("listaItemRubro", listaItem);							
				    //accionweb.agregarObjeto("listSede", listSede); VS.28-12-2016   
				 
				  
				    accionweb.agregarObjeto("estado",estado);
				    accionweb.agregarObjeto("organizacion",organizacion); // VS.28-12-2016 unidad - organizacion
				     
				    //accionweb.agregarObjeto("numVale",String.valueOf(numDoc));
				    if(rutnum == 0)
				    	 accionweb.agregarObjeto("identificador","1");
				    else
				    	accionweb.agregarObjeto("identificador","2");
				    
				    accionweb.agregarObjeto("cuentaPresupMod", cuentaPresupMod);
				    accionweb.agregarObjeto("cuentaPresupNombre", cuentaPresupMod+" - "+ moduloRecuperacionGastoB.getNomOrganizacion(cuentaPresupMod));
				    accionweb.agregarObjeto("identificadorInterno", identificadorInterno);
				    accionweb.agregarObjeto("nomIdentificador",nomIdentificador);
				    //accionweb.agregarObjeto("sede", sede); VS.28-12-2016
				    //accionweb.agregarObjeto("nomSede", nomSede);
				    accionweb.agregarObjeto("rutnum",rutnum);// rut a pago
				    accionweb.agregarObjeto("dvrut",dvrut);
				    accionweb.agregarObjeto("nombrePago", nombrePago);
				    accionweb.agregarObjeto("glosa1",glosa1);
			    	accionweb.agregarObjeto("glosa2",glosa2);
			    	accionweb.agregarObjeto("glosa3",glosa3);
			    	accionweb.agregarObjeto("glosa4",glosa4);
			    	accionweb.agregarObjeto("glosa5",glosa5);
			    	accionweb.agregarObjeto("glosa6",glosa6);	   
				    accionweb.agregarObjeto("valorAPagar", valorAPagar);
				    accionweb.agregarObjeto("estadoFinal",estadoFinal);	
				    accionweb.agregarObjeto("fechaSolicitud", fecha);
				    //accionweb.agregarObjeto("totalSolDistribucion", totalSolDistribucion);
				    accionweb.agregarObjeto("totalSolDocumento", totalSolDocumento);
				    accionweb.agregarObjeto("numDoc", numDoc);
					//System.out.println("listaUnidades: "+listaUnidades);
				    if(listaSolicitudDocumento != null && listaSolicitudDocumento.size() > 0){
				    	accionweb.agregarObjeto("listaSolicitudDocumento", listaSolicitudDocumento);
				    	accionweb.getSesion().setAttribute("listaSolicitudDocumento", listaSolicitudDocumento);
				    	accionweb.agregarObjeto("listaSolicitudDocumOrig", listaSolicitudDocumOrig);
				    	accionweb.getSesion().setAttribute("listaSolicitudDocumOrig", listaSolicitudDocumOrig);
				    }
				    //if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
				    //	accionweb.agregarObjeto("listaSolicitudDistribucion", listaSolicitudDistribucion);
				    //   	accionweb.getSesion().setAttribute("listaSolicitudDistribucion", listaSolicitudDistribucion);
				    //  	for (Cocow36DTO ss : listaSolicitudDistribucion ){
			      	//	System.out.println("Accion: " + ss.getAccion());
			      	//	System.out.println("Accion: " + ss.getIdsoli());
			      	//	System.out.println("Accion: " + ss.getNomcam());
			      	//	System.out.println("Accion: " + ss.getResval());
			      	//	System.out.println("Accion: " + ss.getRutusu());
			      	//	System.out.println("Accion: " + ss.getTiping());
			      	//	System.out.println("Accion: " + ss.getValalf());
			      	//	System.out.println("Accion: " + ss.getValnu1());
			      	//	System.out.println("Accion: " + ss.getValnu2());			      		
					//	}	
				    //}
				    /* VS.13012017.nuevo */
				     boolean agrega2 = true;
				     long valorResumen = 0;
				     String muestraSaldo = "";
				      	for (Cocow36DTO ss1 : listaSolicitudDocumento ){
	
							if (listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0)
							{
								for (Cocow36DTO ss2 : listaSolicitudDistribucion)
								{
								    if (ss2.getResval().trim().equals(ss1.getValalf().substring(3,9)))
									{	
									  agrega2 = false;
									  valorResumen = ss2.getValnu2() + ss1.getValnu2();
									  ss2.setValnu2(valorResumen);
		 																							//org.				rubro
									  long saldoCuenta =  moduloValePagoB.getBuscaSaldo(cuentaPresupMod,ss1.getValalf().substring(3,9));
									  NumberTool numbertool = new NumberTool();
									  muestraSaldo = String.valueOf(numbertool.format(saldoCuenta));
										 
									  if( valorResumen > saldoCuenta){
										  accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" >Este valor EXCEDE el saldo de la cuenta " + cuentaPresupMod + ", que es de: $ " + moduloRecuperacionGastoB.formateoNumeroSinDecimales(Double.parseDouble(saldoCuenta+""))+"</font> .");
									      muestraSaldo = "<font class=\"Estilo_Rojo\" >" + muestraSaldo + "</font>";
										  ss2.setTiping(muestraSaldo);
									   }
									}		
								}
									//org.				rubro
								long saldoCuenta =  moduloValePagoB.getBuscaSaldo(cuentaPresupMod,ss1.getValalf().substring(3,9));
								NumberTool numbertool = new NumberTool();
								muestraSaldo = String.valueOf(numbertool.format(saldoCuenta));								
								if( ss1.getValnu2() > saldoCuenta){
									accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" >Este valor EXCEDE el saldo de la cuenta " + cuentaPresupMod + ", que es de: $ " + moduloRecuperacionGastoB.formateoNumeroSinDecimales(Double.parseDouble(saldoCuenta+""))+"</font> .");
									 muestraSaldo = "<font class=\"Estilo_Rojo\" >" + muestraSaldo + "</font>";
								 }								
								listaSolicitudDistribucion = (List<Cocow36DTO>) listaSolicitudDistribucion;
							} 
							else
							{							
							 // System.out.println("Accion: " + ss1.getValalf());                                             //org.				rubro
							  long saldoCuenta =  moduloValePagoB.getBuscaSaldo(cuentaPresupMod,ss1.getValalf().substring(3,9));
							  
							  NumberTool numbertool = new NumberTool();
							  muestraSaldo = String.valueOf(numbertool.format(saldoCuenta));
								
							  listaSolicitudDistribucion = new ArrayList<Cocow36DTO>();
								if( ss1.getValnu2() > saldoCuenta){
									accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" >Este valor EXCEDE el saldo de la cuenta " + cuentaPresupMod + ", que es de: $ " + moduloRecuperacionGastoB.formateoNumeroSinDecimales(Double.parseDouble(saldoCuenta+""))+"</font> .");
									 muestraSaldo = "<font class=\"Estilo_Rojo\" >" + muestraSaldo + "</font>";
								 }	
							}	 
							if(agrega2)
							{
							 Cocow36DTO cocow36DTO = new Cocow36DTO();
							 cocow36DTO.setNomcam("CUENTA");
							 //if (tipo == 5) cocow36DTO.setResval(ss1.getValalf().substring(4,10));
							 //else cocow36DTO.setResval(ss1.getValalf().substring(3,9)); // ojo... modificacion ver
							 cocow36DTO.setResval(ss1.getValalf().substring(3,9));
							 cocow36DTO.setValnu2(ss1.getValnu2());
							 cocow36DTO.setValalf(cuentaPresupMod.trim());
							 cocow36DTO.setTiping(muestraSaldo);
					    	 cocow36DTO.setAccion(nivelAut);
							 listaSolicitudDistribucion.add(cocow36DTO);	    		
							}
							agrega2 = true;
						}	
				    		
				      	/*
				      	 System.out.println("Accion: " + ss.getAccion());
				      		System.out.println("Accion: " + ss.getIdsoli()); 
				      		System.out.println("Accion: " + ss.getNomcam());
				      		System.out.println("Accion: " + ss.getResval());
				      		System.out.println("Accion: " + ss.getRutusu());
				      		System.out.println("Accion: " + ss.getTiping());
				      		System.out.println("Accion: " + ss.getValalf());
				      		System.out.println("Accion: " + ss.getValnu1());
				      		System.out.println("Accion: " + ss.getValnu2());
				      	 */
				    	if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
				    		for (Cocow36DTO ss : listaSolicitudDistribucion){
				    			totalSolDistribucion = totalSolDistribucion + ss.getValnu2();	 
				    		}
				    	}	   
				    	 accionweb.agregarObjeto("totalSolDistribucion", totalSolDistribucion);
				      	if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
							accionweb.agregarObjeto("listaSolicitudDistribucion", listaSolicitudDistribucion);
						  	accionweb.getSesion().setAttribute("listaSolicitudDistribucion", listaSolicitudDistribucion);
						}				      	
				    /*fin nuevo*/
					
				    if(listaHistorial != null && listaHistorial.size() > 0){
				    	accionweb.agregarObjeto("listaHistorial", listaHistorial);
				       	accionweb.getSesion().setAttribute("listaHistorial", listaHistorial);
				    }
				   	accionweb.agregarObjeto("hayDatoslista", "1");     
			      	if(tipo != 7)
			      		accionweb.agregarObjeto("read",readonly);
		    }
		
		    } else { 
		    	accionweb.agregarObjeto("mensaje", "No existe solicitud "+titulo+"con el n�mero " + numDoc);
		    	if(tipo == 4){
		        accionweb.agregarObjeto("pagina", "recMuestraLista.vm");   
		       // tipo = 4;
		        this.cargaAutorizaRecuperacion(accionweb);
		    	}
		    }

	      	/*for (Cocow36DTO ss : listCocow36DTO ){	
	      		System.out.println("Accion: " + ss.getAccion());
	      		System.out.println("Idsoli: " + ss.getIdsoli());
	      		System.out.println("Nomcam: " + ss.getNomcam());
	      		System.out.println("Resval: " + ss.getResval());
	      		System.out.println("Rutusu: " + ss.getRutusu());
	      		System.out.println("Tiping: " + ss.getTiping());
	      		System.out.println("Valalf: " + ss.getValalf());
	      		System.out.println("Valnu1: " + ss.getValnu1());
	      		System.out.println("Valnu2: " + ss.getValnu2());
				}*/	
		 	accionweb.agregarObjeto("titulo", titulo);
	      	accionweb.agregarObjeto("cuentaPresupAut",cuentaPresupAut);
	      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);	    
			}
	      	
		    accionweb.agregarObjeto("esRecuperacionGastoB", "1");
	      	accionweb.agregarObjeto("opcion", tipo);
	     
	      	
	    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	    	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
				accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
	     // 	System.out.println("termina consulta ");	
		}
	 
	 
	 
	 
	 public void consultaRechazada(AccionWeb accionweb) throws Exception {
		 this.consultaRecuperacion(accionweb);
		 //if(accionweb.getParameter("estadoFinal"))
		     
		}
	 public void imprimeConsulta(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		
			long valorAPagar = 0;
			long rutnum = 0;
			String dvrut = "";
			String identificadorInterno = "";	
			//long sede = 0; VS.04-01-2016.
			int identificador = 0; 
			
		    String nomcam = "";
		    long valnu1 = 0;
		    long valnu2 = 0;
		    String valalf = "";
		    String glosa1 = "";
		    String glosa2 = "";
		    String glosa3 = "";
		    String glosa4 = "";
		    String glosa5 = "";
		    String glosa6 = "";
		    String nomOrganizacion = "";
		    String estadoFinal = "";
		    //String nomSede = ""; VS.04.01.2016
		    String nombrePago = "";
		    String tipoDocumento = "";
		    String nomRubro = "";
		    
		    List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
		    listaTipoDocumento = moduloRecuperacionGastoB.getTipoDocumento();
		    
			/*Item items = new Item();
			Collection<ItemDTO> lista = (Collection<ItemDTO>) items.todos_items();
			List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
			for(ItemDTO pp: lista){
				//System.out.println("incren: "+ pp.getIncren()+" "+ pp.getNomite()+"  "+ pp.getItepre());
				if(pp.getIncren().trim().equals("S"))
					listaItem.add(pp);				
			}*/
			//	este acceso cambia a partir de 27/01/2015 pr instrucciones de correo PP accediendo a partir de ahora a la tabla cocof139 en lugar de cocof23
			List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
			listaItem = moduloRecuperacionGastoB.getItemCocof139B();
			accionweb.agregarObjeto("listaItem", listaItem);
		    
			Presw19DTO preswbean19DTO = new Presw19DTO();
			String titulo = "";
			String tituloDetalle1 = "";
			preswbean19DTO.setTippro("OD1"); // VS.04.01.2016 ODR - OD1
			preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
			preswbean19DTO.setDigide(dv);
			preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
			
			Vector vecSolicitudDocumento = new Vector();	
			Vector vecSolicitudDistribucion = new Vector();	
			Vector vecHistorial = new Vector();	
			List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
			CocowBean cocowBean = new CocowBean();
			listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
			 if(listCocow36DTO != null && listCocow36DTO.size() >0 ) {
				   for(Cocow36DTO ss: listCocow36DTO){
					  // System.out.println("1: " + ss.getAccion());
		            //	System.out.println("2: " + ss.getIdsoli());
		            //	System.out.println("3: " + ss.getNomcam());
		            //	System.out.println("4: " + ss.getResval());
		            //	System.out.println("5: " + ss.getRutusu());
		            //	System.out.println("6: " + ss.getTiping());
		            //	System.out.println("7: " + ss.getValalf());
		            //	System.out.println("8: " + ss.getValnu1());
		            //	System.out.println("9: " + ss.getValnu2());
		            	if(ss.getNomcam().trim().equals("IDMEMO"))
			            	identificadorInterno = ss.getValalf();
			         	if(ss.getNomcam().trim().equals("CODSUC")){
			            	//sede = ss.getValnu1(); VS.04.01.2016
			            	//nomSede = ss.getValalf(); VS.04.01.2016
			            }
			            if(ss.getNomcam().trim().equals("RUTIDE") )
			             	   rutnum = ss.getValnu1();
			            if(ss.getNomcam().trim().equals("DIGIDE") )
			            	dvrut = ss.getValalf();
			            if(ss.getNomcam().trim().equals("NOMPAG") )
			                nombrePago = ss.getValalf();
			 		   
				    	if(ss.getNomcam().trim().equals("DESCR1"))
				    		glosa1 = ss.getValalf();
				    	if(ss.getNomcam().trim().equals("DESCR2"))
				    		glosa2 = ss.getValalf();
				     	if(ss.getNomcam().trim().equals("DESCR3"))
				    		glosa3 = ss.getValalf();
				     	if(ss.getNomcam().trim().equals("DESCR4"))
				    		glosa4 = ss.getValalf();
				     	if(ss.getNomcam().trim().equals("DESCR5"))
				    		glosa5 = ss.getValalf();   
				     	if(ss.getNomcam().trim().equals("DESCR6"))
				    		glosa6 = ss.getValalf();
			            
			            if(ss.getNomcam().trim().equals("VALREC"))
			            	valorAPagar = ss.getValnu1();
			            if(ss.getNomcam().trim().equals("ESTADO"))
			            	estadoFinal = ss.getValalf();
			            
			            if(ss.getNomcam().trim().equals("DOCASO")){
			            	for(Presw18DTO pp: listaTipoDocumento){
							    if(ss.getValalf().substring(0,3).equals(pp.getTipmov().trim()))	
							    	tipoDocumento = pp.getDesuni().trim();
			            	}
			            	for(ItemDTO pp: listaItem){
			            //		System.out.println(ss.getValalf());
			            //		System.out.println("ss.getValalf().substring(3,9): "+ss.getValalf().substring(3,9) +"   pp.getIncren(): "+ pp.getIncren());
							    //if( Integer.parseInt(ss.getValalf().substring(3,9)) == Integer.parseInt(pp.getItepre()+"")) // VS.04.01.2017	
			            		if (ss.getValalf().substring(3,9).equals(pp.getIncren().trim()))
							    	nomRubro = pp.getNomite();
			            	}
			            	Vector vec = new Vector();
			            	//System.out.println(ss.getResval());
			            	vec.addElement(ss.getResval().substring(9).trim()); // numero de solicitud
			            	vec.addElement(tipoDocumento); // tipoDocumento
			               	vec.addElement(moduloValePagoB.formateoNumeroSinDecimales(Double.parseDouble(ss.getResval().substring(0, 8))) + "-" + ss.getResval().substring(8,9)); // rut del documento
			               	vec.addElement(ss.getValnu1()); //iva
			               	vec.addElement(ss.getValnu2()); // total documento
			               	vec.addElement(nomRubro);  // nomRubro
			               	vec.addElement(ss.getValalf().substring(9).trim()); // glosa
			               	vecSolicitudDocumento.addElement(vec);
			            	
			            }
			            
			         		  
			            if(ss.getNomcam().trim().equals("CUENTA")){
			            	Vector vec = new Vector();
			            	nomOrganizacion = ss.getResval().substring(0,6) + "-"+ ss.getValalf();
			            	vec.addElement(ss.getResval().substring(0,6) + "-"+ ss.getValalf()); // cuenta ???? VS.04.01.2017 getValnu1 - getResval
			            	vec.addElement(ss.getValnu2()); // valor???
			               	vecSolicitudDistribucion.addElement(vec);
			            	
			            }
			 
				        
			            if(ss.getNomcam().trim().equals("HISTORIAL")){
			            	String fecha = "";
			            	String diaFecha = "";
			            	String mesFecha = "";
			            	String annoFecha = "";
			            	String valor = "";
			            	String valor2 = "";
			            	if(ss.getValnu1()> 0 ) {
			    				fecha = ss.getValnu1() + "";
			    				diaFecha = fecha.substring(6);
			    				mesFecha = fecha.substring(4,6);
			    				annoFecha = fecha.substring(0,4);
			    				valor = diaFecha+"/"+mesFecha+"/"+annoFecha;   
			            	} else {
			    				valor = ss.getValnu1()+"";
			            	} 
			            	if (ss.getResval().trim().equals("AUTORIZACION"))// se ocupa de la 1-15 el resto se ocupa la observacion del rechazo
			            	   valor +=  " por " + ss.getValalf() + " Cuenta " + ss.getValnu2() ;
			            	else
			            		valor += " por " + ss.getValalf() + " " + ss.getResval().substring(15);
			            	
			            	Vector vec3 = new Vector();
			            	vec3.addElement(ss.getResval().substring(0, 15)); // es la accion, INGRESO, MODIFICACION, AUTORIZACION, PAGO, AUTORIZA FINANZA				        
			            	vec3.addElement(valor); // es la fecha, 
			            	vecHistorial.addElement(vec3);
			            }
			            if(ss.getNomcam().trim().equals("ESTADO"))
			            	estadoFinal = ss.getValalf();
				    // }
				    }	
				    //List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
				    //listSede = moduloValePagoB.getListaSede(accionweb.getReq());
				    //for( Presw18DTO ss:listSede){
				    //	if(ss.getNummes() == sede)
				    //		nomSede = ss.getDesuni();
				    //}
				 
	               accionweb.getSesion().setAttribute("vecSolicitudDocumento", vecSolicitudDocumento);
	               accionweb.getSesion().setAttribute("vecSolicitudDistribucion", vecSolicitudDistribucion);
	               accionweb.getSesion().setAttribute("vecHistorial", vecHistorial);
	               accionweb.getSesion().setAttribute("nombrePago", nombrePago);
	               accionweb.getSesion().setAttribute("glosa1", glosa1);
	               accionweb.getSesion().setAttribute("glosa2", glosa2);
	               accionweb.getSesion().setAttribute("glosa3", glosa3);
	               accionweb.getSesion().setAttribute("glosa4", glosa4);
	               accionweb.getSesion().setAttribute("glosa5", glosa5);
	               accionweb.getSesion().setAttribute("glosa6", glosa6);
	               accionweb.getSesion().setAttribute("nomOrganizacion",nomOrganizacion);
	               if(rutnum > 0)
	            	   accionweb.getSesion().setAttribute("rutnum", moduloValePagoB.formateoNumeroSinDecimales(Double.parseDouble(rutnum+"")) + "-" + dvrut);
	               //accionweb.getSesion().setAttribute("nomSede", nomSede);
	               accionweb.getSesion().setAttribute("valorAPagar", moduloValePagoB.formateoNumeroSinDecimales(Double.parseDouble(valorAPagar+"")));
	               accionweb.getSesion().setAttribute("identificadorInterno", identificadorInterno);
	               accionweb.getSesion().setAttribute("estadoFinal", estadoFinal);
	               accionweb.getSesion().setAttribute("numDoc", String.valueOf(numDoc));
			 }
		}
		public void limpiaSimulador(AccionWeb accionweb) throws Exception {
			if (accionweb.getSesion().getAttribute("listaUnidad") != null)
				accionweb.getSesion().setAttribute("listaUnidad", null);
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			int rutOrigen  = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen") +"","0"));
			String nomSimulacion = "";
			if(accionweb.getSesion().getAttribute("opcionMenu") != null)
				accionweb.getSesion().removeAttribute("opcionMenu");
			
			/*hacer este rut rutUsuario*/
			if(rutOrigen != 0) // lo vuelve a lo original
			{
				rutUsuario = rutOrigen ;
				rutOrigen = 0;
			}
			 
			
			accionweb.getSesion().setAttribute("rutUsuario", rutUsuario);
			accionweb.getSesion().setAttribute("rutOrigen", rutOrigen);
			accionweb.getSesion().setAttribute("nomSimulacion", nomSimulacion);
			accionweb.agregarObjeto("rutOrigen", rutOrigen);
			accionweb.agregarObjeto("nomSimulacion", nomSimulacion);
			
		}
		
		 synchronized public void ejecutaAccion(AccionWeb accionweb) throws Exception {
				int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
				int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
				String estadoVales = Util.validaParametro(accionweb.getParameter("estadoVales"),"");
				String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
				int sedeLista = Util.validaParametro(accionweb.getParameter("sedeLista"), 0);
				
				switch (tipo){
				case 2: {// actualiza
					this.actualizaRecuperacionGasto(accionweb);
					break;
				}
				case 3: case 5: case 6: case 18: case 19: case 20:// autoriza responsable de cuenta, revision de documento, UCP, DIPRES, MECESUP
					{
					this.autorizaRecuperacion(accionweb);
					break;
				}
				case 8: case 9: case 21: case 22: case 23:{// rechaza Recepci�n
					this.rechazaAutSolicitud(accionweb);
				break;
					
				}
				case 16: {
					this.autorizaDAFRecuperacion(accionweb);
					break;
				}
				case 17: {
					this.autorizaSobregiroRecuperacion(accionweb);
					break;
				}
			
				}
				if(!estadoVales.trim().equals(""))
					accionweb.agregarObjeto("estadoVales", estadoVales);
				if(!estado.trim().equals(""))
					accionweb.agregarObjeto("estado", estado);
				if(tipo != 11)
				         this.cargarRecuperacion(accionweb);
				if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
		      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
		      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
		      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
		      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
		      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
		      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
					accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		    	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
					accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
			}	
		 synchronized public void rechazaAutSolicitud(AccionWeb accionweb) throws Exception {
				int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
				int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);	
				String glosa = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "","");
				int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
				String texto = "";
				String tipCue = "";
				
				String nomTipo = "";
				switch(tipo){
				case 3: case 8: {// rechazo responsable de cuenta
						nomTipo = "RR1"; // RRR - RR1
						texto = "de la Autorizaci�n";
						//accionweb.agregarObjeto("tipo", 3);
						break;
						}
				case 5: { // rechazo revisi�n documentaci�n
					nomTipo = "RS1"; // RSR - RS1
					texto = "de la Revisi�n de Documentaci�n";
					//accionweb.agregarObjeto("tipo", 5);
					break;
					}
				case 6: case 9: {// rechazo recepcion finanzas
					nomTipo = "RSS";
					texto = "de la Recepci�n";
					//accionweb.agregarObjeto("tipo", 6);
					break;
					}
				case 18: case 21: { // rechazo mecesup
					nomTipo = "RER";
					tipCue = "Y";
					texto = "de la Autorizaci�n de MECESUP ";
					//accionweb.agregarObjeto("tipo", 18);
					break;
					}
				case 19: case 22: { // rechazo  UCP
					nomTipo = "RER";
					texto = "de la Autorizaci�n de UCP";
					tipCue = "X";
					//accionweb.agregarObjeto("tipo", 19);
					break;
					}
				case 20: case 23: { // rechazo DIPRES
					nomTipo = "RER";
					texto = "de la Autorizaci�n de DIPRES";
					tipCue = "Z";
					//accionweb.agregarObjeto("tipo", 3);
					break;
					}
				}
				try { 
				boolean graba = moduloRecuperacionGastoB.saveRechaza(numdoc, rutUsuario,dv,nomTipo, glosa, tipCue);
				
			    if(graba)
			    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el rechazo " +  texto);
			    else
			    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo " + texto);
			    
				Thread.sleep(500);
				if(tipo != 22)
			          this.cargaAutorizaRecuperacion(accionweb);
			    
			    
				} catch (Exception e) {
					accionweb.agregarObjeto("mensaje", "No guardado rechazaAutSolicitud.");
					accionweb.agregarObjeto("exception", e);
					e.printStackTrace();
				}
				if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
		      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
		      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
		      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
		      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
		      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
		      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
					accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		    	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
					accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
				
			}
		 
		 synchronized public void rechazaAutDAFSolicitud(AccionWeb accionweb) throws Exception {
				int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
				int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);	
				String glosa = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "","");
				int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
				int sedeLista = Util.validaParametro(accionweb.getParameter("sedeLista"), 0);	
				try { 
					boolean graba = moduloRecuperacionGastoB.saveRechaza(numdoc, rutUsuario,dv,"RSA", glosa, "");
				
			    if(graba)
			    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el rechazo de la autorizaci�n");
			    else
			    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la autorizaci�n");
			    
				     
				} catch (Exception e) {
					accionweb.agregarObjeto("mensaje", "No guardado rechazaAutDAFRecuperaci�nGasto.");
					accionweb.agregarObjeto("exception", e);
					e.printStackTrace();
				}
				Thread.sleep(500);
				this.cargaAutorizaRecuperacion(accionweb);
		
			}
		 
		 synchronized public boolean revisaSaldoOrganizacion(List<Cocow36DTO> listaSolicitudes, String estado) throws Exception {
			 	long saldoCuenta = 0;
			 	boolean sobrepasa = false;
			 	boolean compara = false;
			 	if (listaSolicitudes != null && listaSolicitudes.size() > 0){
					for (Cocow36DTO ss2 : listaSolicitudes){
					 // System.out.println(ss2.getValalf().trim()+"-"+ss2.getResval());
					  if (estado.trim().equals("A")) {
						// System.out.println(ss2.getAccion().trim()); 
						if (ss2.getAccion().trim().equals("A")) {
							saldoCuenta =  moduloValePagoB.getBuscaSaldo(ss2.getValalf().trim(),ss2.getResval());
							compara = true;
						}
						else compara = false;
					  }
					  else {
						  saldoCuenta =  moduloValePagoB.getBuscaSaldo(ss2.getValalf().trim(),ss2.getResval());
						  compara = true;
					  }
					  
					  if (compara) {
						//  System.out.println(ss2.getValnu2() + "-" + saldoCuenta); 
						  if( ss2.getValnu2() > saldoCuenta) {
							  sobrepasa = true;
							  break;
						  }	  
					  }
					}
				}
				return sobrepasa;
			 }
		 
		 synchronized public boolean revisaAutOrganizacion(List<Cocow36DTO>  listaSolicitudes, int numdoc, int rutUsuario, String dv, boolean modifica) throws Exception {
			 	boolean aut = true;
			 	String estado = "";
				if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				  if (modifica) {	
				      for (Cocow36DTO ss : listaSolicitudes) {	
				     	  if (ss.getAccion().trim().equals("A")) {
				    		  aut = moduloValePagoB.saveEstadoCuenta(numdoc,ss.getValalf().trim(),rutUsuario,dv,"A");
				    		  if (!aut) break;
				    	  }	  
					  }
			 	 }
				 else {
					   for (Cocow36DTO ss : listaSolicitudes) {	
						   estado = moduloRecuperacionGastoB.getEstadoCOCOF101B(numdoc,ss.getValalf().trim());
						   if (!estado.trim().equals("A")) {
					   		  aut = false;
					   		  break;
					   	  } 	  
					   }
			 	 }
				}	  
			   return aut;
		 	}	
		
		 synchronized public void autorizaRecuperacion(AccionWeb accionweb) throws Exception {
				int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
				int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
				if(numdoc == 0)
					numdoc = Util.validaParametro(accionweb.getSesion().getAttribute("numDoc"), 0);
				if(numdoc > 0){// hasta aca
				int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
				String cuentaPresupAut = Util.validaParametro(accionweb.getParameter("cuentaPresupAut"), "");
				
				//long fechaRecup = Util.validaParametro(accionweb.getParameter("fechaRecup"), 0);
				String nomTipo = "";
				String tipCue = "";
				String nomAut = "";
				String texto = "";
				String mensaje = "";
				//boolean aut  = true; 
				switch(tipo){
				case 3: {   // autorizaci�n responsable de cuenta
					nomTipo = "AR1"; // ARR - AR1
					nomAut = " de Responsable de Cuenta. ";
					texto = " la autorizaci�n ";
					break;
				}
				case 5: {// autoricaci�n revisi�n documento
					nomTipo = "AS1"; // ASF - AS1
					nomAut = " Revisi�n de Documento. ";
					break;
				}
				case 6: {
					nomTipo = "ASR"; // ASR -ASR
					nomAut = " Recepci�n en Finanzas. ";
					break;
				}
				case 18: { // autoriza MECESUP
					
					nomTipo = "AER"; // AER -AER
					tipCue = "Y";
					nomAut = " de MECESUP VRA. ";
					texto = " la autorizaci�n ";
					break;
				}
				case 19: {// AUTORIZA UCP
					nomTipo = "AER"; // AER -AER
					tipCue = "X";
					nomAut = " de UCP. ";
					texto = " la autorizaci�n ";
					break;
				}
				case 20: { // AUTORIZA DIPRES
					nomTipo = "AER"; //AER -AER
					tipCue = "Z";
					nomAut = " de MECESUP DIPRES. ";
					texto = " la autorizaci�n ";
					break;
				}
				}
								
				/* inicio*/
				consultaRecuperacion(accionweb);	
				
				List<Cocow36DTO>  listaSolicitudDoc = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumento");
				List<Cocow36DTO>  listaSolicitudRes = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
				
			   if (tipo == 3) {
				boolean sobrepasaOrg = this.revisaSaldoOrganizacion(listaSolicitudRes,"A"); // Solo las de los autorizadores de sus org.
				if (!sobrepasaOrg){
				  try {	
					boolean aut = moduloRecuperacionGastoB.saveAutoriza(numdoc, rutUsuario,dv,nomTipo,tipCue);
					Thread.sleep(1500);
					if (aut) {
						aut = revisaAutOrganizacion(listaSolicitudRes,numdoc,rutUsuario,dv,false); // se revisan si todas las org del vale estan autorizadas
						Thread.sleep(1500);
						if (aut) {
							sobrepasaOrg = this.revisaSaldoOrganizacion(listaSolicitudRes,"");
							if (sobrepasaOrg) // MB para recepcion masiva 04/05/2018 accionweb.agregarObjeto("mensaje", "No se puede Autorizar la Recuperaci�n de gastos por Organizaciones sin saldo suficiente.");
								mensaje = "No se puede Autorizar la Recuperaci�n de gastos por Organizaciones sin saldo suficiente N� " + numdoc+".";
							else // MB para recepcion masiva 04/05/2018 accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n de Recuperaci�n de Gastos N� " + numdoc+".");
								mensaje = "Se grab� en forma exitosa la autorizaci�n de Recuperaci�n de Gastos N� " + numdoc+".";						}
						else // MB para recepcion masiva 04/05/2018 accionweb.agregarObjeto("mensaje", "Hay organizaciones de la Recuperaci�n de Gastos sin autorizar.");
							mensaje = "Hay organizaciones de la Recuperaci�n de Gastos sin autorizar N� " + numdoc+".";
					}
					else // MB para recepcion masiva 04/05/2018 accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n N� " + numdoc+"." ;
						mensaje = "Problemas en la grabaci�n de autorizaci�n N� " + numdoc+"." ;
					 
			    	} catch (Exception e) {
			    		// MB para recepcion masiva 04/05/2018 accionweb.agregarObjeto("mensaje", "Error al autorizar la Recuperaci�n de Gastos autorizaRecuperacion N� " + numdoc+".");
			    		mensaje =  "Error al autorizar la Recuperaci�n de Gastos autorizaRecuperacion N� " + numdoc+".";
						accionweb.agregarObjeto("exception", e);
						e.printStackTrace();
					}			
			   }
				else // MB para recepcion masiva 04/05/2018 accionweb.agregarObjeto("mensaje", "Organizaciones sin saldo suficiente.");
					mensaje = "Organizaciones sin saldo suficiente N� " + numdoc+".";
			  }
			else {
					boolean aut = moduloRecuperacionGastoB.saveAutoriza(numdoc, rutUsuario,dv,nomTipo,tipCue);
					Thread.sleep(1500);
					if (aut)  // MB para recepcion masiva 04/05/2018 accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n de la Recuperaci�n de Gastos N� " + numdoc +".");
						mensaje = "Se grab� en forma exitosa la autorizaci�n de la Recuperaci�n de Gastos N� " + numdoc +".";
					else // MB para recepcion masiva 04/05/2018 accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n" + nomAut+".");
						mensaje = "Problemas en la grabaci�n de autorizaci�n" + nomAut+" N� " + numdoc +".";
			  }	
		
				Thread.sleep(500);
			    this.cargaAutorizaRecuperacion(accionweb); 
			    if(!mensaje.trim().equals("")){
			    	accionweb.agregarObjeto("mensaje", mensaje);
			    	accionweb.getSesion().setAttribute("mensaje", mensaje);
			    }
				}
				if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
		      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
		      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
		      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
		      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
		      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
		      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
					accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		    	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
					accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
			}	
		 
		 synchronized public void autorizaDAFRecuperacion(AccionWeb accionweb) throws Exception {
				int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
				int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
				if(numdoc == 0)
					numdoc = Util.validaParametro(accionweb.getSesion().getAttribute("numDoc"), 0);
				if(numdoc > 0){// hasta aca
				int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
				String cuentaPresupAut = Util.validaParametro(accionweb.getParameter("cuentaPresupAut"), "");
				
				//long fechaRecup = Util.validaParametro(accionweb.getParameter("fechaRecup"), 0);
				int sobrepasa = 0;
				boolean estadoRecuperacion = false;
				consultaRecuperacion(accionweb);
				String mensaje = "";
								
				/* inicio*/
				
				List<Cocow36DTO>  listaSolicitudDoc = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumento");
				List<Cocow36DTO>  listaSolicitudRes = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
                
				boolean aut = false;
				boolean sobrepasaOrg = this.revisaSaldoOrganizacion(listaSolicitudRes,"");
				if (!sobrepasaOrg){
					aut = revisaAutOrganizacion(listaSolicitudRes,numdoc,rutUsuario,dv,false); // se revisan si todas las org de la recuperaci�n estan autorizadas
					Thread.sleep(1500);
					sobrepasa = 0;
					if (!aut) {
						  try {
						 	aut = actualizaConFinOrganizacion(listaSolicitudRes,numdoc,rutUsuario,dv); //se dejan en estado A las org de la recuperaci�n que estan ahora con saldo saldo
							Thread.sleep(1500);
							listaSolicitudRes = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
							if (!aut) // MB para recepcion masiva 04/05/2018 accionweb.agregarObjeto("mensaje", "Problemas al modificar estado de la Organizaci�n a Autorizada.");
								mensaje = "Problemas al modificar estado de la Organizaci�n a Autorizada N� " + numdoc +".";
						
						} catch (Exception e) {
							// MB para recepcion masiva 04/05/2018 accionweb.agregarObjeto("mensaje", "Error al actualizar estado a Autorizada autorizaDAFValePago.");
							mensaje = "Error al actualizar estado a Autorizada autorizaDAFValePago N� " + numdoc +".";
							accionweb.agregarObjeto("exception", e);
							e.printStackTrace();
						}	
					  }
					}	
				else { 
				 	aut = revisaAutOrganizacion(listaSolicitudRes,numdoc,rutUsuario,dv,false); // se revisan si todas las org del vale estan autorizadas
					Thread.sleep(1500);
					sobrepasa = 1;
					if (aut ) {
						try {
							aut = actualizaSinFinOrganizacion(listaSolicitudRes,numdoc,rutUsuario,dv); //se dejan en estado B las org del vale que estan sin saldo
							Thread.sleep(1500);
							listaSolicitudRes = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
							if (!aut) // MB para recepcion masiva 04/05/2018 accionweb.agregarObjeto("mensaje", "Problemas al modificar estado de la Organizaci�n a sin Financiamiento.");
								mensaje = "Problemas al modificar estado de la Organizaci�n a sin Financiamiento N� " + numdoc +".";
						} catch (Exception e) {
							// MB para recepcion masiva 04/05/2018 accionweb.agregarObjeto("mensaje", "Error al actualizar estado sin Financiamiento autorizaDAFRecuperaci�n.");
							mensaje = "Error al actualizar estado sin Financiamiento autorizaDAFRecuperaci�n  N� " + numdoc +".";
							accionweb.agregarObjeto("exception", e);
							e.printStackTrace();
					     }
					}	
				}
				
			    String rutDvIngresador = moduloRecuperacionGastoB.getRutOperacion(numdoc,"I");
			    String rutDvDAF = rutUsuario + dv;
			    
			    boolean existeGoremal =  moduloValePagoB.getExisteGoremal(rutDvIngresador);
				if (existeGoremal) 
					 existeGoremal = moduloValePagoB.getExisteGoremal(rutDvDAF);
				 
				if (existeGoremal) {
					boolean graba = moduloRecuperacionGastoB.saveAutoriza(numdoc, rutUsuario,dv,"ASA","");
					Thread.sleep(2000);
					if(graba) {
						 if (sobrepasa == 0) 	
							// MB para recepcion masiva 04/05/2018 accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n del Vale N� " + numdoc+".");
							 mensaje = "Se grab� en forma exitosa la autorizaci�n del Vale N� " + numdoc+".";
						 else // MB para recepcion masiva 04/05/2018 accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n del Vale N� " + numdoc + ", pero no se traspasa a BANNER por Organizaci�n sin saldo.");
							 mensaje = "Se grab� en forma exitosa la autorizaci�n del Vale N� " + numdoc + ", pero no se traspasa a BANNER por Organizaci�n sin saldo.";
						 estadoRecuperacion = true;
					 }
					 else {
						// MB para recepcion masiva 04/05/2018 accionweb.agregarObjeto("mensaje","No se grab� la autorizaci�n del Vale N� " + numdoc+".");
						mensaje ="No se grab� la autorizaci�n del Vale N� " + numdoc+".";
						estadoRecuperacion = false;
					}
				}
					 	
			   	//accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa " + texto + nomAut);
				//String rutdv,String numvale, String fecha_invoice, String fecha_pmt_due, String fecha_trans, String cod_doct, String datos	
				boolean continua = true;	
			  try {
				
				if (estadoRecuperacion && sobrepasa == 0 && existeGoremal) {	
				   String fecha = moduloRecuperacionGastoB.getDatoCOCOF102B(numdoc);
				  // String valor_vale = moduloRecuperacionGastoB.getValorDeRGT(numdoc);
				   String rutDvAutorizador = moduloRecuperacionGastoB.getRutOperacion(numdoc,"A"); // No se usa en procedimeitno 
					    		
				   Vector vec_datos = new Vector();
				   vec_datos = moduloRecuperacionGastoB.getDatosAS(numdoc);
				   String datos_parametro = "";
				   Vector vec = null;
				   String comen_vale = "";
				   String tip_doc   = "";
				   String desc_val  = "";
				   int rutPago      = 0;
				   String dvPago    = "";
				   String rutProd  = "";
				   String valor_vale = "";
				   String numdocDetalle = "";
				   String cod_servicio = "";
				   String codOrg = "";
				  
				   
					// verifica en iSeries primero si se envi� el registro a autorizzr y enviar a banner
				    if(moduloRecuperacionGastoB.getVerificaCOCOF132B(numdoc)){
				    	continua = false;				    	
				    }
				    if(continua){// verrifica registro existente en banner,  seg�n definici�n de Fernando Caceres correo 29/3/2019
					    mensaje = moduloRecuperacionGastoB.getVerificaRegistroBanner(vec_datos);
					    if(!mensaje.trim().equals("")){
					    		continua = false;				    		
				    }
				    } else mensaje = "Ya fue enviado a Banner desde Iseries.";
					   
				   if(continua){	    		 
				   for (int i = 0; i < vec_datos.size(); i++) {
					   /* if(i == 0){
					    	// verifica en iSeries primero si se envi� el registro a autorizzr y enviar a banner
					    	if(moduloRecuperacionGastoB.getVerificaCOCOF132B(numdoc)){
					    		continua = false;
					    		mensaje = "Documento est� ingresado en Banner por SIIF";
					    		break;
					    	}
					    
					    }
					    if(continua){  se elimina esta validacion se cambia por la de arriba*/ 
					    datos_parametro = "";
						vec = (Vector) vec_datos.get(i);
						comen_vale =  vec.get(6)+"";
						tip_doc    = vec.get(7)+""; //VS.20.12.2016
				        desc_val   = vec.get(8)+"";
				        rutPago    = Integer.parseInt(vec.get(9)+"");
				        dvPago     = vec.get(10)+"";
				        rutProd    = vec.get(12)+"";
				        valor_vale = String.valueOf(Integer.parseInt(vec.get(4)+"") - Integer.parseInt(vec.get(11)+""));
				        numdocDetalle = String.valueOf(Integer.parseInt(vec.get(13)+""));
				        cod_servicio =  vec.get(2)+"";
				        codOrg =  vec.get(1)+"";
			                    
					 /*   for (int y = 0; y < (vec.size()-1); y++) { // para que rescate hasta valval
					    	if (y == 13) datos_parametro = datos_parametro + vec.get(y) + "@"; // Cunado llegue a VALVAL agrgue @
					    	else if(y < 5 || y == 11)datos_parametro = datos_parametro + vec.get(y) + "&";
					    }*/
				        for (int y = 0; y < (vec.size()-1); y++) { // para que rescate hasta valval
					    	if (y == 11) datos_parametro = datos_parametro + vec.get(y) + "&" + moduloRecuperacionGastoB.getBancoOperacion(codOrg) + "@"; // Cunado llegue a VALor del impuesto agrgue @
					    	else if(y < 5)datos_parametro = datos_parametro + vec.get(y) + "&";
					    }
				 
						//System.out.println("datos_parametro " + datos_parametro); 
						//int error = moduloRecuperacionGastoB.saveCuentasPorPagar(rutUsuario, dv,numdoc, rutnum, dvrut, tipoBoleta,fecha,comen_vale,tip_val,desc_val,valor_vale,datos_parametro ); // VS.19.12.2016
						// 26/04/2017  MAA rutUsuario, dv se debiera sacar de la tabla de COCOF133B en este caso para que muestre rut de pago
						int error = moduloRecuperacionGastoB.saveCuentasPorPagar(rutPago, dvPago,numdoc, 0, "", tip_doc,fecha,comen_vale,tip_doc,desc_val,valor_vale,
								                                                 datos_parametro,cod_servicio,rutDvIngresador,rutDvAutorizador,rutProd, rutDvDAF,numdocDetalle,tipo); 
							    	
						if(error == -12) // MB para recepcion masiva 04/05/2018  accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n del archivo Historial del documento.");
							mensaje = "Problemas en la grabaci�n del archivo Historial del documento N� " + numdoc+".";
						else if(error == -13) // MB para recepcion masiva 04/05/2018  accionweb.agregarObjeto("mensaje", "Problemas en la actualizaci�n del estado del Documento.");
							mensaje =  "Problemas en la actualizaci�n del estado del Documento  N� " + numdoc+".";
						else if (error < 0) // MB para recepcion masiva 04/05/2018  accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n a Autorizar. C�digo Banner.");//B=Banner
							mensaje = "Problemas en la grabaci�n a Autorizar. C�digo Banner N� " + numdoc+".";
						else  // MB para recepcion masiva 04/05/2018  accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n con traspaso a Banner.");
							mensaje = "Se grab� en forma exitosa la autorizaci�n con traspaso a Banner N� " + numdoc+". C�digo : "+ moduloRecuperacionGastoB.getCodigoBanerRec();
						}
					//	}
				   }
				
				}
			  } catch (Exception e) {
				// MB para recepcion masiva 04/05/2018  accionweb.agregarObjeto("mensaje", "No guardado en BANNER autorizaDAFRecupercion.");
				    mensaje = "No guardado en BANNER autorizaDAFRecupercion. N� " + numdoc+".";
					accionweb.agregarObjeto("exception", e);
					e.printStackTrace();
				}
			  
			  if (!existeGoremal && continua) // MB para recepcion masiva 04/05/2018  accionweb.agregarObjeto("mensaje", "Existe alg�n funcionario ingresador o autoizador no ingresado en GOREMAL."); 
				  mensaje = "Existe alg�n funcionario ingresador o autorizador no ingresado en GOREMAL. N� " + numdoc+".";
			    Thread.sleep(500);
			    this.cargaAutorizaRecuperacion(accionweb); 
			    if(!mensaje.trim().equals("")){
			    	accionweb.agregarObjeto("mensaje", mensaje);
			    	accionweb.getSesion().setAttribute("mensaje", mensaje);
			    }
				}
				
				if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
		      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
		      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
		      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
		      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
		      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
		      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
					accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		    	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
					accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
			}
		 
		 synchronized public void autorizaSobregiroRecuperacion(AccionWeb accionweb) throws Exception {
				int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
				int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
				if(numdoc == 0)
					numdoc = Util.validaParametro(accionweb.getSesion().getAttribute("numDoc"), 0);
				if(numdoc > 0){// hasta aca
				int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
				String cuentaPresupAut = Util.validaParametro(accionweb.getParameter("cuentaPresupAut"), "");
				String mensaje = "";
				consultaRecuperacion(accionweb);
								
				/* inicio*/
				
				List<Cocow36DTO>  listaSolicitudDoc = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumento");
				List<Cocow36DTO>  listaSolicitudRes = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
             
				boolean aut = false;
				aut = revisaAutOrganizacion(listaSolicitudRes,numdoc,rutUsuario,dv,false); // se revisan si todas las org de la recuperaci�n estan autorizadas
				Thread.sleep(1500);
				if (!aut) {
					 try {
						 if (listaSolicitudRes != null && listaSolicitudRes.size() > 0){ 
							 for (Cocow36DTO ss : listaSolicitudRes) {
							    aut = moduloValePagoB.saveEstadoCuenta(numdoc,ss.getValalf().trim(),rutUsuario,dv,"A");
							    if (!aut) {
							    	// MB para recepcion masiva 04/05/2018  accionweb.agregarObjeto("mensaje", "Problemas al modificar estado de la Organizaci�n a Autorizada.");
							    	 mensaje = "Problemas al modificar estado de la Organizaci�n a Autorizada N� " + numdoc+".";
						  	    	 break;
						  	   	}   	
						    }
						 }
						} catch (Exception e) {
							// MB para recepcion masiva 04/05/2018  accionweb.agregarObjeto("mensaje", "Error al actualizar estado a Autorizada autorizaSobregiroRecuperacion.");
							mensaje = "Error al actualizar estado a Autorizada autorizaSobregiroRecuperacion. N� " + numdoc+".";
							accionweb.agregarObjeto("exception", e);
							e.printStackTrace();
						}	
				}
					//String rutdv,String numvale, String fecha_invoice, String fecha_pmt_due, String fecha_trans, String cod_doct, String datos	
			boolean continua = true; // MB agrega para verificar que no se registre nuevamente la misma recuperaci�n en banner
			try {
				  String fecha = moduloRecuperacionGastoB.getDatoCOCOF102B(numdoc);
				  // String valor_vale = moduloRecuperacionGastoB.getValorDeRGT(numdoc);
				  String rutDvIngresador = moduloRecuperacionGastoB.getRutOperacion(numdoc,"I");
				  String rutDvAutorizador = moduloRecuperacionGastoB.getRutOperacion(numdoc,"A");
				  String rutDvDAF = rutUsuario + dv;
					    		
				   Vector vec_datos = new Vector();
				   vec_datos = moduloRecuperacionGastoB.getDatosAS(numdoc);
				   String datos_parametro = "";
				   Vector vec = null;
				   String comen_vale = "";
				   String tip_doc   = "";
				   String desc_val  = "";
				   int rutPago      = 0;
				   String dvPago    = "";
				   String rutProd  = "";
				   String valor_vale = "";
				   String numdocDetalle = "";
				   String cod_servicio = "";
				   String codOrg = "";
					    
				  // verifica en iSeries primero si se envi� el registro a autorizzr y enviar a banner
				    if(moduloRecuperacionGastoB.getVerificaCOCOF132B(numdoc)){
				    	continua = false;				    	
				    }
				    if(continua){// verrifica registro existente en banner,  seg�n definici�n de Fernando Caceres correo 29/3/2019
					    mensaje = moduloRecuperacionGastoB.getVerificaRegistroBanner(vec_datos);
					    if(!mensaje.trim().equals("")){
					    		continua = false;				    		
				    }
				    } else mensaje = "Ya fue enviado a Banner desde Iseries.";
				   if(continua){
				   
				   for (int i = 0; i < vec_datos.size(); i++) {
						    datos_parametro = "";
							vec = (Vector) vec_datos.get(i);
							comen_vale =  vec.get(6)+"";
							tip_doc    = vec.get(7)+""; //VS.20.12.2016
					        desc_val   = vec.get(8)+"";
					        rutPago    = Integer.parseInt(vec.get(9)+"");
					        dvPago     = vec.get(10)+"";
					        rutProd    = vec.get(12)+"";
					        valor_vale = String.valueOf(Integer.parseInt(vec.get(4)+"") - Integer.parseInt(vec.get(11)+""));
					        numdocDetalle = String.valueOf(Integer.parseInt(vec.get(13)+""));
					        cod_servicio =  vec.get(2)+"";
					        codOrg =  vec.get(1)+"";
				                    
						 /*   for (int y = 0; y < (vec.size()-1); y++) { // para que rescate hasta valval
						    	if (y == 13) datos_parametro = datos_parametro + vec.get(y) + "@"; // Cunado llegue a VALVAL agrgue @
						    	else if(y < 5 || y == 11)datos_parametro = datos_parametro + vec.get(y) + "&";
						    }*/
					        for (int y = 0; y < (vec.size()-1); y++) { // para que rescate hasta valval
						    	if (y == 11) datos_parametro = datos_parametro + vec.get(y) + "&" + moduloRecuperacionGastoB.getBancoOperacion(codOrg) + "@"; // Cunado llegue a VALor del impuesto agrgue @
						    	else if(y < 5)datos_parametro = datos_parametro + vec.get(y) + "&";
						    }
					 
							//System.out.println("datos_parametro " + datos_parametro); 
							//int error = moduloRecuperacionGastoB.saveCuentasPorPagar(rutUsuario, dv,numdoc, rutnum, dvrut, tipoBoleta,fecha,comen_vale,tip_val,desc_val,valor_vale,datos_parametro ); // VS.19.12.2016
							// 26/04/2017  MAA rutUsuario, dv se debiera sacar de la tabla de COCOF133B en este caso para que muestre rut de pago
							int error = moduloRecuperacionGastoB.saveCuentasPorPagar(rutPago, dvPago,numdoc, 0, "", tip_doc,fecha,comen_vale,tip_doc,desc_val,valor_vale,
									                                                 datos_parametro,cod_servicio,rutDvIngresador,rutDvAutorizador,rutProd, rutDvDAF,numdocDetalle,tipo); 
								    	
							if(error == -12 || error == -14) // MB para recepcion masiva 04/05/2018  accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n del archivo Historial del documento.");
								mensaje = "Problemas en la grabaci�n del archivo Historial del documento. N� " + numdoc+".";
							else if(error == -13) // MB para recepcion masiva 04/05/2018  accionweb.agregarObjeto("mensaje", "Problemas en la actualizaci�n del estado del Documento.");
								mensaje = "Problemas en la actualizaci�n del estado del Documento. N� " + numdoc+".";
							else if (error < 0) // MB para recepcion masiva 04/05/2018  accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n a Autorizar. C�digo Banner.");//B=Banner
								mensaje = "Problemas en la grabaci�n a Autorizar. C�digo Banner. N� " + numdoc+".";
							else  // MB para recepcion masiva 04/05/2018  accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n con traspaso a Banner.");
								mensaje = "Se grab� en forma exitosa la autorizaci�n con traspaso a Banner. N� " + numdoc+". C�digo: " + moduloRecuperacionGastoB.getCodigoBanerRec();
					  }	
				   } else mensaje = "Ya fue enviado a Banner desde Iseries.";
		}
	 catch (Exception e) {
		// MB para recepcion masiva 04/05/2018  accionweb.agregarObjeto("mensaje", "No guardado en BANNER autorizaSobregiroRecupercion.");
		 mensaje = "No guardado en BANNER autorizaSobregiroRecuperacion. N� " + numdoc+".";
		accionweb.agregarObjeto("exception", e);
		e.printStackTrace();
	}
	if(!mensaje.trim().equals("")){
	   	accionweb.agregarObjeto("mensaje", mensaje);
	   	accionweb.getSesion().setAttribute("mensaje", mensaje);
	}
	
	Thread.sleep(500);
	this.cargaAutorizaRecuperacion(accionweb); 
	}		    
	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	   accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	   accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	   accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
	   accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	if(accionweb.getSesion().getAttribute("autorizaDAF") != null)
		accionweb.agregarObjeto("autorizaDAF", accionweb.getSesion().getAttribute("autorizaDAF"));
}	 


 synchronized public boolean actualizaSinFinOrganizacion(List<Cocow36DTO> listaSolicitudes, int numdoc, int rutUsuario, String dv) throws Exception {
			 	long saldoCuenta = 0;
			 	boolean sf = true;  // Sin Financiamiento
				if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				    for (Cocow36DTO ss : listaSolicitudes) {
			  		     saldoCuenta = moduloValePagoB.getBuscaSaldo(ss.getValalf().trim(),ss.getResval().trim());
			  		   if( ss.getValnu2() > saldoCuenta) 
				    	 sf = moduloValePagoB.saveEstadoCuenta(numdoc,ss.getValalf().trim(),rutUsuario,dv,"B");
					   	
				    }
			    }
				return sf;
			 }
		 
		 synchronized public boolean actualizaConFinOrganizacion(List<Cocow36DTO> listaSolicitudes, int numdoc, int rutUsuario, String dv) throws Exception {
			 	long saldoCuenta = 0;
			 	boolean cf = true;  // Con Financiamiento
				if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				    for (Cocow36DTO ss : listaSolicitudes) {
			  		     saldoCuenta = moduloValePagoB.getBuscaSaldo(ss.getValalf().trim(),ss.getResval().trim());
			  		   if( saldoCuenta > ss.getValnu2()) 
				    	 cf = moduloValePagoB.saveEstadoCuenta(numdoc,ss.getValalf().trim(),rutUsuario,dv,"A");
					   	
				    }
			    }
				return cf;
			 }
		 
		 synchronized public void recepcionaSolicitud(AccionWeb accionweb) throws Exception {
				int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
				int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			

				try { 
				boolean graba = moduloValePagoB.saveRecepciona(numdoc, rutUsuario,dv,"REV");
			    if(graba)
			    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la Recepci�n.");
			    else
			    	accionweb.agregarObjeto("mensaje", "No se Grab� la Recepci�n.");
				Thread.sleep(500);
			    this.cargaAutorizaRecuperacion(accionweb);
				} catch (Exception e) {
					accionweb.agregarObjeto("mensaje", "No guardado recepcionaSolicitud.");
					accionweb.agregarObjeto("exception", e);
					e.printStackTrace();
				}
			
				
			}
		 
            // mb incorpora autorizacion Masiva				 
			 synchronized public void recepcionaMasivo(AccionWeb accionweb) throws Exception {
					int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
					String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
					String linea = Util.validaParametro(accionweb.getParameter("linea"),"");
					int opcion = Util.validaParametro(accionweb.getParameter("tipo"), 0);
					int numdoc = 0;
					String texto = "";
					//MB incorpora grabaci�n masiva en banner  4/05/2018
					try { 
						int index = 1;
						boolean graba = false;
						String mensaje = "";
						while(linea.length() > 0){
							if(accionweb.getSesion().getAttribute("numDoc") != null)
								accionweb.getSesion().removeAttribute("numDoc");
							index = linea.indexOf("@");
							if(index > 0)
								numdoc = Integer.parseInt(linea.substring(0,index));
							else{
								numdoc = Integer.parseInt(linea);
								linea = "";
							}
							
							accionweb.getSesion().setAttribute("numDoc", numdoc);
							if(numdoc > 0){
								
							
							switch(opcion){
							case 3: {this.autorizaRecuperacion(accionweb);
									 texto = "Autorizaci&oacute;n";
									 break;						
							}
							case 16:{
								this.autorizaDAFRecuperacion(accionweb);
								texto = "Autorizaci&oacute;n DAF";
								break;
							}
							case 17: {
								this.autorizaSobregiroRecuperacion(accionweb);
								texto = "Autorizaci&oacute;n Sobregiro";
							}
							}
							}
				
							mensaje += accionweb.getSesion().getAttribute("mensaje")  + "<br>"; 
							accionweb.getSesion().removeAttribute("mensaje");
							linea = linea.substring(index+1);
						Thread.sleep(100);	
						}
						if(accionweb.getSesion().getAttribute("numDoc") != null)
							accionweb.getSesion().removeAttribute("numDoc");
					   	accionweb.agregarObjeto("mensaje", mensaje);
					   
						} catch (Exception e) {
						accionweb.agregarObjeto("mensaje", "No guardado autjorizaMasivo.");
						accionweb.agregarObjeto("exception", e);
						e.printStackTrace();
					}
					accionweb.agregarObjeto("titulo", texto);
					
				}


		public ModuloRecuperacionGastoB getModuloRecuperacionGastoB() {
			return moduloRecuperacionGastoB;
		}

		public void setModuloRecuperacionGastoB(
				ModuloRecuperacionGastoB moduloRecuperacionGastoB) {
			this.moduloRecuperacionGastoB = moduloRecuperacionGastoB;
		}

		public ModuloValePagoB getModuloValePagoB() {
			return moduloValePagoB;
		}

		public void setModuloValePagoB(ModuloValePagoB moduloValePagoB) {
			this.moduloValePagoB = moduloValePagoB;
		} 
		 
		
/* VS.05.01.2017.- Nuevos Procesos. */		 
		synchronized public boolean revisaSaldoOrganizacion(List<Presw18DTO>  listaSolicitudes, List<Presw18DTO> listaTipoPago) throws Exception {
		 	
			long total = 0;
		 	long saldoCuenta = 0;
		 	boolean sobrepasa = false;
			if (listaSolicitudes != null && listaSolicitudes.size() > 0){
			    for (Presw18DTO ss : listaSolicitudes) {
		     	   total = ss.getUsadom();
				   for (Presw18DTO ltp : listaTipoPago) {
						if (ss.getTipmov().trim().equals(ltp.getTipmov().trim())) {
					        saldoCuenta = moduloValePagoB.getBuscaSaldo(ss.getNompro().trim(),ltp.getNomtip().trim());
						    break;                          // codOrg.                              codCuenta que se dejo en Nomtip
						}    
			       }	
				
				   if(total > saldoCuenta ) {
					   sobrepasa = true;
					   break;
				   }
				 }	
		      } else  sobrepasa = true;
			return sobrepasa;
		 
		 }
		

/**/


}
