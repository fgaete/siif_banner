package cl.utfsm.srgB.modulo;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;

import cl.utfsm.POJO.PRESF200;
import cl.utfsm.base.util.ConexionBD;
import cl.utfsm.base.util.ConexionBanner;
import cl.utfsm.base.util.Util;
import cl.utfsm.conexion.ConexionAs400RecGasto;
import cl.utfsm.srg.datos.RecuperacionGastoDao;
import descad.cliente.Ingreso_Documento;
import descad.cliente.MD5;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.ItemDTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw25DTO;

public class ModuloRecuperacionGastoB {
	RecuperacionGastoDao recuperacionGastoDao;

	
	
	 public List<Presw18DTO> getTipoDocumento(){
		 Collection<Presw18DTO> listaPresw18 = null;
		 List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
			 
		 listaPresw18 = this.getConsulta("TVD", 0, 0, 0, 0, 0);
				
		 for (Presw18DTO ss : listaPresw18){
			 Presw18DTO tipoDocumento = new Presw18DTO();
			 tipoDocumento.setTipmov(ss.getTipmov());
			 tipoDocumento.setDesuni(ss.getDesuni());
			 tipoDocumento.setIndprc(ss.getIndprc());// A - afecto ... E - exento
			 lista.add(tipoDocumento);
		 }	/*	 
		 Presw18DTO tipoDocumento = new Presw18DTO();
		 tipoDocumento.setTipmov("t1");
		 tipoDocumento.setDesuni("nomtipo");
		 lista.add(tipoDocumento);
		 
		 tipoDocumento = new Presw18DTO();
		 tipoDocumento.setTipmov("t2");
		 tipoDocumento.setDesuni("nomtipo 2");
		 lista.add(tipoDocumento);*/
		return lista;
		 }
	 
	// public Collection<Presw18DTO> getVerificaRut(int rutnum, String dv, String tipo){
	//	  	PreswBean preswbean = null;
	//		Collection<Presw18DTO> lista = null;
	//		if(rutnum > 0){
	//			preswbean = new PreswBean(tipo,0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
	//			lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
	//		}		
	//	 return lista;
	//	}
	 /* VS.21.12.2016.- */
	 public Collection<Presw18DTO> getVerificaRut(String rutnumdv){
		  // Monica... A futuro se tiene que verificar recusos humanos.
		   // System.out.println(rutnumdv);
		    ConexionBanner con = new ConexionBanner();
		    PreparedStatement sent = null;
		    ResultSet res = null;
		    String nombre ="";
		    
		    String rut = rutnumdv.substring(0,rutnumdv.length()-1);
		    String dig = rutnumdv.substring(rutnumdv.length()-1,rutnumdv.length());
		    if (dig.equals("k")) rutnumdv = rut+"K";
		    
		    List<Presw18DTO> lista = new ArrayList<Presw18DTO>();;
		    try{		      
		    /*  sent = con.conexion().prepareStatement("SELECT spbpers_legal_name FROM spriden, spbpers, FTVVEND " +
		    		                                 " WHERE spriden_id = ? " +
		    		                                 " AND spriden_pidm = spbpers_pidm " +
		    		                                 " AND  spriden_PIDM = FTVVEND_PIDM" +
		    		                    		     "AND spriden_change_ind is null");*/
		    	  /*segun correo de fernando caceres todos deben validarse en FTVVEND*/
			      sent = con.conexion().prepareStatement(" SELECT spbpers_legal_name FROM spriden, spbpers, FTVVEND " +
			    		                                                   " WHERE spriden_id = ? " +
			    		                                                   " AND spriden_pidm = spbpers_pidm " +
			    		                                                   " AND  spriden_PIDM = FTVVEND_PIDM" +
			    		                                                   " AND spriden_change_ind is null");

		      sent.setString(1,rutnumdv);
		      res = sent.executeQuery();

		      while (res.next()){
			    	Presw18DTO ss = new Presw18DTO();
			        ss.setDesuni(res.getString(1)); // nombre + apellidos
			        lista.add(ss);
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloRecuperacionGastoB.getVerificaRut: " + e);
			}
		    return lista;
	 }
	 
	 
	 public Collection<Presw18DTO> getVerificaRutDocumento(String rutnumdv){
		  // Monica... A futuro se tiene que verificar recusos humanos.
		   // System.out.println(rutnumdv);
		    ConexionBanner con = new ConexionBanner();
		    PreparedStatement sent = null;
		    ResultSet res = null;
		    String nombre ="";
		    
		    String rut = rutnumdv.substring(0,rutnumdv.length()-1);
		    String dig = rutnumdv.substring(rutnumdv.length()-1,rutnumdv.length());
		    if (dig.equals("k")) rutnumdv = rut+"K";
		    
		    List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
		    try{		      
		      sent = con.conexion().prepareStatement("SELECT spbpers_legal_name, spriden_pidm FROM spriden, spbpers " +
		    		                                 "WHERE spriden_id = ? " +
		    		                                 "AND spriden_pidm = spbpers_pidm " +
		    		                                 "AND spriden_change_ind is null");
		      sent.setString(1,rutnumdv);
		      res = sent.executeQuery();

		      while (res.next()){
		    	    ResultSet res2 = null;
		    	    
		    	    Presw18DTO ss = new Presw18DTO();
			        ss.setDesuni(res.getString(1)); // nombre + apellidos
		    	    sent = con.conexion().prepareStatement(" SELECT FTVVEND_PIDM FROM FTVVEND " +
                                                           " WHERE FTVVEND_PIDM  = " + res.getString(2));
		    	    
		    	    res2 = sent.executeQuery();
		    	    if (res2.next()) ss.setIndhay("A");
		    	    else ss.setIndhay("P");	
			        
		    	    lista.add(ss);
		    	    res2.close();
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloRecuperacionGastoB.getVerificaRut: " + e);
			}
		    return lista;
	 }
	 
	 
	            //OJO preguntar que pasa con codUnidad                             
	 public Collection<Presw18DTO> getConsulta(String tippro, int codUnidad, int item, int anno, int mes, int rutIde){
			PreswBean preswbean = new PreswBean(tippro, codUnidad, item, anno, mes, rutIde);
			Collection<Presw18DTO> listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			return listaPresw18;
		 }
	
	 public String getValidaDocumento(String tippro, int rutDoc, String dvDoc, int numeroDocumento, String tipoDocumento){
		  	PreswBean preswbean = null;
			Collection<Presw18DTO> lista = null;
			String indprc = "";
			preswbean = new PreswBean(tippro, 0, 0, 0, 0, rutDoc);
			preswbean.setDigide(dvDoc);
			preswbean.setNumdoc(numeroDocumento);
			preswbean.setCajero(tipoDocumento); //VS.29.12.2016. setTipdoc(tipoDocumento);
			
			lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
				   for (Presw18DTO ss : lista){
					   indprc = ss.getIndprc();
				   }	
		 return indprc;
		}
	 
	 public void getEliminaListaSolicitaDocumento( HttpServletRequest req){
		 HttpSession sesion = req.getSession();
	     String valalf = Util.validaParametro(req.getParameter("valalf"),"");
	     String resval = Util.validaParametro(req.getParameter("resval"),"");
	     long valor = Util.validaParametro(req.getParameter("valor"),0);
	     long valorRubro = Util.validaParametro(req.getParameter("valorRubro"),0);
	     
		 List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO> )sesion.getAttribute("listaSolicitudDocumento");
		 List<Cocow36DTO>  listas = new ArrayList<Cocow36DTO>();
		 
		 List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO> )sesion.getAttribute("listaSolicitudDistribucion");
		 List<Cocow36DTO>  listas2 = new ArrayList<Cocow36DTO>();		 
	
		 if(listaSolicitudDocumento != null && listaSolicitudDocumento.size()> 0)
			 sesion.removeAttribute("listaSolicitudDocumento");
		 if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size()> 0)
			 sesion.removeAttribute("listaSolicitudDistribucion");
		 
		 if(listaSolicitudDocumento != null && listaSolicitudDocumento.size() > 0){
			 for (Cocow36DTO ss: listaSolicitudDocumento){
				 if(ss.getResval().trim().equals(resval.trim()) && ss.getValalf().trim().equals(valalf.trim()))
				 {
					if (valor ==0 ) valor = ss.getValnu2();
				 	 continue;
				 }
				 else
				 {
					 Cocow36DTO cocow36DTO = new Cocow36DTO();
					 cocow36DTO.setNomcam(ss.getNomcam());
					 cocow36DTO.setResval(ss.getResval());
					 cocow36DTO.setValalf(ss.getValalf());
					 cocow36DTO.setValnu1(ss.getValnu1());
					 cocow36DTO.setValnu2(ss.getValnu2());
					 listas.add(cocow36DTO);
				 }
			 }
		 }
		 
		 if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0 &&  
				    listas.size() == 0)
				 {
					 for (Cocow36DTO ss: listaSolicitudDistribucion){
						 if (ss.getResval().trim().equals(valalf.substring(3,9)))
							 continue;				 
					 }
				 }

		 if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0 && listas.size() > 0){
			 for (Cocow36DTO ss: listaSolicitudDistribucion){
//				 System.out.println("getAccion: " + ss.getAccion());
//			    	System.out.println("getIdsoli: " + ss.getIdsoli());
//			    	System.out.println("Nomcam: " + ss.getNomcam());
//			    		System.out.println("getResval: " + ss.getResval());
//			    		System.out.println("getRutusu: " + ss.getRutusu());
//			    	System.out.println("getTiping: " + ss.getTiping());
//			    System.out.println("getValalf: " + ss.getValalf());
//			    System.out.println("getValnu1: " + ss.getValnu1());
//			    System.out.println("getValnu2: " + ss.getValnu2());	
//			    System.out.println("valalf. " + valalf);
//			    System.out.println("resval: " + resval);
			    if (ss.getResval().trim().equals(valalf.substring(3,9)))
				 {
				  /*  	System.out.println("getAccion: " + ss.getAccion());
				    	System.out.println("getIdsoli: " + ss.getIdsoli());
				    	System.out.println("Nomcam: " + ss.getNomcam());
				    		System.out.println("getResval: " + ss.getResval());
				    		System.out.println("getRutusu: " + ss.getRutusu());
				    	System.out.println("getTiping: " + ss.getTiping());
				    System.out.println("getValalf: " + ss.getValalf());
				    System.out.println("getValnu1: " + ss.getValnu1());
				    System.out.println("getValnu2: " + ss.getValnu2());	*/
					long valorRumbo = ss.getValnu2() - valor;
					if (valorRumbo > 0)
					{					                   //org.				rubro
						long saldoCuenta =  this.getBuscaSaldo(ss.getValalf(),ss.getResval());
						String muestraSaldo = "";
						NumberTool numbertool = new NumberTool();
						muestraSaldo = String.valueOf(numbertool.format(saldoCuenta));
						 
						if( (ss.getValnu2() - valor) > saldoCuenta){
							 muestraSaldo = "<font class=\"Estilo_Rojo\" >" + muestraSaldo + "</font>";
						}	 
						Cocow36DTO cocow36DTO = new Cocow36DTO();
						cocow36DTO.setNomcam(ss.getNomcam());
						cocow36DTO.setResval(ss.getResval());					
						cocow36DTO.setValnu2(ss.getValnu2() - valor );
						cocow36DTO.setTiping(muestraSaldo);
						cocow36DTO.setValalf(ss.getValalf());
						listas2.add(cocow36DTO);						 
						 
				}
			}
				 else
				 {	

						long saldoCuenta =  this.getBuscaSaldo(ss.getValalf(),ss.getResval());
						String muestraSaldo = "";
						NumberTool numbertool = new NumberTool();
						muestraSaldo = String.valueOf(numbertool.format(saldoCuenta));
						 
						if( (ss.getValnu2()) > saldoCuenta){
							 muestraSaldo = "<font class=\"Estilo_Rojo\" >" + muestraSaldo + "</font>";
						}	
						
					    Cocow36DTO cocow36DTO = new Cocow36DTO();
					    cocow36DTO.setNomcam(ss.getNomcam());
						cocow36DTO.setResval(ss.getResval());
						cocow36DTO.setValnu2(ss.getValnu2());
						cocow36DTO.setTiping(muestraSaldo);
						cocow36DTO.setValalf(ss.getValalf());		
						listas2.add(cocow36DTO);
						
				 }
			 }
		 }
		
		   sesion.setAttribute("listaSolicitudDocumento", listas);
		   sesion.setAttribute("listaSolicitudDistribucion", listas2);	   
			
	 }
	 public void getEliminaTodoListaSolicitaDocumento( HttpServletRequest req){
		 HttpSession sesion = req.getSession();

		 List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO> )sesion.getAttribute("listaSolicitudDocumento");
		 List<Cocow36DTO>  listas = new ArrayList<Cocow36DTO>();
		 
		 List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO> )sesion.getAttribute("listaSolicitudDistribucion");
		 List<Cocow36DTO>  listas2 = new ArrayList<Cocow36DTO>();		 
	
		 if(listaSolicitudDocumento != null && listaSolicitudDocumento.size()> 0)
			 sesion.removeAttribute("listaSolicitudDocumento");
		 if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size()> 0)
			 sesion.removeAttribute("listaSolicitudDistribucion");
		 
		 if(listaSolicitudDocumento != null && listaSolicitudDocumento.size() > 0){
			 for (Cocow36DTO ss: listaSolicitudDocumento){
				 	 continue;				
				 }
			 }		
		 
		 if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0 &&  
				    listas.size() == 0)
				 {
					 for (Cocow36DTO ss: listaSolicitudDistribucion){						 
							 continue;				 
					 }
				 }
		 
		   sesion.setAttribute("listaSolicitudDocumento", listas);
		   sesion.setAttribute("listaSolicitudDistribucion", listas2);	   
			
	 }
	 public void getactualizaSaldoTablaResumen(HttpServletRequest req)
	 {		
		 HttpSession sesion = req.getSession();

		 List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO> )sesion.getAttribute("listaSolicitudDistribucion");
		 List<Cocow36DTO>  listas2 = new ArrayList<Cocow36DTO>();		 
	
		 if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size()> 0)
			 sesion.removeAttribute("listaSolicitudDistribucion");
		 int sobrepasa = 0;
		 if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0 )
		 {
			for (Cocow36DTO ss2 : listaSolicitudDistribucion)
			{
			  long saldoCuenta =  this.getBuscaSaldo(ss2.getValalf(),ss2.getResval());
			  NumberTool numbertool = new NumberTool();
			  String muestraSaldo = String.valueOf(numbertool.format(saldoCuenta));
			  if(ss2.getValnu2() > saldoCuenta){
				  sobrepasa = 1;				  
			      muestraSaldo = "<font class=\"Estilo_Rojo\" >" + muestraSaldo + "</font>";
				  ss2.setTiping(muestraSaldo);
			   }					
			}		
		 }
		 
		 sesion.setAttribute("listaSolicitudDistribucion", listas2);	   
			
	 }	 
	 public void getEliminaListaSolicitaDistribucion( HttpServletRequest req){
		 HttpSession sesion = req.getSession();
	     String codUnidad = Util.validaParametro(req.getParameter("codUnidad"),""); // VS.03.01.2016. int. - string
	        
		 List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO> )sesion.getAttribute("listaSolicitudDistribucion");
		 List<Cocow36DTO>  listas = new ArrayList<Cocow36DTO>();
	
		 if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size()> 0)
			 sesion.removeAttribute("listaSolicitudDistribucion");
		 if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
			 for (Cocow36DTO ss: listaSolicitudDistribucion){
				  // System.out.println(ss.getResval());
				 //System.out.println(codUnidad.trim());
				 if (ss.getResval().trim().equals(codUnidad.trim())) //VS.03.01.2016. getVAlnu1 - resval -- (ss.getVAlnu1() == codUnidad.trim()) // 
					 continue;
				 else
				 {
						Cocow36DTO cocow36DTO = new Cocow36DTO();
						cocow36DTO.setNomcam(ss.getNomcam());
						cocow36DTO.setValnu1(ss.getValnu1());
						cocow36DTO.setValnu2(ss.getValnu2());
						cocow36DTO.setValalf(ss.getValalf());
						listas.add(cocow36DTO);
				 }
			 }
			 
		
		 }
		
		   sesion.setAttribute("listaSolicitudDistribucion", listas);
			
	 }
	 
		synchronized  public int getRegistraRecuperacion(HttpServletRequest req, int rutide, String digide){
			int numSolicitud = 0;
			List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
			listCocow36DTO = getCreaCocow36(req, rutide, digide,"GS1"); // VS.03.01.2016. GSD - GS1
			Ingreso_Documento ingresoDocumento = new Ingreso_Documento("GS1",rutide,digide); // VS.03.01.2016. GSD - GS1
			numSolicitud = ingresoDocumento.Ingresar_Documento(listCocow36DTO);
			
		
			return numSolicitud;
		}

		public boolean getActualizaRecuperacion(HttpServletRequest req, int rutide, String digide, int numDoc){
			boolean registra = false;
			List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
			listCocow36DTO = getCreaCocow36(req, rutide, digide,"MS1"); // VS.04.01.2017. MSR - MS1
			Ingreso_Documento ingresoDocumento = new Ingreso_Documento("MS1",rutide,digide); // VS.04.01.2017. MSR - MS1
			ingresoDocumento.setNumdoc(numDoc);
			
			registra = ingresoDocumento.Actualizar_Documento(listCocow36DTO);
			
		
			return registra;
		}
		
		public List<Cocow36DTO> getCreaCocow36(HttpServletRequest req, int rutide, String digide, String tiping){
			//List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO>) req.getSession().getAttribute("listaSolicitudDistribucion");
			List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO>) req.getSession().getAttribute("listaSolicitudDocumento");
		
			long rutnum = Util.validaParametro(req.getParameter("rutnum"), 0);
			String dvrut = Util.validaParametro(req.getParameter("dvrut"),"");
			String identificadorInterno = Util.validaParametro(req.getParameter("identificadorInterno"),"");	
			String glosa1 = Util.validaParametro(req.getParameter("glosa1"),"");	
			String glosa2 = Util.validaParametro(req.getParameter("glosa2"),"");
			String glosa3 = Util.validaParametro(req.getParameter("glosa3"),"");
			String glosa4 = Util.validaParametro(req.getParameter("glosa4"),"");
			String glosa5 = Util.validaParametro(req.getParameter("glosa5"),"");
			String glosa6 = Util.validaParametro(req.getParameter("glosa6"),"");
	  	    String cuentaPresup = Util.validaParametro(req.getParameter("cuentaPresup"), ""); //VS.06.01.2017.
	 	    String desuni = Util.validaParametro(req.getParameter("desuni"),""); //VS.06.01.2017.

			//long sede = Util.validaParametro(req.getParameter("sede"), 0); VS.03.01.2016.
			List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
			if(glosa1.trim().length() > 80)
				glosa1 = glosa1.substring(0,79);
			if(glosa2.trim().length() > 80)
				glosa2 = glosa2.substring(0,79);
			if(glosa3.trim().length() > 80)
				glosa3 = glosa3.substring(0,79);
			if(glosa4.trim().length() > 80)
				glosa4 = glosa4.substring(0,79);
			if(glosa5.trim().length() > 80)
				glosa5 = glosa5.substring(0,79);
			if(glosa6.trim().length() > 80)
				glosa6 = glosa6.substring(0,79);
			
		    //String tiping = "GSD";
		    String nomcam = "";
		    long valnu1 = 0;
		    long valnu2 = 0;
		    String valalf = "";
		    String resval = "";
		    long valrec = 0;
		

	    /* prepara archivo para grabar, llenar listCocow36DTO*/
	    nomcam = "RUTIDE";
	    valnu1 = rutnum;
	    valnu2 = 0;
	    valalf = "";
	    
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval);
	    
	    nomcam = "DIGIDE";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = (dvrut.trim().equals('k')?dvrut.trim().toUpperCase():dvrut.trim());
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );
	    
	    nomcam = "CODSUC";
	    //valnu1 = sede; VS.03.01.2016
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );
	    
	    nomcam = "IDMEMO";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = identificadorInterno;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );
	    
	    nomcam = "DESCR1";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = this.eliminaAcentosString(glosa1.toUpperCase());
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );

	    nomcam = "DESCR2";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = this.eliminaAcentosString(glosa2.toUpperCase());
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );

	    nomcam = "DESCR3";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = this.eliminaAcentosString(glosa3.toUpperCase());
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );
	    
	    nomcam = "DESCR4";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = this.eliminaAcentosString(glosa4.toString().toUpperCase());
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );

	    nomcam = "DESCR5";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = this.eliminaAcentosString(glosa5.toUpperCase());
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );

	    nomcam = "DESCR6";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = this.eliminaAcentosString(glosa6.toUpperCase());
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );
	    
	  	    
	    for (Cocow36DTO ss : listaSolicitudDocumento){
	    	nomcam = ss.getNomcam();
	 	    valnu1 = ss.getValnu1();
	 	    valnu2 = ss.getValnu2();
	 	    valalf = this.eliminaAcentosString(ss.getValalf().trim().toUpperCase());
	 	    resval = ss.getResval().trim().toUpperCase();
	 	    valrec = valrec + valnu2;
	 	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );
	 	   
	    }
	    // VS.11.01.2017
	  //  for (Cocow36DTO ss : listaSolicitudDistribucion){
	  //  	nomcam = "CUENTA";
	 //	    valnu1 = ss.getValnu1();
	 //	    valnu2 = ss.getValnu2();
	 //	    valalf = ss.getResval(); //VS.03.01.2016. "" -resval
	 //	    resval = "";
	 //	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );
	 //	    valrec = valrec + valnu2;
	  //  }	
	    
		   	nomcam = "CUENTA";
		     valalf = cuentaPresup.trim(); 
		     resval = desuni;	     
		    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );
		    
	 	nomcam = "VALREC";
 	    valnu1 = valrec;
 	    valnu2 = 0;
 	    valalf = "";
 	    resval = "";
 	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );

		return listCocow36DTO;
		}
		 public List<Cocow36DTO> getAgregaLista(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf, String resval){
			 Cocow36DTO cocow36DTO = new Cocow36DTO();
			 cocow36DTO.setTiping(tiping);
			 cocow36DTO.setNomcam(nomcam);
			 cocow36DTO.setValnu1(valnu1);
			 cocow36DTO.setValnu2(valnu2);
			 cocow36DTO.setValalf(valalf);
			 cocow36DTO.setResval(resval);
			 lista.add(cocow36DTO);
			 return lista;
			 
		 }
		 public List<Cocow36DTO> getAgregaListaMD5(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf, String resval){
			 Cocow36DTO cocow36DTO = new Cocow36DTO();
			 cocow36DTO.setTiping(tiping);
			 cocow36DTO.setNomcam(nomcam);
			 cocow36DTO.setValnu1(valnu1);
			 cocow36DTO.setValnu2(valnu2);
			 cocow36DTO.setValalf(valalf);
			 cocow36DTO.setResval(resval);
			 lista.add(cocow36DTO);
			 return lista;
			 
		 }	
		 
		 public boolean saveAutoriza(int numdoc, int rutUsuario, String digide, String nomTipo, String tipCue){
				PreswBean preswbean = new PreswBean(nomTipo, 0,0,0,0, rutUsuario);
				preswbean.setDigide(digide);
				preswbean.setNumdoc(numdoc);
				preswbean.setTipcue(tipCue);
				
			return preswbean.ingreso_presw19();
		}
			public boolean saveRechaza(int numdoc, int rutUsuario, String digide, String tipo, String glosa, String tipCue){
				PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
				preswbean.setDigide(digide);
				preswbean.setNumdoc(numdoc);
				//preswbean.ingreso_presw19();
				
				List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
				Presw25DTO presw25DTO = new Presw25DTO();
				presw25DTO.setTippro(tipo);
				presw25DTO.setPres01(numdoc);
				presw25DTO.setComen1(glosa);
				presw25DTO.setRutide(rutUsuario);
				presw25DTO.setDigide(digide);
				presw25DTO.setIndexi(tipCue);
				lista.add(presw25DTO);
			return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
			}
			
		 
	public RecuperacionGastoDao getRecuperacionGastoDao() {
		return recuperacionGastoDao;
	}

	public void setRecuperacionGastoDao(RecuperacionGastoDao recuperacionGastoDao) {
		this.recuperacionGastoDao = recuperacionGastoDao;
	}
	
	public List<ItemDTO> getItemCocof139B()throws SQLException{			
	 ConexionAs400RecGasto con = new ConexionAs400RecGasto();
	 //System.out.println(""");
	 List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
	 try {
		 PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CUEBAN, DESPAG"+
		 		                                                       " FROM USMMBP.COCOF139B");
         		
		 ResultSet res = sent.executeQuery();		 
		 while (res.next()) {
			 ItemDTO item = new ItemDTO();
		     item.setIncren(res.getString(1));   // VS.03.01.2017.
		     //setItepre(res.getBigDecimal(1));  // VS.03.01.2017.
		     item.setNomite(res.getString(2));
		     listaItem.add(item);
		    }
		 res.close();
	     sent.close();	
	     con.generaConnection().close();
	 }
      catch (SQLException e) {
	    	System.out.println("Error en moduloRecuperacionGastoB.getItemCocof139B:" + e.toString());
	}
	return listaItem;					
	}
    // antiguo
	//public List<ItemDTO> getItemCocof139()throws SQLException{
	///*String query = 	" SELECT codpag, despag" +
	//				" FROM view_cocof139@badin" ;*/
		//	String query = 	" SELECT codpag, despag" +
		//					" FROM DBO_DB_UTFSM.view_cocof139" ;
		//	
	//System.out.println("query: "+ query);
		//List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
	//try {
		//	ConexionBD con = new ConexionBD();
		//	Statement sent = con.conexion().createStatement();
		//	ResultSet res = sent.executeQuery(query);
		//	while (res.next()) {
			//		ItemDTO item = new ItemDTO();
			//	    item.setItepre(res.getBigDecimal(1));
		    //	    item.setNomite(res.getString(2));
		    //	    listaItem.add(item);
		    //}
		//res.close();
	//sent.close();
	//con.close();
	//}
	//catch (SQLException e) {
		//System.out.println("Error en moduloRecuperacionGastoB.getItemCocof139:" + e.toString());
	//}
	//return listaItem;					
	//}
	
	 /**VS.06.01.2017. saldo
	  * PT
	  * 2 FORMAS DE CALCULO DE SALDO  MB cambiado 15/05/2018
	  * 1 .- PARA EL CASO DE ORGANIZACIONES QUE LOS FONDOS NO SE ENCUENTRAN EN TABLA PRESF112 - SE MANTIENE EL CALCULO POR ORGANIZACION Y CUENTA
	  * 2 .- PARA EL CASO DE ORGANIZACIONES QUE LOS FONDOS SI SE ENCUENTREN EN LA TABLA PRESF112 SE REALIZA EL CALCULO POR ORGANIZACI�N SIN CUENTA Y SE LE AGREGA***/
	/* public long getBuscaSaldo(String codOrg, String codCuenta){		 
	      // getbuscasaldo  
		  ConexionBanner con = new ConexionBanner();
		  long saldoCuenta = 0;
		  try{
		   /* PreparedStatement sent = con.conexion().prepareStatement("SELECT (fgbbavl_sum_adopt_bud+fgbbavl_sum_bud_adjt-fgbbavl_sum_ytd_actv-fgbbavl_sum_encumb-fgbbavl_sum_bud_rsrv) " +
		    		                                                 "FROM fgbbavl t1 " +
	                                                                 "WHERE t1.fgbbavl_coas_code = 'S' " + 
	                                                                 "AND t1.fgbbavl_orgn_code =  ? " +
	                                                                 "AND t1.fgbbavl_acct_code = ? " +
	                                                                 "AND t1.fgbbavl_activity_date = (SELECT MAX(t2.fgbbavl_activity_date) " + 
	                                                                 "FROM fgbbavl t2 " +
	                                                                 "WHERE t1.fgbbavl_coas_code = t2.fgbbavl_coas_code " +
	                                                                 "AND t1.fgbbavl_orgn_code = t2.fgbbavl_orgn_code " +
	                                                                 "AND t1.fgbbavl_acct_code = t2.fgbbavl_acct_code)");
			 */
	/*		  String mesActual = this.getMesActual();
			  String a�oActual = this.getAnioActual();
			  String fondo = this.getFondoSaldo(codOrg);
			  String query =   "SELECT sum(fgbbavl_sum_adopt_bud+(fgbbavl_sum_bud_adjt-fgbbavl_sum_ytd_actv-fgbbavl_sum_encumb-fgbbavl_sum_bud_rsrv)) " +
              " FROM fgbbavl t1 " +
              " WHERE t1.fgbbavl_coas_code = 'S'" + 
              " AND t1.fgbbavl_orgn_code =  ? " +
              " AND t1.fgbbavl_acct_code = ? " +
              " AND FGBBAVL_PERIOD <= '" + mesActual+ "'" +
              " AND FGBBAVL_FSYR_CODE = '" + a�oActual+ "'" +
              " ORDER BY FGBBAVL_PERIOD";
		//	  System.out.println(query);
			PreparedStatement sent = con.conexion().prepareStatement(query);
		    sent.setString(1,codOrg);
		    sent.setString(2,codCuenta);
		    ResultSet res = sent.executeQuery();

		     while (res.next()){
		    	saldoCuenta = res.getLong(1); // Saldo
		     }
		     res.close();
		     sent.close(); 
		     if(!fondo.trim().equals("")) {
			      query =   
					  " SELECT SUM(FGBTRNH_TRANS_AMT) AS TOTAL " +
					  " FROM FIMSMGR.FGBTRNH FGBTRNH,FIMSMGR.FTVORGN FTVORGN,SATURN.SPRIDEN SPRIDEN " +
					  " WHERE  ( FTVORGN.FTVORGN_ORGN_CODE = FGBTRNH.FGBTRNH_ORGN_CODE " +
					  " AND FTVORGN.FTVORGN_FUND_CODE_DEF = FGBTRNH.FGBTRNH_FUND_CODE " +
					  " AND FTVORGN.FTVORGN_PROG_CODE_DEF = FGBTRNH.FGBTRNH_PROG_CODE " +
					  " AND FGBTRNH.FGBTRNH_VENDOR_PIDM = SPRIDEN.SPRIDEN_PIDM ) " +
					  " AND ( FGBTRNH.FGBTRNH_LEDC_CODE ='FINANC' " +
					  " AND FGBTRNH.FGBTRNH_ACCT_CODE LIKE ('11A%') " +
					  " AND FGBTRNH.FGBTRNH_RUCL_CODE IN ('APS1','APS2','APS3','APS4','CHS1','CSS1') " +
					  " AND FGBTRNH.FGBTRNH_FSYR_CODE = '" + a�oActual+ "' ) " +
					  " AND FGBTRNH_ORGN_CODE = '"+ codOrg.trim() +"'" +
					  " AND FGBTRNH_ACCI_CODE = '"+ codCuenta.trim() +"'" +
					  " AND FGBTRNH_FUND_CODE = '" + fondo.trim() + "'" +
					  " AND FGBTRNH.FGBTRNH_POSTING_PERIOD <= '" + mesActual+ "'" +
					  " AND FGBTRNH_FSYR_CODE = '" + a�oActual+ "'" +
					  " ORDER BY FGBTRNH.FGBTRNH_POSTING_PERIOD, " +
		              " FGBTRNH.FGBTRNH_DOC_CODE, " +
		              " FGBTRNH.FGBTRNH_SEQ_NUM"; 
				//	 System.out.println("saldo: "+query + "  codOrg: " + codOrg+ " - "+codCuenta);
					  PreparedStatement sent2 = con.conexion().prepareStatement(query);
					  ResultSet res2 = sent2.executeQuery();
				     if (res2.next()){
				    	saldoCuenta += res2.getLong(1); // Saldo
				     } else saldoCuenta += 0;
				     res2.close();
				     sent2.close(); 
			     }
		     con.close();
		     }
		    catch (SQLException e){
		    	System.out.println("Error moduloRecuperacionGastoB.getBuscaSaldo: " + e);
			} 
		//saldoCuenta = 6;
	    return saldoCuenta;
	}
	 */
	
	 public String getNomOrganizacion(String codOrg){
			// Luego cambiar a una conexion as para Contratacion
		 ConexionAs400RecGasto con = new ConexionAs400RecGasto();
		 String nombre = "";
		     
		     try {
		        PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODORG, DESORG " +
		                                                                      " FROM USMMBP.PRESF200 " +
		                                                                      " WHERE CODORG = '" + codOrg + "'"); 
		        ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			    	nombre = res.getString(2).trim(); // 1 nombre de la organzaci�n;
			     }
			     res.close();
			     sent.close();
			     con.getConnection().close();
			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloContratoDocenteB.getNomOrganizacion : " + e );
			    }
			    return nombre;
		}
	
	 public String formateoNumeroSinDecimales(Double numero){
			/*formatear n�mero */
		     Locale l = new Locale("es","CL");
		     DecimalFormat formatter1 = (DecimalFormat)DecimalFormat.getInstance(l);
		     formatter1.applyPattern("###,###,###");
		 
		   //  formatter1.format(rs.getDouble("RUTNUM"))   /*sin decimales*/
		 return formatter1.format(numero);
		}	
// VS.10.01.2017 nuevo --- ingreso 
	 public int getAgregaListaSolicitudDocumento( HttpServletRequest req, long saldoCuenta, int rutUsuario, String dv){
		 HttpSession sesion = req.getSession();
	     //VS.06.01.2017. nuevo cambio
		 int numeroDocumento = Util.validaParametro(req.getParameter("numeroDocumento"),0);
		 String tipoDocumento = Util.validaParametro(req.getParameter("tipoDocumento"),"");
		 int rutnumDoc = Util.validaParametro(req.getParameter("rutnumDoc"), 0);
	   	 String dvrutDoc = Util.validaParametro(req.getParameter("dvrutDoc"),"");
	   	 long iva = Util.validaParametro(req.getParameter("iva"), 0);
	   	 long totalDocumento = Util.validaParametro(req.getParameter("totalDocumento"), 0);
	   	 String glosa = Util.validaParametro(req.getParameter("glosa"),"");
	   	 String rubroGasto = Util.validaParametro(req.getParameter("rubroGasto"),""); // VS.03.01.2017. long -- string -- "" 	   
	   	 String cuentaPresup = Util.validaParametro(req.getParameter("cuentaPresup"), "");
	   	 String cuentaPresupMod = Util.validaParametro(req.getParameter("cuentaPresupMod"), "");
	  	 String cuentaPresupModCam = Util.validaParametro(req.getParameter("cuentaPresupModCam"), ""); 
	 	 String desuni = Util.validaParametro(req.getParameter("desuni"),"");	 	
	  	 boolean agrega2 = true;
	     if(dvrutDoc.trim().equals("k"))
	    	 dvrutDoc = "K";
        if(cuentaPresup.trim().equals(""))
        {        	
        	if(cuentaPresupModCam.trim().equals(""))
        	{
        	  cuentaPresup = cuentaPresupMod;	
        	}
        	else cuentaPresup = cuentaPresupModCam;
        }	
	 	 //Fin nuevo cambio
	    // String desuni = this.eliminaBlancosString(des);
	   		   	
	    //List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumento");
	
	 	 long total = 0;
		 long totalTotal = 0;
		 int sobrepasa = 0;
		 long Totalrubro = 0;
		 long valorResumen = 0;
	 	 
	 	 
	 	/*valida que est� en proveedor*/
		 try {
				if(!this.getVerificaProveedorBanner(rutnumDoc + dvrutDoc))
					 sobrepasa = 4;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		if(sobrepasa == 0) {
		 	List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO>)sesion.getAttribute("listaSolicitudDocumento");	 	
	   		List<Cocow36DTO>  lista = new ArrayList<Cocow36DTO>();	   	

		 	 //listaSolicitudDistribucion -> para tabla rubro gasto
		 	 List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO>)sesion.getAttribute("listaSolicitudDistribucion");
		 	 List<Cocow36DTO> lista2 = new ArrayList<Cocow36DTO>();	
		 //List<Presw18DTO>  listaSolicitudDocumento = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudDocumento");
		 if(listaSolicitudDocumento != null && listaSolicitudDocumento.size()> 0)
			 sesion.removeAttribute("listaSolicitudDocumento");
		 
		 if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size()> 0)
			 sesion.removeAttribute("listaSolicitudDistribucion");
		 
		
		 
		 String muestraSaldo = "";
		 NumberTool numbertool = new NumberTool();
		 muestraSaldo = String.valueOf(numbertool.format(saldoCuenta));
	
		
		  	 //VS.06.01.2017. nuevo cambio...
				boolean agrega = true;
				
			
				DecimalFormat formato = new DecimalFormat("00000000");
				String rutFormateado = formato.format(rutnumDoc);
				formato = new DecimalFormat("00000000");
				String numeroFormateado = formato.format(numeroDocumento);
				formato = new DecimalFormat("000");
				//String rubroFormateado = formato.format(rubroGasto); VS.03.01.2016
				String dvrutDocFormato = dvrutDoc.trim();
				if(dvrutDocFormato.trim().equals(""))
					dvrutDocFormato = " ";
				
				if (listaSolicitudDocumento != null && listaSolicitudDocumento.size() > 0){
					if(sobrepasa == 0){
					for (Cocow36DTO ss : listaSolicitudDocumento){
						if(ss.getResval().trim().equals(rutFormateado.trim() + dvrutDocFormato + numeroFormateado.trim() ) && ss.getValalf().substring(0,3).equals(tipoDocumento.trim()))
						{
							agrega = false;
							sobrepasa = 3;
						}
						total = total + ss.getValnu2();				        
					}
					} else agrega = false;
					lista = (List<Cocow36DTO>) listaSolicitudDocumento;
				} else lista = new ArrayList<Cocow36DTO>();
				
				if(agrega){
					Cocow36DTO cocow36DTO = new Cocow36DTO();
					cocow36DTO.setNomcam("DOCASO");
					cocow36DTO.setValnu1(iva);
					cocow36DTO.setValnu2(totalDocumento);
					cocow36DTO.setResval(rutFormateado + dvrutDocFormato + numeroFormateado);
					cocow36DTO.setValalf(tipoDocumento + rubroGasto + glosa);
					lista.add(cocow36DTO);
					total = total + totalDocumento;	       
				}
				//Pendiente hasta nuevo aviso... ojojojojooo
				//else 	{
				//		accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > El Documento N&deg; "+numeroFormateado+" ya se encuentra agregado.</font> .");					
				//}			 
			 //Fin nuevo cambio
			    //	lista = (List<Cocow36DTO>) listaSolicitudDocumento;
			    //	if(lista != null && lista.size() > 0){
			    //		for (Cocow36DTO ss : lista){
			    //			total = total + ss.getValnu2();	
			    //			System.out.println(ss.getAccion());
			    //			System.out.println(ss.getIdsoli());
			    //			System.out.println(ss.getNomcam());
			    //			System.out.println(ss.getResval());
			    //			System.out.println(ss.getRutusu());
			    //			System.out.println(ss.getTiping());
			    //			System.out.println(ss.getValalf());
			    //			System.out.println(ss.getValnu1());
			    //			System.out.println(ss.getValnu2());		    			
			    //		}
			     //    }
			    	// Nuevo...
			    	agrega2 = true;
			    	if(lista != null && lista.size() > 0 && agrega )
			    	{	
						if (listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
							for (Cocow36DTO ss : listaSolicitudDistribucion){
								if (ss.getResval().trim().equals(rubroGasto))
								{	
								  agrega2 = false;
								  valorResumen = ss.getValnu2() + totalDocumento;
								  ss.setValnu2(ss.getValnu2() + totalDocumento );
								  if( valorResumen > saldoCuenta){
									 sobrepasa = 1;
									 muestraSaldo = "<font class=\"Estilo_Rojo\" >" + muestraSaldo + "</font>";
									 ss.setTiping(muestraSaldo);
								   }									  
							//	total = total + ss.getValnu2();
								}		
							}
							if( totalDocumento > saldoCuenta){
								 sobrepasa = 1;
								 muestraSaldo = "<font class=\"Estilo_Rojo\" >" + muestraSaldo + "</font>";
							 }
							lista2 = (List<Cocow36DTO>) listaSolicitudDistribucion;
						} else
						{
							lista2 = new ArrayList<Cocow36DTO>();
							 if( totalDocumento > saldoCuenta){
							 sobrepasa = 1;
							 muestraSaldo = "<font class=\"Estilo_Rojo\" >" + muestraSaldo + "</font>";
						      }	
						}	 
						if(agrega2){
					    		Cocow36DTO cocow36DTO = new Cocow36DTO();
								cocow36DTO.setNomcam("CUENTA");
								cocow36DTO.setResval(rubroGasto.trim());
								cocow36DTO.setValnu2(totalDocumento);
								cocow36DTO.setValalf(cuentaPresup.trim());
								cocow36DTO.setTiping(muestraSaldo);
								lista2.add(cocow36DTO);	    		
					    	}						
			    	}
					
			    	//if(lista2 != null && lista2.size() > 0){ 
			    	//	for (Cocow36DTO ss : lista2){
			    	//		System.out.println("getAccion: " + ss.getAccion());
			    	//		System.out.println("getIdsoli: " + ss.getIdsoli());
			    	//		System.out.println("Nomcam: " + ss.getNomcam());
			    	//		System.out.println("getResval: " + ss.getResval());
			    	//		System.out.println("getRutusu: " + ss.getRutusu());
			    	//		System.out.println("getTiping: " + ss.getTiping());
			    	//		System.out.println("getValalf: " + ss.getValalf());
			    	//		System.out.println("getValnu1: " + ss.getValnu1());
			    	//		System.out.println("getValnu2: " + ss.getValnu2());		    			
			    	//	}
			         //}	    	
			    	// fin nuevo				
		 

	    	
		//sesion.setAttribute("listaSolicitudDocumento", listaSolicitudDocumento);
		 if(lista != null && lista.size() > 0)
			 sesion.setAttribute("listaSolicitudDocumento", lista);
		 if(lista2 != null && lista2.size() > 0)
			 sesion.setAttribute("listaSolicitudDistribucion", lista2);
		}
		return sobrepasa;	
}	 
	 public String getDatoCOCOF102B(int numero){
		   // ConexionBD con = new ConexionBD();
		    ConexionAs400RecGasto con = new ConexionAs400RecGasto();
		    String fecha = "";
		    
		    try{	      
		       PreparedStatement sent = con.getConnection().prepareStatement("SELECT MIN(fecope) " +
	                                                                         "FROM USMMBP.COCOF102B " +
	                                                                         "WHERE NUMVAL = ? " );	
		 
		      sent.setInt(1,numero);
		      //sent.setInt(2,rutide);
		      //sent.setString(3, digide);
		      //sent.setString(4, tipdoc);
		      ResultSet res = sent.executeQuery();

		      if (res.next()){
		        fecha = res.getString(1); // fecha mas antigua de la boleta con tipo de operacion I
		       // System.out.println(res.getString(1));
		      }
		      res.close();
		      sent.close();
		     // con.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloRecuperacionGastoB.getDatoCOCOF102B: " + e);
			}
		 return fecha;
	}	 

	 public String getValorDeRGT(int numero){
		    //ConexionBD con = new ConexionBD();
		    ConexionAs400RecGasto con = new ConexionAs400RecGasto();
		    String valor = "0";
		    try{
		      
		   /*   PreparedStatement sent = con.conexion().prepareStatement("SELECT valval FROM VIEW_COCOF100B " +
		    		                                                   "WHERE numval = ? " +
		    		                                                   "AND ESTVAL = 'A'"); */

		       PreparedStatement sent = con.getConnection().prepareStatement("SELECT VALREC FROM USMMBP.COCOF132B " +
																			    			"WHERE NUMREC = ? " ); 	
		      
		    	
		      sent.setInt(1,numero);		     
		      ResultSet res = sent.executeQuery();

		      if (res.next()){
		        valor = res.getString(1); // valor de vale
		        //System.out.println(res.getString(1));
		      }
		      res.close();
		      sent.close();
		     // con.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloRecuperacionGastoB.getValorDeVale: " + e);
			}
		 return valor;
	}
	 
	 public String getEstadoCOCOF101B(int numero, String codOrg){
		   // ConexionBD con = new ConexionBD();
		    ConexionAs400RecGasto con = new ConexionAs400RecGasto();
		    String estado = "";
		    
		    try{	      
		       PreparedStatement sent = con.getConnection().prepareStatement(" SELECT ESTIMP " +
	                                                                         " FROM USMMBP.COCOF101B " +
	                                                                         " WHERE NUMVAL = ? " +
	                                                                         " AND CODORG = ? ");	
		 
		      sent.setInt(1,numero);
		      sent.setString(2, codOrg);
		      ResultSet res = sent.executeQuery();

		      if (res.next()){
		        estado = res.getString(1); // fecha mas antigua de la boleta con tipo de operacion I
		      }
		      res.close();
		      sent.close();
		     // con.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloRecuperacionGastoB.getEstadoCOCOF101B: " + e);
			}
		 return estado;
	}	 
	 
	 public String getRutOperacion(int numero, String tipOpe){
		    //ConexionBD con = new ConexionBD();
		    ConexionAs400RecGasto con = new ConexionAs400RecGasto();
		    String rutDv = "";
		    try{
		      
		      PreparedStatement sent = con.getConnection().prepareStatement(" SELECT RUTIDE,DIGIDE FROM USMMBP.COCOF102B " +
	    																	" WHERE NUMVAL = ? " +
			    															" AND TIPOPE = ? "); 	
		    	
		      sent.setInt(1,numero);
		      sent.setString(2,tipOpe);
		      ResultSet res = sent.executeQuery();

		      while (res.next()){
		        rutDv = res.getInt(1)+""+res.getString(2); // rut operaci�n
		      }
		      res.close();
		      sent.close();
		     // con.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloRecuperacionGastoB.getRutOperacion: " + e);
			}
		 return rutDv;
	}
	 

	 public Vector getDatosAS(int numero){ //, int rutide, String digide, String tipdoc
		    // ConexionBD con = new ConexionBD();
	
		     Vector vec = new Vector();
		     Vector detalle = null;
		     
		     try {
					List<ItemDTO> listaItem2 = new ArrayList<ItemDTO>();
					listaItem2 = getItemCocof139B();
					ConexionAs400RecGasto con = new ConexionAs400RecGasto();		    	 
		       /* PreparedStatement sent = con.conexion().prepareStatement("SELECT v2.codfon, v1.codorg, v3.cueban, v2.codpro,v1.valasi, v.numval, " +
		        		                                                 "v.glode1 || ' ' || v.glode2, v.tipval, v3.desval" +
		                                                                 "FROM VIEW_COCOF100B v, VIEW_COCOF101B v1, VIEW_PRESF200 v2 , VIEW_COCOF105B v3 " +
		                                                                 "WHERE v.numval = ? " + 
		                                                                 "AND v.numval = v1.numval " +
		                                                                 "AND v1.codorg = v2.codorg " +
		                                                                 "AND v.tipval = v3.tipval");*/
		    	/*PreparedStatement sent = con.getConnection().prepareStatement(" SELECT v2.codfon, v1.codorg, V1.TIPDOC, v2.codpro, v1.valasi," +                  
		    																  " v.NUMDOC, v.glode1, v.glode2, v.TIPGAS" +                                      
		    																  " FROM USMMBP.COHOF100B v, USMMBP.COHOF101B v1, USMMBP.PRESF200 v2" +   
		                                                                   	  " WHERE  v1.codorg = v2.codorg" +                                       
		                                                                   	  " AND v.NUMDOC = v1.NUMDOC" +                                           
		                                                                   	  " AND v.RUTIDE = v1.RUTIDE" +                                           
		                                                                   	  " AND v.DIGIDE = V1.DIGIDE" +                                           
		                                                                   	  " AND V.TIPDOC = V1.TIPDOC" +                                           
		                                                                   	  " AND V.NUMDOC = ?" +                                                  
		                                                                   	  " AND V.RUTIDE = ?" +                                            
		                                                                   	  " AND V.DIGIDE = ?" +                                                 
		                                                                   	  " AND V.TIPDOC = ?"); */
			/*	String query = " SELECT v3.codfon," +   
				               " v1.codorg," +    
				               " v.itedoc," +     
				               " v3.codpro," +
				               " v1.valasi," +              
				               " v.numrec," +  
				               " v.glodoc," +  
				               " v.itedoc," +  
				               " v.rutide," +
				               " v.digide" +
				               " FROM USMMBP.COCOF133B v," +
				               " USMMBP.COCOF101B v1," +
				               " USMMBP.PRESF200 v3" +
				               " WHERE v.NUMREC = v1.NUMVAL" +
				               " AND v1.CODORG = v3.CODORG" +
				               " AND v.NUMREC = ?";*/
				
				
				//" SELECT v.tipdoc, v3.codfon, v1.codorg, v.itedoc, v3.codpro,(v.impdoc + v.valdoc),v.numrec,v.glodoc, v.itedoc, v2.rutide,v2.digide" +
				
				String query = " SELECT v3.codfon," +   
				               " v1.codorg," +    
				               " v.itedoc," +     
				               " v3.codpro," +
				               " v.valdoc," +              
				               " v.numrec," +  
				               " v.glodoc," +  
				               " v.tipdoc," +  
				               " v2.rutide," +  // rut a pago
				               " v2.digide," +
				               " v.impdoc, " +
				               " v.rutide," +   // rut proveedor
				               " v.digide," +
				               " v.numdoc" +
				               " FROM USMMBP.COCOF133B v," +
                        	   " USMMBP.COCOF132B v2," +
				               " USMMBP.COCOF101B v1," +
				               " USMMBP.PRESF200 v3" +
				               " WHERE v.NUMREC = v1.NUMVAL" +
                        	   " AND v.NUMREC = v2.NUMREC" +
				               " AND v1.CODORG = v3.CODORG" +
				               " AND v.NUMREC = ?";
				System.out.println(query);
		    	 PreparedStatement sent = con.getConnection().prepareStatement(query);
		 
		        sent.setInt(1,numero);
		        //sent.setInt(2,rutide);
		        //sent.setString(3,digide);       
		        //sent.setString(4,tipdoc);
		       
			    ResultSet res = sent.executeQuery();

			    while (res.next()) { 
			       detalle = new Vector();
			       String tipoRubro="";
			       detalle.add(res.getString(1).trim()); // 0 codigo del fondo;
				   detalle.add(res.getString(2).trim()); // 1 codigo de la organizacion;		   
				   detalle.add(res.getString(3).trim()); // 2 codigo de cuenta DE SERVICIO
				   detalle.add(res.getString(4).trim()); // 3 codpro
				   detalle.add(res.getInt(5) - res.getInt(11)); // 4 valor del vale
				   detalle.add(res.getString(6).trim()); // 5 numero de vale
				   //detalle.add(res.getString(7).trim()+" " + res.getString(8).trim()); // descripcion del vale
				   detalle.add(res.getString(7).trim()); // 6 descripcion 
					for(ItemDTO pp:listaItem2)
					{
					   if(pp.getIncren().trim().equals(res.getString(3).trim()))
					   {
						 tipoRubro = pp.getNomite().trim();
						 break;
					   }										 	
					}
				   				
				   detalle.add(res.getString(8).trim()); // 7 tipo de recuperacion (BCO, FAC,etc);
				   detalle.add(tipoRubro); 				// 8 DescriPcion del tipo de rubro(cuenta del rubro del gasto)
				   detalle.add(res.getString(9).trim()); // 9 Rut a pago   de la Recuperaci�n
				   detalle.add(res.getString(10).trim()); // 10 dev rut a pago
				   detalle.add(res.getString(11).trim()); // 11 impuesto
				   detalle.add(res.getString(12).trim()+res.getString(13).trim()); // 12 rut
				   detalle.add(res.getString(14).trim()); // 13 numdoc
				//   detalle.add(this.getBancoOperacion(res.getString(2))); // 14 codigo banco
				   vec.add(detalle);
			     }
			    
			     res.close();
			     sent.close();
			     // con.close();
			     con.getConnection().close();

			    }
			    catch (SQLException e ) {
			      	
			      System.out.println("Error en moduloRecuperacionGastoB.getDatosAS : " + e );
			    }
			    return vec;
		}
	 String codigoBanerRec = "";
	 public String getCodigoBanerRec() {
		return codigoBanerRec;
	}

	public void setCodigoBanerRec(String codigoBanerRec) {
		this.codigoBanerRec = codigoBanerRec;
	}

	public int saveCuentasPorPagar(int rutUsuario, String dvUsuario,int numdoc,int rutnum, String dvrut,String tip_doc, String fecha_invoice,String comen_vale,
			                        String tip_val, String desc_val, String valor_vale,String datos,String cod_servicio, String ingresador, String autorizador, String rutProd,
			                        String rutDvDAF, String numdocDetalle, int tipoAut){

	
		  ConexionBanner con = new ConexionBanner();
		  CallableStatement sent;
		  int error = -1;
		  String codigoBaner = "";
		  String rutdv = String.valueOf(rutUsuario)+dvUsuario;
		  this.setCodigoBanerRec("");
		 System.out.println(rutdv+",I,"+numdoc+","+fecha_invoice+","+comen_vale+","+tip_doc+","+desc_val+","+valor_vale+","+datos+",:v_cod_error,:v_cod_banner,'"+cod_servicio+"','',"+ingresador+","+autorizador+","+rutProd+" DAF "+rutDvDAF);
		  try{ //invoice_dateDATE, pmt_due_dateDATE, trans_dateDATE,doct_codeVARCHAR2,datos_farinvaVARCHAR2
			  //codigo detalle el futuro RGT --- BCO
		       sent = con.conexion().prepareCall("{call P_USM_CXP_REC_SIIF(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
		       sent.setString(1,rutdv);
		       sent.setString(2,"I");
		       sent.setString(3, String.valueOf(numdoc));
		       sent.setString(4,fecha_invoice);
		       sent.setString(5,comen_vale);
		       sent.setString(6,tip_doc);  // por ahora es BCO para despues  RGT
		       sent.setString(7,desc_val);
		       sent.setString(8,valor_vale);
		       sent.setString(9,datos);
		       sent.registerOutParameter(10, Types.INTEGER);
		       sent.registerOutParameter(11, Types.VARCHAR);
		       sent.setString(12, cod_servicio);
		       sent.setString(13,ingresador);
		       sent.setString(14,autorizador);
		       sent.setString(15,rutProd);
		       sent.setString(16,rutDvDAF);
		       sent.setString(17,numdocDetalle);
			     
		       sent.executeQuery();
		       error = sent.getInt(10);
		       codigoBaner = sent.getString(11);
		       //System.out.println("codigoBaner: "+codigoBaner + " error: "+error);   
		       sent.close(); 
		       con.close();
		     }
		    catch (SQLException e){
		    	System.out.println("Error moduloRecuperacionGastoB.saveCuentasPorPagar: " + e);
			} 
		    
		    if(error == 0) {
		    	 Calendar fecha = new GregorianCalendar();
			     int a�o = fecha.get(Calendar.YEAR);
			     int mes = fecha.get(Calendar.MONTH)+1;
			     int dia = fecha.get(Calendar.DAY_OF_MONTH);
			     int hora = fecha.get(Calendar.HOUR_OF_DAY);
			     int minuto = fecha.get(Calendar.MINUTE);
			     int segundo = fecha.get(Calendar.SECOND);
			     String mestexto = (mes > 9)?String.valueOf(mes):"0"+String.valueOf(mes);
			     String diatexto = (dia > 9)?String.valueOf(dia):"0"+String.valueOf(dia);
			     String mintexto = (minuto > 9)?String.valueOf(minuto):"0"+String.valueOf(minuto);
			     String segtexto = (segundo > 9)?String.valueOf(segundo):"0"+String.valueOf(segundo); 
			     int fechaAc = Integer.parseInt(String.valueOf(a�o) + mestexto + diatexto);
			     int horaAc = Integer.parseInt(String.valueOf(hora) + mintexto + segtexto);
			   //  int fechaAc = Integer.parseInt(String.valueOf(a�o) + String.valueOf((mes+1)) + String.valueOf(dia));
			   //  int horaAc = Integer.parseInt(String.valueOf(hora) + String.valueOf(minuto) + String.valueOf(segundo));
			         
			     // guarda informacion en cohof102B
			     ConexionAs400RecGasto con2 = new ConexionAs400RecGasto();
		         Vector vec2 = new Vector();
		         Vector detalle2 = null;
		         
		         if (tipoAut == 17) {
		        	 
		        	 try {
			              PreparedStatement sent2 = con2.getConnection().prepareStatement(" INSERT INTO USMMBP.COCOF102B (" +                                           
	                           	  														  " NUMVAL ," +  
	                           	  														  " FECOPE ," +
	                           	  														  " HOROPE ," +
	                           	  														  " TIPOPE ," +
	                           	  														  " RUTIDE ," +                                            
	                           	  														  " DIGIDE ," +                                                 
	                           	  														  " CODORG ,"+
	                              														  " COMENT ) VALUES( ?, ?, ?, ?, ?, ?, ?, ?)"); 		              
	                           	   		        	 
			            sent2.setInt(1,numdoc);
			            sent2.setInt(2, fechaAc);
			            sent2.setInt(3, horaAc);
			            sent2.setString(4,"S");	
			            sent2.setInt(5, Integer.parseInt(rutDvDAF.substring(0,rutDvDAF.length()-1)));
			            sent2.setString(6, rutDvDAF.substring(rutDvDAF.length()-1));
			            sent2.setString(7, cod_servicio.trim());
			            sent2.setString(8, " ");	            
			           
			    	    int res = sent2.executeUpdate();
		                if(res < 0)
		                	error = -14;     
		           
		         }	
		         catch (SQLException e ) {
    	    	      System.out.println("Error en moduloRecuperacionGastoB.saveCuentasPorPagar COCOF102B graba estado de sobregiro : " + e );
                    error = -15;  
                      
    	    	    } 
	            } 
		      if(error == 0) {
		        try {
		              PreparedStatement sent2 = con2.getConnection().prepareStatement(" INSERT INTO USMMBP.COCOF102B (" +                                           
                           	  														  " NUMVAL ," +  
                           	  														  " FECOPE ," +
                           	  														  " HOROPE ," +
                           	  														  " TIPOPE ," +
                           	  														  " RUTIDE ," +                                            
                           	  														  " DIGIDE ," +                                                 
                           	  														  " CODORG ,"+
                              														  " COMENT ) VALUES( ?, ?, ?, ?, ?, ?, ?, ?)"); 		              
                           	   		        	 
		            sent2.setInt(1,numdoc);
		            sent2.setInt(2, fechaAc);
		            sent2.setInt(3, horaAc);
		            sent2.setString(4,"E");	
		            sent2.setInt(5, Integer.parseInt(rutDvDAF.substring(0,rutDvDAF.length()-1)));
		            sent2.setString(6, rutDvDAF.substring(rutDvDAF.length()-1));
		            sent2.setString(7, cod_servicio.trim());
		            sent2.setString(8, codigoBaner);	            
		            this.setCodigoBanerRec(codigoBaner);
		    	    int res = sent2.executeUpdate();
	                if(res < 0)
	                	error = -12;      	    	    
	                else {
	                	try {	
	                		sent2 = con2.getConnection().prepareStatement(" UPDATE USMMBP.COCOF132B SET ESTREC = ? " +
	                				                                      " WHERE NUMREC = ? ");
	                		sent2.setString(1,"E");
	                		sent2.setInt(2,numdoc);
	                	
	                		int res2 = sent2.executeUpdate();
	                		if(res2 < 0){
	                			error = -13;    	  
	                		}
	                      }
	                    catch (SQLException e ) {
	      	    	      System.out.println("Error en moduloRecuperacionGastoB.saveCuentasPorPagar:  COCOF132B : " + e );
	                      error = -6;  
	                        
	      	    	    } 
	                 }
		    	     sent2.close();
		    	     // con.close();
		    	     con2.getConnection().close();

		    	    }
		    	    catch (SQLException e ) {
		    	      System.out.println("Error en moduloRecuperacionGastoB.saveCuentasPorPagar: COCOF102B" + e );
		    	    }
		         }   
		      }  
		    
		 return error;
	}	 
	 public String getMesActual(){
		 DateTool fechaActual = new DateTool();
		 String mesActual = "";
			
		 String mes = (fechaActual.getMonth()+1)+"";
		 if(mes.length()== 1)
					mes = "0" + mes;
		 mesActual = mes;
		 return mesActual;
	 }
	 public String getAnioActual(){
		 DateTool fechaActual = new DateTool();
		 String a�oActual = String.valueOf(fechaActual.getYear());
			
		 
		 return a�oActual.substring(2,4);
	 }
	 
	 public String eliminaAcentosString(String sTexto){
			/*elimina acentos del string*/
		   String linea = "";  
		    if(!sTexto.trim().equals("")){
		    for (int x=0; x < sTexto.length(); x++) {
		    	  if ((sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') || 
		    	      (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
		    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') || 
		    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
		    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
		    		  sTexto.charAt(x) == '\''){
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'a';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'A';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'e';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'E';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'i';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'I';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'o';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'O';
		    		   if(sTexto.charAt(x) == '�' )
		    			  linea += 'u';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'U';
		    		  if(sTexto.charAt(x) == '\'')
		    			  linea += ' ';
		    	  }
		    	  else  linea += sTexto.charAt(x);
		    	}				
		    }
		
		 return linea;
		}
	 
	 public String getBancoOperacion(String organizacion){
		 
		    ConexionAs400RecGasto con = new ConexionAs400RecGasto();
		    String codBan = "DC";
		    try{
		      
		      PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODBAN FROM USMMBP.COCBF07B " +
	    																	" WHERE CODORG LIKE ? "); 			    	
		    
		      sent.setString(1,organizacion);
		      ResultSet res = sent.executeQuery();

		      if (res.next()){
		    	  codBan = res.getString(1).trim(); // CODBAN
		      }
		      res.close();
		      sent.close();
		     // con.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloRecuperacionGastoB.getBancoOperacion: " + e);
			}
		 return codBan;
	}
	 public int getVerificaRecuperacionBanner(int rutnum, String dv, int numeroBoleta,String tipoBoleta) throws Exception{
		  ConexionBanner con = new ConexionBanner();
		  int pidm = 0;
		  String rutDoc = rutnum + ((dv.equals("k"))?"K":dv);
          try{
		      String query =   	" SELECT FABINVH_VEND_PIDM" +
		      					" FROM FABINVH, FVRDCIN, SPRIDEN" +
								" WHERE FABINVH_CODE = FVRDCIN_INVH_CODE" +
								" AND FABINVH_VEND_INV_CODE      = '" + numeroBoleta +"'" +
								" AND FVRDCIN_DOCT_CODE = '" + tipoBoleta +"'" +
								" AND SPRIDEN_ID = '" +rutDoc +"'" +
								" AND FABINVH_VEND_PIDM = SPRIDEN_PIDM" ;
			PreparedStatement sent = con.conexion().prepareStatement(query);
		    
		    ResultSet res = sent.executeQuery();

		     if (res.next()){
		    	 pidm = res.getInt(1); // pidm
		     }
		     res.close();
		     sent.close(); 
		     con.close();
		     }
		    catch (SQLException e){
		    	System.out.println("Error moduloHonorarioB.getVerificaBoletaBanner: " + e);
			} 
		 
		
		 return pidm;
		}
	
	 public boolean getVerificaProveedorBanner(String rutDocumento) throws Exception{
		  ConexionBanner con = new ConexionBanner();
		  boolean existe = false;
		  
         try{
    	      String query =   	" SELECT spriden_pidm " +
		      					" FROM spriden s, FTVVEND f" +
		      					" WHERE spriden_id = '" + rutDocumento + "'" +
		      					" AND  spriden_PIDM = FTVVEND_PIDM" +
		      					" AND spriden_change_ind is null" ;
			PreparedStatement sent = con.conexion().prepareStatement(query);
		    
		    ResultSet res = sent.executeQuery();

		     if (res.next()){
		    	 existe = true;
		     }
		     res.close();
		     sent.close(); 
		     con.close();
		     }
		    catch (SQLException e){
		    	System.out.println("Error moduloHonorarioB.getVerificaProveedorBanner: " + e);
			} 
		 
		
		 return existe;
		}
	 public long getBuscaSaldo(String codOrg, String codCuenta){
		 ConexionBanner con = new ConexionBanner();
		 int error = -1;
		 long saldoCuenta = 0;
		 String codFondo = "";
		  
		 try{ //invoice_dateDATE, pmt_due_dateDATE, trans_dateDATE,doct_codeVARCHAR2,datos_farinvaVARCHAR2
			 codFondo = this.getFondoSaldo(codOrg);  // MB para prod que no se calcule hasta que no quede aprobado
			  
			 String sql = "{?=call PKG_VALIDACIONES.F_CALCULO_SALDO(?,?,?)}";
			
		     CallableStatement cstmt = con.getConexion().prepareCall(sql);
		     cstmt.registerOutParameter(1, Types.INTEGER);
		     cstmt.setString(2,codOrg);
		     cstmt.setString(3, codCuenta);
		     cstmt.setInt(4,(codFondo.trim().equals("")?0:1));
			
			 cstmt.executeQuery();
			       
			 saldoCuenta  = cstmt.getLong(1);
			       
			 cstmt.close();
		     con.close();
		    }
		   catch (SQLException e){
		   	System.out.println("Error moduloRecuperacionGastoB.getBuscaSaldo: " + e);
		   	error = -5;

			} 
		   
		   return saldoCuenta;
		 }
		 
		 public String getFondoSaldo(String organizacion){
				 ConexionAs400RecGasto con = new ConexionAs400RecGasto();
				 String fondo = "";
			     
			     try {
			       	PreparedStatement sent = con.getConnection().prepareStatement("SELECT pr1.codfon" +
			       																" FROM USMMBP.PRESF200 pr1" +
			       																" WHERE pr1.CODFON IN (SELECT pr2.CODFON FROM USMMBP.PRESF112 pr2" +
			       																"                          WHERE pr1.CODFON = pr2.CODFON)" +                         
			       																" AND pr1.CODORG = ?"); 
			        sent.setString(1,organizacion);
				    ResultSet res = sent.executeQuery();

				    if (res.next()) { 
				    	fondo = res.getString(1);
				      }
				     res.close();
				     sent.close();
				     // con.close();
				     con.getConnection().close();

				    }
				    catch (SQLException e ) {
				      System.out.println("Error en moduloRecuperacionGastoB.getFondoSaldo : " + e );
				    }
				    return fondo;
			}
		 
		 String codigoBanerVale = "";
// FIN 



		public String getCodigoBanerVale() {
			return codigoBanerVale;
		}

		public void setCodigoBanerVale(String codigoBanerVale) {
			this.codigoBanerVale = codigoBanerVale;
		}
		
		
		 public String getVerificaRegistroBanner(Vector vec_datos){
			  // MB se agrega esta verificaci�n para que verifique si existe en banner algun documento de la recuperaci�n de gastos y se rechaza completa
			  // seg�n definici�n de Fernando Caceres correo 29/3/2019
			 
			    ConexionBanner con = new ConexionBanner();
			    PreparedStatement sent = null;
			    ResultSet res = null;
			   	String mensaje = "";
			   	String numdocDetalle = "";
			   	String rutProd = "";
			   	String cod_servicio = "";
			   	String tip_doc = "";
			      try{		      
			    	  for (int i = 0; i < vec_datos.size(); i++) {
			    		  Vector vec = (Vector) vec_datos.get(i);  			    	  
			    		  numdocDetalle = String.valueOf(Integer.parseInt(vec_datos.get(13)+"")).trim();
			    		  rutProd    = String.valueOf(vec_datos.get(12)+"").trim();
			    		  tip_doc    = String.valueOf(vec.get(7)+"").trim(); 
			    		  String query = " SELECT FVRDCIN_INVH_CODE " +
										 " FROM FVRDCIN, FARINVC, FABINVH, spriden" + 
										 " WHERE FARINVC_INVH_CODE = FVRDCIN_INVH_CODE" +
										 " AND FABINVH_CODE = FARINVC_INVH_CODE" +
										 " AND FABINVH_VEND_PIDM = spriden_pidm" +
										 " AND FVRDCIN_DOCT_CODE in ('FAE', 'FEE', 'FAP', 'BCO') " +
										 " AND FVRDCIN_DOCT_CODE like ('"+ tip_doc + "') " +
										 " AND FARINVC_VEND_INV_CODE like '" +numdocDetalle+"'" +
										 " AND SPRIDEN_ID like '" +rutProd+"'";										 
			    	sent = con.conexion().prepareStatement(query);	  								 

			      	res = sent.executeQuery();

				      if (res.next()){
				    	if(!res.getString(1).trim().equals(""))  
				         	  mensaje += "Existe en Banner Documento: "+ numdocDetalle +", tipo:  " + cod_servicio + ", rut Documento: " +rutProd + "<br>";
				      }
			      }
			      res.close();
			      sent.close();
			      con.close();
			    }
			    catch (SQLException e){
			    	System.out.println("Error moduloRecuperacionGastoB.getVerificaRegistroBanner: " + e);
			    	con.close();
				}
			    return mensaje;
		 }
		 public boolean getVerificaRegistroiSeries(int numDoc){
			  // MB se agrega esta verificaci�n para que no se env�e 2 veces a banner un socumento que ya existe 20180831
			    ConexionBanner con = new ConexionBanner();
			    PreparedStatement sent = null;
			    ResultSet res = null;
			   	boolean existe = false;
			      try{		      
			   	      sent = con.conexion().prepareStatement(" SELECT FVRDCIN_INVH_CODE FROM FVRDCIN" +
			   	    		  								 " WHERE FVRDCIN_VEND_INV_CODE LIKE ?" +
			   	    		  								 " AND FVRDCIN_DOCT_CODE NOT IN ('BHE', 'FAE','FAP', 'FEE')" +
			   	    		  								 " AND FVRDCIN_DATA_ORIGIN = 'SIIF_BANNER'");

			      sent.setInt(1,numDoc);
			      res = sent.executeQuery();

			      if (res.next()){
			    	  existe = true;
			      }
			      res.close();
			      sent.close();
			      con.close();
			    }
			    catch (SQLException e){
			    	System.out.println("Error moduloRecuperacionGastoB.getVerificaRegistroBanner: " + e);
				}
			    return existe;
		 }
		 public boolean getVerificaCOCOF132B(int numero){
			 //MB agregado para que asegurarse que ya fue enviado a banner antes
			   // ConexionBD con = new ConexionBD();
			    ConexionAs400RecGasto con = new ConexionAs400RecGasto();
			    boolean agrega =  false;
			    
			    try{	      
			       PreparedStatement sent = con.getConnection().prepareStatement("SELECT ESTREC " +
		                                                                         "FROM USMMBP.COCOF132B " +
		                                                                         "WHERE NUMVAL = ? " );	
			 
			      sent.setInt(1,numero);			      
			      ResultSet res = sent.executeQuery();

			      if (res.next()){
			        if (res.getString(1).trim().equals("E")) // significa que ya fue ingresada
			        	agrega = true;
			        else {
			        	try {	// como no ha sido ingresada en iseries se registra para asegurar la demora posterior por si la hay
				      		PreparedStatement sent2 = con.getConnection().prepareStatement(" UPDATE USMMBP.COCOF132B SET ESTREC = ? " +
		            				                                      " WHERE NUMREC = ? ");
		            		sent2.setString(1,"E");
		            		sent2.setInt(2,numero);
		            	
		            		int res2 = sent2.executeUpdate();
		            		if(res2 < 0){
		            			agrega = true;    	  
		            		}
		                  }
		                catch (SQLException e ) {
		  	    	      System.out.println("Error en moduloRecuperacionGastoB.getVerificaCOCOF132B:  COCOF132B : " + e );                  
		                    
		  	    	    } 
			        }
			      }
			      res.close();
			      sent.close();
			      con.getConnection().close();
			    }
			    catch (SQLException e){
			    	System.out.println("Error moduloRecuperacionGastoB.getVerificaCOCOF132B: " + e);
				}
			    
		  
			    
			 return agrega;
		}
}


