package cl.utfsm.sitB.datos;

import java.text.SimpleDateFormat;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import POJO.pojoUF;

import cl.utfsm.base.util.ConexionBanner;

public class HibernateTesoreriaDao extends HibernateDaoSupport implements
		TesoreriaDao {

	public static Collection<pojoUF> getProcesoUfanterior(
			Collection<pojoUF> lista, String fecha,Boolean mes_sgte) throws Exception {

		ArrayList<pojoUF> listaBD = new ArrayList<pojoUF>();
		Collection<pojoUF> listafinal = new ArrayList<pojoUF>();

		try {

			String sql = "SELECT GVRCURR_CONV_RATE,to_char(GVRCURR_RATE_EFF_DATE,'yyyy-mm-dd') "
					+ " FROM GVRCURR"
					+ " WHERE to_char(GVRCURR_RATE_EFF_DATE,'mm/yyyy') = ?  AND GVRCURR_CURR_CODE = 'UF'"
					+ " ORDER BY GVRCURR_RATE_EFF_DATE asc";
		//	 System.out.println(sql);
			ConexionBanner con = new ConexionBanner();
			PreparedStatement sent = null;
			ResultSet res = null;
			sent = con.conexion().prepareStatement(sql);

			sent.setString(1, fecha);
			res = sent.executeQuery();
			//System.out.println("fecha  " + fecha) ;
			while (res.next()) {

				pojoUF ss = new pojoUF();
				ss.setValor(res.getFloat(1));
				ss.setFecha(res.getString(2));
				listaBD.add(ss);

			}

			res.close();
			sent.close();
			con.close();

			for (pojoUF ssBD : listaBD) {
				for (pojoUF ssUF : lista) {
					String mesaux = ssUF.getFecha().substring(1,
							ssUF.getFecha().length() - 1);
					if (ssBD.getFecha().trim().equals(mesaux.trim())
							&& (ssBD.getValor() == ssUF.getValor())) {
						listafinal.add(ssUF);
						break;

					}
				}
			}

			lista.removeAll(listafinal);
			System.out.println("tama�o lista " + lista.size()) ;
			if (lista.size() == 0) {
			//	if (!mes_sgte) {
				pojoUF ss = new pojoUF();
				ss.setValor(0);
				ss.setFecha("Mes cargado ya en Base de datos");
				lista.add(ss);
				//}
			}

		} catch (Exception ex) {
			System.out
					.println("Error  HibernateTesoreriaDao.getProcesoUfanterior " + ex.getMessage());
		}
		return lista;

	}

	private static String getUsuario(String rut) {
		String user = "";
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;
		try {
			String sql = "SELECT UPPER(REPLACE(SUBSTR(GOR.goremal_email_address,1,INSTR(GOR.goremal_email_address,'@')-1),'.',''))  USUARIO  "
					+ " FROM GOREMAL GOR, SPRIDEN SPR"
					+ " WHERE GOR.GOREMAL_PIDM = SPR.SPRIDEN_PIDM"
					+ " AND SPR.SPRIDEN_ID = ? AND GOREMAL_EMAL_CODE = ?";
			sent = con.conexion().prepareStatement(sql);
			sent.setString(1, rut);
			sent.setString(2, "INS1");
			res = sent.executeQuery();
			while (res.next()) {
				user = res.getString(1);
			}
			res.close();
			sent.close();
			con.close();

		} catch (Exception ex) {
			user = "error";
			System.out.println("Error sitB HibernateTesoreriaDao.getUsuario: " + ex.getMessage());
			con.close();

		}
		return user;
	}

	public static Boolean setCargaBDUF(ArrayList<pojoUF> lista, String rut) {
		Boolean resp = false;
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;

		try {
			String user = getUsuario(rut);
			String query = "INSERT ALL ";
			String queryInsert = "";
			String queryBody = "";

			if (user.equals("error")) {
				resp = false;
			} else {
				for (pojoUF ss : lista) {
					 
					/*String valor = String.valueOf(ss.getValor()).replace(".",
							",");
					*/
					String valor = String.valueOf(ss.getValor());
					String dateInString = ss.getFecha().substring(1,
							ss.getFecha().length() - 1);

					SimpleDateFormat outFmt = new SimpleDateFormat("ddMMyyyy");
					String fec = outFmt.format(outFmt.parse(dateInString));
					dateInString = dateInString.replace("-", "");
					
					
					queryBody = queryBody
							+ " INTO GVRCURR(GVRCURR_CURR_CODE,	GVRCURR_CONV_RATE,	GVRCURR_RATE_EFF_DATE,	GVRCURR_RATE_NCHG_DATE,	GVRCURR_ACTIVITY_DATE,	GVRCURR_USER_ID)"
							+ " VALUES ('UF', '" + valor + "',to_date('"
							+ dateInString + "','yyyyMMdd'),to_date('"
							+ dateInString 
							+ "','yyyyMMdd') + 1 ,SYSDATE,'"+ user+ "') ";

				}
				queryInsert = query + " " + queryBody + " SELECT * FROM DUAL";
			//	System.out.println(" getCargaOrgTemporal masivo " + queryInsert);
				// con.getConexion().setAutoCommit(false);

				sent = con.getConexion().prepareStatement(queryInsert);
				int res = sent.executeUpdate();
				// Obtiene informacion de las organizaciones

				if (res > 0)
					resp = true;

				// con.getConexion().setAutoCommit(true);
				con.getConexion().commit();
				sent.close();
				con.close();
			}
		} catch (Exception ex) {
			System.out.println("Error sitB HibernateTesoreriaDao.setCargaBDUF: *** "
					+ ex.getMessage());
			con.close();
		}
		return resp;
	}

}
