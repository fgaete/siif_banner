package cl.utfsm.sitB.mvc;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Vector;
import restclient.NetClientGet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import POJO.pojoUF;

import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.sipB.POJO.Pres18POJO;
import cl.utfsm.sitB.datos.HibernateTesoreriaDao;
import cl.utfsm.sitB.modulo.ModuloTesoreriaB;
import descad.presupuesto.Presw18DTO;

public class TesoreriaInterceptorB  extends HandlerInterceptorAdapter {
	private ModuloTesoreriaB moduloTesoreriaB;

	public ModuloTesoreriaB getModuloTesoreriaB() {
		return moduloTesoreriaB;
	}

	public void setModuloTesoreriaB(ModuloTesoreriaB moduloTesoreriaB) {
		this.moduloTesoreriaB = moduloTesoreriaB;
	}
	
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
	}
	public void cargarMenu(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		accionweb.agregarObjeto("esTesoreriaB", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		
		if(tipo == 3) { 
		// Carga mes y a�o actual opci�n UF
			Calendar fecha = Calendar.getInstance();
			int a�o = fecha.get(Calendar.YEAR);
			String a�osql = String.valueOf(a�o);
			int mes = fecha.get(Calendar.MONTH) + 1;
			
			accionweb.agregarObjeto("anno", a�osql);
			accionweb.agregarObjeto("mesUF", mes);
			accionweb.agregarObjeto("annoselec", a�osql);
		}

	}
	public void cargaDocumentos(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
	    Vector  resultadoImport = (Vector) accionweb.getSesion().getAttribute("resultadoImport");
		accionweb.agregarObjeto("esTesoreriaB", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		accionweb.agregarObjeto("resultadoImport", resultadoImport);

	}

	public void getobtenerUF(AccionWeb accionweb) throws Exception {
		
		NetClientGet apiUF = new NetClientGet();
		Collection<pojoUF> lista = new ArrayList<pojoUF>();		
		String mes = accionweb.getParameter("fechaMes");
		
		String anno = "" + accionweb.getParameter("fechaAnno");
		Boolean continua = true;
		Calendar fecha = Calendar.getInstance();
		int a�o = fecha.get(Calendar.YEAR);
		String a�osqlactual = String.valueOf(a�o);
		int mesactual = fecha.get(Calendar.MONTH) + 1;
		int messelecc = Integer.parseInt(mes.trim());
		if (Integer.parseInt(anno) == Integer.parseInt(a�osqlactual)){
			if(messelecc > mesactual) {
				continua = false;
			}
		}
		accionweb.agregarObjeto("correcto", continua);
		if (mes.length() < 2) 
			mes = "0" + mes;
		
		if (continua) {
			String fecha_consulta = mes + "/"+ anno;					
			accionweb.agregarObjeto("correcto", continua);
			lista = apiUF.getobtieneUF(Integer.parseInt(mes.trim()), Integer
					.parseInt(anno.trim()));
			// si fecha sistema es mayor de 9, se debe consultar por el mes siguiente
			
			int dia = fecha.get(Calendar.DATE);
			Collection<pojoUF> lista_sgte = new ArrayList<pojoUF>();
			boolean mes_sgte = false;
			if (dia > 8) { // busca valores del mes siguiente
				lista_sgte = apiUF.getobtieneUF(Integer.parseInt(mes.trim()) + 1, Integer
						.parseInt(anno.trim()));
				lista.addAll(lista_sgte);
				mes_sgte = true;
			}
			
			System.out.println("Lista antes tama�o " + lista.size());
			lista = (Collection<pojoUF> )HibernateTesoreriaDao.getProcesoUfanterior(lista,fecha_consulta,false) ;
			 if (mes_sgte) {
				 fecha_consulta = "0"  + Integer.toString(Integer.parseInt(mes) + 1)  + "/"+ anno;
				 Collection<pojoUF> lista_sgteBD = new ArrayList<pojoUF>();
				 lista_sgteBD = (Collection<pojoUF> )HibernateTesoreriaDao.getProcesoUfanterior(lista,fecha_consulta,true) ;
				// lista.addAll(lista_sgteBD);
			 }
			 
			Boolean tiene_datos = true;
			for (pojoUF ssBD : lista) {								
				if ((ssBD.getFecha().equals("Mes cargado ya en Base de datos")) && (ssBD.getValor() == 0)) {
					tiene_datos = false;
					break;
				}
			}
			System.out.println("Lista despues tama�o " + lista.size());
			accionweb.agregarObjeto("valoresUF", lista);
			accionweb.agregarObjeto("tiene_datos", tiene_datos);
			accionweb.agregarObjeto("muestra_msje", false);
		/*	for (pojoUF str : lista) {
				// imprimimos el objeto pivote
				System.out.println("Fecha: " + str.getFecha());
				System.out.println("Valor: " + str.getValor());

			} */
			accionweb.agregarObjeto("correctoBD", true);
			accionweb.getSesion().setAttribute("listaUFfinal", lista);

		
		}
	}

	public void guardaBD(AccionWeb accionweb) throws Exception {
		try {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			//int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
			
			//System.out.println("Rut guardaBD " + rutUsuario + dv) ;
			Boolean resp = false;
			ArrayList<pojoUF> lista = new ArrayList<pojoUF>();
			if (accionweb.getSesion().getAttribute("listaUFfinal") != null) {
				lista = (ArrayList<pojoUF>) accionweb.getSesion().getAttribute("listaUFfinal");			
			}
			resp = HibernateTesoreriaDao.setCargaBDUF(lista, rutUsuario + dv) ;
			
			if (resp) {
				accionweb.getSesion().removeAttribute("listaUFfinal");
				
			}
			
			accionweb.agregarObjeto("correcto", true);
			accionweb.agregarObjeto("correctoBD", true);
			accionweb.agregarObjeto("tiene_datos", false);
			accionweb.agregarObjeto("muestra_msje", true);

			
		} catch (Exception ex) {
			System.out.println("error guardaBD " + ex.getMessage());
		}
		}


/*
	public void guardaBD(AccionWeb accionweb) throws Exception {
	try {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		//int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+"","");
		
		//System.out.println("Rut guardaBD " + rutUsuario + dv) ;
		Boolean resp = false;
		ArrayList<pojoUF> lista = new ArrayList<pojoUF>();
		if (accionweb.getSesion().getAttribute("listaUFfinal") != null) {
			lista = (ArrayList<pojoUF>) accionweb.getSesion().getAttribute("listaUFfinal");			
		}
		resp = HibernateTesoreriaDao.setCargaBDUF(lista, rutUsuario + dv) ;
		
		if (resp) {
			accionweb.getSesion().removeAttribute("listaUFfinal");
			
		}
		
		accionweb.agregarObjeto("correcto", true);
		accionweb.agregarObjeto("correctoBD", true);
		accionweb.agregarObjeto("tiene_datos", false);
		accionweb.agregarObjeto("muestra_msje", true);

		
	} catch (Exception ex) {
		System.out.println("error guardaBD " + ex.getMessage());
	}
	
	
	
	}
	
	*/
	
	public void registraDocumento(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
	    Vector  resultadoImport = (Vector) accionweb.getSesion().getAttribute("resultadoImport");
		accionweb.agregarObjeto("esTesoreriaB", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		accionweb.agregarObjeto("resultadoImport", resultadoImport);
		String mensaje = "";
		mensaje = moduloTesoreriaB.updteDocumentos(resultadoImport);
	/*	for(int i=0; i < resultadoImport.size() - 1 ; i++){
			int error = 0;
			error = moduloTesoreriaB.updteDocumentos(resultadoImport.get(i).toString());
			mensaje += "<br> el doc. N� "+resultadoImport.get(i).toString()+ ((error>=0)?" OK":" Error");
		}*/
		accionweb.agregarObjeto("esTesoreriaB", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		accionweb.agregarObjeto("mensaje", mensaje);
		accionweb.getSesion().removeAttribute("resultadoImport");
	}
	
}
