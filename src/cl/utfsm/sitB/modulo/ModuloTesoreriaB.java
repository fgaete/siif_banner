package cl.utfsm.sitB.modulo;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;

import cl.utfsm.base.util.ConexionBanner;
import cl.utfsm.conexion.ConexionAs400ValePago;
import cl.utfsm.sitB.datos.TesoreriaDao;

public class ModuloTesoreriaB {
	TesoreriaDao tesoreriaDao;

	public TesoreriaDao getTesoreriaDao() {
		return tesoreriaDao;
	}

	public void setTesoreriaDao(TesoreriaDao tesoreriaDao) {
		this.tesoreriaDao = tesoreriaDao;
	}
/*	synchronized public int updteDocumentos(String numero) throws Exception {
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent;
		String mensaje = "";
		int error = 0;		
		
				try{ 
					sent = con.conexion().prepareStatement(" UPDATE fimsmgr.fabinvh SET fabinvh_hold_ind = 'N'," +
							                               " FABINVH_DATA_ORIGIN = 'MONICA'" +
														   " WHERE fabinvh_code = ?" +
														   " AND fabinvh_hold_ind = 'Y'");
					
			
					sent.setString(1,numero);
				
					int res = sent.executeUpdate();
					if(res < 0){
					error = -1;    	  
					}
					sent.close();
					}
				catch (SQLException e ) {
				System.out.println("Error en moduloTesoreriaB.updteDocumentos.fabinvh : " + e.toString() );
				error = -6;  
				
				con.close();
				} 
				if(error == 0){
					try{ 
						sent = con.conexion().prepareStatement(" UPDATE fimsmgr.farinvc SET farinvc_hold_ind = 'N'," +
								                                " FARINVC_DATA_ORIGIN  = 'MONICA'" +
																" WHERE farinvc_invh_code = ?" +
																" AND farinvc_hold_ind = 'Y'");
						sent.setString(1,numero);
				
						int res = sent.executeUpdate();
						if(res < 0){
						error = -1;    	  
						}
						sent.close();
					
						}
					catch (SQLException e ) {
					System.out.println("Error en moduloTesoreriaB.updteDocumentos.farinvc : " + e.toString() );
					error = -6;  
					con.close();
					}
				}
				
				con.close();
	
		return error;
	}*/
	
	synchronized public String updteDocumentos(Vector resultadoImport) throws Exception {
	ConexionBanner con = new ConexionBanner();
	con.conexion().setAutoCommit(false);
	PreparedStatement sent;
	String mensaje = "";
	for(int i=0; i < resultadoImport.size()  ; i++){
		int error = -1;		
		try{ 
			sent = con.conexion().prepareStatement(" SELECT fabinvh_code" +
												   " FROM fabinvh" +
					                               " WHERE fabinvh_code = ?" /*+
												   " AND fabinvh_hold_ind = 'Y'"*/);
			
	
			sent.setString(1,resultadoImport.get(i).toString());
			ResultSet resul = null;
			resul = sent.executeQuery();
			if(resul.next()){
			   if(resul.getString(1).trim().equals(resultadoImport.get(i).toString().trim()))
				  error = 0;
			   else error = -2;
			} else error = -3;
			sent.close();
			
		//if(error == 0) {
				sent = con.conexion().prepareStatement(" UPDATE fimsmgr.fabinvh SET fabinvh_hold_ind = 'N'" +
						                          	   " WHERE fabinvh_code = ?" +
													   " AND fabinvh_hold_ind = 'Y'");
				
		
				sent.setString(1,resultadoImport.get(i).toString());
			
				int res = sent.executeUpdate();
				if(res < 0){
				error = -5;    	  
				} else error = 0;
				sent.close();
		
		//}
		//	if(error == 0){
					sent = con.conexion().prepareStatement(" SELECT farinvc_invh_code" +
														    " FROM farinvc" +			
															" WHERE farinvc_invh_code = ?" /*+
															" AND farinvc_hold_ind = 'Y'"*/);
					
			
					sent.setString(1,resultadoImport.get(i).toString());
					ResultSet resul2 = null;
					resul2 = sent.executeQuery();
					if(resul2.next()){
					   if(resul2.getString(1).trim().equals(resultadoImport.get(i).toString().trim()))
						  error = 0; 
					   else error = -7;
					} else error = -8;
					sent.close();
				
		//	}
		//	if(error == 0){
				
					sent = con.conexion().prepareStatement(" UPDATE fimsmgr.farinvc SET farinvc_hold_ind = 'N'" +
							                             	" WHERE farinvc_invh_code = ?" +
															" AND farinvc_hold_ind = 'Y'");
					sent.setString(1,resultadoImport.get(i).toString());
			
					int res2 = sent.executeUpdate();
					if(res2 < 0){
					error = -10;    	  
					} else error = 0;
					sent.close();
				
				
		//	}
			switch (error){
			case 0:{ mensaje += "<br> El doc. N� "+resultadoImport.get(i).toString()+ " Ejecutado";
			         break;
			         }
			case -2: case -7: {
			         mensaje += "<br> El doc. N� "+resultadoImport.get(i).toString()+ " No Ejecuta, no encuentra("+error+")";
			         break;
			         }
			default: {
				 mensaje += "<br> El doc. N� "+resultadoImport.get(i).toString()+ " No Ejecuta("+error+")";
				 break;
			}
			}
		}
		catch (SQLException e ) {
		System.out.println("Error en moduloTesoreriaB.updteDocumentos.fabinvh : " + e.toString() );
		error = -4; 
		con.close();
		} 
			if(error == 0) con.conexion().commit();

		    else con.conexion().rollback();		
	}
	
	con.close();
	return mensaje;
}
}
