package cl.utfsm.sicbB.modulo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import cl.utfsm.base.util.ConexionBanner;
import cl.utfsm.sicbB.datos.ConciliacionBancariaDao;
import descad.cliente.PreswBean;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw25DTO;

public class ModuloConciliacionBancariaB {
	ConciliacionBancariaDao conciliacionBancariaDao;




	public ConciliacionBancariaDao getConciliacionBancariaDao() {
		return conciliacionBancariaDao;
	}
	public void setConciliacionBancariaDao(
			ConciliacionBancariaDao conciliacionBancariaDao) {
		this.conciliacionBancariaDao = conciliacionBancariaDao;
	}
	
	public List<Presw18DTO>  getConciliacion( HttpServletRequest req, long fecha){
	    // ConexionBD con = new ConexionBD();
		 HttpSession sesion = req.getSession();
			
		 ConexionBanner con = new ConexionBanner();
	     List<Presw18DTO> listaPresw18DTO = new ArrayList<Presw18DTO>();
	    	 
	     try {
		   	String query = " SELECT FGBTRND_DOC_CODE, GXVBANK_BANK_CODE, GXVBANK_ACCT_NAME, TO_CHAR(FGBTRNH_TRANS_DATE,'YYYYMMDD'), " + 
		   	               " CASE WHEN SUBSTR(FGBTRND_DOC_CODE ,1,1) = 'A' " + 
                                " THEN ( SELECT CASE WHEN  FVRDNMA_DOC_TYPE = 'M' " + 
                                                   " THEN LTRIM(SUBSTR(FVRDNMA_LARGE_DOC_NUM,8,17),'0') " + 
                                                   " ELSE LTRIM(SUBSTR(FGBTRND_DOC_CODE,2,8),'0') " +
                                               " END " +
                                         " FROM FABCHKS, FVRDNMA " +   
                                         " WHERE FGBTRND_DOC_CODE = FABCHKS_CHECK_NUM " +
                                         " AND FABCHKS_CHECK_TYPE_IND IN ('M','B','O') " +
                                         " AND FABCHKS_CHECK_NUM = FVRDNMA_CHECK_NUM) " +
                                " ELSE  LTRIM(SUBSTR(FGBTRND_DOC_CODE,2,8),'0') " +
                           " END, " +
                           " CASE WHEN SUBSTR(FGBTRND_DOC_CODE ,1,1) = '!' THEN 'NOMINA' " +
                                " WHEN SUBSTR(FGBTRND_DOC_CODE ,1,1) = 'A' THEN (SELECT CASE WHEN FVRDNMA_DOC_TYPE IN ('B','O') " +
                                                                                           " THEN 'VALE VISTA' " +
                                                                                           " WHEN FVRDNMA_DOC_TYPE = 'M' " +
                                                                                           " THEN 'CHEQUE' " +
                                                                                           " ELSE ' ' END " +
                                                                                " FROM FABCHKS, FVRDNMA " +   
                                                                                " WHERE FGBTRND_DOC_CODE = FABCHKS_CHECK_NUM " +
                                                                                " AND FABCHKS_CHECK_NUM = FVRDNMA_CHECK_NUM) " +
                                " WHEN SUBSTR(FGBTRND_DOC_CODE ,1,1) = 'J' AND FGBTRND_DR_CR_IND = 'D' THEN 'ABONOS' " +
                                " WHEN SUBSTR(FGBTRND_DOC_CODE ,1,1) = 'J' AND FGBTRND_DR_CR_IND = 'C' THEN 'CARGOS' " +
                                " WHEN SUBSTR(FGBTRND_DOC_CODE ,1,1) = 'F' THEN 'DEPOSITOS' " +
                                " WHEN SUBSTR(FGBTRND_DOC_CODE ,1,1) = 'I' THEN 'TRASPASOS' " +
                                " ELSE ' ' " +
                           " END, FGBTRND_TRANS_AMT, FGBTRND_DR_CR_IND, FGBTRNH_TRANS_DESC, TO_CHAR(FGBTRND_ACTIVITY_DATE,'YYYYMMDD') " + 
                           " FROM FGBTRND, GXVBANK, FGBTRNH " +  
                           " WHERE FGBTRND_ACCT_CODE = GXVBANK_ACCT_CODE_CASH " +
                           " AND GXVBANK_ACH_STATUS = ? " +
                           " AND TO_CHAR(FGBTRND_ACTIVITY_DATE,'YYYYMMDD') = ?"  + 
                           " AND FGBTRND_DOC_CODE = FGBTRNH_DOC_CODE " +
                           " AND FGBTRND_DOC_SEQ_CODE = FGBTRNH_DOC_SEQ_CODE " +
                           " AND FGBTRND_SEQ_NUM = FGBTRNH_SEQ_NUM " +
                           " AND FGBTRND_ITEM_NUM = FGBTRNH_ITEM_NUM " +
                           " AND FGBTRND_SUBMISSION_NUMBER = FGBTRNH_SUBMISSION_NUMBER " +
                           " AND FGBTRND_REVERSAL_IND = FGBTRNH_REVERSAL_IND " +
                           " AND FGBTRND_SERIAL_NUM = FGBTRNH_SERIAL_NUM " +
                           " AND FGBTRND_COAS_CODE = FGBTRNH_COAS_CODE " +
                           " AND FGBTRND_RUCL_CODE = FGBTRNH_RUCL_CODE " +
                           " AND FGBTRNH_LEDC_CODE = FGBTRND_LEDC_CODE " +
                           " AND FGBTRND_LEDC_CODE = ? " +
                           " AND FGBTRND_COAS_CODE =  ? " +
                           " ORDER BY FGBTRND_DOC_CODE ";
									    


	    	 //System.out.println(query);
	    	PreparedStatement sent = con.conexion().prepareStatement(query); 
	    	sent.setString(1, "A");
	        sent.setString(2, String.valueOf(fecha));
	        sent.setString(3, "FINANC");
	        sent.setString(4, "S");
		    ResultSet res = sent.executeQuery();
            Vector v = new Vector();
		    while (res.next()) {	
		    	/*if(res.getString(1).trim().equals("I0076821")){
		    		System.out.println("res.getString(1): "+res.getString(1).length() + "res.getString(5): "+res.getString(5).length()+"res.getString(4): "+res.getString(4).length()+"res.getString(2): "+res.getString(2).length()+"res.getString(9): "+res.getString(9).length()+"res.getString(8): "+res.getString(8).length()+"res.getString(7): "+res.getString(7).length()+"res.getString(6): "+res.getString(6).length()+"res.getString(10): "+res.getString(10).length());
		    		System.out.println("res.getString(1): "+res.getString(1) + "res.getString(5): "+res.getString(5)+"res.getString(4): "+res.getString(4)+"res.getString(2): "+res.getString(2).length()+"res.getString(9): "+res.getString(9).length()+"res.getString(8): "+res.getString(8).length()+"res.getString(7): "+res.getString(7).length()+"res.getString(6): "+res.getString(6).length()+"res.getString(10): "+res.getString(10).length());
		    	}*/
		     	Presw18DTO presw18DTO = new Presw18DTO();
		    	presw18DTO.setTippro("GDC");// tippro
		    	presw18DTO.setNompro(res.getString(1)); // c�digo del documento
		    	presw18DTO.setNumdoc(res.getInt(5)); // numero documento
		    	presw18DTO.setFecdoc(res.getInt(4)); // fecha movimiento
		     	presw18DTO.setNomtip(res.getString(2)); // Cod. Banco
		    	presw18DTO.setDesuni(this.eliminaCaracteresString(res.getString(9))); // Descripci�n mov.
		     	presw18DTO.setIndhay(res.getString(8)); // Tipo Tranasaci�n C � D
		    	presw18DTO.setPresum(res.getLong(7)); // valor Mov.
		    	presw18DTO.setDesite(res.getString(6)); // Tipo de Movimiento (vale vista, nomina, abonos, cargos, depositos, traspaso y cheques)
		    	presw18DTO.setUsadom(res.getLong(10)); // fecha de ingreso a Banner 
		    	listaPresw18DTO.add(presw18DTO);
		    	
		    	
		    }		   
		     res.close();
		     sent.close();
		     con.close();
		   
		    }
		    catch (SQLException e ) {
		      System.out.println("Error en moduloCorreccion.getConciliacion : " + e );
		    }
		    return listaPresw18DTO ;
	}	
	public int getCuentaConciliacion( HttpServletRequest req, long fecha){
	    // ConexionBD con = new ConexionBD();
		// a raiz del no traspaso de informaci�n de 1 documento de banner a produccion es que se decide agregar un conteo de manera de comparar al final si lo traspasado coincide con lo de banner
		// MB -PP 20190329
		 HttpSession sesion = req.getSession();
			
		 ConexionBanner con = new ConexionBanner();
         int cont = 0;
	    	 
	     try {
		   	String query = " SELECT COUNT(*) " + 
                           " FROM FGBTRND, GXVBANK, FGBTRNH " +  
                           " WHERE FGBTRND_ACCT_CODE = GXVBANK_ACCT_CODE_CASH " +
                           " AND GXVBANK_ACH_STATUS = ? " +
                           " AND TO_CHAR(FGBTRND_ACTIVITY_DATE,'YYYYMMDD') = ?"  + 
                           " AND FGBTRND_DOC_CODE = FGBTRNH_DOC_CODE " +
                           " AND FGBTRND_DOC_SEQ_CODE = FGBTRNH_DOC_SEQ_CODE " +
                           " AND FGBTRND_SEQ_NUM = FGBTRNH_SEQ_NUM " +
                           " AND FGBTRND_ITEM_NUM = FGBTRNH_ITEM_NUM " +
                           " AND FGBTRND_SUBMISSION_NUMBER = FGBTRNH_SUBMISSION_NUMBER " +
                           " AND FGBTRND_REVERSAL_IND = FGBTRNH_REVERSAL_IND " +
                           " AND FGBTRND_SERIAL_NUM = FGBTRNH_SERIAL_NUM " +
                           " AND FGBTRND_COAS_CODE = FGBTRNH_COAS_CODE " +
                           " AND FGBTRND_RUCL_CODE = FGBTRNH_RUCL_CODE " +
                           " AND FGBTRNH_LEDC_CODE = FGBTRND_LEDC_CODE " +
                           " AND FGBTRND_LEDC_CODE = ? " +
                           " AND FGBTRND_COAS_CODE =  ? " +
                           " ORDER BY FGBTRND_DOC_CODE ";
									    


	    	 System.out.println(query);
	    	PreparedStatement sent = con.conexion().prepareStatement(query); 
	    	sent.setString(1, "A");
	        sent.setString(2, String.valueOf(fecha));
	        sent.setString(3, "FINANC");
	        sent.setString(4, "S");
		    ResultSet res = sent.executeQuery();
          
		    while (res.next()) {	
		    	cont = res.getInt(1);
		 	
		    	
		    }		   
		     res.close();
		     sent.close();
		     con.close();
		   
		    }
		    catch (SQLException e ) {
		      System.out.println("Error en moduloCorreccion.getConciliacion : " + e );
		    }
		    return cont ;
	}	
	 public boolean setGeneraiSeries(List<Presw18DTO> lista){
		    PreswBean preswbean = null;
			preswbean = new PreswBean("GDC",0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
			Presw18DTO presw18 = new Presw18DTO();	
			boolean respuesta = false;
			respuesta = preswbean.ingreso_presw18((List<Presw18DTO>)lista);
		 return respuesta;
		}
	 public String eliminaCaracteresString(String sTexto){
			/*elimina acentos del string*/
		   String linea = "";  
		   linea = sTexto.replaceAll("\'","\'\'");
		 return linea;
		}
}
