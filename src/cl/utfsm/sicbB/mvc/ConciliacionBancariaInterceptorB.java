package cl.utfsm.sicbB.mvc;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.sicbB.modulo.ModuloConciliacionBancariaB;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw25DTO;

public class ConciliacionBancariaInterceptorB extends HandlerInterceptorAdapter {
	private ModuloConciliacionBancariaB moduloConciliacionBancariaB;

	
	
	public ModuloConciliacionBancariaB getModuloConciliacionBancariaB() {
		return moduloConciliacionBancariaB;
	}
	public void setModuloConciliacionBancariaB(
			ModuloConciliacionBancariaB moduloConciliacionBancariaB) {
		this.moduloConciliacionBancariaB = moduloConciliacionBancariaB;
	}
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
	}
	public void cargarMenu(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		//accionweb.agregarObjeto("esConciliacion", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		accionweb.agregarObjeto("esTesoreriaB", 1); 
		
	}
	public void generaConciliacion(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		String  fechaIng = Util.validaParametro(accionweb.getParameter("fechaIng"), "");
		long fechaConc = Long.parseLong(fechaIng.substring(6)+""+fechaIng.substring(3,5)+""+fechaIng.substring(0,2));
		List<Presw18DTO> listpresw18DTO = moduloConciliacionBancariaB.getConciliacion(accionweb.getReq(), fechaConc);
		int cuenta = moduloConciliacionBancariaB.getCuentaConciliacion(accionweb.getReq(), fechaConc);
		boolean respuesta = false;
		String mensaje = "";
	
		if (listpresw18DTO != null && listpresw18DTO.size() > 0) {
			respuesta = moduloConciliacionBancariaB.setGeneraiSeries(listpresw18DTO);
			mensaje = (cuenta == listpresw18DTO.size())?"Con todos los documentos":"Faltaron Documentos, se traspasaron "+listpresw18DTO.size()+" de un total de "+cuenta;
		}
		accionweb.agregarObjeto("respuesta", respuesta);
		accionweb.agregarObjeto("mensaje", mensaje);
		//accionweb.agregarObjeto("esConciliacion", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);
		accionweb.agregarObjeto("esTesoreriaB", 1); 
		
	}
}
