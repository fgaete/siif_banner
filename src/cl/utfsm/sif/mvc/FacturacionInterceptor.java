package cl.utfsm.sif.mvc;
/**
 * SISTEMA DE SOLICITUDES DE FACTURACI�N POR INTERNET
 * PROGRAMADOR		: 	M�NICA BARRERA FREZ
 * FECHA ULT.MODIF  : 	03/07/2012
 * UNIDAD DE DESARROLLO INSTITUCIONAL(DTI)  
 * migraci�n oracle : 15/06/2013 MB 
 */
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import org.apache.velocity.tools.generic.DateTool;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.sib.modulo.ModuloHonorario;
import cl.utfsm.sif.modulo.ModuloFacturacion;
import descad.cliente.CocowBean;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw21DTO;
import descad.presupuesto.Presw25DTO;

public class FacturacionInterceptor extends HandlerInterceptorAdapter {
	private ModuloFacturacion moduloFacturacion;
	 
	

	public ModuloFacturacion getModuloFacturacion() {
		return moduloFacturacion;
	}

	public void setModuloFacturacion(ModuloFacturacion moduloFacturacion) {
		this.moduloFacturacion = moduloFacturacion;
	}
	
	public void cargarMenuFacturacion(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
	/*  	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.getSesion().removeAttribute("autorizaProyecto");
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.getSesion().removeAttribute("autorizaUCP");
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.getSesion().removeAttribute("autorizaDIRPRE");*/
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.getSesion().removeAttribute("autorizaFinanzas");
	/* se debe buscar segun el usuario si tiene permiso para autorizar*/
	List<Presw18DTO> listaAutorizaciones = new ArrayList<Presw18DTO>();
	listaAutorizaciones = moduloFacturacion.getConsultaAutoriza(rutUsuario, dv);
	if(listaAutorizaciones != null && listaAutorizaciones.size() > 0){
		 for (Presw18DTO ss : listaAutorizaciones){
			if(ss.getIndprc().trim().equals("Y")){
				accionweb.agregarObjeto("autorizaProyecto", 1);
				accionweb.getSesion().setAttribute("autorizaProyecto", 1);
			}
			if(ss.getIndprc().trim().equals("X")){
				accionweb.agregarObjeto("autorizaUCP", 1);
				accionweb.getSesion().setAttribute("autorizaUCP", 1);
			}
			if(ss.getIndprc().trim().equals("Z")){
				accionweb.agregarObjeto("autorizaDIRPRE", 1);
				accionweb.getSesion().setAttribute("autorizaDIRPRE", 1);
			}
			if(ss.getIndprc().trim().equals("F")){
				accionweb.agregarObjeto("autorizaFinanzas", 1);
				accionweb.getSesion().setAttribute("autorizaFinanzas", 1);
			}
		 }
	}
	if(rutUsuario == 6115981 || // PATRICIO PINTO
			   rutUsuario == 7714156 || // miguel angel martinez
			   rutUsuario == 8399195 || // ANGELA LOYOLA
			   rutUsuario == 13560745 || // CRISTIAN PEREZ
			   rutUsuario == 9696493) // MARTA BARRERA
	{  accionweb.agregarObjeto("autorizaClave", 1);
	       accionweb.getSesion().setAttribute("autorizaClave", 1);   
	}		   
		if(rutOrigen > 0){
			this.limpiaSimulador(accionweb);
		}
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		if(listaSolicitudes != null && listaSolicitudes.size() > 0){
			accionweb.getSesion().removeAttribute("listaSolicitudes");
			accionweb.getSesion().setAttribute("listaSolicitudes", null);
		}
		
		accionweb.agregarObjeto("esFactura", 1);
		accionweb.agregarObjeto("opcionMenu", tipo);
		
		accionweb.agregarObjeto("listaAutorizaciones", listaAutorizaciones);
	}

	public void limpiaSimulador(AccionWeb accionweb) throws Exception {
		if (accionweb.getSesion().getAttribute("listaUnidad") != null)
			accionweb.getSesion().setAttribute("listaUnidad", null);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutOrigen  = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen") +"","0"));
		String nomSimulacion = "";
		if(accionweb.getSesion().getAttribute("opcionMenu") != null){
			accionweb.getSesion().removeAttribute("opcionMenu");
			accionweb.getSesion().setAttribute("listaSolicitudes", null);
		}
		
		/*hacer este rut rutUsuario*/
		if(rutOrigen != 0) // lo vuelve a lo original
		{
			rutUsuario = rutOrigen ;
			rutOrigen = 0;
		}
		 
		
		accionweb.getSesion().setAttribute("rutUsuario", rutUsuario);
		accionweb.getSesion().setAttribute("rutOrigen", rutOrigen);
		accionweb.getSesion().setAttribute("nomSimulacion", nomSimulacion);
		accionweb.agregarObjeto("rutOrigen", rutOrigen);
		accionweb.agregarObjeto("nomSimulacion", nomSimulacion);
		
	}
	
	public void cargaAutorizaFacturacion(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);	
		
		PreswBean preswbean = null;
		Collection<Presw18DTO> listaFacturacion = null;
		String titulo = "";
		String tituloDetalle1 = "";
		String tituloDetalle2 = "";
		String tipcue = "";
		//System.out.println("rutUsuario: "+rutUsuario);
		//System.out.println("dv: "+dv);
		switch (tipo) {
		case  2:// lista de solicitudes de facturacion para modificar
			{preswbean = new PreswBean("LSM",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			titulo = "Modificaci�n";
			break;
		}
		case 3: // lista de solicitudes de facturacion para autorizar RESPONSABLE DE LA CUENTA
			{String nomtipo = "";
			if(tipo == 3 ){
				titulo = "Autorizaci�n Responsable de la Cuenta";
				nomtipo = "LSA";
			}
			
			preswbean = new PreswBean(nomtipo,0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			preswbean.setTipcue(tipcue);
			tituloDetalle1 = "Autorizar";
			tituloDetalle2 = "Rechazar";
			accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
			accionweb.agregarObjeto("opcion2", 6);
			
			break;
			}
		case 4: // lista de solicitudes de facturacion para consulta
			{		
			String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
			int fechaInicio = Util.validaParametro(accionweb.getParameter("fechaInicio"),0);
			int fechaTermino = Util.validaParametro(accionweb.getParameter("fechaTermino"),0);
			String fechaIni = Util.validaParametro(accionweb.getParameter("fechaIni"),"");
			String fechaTer = Util.validaParametro(accionweb.getParameter("fechaTer"),"");
			int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0);
			if(fechaInicio > 201171231)
				fechaInicio = 201171231;
			if(fechaTermino > 201171231)
				fechaTermino = 201171231;
			
			if(fechaInicio > 0 && fechaTermino == 0 ){
				DateTool fechaActual = new DateTool();
				String dia = fechaActual.getDay()+"";
				if(dia.length() == 1)
					dia = "0" + dia;
				String mes = (fechaActual.getMonth()+1)+"";
				if(mes.length()== 1)
					mes = "0" + mes;
				fechaTermino = Integer.parseInt(String.valueOf(fechaActual.getYear()) + mes + dia);
				fechaTer = dia + "/" + mes + "/" + String.valueOf(fechaActual.getYear());
				
			}
			if(unidad >= 0){
			preswbean = new PreswBean("LSC",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			preswbean.setCoduni(unidad);
			preswbean.setTipcue(estado);
			preswbean.setFecmov(fechaInicio);
			preswbean.setNumcom(fechaTermino);
			listaFacturacion = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			}
			titulo = "Consulta General";	
			List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
			moduloFacturacion.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
			listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
			accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
		   	accionweb.agregarObjeto("estado", estado);
		   	accionweb.agregarObjeto("unidad", unidad);
		   	accionweb.agregarObjeto("fechaIni", fechaIni);
		   	accionweb.agregarObjeto("fechaTer", fechaTer);
			break;
			}
		case 5: // lista de FACTURAS para Recepci�n en Finanzas
			{
			int sede = Util.validaParametro(accionweb.getParameter("sede"),0);	
			String estadoFactura = Util.validaParametro(accionweb.getParameter("estadoFactura"),"");
			preswbean = new PreswBean("LSF",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			preswbean.setSucur(sede);
			preswbean.setTipcue(estadoFactura);
			titulo = "Recepci�n Finanzas";
			tituloDetalle1 = "Recepci�n";
			tituloDetalle2 = "Rechazar";
			accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
			///   asi estaba    accionweb.agregarObjeto("opcion2", 5);
			accionweb.agregarObjeto("opcion2", 7);
			List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
			listSede = moduloFacturacion.getListaSede(accionweb.getReq());
			accionweb.agregarObjeto("listSede", listSede);
			accionweb.agregarObjeto("sede", sede);
			accionweb.agregarObjeto("estadoFactura", estadoFactura);
			break;
			}
		case 6:// lista de solicitudes de facturas ingresadas por el rut
		{
		   titulo = "Consulta Ingresadas";
		   preswbean = new PreswBean("LSR",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
		   break;
		}
		case 7:// Ingreso de notas de cr�dito  
		{
		   int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"),0);
		   String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		   String tipdoc = Util.validaParametro(accionweb.getParameter("tipoDocumento"),"");
		   int numeroFactura = Util.validaParametro(accionweb.getParameter("numeroFactura"), 0); 
		   String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		   
		   titulo = "Ingreso";
			
		
		  
		   List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
    	   listSede = moduloFacturacion.getListaSede(accionweb.getReq());
    	   accionweb.agregarObjeto("listSede", listSede);
		   accionweb.agregarObjeto("rutnum", rutnum);
		   accionweb.agregarObjeto("dvrut", dvrut);
		   accionweb.agregarObjeto("tipdoc", tipdoc);
		   accionweb.agregarObjeto("numeroFactura", numeroFactura);
		   
		   break;
		}
		case 8:// COnsulta de liquidaciones de facturacion  
		{
			int fechaInicio = Util.validaParametro(accionweb.getParameter("fechaInicio"),0);
			int fechaTermino = Util.validaParametro(accionweb.getParameter("fechaTermino"),0);
			String fechaIni = Util.validaParametro(accionweb.getParameter("fechaIni"),"");
			String fechaTer = Util.validaParametro(accionweb.getParameter("fechaTer"),"");
			int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0);
			if(fechaInicio > 0 && fechaTermino == 0 ){
				DateTool fechaActual = new DateTool();
				String dia = fechaActual.getDay()+"";
				if(dia.length() == 1)
					dia = "0" + dia;
				String mes = (fechaActual.getMonth()+1)+"";
				if(mes.length()== 1)
					mes = "0" + mes;
				fechaTermino = Integer.parseInt(String.valueOf(fechaActual.getYear()) + mes + dia);
				fechaTer = dia + "/" + mes + "/" + String.valueOf(fechaActual.getYear());
				
			}
	
			   titulo = "Consulta de liquidaciones de Facturaci�n";
			   List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
				moduloFacturacion.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
				listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
				accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
		       List<Presw21DTO> listaLiquidacion = null;
		       Collection<Presw21DTO> listaPresw21 = null;
			   preswbean = new PreswBean("LLP",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			   preswbean.setCoduni(unidad);
			   preswbean.setFecmov(fechaInicio);
			   preswbean.setNumcom(fechaTermino);
			   listaPresw21 = (Collection<Presw21DTO>) preswbean.consulta_presw21();
			   if(listaPresw21.size()>0)
					accionweb.agregarObjeto("hayDatoslista", "1");
			   accionweb.agregarObjeto("unidad", unidad);
			   	accionweb.agregarObjeto("fechaIni", fechaIni);
			   	accionweb.agregarObjeto("fechaTer", fechaTer);
			   	accionweb.agregarObjeto("listaPresw21", listaPresw21);
			   break;
		}
		case 11: // lista de boletas a honorarios para consulta
		{		
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0);
		if(unidad >= 0){
		preswbean = new PreswBean("LSR",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
		preswbean.setCoduni(unidad);
		preswbean.setTipcue(estado);
		listaFacturacion = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		}
		titulo = "Consulta";
		if(tipo == 11){
			titulo = "Existentes";
			int volver = Util.validaParametro(accionweb.getParameter("volver"),0);
			if(volver == 1) {
			int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"),0);
			String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
			String nomIdentificador = Util.validaParametro(accionweb.getParameter("nomIdentificador"),"");
			if(rutnum > 0 /*&& nomIdentificador.trim().equals("")*/)
				this.verificaRut(accionweb);
			accionweb.agregarObjeto("dvrut", dvrut);
			accionweb.agregarObjeto("rutnum", rutnum);
			accionweb.agregarObjeto("nomIdentificador", nomIdentificador);
			}
		}
		List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
		moduloFacturacion.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
		listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
	   	accionweb.agregarObjeto("estado", estado);
	   	accionweb.agregarObjeto("unidad", unidad);
		break;
		}	
		
		case  12:// lista de Ingresadores
		{	int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0);
		    String accion = Util.validaParametro(accionweb.getParameter("accion"),""); 
		    if(!accion.trim().equals("")){
		    	int rutide = Util.validaParametro(accionweb.getParameter("rutide"), 0);
		    	String digide = Util.validaParametro(accionweb.getParameter("digide"),""); 
		    	try {
		    	boolean graba = moduloFacturacion.saveDeleteIngresador(rutide, digide, unidad, accion);
		    	if(graba)
		    		accionweb.agregarObjeto("mensaje", "registro exitoso.");
		    	else
		    		accionweb.agregarObjeto("mensaje", "problemas al registrar.");
		    	Thread.sleep(500);
		    	} catch (Exception e) {
					accionweb.agregarObjeto("mensaje", "No guardado saveDeleteIngresador.");
					accionweb.agregarObjeto("exception", e);
					e.printStackTrace();
				}
		    }
		
			List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
			moduloFacturacion.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
			listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
			accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
		   	accionweb.agregarObjeto("unidad", unidad);
			if(unidad > 0){
		    	preswbean = new PreswBean("OLI",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
		    	preswbean.setCoduni(unidad);
		    	Collection<Presw25DTO> listaValePago25 = (Collection<Presw25DTO>) preswbean.consulta_presw25();
		    	accionweb.agregarObjeto("hayDatoslista", 1);
		    	accionweb.agregarObjeto("listaValePago25", listaValePago25);
			}
			titulo = "Ingresadores";
		break;
	}
		case  13:// consulta clave de Portal de Pagos de Clientes
		{	String accion = Util.validaParametro(accionweb.getParameter("accion"),""); 
		    if(!accion.trim().equals("")){
		    	int rutide = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		    	String digide = Util.validaParametro(accionweb.getParameter("dvrut"),""); 	
		    	preswbean = new PreswBean("OCP",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
		    	preswbean.setNumdoc(rutide);
		    	preswbean.setTipcue(digide);
		    	
		    	Collection<Presw25DTO> listaValePago25 = (Collection<Presw25DTO>) preswbean.consulta_presw25();
		    	if(listaValePago25 != null && listaValePago25.size()> 0){
		    	 for (Presw25DTO ss : listaValePago25){
		    		 if(ss.getNumreq() == 0){
		    			accionweb.agregarObjeto("rutCliente", moduloFacturacion.formateoNumeroEntero(rutide)+"-"+digide);
		    			accionweb.agregarObjeto("nomCliente", ss.getComen5());
		    			accionweb.agregarObjeto("direccion", ss.getComen1());
		    			accionweb.agregarObjeto("comuna", ss.getComen2());
		    			accionweb.agregarObjeto("ciudad", ss.getComen3());
		    			accionweb.agregarObjeto("giro", ss.getComen4());
		    			accionweb.agregarObjeto("clave", ss.getComen6());
		    		 } else {
		    			 switch(ss.getNumreq()){
		    			 case 1:{ accionweb.agregarObjeto("mensaje", "Cliente no existe.");
		    			 break;
		    			 }
		    			 case 2:{ accionweb.agregarObjeto("mensaje", "Cliente sin clave.");
		    			 break;
		    			 }
		    			 case 3:{ accionweb.agregarObjeto("mensaje", "Cliente sin facturas en cuentas autorizadas para usuario.");
		    			 break;
		    			 }
		    			 }
		    			 }
		    		 }
		    			   		 
		    	 }
		    	}		
		     	
		  
			titulo = "Consulta Clave de Cliente en Portal de Pagos";
		break;
	}
		}
		if(tipo != 4 && tipo != 12 && tipo != 7  && tipo != 8  && tipo != 13)
			listaFacturacion = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		
	
	    if(listaFacturacion != null && listaFacturacion.size() > 0)
        	accionweb.agregarObjeto("hayDatoslista", "1");
	    
	    
        accionweb.agregarObjeto("listaFacturacion", listaFacturacion);
      	accionweb.agregarObjeto("esFactura", "1");
      	accionweb.agregarObjeto("opcion", String.valueOf(tipo));
    	accionweb.agregarObjeto("opcionMenu", tipo);
      	accionweb.agregarObjeto("titulo", titulo);
      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
     /* 	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));*/
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
       	if(accionweb.getSesion().getAttribute("autorizaClave") != null)
			accionweb.agregarObjeto("autorizaClave", accionweb.getSesion().getAttribute("autorizaClave"));
   }
	public void cargaIngresoIngresador(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
		String nomUnidad = "";
		String titulo = "Nuevo Ingresador";
	
		 moduloFacturacion.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
		 List<Presw18DTO>    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		 if(listaUnidades != null && listaUnidades.size() > 0){
			 for (Presw18DTO ss : listaUnidades){
				 if(ss.getCoduni() == unidad)
					 nomUnidad = ss.getDesuni();
			 }
		 }
		accionweb.agregarObjeto("unidad", unidad);
		accionweb.agregarObjeto("nomUnidad", nomUnidad);
      	accionweb.agregarObjeto("esFactura", 1);
      	accionweb.agregarObjeto("opcion", tipo);
    	accionweb.agregarObjeto("opcionMenu", tipo);
      	accionweb.agregarObjeto("titulo", titulo);
	}
	public void verificaRut(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		int identificador = Util.validaParametro(accionweb.getParameter("identificador"), 0);
		int opcion = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		if(listaSolicitudes != null && listaSolicitudes.size() > 0){
			accionweb.getSesion().removeAttribute("listaSolicitudes");
			accionweb.getSesion().setAttribute("listaSolicitudes", null);
		}
	
		String nombre = "";
		String domicilio = "";
		String comuna = "";
		String ciudad = "";
		String giro = "";
		long codBan = 0;
		String cuentaBanco = "";
		String correo = "";
		int error = 0;
		if(rutnum > 0){
			Collection<Presw25DTO> lista = moduloFacturacion.getVerificaRut(rutnum, dvrut);
			if(lista != null && lista.size() > 0){
				 for (Presw25DTO ss : lista){
					// nombre = ss.getDesuni();
					 nombre = ss.getMotiv1() + ss.getMotiv2().trim();
					 domicilio = ss.getComen1();
					 comuna = ss.getComen2();
					 ciudad = ss.getComen3();
					 giro = ss.getComen4();
					 error = ss.getItedoc();
					 codBan = ss.getPres03();
					 cuentaBanco = ss.getMotiv1();
					 correo = ss.getMotiv2();
				   }
			}
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
			
		}
	   	if(!nombre.trim().equals("") && error != 4){
	    		accionweb.agregarObjeto("hayDatoslista", 1);
	    		accionweb.agregarObjeto("nomCliente", nombre);
	    		accionweb.agregarObjeto("domicilioCliente", domicilio);
	    		accionweb.agregarObjeto("comunaCliente", comuna);
	    		accionweb.agregarObjeto("ciudadCliente", ciudad);
	    		accionweb.agregarObjeto("giroCliente", giro);
	    		accionweb.agregarObjeto("codBanco", codBan);
	    		accionweb.agregarObjeto("cuentaBanco", cuentaBanco);
	    		accionweb.agregarObjeto("correo", correo);
	    		List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
	    		List<Presw18DTO> listBanco = new ArrayList<Presw18DTO>();
	    		listBanco = moduloFacturacion.getListaBanco(accionweb.getReq());
	    		listSede = moduloFacturacion.getListaSede(accionweb.getReq());
	    		String comboSede = "<select name=\"sede\" id=\"sede\" class=\"estilo_Azul\" onChange=\"\" >"+
	    		                   "<option value=\"-1\" selected=\"selected\">Seleccione</option> ";
	    		  //  if(identificador == 1)
	    		    for (Presw18DTO ss : listSede){
	    		    	comboSede += "<option value=" + ss.getNummes() + ">" + ss.getDesuni() + "</option>";
	    		    	
	    		    }
	    		    comboSede += "</select>";
	    		
	    		List<Presw18DTO> listServicio = new ArrayList<Presw18DTO>();
	    		listServicio = moduloFacturacion.getListaServicio(accionweb.getReq());    
	    		String comboServicio = "<select name=\"tipoServicio\" id=\"tipoServicio\" class=\"estilo_Azul\" onChange=\"\" >"+
	                   					"<option value=\"-1\" selected=\"selected\">Seleccione</option> ";
			    for (Presw18DTO ss : listServicio){
			    	comboSede += "<option value=" + ss.getTipmov() + ">" + ss.getTipmov().trim() +" - " + ss.getDesuni() +" - " + ss.getIddigi()  +" - " + ss.getNummes()+ "</option>";
	    	
			    }
			    comboSede += "</select>";    
			    List<Presw18DTO> listCondicionVenta = new ArrayList<Presw18DTO>();
			    listCondicionVenta = moduloFacturacion.getListaCondicionVenta(accionweb.getReq());    
	        
			    accionweb.agregarObjeto("comboServicio", comboServicio);
    		    accionweb.agregarObjeto("listServicio", listServicio);  
	    		accionweb.agregarObjeto("comboSede", comboSede);
	    		accionweb.agregarObjeto("listSede", listSede);
	    		accionweb.agregarObjeto("listBanco", listBanco);
	    		accionweb.agregarObjeto("listCondicionVenta", listCondicionVenta);
	    		    
	    } else{
	        if(error == 4)// error agregado el 28/03/2013 segun req:3801	   		 
	        	accionweb.agregarObjeto("mensaje", "No se puede emitir factura a este Cliente.\\nNo ha informado pago de facturas con vencimiento mayor a 6 meses.");
	         else
	        	accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado. Debe agregarlo.");
	    }
	  
	   
	    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
	    moduloFacturacion.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		accionweb.agregarObjeto("identificador", identificador);		  		  
		accionweb.agregarObjeto("esFactura", 1);   
		accionweb.agregarObjeto("opcion", opcion);
	
		accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);  
 /*   	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));*/
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
       	if(accionweb.getSesion().getAttribute("autorizaClave") != null)
			accionweb.agregarObjeto("autorizaClave", accionweb.getSesion().getAttribute("autorizaClave"));
	
	}
	public void verificaPagoCuenta(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int cuentaPresup = Util.validaParametro(accionweb.getParameter("cuentaPresup"), 0);
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
	// mb reemplaza VFC por VVU		
		String advertenciaCliente = "";
		String mensajeAlertCliente = "";
		String advertenciaUnidad = "";
		String mensajeAlertUnidad = "";
		String linea1 = "";
		String linea2 = "";
		String linea3 = "";
		String indicadorCliente = "";
		String motivo1 = "";
		String motivo2 = "";
		String motivo3 = "";
		String indicadorUnidad = "";
		long pres01 = 0;
		int pres02 = 0;
		
			Collection<Presw25DTO> lista = moduloFacturacion.getVerificaPagoCuenta(rutnum, dvrut, cuentaPresup);
			if(lista != null && lista.size() > 0){
				 for (Presw25DTO ss : lista){
						linea1 = ss.getComen1().trim();
						linea2 = ss.getComen2().trim();
						linea3 = ss.getComen3().trim();
						indicadorCliente = ss.getComen4().trim();
						motivo1 = ss.getMotiv1().trim();
						motivo2 = ss.getMotiv2().trim();
						motivo3 = ss.getMotiv3().trim();
						indicadorUnidad = ss.getMotiv4().trim();
						pres01 = ss.getPres01();
				   }
			}
		    if(!indicadorCliente.trim().equals("") || !indicadorUnidad.trim().equals("")){
		    	if(indicadorCliente.trim().equals("S") || indicadorCliente.trim().equals("s")){
		    		mensajeAlertCliente = linea1  +"\n" + linea2 + "\n" + linea3 ;
		    	}else {
		    		advertenciaCliente = linea1  +"\n" + linea2 + "\n" + linea3 ;
		    	}
		    	if(indicadorUnidad.trim().equals("S") || indicadorUnidad.trim().equals("s")){
		    		mensajeAlertUnidad = motivo1  +"\n" + motivo2 + "\n" + motivo3 ;
		    	}else {
		    		advertenciaUnidad = motivo1  +"\n" + motivo2 + "\n" + motivo3 ;
		    	}
		    	if(!advertenciaCliente.trim().equals(""))
		    		accionweb.agregarObjeto("advertenciaCliente", advertenciaCliente);
		    	if(!mensajeAlertCliente.trim().equals(""))
		    		accionweb.agregarObjeto("mensajeAlertCliente", mensajeAlertCliente);
		      	if(!advertenciaUnidad.trim().equals(""))
		    		accionweb.agregarObjeto("advertenciaUnidad", advertenciaUnidad);
		    	if(!mensajeAlertUnidad.trim().equals(""))
		    		accionweb.agregarObjeto("mensajeAlertUnidad", mensajeAlertUnidad);
		     	accionweb.agregarObjeto("indicadorCliente", indicadorCliente);
		    	accionweb.agregarObjeto("indicadorUnidad", indicadorUnidad);
		    }
			  
			
		
	}	
	
	public void cargaIngresoFacturacion(AccionWeb accionweb) throws Exception {
		verificaRut(accionweb);
		if(accionweb.getReq().getAttribute("mensaje") == null || accionweb.getReq().getAttribute("mensaje").toString().trim().equals(""))
			cargaAutorizaFacturacion(accionweb);
		return;
	}
	public void cargaResultadoSolicitudFactura(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		String descripcion = Util.validaParametro(accionweb.getParameter("descripcion"), "");
		String afecto = Util.validaParametro(accionweb.getParameter("afecto"), "");
		long valor = Util.validaParametro(accionweb.getParameter("valor"), 0);
		long valorIVA = Util.validaParametro(accionweb.getParameter("valorIVA"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);	
		int cuentaPresup = Util.validaParametro(accionweb.getParameter("cuentaPresup"), 0);
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
		String indicadorUnidad = Util.validaParametro(accionweb.getParameter("indicadorUnidad"),"");
		String indicadorCliente = Util.validaParametro(accionweb.getParameter("indicadorCliente"),"");
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		
		/*aca qued� para validar*/
		if(indicadorUnidad.trim().equals("") || indicadorUnidad.trim().equals("S") || indicadorCliente.trim().equals("") || indicadorCliente.trim().equals("S")){
		try {
			String advertencia = "";
			String mensajeAlertCliente = "";
			String mensajeAlertUnidad = "";
			String linea1 = "";
			String linea2 = "";
			String linea3 = "";
			String motivo1 = "";
			String motivo2 = "";
			String motivo3 = "";
			long pres01 = 0;
			int pres02 = 0;
		
				Collection<Presw25DTO> lista = moduloFacturacion.getVerificaPagoCuenta(rutnum, dvrut, cuentaPresup);
				if(lista != null && lista.size() > 0){
					 for (Presw25DTO ss : lista){
							linea1 = ss.getComen1().trim();
							linea2 = ss.getComen2().trim();
							linea3 = ss.getComen3().trim();
							indicadorCliente = ss.getComen4().trim();
							motivo1 = ss.getMotiv1().trim();
							motivo2 = ss.getMotiv2().trim();
							motivo3 = ss.getMotiv3().trim();
							indicadorUnidad = ss.getMotiv4().trim();
							pres01 = ss.getPres01();
					   }
				}
			    if(!indicadorCliente.trim().equals("") || !indicadorUnidad.trim().equals("")){
			    	if(indicadorCliente.trim().equals("N") || indicadorCliente.trim().equals("n")){
			    		mensajeAlertCliente = linea1  +"\n" + linea2 + "\n" + linea3 ;
			    	}
			    	if(indicadorUnidad.trim().equals("N") || indicadorUnidad.trim().equals("n")){
			    		mensajeAlertUnidad = motivo1  +"\n" + motivo2 + "\n" + motivo3 ;
			    	}
			    	if(!advertencia.trim().equals(""))
			    		accionweb.agregarObjeto("advertencia", advertencia);
			    	if(!mensajeAlertCliente.trim().equals(""))
			    		accionweb.agregarObjeto("mensajeAlertCliente", mensajeAlertCliente);
			    	if(!mensajeAlertUnidad.trim().equals(""))
			    		accionweb.agregarObjeto("mensajeAlertUnidad", mensajeAlertUnidad);
			      }
	  // moduloFacturacion.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    if(!indicadorCliente.trim().equals("N") && !indicadorUnidad.trim().equals("N")) {
				List<Presw18DTO> listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
			    moduloFacturacion.getAgregaListaSolicitaFactura(accionweb.getReq());
			
				listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
				if (listaSolicitudes != null && listaSolicitudes.size() > 0)
		           	accionweb.agregarObjeto("afecto",afecto.trim());  
		       	 
				
			    }
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "cargaResultadoSolicitudFactura.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		}
		accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes); 		
    	accionweb.agregarObjeto("registra", 1);	
	   	accionweb.agregarObjeto("esFactura", 1);   
		accionweb.agregarObjeto("identificador", 1);  
	    accionweb.agregarObjeto("valorIVA", valorIVA);
	    accionweb.agregarObjeto("opcion", tipo);
	   	accionweb.agregarObjeto("indicadorCliente", indicadorCliente);
    	accionweb.agregarObjeto("indicadorUnidad", indicadorUnidad);

		
	}
	 synchronized public void eliminaResultadoSolicitudFact(AccionWeb accionweb) throws Exception {
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		String afecto = Util.validaParametro(accionweb.getParameter("afecto"), "");
		long valorIVA = Util.validaParametro(accionweb.getParameter("valorIVA"), 0);
		
		
		   moduloFacturacion.getEliminaListaSolicitaFact(accionweb.getReq());
		   	accionweb.agregarObjeto("mensaje", "Se elimin� satisfactoriamente.");
		    	
			List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		   	accionweb.agregarObjeto("esFactura", 1);   
			accionweb.agregarObjeto("identificador", 1);  
			long total = 0;
			//if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				accionweb.agregarObjeto("registra", 1);
				
			//}
		    accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes); 
		   	accionweb.agregarObjeto("afecto",afecto.trim()); 
		    accionweb.agregarObjeto("valorIVA", valorIVA);
			if(tipo > 0)
			 accionweb.agregarObjeto("opcion", tipo);
			
		}
	 synchronized public void modificaResultadoSolicitudFact(AccionWeb accionweb) throws Exception {
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		String afecto = Util.validaParametro(accionweb.getParameter("afecto"), "");
		long valorIVA = Util.validaParametro(accionweb.getParameter("valorIVA"), 0);
		
		
		   moduloFacturacion.getModificaSolicitaFact(accionweb.getReq());
		   	accionweb.agregarObjeto("mensaje", "Se modific� satisfactoriamente.");
		    	
			List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		   	accionweb.agregarObjeto("esFactura", 1);   
			accionweb.agregarObjeto("identificador", 1);  
			long total = 0;
			//if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				accionweb.agregarObjeto("registra", 1);
				
			//}
		    accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes); 
		   	accionweb.agregarObjeto("afecto",afecto.trim()); 
		    accionweb.agregarObjeto("valorIVA", valorIVA);
			if(tipo > 0)
			 accionweb.agregarObjeto("opcion", tipo);
			
		}
	 synchronized public void registraFacturacion(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
	
	    int numFactura = 0;
	    int cuentaPresup = Util.validaParametro(accionweb.getParameter("cuentaPresup"), 0);
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"), "");
        int error = 0;
        String mensaje = "";
	    Collection<Presw25DTO> lista = moduloFacturacion.getVerificaPagoCuenta(rutnum, dvrut, cuentaPresup);
			if(lista != null && lista.size() > 0){
				 for (Presw25DTO ss : lista){
					 error = ss.getItedoc();
					
				   }
			}
			
			if(error == 2){
				    mensaje = "Cliente con facturas con vencimiento mayor a tope m�ximo de mora, no se puede generar nuevos documentos. Consultas adicionales a Direcci�n de Finanzas.";
					accionweb.agregarObjeto("mensaje", mensaje);
					accionweb.agregarObjeto("esFactura", 1); 
			} else {
	
			        numFactura = moduloFacturacion.getRegistraFactura(accionweb.getReq(),rutUsuario,dv);
			        if(numFactura > 0){
			   	       accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa la Solicitud de Facturaci�n, el n�mero interno es: "+ numFactura);
			   	    if (listaSolicitudes != null && listaSolicitudes.size() > 0){
						accionweb.agregarObjeto("registra", 1);
					    accionweb.getSesion().removeAttribute("listaSolicitudes");
					    accionweb.getSesion().setAttribute("listaSolicitudes", null);
							
					}
			   	    this.cargarMenuFacturacion(accionweb);
			        } else
			        	accionweb.agregarObjeto("mensaje", "No se registr� la Solicitud de facturaci�n.");
				  		accionweb.agregarObjeto("esFactura", 1);   
				}
	/*		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));*/
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	       	if(accionweb.getSesion().getAttribute("autorizaClave") != null)
				accionweb.agregarObjeto("autorizaClave", accionweb.getSesion().getAttribute("autorizaClave"));

	}
	 synchronized public void registraNotaCredito(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		String tipoNota = Util.validaParametro(accionweb.getParameter("tipoNota"),"");
		String mensajeNota = "";
		if(tipoNota.trim().equals("NCE"))
			mensajeNota ="Nota de Cr�dito";
		else
			mensajeNota ="Nota de D�bito";
		
	    int numFactura = 0;
	    
	    numFactura = moduloFacturacion.getRegistraFactura(accionweb.getReq(),rutUsuario,dv);
	    if(numFactura > 0){
	   	    accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa la "+mensajeNota+", el n�mero interno es: "+ numFactura);
	   	    if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				accionweb.agregarObjeto("registra", 1);
			    accionweb.getSesion().removeAttribute("listaSolicitudes");
			    accionweb.getSesion().setAttribute("listaSolicitudes", null);
			}
	   	 this.cargarMenuFacturacion(accionweb);
	    } else
	    	accionweb.agregarObjeto("mensaje", "No se registr� la " + mensajeNota +".");
		  	accionweb.agregarObjeto("esFactura", 1);   

	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	       	if(accionweb.getSesion().getAttribute("autorizaClave") != null)
				accionweb.agregarObjeto("autorizaClave", accionweb.getSesion().getAttribute("autorizaClave"));
	       	accionweb.agregarObjeto("titulo", "Ingreso");
	}
	
	public void consultaFactura(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
		long sede = Util.validaParametro(accionweb.getParameter("sede"), 0); 
		String estadoFactura = Util.validaParametro(accionweb.getParameter("estadoFactura"),"");
		String tipmotiv = Util.validaParametro(accionweb.getParameter("tipmotiv"),"");
		String nomIdentificador = "";
		long numFactura = Util.validaParametro(accionweb.getParameter("numFactura"),0);
		//System.out.println("numDoc: "+numDoc);
		if(sede > 0)
			accionweb.agregarObjeto("sedeFactura", sede);
		if(!estadoFactura.trim().equals(""))
			accionweb.agregarObjeto("estadoFactura", estadoFactura);
		Presw19DTO preswbean19DTO = new Presw19DTO();
		String titulo = "";
		String tituloDetalle1 = "";
		preswbean19DTO.setTippro("DSF");
		preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
		preswbean19DTO.setDigide(dv);
		//if(numFactura > 0)
		//	preswbean19DTO.setNumdoc(new BigDecimal(numFactura));
		//else
			preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
		
		
		String nombreCliente = "";
	    String direccion = "";
	    String comuna = "";
	    String ciudad = "";
	    String giro = "";
	    String digide = "";
	    long rutide = 0;
		int rutnum = 0;
		String dvrut = "";  
	   
	
		String readonly = "readonly";
		

		
/*capturar los datos */
		
		if(tipo == 2) // lista de facturas para consultar
		{	titulo = "Modifica ";
			tituloDetalle1 = "Modificar";
			readonly = "";
			}
		if(tipo == 3) // lista de facturas para autorizar
			{
			titulo = "Autorizaci�n Responsable de la Cuenta";
			tituloDetalle1 = "Autorizar";		 
			}
		if(tipo == 4 || tipo == 6) // lista de facturas para consulta
			{
			titulo = "Consulta ";
			tituloDetalle1 = "Consultar";
			}
		if(tipo == 5) // lista de facturas para Recepci�n de facturas en Finanzas
			{
			titulo = "Recepci�n ";
			tituloDetalle1 = "Recepci�n";
			}
		if(tipo == 7) // Notas de cr�dito
		{
		titulo = "Nota de Credito ";
		tituloDetalle1 = "";
		}
		if(tipo == 11) // muestra factura para copiar
		{	titulo = "Ingreso";
			tituloDetalle1 = "Ingreso";
			readonly = "";
			rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
			dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");  
		
			String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		
			if(rutnum > 0){
				Collection<Presw25DTO> lista = moduloFacturacion.getVerificaRut(rutnum, dvrut);
				if(lista != null && lista.size() > 0){
					 for (Presw25DTO ss : lista){
						 //nombreCliente = ss.getDesuni();
						 direccion = ss.getComen1();
						 comuna = ss.getComen2();
						 ciudad = ss.getComen3();
						 giro = ss.getComen4();
					   }
					 if(nombreCliente.trim().equals("")){
						 if(tipo != 7)
							accionweb.agregarObjeto("mensaje", "Lo sentimos, esta factura que quiere copiar no existe.");
						 else
							accionweb.agregarObjeto("mensaje", "Lo sentimos, esta factura no existe.");
							
							accionweb.agregarObjeto("encuentra", "1");
					 }
				} else {
					 if(tipo != 7)
							accionweb.agregarObjeto("mensaje", "Lo sentimos, esta factura que quiere copiar no existe.");
						 else
							accionweb.agregarObjeto("mensaje", "Lo sentimos, esta factura no existe.");
						accionweb.agregarObjeto("encuentra", "1");
				}
				accionweb.agregarObjeto("rut_aux", rut_aux);  
				accionweb.agregarObjeto("rutnum", rutnum); 
				accionweb.agregarObjeto("dvrut", dvrut); 
				
		
		   	if(!nombreCliente.trim().equals("")){
		    		accionweb.agregarObjeto("hayDatoslista", 1);
		    		accionweb.agregarObjeto("nomCliente", nombreCliente);
		    		accionweb.agregarObjeto("domicilioCliente", direccion);
		    		accionweb.agregarObjeto("comunaCliente", comuna);
		    		accionweb.agregarObjeto("ciudadCliente", ciudad);
		    		accionweb.agregarObjeto("giroCliente", giro);

		
			}
			}
		}
	
		List<Presw18DTO>  listaSolicitudes = new ArrayList<Presw18DTO>();
		List<Presw18DTO>  listaHistorial = new ArrayList<Presw18DTO>();
	
		sede = 0;
	 
	
	    String tipoDocumento = "";
	    String tipoServicio = "";
	    String descripcion = "";
	    String nomSede = "";
	    String referencia = "";
	    String condicion = "";	   
	    String nomUnidad = "";
	    String numReferencia = "";
	    String tipoReferencia = "";
	    String codEstado = "";
	    long valorIVA = 0;
	    long codSede = 0;
	    long unidad = 0;
	    long vencimiento = 0;
	    long valorneto = 0;
	    long valorExento = 0;
	    long valorImpuesto = 0;
	    long valorTotal = 0;
	       
	    /*para nota de credito*/
	    String tipoNota = "";
	    String pagina = "consultaFactura.vm";
	    String tipoDoc = "";
	    
		List<Presw18DTO> listServicio = new ArrayList<Presw18DTO>();   	
	    
	    listServicio = moduloFacturacion.getListaServicio(accionweb.getReq());    
    	
		List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
		CocowBean cocowBean = new CocowBean();
		//System.out.println("preswbean19DTO : "+ preswbean19DTO.getRutide()+"  - "+preswbean19DTO.getNumdoc()+"--- "+preswbean19DTO.getTippro()+"  --- "+preswbean19DTO.getFecmov());
		listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
		//System.out.println("listCocow36DTO : "+ listCocow36DTO.size() );
		String afecto = "";
	    if(listCocow36DTO != null && listCocow36DTO.size() >0 ){
	    
	    for(Cocow36DTO ss: listCocow36DTO){	   		 
	    	    
	    	if(ss.getNomcam().trim().equals("TIPDOC")){
	    		tipoDocumento = ss.getValalf();	
	    		tipoDoc = tipoDocumento.trim();
		    		
	    		if(tipoDocumento.trim().equals("FEC")) 	    			
	    			tipoDocumento = tipoDoc + " - Afecto";	
	    		else {	    			
	    			if(tipoDocumento.trim().equals("FET")){
	    				tipoDocumento = tipoDoc + " - Exento";	    		    	
	    			}else{
	    				pagina = "notaCredito.vm";
	    				if(tipo == 2)
	    					tipoNota = tipoDocumento;
	    				
	    				}
	    			}
	    	}
	    	if(ss.getNomcam().trim().equals("TIPSER")){
	    		tipoServicio = ss.getValalf().trim();
	    		descripcion = ss.getResval();
	    		for(Presw18DTO dd:listServicio){
	  	      		if (dd.getTipmov().trim().equals(tipoServicio)){
	  	      			valorIVA =dd.getNummes();
	  	      			afecto = dd.getIddigi();
	  	      		}
	  	      	}
	    	}
	     	if(ss.getNomcam().trim().equals("SUCUR")){
	     		codSede = ss.getValnu1();
	     		nomSede = ss.getValalf();
	     	}
	     	if(tipo != 11){ // pues en este caaso vienen los datos del que se consulta
	     	if(ss.getNomcam().trim().equals("RUTIDE"))
	     		rutide = ss.getValnu1();
	     	if(ss.getNomcam().trim().equals("DIGIDE"))
	     		digide = ss.getValalf();   
	     	if(ss.getNomcam().trim().equals("NOMCLI"))
	     		nombreCliente = ss.getValalf();   
            if(ss.getNomcam().trim().equals("DIRCLI") /*&& tipo != 11*/)
            	direccion = ss.getValalf();
            if(ss.getNomcam().trim().equals("COMUNA") /*&& tipo != 11*/)
            	comuna = ss.getValalf();
            if(ss.getNomcam().trim().equals("CIUDAD") /*&& tipo != 11*/)
                ciudad = ss.getValalf();
            if(ss.getNomcam().trim().equals("GIRCLI"))
            	giro = ss.getValalf();
	     	}
            if(ss.getNomcam().trim().equals("REFCLI"))
            	referencia = ss.getValalf();
            if(ss.getNomcam().trim().equals("CONVEN"))
            	condicion = ss.getValalf();
            if(ss.getNomcam().trim().equals("CODUNI"))
            	unidad = ss.getValnu1();            
            if(ss.getNomcam().trim().equals("DESUNI"))
            	nomUnidad = ss.getValalf();
            if(ss.getNomcam().trim().equals("DIAVEN"))
            	vencimiento = ss.getValnu1();  
            if(ss.getNomcam().trim().equals("VALNET"))
            	valorneto = ss.getValnu1(); 
            if(ss.getNomcam().trim().equals("VALEXE"))
            	valorExento = ss.getValnu1(); 
            if(ss.getNomcam().trim().equals("VALIVA"))
            	valorImpuesto = ss.getValnu1(); 
            if(ss.getNomcam().trim().equals("VALTOT"))
            	valorTotal = ss.getValnu1(); 
            if(ss.getNomcam().trim().equals("NUMREF"))
            	numReferencia = ss.getValalf();
            if(ss.getNomcam().trim().equals("TIPREF"))
            	tipoReferencia = ss.getValalf();
            if(ss.getNomcam().trim().equals("ESTADO")){
            	estado = ss.getValalf();
            	codEstado =ss.getResval();
            }
            if(ss.getNomcam().trim().equals("NUMDOC"))
	     		numFactura = ss.getValnu1();  
            
            
            if(tipoDoc.equals("NCE") || tipoDoc.trim().equals("NDE")){
         	    if(ss.getNomcam().trim().equals("FACNOT"))
         	    	numFactura = ss.getValnu1();
         	   if(ss.getNomcam().trim().equals("TIDORE"))
         		  tipoNota = ss.getValalf();  
            
            }
                 
             if(ss.getNomcam().trim().equals("DETFAC")){
            	Presw18DTO presw18DTO = new Presw18DTO();
            	presw18DTO.setUsadom(ss.getValnu1());
               	presw18DTO.setDesuni(ss.getValalf());
            	listaSolicitudes.add(presw18DTO);
      
            }
       
            if(ss.getNomcam().trim().equals("HISTORIAL")){
            	Presw18DTO presw18DTO = new Presw18DTO();
            	presw18DTO.setUsadom(ss.getValnu1()); // es la fecha, 
            	presw18DTO.setUsadac(ss.getValnu2());//en caso de Resval="Autoriza" es la unidad
            	presw18DTO.setDesite(ss.getValalf()); // es eL NOMBRE DE USUARIO
            	presw18DTO.setDesuni(ss.getResval()); // es la accion, GLOSA DE LA OPERACI�N INGRESO, MODIFICACION, AUTORIZACION, PAGO, AUTORIZA FINANZAS
            	listaHistorial.add(presw18DTO);
            }
          
	    //	 }
	    }
	  
	    if(tipoDoc.equals("NCE") || tipoDoc.trim().equals("NDE")){
	    	for(Presw18DTO dd:listServicio){
  	      		if (dd.getTipmov().trim().equals(tipoServicio)){
  	      			valorIVA =dd.getNummes();
  	      			afecto = dd.getIddigi();
  	      		}
  	      	}
	    	
	    	
	    }
	   
	    String selected = (sede == 0)?"selected=\"selected\"":"";
	    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
	    listSede = moduloFacturacion.getListaSede(accionweb.getReq());
	    List<Presw18DTO> listCondicionVenta = new ArrayList<Presw18DTO>();
	    listCondicionVenta = moduloFacturacion.getListaCondicionVenta(accionweb.getReq());
	    accionweb.agregarObjeto("listCondicionVenta", listCondicionVenta);
	//System.out.println("listSede: "+listSede);
	    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
	    moduloFacturacion.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
	    //System.out.println("listaUnidades: "+listaUnidades);
	    if(listaSolicitudes != null && listaSolicitudes.size() > 0){
	    	accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes);
	    	accionweb.getSesion().setAttribute("listaSolicitudes", listaSolicitudes);
	    }
	
	    if(listaHistorial != null && listaHistorial.size() > 0){
	    	accionweb.agregarObjeto("listaHistorial", listaHistorial);
	       	accionweb.getSesion().setAttribute("listaHistorial", listaHistorial);
	    }
	    accionweb.agregarObjeto("listSede", listSede);
	    
	 
	    accionweb.agregarObjeto("estado",estado);
	    accionweb.agregarObjeto("estadoFinal",estado);
	    
	
	
	    accionweb.agregarObjeto("cuentasPresupuestarias",listaUnidades);
	    accionweb.agregarObjeto("sede", sede);
	    accionweb.agregarObjeto("tipoDocumento", tipoDocumento);
	    accionweb.agregarObjeto("tipoServicio", tipoServicio);
	    accionweb.agregarObjeto("descripcion", descripcion);
	    accionweb.agregarObjeto("nomSede", nomSede);
	    accionweb.agregarObjeto("nombreCliente", nombreCliente);
	    accionweb.agregarObjeto("direccion", direccion);
	    accionweb.agregarObjeto("comuna", comuna);
	    accionweb.agregarObjeto("ciudad", ciudad);
	    accionweb.agregarObjeto("giro", giro);
	    accionweb.agregarObjeto("referencia", referencia);
	    accionweb.agregarObjeto("condicion", condicion);
	    accionweb.agregarObjeto("nomUnidad", nomUnidad);
	    accionweb.agregarObjeto("numReferencia", numReferencia);
	    accionweb.agregarObjeto("tipoReferencia", tipoReferencia);
	    accionweb.agregarObjeto("codSede", codSede);
	    accionweb.agregarObjeto("unidad", unidad);
	    accionweb.agregarObjeto("vencimiento", vencimiento);
	    accionweb.agregarObjeto("estado", estado);
	    accionweb.agregarObjeto("valorNeto", valorneto);
	    accionweb.agregarObjeto("valorExento", valorExento);
	    accionweb.agregarObjeto("valorImpuesto", valorImpuesto);
	    accionweb.agregarObjeto("valorTotal", valorTotal);
	    accionweb.agregarObjeto("numeroFactura", numFactura);
	    accionweb.agregarObjeto("tipoNota", tipoNota);
	    accionweb.agregarObjeto("afecto", afecto);
	    if(tipo == 11){ // pues en este caaso vienen los datos del que se consulta
	    	   accionweb.agregarObjeto("rutide", rutnum);
	    	   accionweb.agregarObjeto("digide", dvrut);
	    }else {
	    	   accionweb.agregarObjeto("rutide", rutide);
	    	   accionweb.agregarObjeto("digide", digide);
	    }
	
      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
      	accionweb.agregarObjeto("listServicio", listServicio);
      	accionweb.agregarObjeto("numDoc", numDoc);
      	accionweb.agregarObjeto("valorIVA", valorIVA);
      	accionweb.agregarObjeto("hayDatoslista", "1"); 
	    } else {
	    	 if(tipo != 7 && tipo != 4)
					accionweb.agregarObjeto("mensaje", "Lo sentimos, esta factura que quiere copiar no existe.");
			else{
				accionweb.agregarObjeto("mensaje", "Lo sentimos, esta factura no existe.");
				pagina = "muestraListaFac.vm";
			}
			accionweb.agregarObjeto("encuentra", "1");
			accionweb.agregarObjeto("nomCliente", "");
	    }

		    
      	accionweb.agregarObjeto("esFactura", "1");
      	accionweb.agregarObjeto("opcion", tipo);
      	accionweb.agregarObjeto("titulo", titulo);
    	accionweb.agregarObjeto("pagina", pagina);
    /*	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));*/
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
       	if(accionweb.getSesion().getAttribute("autorizaClave") != null)
			accionweb.agregarObjeto("autorizaClave", accionweb.getSesion().getAttribute("autorizaClave"));
// 	System.out.println("termina consulta ");	
	}
	public void ejecutaAccion(AccionWeb accionweb) throws Exception {
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		String estadoFactura = Util.validaParametro(accionweb.getParameter("estadoFactura"),"");
		String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
		
		switch (tipo){
		case 2: {
			this.actualizaFactura(accionweb);
			break;
		}
		case 3: {
			this.autorizaFactura(accionweb);
			break;
		}
		case 5:{
			this.recepcionaFactura(accionweb);
		break;
			
		}
		case 6:{// rechaza autorizaci�n
			this.rechazaAutFactura(accionweb);
		break;
			
		}
		case 7:{// rechaza Recepci�n
			this.rechazaRecepFactura(accionweb);
		break;
			
		}
		case 11:{// registra vale
			this.registraFacturacion(accionweb);
		break;
			
		}
		}
		if(!estadoFactura.trim().equals(""))
			accionweb.agregarObjeto("estadoFactura", estadoFactura);
		if(!estado.trim().equals(""))
			accionweb.agregarObjeto("estado", estado);
		if(tipo != 11)
			this.consultaFactura(accionweb);
		
	}
	 synchronized public void actualizaFactura(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		
	    boolean registra = false;
	    
	    registra = moduloFacturacion.getActualizaFactura(accionweb.getReq(),rutUsuario,dv,numdoc,"MBS");
	    if(registra){
	   	    accionweb.agregarObjeto("mensaje", "Se actualiz� en forma exitosa la Solicitud de Facturaci�n.");
	   	    if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				accionweb.agregarObjeto("registra", 1);
			    accionweb.getSesion().removeAttribute("listaSolicitudes");
			    accionweb.getSesion().setAttribute("listaSolicitudes", null);
			}
	   	// cargarMenu(accionweb);
	   	 this.consultaFactura(accionweb);
	    } else
	    	accionweb.agregarObjeto("mensaje", "No se modific� la Solicitud de Facturaci�n.");
		  	accionweb.agregarObjeto("esFactura", 1);   
	 
		 
	/*		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));*/
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	       	if(accionweb.getSesion().getAttribute("autorizaClave") != null)
				accionweb.agregarObjeto("autorizaClave", accionweb.getSesion().getAttribute("autorizaClave"));
}
	 synchronized public void actualizaNotaCredito(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		
	    boolean registra = false;
	    String mensaje = "Se actualiz� en forma exitosa la Nota de Cr�dito.";
	    String tipoNota = Util.validaParametro(accionweb.getParameter("tipoNota"),"");
	    if(tipoNota.trim().equals("NDE"))
	    	mensaje = "Se actualiz� en forma exitosa la Nota de D�bito.";
	    registra = moduloFacturacion.getActualizaFactura(accionweb.getReq(),rutUsuario,dv,numdoc,"MBS");
	    if(registra){
	   	    accionweb.agregarObjeto("mensaje", mensaje);
	   	    if (listaSolicitudes != null && listaSolicitudes.size() > 0){
				accionweb.agregarObjeto("registra", 1);
			    accionweb.getSesion().removeAttribute("listaSolicitudes");
			    accionweb.getSesion().setAttribute("listaSolicitudes", null);
			}
	   	// cargarMenu(accionweb);
	   	 this.consultaFactura(accionweb);
	    } else{
	    	   if(tipoNota.trim().equals("NDE"))
	   	    	mensaje = "No se modific� la Nota de D�bito.";
	    	   else
	    		mensaje = "No se modific� la Nota de Cr�dito.";
	    	accionweb.agregarObjeto("mensaje", mensaje);
	    }
		  	accionweb.agregarObjeto("esFactura", 1);   
	 
		 
	/*		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));*/
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	       	if(accionweb.getSesion().getAttribute("autorizaClave") != null)
				accionweb.agregarObjeto("autorizaClave", accionweb.getSesion().getAttribute("autorizaClave"));
}
	 synchronized public void autorizaFactura(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		String nomTipo = "ARS";		
		String tipCue = "";
		String nomAut = nomAut = " de Responsable de Cuenta. ";
		
		try {
		boolean graba = moduloFacturacion.saveAutoriza(numdoc, rutUsuario,dv,nomTipo,tipCue);
	    if(graba)
	    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la autorizaci�n" + nomAut);
	    else
	    	accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n" + nomAut);
    	} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado saveAutoriza.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
    	Thread.sleep(500);
	    this.cargaAutorizaFacturacion(accionweb);
	    
	/*	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));*/
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
       	if(accionweb.getSesion().getAttribute("autorizaClave") != null)
			accionweb.agregarObjeto("autorizaClave", accionweb.getSesion().getAttribute("autorizaClave"));
}
	 synchronized public void recepcionaFactura(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);		
		try { 
		boolean graba = moduloFacturacion.saveRecepciona(numdoc, rutUsuario,dv,"TFS");
	    if(graba)
	    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la Recepci�n.");
	    else
	    	accionweb.agregarObjeto("mensaje", "No se Grab� la Recepci�n.");
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado saveRecepciona.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		Thread.sleep(500);
	    this.cargaAutorizaFacturacion(accionweb);
		
	}
	 
	 synchronized public void recepcionaMasivo(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			String linea = Util.validaParametro(accionweb.getParameter("linea"),"");
			int numdoc = 0;
			try { 
				int index = 1;
				boolean graba = false;
				String mensaje = "";
				while(linea.length() > 0){
					index = linea.indexOf("@");
					if(index > 0)
						numdoc = Integer.parseInt(linea.substring(0,index));
					else{
						numdoc = Integer.parseInt(linea);
						linea = "";
					}
						graba = moduloFacturacion.saveRecepciona(numdoc, rutUsuario,dv,"TFS");
					
					    if(graba)
					    	mensaje += "Se grab&oacute; en forma exitosa la Recepci&oacute;n de la Boleta N " + numdoc + "<br>";
					    else
					    	mensaje += "No se Grab� la Recepci&oacute;n de la Boleta  N " + numdoc;
					    linea = linea.substring(index+1);
					   	Thread.sleep(100);
				}
			   	accionweb.agregarObjeto("mensaje", mensaje);	
		
				} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado recepcionaMasivo.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
			
			
		}
	 synchronized public void rechazaAutFactura(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);		
		try { 
		boolean graba = moduloFacturacion.saveRecepciona(numdoc, rutUsuario,dv,"RRS");
	    if(graba)
	    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el rechazo de la Autorizaci�n.");
	    else
	    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Autorizaci�n.");
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado rechazaAutFactura.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		Thread.sleep(500);
	    this.cargaAutorizaFacturacion(accionweb);
		
	}
	 synchronized public void rechazaRecepFactura(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);		
		try { 
		boolean graba = moduloFacturacion.saveRecepciona(numdoc, rutUsuario,dv,"RSF");
	    if(graba)
	    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el Rechazo de la Recepci�n.");
	    else
	    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo de la Recepci�n.");
		} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado saveRecepciona.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
		}
		Thread.sleep(500);
	    this.cargaAutorizaFacturacion(accionweb);
		
	}
	 synchronized public void eliminaFactura(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		String nomTipo = "";
		String tipCue = "";
		String nomAut = "";
		
			nomTipo = "ABS";
			if(tipo == 3)
				nomAut = " de Responsable de Cuenta. ";
		
		    boolean resultado = moduloFacturacion.getEliminaFactura(rutUsuario,dv,numdoc, nomTipo, tipCue );
		    if(resultado)
		    	accionweb.agregarObjeto("mensaje", "Qued� anulada la Solicitud de Facturaci�n " + nomAut);
		    else
		    	accionweb.agregarObjeto("mensaje", "Problemas al anular la Solicitud de Facturaci�n" + nomAut);
			this.cargaAutorizaFacturacion(accionweb);
			//accionweb.agregarObjeto("titulo","Modificaci�n");
			
	/*		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));*/
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	       	if(accionweb.getSesion().getAttribute("autorizaClave") != null)
				accionweb.agregarObjeto("autorizaClave", accionweb.getSesion().getAttribute("autorizaClave"));
}
	public void limpiaSolicitudFactura(AccionWeb accionweb) throws Exception {
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		listaSolicitudes = null;
		accionweb.getSesion().removeAttribute("listaSolicitudes");
		accionweb.getSesion().setAttribute("listaSolicitudes", null);
		accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes);
	
	}
	
	
	public void imprimeFactura(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
	

	    Presw19DTO preswbean19DTO = new Presw19DTO();
		String titulo = "";
		String tituloDetalle1 = "";
		preswbean19DTO.setTippro("DSF");
		preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
		preswbean19DTO.setDigide(dv);
		preswbean19DTO.setNumdoc(new BigDecimal(numDoc));

		
		Vector vecSolicitudes = new Vector();	
		Vector vecHistorial = new Vector();	
		Vector vecDatos = new Vector();	
	
	    String tipoDocumento = "";
	    String tipoServicio = "";
	    String descripcion = "";
	    String digide = "";
	    String nombreCliente = "";
	    String direccion = "";
	    String comuna = "";
	    String ciudad = "";
	    String giro = "";
	    String referencia = "";
	    String condicion = "";	   
	    String nomUnidad = "";
	    String numReferencia = "";
	    String tipoReferencia = "";
	    String codEstado = "";
	    String estado = "";
	    String nomSede = "";
	    String tipDoc = "";
	    String tipoNota = "";
	    long valorIVA = 0;
	    long codSede = 0;
	    long rutide = 0;
	    long unidad = 0;
	    long vencimiento = 0;
	    long valorneto = 0;
	    long valorExento = 0;
	    long valorImpuesto = 0;
	    long valorTotal = 0;
		int sede = 0;
		long numFactura = 0;
		long numNota = 0;
		
		List<Presw18DTO> listServicio = new ArrayList<Presw18DTO>();   	
	    
	    listServicio = moduloFacturacion.getListaServicio(accionweb.getReq());    
    	
		List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
		CocowBean cocowBean = new CocowBean();
		//System.out.println("preswbean19DTO : "+ preswbean19DTO.getRutide()+"  - "+preswbean19DTO.getNumdoc()+"--- "+preswbean19DTO.getTippro()+"  --- "+preswbean19DTO.getFecmov());
		listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
		//System.out.println("listCocow36DTO : "+ listCocow36DTO.size() );
	    if(listCocow36DTO != null && listCocow36DTO.size() >0 )
	    
	    for(Cocow36DTO ss: listCocow36DTO){
	   		   
	    	if(ss.getNomcam().trim().equals("TIPDOC")){
	    		tipoDocumento = ss.getValalf();	
	    		tipDoc = ss.getValalf();	
	    		accionweb.getSesion().setAttribute("afecto", tipoDocumento.trim());
	    		
	    		if(tipoDocumento.trim().equals("FEC")){
	    			for(Presw18DTO dd:listServicio){
		        		if (dd.getIddigi().trim().equals("FEC"))
		        			valorIVA =dd.getNummes();
		        	}
	    			  tipoDocumento = tipoDocumento.trim() + " - Afecto";
	    		}
	    		if(tipoDocumento.trim().equals("FET"))
	    			  tipoDocumento = tipoDocumento.trim() + " - Exento";	
	    	}
	    	if(ss.getNomcam().trim().equals("TIPSER")){
	    		tipoServicio = ss.getValalf();
	    		descripcion = ss.getResval();
	    	
	    	}
	     	if(ss.getNomcam().trim().equals("SUCUR")){
	     		codSede = ss.getValnu1();
	     		nomSede = ss.getValalf();
	     	}
	     	if(ss.getNomcam().trim().equals("RUTIDE"))
	     		rutide = ss.getValnu1();
	     	if(ss.getNomcam().trim().equals("DIGIDE"))
	     		digide = ss.getValalf();   
	     	if(ss.getNomcam().trim().equals("NOMCLI"))
	     		nombreCliente = ss.getValalf();   
            if(ss.getNomcam().trim().equals("DIRCLI"))
            	direccion = ss.getValalf();
            if(ss.getNomcam().trim().equals("COMUNA"))
            	comuna = ss.getValalf();
            if(ss.getNomcam().trim().equals("CIUDAD"))
                ciudad = ss.getValalf();
            if(ss.getNomcam().trim().equals("GIRCLI"))
            	giro = ss.getValalf();
            if(ss.getNomcam().trim().equals("REFCLI"))
            	referencia = ss.getValalf();
            if(ss.getNomcam().trim().equals("CONVEN"))
            	condicion = ss.getValalf();
            if(ss.getNomcam().trim().equals("CODUNI"))
            	unidad = ss.getValnu1();            
            if(ss.getNomcam().trim().equals("DESUNI"))
            	nomUnidad = ss.getValalf();
            if(ss.getNomcam().trim().equals("DIAVEN"))
            	vencimiento = ss.getValnu1();  
            if(ss.getNomcam().trim().equals("VALNET"))
            	valorneto = ss.getValnu1(); 
            if(ss.getNomcam().trim().equals("VALEXE"))
            	valorExento = ss.getValnu1(); 
            if(ss.getNomcam().trim().equals("VALIVA"))
            	valorImpuesto = ss.getValnu1(); 
            if(ss.getNomcam().trim().equals("VALTOT"))
            	valorTotal = ss.getValnu1(); 
            if(ss.getNomcam().trim().equals("NUMREF"))
            	numReferencia = ss.getValalf();
            if(ss.getNomcam().trim().equals("TIPREF"))
            	tipoReferencia = ss.getValalf();
            if(ss.getNomcam().trim().equals("ESTADO")){
            	estado = ss.getValalf();
            	codEstado =ss.getResval();
            }
           
          if(tipDoc.trim().equals("NCE") || tipDoc.trim().equals("NDE")){
         	    if(ss.getNomcam().trim().equals("FACNOT"))
         	    	numFactura = ss.getValnu1();
         	   if(ss.getNomcam().trim().equals("TIDORE"))
         		    tipoNota = ss.getValalf();  
         	  if(ss.getNomcam().trim().equals("NUMDOC"))
         		 numNota = ss.getValnu1();
            }else {
            	 if(ss.getNomcam().trim().equals("NUMDOC"))
     	     		numFactura = ss.getValnu1();  
            
            }
           
             if(ss.getNomcam().trim().equals("DETFAC")){
            	 Vector vec = new Vector();
            	 vec.addElement(ss.getValalf());
	             vec.addElement(ss.getValnu1());	         
	             vecSolicitudes.add(vec);
      
            }
       
            if(ss.getNomcam().trim().equals("HISTORIAL")){
            	String fecha = "";
            	String diaFecha = "";
            	String mesFecha = "";
            	String annoFecha = "";
            	String valor = "";
            	String valor2 = "";
            	if(ss.getValnu1()> 0 ) {
    				fecha = ss.getValnu1() + "";
    				diaFecha = fecha.substring(6);
    				mesFecha = fecha.substring(4,6);
    				annoFecha = fecha.substring(0,4);
    				valor = diaFecha+"/"+mesFecha+"/"+annoFecha;   
            	} else {
    				valor = ss.getValnu1()+"";
            	} 
            	if (ss.getResval().trim().equals("AUTORIZACION"))// se ocupa de la 1-15 el resto se ocupa la observacion del rechazo
            	   valor +=  " por " + ss.getValalf() + " Cuenta " + ss.getValnu2() ;
            	else
            		valor += " por " + ss.getValalf();
            	
            	Vector vec3 = new Vector();
            	vec3.addElement(ss.getResval()); // es la accion, INGRESO, MODIFICACION, AUTORIZACION, PAGO, AUTORIZA FINANZA				        
            	vec3.addElement(valor); // es la fecha, 
            	vecHistorial.addElement(vec3);            	
            
            }
          

	    }	
	    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
	    listSede = moduloFacturacion.getListaSede(accionweb.getReq());
	    for( Presw18DTO ss:listSede){
	    	if(ss.getNummes() == sede)
	    		nomSede = ss.getDesuni();
	    }
	    
	    vecDatos.addElement(String.valueOf(valorIVA));	 
	    vecDatos.addElement(tipoDocumento);
	    vecDatos.addElement(tipoServicio);
	    vecDatos.addElement(descripcion);
	    vecDatos.addElement(nomSede);
	    vecDatos.addElement(String.valueOf(rutide));
	    vecDatos.addElement(digide);
	    vecDatos.addElement(nombreCliente);
	    vecDatos.addElement(direccion);
	    vecDatos.addElement(comuna);
	    vecDatos.addElement(ciudad);
	    vecDatos.addElement(giro);
	    vecDatos.addElement(referencia);
	    vecDatos.addElement(condicion);
	    vecDatos.addElement(unidad);
	    vecDatos.addElement(nomUnidad);
	    vecDatos.addElement(String.valueOf(vencimiento));
	    vecDatos.addElement(String.valueOf(valorneto));
	    vecDatos.addElement(String.valueOf(valorExento));
	    vecDatos.addElement(String.valueOf(valorImpuesto));
	    vecDatos.addElement(String.valueOf(valorTotal));
	    vecDatos.addElement(numReferencia);
	    vecDatos.addElement(tipoReferencia);
	    vecDatos.addElement(estado);
	    vecDatos.addElement(numDoc+"");
	    vecDatos.addElement(numFactura);
	    vecDatos.addElement(tipDoc); 
	    vecDatos.addElement(numNota); 
	    
	 
	    	
	    	accionweb.getSesion().setAttribute("vecDatos", vecDatos);
            accionweb.getSesion().setAttribute("vecSolicitudes", vecSolicitudes);
            accionweb.getSesion().setAttribute("vecHistorial", vecHistorial);
                               
		 
	}
	public void verificaNotaCredito(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
		String tipoDocumento = Util.validaParametro(accionweb.getParameter("tipoDocumento"),"");
		int numeroFactura = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		String tipoNota = Util.validaParametro(accionweb.getParameter("tipoNota"),"");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		if(listaSolicitudes != null && listaSolicitudes.size() > 0){
			accionweb.getSesion().removeAttribute("listaSolicitudes");
			accionweb.getSesion().setAttribute("listaSolicitudes", null);
		}
	    String mensaje ="";
		List<Presw18DTO> list = new ArrayList<Presw18DTO>();
		int validaNota = 0;
		String id = "";
		
		Collection<Presw18DTO> lista = moduloFacturacion.getVerificaNotaCredito(rutUsuario, dv, tipoDocumento, numeroFactura,tipoNota);
		if(lista != null && lista.size()> 0){
			for (Presw18DTO ss : lista){
				validaNota = ss.getNummes();
				id = ss.getIdsoli();
			}	
			}
			if(validaNota > 0){
			switch(validaNota){
			case 1: {
				mensaje = "La Factura no existe.";
				break;}
			case 2:{
				mensaje = "El Documento es de una sede no autorizada para el usuario.";
				break;}
			case 3: {
				mensaje = "La cuenta asociada al documento no est� autorizada para usuario.";
				break;}
			}
				accionweb.agregarObjeto("mensaje", mensaje);
			} else {
				List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
				CocowBean cocowBean = new CocowBean();
				listCocow36DTO = cocowBean.buscar_cocow36_id(id);
				List<Presw18DTO> listServicio = new ArrayList<Presw18DTO>();   	
			    listServicio = moduloFacturacion.getListaServicio(accionweb.getReq()); 
			    List<Presw18DTO> listCondicionVenta = new ArrayList<Presw18DTO>();   	
			    listCondicionVenta = moduloFacturacion.getListaCondicionVenta(accionweb.getReq()); 
			    accionweb.agregarObjeto("listCondicionVenta", listCondicionVenta);
			    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
			    listSede = moduloFacturacion.getListaSede(accionweb.getReq());
			    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
			    moduloFacturacion.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
			    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
		
			    
				this.muestraDatosCocow36(listCocow36DTO, accionweb, listServicio, tipo);
				if(accionweb.getSesion().getAttribute("listaSolicitudes") != null ){
					accionweb.getSesion().removeAttribute("listaSolicitudes");
					accionweb.getSesion().setAttribute("listaSolicitudes", null);
					accionweb.agregarObjeto("listaSolicitudes", null);
				}
				accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
				accionweb.agregarObjeto("listSede", listSede);
				accionweb.agregarObjeto("numeroFactura", numeroFactura);
				accionweb.agregarObjeto("tipoNota", tipoNota);
				accionweb.agregarObjeto("listServicio", listServicio);
			} 
			accionweb.agregarObjeto("hayDatoslista", "1");     
	      	accionweb.agregarObjeto("esFactura", "1");
	      	accionweb.agregarObjeto("opcion", tipo);
	      	accionweb.agregarObjeto("titulo", "Ingreso");
	
	
	}
	
	
	
	public void exportarListafac(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);	
		Vector vecDatos = new Vector();
		Vector vec = new Vector();
		
		PreswBean preswbean = null;
		Collection<Presw18DTO> listaFacturacion = null;
		String titulo = "";
		String tituloDetalle1 = "";
		String tituloDetalle2 = "";
		String tipcue = "";
		switch (tipo) {
		case 4: case 6:// lista de solicitudes de facturacion para consulta
			{		
			String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
			int fechaInicio = Util.validaParametro(accionweb.getParameter("fechaInicio"),0);
			int fechaTermino = Util.validaParametro(accionweb.getParameter("fechaTermino"),0);
			String fechaIni = Util.validaParametro(accionweb.getParameter("fechaIni"),"");
			String fechaTer = Util.validaParametro(accionweb.getParameter("fechaTer"),"");
			int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0);
			if(fechaInicio > 0 && fechaTermino == 0 ){
				DateTool fechaActual = new DateTool();
				String dia = fechaActual.getDay()+"";
				if(dia.length() == 1)
					dia = "0" + dia;
				String mes = (fechaActual.getMonth()+1)+"";
				if(mes.length()== 1)
					mes = "0" + mes;
				fechaTermino = Integer.parseInt(String.valueOf(fechaActual.getYear()) + mes + dia);
				fechaTer = dia + "/" + mes + "/" + String.valueOf(fechaActual.getYear());
				
			}
			if(unidad >= 0){
				if(tipo == 4){
					preswbean = new PreswBean("LSC",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
					preswbean.setCoduni(unidad);
					preswbean.setTipcue(estado);
					preswbean.setFecmov(fechaInicio);
					preswbean.setNumcom(fechaTermino);
				} else if(tipo == 6){
					   preswbean = new PreswBean("LSR",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
						
					
				 }
					listaFacturacion = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			String tipoFactura = "";
			String tipoMovimiento = "";
			String estadoPago = "";
			String cliente = "";
			if(listaFacturacion.size()> 0){
			    for( Presw18DTO ss:listaFacturacion){
			    	String fecha = ss.getFecdoc()+"";
			    	String diaFecha = fecha.substring(6);
			    	String mesFecha = fecha.substring(4,6);
			    	String annoFecha = fecha.substring(0,4);
			    	fecha = diaFecha+"/"+mesFecha+"/"+annoFecha;
			    	
			    	if(ss.getIndprc().trim().equals("1") )
			    		estadoPago = "Pagado";
			    	if(ss.getIndprc().trim().equals("2"))
			    		estadoPago = "Abonado";
			        if(ss.getIndprc().trim().equals("3"))
			    		estadoPago = "Pendiente";
			    	if(ss.getIndprc().trim().equals("4"))
			    		estadoPago = "Anulada";
			        if(ss.getTipmov().trim().equals("FEC"))
			    		tipoFactura = "Afecto";
			    	if(ss.getTipmov().trim().equals("FET"))
			    		tipoFactura = "Exento";
			    	if(ss.getTipmov().trim().equals("NCE"))
			    		tipoFactura = "Nota Cr&eacute;dito";
			    	if(ss.getTipmov().trim().equals("NDE"))
			    		tipoFactura = "Nota D&eacute;bito";			     
			    	tipoMovimiento = ss.getTipmov().trim();
			    	if(ss.getPresac() > 0)
			    		tipoMovimiento += "-"+ss.getPresac();
			    	else
			    		estadoPago = "";
			    	
			    	 vec = new Vector();
			    	 vec.addElement(ss.getNumdoc());// numero interno
			    	 vec.addElement(fecha);// fecha
			    	 vec.addElement(moduloFacturacion.formateoNumeroEntero(ss.getRutide())+"-"+ss.getIndpro()+" "+ss.getDesite().trim());// cliente
			    	 vec.addElement(ss.getDesuni().trim()); // glosa
			    	 vec.addElement(String.valueOf(ss.getUsadom())); // valor
			    	 vec.addElement(ss.getNompro().trim()); // estado- pago				    	
				     vec.addElement(tipoFactura); // tipo Factura
			    	 vec.addElement(tipoMovimiento); // tipo movimiento
			    	 vec.addElement(estadoPago); // estadoPago
			    	 vec.addElement(String.valueOf(ss.getUsadac())); // saldo
			    	 vec.addElement(String.valueOf(ss.getPresum())); // valor pagado
			    	 vec.addElement(String.valueOf(ss.getPreano())); // monto pagado con cheque
			    	 vec.addElement(String.valueOf(ss.getDiferm())); // cheque pendiente por depositar
	   	        	 vecDatos.addElement(vec);
			    	 
			       	}
			}
			}
			String nomEstado = "";
			String nomUnidad = "";
		     if(estado.trim().equals("")) nomEstado = "Todas";
		     if(estado.trim().equals("N")) nomEstado = "Anuladas";
		     if(estado.trim().equals("A")) nomEstado = "Cuenta";
		     if(estado.trim().equals("F")) nomEstado = "Autorizado Finanzas";
		     if(estado.trim().equals("P")) nomEstado = "Facturadas";
		     if(estado.trim().equals("I")) nomEstado = "Ingresadas";
		     if(estado.trim().equals("L")) nomEstado = "Liquidadas";
		     if(estado.trim().equals("G")) nomEstado = "Pagadas";
		     if(estado.trim().equals("U")) nomEstado = "Rechazo Autorizador";
		     if(estado.trim().equals("R")) nomEstado = "Rechazo Finanzas";
		     if(estado.trim().equals("T")) nomEstado = "Traspaso a Finanzas";
		     if(unidad > 0){
		    	    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
					moduloFacturacion.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
					listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
					for (Presw18DTO ss : listaUnidades){
						if(ss.getCoduni() == unidad)
							nomUnidad = ss.getDesuni();
					}
						 
		     }
		   	    accionweb.getSesion().setAttribute("nomEstado", nomEstado);
		        accionweb.getSesion().setAttribute("fechaInicio", fechaIni);
		        accionweb.getSesion().setAttribute("fechaTermino", fechaTer);
		        accionweb.getSesion().setAttribute("nomUnidad", unidad + "-"+nomUnidad);
		  
		        
		       	
			}
			titulo = "Consulta General";
			
	        moduloFacturacion.agregaListaFacturacion(accionweb.getReq(), vecDatos);
	       		break;
			}
			
    	}
	
	public void muestraDatosCocow36(List<Cocow36DTO>  listCocow36DTO, AccionWeb accionweb, List<Presw18DTO> listServicio , int tipo) throws Exception {
			
		String nombreCliente = "";
	    String direccion = "";
	    String comuna = "";
	    String ciudad = "";
	    String giro = "";
	    String digide = "";
	    long rutide = 0;
		String tipoDocumento = "";
	    String tipoServicio = "";
	    String descripcion = "";
	    String nomSede = "";
	    String referencia = "";
	    String condicion = "";	   
	    String nomUnidad = "";
	    String numReferencia = "";
	    String tipoReferencia = "";
	    String codEstado = "";
	    String estado = "";
	    long valorIVA = 0;
	    long codSede = 0;
	    long unidad = 0;
	    long vencimiento = 0;
	    long valorneto = 0;
	    long valorExento = 0;
	    long valorImpuesto = 0;
	    long valorTotal = 0;
	    long numFactura = 0;
		List<Presw18DTO>  listaSolicitudes = new ArrayList<Presw18DTO>();
	
		if(listCocow36DTO != null && listCocow36DTO.size() >0 ){
	    
	    for(Cocow36DTO ss: listCocow36DTO){
	   		 
	    	    
	    	if(ss.getNomcam().trim().equals("TIPDOC")){
	    		tipoDocumento = ss.getValalf();	
	    		accionweb.agregarObjeto("afecto", tipoDocumento.trim());
	    		
	    		if(tipoDocumento.trim().equals("FEC")){
	    			for(Presw18DTO dd:listServicio){
		        		if (dd.getIddigi().trim().equals("FEC"))
		        			valorIVA =dd.getNummes();
		        	}
	    			  tipoDocumento = tipoDocumento.trim() + " - Afecto";
	    		}
	    		if(tipoDocumento.trim().equals("FET"))
	    			  tipoDocumento = tipoDocumento.trim() + " - Exento";	
	    	}
	    	if(ss.getNomcam().trim().equals("TIPSER")){
	    		tipoServicio = ss.getValalf();
	    		descripcion = ss.getResval();
	    	
	    	}
	     	if(ss.getNomcam().trim().equals("SUCUR")){
	     		codSede = ss.getValnu1();
	     		nomSede = ss.getValalf();
	     	}
	     	if(tipo != 11){ // pues en este caaso vienen los datos del que se consulta
	     	if(ss.getNomcam().trim().equals("RUTIDE"))
	     		rutide = ss.getValnu1();
	     	if(ss.getNomcam().trim().equals("DIGIDE"))
	     		digide = ss.getValalf();   
	     	if(ss.getNomcam().trim().equals("NOMCLI"))
	     		nombreCliente = ss.getValalf();   
            if(ss.getNomcam().trim().equals("DIRCLI") /*&& tipo != 11*/)
            	direccion = ss.getValalf();
            if(ss.getNomcam().trim().equals("COMUNA") /*&& tipo != 11*/)
            	comuna = ss.getValalf();
            if(ss.getNomcam().trim().equals("CIUDAD") /*&& tipo != 11*/)
                ciudad = ss.getValalf();
            if(ss.getNomcam().trim().equals("GIRCLI"))
            	giro = ss.getValalf();
	     	}
            if(ss.getNomcam().trim().equals("REFCLI"))
            	referencia = ss.getValalf();
            if(ss.getNomcam().trim().equals("CONVEN"))
            	condicion = ss.getValalf();
            if(ss.getNomcam().trim().equals("CODUNI"))
            	unidad = ss.getValnu1();            
            if(ss.getNomcam().trim().equals("DESUNI"))
            	nomUnidad = ss.getValalf();
            if(ss.getNomcam().trim().equals("DIAVEN"))
            	vencimiento = ss.getValnu1();  
            if(ss.getNomcam().trim().equals("VALNET"))
            	valorneto = ss.getValnu1(); 
            if(ss.getNomcam().trim().equals("VALEXE"))
            	valorExento = ss.getValnu1(); 
            if(ss.getNomcam().trim().equals("VALIVA"))
            	valorImpuesto = ss.getValnu1(); 
            if(ss.getNomcam().trim().equals("VALTOT"))
            	valorTotal = ss.getValnu1(); 
            if(ss.getNomcam().trim().equals("NUMREF"))
            	numReferencia = ss.getValalf();
            if(ss.getNomcam().trim().equals("TIPREF"))
            	tipoReferencia = ss.getValalf();
            if(ss.getNomcam().trim().equals("ESTADO")){
            	estado = ss.getValalf();
            	codEstado =ss.getResval();
            }
            if(ss.getNomcam().trim().equals("NUMDOC"))
	     		numFactura = ss.getValnu1();  
                  
             if(ss.getNomcam().trim().equals("DETFAC")){
            	Presw18DTO presw18DTO = new Presw18DTO();
            	presw18DTO.setUsadom(ss.getValnu1());
               	presw18DTO.setDesuni(ss.getValalf());
            	listaSolicitudes.add(presw18DTO);
      
            }
       
          
          
	    	 }
	    } else {
	    	if(tipo != 7)
	    		accionweb.agregarObjeto("mensaje", "Lo sentimos, esta factura que quiere copiar no existe.");
	    	else
	    		accionweb.agregarObjeto("mensaje", "Lo sentimos, esta factura no existe.");
	    }
	    if(listaSolicitudes != null && listaSolicitudes.size() > 0){
	    	accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes);
	    	accionweb.getSesion().setAttribute("listaSolicitudes", listaSolicitudes);
	    }
	
	   
	    accionweb.agregarObjeto("estado",estado);
	    accionweb.agregarObjeto("estadoFinal",estado);
	    accionweb.agregarObjeto("tipoDocumento", tipoDocumento);
	    accionweb.agregarObjeto("tipoServicio", tipoServicio);
	    accionweb.agregarObjeto("descripcion", descripcion);
	    accionweb.agregarObjeto("nomSede", nomSede);
	    accionweb.agregarObjeto("nombreCliente", nombreCliente);
	    accionweb.agregarObjeto("direccion", direccion);
	    accionweb.agregarObjeto("comuna", comuna);
	    accionweb.agregarObjeto("ciudad", ciudad);
	    accionweb.agregarObjeto("giro", giro);
	    accionweb.agregarObjeto("referencia", referencia);
	    accionweb.agregarObjeto("condicion", condicion);
	    accionweb.agregarObjeto("nomUnidad", nomUnidad);
	    accionweb.agregarObjeto("numReferencia", numReferencia);
	    accionweb.agregarObjeto("tipoReferencia", tipoReferencia);
	    accionweb.agregarObjeto("codSede", codSede);
	    accionweb.agregarObjeto("unidad", unidad);
	    accionweb.agregarObjeto("vencimiento", vencimiento);
	    accionweb.agregarObjeto("estado", estado);
	    accionweb.agregarObjeto("valorNeto", valorneto);
	    accionweb.agregarObjeto("valorExento", valorExento);
	    accionweb.agregarObjeto("valorImpuesto", valorImpuesto);
	    accionweb.agregarObjeto("valorTotal", valorTotal);
	    accionweb.agregarObjeto("numFactura", numFactura);
	   	accionweb.agregarObjeto("rutide", rutide);
	    accionweb.agregarObjeto("digide", digide);	
        accionweb.agregarObjeto("valorIVA", valorIVA);

	//		accionweb.agregarObjeto("encuentra", "1");		

		  	}
	public void verificaRutIngresador(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
		String nomUnidad = Util.validaParametro(accionweb.getParameter("nomUnidad"),"");
		
	
		String nombre = "";
		String indprc = "";
		if(rutnum > 0){
			Collection<Presw18DTO> lista = moduloFacturacion.getVerificaRutIngresador(rutnum, dvrut);
			if(lista != null && lista.size() > 0){
				 for (Presw18DTO ss : lista){
					 nombre = ss.getDesuni();
					 indprc = ss.getIndprc();
				   }
			}
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
			accionweb.agregarObjeto("unidad", unidad); 
			accionweb.agregarObjeto("nomUnidad", nomUnidad); 
			
		}
	   	if(!nombre.trim().equals("")){
	   		if(indprc.trim().equals("F")){
	    		accionweb.agregarObjeto("hayDatoslista", 1);
	    		accionweb.agregarObjeto("nomIngresador", nombre);
	   		} else {
	   			accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado como Funcionario.");
	   			
	   		}
	    } else
	    	accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado.");
	  	accionweb.agregarObjeto("esFactura", 1);  
	  	accionweb.agregarObjeto("titulo", "Nuevo Ingresador");  
	 		
	}
	
	public void cargaCliente(AccionWeb accionweb) throws Exception {
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		List<Presw18DTO> listBanco = new ArrayList<Presw18DTO>();
		listBanco = moduloFacturacion.getListaBanco(accionweb.getReq());
		accionweb.agregarObjeto("listBanco", listBanco);
		accionweb.agregarObjeto("rutnum", rutnum);
 		accionweb.agregarObjeto("dvrut", dvrut);
 		accionweb.agregarObjeto("rut_aux", rut_aux); 
 	   	accionweb.agregarObjeto("esFactura", 1);   
    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
       	if(accionweb.getSesion().getAttribute("autorizaClave") != null)
			accionweb.agregarObjeto("autorizaClave", accionweb.getSesion().getAttribute("autorizaClave"));
}
	
	
	public void verificaRutFacturacion(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		int identificador = Util.validaParametro(accionweb.getParameter("identificador"), 0);
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		if(listaSolicitudes != null && listaSolicitudes.size() > 0){
			accionweb.getSesion().removeAttribute("listaSolicitudes");
			accionweb.getSesion().setAttribute("listaSolicitudes", null);
		}
		List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudesPago");
		if(listaSolicitudesPago != null && listaSolicitudesPago.size() > 0){
			accionweb.getSesion().removeAttribute("listaSolicitudesPago");
			accionweb.getSesion().setAttribute("listaSolicitudes", null);
		}
		String nombre = "";
		if(rutnum > 0){
			Collection<Presw25DTO> lista = moduloFacturacion.getVerificaRutFacturacion(rutnum, dvrut);
			if(lista != null && lista.size() > 0){
				 for (Presw25DTO ss : lista){
					 nombre = ss.getDesuni();
				   }
			}
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
			
		}
	   	if(!nombre.trim().equals("")){
	    		accionweb.agregarObjeto("hayDatoslista", 1);
	    		accionweb.agregarObjeto("nomIdentificador", nombre);
	    } else
	    	accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado. Debe agregarlo.");
		
	    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
	    listSede = moduloFacturacion.getListaSede(accionweb.getReq());
	    String comboSede = "<select name=\"sede\" id=\"sede\" class=\"estilo_Azul\" onChange=\"\" >"+
	                       "<option value=\"-1\" selected=\"selected\">Seleccione</option> ";
	  //  if(identificador == 1)
	    for (Presw18DTO ss : listSede){
	    	comboSede += "<option value=" + ss.getNummes() + ">" + ss.getDesuni() + "</option>";
	    	
	    }
	    comboSede += "</select>";
	   
	    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
	    moduloFacturacion.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
	
	    
  
	    
	    accionweb.agregarObjeto("comboSede", comboSede);
	    accionweb.agregarObjeto("listSede", listSede);
		accionweb.agregarObjeto("identificador", identificador);		  		  
		accionweb.agregarObjeto("esFactura", 1);   

	
	
		accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);  
    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
       	if(accionweb.getSesion().getAttribute("autorizaClave") != null)
			accionweb.agregarObjeto("autorizaClave", accionweb.getSesion().getAttribute("autorizaClave"));

	}
	
	public void validaCliente(AccionWeb accionweb) throws Exception {
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
				
		List<Presw25DTO> datosCliente = new ArrayList<Presw25DTO>();
		String nomCliente = "";
		if(rutnum > 0){
			Collection<Presw25DTO> lista = moduloFacturacion.getVerificaRutFacturacion(rutnum, dvrut);
			if(lista != null && lista.size() > 0){
				 for (Presw25DTO ss : lista){
					 // cambiado segun peticion 25/01/2016 nomCliente = ss.getDesuni();
					 nomCliente = ss.getMotiv1().trim() + " " + ss.getMotiv2().trim();					 
				   }
			}
			accionweb.agregarObjeto("rut_aux", rut_aux);  
		 
			
		}
	   	if(!nomCliente.trim().equals("")){
	    		accionweb.agregarObjeto("hayDatoslista", 1);
	    		accionweb.agregarObjeto("nomIdentificador", nomCliente);
	    }
		
	   	List<Presw18DTO> listBanco = new ArrayList<Presw18DTO>();
		listBanco = moduloFacturacion.getListaBanco(accionweb.getReq());
		accionweb.agregarObjeto("listBanco", listBanco);
	
		if(!nomCliente.trim().equals(""))
		  accionweb.agregarObjeto("mensaje", "Este rut ya se encuentra registrado como cliente, no puede agregarlo");
	    else{
		  accionweb.agregarObjeto("nomCliente", nomCliente);		 		
		  accionweb.agregarObjeto("cliente", 1);
	 	  }
	    accionweb.agregarObjeto("rutnum", rutnum);
        accionweb.agregarObjeto("dvrut", dvrut);
	    accionweb.agregarObjeto("rut_aux", rut_aux);	
	   	accionweb.agregarObjeto("esFactura", 1);   
    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
       	if(accionweb.getSesion().getAttribute("autorizaClave") != null)
			accionweb.agregarObjeto("autorizaClave", accionweb.getSesion().getAttribute("autorizaClave"));

	}
	 synchronized public void registraCliente(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		String nombre = Util.validaParametro(accionweb.getParameter("nombre"),"");
		String direccion = Util.validaParametro(accionweb.getParameter("direccion"),"");
		String comuna = Util.validaParametro(accionweb.getParameter("comuna"),"");
		String ciudad = Util.validaParametro(accionweb.getParameter("ciudad"),"");
		int fono = Util.validaParametro(accionweb.getParameter("fono"),0);
		long anexo = Util.validaParametro(accionweb.getParameter("anexo"),0);
		String fax = Util.validaParametro(accionweb.getParameter("fax"),"");
		String giro = Util.validaParametro(accionweb.getParameter("giro"),"");
		String cueBan = Util.validaParametro(accionweb.getParameter("cueBan"),"");
		int codBan = Util.validaParametro(accionweb.getParameter("codBan"),0);
		String correo = Util.validaParametro(accionweb.getParameter("correo"),"");
		String finCorreo = Util.validaParametro(accionweb.getParameter("finCorreo"),"");
		
		String nom = moduloFacturacion.eliminaAcentosString(nombre);
		if(nom.trim().length() > 70)
			nom = nom.substring(0,70);
	
		String dir = moduloFacturacion.eliminaAcentosString(direccion);
		if(dir.trim().length() > 40)
			dir = dir.substring(0,40);

		String com = moduloFacturacion.eliminaAcentosString(comuna);
		String ciu = moduloFacturacion.eliminaAcentosString(ciudad);
		String fa = moduloFacturacion.eliminaAcentosString(fax);
		String gir = moduloFacturacion.eliminaAcentosString(giro);
		String corr = moduloFacturacion.eliminaAcentosString(correo);
		String finCorr = moduloFacturacion.eliminaAcentosString(finCorreo);
		
		List<Presw21DTO> datosCliente = new ArrayList<Presw21DTO>();
		String nomCliente = "";
		String correoProveedor = "";
		
		if(rutnum > 0){
			Collection<Presw25DTO> lista = moduloFacturacion.getVerificaRutFacturacion(rutnum, dvrut);
			if(lista != null && lista.size() > 0){
				 for (Presw25DTO ss : lista){
					 //nomCliente = ss.getDesuni();
					 nomCliente = ss.getMotiv1().trim();
				   }
			}
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
			
		}
	   	if(!nomCliente.trim().equals("")){
		
	   		accionweb.agregarObjeto("mensaje", "Este RUT ya se encuentra en nuestros registros, no puede agregarse."); 
		} else { 
			// se agrega que el nombre sea guardado en dos campos
			String nom1 = "";
			String nom2 = "";
			if(nom.trim().length() > 40) {
				nom1 = nom.substring(0,40);
				nom2 = nom.substring(40);
			} else
				nom1 = nom;
			List<Presw25DTO>  lista = new ArrayList<Presw25DTO>();
			Presw25DTO presw25 = new Presw25DTO();
			presw25.setTippro("CLI");
			presw25.setRutide(rutnum);
			presw25.setDigide(dvrut);
		//	presw25.setComen1(nom.toUpperCase());
			presw25.setMotiv3(nom1.toUpperCase());
			presw25.setMotiv4(nom2.toUpperCase());
			presw25.setComen2(dir.toUpperCase());
			presw25.setComen3(com.toUpperCase());
			presw25.setComen4(ciu.toUpperCase());
			presw25.setPres01(fono);
			presw25.setPres02(anexo);
			presw25.setComen5(fax);
			presw25.setComen6(gir.toUpperCase());
			presw25.setPres03(codBan);
			presw25.setMotiv1(cueBan.trim());
			presw25.setMotiv2(corr.trim()+"@."+finCorr.trim());
			lista.add(presw25);
			try {
		 	 boolean graba = moduloFacturacion.saveCliente(lista,rutUsuario);
		 	if (graba)	  
		 		 accionweb.agregarObjeto("mensaje", "Este Cliente se registr� en forma exitosa.");
		 	else
		 		 accionweb.agregarObjeto("mensaje", "Este Cliente NO se registr�.");
		 	Thread.sleep(500);
			} catch (Exception e) {
			accionweb.agregarObjeto("mensaje", "No guardado registraCliente.");
			accionweb.agregarObjeto("exception", e);
			e.printStackTrace();
			}
	        accionweb.agregarObjeto("cliente", 1);		
	  	   	accionweb.agregarObjeto("esFactura", 1);
	  		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	       	if(accionweb.getSesion().getAttribute("autorizaClave") != null)
				accionweb.agregarObjeto("autorizaClave", accionweb.getSesion().getAttribute("autorizaClave"));
}
	}
	 
/*	 public void cargaResultadoAlumnoFactura(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int sedeAlumno = Util.validaParametro(accionweb.getParameter("sedeAlumno"), 0);
			int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
			String dvRut = Util.validaParametro(accionweb.getParameter("dv"), "");
			int carrera = Util.validaParametro(accionweb.getParameter("carrera"), 0);	
			int anioArancel = Util.validaParametro(accionweb.getParameter("anioArancel"), 0);
			List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) accionweb.getSesion().getAttribute("listaSolicitudes");
		    int resul = 0;	
		
			try {
			      
			  	  List<Presw25DTO> datosAlumno = moduloFacturacion.getExisteAlumno(sedeAlumno, rutnum, dvRut, carrera, anioArancel);
					if(datosAlumno != null && datosAlumno.size() > 0){
						 accionweb.agregarObjeto("datosAlumno", datosAlumno);
					}
			} catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "cargaResultadoAlumnoFactura.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
						
		}*/
}
