package cl.utfsm.sif.modulo;
/**
 * SISTEMA DE SOLICITUDES DE FACTURACI�N POR INTERNET
 * PROGRAMADOR		: 	M�NICA BARRERA FREZ
 * FECHA ULT.MODIF  : 	21/06/2012
 * UNIDAD DE DESARROLLO INSTITUCIONAL(DTI)   
 */
/*import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;*/
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.tools.generic.NumberTool;

import cl.utfsm.base.AccionWeb;
//import cl.utfsm.base.util.ConexionBD;
import cl.utfsm.base.util.Util;
import cl.utfsm.sif.datos.FacturacionDao;
import descad.cliente.CocofBean;
import descad.cliente.Ingreso_Documento;
import descad.cliente.MD5;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Cocof17DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw25DTO;

public class ModuloFacturacion {
	FacturacionDao facturacionDao;

	public FacturacionDao getFacturacionDao() {
		return facturacionDao;
	}

	public void setFacturacionDao(FacturacionDao facturacionDao) {
		this.facturacionDao = facturacionDao;
	} 
	public List<Presw18DTO> getConsultaAutoriza(int rutnum, String dv){
	  	PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		List<Presw18DTO> listaAutoriza =  new ArrayList<Presw18DTO>();
		String nombre = "";
		if(rutnum > 0){
			preswbean = new PreswBean("PAU",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
			lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			   for (Presw18DTO ss : lista){
				   Presw18DTO presw18DTO = new Presw18DTO();
				   presw18DTO.setIndprc(ss.getIndprc());
				   listaAutoriza.add(presw18DTO);
			   }
					
		}
	
	 return listaAutoriza;
	}
	 public Collection<Presw25DTO> getVerificaRut(int rutnum, String dv){
		  	PreswBean preswbean = null;
			Collection<Presw25DTO> lista = null;
			if(rutnum > 0){
				preswbean = new PreswBean("VCL",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
				lista = (Collection<Presw25DTO>) preswbean.consulta_presw25();
			}
		
		 return lista;
		}
	 
	 public Collection<Presw25DTO> getVerificaPagoCuenta(int rutnum, String dv, int cuenta){
		  	PreswBean preswbean = null;
			Collection<Presw25DTO> lista = null;
			if(rutnum > 0){
				//preswbean = new PreswBean("VFC",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"",""); se modifica por el proceso siguiente
				preswbean = new PreswBean("VVU",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
				preswbean.setCoduni(cuenta);
				lista = (Collection<Presw25DTO>) preswbean.consulta_presw25();
			}
		
		 return lista;
		}
	 public Collection<Presw25DTO> getVerificaRutFacturacion(int rutnum, String dv){
		  	PreswBean preswbean = null;
			Collection<Presw25DTO> lista = null;
			if(rutnum > 0){
				preswbean = new PreswBean("VCL",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
				lista = (Collection<Presw25DTO>) preswbean.consulta_presw25();
					}
		
		 return lista;
		}
	 public Collection<Presw18DTO> getVerificaRutIngresador(int rutnum, String dv){
		  	PreswBean preswbean = null;
			Collection<Presw18DTO> lista = null;
			if(rutnum > 0){
				preswbean = new PreswBean("VRU",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
				lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			}
		
		 return lista;
		}
	 public void getCuentasAutorizadas( HttpServletRequest req, int rutide){
		 HttpSession sesion = req.getSession();
		 Collection<Presw18DTO> listaPresw18 = null;
		 
		 listaPresw18 = this.getConsulta("VCA", 0, 0, 0, 0, rutide);
		 List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
			
		 for (Presw18DTO ss : listaPresw18){
			 Presw18DTO cuentapresupuestaria = new Presw18DTO();
			 cuentapresupuestaria.setCoduni(ss.getCoduni());
			 cuentapresupuestaria.setNomtip(ss.getNomtip());
			 cuentapresupuestaria.setDesuni(ss.getDesuni());
			 cuentapresupuestaria.setIndprc(ss.getIndprc());
			 lista.add(cuentapresupuestaria);
		 }
		 
		 
		 sesion.setAttribute("cuentasPresupuestarias", lista);
		return ;
		 }
	 public Collection<Presw18DTO> getConsulta(String tippro, int codUnidad, int item, int anno, int mes, int rutIde){
			PreswBean preswbean = null;
			Collection<Presw18DTO> listaPresw18 = null;
			if(item >= 0){
				preswbean = new PreswBean(tippro, codUnidad, item, anno, mes, rutIde);
				listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
				}
			return listaPresw18;
		 }
	 public List<Presw18DTO> getListaSede( HttpServletRequest req) throws Exception {
			/*	esto es en caso de que se muestren las sedes que puede ver el usuario */
				int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
			
				PreswBean preswbean = null;
				Collection<Presw18DTO> lista = null;
				List<Presw18DTO> list = new ArrayList<Presw18DTO>();
				String nombre = "";
				preswbean = new PreswBean("SED",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
					lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
					   for (Presw18DTO ss : lista){
						   Presw18DTO presw18DTO = new Presw18DTO();
						   presw18DTO.setNummes(ss.getNummes());
						   presw18DTO.setDesuni(ss.getDesuni());
						   list.add(presw18DTO);
					   }	
				   
			return list;
			}
	 public List<Presw18DTO> getListaBanco( HttpServletRequest req) throws Exception {
			/*	se muestran todos los bancos */
				int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
			
				PreswBean preswbean = null;
				Collection<Presw18DTO> lista = null;
				List<Presw18DTO> list = new ArrayList<Presw18DTO>();
				String nombre = "";
				preswbean = new PreswBean("BAN",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
					lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
					   for (Presw18DTO ss : lista){
						   Presw18DTO presw18DTO = new Presw18DTO();
						   presw18DTO.setItedoc(ss.getItedoc());
						   presw18DTO.setDesuni(ss.getDesuni());
						   list.add(presw18DTO);
					   }	
				   
			return list;
			}
	 public List<Presw18DTO> getListaServicio( HttpServletRequest req) throws Exception {
			/*	esto es en caso de que se muestren los servicios a facturar */
				int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
			
				PreswBean preswbean = null;
				Collection<Presw18DTO> lista = null;
				List<Presw18DTO> list = new ArrayList<Presw18DTO>();
				String nombre = "";
				preswbean = new PreswBean("TSF",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
					lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
					   for (Presw18DTO ss : lista){
						   Presw18DTO presw18DTO = new Presw18DTO();							
						   if(ss.getIddigi().trim().equals("FEC") || ss.getIddigi().trim().equals("FET") || ss.getIddigi().trim().equals("FEX")){
							   presw18DTO.setTipmov(ss.getTipmov().trim());
							   presw18DTO.setDesuni(ss.getDesuni().trim());
							   presw18DTO.setIddigi(ss.getIddigi().trim());
							   presw18DTO.setNummes(ss.getNummes());
							   list.add(presw18DTO);
						   }
					   }	
				   
			return list;
			}
	 public List<Presw18DTO> getListaCondicionVenta( HttpServletRequest req) throws Exception {
			/*	esto es en caso de que se muestren los plazos de pago */
				int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
			
				PreswBean preswbean = null;
				Collection<Presw18DTO> lista = null;
				List<Presw18DTO> list = new ArrayList<Presw18DTO>();
				String nombre = "";
				preswbean = new PreswBean("GLV",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
					lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
					   for (Presw18DTO ss : lista){
						   Presw18DTO presw18DTO = new Presw18DTO();
						   presw18DTO.setItedoc(ss.getItedoc());
						   presw18DTO.setDesuni(ss.getDesuni().trim());
						   list.add(presw18DTO);
					   }	
				   
			return list;
			}
		 public boolean saveDeleteIngresador( int rut, String dv, int coduni, String tipcue ){
			  	PreswBean preswbean = null;
				Collection<Presw18DTO> lista = null;
				long saldoCuenta = 0;
				preswbean = new PreswBean("ALI",0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
				preswbean.setRutide(rut);
				preswbean.setDigide(dv);
				preswbean.setCoduni(coduni);
				preswbean.setTipcue(tipcue);
				
				return preswbean.ingreso_presw19();	
				
		 } 
		 
		 
		 
		 
		 public void getAgregaListaSolicitaFactura( HttpServletRequest req){
			 HttpSession sesion = req.getSession();
		     long valor = Util.validaParametro(req.getParameter("valor"), 0);
		     String descripcion = Util.validaParametro(req.getParameter("descripcion"), "");
		     String descLimpia = this.eliminaBlancosString(descripcion);	
		     if(descLimpia.length() > 40)
		    	 descLimpia = descLimpia.substring(0,39);
		 	 List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudes");
			 if(listaSolicitudes != null && listaSolicitudes.size()> 0){
				 sesion.removeAttribute("listaSolicitudes");
			     sesion.setAttribute("listaSolicitudes", null);
					
			 }
			 if(listaSolicitudes == null)
					listaSolicitudes = new ArrayList<Presw18DTO>();
			 if(listaSolicitudes.size() < 20){
				 		Presw18DTO presw18DTO = new Presw18DTO();
				 		presw18DTO.setDesuni(descLimpia);
				 		presw18DTO.setUsadom(valor);
				 		listaSolicitudes.add(presw18DTO);
			 } 
			 
			 sesion.setAttribute("listaSolicitudes", listaSolicitudes);
				
		 }
		 public void getEliminaListaSolicitaFact( HttpServletRequest req){
			 HttpSession sesion = req.getSession();
		     long valor = Util.validaParametro(req.getParameter("usadom"), 0);
		     String desuni = Util.validaParametro(req.getParameter("desuni"),"");
		     
			 List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudes");
			 List<Presw18DTO> listaFacturas = new ArrayList<Presw18DTO>();
			 if(listaSolicitudes != null && listaSolicitudes.size()> 0){
				 sesion.removeAttribute("listaSolicitudes");
			     sesion.setAttribute("listaSolicitudes", null);
					
			 }
			 if(listaSolicitudes != null && listaSolicitudes.size() > 0){
				 for (Presw18DTO ss: listaSolicitudes){
					 if(ss.getUsadom() == valor && ss.getDesuni().trim().equals(desuni.trim()))
						 continue;
					 else
					 {
						 Presw18DTO presw18DTO = new Presw18DTO();
						 presw18DTO.setDesuni(ss.getDesuni());
						 presw18DTO.setUsadom(ss.getUsadom());
						 listaFacturas.add(presw18DTO);
					 }
				 }
				 
			
			 }
			 //if(listaFacturas.size() > 0)
			   sesion.setAttribute("listaSolicitudes", listaFacturas);
				
		 }
		  public  void getModificaSolicitaFact( HttpServletRequest req){
			 HttpSession sesion = req.getSession();
		     long valor = Util.validaParametro(req.getParameter("usadom"), 0);
		     String desuni = Util.validaParametro(req.getParameter("desuni"),"");
		     long valorNuevo = Util.validaParametro(req.getParameter("usadomNuevo"), 0);
		     String desuniNuevo = Util.validaParametro(req.getParameter("desuniNuevo"),"");
		     String descLimpia = this.eliminaBlancosString(desuniNuevo);	
			 	  
			 List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudes");
			 List<Presw18DTO> listaFacturas = new ArrayList<Presw18DTO>();
			 if(listaSolicitudes != null && listaSolicitudes.size()> 0){
				 sesion.removeAttribute("listaSolicitudes");
			     sesion.setAttribute("listaSolicitudes", null);
			 }
			 if(listaSolicitudes != null && listaSolicitudes.size() > 0){
				 for (Presw18DTO ss: listaSolicitudes){
					 if(ss.getUsadom() == valor && ss.getDesuni().trim().equals(desuni.trim())){
						 Presw18DTO presw18DTO = new Presw18DTO();
						 presw18DTO.setDesuni(descLimpia);
						 presw18DTO.setUsadom(valorNuevo);
						 listaFacturas.add(presw18DTO);
					 } else
					 {
						 Presw18DTO presw18DTO = new Presw18DTO();
						 presw18DTO.setDesuni(ss.getDesuni());
						 presw18DTO.setUsadom(ss.getUsadom());
						 listaFacturas.add(presw18DTO);
					 }
				 }
				 
			
			 }
			 //if(listaFacturas.size() > 0)
			   sesion.setAttribute("listaSolicitudes", listaFacturas);
				
		 }
		 public int getRegistraFactura(HttpServletRequest req, int rutide, String digide){
				int numFactura = 0;
				List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
				listCocow36DTO = this.getCreaCocow36(req, rutide, digide, "GSF");
				Ingreso_Documento ingresoDocumento = new Ingreso_Documento("GSF",rutide,digide);
					
				numFactura = ingresoDocumento.Ingresar_Documento(listCocow36DTO);
				
			
				return numFactura;
			}
		 public List<Cocow36DTO> getCreaCocow36(HttpServletRequest req, int rutide, String digide, String tipoProceso){
				List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) req.getSession().getAttribute("listaSolicitudes");
				String nombreCliente = Util.validaParametro(req.getParameter("nombreCliente"), "");
				long rutnum = Util.validaParametro(req.getParameter("rutnum"), 0);
				String dvrut = Util.validaParametro(req.getParameter("dvrut"),"");
				String direccionCliente = Util.validaParametro(req.getParameter("direccionCliente"),"");
				String comunaCliente = Util.validaParametro(req.getParameter("comunaCliente"),"");
				String ciudadCliente = Util.validaParametro(req.getParameter("ciudadCliente"),"");
				String giroCliente = Util.validaParametro(req.getParameter("giroCliente"),"");
				String referenciaAtt = Util.validaParametro(req.getParameter("referenciaAtt"),"");
				String condicionVenta = Util.validaParametro(req.getParameter("condicionVenta"),"");
				String tipoServicio = Util.validaParametro(req.getParameter("tipoServicio"),"");
				String tipoDocumento = Util.validaParametro(req.getParameter("afecto"),"");
				String desuni = Util.validaParametro(req.getParameter("desuni"),"");
				nombreCliente = this.eliminaBlancosString(nombreCliente);
				comunaCliente = this.eliminaBlancosString(comunaCliente);
				direccionCliente = this.eliminaBlancosString(direccionCliente);
				giroCliente = this.eliminaBlancosString(giroCliente);
				ciudadCliente = this.eliminaBlancosString(ciudadCliente);
				referenciaAtt = this.eliminaBlancosString(referenciaAtt);
				condicionVenta = this.eliminaBlancosString(condicionVenta);
				desuni = this.eliminaBlancosString(desuni);
				 
				int diasVencimiento = Util.validaParametro(req.getParameter("diasVencimiento"), 0);
				int cuentaPresup = Util.validaParametro(req.getParameter("cuentaPresup"), 0);
				long totalNeto = Util.validaParametro(req.getParameter("totalNeto"), 0);
				long totalIVA = Util.validaParametro(req.getParameter("totalIVA"), 0);
				long totalFac = Util.validaParametro(req.getParameter("totalFac"), 0);
				long valorIVA = Util.validaParametro(req.getParameter("valorIVA"), 0);
				long sede = Util.validaParametro(req.getParameter("sede"), 0);
				String tipoCuenta = Util.validaParametro(req.getParameter("tipoCuenta"),"");
				String numTipoCuenta = Util.validaParametro(req.getParameter("numTipoCuenta"),"");
				MathTool mathtool = new MathTool();
			    totalIVA = mathtool.mul(totalNeto,valorIVA).longValue();
			    totalIVA = mathtool.round(mathtool.div(totalIVA,100)).longValue();
				List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
				/*mb agrega datos alumno vespertino nov-2014*/
			/*	List<Presw25DTO> datosAlumno = new ArrayList<Presw25DTO>();
				datosAlumno = (List<Presw25DTO>) req.getSession().getAttribute("datosAlumno");
				Presw25DTO presw25DTO = new Presw25DTO();*/
				
				/**para nota de credito*/
				String tipoNota = Util.validaParametro(req.getParameter("tipoNota"),"");
				long numDoc = Util.validaParametro(req.getParameter("numDoc"), 0);
			 /*****/
				
			    String tiping = tipoProceso;
			    String nomcam = "";
			    long valnu1 = 0;
			    long valnu2 = 0;
			    long valorExento = 0;
				if(tipoDocumento.trim().equals("FET")){
					valorExento = totalNeto;
					totalNeto = 0;
				}
			    
			    String valalf = "";
			

		    /* prepara archivo para grabar, llenar listCocow36DTO*/
			    
		    nomcam = "TIPDOC";
		    valnu1 = 0;
		    valnu2 = 0;
		    if(!tipoNota.trim().equals(""))
		    	 valalf = tipoNota;
		    else
		    	valalf = tipoDocumento;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "SUCUR";
		    valnu1 = sede;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "RUTIDE";
		    valnu1 = rutnum;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "DIGIDE";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = dvrut;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "NOMCLI";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = nombreCliente;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "DIRCLI";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = direccionCliente;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "COMUNA";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = comunaCliente;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "CIUDAD";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = ciudadCliente;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "GIRCLI";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = giroCliente;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

		    nomcam = "TIPSER";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = tipoServicio;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

		    nomcam = "REFCLI";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = referenciaAtt;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "CONVEN";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = condicionVenta;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

		    nomcam = "CODUNI";
		    valnu1 = cuentaPresup;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

		    nomcam = "DESUNI";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = desuni;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "VALNET";
		    valnu1 = totalNeto;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    

		    nomcam = "VALEXE";
		    valnu1 = valorExento;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "VALIVA";
		    valnu1 = totalIVA;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "VALTOT";
		    valnu1 = totalFac;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    
		    nomcam = "TIPREF";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = tipoCuenta;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		 
		    nomcam = "NUMREF";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = numTipoCuenta;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    
		    
		    nomcam = "DIAVEN";
		    valnu1 = diasVencimiento;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    /* datos alumno vespertino que se asocia factura*/
		 /*    if(datosAlumno != null && datosAlumno.size() > 0){
		    	 presw25DTO = datosAlumno.get(0);
		    	 nomcam = "RUTVES";
				 valnu1 = presw25DTO.getRutide();
				 valnu2 = 0;
				 valalf = "";
				 listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
				
				 nomcam = "DIGVES";
				 valnu1 = 0;
				 valnu2 = 0;
				 valalf = presw25DTO.getDigide();
				 listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
			
				 nomcam = "ANOARA";
				 valnu1 = presw25DTO.getAnopre();
				 valnu2 = 0;
				 valalf = "";
				 listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
			
				 nomcam = "CODCAR";
				 valnu1 = presw25DTO.getCoduni();
				 valnu2 = 0;
				 valalf = "";
				 listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
				 
				 nomcam = "CODSUC";
				 valnu1 = presw25DTO.getCanman();
				 valnu2 = 0;
				 valalf = "";
				 listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		     }*/
		    
		    /**/
		    /*  agregar MD5   TIPDOC + RUTIDE + DIGIDE + NOMCLI + VALTOT*/
		    nomcam = "CODMD5";
		    MD5 md5 = new MD5();
		    String texto = md5.getMD5(tipoDocumento.trim() + rutide + dvrut.trim() + nombreCliente.trim() + totalFac) ;
		    if(!tipoNota.trim().equals(""))
		    	texto = md5.getMD5(tipoNota.trim() + rutide + dvrut.trim() + nombreCliente.trim() + totalFac) ;
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = texto;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    /*para nota de credito*/
		    if(!tipoNota.trim().equals("")){
		    nomcam = "TIDORE";
		    valnu1 = 0;
		    valnu2 = 0;
		    valalf = tipoDocumento;
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    
		    nomcam = "FACNOT";
		    valnu1 = numDoc;
		    valnu2 = 0;
		    valalf = "";
		    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		    }
		    
		    
		  
		    for (Presw18DTO ss : listaSolicitudes){
		    	nomcam = "DETFAC";
		 	    valnu1 = ss.getUsadom();
		 	    valnu2 = 0;
		 	    valalf = ss.getDesuni();
		 	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
		 	     
		    }
				
	
			return listCocow36DTO;
			}
		 public List<Cocow36DTO> getAgregaLista(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf){
			 Cocow36DTO cocow36DTO = new Cocow36DTO();
			 cocow36DTO.setTiping(tiping);
			 cocow36DTO.setNomcam(nomcam);
			 cocow36DTO.setValnu1(valnu1);
			 cocow36DTO.setValnu2(valnu2);
			 cocow36DTO.setValalf(valalf);
			 lista.add(cocow36DTO);
			 return lista;
			 
		 }
		 public List<Cocow36DTO> getAgregaListaMD5(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf, String resval){
			 Cocow36DTO cocow36DTO = new Cocow36DTO();
			 cocow36DTO.setTiping(tiping);
			 cocow36DTO.setNomcam(nomcam);
			 cocow36DTO.setValnu1(valnu1);
			 cocow36DTO.setValnu2(valnu2);
			 cocow36DTO.setValalf(valalf);
			 cocow36DTO.setResval(resval);
			 lista.add(cocow36DTO);
			 return lista;
			 
		 }
			public boolean getActualizaFactura(HttpServletRequest req, int rutide, String digide, int numDoc, String tipoProceso){
				boolean registra = false;
				List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
				listCocow36DTO = getCreaCocow36(req, rutide, digide,tipoProceso);
				Ingreso_Documento ingresoDocumento = new Ingreso_Documento(tipoProceso,rutide,digide);
				ingresoDocumento.setNumdoc(numDoc);
					
				registra = ingresoDocumento.Actualizar_Documento(listCocow36DTO);
				
			
				return registra;
			}
			public boolean saveAutoriza(int numdoc, int rutUsuario, String digide, String nomTipo, String tipCue){
				PreswBean preswbean = new PreswBean(nomTipo, 0,0,0,0, rutUsuario);
				preswbean.setDigide(digide);
				preswbean.setNumdoc(numdoc);
				preswbean.setTipcue(tipCue);
			return preswbean.ingreso_presw19();
			}
			public boolean saveRecepciona(int numdoc, int rutUsuario, String digide, String tipo){
				PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
				preswbean.setDigide(digide);
				preswbean.setNumdoc(numdoc);
			
				
			return preswbean.ingreso_presw19();
			}
			 public boolean getEliminaFactura( int rut, String dv, int numdoc, String nomTipo, String tipCue ){
				  	PreswBean preswbean = null;
					Collection<Presw18DTO> lista = null;
					long saldoCuenta = 0;
					preswbean = new PreswBean(nomTipo,0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
					preswbean.setRutide(rut);
					preswbean.setDigide(dv);
					preswbean.setNumdoc(numdoc);
					preswbean.setTipcue(tipCue);
					
					return preswbean.ingreso_presw19();	
					
			 } 
			 
			 public Collection<Presw18DTO> getVerificaNotaCredito(int rutUsuario, String dv, String tipoDocumento,int numeroFactura, String tipoNota) throws Exception {
					/*	se verifica en nota de credito si existe segun datos enviados */
						PreswBean preswbean = null;
						Collection<Presw18DTO> lista = null;					
						preswbean = new PreswBean("VNF",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
						preswbean.setRutide(rutUsuario);
						preswbean.setDigide(dv);
						//preswbean.setSucur(sede);
						preswbean.setNumdoc(numeroFactura);
						preswbean.setTipdoc(tipoDocumento);
						lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
					
					return lista;
					}
			 
				public void agregaListaFacturacion(HttpServletRequest req, Vector<String> lista){
					HttpSession sesion = req.getSession();
					sesion.setAttribute("listadoFacturacion",lista);

				}
			 
				public String formateoNumeroEntero(int numero){
					/*formatear n�mero */
				     Locale l = new Locale("es","CL");
				     DecimalFormat formatter1 = (DecimalFormat)DecimalFormat.getInstance(l);
				     formatter1.applyPattern("###,###,###");
				 
				   //  formatter1.format(rs.getDouble("RUTNUM"))   /*sin decimales*/
				 return formatter1.format(numero);
				}
				public String formateoNumeroLong(long numero){
					/*formatear n�mero */
				     Locale l = new Locale("es","CL");
				     DecimalFormat formatter1 = (DecimalFormat)DecimalFormat.getInstance(l);
				     formatter1.applyPattern("###,###,###");
				 
				   //  formatter1.format(rs.getDouble("RUTNUM"))   /*sin decimales*/
				 return formatter1.format(numero);
				}
				public String eliminaBlancosString(String sTexto){
					/*elimina blancos entre medio*/
				   String linea = "";
				    
				    
				    for (int x=0; x < sTexto.length(); x++) {
				    	  if ((sTexto.charAt(x) != ' ' || 
				    		  (x < (sTexto.length() - 1) && sTexto.charAt(x) == ' '  && sTexto.charAt(x+1) != ' ')) &&
				    		  sTexto.charAt(x) != '\'' &&
				    		  sTexto.charAt(x) != '\"')
				    		  linea += sTexto.charAt(x);
				    	}				
				
				 return linea;
				}
				public boolean saveCliente(List<Presw25DTO> lista,int rutUsuario){
					PreswBean preswbean = new PreswBean("CLI", 0, 0, 0, 0, rutUsuario);
					
					return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
				} 
				 public String eliminaAcentosString(String sTexto){
						/*elimina acentos del string*/
					   String linea = "";  
					    if(!sTexto.trim().equals("")){
					    for (int x=0; x < sTexto.length(); x++) {
					    	  if ((sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') || 
					    	      (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
					    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') || 
					    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
					    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�')){
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'a';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'A';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'e';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'E';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'i';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'I';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'o';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'O';
					    		   if(sTexto.charAt(x) == '�' )
					    			  linea += 'u';
					    		  if(sTexto.charAt(x) == '�' )
					    			  linea += 'U';
					    	  }
					    	  else  linea += sTexto.charAt(x);
					    	}				
					    }
					
					 return linea;
					}
		/*		 
public List<Presw25DTO> getDatosAlumno(	int sede, 
									int rutAlumno, 
									String digRut , 
									int codCarrera, 
									int annioArancel)throws SQLException{
						
//	String query = 	" SELECT AL.APATERNO, AL.AMATERNO, AL.NOMBRES, CARR.NOMCAR" +
//					" FROM USMMBP.COVEF03@aslink COVER, ALUMNOBE.VAPFALUMVI@aslink AL, USMMBP.COVEF01@aslink CARR" +
//					" WHERE COVER.RUTIDE = "+rutAlumno +
//					" AND COVER.DIGIDE = " + digRut +
//					" AND COVER.SUCUR = " + sede +
//					" AND COVER.CODCAR = " + codCarrera +
//					" AND COVER.ANOARA = " + annioArancel +
//					" AND COVER.RUTIDE = AL.RUTNUM" +
//					" AND COVER.DIGIDE = AL.RUTDV" +
//					" AND CARR.CODCAR = COVER.CODCAR" +
//					" AND CARR.SUCUR = COVER.SUCUR"; 
	// con vista
	String query = 	" SELECT NOMBRE, NOMCAR, NOMSUC" +
					" FROM view_alumno_descad" +
					" WHERE RUTIDE = "+rutAlumno +
					" AND DIGIDE = " + digRut +
					" AND SUCUR = " + sede +
					" AND CODCAR = " + codCarrera +
					" AND ANOARA = " + annioArancel ;
	
	System.out.println("query: "+ query);
	 List<Presw25DTO> datosAlumno = new ArrayList<Presw25DTO>();
	try {
		ConexionBD con = new ConexionBD();
		Statement sent = con.conexion().createStatement();
		ResultSet res = sent.executeQuery(query);
		if (res.next()) {
			Presw25DTO alum= new Presw25DTO();
			alum.setRutide(rutAlumno);
			alum.setDigide(digRut);
			alum.setComen1(res.getString(1)); // Nombre
			alum.setAnopre(annioArancel); // a�o arancel
			alum.setCoduni(codCarrera); // cod carrera
			alum.setComen4(res.getString(2)); // NOM CARRERA
			alum.setCanman(sede); // sede
			alum.setComen5(res.getString(3)); // nom sede
			datosAlumno.add(alum);
		}
		res.close();
		sent.close();
		con.close();
		}
	catch (SQLException e) {
		System.out.println("Error en moduloFacturacion.getDatosAlumno:" + e.toString());
		}
	return datosAlumno;					
}
public List<Presw25DTO> getCarrerasVespertinas(	int sede)throws SQLException{
		
	
	
	String query = 	" SELECT CODCAR, NOMCAR" +
					" FROM view_carrera_descad" +
					" WHERE SUCUR = " + sede +
					" ORDER BY CODCAR";
		
		//System.out.println("query: "+ query);
		List<Presw25DTO> listaCarreras = new ArrayList<Presw25DTO>();
		try {
			ConexionBD con = new ConexionBD();
			Statement sent = con.conexion().createStatement();
			ResultSet res = sent.executeQuery(query);
			while (res.next()) {
				Presw25DTO carr= new Presw25DTO();
				carr.setCoduni(res.getInt(1)); // cod carrera
				carr.setDesuni(res.getString(2)); // nom carrera
				listaCarreras.add(carr);
			}
			res.close();
			sent.close();
			con.close();
		}
		catch (SQLException e) {
		System.out.println("Error en moduloFacturacion.getCarrerasVespertinas:" + e.toString());
		}
		return listaCarreras;					
		}*/
}
