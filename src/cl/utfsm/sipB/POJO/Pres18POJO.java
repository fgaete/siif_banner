package cl.utfsm.sipB.POJO;

public class Pres18POJO implements java.io.Serializable , Comparable<Pres18POJO> {
	  private String coduni = "";
	  private int nummes = 0;
	  private String tippro = "";
	  private long presum = 0L;
	  private long usadom = 0L;
	  private String indprc = "";
	  private long diferm = 0L;
	  private long acumum = 0L;
	  private float porc1 = 0;
	  private float porc2 = 0;
	  private long saldoMes = 0;
	  private long total = 0;
	  private String tipodoc = "";
	  private String numdocbanner = "";
	  private String indhay = "";
	  private String tipmov = "";
	  private String nomtip = "";
	  private String itedoc = "";
	  private String  numdoc = "";
	  private String fecdoc = "";
	  private String nompro = "";
	  private String indpro = "";
	  private int valdoc = 0;
	  private String iddigi = "";
	  private int rutide = 0;
	  private String idsoli = "";
	  private long presac = 0L;
	  private long usadac = 0L;
	  private long preano = 0L;
	  private String desuni = "";
	  private String desite = "";
	  private long total_fin = 0;
	  private String pagos = "";
	  private String operacion= "";
	  private String rut= "";
	  private String unidad= "";
	  
	  
	  
	  
	  
	  public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public long getTotal_fin() {
		return total_fin;
	}
	  
	  public String getOperacion() {
			return operacion;
		}
		
		public void setOperacion(String operacion) {
			this.operacion = operacion;
		}

	public void setTotal_fin(long total_fin) {
		this.total_fin = total_fin;
	}

	public String getPagos() {
		return pagos;
	}
	
	public void setPagos(String pagos) {
		this.pagos = pagos;
	}
	
	public String getTipodoc() {
		return tipodoc;
	}

	public void setTipodoc(String tipodoc) {
		this.tipodoc = tipodoc;
	}

	public String getNumdocbanner() {
		return numdocbanner;
	}

	public void setNumdocbanner(String numdocbanner) {
		this.numdocbanner = numdocbanner;
	}

	public Pres18POJO() {

	    }
	  
	  public String getCoduni()
	  {
	    return this.coduni;
	  }
	  
	  public void setCoduni(String coduni)
	  {
	    this.coduni = coduni;
	  }
	  
	  public int getNummes()
	  {
	    return this.nummes;
	  }
	  
	  public void setNummes(int nummes)
	  {
	    this.nummes = nummes;
	  }
	  
	  public String getTippro()
	  {
	    return this.tippro;
	  }
	  
	  public void setTippro(String tippro)
	  {
	    this.tippro = tippro;
	  }
	  
	  public long getPresum()
	  {
	    return this.presum;
	  }
	  
	  public void setPresum(long presum)
	  {
	    this.presum = presum;
	  }
	  
	  public long getUsadom()
	  {
	    return this.usadom;
	  }
	  
	  public void setUsadom(long usadom)
	  {
	    this.usadom = usadom;
	  }
	  
	  public String getIndprc()
	  {
	    return this.indprc;
	  }
	  
	  public void setIndprc(String indprc)
	  {
	    this.indprc = indprc;
	  }
	  
	  public long getDiferm()
	  {
	    return this.diferm;
	  }
	  
	  public void setDiferm(long diferm)
	  {
	    this.diferm = diferm;
	  }
	  
	  public long getAcumum()
	  {
	    return this.acumum;
	  }
	  
	  public void setAcumum(long acumum)
	  {
	    this.acumum = acumum;
	  }
	  
	  public String getIndhay()
	  {
	    return this.indhay;
	  }
	  
	  public void setIndhay(String indhay)
	  {
	    this.indhay = indhay;
	  }
	  
	  public String getTipmov()
	  {
	    return this.tipmov;
	  }
	  
	  public void setTipmov(String tipmov)
	  {
	    this.tipmov = tipmov;
	  }
	  
	  public String getNomtip()
	  {
	    return this.nomtip;
	  }
	  
	  public void setNomtip(String nomtip)
	  {
	    this.nomtip = nomtip;
	  }
	  
	  public String getItedoc()
	  {
	    return this.itedoc;
	  }
	  
	  public void setItedoc(String itedoc)
	  {
	    this.itedoc = itedoc;
	  }
	  
	  public String getNumdoc()
	  {
	    return this.numdoc;
	  }
	  
	  public void setNumdoc(String numdoc)
	  {
	    this.numdoc = numdoc;
	  }
	  
	  public String getFecdoc()
	  {
	    return this.fecdoc;
	  }
	  
	  public void setFecdoc(String fecdoc)
	  {
	    this.fecdoc = fecdoc;
	  }
	  
	  public String getNompro()
	  {
	    return this.nompro;
	  }
	  
	  public void setNompro(String nompro)
	  {
	    this.nompro = nompro;
	  }
	  
	  public String getIndpro()
	  {
	    return this.indpro;
	  }
	  
	  public void setIndpro(String indpro)
	  {
	    this.indpro = indpro;
	  }
	  
	  public int getValdoc()
	  {
	    return this.valdoc;
	  }
	  
	  public void setValdoc(int valdoc)
	  {
	    this.valdoc = valdoc;
	  }
	  
	  public String getIddigi()
	  {
	    return this.iddigi;
	  }
	  
	  public void setIddigi(String iddigi)
	  {
	    this.iddigi = iddigi;
	  }
	  
	  public int getRutide()
	  {
	    return this.rutide;
	  }
	  
	  public void setRutide(int rutide)
	  {
	    this.rutide = rutide;
	  }
	  
	  public String getIdsoli()
	  {
	    return this.idsoli;
	  }
	  
	  public void setIdsoli(String idsoli)
	  {
	    this.idsoli = idsoli;
	  }
	  
	  public long getPresac()
	  {
	    return this.presac;
	  }
	  
	  public void setPresac(long presac)
	  {
	    this.presac = presac;
	  }
	  
	  public long getUsadac()
	  {
	    return this.usadac;
	  }
	  
	  public void setUsadac(long usadac)
	  {
	    this.usadac = usadac;
	  }
	  
	  public long getPreano()
	  {
	    return this.preano;
	  }
	  
	  public void setPreano(long preano)
	  {
	    this.preano = preano;
	  }
	  
	  public String getDesuni()
	  {
	    return this.desuni;
	  }
	  
	  public void setDesuni(String desuni)
	  {
	    this.desuni = desuni;
	  }
	  
	  public String getDesite()
	  {
	    return this.desite;
	  }
	  
	  public void setDesite(String desite)
	  {
	    this.desite = desite;
	  }
	 
	  public long getSaldoMes()
	  {
	    return this.saldoMes;
	  }
	  
 	  public void setSaldoMes(long saldoMes)
	  {
	    this.saldoMes = saldoMes;
	  }
	  
	  public long getTotal()
	  {
	    return this.total;
	  }
	  
	  public void setTotal(long total)
	  {
	    this.total = total;
	  }
	  
	  
	  public float getPorc1()
	  {
	    return this.porc1;
	  }
	  
	  public void setPorc1(float porc1)
	  {
	    this.porc1 = porc1;
	  }
	  
	  public float getPorc2()
	  {
	    return this.porc2;
	  }
	  
	  public void setPorc2(float porc2)
	  {
	    this.porc2 = porc2;
	  }
	  
	  public String toString()
	  {
	    StringBuffer returnStringBuffer = new StringBuffer(800);
	    returnStringBuffer.append("[");
	    returnStringBuffer.append("coduni:").append(this.coduni);
	    returnStringBuffer.append("nummes:").append(this.nummes);
	    returnStringBuffer.append("tippro:").append(this.tippro);
	    returnStringBuffer.append("presum:").append(this.presum);
	    returnStringBuffer.append("usadom:").append(this.usadom);
	    returnStringBuffer.append("indprc:").append(this.indprc);
	    returnStringBuffer.append("diferm:").append(this.diferm);
	    returnStringBuffer.append("acumum:").append(this.acumum);
	    returnStringBuffer.append("indhay:").append(this.indhay);
	    returnStringBuffer.append("tipmov:").append(this.tipmov);
	    returnStringBuffer.append("nomtip:").append(this.nomtip);
	    returnStringBuffer.append("itedoc:").append(this.itedoc);
	    returnStringBuffer.append("numdoc:").append(this.numdoc);
	    returnStringBuffer.append("fecdoc:").append(this.fecdoc);
	    returnStringBuffer.append("nompro:").append(this.nompro);
	    returnStringBuffer.append("indpro:").append(this.indpro);
	    returnStringBuffer.append("valdoc:").append(this.valdoc);
	    returnStringBuffer.append("iddigi:").append(this.iddigi);
	    returnStringBuffer.append("rutide:").append(this.rutide);
	    returnStringBuffer.append("idsoli:").append(this.idsoli);
	    returnStringBuffer.append("presac:").append(this.presac);
	    returnStringBuffer.append("usadac:").append(this.usadac);
	    returnStringBuffer.append("preano:").append(this.preano);
	    returnStringBuffer.append("desuni:").append(this.desuni);
	    returnStringBuffer.append("desite:").append(this.desite);
	    returnStringBuffer.append("]");
	    return returnStringBuffer.toString();
	  }
	  
	  @Override
		public int compareTo(Pres18POJO cuentas) {
			// TODO Auto-generated method stub		
			return this.itedoc.compareTo(cuentas.getItedoc());
		}
	}