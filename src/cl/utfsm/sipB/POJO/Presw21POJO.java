package cl.utfsm.sipB.POJO;

public class Presw21POJO implements java.io.Serializable {

	
	   private String numdoc;
	   private String tipdoc;
	   private int fecdoc;
	   private int sucur;
	   private int rutide;
	   private String digide;
	   private int fecven;
	   private int nrodoc;
	   private String tipmov;
	   private String tipcue;
	   private int mesdoc;
	   private int diadoc;
	   private int anodoc;
	   private int codban;
	   private String cajero;
	   private int tippag;
	   private int numdo1;
	   private String tipdo1;
	   private String glosa1;
	   private String glosa2;
	   private String glosa3;
	   private String glosa4;
	   private String glosa5;
	   private String glosa6;
	   private String glosa7;
	   private String glosa8;
	   private String glosa9;
	   private String glosa10;
	   private String glosa11;
	   private String glosa12;
	   private long valor1;
	   private long valor2;
	   private long valor3;
	   private long valor4;
	   private long valor5;
	   private long valor6;
	   private long valor7;
	   private long valor8;
	   private int codun1;
	   private int codun2;
	   private int codun3;
	   private int codun4;
	   private int codun5;
	   private int codun6;
	   private String idsoli;
	   private int rutid1;
	   private String tippro;
	   private String glosa13;
	   
	   public String getNumdoc()
	   {
	    return this.numdoc;
	   }
	   
	   public void setNumdoc(String numdoc) {
	     this.numdoc = numdoc;
	   }
	   
	   public String getTipdoc() {
	     return this.tipdoc;
	   }
	   
	   public void setTipdoc(String tipdoc) {
	   this.tipdoc = tipdoc;
	   }
	   
	   public int getFecdoc() {
	   return this.fecdoc;
	   }
	   
	   public void setFecdoc(int fecdoc) {
	     this.fecdoc = fecdoc;
	   }
	   
	   public int getSucur() {
	    return this.sucur;
	   }
	   
	   public void setSucur(int sucur) {
	     this.sucur = sucur;
	   }
	   
	   public int getRutide() {
	     return this.rutide;
	   }
	   
	   public void setRutide(int rutide) {
	  this.rutide = rutide;
	   }
	   
	   public String getDigide() {
	   return this.digide;
	   }
	   
	   public void setDigide(String digide) {
	    this.digide = digide;
	   }
	   
	   public int getFecven() {
	    return this.fecven;
	   }
	   
	   public void setFecven(int fecven) {
	   this.fecven = fecven;
	   }
	   
	   public int getNrodoc() {
	    return this.nrodoc;
	   }
	   
	   public void setNrodoc(int nrodoc) {
	    this.nrodoc = nrodoc;
	   }
	   
	   public String getTipmov() {
	   return this.tipmov;
	   }
	   
	   public void setTipmov(String tipmov) {
	    this.tipmov = tipmov;
	   }
	   
	   public String getTipcue() {
	   return this.tipcue;
	   }
	   
	   public void setTipcue(String tipcue) {
	    this.tipcue = tipcue;
	   }
	   
	   public int getMesdoc() {
	   return this.mesdoc;
	   }
	   
	   public void setMesdoc(int mesdoc) {
	   this.mesdoc = mesdoc;
	   }
	   
	   public int getDiadoc() {
	     return this.diadoc;
	   }
	   
	   public void setDiadoc(int diadoc) {
	    this.diadoc = diadoc;
	   }
	   
	   public int getAnodoc() {
	    return this.anodoc;
	   }
	   
	   public void setAnodoc(int anodoc) {
	   this.anodoc = anodoc;
	   }
	   
	   public int getCodban() {
	    return this.codban;
	   }
	   
	   public void setCodban(int codban) {
	   this.codban = codban;
	   }
	   
	   public String getCajero() {
	    return this.cajero;
	   }
	   
	   public void setCajero(String cajero) {
	    this.cajero = cajero;
	   }
	   
	   public int getTippag() {
	return this.tippag;
	   }
	   
	   public void setTippag(int tippag) {
	    this.tippag = tippag;
	   }
	   
	   public int getNumdo1() {
	   return this.numdo1;
	   }
	   
	   public void setNumdo1(int numdo1) {
	   this.numdo1 = numdo1;
	   }
	   
	   public String getTipdo1() {
	    return this.tipdo1;
	   }
	   
	   public void setTipdo1(String tipdo1) {
	     this.tipdo1 = tipdo1;
	   }
	   
	   public String getGlosa1() {
	    return this.glosa1;
	   }
	   
	   public void setGlosa1(String glosa1) {
	     this.glosa1 = glosa1;
	   }
	   
	   public String getGlosa2() {
	     return this.glosa2;
	   }
	   
	   public void setGlosa2(String glosa2) {
	  this.glosa2 = glosa2;
	   }
	   
	   public String getGlosa3() {
	     return this.glosa3;
	   }
	   
	   public void setGlosa3(String glosa3) {
	     this.glosa3 = glosa3;
	   }
	   
	   public String getGlosa4() {
	    return this.glosa4;
	   }
	   
	   public void setGlosa4(String glosa4) {
	    this.glosa4 = glosa4;
	   }
	   
	   public String getGlosa5() {
	    return this.glosa5;
	   }
	   
	   public void setGlosa5(String glosa5) {
	    this.glosa5 = glosa5;
	   }
	   
	   public String getGlosa6() {
	    return this.glosa6;
	   }
	   
	   public void setGlosa6(String glosa6) {
	   this.glosa6 = glosa6;
	   }
	   
	   public String getGlosa7() {
	    return this.glosa7;
	   }
	   
	   public void setGlosa7(String glosa7) {
	    this.glosa7 = glosa7;
	   }
	   
	   public String getGlosa8() {
	     return this.glosa8;
	   }
	   
	   public void setGlosa8(String glosa8) {
	    this.glosa8 = glosa8;
	   }
	   
	   public String getGlosa9() {
	     return this.glosa9;
	   }
	   
	   public void setGlosa9(String glosa9) {
	   this.glosa9 = glosa9;
	   }
	   
	   public String getGlosa10() {
	    return this.glosa10;
	   }
	   
	   public void setGlosa10(String glosa10) {
	    this.glosa10 = glosa10;
	   }
	   
	   public String getGlosa11() {
	    return this.glosa11;
	   }
	   
	   public void setGlosa11(String glosa11) {
	    this.glosa11 = glosa11;
	   }
	   
	   public String getGlosa12() {
	    return this.glosa12;
	   }
	   
	   public void setGlosa12(String glosa12) {
	    this.glosa12 = glosa12;
	   }
	   
	   public long getValor1() {
	    return this.valor1;
	   }
	   
	   public void setValor1(long valor1) {
	     this.valor1 = valor1;
	   }
	   
	   public long getValor2() {
	    return this.valor2;
	   }
	   
	   public void setValor2(long valor2) {
	     this.valor2 = valor2;
	   }
	   
	   public long getValor3() {
	    return this.valor3;
	   }
	   
	   public void setValor3(long valor3) {
	    this.valor3 = valor3;
	   }
	   
	   public long getValor4() {
	     return this.valor4;
	   }
	   
	   public void setValor4(long valor4) {
	    this.valor4 = valor4;
	   }
	   
	   public long getValor5() {
	     return this.valor5;
	   }
	   
	   public void setValor5(long valor5) {
	     this.valor5 = valor5;
	   }
	   
	   public long getValor6() {
	return this.valor6;
	   }
	   
	   public void setValor6(long valor6) {
	   this.valor6 = valor6;
	   }
	   
	   public long getValor7() {
	    return this.valor7;
	   }
	   
	   public void setValor7(long valor7) {
	    this.valor7 = valor7;
	   }
	   
	   public long getValor8() {
	    return this.valor8;
	   }
	   
	   public void setValor8(long valor8) {
	   this.valor8 = valor8;
	   }
	   
	   public int getCodun1() {
	    return this.codun1;
	   }
	   
	   public void setCodun1(int codun1) {
	    this.codun1 = codun1;
	   }
	   
	   public int getCodun2() {
	    return this.codun2;
	   }
	   
	   public void setCodun2(int codun2) {
	   this.codun2 = codun2;
	   }
	   
	   public int getCodun3() {
	     return this.codun3;
	   }
	   
	   public void setCodun3(int codun3) {
	    this.codun3 = codun3;
	   }
	   
	   public int getCodun4() {
	    return this.codun4;
	   }
	   
	   public void setCodun4(int codun4) {
	    this.codun4 = codun4;
	   }
	   
	   public int getCodun5() {
	    return this.codun5;
	   }
	   
	   public void setCodun5(int codun5) {
	    this.codun5 = codun5;
	   }
	   
	   public int getCodun6() {
	   return this.codun6;
	   }
	   
	   public void setCodun6(int codun6) {
	    this.codun6 = codun6;
	   }
	   
	   public String getIdsoli() {
	    return this.idsoli;
	   }
	   
	   public void setIdsoli(String idsoli) {
	     this.idsoli = idsoli;
	   }
	   
	   public int getRutid1() {
	     return this.rutid1;
	   }
	   
	   public void setRutid1(int rutid1) {
	    this.rutid1 = rutid1;
	   }
	   
	   public String getTippro() {
	    return this.tippro;
	   }
	   
	   public String getGlosa13() {
	    return this.glosa13;
	   }
	   
	   public void setTippro(String tippro) {
	    this.tippro = tippro;
	   }
	   
	   public void setGlosa13(String glosa13) {
	     this.glosa13 = glosa13;
	   }
	   
	   public boolean equals(Object obj) {
	   if (this == obj)
	       return true;
	    if (!(obj instanceof Presw21POJO))
	       return false;
	    Presw21POJO that = (Presw21POJO)obj;
	     if (that.numdoc != this.numdoc)
	      return false;
	     if (that.tipdoc == null ? this.tipdoc != null : !that.tipdoc.equals(this.tipdoc))
	     {
	      return false; }
	     if (that.fecdoc != this.fecdoc)
	      return false;
	     if (that.sucur != this.sucur)
	       return false;
	     if (that.rutide != this.rutide)
	       return false;
	     if (that.digide == null ? this.digide != null : !that.digide.equals(this.digide))
	     {
	       return false; }
	     if (that.fecven != this.fecven)
	       return false;
	     if (that.nrodoc != this.nrodoc)
	       return false;
	     if (that.tipmov == null ? this.tipmov != null : !that.tipmov.equals(this.tipmov))
	     {
	       return false; }
	     if (that.tipcue == null ? this.tipcue != null : !that.tipcue.equals(this.tipcue))
	     {
	       return false; }
	     if (that.mesdoc != this.mesdoc)
	      return false;
	     if (that.diadoc != this.diadoc)
	      return false;
	     if (that.anodoc != this.anodoc)
	      return false;
	    if (that.codban != this.codban)
	     return false;
	    if (that.cajero == null ? this.cajero != null : !that.cajero.equals(this.cajero))
	     {
	      return false; }
	   if (that.tippag != this.tippag)
	      return false;
	    if (that.numdo1 != this.numdo1)
	      return false;
	    if (that.tipdo1 == null ? this.tipdo1 != null : !that.tipdo1.equals(this.tipdo1))
	     {
	       return false; }
	   if (that.glosa1 == null ? this.glosa1 != null : !that.glosa1.equals(this.glosa1))
	     {
	     return false; }
	    if (that.glosa2 == null ? this.glosa2 != null : !that.glosa2.equals(this.glosa2))
	     {
	      return false; }
	    if (that.glosa3 == null ? this.glosa3 != null : !that.glosa3.equals(this.glosa3))
	     {
	     return false; }
	   if (that.glosa4 == null ? this.glosa4 != null : !that.glosa4.equals(this.glosa4))
	     {
	      return false; }
	    if (that.glosa5 == null ? this.glosa5 != null : !that.glosa5.equals(this.glosa5))
	     {
	      return false; }
	    if (that.glosa6 == null ? this.glosa6 != null : !that.glosa6.equals(this.glosa6))
	     {
	      return false; }
	    if (that.glosa7 == null ? this.glosa7 != null : !that.glosa7.equals(this.glosa7))
	     {
	       return false; }
	   if (that.glosa8 == null ? this.glosa8 != null : !that.glosa8.equals(this.glosa8))
	     {
	      return false; }
	    if (that.glosa9 == null ? this.glosa9 != null : !that.glosa9.equals(this.glosa9))
	     {
	      return false; }
	    if (that.glosa10 == null ? this.glosa10 != null : !that.glosa10.equals(this.glosa10))
	     {
	     return false; }
	    if (that.glosa11 == null ? this.glosa11 != null : !that.glosa11.equals(this.glosa11))
	     {
	      return false; }
	     if (that.glosa12 == null ? this.glosa12 != null : !that.glosa12.equals(this.glosa12))
	     {
	      return false; }
	   if (that.valor1 != this.valor1)
	      return false;
	    if (that.valor2 != this.valor2)
	      return false;
	    if (that.valor3 != this.valor3)
	      return false;
	  if (that.valor4 != this.valor4)
	      return false;
	     if (that.valor5 != this.valor5)
	      return false;
	     if (that.valor6 != this.valor6)
	      return false;
	    if (that.valor7 != this.valor7)
	      return false;
	    if (that.valor8 != this.valor8)
	      return false;
	    if (that.codun1 != this.codun1)
	      return false;
	    if (that.codun2 != this.codun2)
	      return false;
	     if (that.codun3 != this.codun3)
	      return false;
	     if (that.codun4 != this.codun4)
	       return false;
	     if (that.codun5 != this.codun5)
	       return false;
	    if (that.codun6 != this.codun6)
	      return false;
	    if (that.idsoli == null ? this.idsoli != null : !that.idsoli.equals(this.idsoli))
	     {
	      return false; }
	     if (that.rutid1 != this.rutid1)
	      return false;
	    if (that.tippro == null ? this.tippro != null : !that.tippro.equals(this.tippro))
	     {
	     return false; }
	     if (that.glosa13 == null ? this.glosa13 != null : !that.glosa13.equals(this.glosa13))
	     {
	      return false;
	     }
	    return true;
	   }
	   
	  public int hashCode() { int result = 17;
	  // result = 37 * result + this.numdoc;
	    result = 37 * result + this.tipdoc.hashCode();
	     result = 37 * result + this.fecdoc;
	    result = 37 * result + this.sucur;
	     result = 37 * result + this.rutide;
	    result = 37 * result + this.digide.hashCode();
	     result = 37 * result + this.fecven;
	    result = 37 * result + this.nrodoc;
	    result = 37 * result + this.tipmov.hashCode();
	    result = 37 * result + this.tipcue.hashCode();
	    result = 37 * result + this.mesdoc;
	    result = 37 * result + this.diadoc;
	    result = 37 * result + this.anodoc;
	   result = 37 * result + this.codban;
	    result = 37 * result + this.cajero.hashCode();
	    result = 37 * result + this.tippag;
	    result = 37 * result + this.numdo1;
	    result = 37 * result + this.tipdo1.hashCode();
	    result = 37 * result + this.glosa1.hashCode();
	     result = 37 * result + this.glosa2.hashCode();
	    result = 37 * result + this.glosa3.hashCode();
	    result = 37 * result + this.glosa4.hashCode();
	    result = 37 * result + this.glosa5.hashCode();
	   result = 37 * result + this.glosa6.hashCode();
	    result = 37 * result + this.glosa7.hashCode();
	    result = 37 * result + this.glosa8.hashCode();
	   result = 37 * result + this.glosa9.hashCode();
	    result = 37 * result + this.glosa10.hashCode();
	     result = 37 * result + this.glosa11.hashCode();
	     result = 37 * result + this.glosa12.hashCode();
	     result = 37 * result + (int)this.valor1;
	     result = 37 * result + (int)this.valor2;
	    result = 37 * result + (int)this.valor3;
	     result = 37 * result + (int)this.valor4;
	    result = 37 * result + (int)this.valor5;
	     result = 37 * result + (int)this.valor6;
	    result = 37 * result + (int)this.valor7;
	    result = 37 * result + (int)this.valor8;
	    result = 37 * result + this.codun1;
	    result = 37 * result + this.codun2;
	     result = 37 * result + this.codun3;
	     result = 37 * result + this.codun4;
	    result = 37 * result + this.codun5;
	     result = 37 * result + this.codun6;
	     result = 37 * result + this.idsoli.hashCode();
	    result = 37 * result + this.rutid1;
	     result = 37 * result + this.tippro.hashCode();
	    result = 37 * result + this.glosa13.hashCode();
	     return result;
	   }
	   
	   public String toString() {
	    StringBuffer returnStringBuffer = new StringBuffer(1504);
	     returnStringBuffer.append("[");
	    returnStringBuffer.append("numdoc:").append(this.numdoc);
	     returnStringBuffer.append("tipdoc:").append(this.tipdoc);
	     returnStringBuffer.append("fecdoc:").append(this.fecdoc);
	     returnStringBuffer.append("sucur:").append(this.sucur);
	    returnStringBuffer.append("rutide:").append(this.rutide);
	     returnStringBuffer.append("digide:").append(this.digide);
	    returnStringBuffer.append("fecven:").append(this.fecven);
	    returnStringBuffer.append("nrodoc:").append(this.nrodoc);
	     returnStringBuffer.append("tipmov:").append(this.tipmov);
	     returnStringBuffer.append("tipcue:").append(this.tipcue);
	     returnStringBuffer.append("mesdoc:").append(this.mesdoc);
	    returnStringBuffer.append("diadoc:").append(this.diadoc);
	     returnStringBuffer.append("anodoc:").append(this.anodoc);
	    returnStringBuffer.append("codban:").append(this.codban);
	    returnStringBuffer.append("cajero:").append(this.cajero);
	     returnStringBuffer.append("tippag:").append(this.tippag);
	    returnStringBuffer.append("numdo1:").append(this.numdo1);
	     returnStringBuffer.append("tipdo1:").append(this.tipdo1);
	     returnStringBuffer.append("glosa1:").append(this.glosa1);
	    returnStringBuffer.append("glosa2:").append(this.glosa2);
	    returnStringBuffer.append("glosa3:").append(this.glosa3);
	     returnStringBuffer.append("glosa4:").append(this.glosa4);
	    returnStringBuffer.append("glosa5:").append(this.glosa5);
	    returnStringBuffer.append("glosa6:").append(this.glosa6);
	    returnStringBuffer.append("glosa7:").append(this.glosa7);
	     returnStringBuffer.append("glosa8:").append(this.glosa8);
	     returnStringBuffer.append("glosa9:").append(this.glosa9);
	     returnStringBuffer.append("glosa10:").append(this.glosa10);
	    returnStringBuffer.append("glosa11:").append(this.glosa11);
	   returnStringBuffer.append("glosa12:").append(this.glosa12);
	   returnStringBuffer.append("valor1:").append(this.valor1);
	    returnStringBuffer.append("valor2:").append(this.valor2);
	     returnStringBuffer.append("valor3:").append(this.valor3);
	     returnStringBuffer.append("valor4:").append(this.valor4);
	    returnStringBuffer.append("valor5:").append(this.valor5);
	    returnStringBuffer.append("valor6:").append(this.valor6);
	   returnStringBuffer.append("valor7:").append(this.valor7);
	    returnStringBuffer.append("valor8:").append(this.valor8);
	     returnStringBuffer.append("codun1:").append(this.codun1);
	   returnStringBuffer.append("codun2:").append(this.codun2);
	   returnStringBuffer.append("codun3:").append(this.codun3);
	    returnStringBuffer.append("codun4:").append(this.codun4);
	     returnStringBuffer.append("codun5:").append(this.codun5);
	     returnStringBuffer.append("codun6:").append(this.codun6);
	     returnStringBuffer.append("idsoli:").append(this.idsoli);
	    returnStringBuffer.append("rutid1:").append(this.rutid1);
	     returnStringBuffer.append("tippro:").append(this.tippro);
	     returnStringBuffer.append("glosa13:").append(this.glosa13);
	    returnStringBuffer.append("]");
	     return returnStringBuffer.toString();
	   }
	 }


	/* Location:              C:\Users\Juan\Documents\descad.jar.zip!\descad\presupuesto\Presw21DTO.class
	 * Java compiler version: 6 (50.0)
	 * JD-Core Version:       0.7.1
	 */
