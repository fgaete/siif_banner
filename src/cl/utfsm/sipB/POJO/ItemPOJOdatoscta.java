package cl.utfsm.sipB.POJO;

import java.io.Serializable;


public class ItemPOJOdatoscta  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String coditem;
	private String nomtem;
	
	
	  public String getCoditem() {
		    return this.coditem;
		  }
		  
		  public void setCoditem(String coditem) {
		   this.coditem = coditem;
		  }
		  
	  	
		  public String getNomtem() {
			    return this.nomtem;
			  }
			  
			  public void setNomtem(String nomtem) {
			   this.nomtem = nomtem;
			  }
			  
}
