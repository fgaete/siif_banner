package cl.utfsm.sipB.POJO;



import java.io.Serializable;
import java.math.BigDecimal;







public class ItemPOJO
  implements Serializable
{
  private String itepre;
  private String nomite;
  private String incren;
  private String coditem;
  private String nomtem;
  private long saldo ;
	
  
  public long getSaldo() {
	return saldo;
}

public void setSaldo(long saldo) {
	this.saldo = saldo;
}

public String getItepre()
  {
    return this.itepre;
  }
  
  public void setItepre(String itepre) {
   this.itepre = itepre;
  }
  
  public String getNomite() {
    return this.nomite;
  }
  
  public void setNomite(String nomite) {
   this.nomite = nomite;
  }
  

  public String getIncren()
  {
    return this.incren;
  }
  

  public String getCoditem() {
	    return this.coditem;
	  }
	  
	  public void setCoditem(String coditem) {
	   this.coditem = coditem;
	  }
	  
	
	  public String getNomtem() {
		    return this.nomtem;
		  }
		  
		  public void setNomtem(String nomtem) {
		   this.nomtem = nomtem;
		  }


  public void setIncren(String incren) { this.incren = incren; }
  
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!(obj instanceof ItemPOJO))
     return false;
    ItemPOJO that = (ItemPOJO)obj;
    if (that.itepre == null ? this.itepre != null : !that.itepre.equals(this.itepre))
    {
    return false; }
   if (that.nomite == null ? this.nomite != null : !that.nomite.equals(this.nomite))
    {
      return false; }
    if (that.incren == null ? this.incren != null : !that.incren.equals(this.incren))
    {
      return false; }
    return true;
  }
  
  public int hashCode() {
    int result = 17;
    result = 37 * result + getItepre().hashCode();
    result = 37 * result + getNomite().hashCode();
   result = 37 * result + getIncren().hashCode();
   return result;
  }
  
  public String toString() {
    StringBuffer returnStringBuffer = new StringBuffer(64);
   returnStringBuffer.append("[");
    returnStringBuffer.append("itepre:").append(getItepre());
    returnStringBuffer.append("nomite:").append(getNomite());
   returnStringBuffer.append("incren:").append(getIncren());
    returnStringBuffer.append("]");
   return returnStringBuffer.toString();
  }
}


/* Location:              C:\Users\Juan\Documents\descad.jar.zip!\descad\presupuesto\ItemDTO.class
* Java compiler version: 6 (50.0)
* JD-Core Version:       0.7.1
*/