package cl.utfsm.sipB.modulo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.velocity.tools.generic.MathTool;
import org.hibernate.mapping.Set;

import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.auditoria.AuditoriaServicio;
import cl.utfsm.base.auditoria.DetalleServicio;
import cl.utfsm.base.auditoria.TipoAccionServicio;
import cl.utfsm.base.util.ConexionBanner;
import cl.utfsm.base.util.Util;
import cl.utfsm.conexion.ConexionAs400;
import cl.utfsm.sgf.PRESF48B;
import cl.utfsm.sip.PRESW25;
import cl.utfsm.sip.TipoCuenta;
import cl.utfsm.sip.TipoPresupuesto;
import cl.utfsm.sip.TipoUnidad;
import cl.utfsm.sipB.POJO.ItemPOJO;
import cl.utfsm.sipB.POJO.Pres18POJO;
import cl.utfsm.sipB.POJO.Presw21POJO;
import cl.utfsm.sipB.POJO.Presw25POJO;
import cl.utfsm.sipB.datos.PresupuestoDaoB;
import descad.cliente.CocofBean;
import descad.cliente.CocowBean;
import descad.cliente.Ingreso_Documento;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Cocof17DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw25DTO;
import descad.presupuesto.Presw21DTO;
public class ModuloPresupuestoB {
	PresupuestoDaoB presupuestoDaoB;

	public void agregaUnidad(HttpServletRequest req,
			Collection<Pres18POJO> lista) {
		// public void agregaUnidad(HttpServletRequest req, Collection<Presw18>
		// lista){
		HttpSession sesion = req.getSession();
		/*
		 * esto es para probar Presw18DTO lista2 = new Presw18DTO();
		 * lista2.setCoduni(340610); lista2.setDesuni("DEPARTAMENTO DE
		 * INDUSTRIA, OPERACI�N"); lista.add(lista2); /*lista2 = new
		 * Presw18DTO(); lista2.setCoduni(111319); lista2.setDesuni("INGRESOS
		 * PROPIOS UNID. ACADEMICAS"); lista.add(lista2);
		 */
		/* hasta ac� */

		// sesion.setAttribute("listaUnidad",lista);
		sesion.setAttribute("listaOrganizacion", lista);

	}

	public void agregaUsuario(HttpServletRequest req, int rutUsuario,
			String funcionario, int codigoPerfil) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("rutUsuario", rutUsuario);
		sesion.setAttribute("funcionario", funcionario);
		sesion.setAttribute("codigoPerfil", codigoPerfil);

	}

	public PresupuestoDaoB getPresupuestoDaoB() {
		return presupuestoDaoB;
	}

	public void setPresupuestoDaoB(PresupuestoDaoB presupuestoDaoB) {
		this.presupuestoDaoB = presupuestoDaoB;
	}

	public void saveAuditoriaServicio(AuditoriaServicio auditoriaServicio) {
		presupuestoDaoB.saveAuditoriaServicio(auditoriaServicio);
	}

	public DetalleServicio getDetalleServicio(Long id) {
		return presupuestoDaoB.getDetalleServicio(id);
	}

	public TipoAccionServicio getTipoAccionServicio(int codigo) {
		return presupuestoDaoB.getTipoAccionServicio(codigo);
	}

	public void agregaResulPresup(HttpServletRequest req, List<Presw25DTO> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresup", lista);

	}
	public void agregaResulPresupEsp(HttpServletRequest req, List<Presw25DTO> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresupEsp", lista);
	}
	public void agregaResulVacante(HttpServletRequest req, List<PRESW25> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresupVacantes", lista);

	}

	public void agregaResulPresupActualiza(HttpServletRequest req,
			List<PRESW25> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresup", lista);

	}

	public void agregaResultadoPresup(HttpServletRequest req,
			List<Presw25DTO> lista) {
		/* MAA itedoc por motiv2 */
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresup", lista);
		List<Presw25DTO> listaOriginal = new ArrayList<Presw25DTO>();

		for (Presw25DTO ss : lista) {
			Presw25DTO obj = new Presw25DTO();
			obj.setAnopre(ss.getAnopre());
			// obj.setItedoc(ss.getItedoc());
			obj.setMotiv2(ss.getMotiv2());
			// obj.setCoduni(ss.getCoduni());
			obj.setMotiv1(ss.getMotiv1());
			obj.setPres01(ss.getPres01());
			obj.setPres02(ss.getPres02());
			obj.setPres03(ss.getPres03());
			obj.setPres04(ss.getPres04());
			obj.setPres05(ss.getPres05());
			obj.setPres06(ss.getPres06());
			obj.setPres07(ss.getPres07());
			obj.setPres08(ss.getPres08());
			obj.setPres09(ss.getPres09());
			obj.setPres10(ss.getPres10());
			obj.setPres11(ss.getPres11());
			obj.setPres12(ss.getPres12());
			obj.setComen1(ss.getComen1());
			obj.setComen2(ss.getComen2());
			obj.setComen3(ss.getComen3());
			obj.setComen4(ss.getComen4());
			obj.setComen5(ss.getComen5());
			obj.setComen6(ss.getComen6());
			obj.setRutide(ss.getRutide());
			obj.setDigide(ss.getDigide());
			obj.setDesite(ss.getDesite());
			obj.setNumreq(ss.getNumreq());
			obj.setIndexi(ss.getIndexi());
			obj.setTotpre(ss.getTotpre());
			obj.setRutide(ss.getRutide());
			obj.setDigide(ss.getDigide());
			obj.setNumreq(ss.getNumreq());
			obj.setRespue(ss.getRespue());
			listaOriginal.add(obj);
		}

		sesion.setAttribute("Original", listaOriginal);

	}
	public void agregaResultadoPresupEsp(HttpServletRequest req,
			List<Presw25DTO> lista) {
		/* MAA itedoc por motiv2 */
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresupEsp", lista);
		List<Presw25DTO> listaOriginal = new ArrayList<Presw25DTO>();

		for (Presw25DTO ss : lista) {
			Presw25DTO obj = new Presw25DTO();
			obj.setAnopre(ss.getAnopre());
			// obj.setItedoc(ss.getItedoc());
			obj.setMotiv2(ss.getMotiv2());
			// obj.setCoduni(ss.getCoduni());
			obj.setMotiv1(ss.getMotiv1());
			obj.setPres01(ss.getPres01());
			obj.setPres02(ss.getPres02());
			obj.setPres03(ss.getPres03());
			obj.setPres04(ss.getPres04());
			obj.setPres05(ss.getPres05());
			obj.setPres06(ss.getPres06());
			obj.setPres07(ss.getPres07());
			obj.setPres08(ss.getPres08());
			obj.setPres09(ss.getPres09());
			obj.setPres10(ss.getPres10());
			obj.setPres11(ss.getPres11());
			obj.setPres12(ss.getPres12());
			obj.setComen1(ss.getComen1());
			obj.setComen2(ss.getComen2());
			obj.setComen3(ss.getComen3());
			obj.setComen4(ss.getComen4());
			obj.setComen5(ss.getComen5());
			obj.setComen6(ss.getComen6());
			obj.setRutide(ss.getRutide());
			obj.setDigide(ss.getDigide());
			obj.setDesite(ss.getDesite());
			obj.setNumreq(ss.getNumreq());
			obj.setIndexi(ss.getIndexi());
			obj.setTotpre(ss.getTotpre());
			obj.setRutide(ss.getRutide());
			obj.setDigide(ss.getDigide());
			obj.setNumreq(ss.getNumreq());
			obj.setRespue(ss.getRespue());
			listaOriginal.add(obj);
		}

		sesion.setAttribute("OriginalEsp", listaOriginal);

	}
	public void agregaRegistraMatriz(HttpServletRequest req,
			List<Presw25DTO> lista, List<Presw25DTO> lista2) {
		// MAA cambia unidad a organizacion
		HttpSession sesion = req.getSession();
		List<Presw25DTO> listaPRP = new ArrayList();
		List<Presw25DTO> listaCPR = new ArrayList();
		String indexi = Util.validaParametro(req.getParameter("indexi"), "");
		// int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		String organizacion = Util.validaParametro(req
				.getParameter("organizacion"), "");
		for (Presw25DTO ss : lista) {
			Presw25DTO presw25 = ss;
			presw25.setTippro("PRP");
			presw25.setIndexi(indexi);
			listaPRP.add((Presw25DTO) presw25);
		}
		for (Presw25DTO ss : lista2) {
			Presw25DTO presw25 = ss;
			// if(ss.getCoduni() == unidad)
			if (ss.getComen4().trim().equals(organizacion.trim()))
				presw25.setIndexi(indexi);

			listaCPR.add((Presw25DTO) presw25);
		}
		sesion.setAttribute("resultadoPresup", listaPRP);
		sesion.setAttribute("resultadoCPR", listaCPR);

	}
	
	public void agregaRegistraMatrizEsp(HttpServletRequest req,
			List<Presw25DTO> lista, List<Presw25DTO> lista2) {
		// MAA cambia unidad a organizacion
		HttpSession sesion = req.getSession();
		List<Presw25DTO> listaPRP = new ArrayList();
		List<Presw25DTO> listaCPR = new ArrayList();
		String indexi = Util.validaParametro(req.getParameter("indexi"), "");
		// int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		String organizacion = Util.validaParametro(req
				.getParameter("organizacion"), "");
		for (Presw25DTO ss : lista) {
			Presw25DTO presw25 = ss;
			presw25.setTippro("PPE");
			presw25.setIndexi(indexi);
			listaPRP.add((Presw25DTO) presw25);
		}
		for (Presw25DTO ss : lista2) {
			Presw25DTO presw25 = ss;
			// if(ss.getCoduni() == unidad)
			if (ss.getComen4().trim().equals(organizacion.trim()))
				presw25.setIndexi(indexi);

			listaCPR.add((Presw25DTO) presw25);
		}
		sesion.setAttribute("resultadoPresupEsp", listaPRP);
		sesion.setAttribute("resultadoCPREsp", listaCPR);

	}
	/*
	 * public List<Pres18POJO> getCuentas() { //Agregado cuentas banner
	 * System.out.println("getCuentas"); return presupuestoDaoB.getCuentas(); }
	 */
	
	

	public List<Presw25DTO> getListaPresw25(String tipo, int rutUsuario,
			int anno, String dv, int sucur, int codUni, int numDoc) {
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(dv);
		if (anno > 0)
			preswbean.setAnopar(anno);
		if (sucur > 0)
			preswbean.setSucur(sucur);
		if (codUni > 0)
			preswbean.setCoduni(codUni);
		if (numDoc > 0)
			preswbean.setNumdoc(numDoc);
		Collection<Presw25DTO> listaPresw25 = (Collection<Presw25DTO>) preswbean
				.consulta_presw25();
		MathTool mathtool = new MathTool();
		for (Presw25DTO ss : listaPresw25) {
			presw25DTO = new Presw25DTO();
			presw25DTO.setMespre(ss.getMespre());
			presw25DTO.setCoduni(ss.getCoduni());
			presw25DTO.setComen1(ss.getComen1());
			/*
			 * Vanessa dice que no se debe dividir por 1 mill�n los valores,
			 * para asi tener valores m�s exactos
			 * presw25DTO.setPres01(mathtool.round(mathtool.div(ss.getPres01(),1000000)));
			 * presw25DTO.setPres02(mathtool.round(mathtool.div(ss.getPres02(),1000000)));
			 * presw25DTO.setPres03(mathtool.round(mathtool.div(ss.getPres03(),1000000)));
			 * presw25DTO.setPres04(mathtool.round(mathtool.div(ss.getPres04(),1000000)));
			 * presw25DTO.setPres05(mathtool.round(mathtool.div(ss.getPres05(),1000000)));
			 * presw25DTO.setPres06(mathtool.round(mathtool.div(ss.getPres06(),1000000)));
			 * presw25DTO.setPres07(mathtool.round(mathtool.div(ss.getPres07(),1000000)));
			 * presw25DTO.setPres08(mathtool.round(mathtool.div(ss.getPres08(),1000000)));
			 * presw25DTO.setPres09(mathtool.round(mathtool.div(ss.getPres09(),1000000)));
			 * presw25DTO.setPres11(mathtool.round(mathtool.div(ss.getPres11(),1000000)));
			 * presw25DTO.setPres12(mathtool.round(mathtool.div(ss.getPres12(),1000000)));
			 */
			presw25DTO.setPres01(ss.getPres01());
			presw25DTO.setPres02(ss.getPres02());
			presw25DTO.setPres03(ss.getPres03());
			presw25DTO.setPres04(ss.getPres04());
			presw25DTO.setPres05(ss.getPres05());
			presw25DTO.setPres06(ss.getPres06());
			presw25DTO.setPres07(ss.getPres07());
			presw25DTO.setPres08(ss.getPres08());
			presw25DTO.setPres09(ss.getPres09());
			presw25DTO.setPres11(ss.getPres11());
			presw25DTO.setPres12(ss.getPres12());
			presw25DTO.setComen2(ss.getComen2());
			/*
			 * segun Pedro debiera multiplicarse al recibir y dividirse al
			 * mostrar, por lo tanto no har� nada 5/05/2011
			 * presw25DTO.setPres10(mathtool.mul(ss.getPres10(),100).longValue());
			 * presw25DTO.setPres10(mathtool.mul(ss.getValuni(),100).longValue());
			 */
			presw25DTO.setPres10(ss.getPres10());
			presw25DTO.setValuni(ss.getValuni());

			presw25DTO.setCoduni(ss.getCoduni());
			lista.add(presw25DTO);
		}

		return lista;
	}

	public void agregaResultadoPresupSesion(HttpServletRequest req,
			List<Presw25DTO> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresupNomina", lista);

	}

	public void agregaResultadoCPRSesion(HttpServletRequest req,
			List<Presw25DTO> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoCPR", lista);

	}
	
	public void agregaResultadoCPREspSesion(HttpServletRequest req,
			List<Presw25DTO> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoCPREsp", lista);

	}

	public void agregaResultadoVacanteSesion(HttpServletRequest req,
			List<Presw25DTO> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresupVacante", lista);

	}

	public void agregaResultadoPresupRemSesion(HttpServletRequest req,
			List<Presw25DTO> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresupRemuneracion", lista);

	}

	public void agregaPresupRequerimientoSesion(HttpServletRequest req,
			List<Presw25DTO> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("presupItemreq", lista);

	}

	public void agregaPresupOtrosSesion(HttpServletRequest req,
			List<Presw25DTO> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("resultadoPresupIngresos", lista);

	}

	public void agregaDetalleItemReqSesion(HttpServletRequest req,
			List<Presw25DTO> lista, List<Presw25DTO> lista2) {
		HttpSession sesion = req.getSession();
		int rutUsuario = Util.validaParametro(Integer.parseInt(req.getSession()
				.getAttribute("rutUsuario")
				+ ""), 0);
		int a�o = Util.validaParametro(req.getParameter("anno"), 0);
		int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		int mes = Util.validaParametro(req.getParameter("mes"), 0);
		int sucur = Util.validaParametro(req.getParameter("sucur"), 0);
		int numrq = 0;
		Long totalMensual = Util.validaParametro(req.getParameter("total"),
				new Long(0));
		List<Presw25DTO> lista3 = new ArrayList<Presw25DTO>();
		List<Presw25DTO> lista4 = new ArrayList<Presw25DTO>();

		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();

		/*
		 * primero verificar si es requerimiento nuevo, se coloca como n�mero
		 * 1000
		 */
		if (sucur == 0) {
			if (lista2.size() > 0) {
				for (Presw25DTO ss : lista2) {
					if (ss.getNumreq() > numrq)
						numrq = ss.getNumreq();
				}
				numrq++;
			} else
				numrq = 1000;
			/* y agregarlo a la lista en lista3 */
			for (Presw25DTO ss : lista) {
				ss.setNumreq(numrq);
				ss.setValpr1(1); // esto es solo para marcar que es nuevo
									// requerimiento
				ss.setCanman(0);
				lista3.add(ss);
			}
		} else {
			numrq = sucur;
			for (Presw25DTO ss : lista) {
				lista3.add(ss);
			}
		}

		boolean agrega = false;
		/*
		 * recorre la lista de sesi�n y ve si ya existe sino lo agrega a lista4
		 * que tiene la suma final
		 */
		if (lista2 != null && lista2.size() > 0) {
			for (Presw25DTO ss : lista2) {
				if (ss.getNumreq() == numrq && ss.getItedoc() == itedoc) {
					for (Presw25DTO rs : lista3) {
						if (rs.getCoduni() == ss.getCoduni()
								&& rs.getItedoc() == ss.getItedoc()
								&& rs.getNumreq() == ss.getNumreq()
								&& rs.getMespre() == ss.getMespre()
								&& rs.getCodman() == ss.getCodman()) {
							lista4.add(rs);
							agrega = true;
						}

					}
				} else {
					lista4.add(ss);

				}
			}
		} else {
			for (Presw25DTO rs : lista3) {
				lista4.add(rs);
			}
			agrega = true;
		}

		if (!agrega) {
			for (Presw25DTO rs : lista3) {
				lista4.add(rs);
			}
		}

		sesion.setAttribute("listaPresw25", lista3);
		sesion.setAttribute("resultadoDetalleItemReq", lista4);

	}

	public void agregaResultadoPresupNomina(HttpServletRequest req,
			List<Presw25DTO> lista, List<Presw25DTO> lista2) {
		HttpSession sesion = req.getSession();
		List<Presw25DTO> listaCOP = new ArrayList();
		List<Presw25DTO> listaCPR = new ArrayList();
		if (lista.size() > 0) {
			String motiv1 = Util
					.validaParametro(req.getParameter("motiv1"), "");
			String motiv2 = Util
					.validaParametro(req.getParameter("motiv2"), "");
			String motiv3 = Util
					.validaParametro(req.getParameter("motiv3"), "");
			String motiv4 = Util
					.validaParametro(req.getParameter("motiv4"), "");
			String motiv5 = Util
					.validaParametro(req.getParameter("motiv5"), "");
			String motiv6 = Util
					.validaParametro(req.getParameter("motiv6"), "");
			String tipcon = Util
					.validaParametro(req.getParameter("tipcon"), "");
			String area1 = Util.validaParametro(req.getParameter("area1"), "");
			String area2 = Util.validaParametro(req.getParameter("area2"), "");
			String desite = Util
					.validaParametro(req.getParameter("desite"), "");
			String indexi = Util
					.validaParametro(req.getParameter("indexi"), "");
			// int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
			String organizacion = Util.validaParametro(req
					.getParameter("organizacion"), "");

			/*
			 * String motiv= Util.validaParametro(req.getParameter("motiv"),
			 * ""); if(motiv.length() <= 40) motiv1 = motiv; else { motiv1 =
			 * motiv.substring(0,40); if(motiv.length() <= 80) motiv2 =
			 * motiv.substring(40); else { motiv2 = motiv.substring(40,80);
			 * if(motiv.length() <= 120) motiv3 = motiv.substring(80); else {
			 * motiv3 = motiv.substring(80,120); if(motiv.length() <= 160)
			 * motiv4 = motiv.substring(120); else { motiv4 =
			 * motiv.substring(120, 160); if(motiv.length() <= 200) motiv5 =
			 * motiv.substring(160); else { motiv5 = motiv.substring(160,200);
			 * if(motiv.length() <= 240) motiv6 = motiv.substring(200); else {
			 * motiv6 = motiv.substring(200,240); if(motiv.length() <= 280)
			 * tipcon = motiv.substring(240); else { tipcon =
			 * motiv.substring(240,280); if(motiv.length() <= 320) area1 =
			 * motiv.substring(280); else { area1 = motiv.substring(280,320);
			 * if(motiv.length() <= 360) area2 = motiv.substring(320); else
			 * desite = motiv.substring(360,400);
			 * 
			 *  } } } } } } } }
			 */
			int i = 0;
			for (Presw25DTO ss : lista) {
				i++;
				String respue = Util.validaParametro(req.getParameter("respue"
						+ i), "");
				String comen1 = Util.validaParametro(req.getParameter("comen1"
						+ i), "");
				String comen2 = Util.validaParametro(req.getParameter("comen2"
						+ i), "");
				String comen3 = Util.validaParametro(req.getParameter("comen3"
						+ i), "");
				String comen4 = Util.validaParametro(req.getParameter("comen4"
						+ i), "");
				String comen5 = Util.validaParametro(req.getParameter("comen5"
						+ i), "");
				String comen6 = Util.validaParametro(req.getParameter("comen6"
						+ i), "");

				Presw25DTO presw25 = ss;

				presw25.setTippro("COP");
				presw25.setRespue(respue);
				presw25.setComen1(comen1);
				presw25.setComen2(comen2);
				presw25.setComen3(comen3);
				presw25.setComen4(comen4);
				presw25.setComen5(comen5);
				presw25.setComen6(comen6);
				presw25.setMotiv1(motiv1);
				presw25.setMotiv2(motiv2);
				presw25.setMotiv3(motiv3);
				presw25.setMotiv4(motiv4);
				presw25.setMotiv5(motiv5);
				presw25.setMotiv6(motiv6);
				presw25.setTipcon(tipcon);
				presw25.setArea1(area1);
				presw25.setArea2(area2);
				presw25.setDesite(desite);
				presw25.setIndexi(indexi);
				presw25.setCodman(organizacion);
				listaCOP.add((Presw25DTO) presw25);
			}
			for (Presw25DTO ss : lista2) {
				Presw25DTO presw25 = ss;
				// if(ss.getCoduni() == unidad)
				if (ss.getComen4().trim().equals(organizacion.trim()))
					presw25.setIndexi(indexi);
				listaCPR.add((Presw25DTO) presw25);
			}
			sesion.setAttribute("resultadoCPR", listaCPR);
		}
		sesion.setAttribute("resultadoPresupNomina", listaCOP);
	}

	public void agregaResultadoPresupRemuneracion(HttpServletRequest req,
			List<Presw25DTO> lista, List<Presw25DTO> lista2) {
		HttpSession sesion = req.getSession();
		List<Presw25DTO> listaASP = new ArrayList();
		List<Presw25DTO> listaCPR = new ArrayList();
		String indexi = Util.validaParametro(req.getParameter("indexi"), "");
		int unidad = Util.validaParametro(req.getParameter("unidad"), 0);

		if (lista.size() > 0) {

			int i = 0;
			for (Presw25DTO ss : lista) {
				i++;
				String valor1 = Util.validaParametro(req.getParameter("valpr1_"
						+ i), "0");
				String valor2 = Util.validaParametro(req.getParameter("valpr2_"
						+ i), "0");
				valor1 = valor1.replace(".", "");
				valor2 = valor2.replace(".", "");
				int valpr1 = Integer.parseInt(valor1);
				int valpr2 = Integer.parseInt(valor2);

				Presw25DTO presw25 = ss;

				presw25.setTippro("ASP");
				presw25.setValpr1(valpr1);
				presw25.setValpr2(valpr2);
				presw25.setIndexi(indexi);
				listaASP.add((Presw25DTO) presw25);
			}
			for (Presw25DTO ss : lista2) {
				Presw25DTO presw25 = ss;
				if (ss.getCoduni() == unidad)
					presw25.setIndexi(indexi);
				listaCPR.add((Presw25DTO) presw25);
			}
			sesion.setAttribute("resultadoCPR", listaCPR);
		}
		sesion.setAttribute("resultadoPresupRemuneracion", listaASP);
	}

	public void agregaItemReqDet(HttpServletRequest req,
			List<Presw25DTO> lista, List<Presw25DTO> presupItemreq,
			List<Presw25DTO> listarResultadoPresup) {
		// resultadoDetalleItemReq, presupItemreq, listarResultadoPresup
		HttpSession sesion = req.getSession();
		List<Presw25DTO> listaRQP = new ArrayList();
		List<Presw25DTO> listaResultado = new ArrayList();

		String area1 = Util.validaParametro(req.getParameter("area1"), "");
		String area2 = Util.validaParametro(req.getParameter("area2"), "");
		String comen1 = Util.validaParametro(req.getParameter("comen1"), "");
		String comen2 = Util.validaParametro(req.getParameter("comen2"), "");
		String comen3 = Util.validaParametro(req.getParameter("comen3"), "");
		String comen4 = Util.validaParametro(req.getParameter("comen4"), "");
		String comen5 = Util.validaParametro(req.getParameter("comen5"), "");
		String comen6 = Util.validaParametro(req.getParameter("comen6"), "");
		String motiv1 = Util.validaParametro(req.getParameter("motiv1"), "");
		String motiv2 = Util.validaParametro(req.getParameter("motiv2"), "");
		String motiv3 = Util.validaParametro(req.getParameter("motiv3"), "");
		String motiv4 = Util.validaParametro(req.getParameter("motiv4"), "");
		String motiv5 = Util.validaParametro(req.getParameter("motiv5"), "");
		String motiv6 = Util.validaParametro(req.getParameter("motiv6"), "");
		Long total = Util.validaParametro(req.getParameter("totalRequ"),
				new Long(0));
		int mes = Util.validaParametro(req.getParameter("mes"), 0);
		int numreq = Util.validaParametro(req.getParameter("sucur"), 0);
		int item = Util.validaParametro(req.getParameter("itedoc"), 0);
		int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		int anno = Util.validaParametro(req.getParameter("anno"), 0);
		Long totalTotal = Util.validaParametro(req.getParameter("totalMes"),
				new Long(0));
		int i = Util.validaParametro(req.getParameter("item"), 0);
		int itemElimina = Util.validaParametro(req.getParameter("itemElimina"),
				0);
		if (itemElimina > 0)
			i = itemElimina;
		int j = 0;
		long pres03 = 0;
		long pres01 = 0;
		String fecha = "";
		int pres02 = 0;

		int valpr = 0;
		String desite = "";
		String desuni = "";
		int canman = 0;
		long valorReq = 0;
		if (lista.size() > 0) {
			for (Presw25DTO ss : lista) {
				pres02 = 0;
				pres03 = 0;
				pres01 = 0;

				if (ss.getNumreq() == numreq && ss.getMespre() == mes) {
					// System.out.println(ss.getItedoc());
					j++;
					if (j == i) {
						if (itemElimina > 0) {
							if (ss.getCodman().trim().equals("COTIZACION")) {
								valorReq = ss.getPres03();
								ss.setCanman(0);
							} else
								valorReq = (ss.getCanman() * ss.getValuni());
							totalTotal -= valorReq;
							total -= valorReq;
							ss.setIndmod("D");
						} else
							valorReq = 0;
						canman = Util.validaParametro(req
								.getParameter("canman"), 0);
						pres03 = Util.validaParametro(req
								.getParameter("pres03"), 0);
						pres01 = Util.validaParametro(req
								.getParameter("pres01"), 0);
						fecha = Util.validaParametro(req
								.getParameter("fechaInicio"), "");
						if (!fecha.trim().equals(""))
							pres02 = Integer.parseInt(fecha.substring(6)
									+ fecha.substring(3, 5)
									+ fecha.substring(0, 2));

						if (pres03 > 0) {
							valorReq = pres03;
							// canman = 1;
						} else {
							valorReq = (canman * ss.getValuni());
						}
						total += valorReq;
						totalTotal = totalTotal + valorReq;

					} else
						canman = ss.getCanman();
					// if(canman > 0) {
					ss.setTippro("RQP");
					ss.setArea1(area1);
					ss.setArea2(area2);
					ss.setComen1(comen1);
					ss.setComen2(comen2);
					ss.setComen3(comen3);
					ss.setComen4(comen4);
					ss.setComen5(comen5);
					ss.setComen6(comen6);
					ss.setMotiv1(motiv1);
					ss.setMotiv2(motiv2);
					ss.setMotiv3(motiv3);
					ss.setMotiv4(motiv4);
					ss.setMotiv5(motiv5);
					ss.setMotiv6(motiv6);
					ss.setCanman(canman);
					ss.setPres01(pres01);
					ss.setPres02(pres02);
					ss.setPres03(pres03);
					listaRQP.add((Presw25DTO) ss);
					valpr = ss.getValpr1();
					desite = ss.getDesite();
					desuni = ss.getDesuni();
					// }

				} else
					listaRQP.add(ss);

			}

		}
		/* agrega en MAN */
		Presw25DTO presw25 = new Presw25DTO();
		presw25.setTippro("MAN");
		presw25.setCoduni(unidad);
		presw25.setItedoc(item);
		presw25.setAnopre(anno);
		presw25.setMespre(mes);
		presw25.setNumreq(numreq);
		presw25.setValpr1(valpr);
		presw25.setDesite(desite);
		presw25.setComen1(area1);
		presw25.setComen2(area2);
		presw25.setDesuni(desuni);
		presw25.setTotpre(total);
		if (total == 0)
			presw25.setIndmod("D");
		if (presupItemreq != null && presupItemreq.size() > 0) {
			i = -1;
			boolean agrega = false;
			for (Presw25DTO ss : presupItemreq) {
				i++;
				if (ss.getCoduni() == unidad && ss.getItedoc() == item
						&& ss.getMespre() == mes && ss.getNumreq() == numreq) {
					presupItemreq.set(i, presw25);
					agrega = true;
				}
			}
			if (!agrega)
				presupItemreq.add(presw25);
		} else
			presupItemreq.add(presw25);

		Long totalItemReq = new Long(0);
		Long ene = new Long(0);
		Long feb = new Long(0);
		Long mar = new Long(0);
		Long abr = new Long(0);
		Long may = new Long(0);
		Long jun = new Long(0);
		Long jul = new Long(0);
		Long ago = new Long(0);
		Long sep = new Long(0);
		Long oct = new Long(0);
		Long nov = new Long(0);
		Long dic = new Long(0);

		if (listarResultadoPresup.size() > 0) {
			for (Presw25DTO ss : presupItemreq) { // agregar todo lo que est�
													// en mantencion al presup
				if (ss.getItedoc() == item && ss.getCoduni() == unidad) {
					switch (ss.getMespre()) {
					case 1:
						ene = ene + ss.getTotpre();
						break;
					case 2:
						feb = feb + ss.getTotpre();
						break;
					case 3:
						mar = mar + ss.getTotpre();
						break;
					case 4:
						abr = abr + ss.getTotpre();
						break;
					case 5:
						may = may + ss.getTotpre();
						break;
					case 6:
						jun = jun + ss.getTotpre();
						break;
					case 7:
						jul = jul + ss.getTotpre();
						break;
					case 8:
						ago = ago + ss.getTotpre();
						break;
					case 9:
						sep = sep + ss.getTotpre();
						break;
					case 10:
						oct = oct + ss.getTotpre();
						break;
					case 11:
						nov = nov + ss.getTotpre();
						break;
					case 12:
						dic = dic + ss.getTotpre();
						break;
					}
					totalItemReq = totalItemReq + ss.getTotpre();
				}
			}

			i = -1;
			for (Presw25DTO ss : listarResultadoPresup) {
				i++;
				if (ss.getItedoc() == item && ss.getCoduni() == unidad) {
					ss.setItedoc(item);
					ss.setPres01(ene);
					ss.setPres02(feb);
					ss.setPres03(mar);
					ss.setPres04(abr);
					ss.setPres05(may);
					ss.setPres06(jun);
					ss.setPres07(jul);
					ss.setPres08(ago);
					ss.setPres09(sep);
					ss.setPres10(oct);
					ss.setPres11(nov);
					ss.setPres12(dic);
					ss.setTotpre(Long.parseLong(String.valueOf(ss.getPres01()
							+ ss.getPres02() + ss.getPres03() + ss.getPres04()
							+ ss.getPres05() + ss.getPres06() + ss.getPres07()
							+ ss.getPres08() + ss.getPres09() + ss.getPres10()
							+ ss.getPres11() + ss.getPres12())));
				}
				listaResultado.add(ss);
				// System.out.println("result: " + ss.getTotpre());
			}

		}
		/*
		 * System.out.println("tama�o presupItemreq: "+presupItemreq.size());
		 * for (Presw25DTO ss : presupItemreq){
		 * 
		 * System.out.println("ss "+ss.getMespre()+"cant " +
		 * ss.getPres01()+"req: "+ ss.getNumreq()); System.out.println("ss
		 * "+ss.getMespre()+"cant " + ss.getPres02()+"req: "+ ss.getNumreq());
		 * System.out.println("ss "+ss.getMespre()+"cant " +
		 * ss.getPres03()+"req: "+ ss.getNumreq());
		 *  } System.out.println("presupItemreq desp **:
		 * "+presupItemreq.size());
		 */
		if (presupItemreq != null)
			sesion.setAttribute("presupItemreq", presupItemreq);
		sesion.setAttribute("resultadoDetalleItemReq", listaRQP);

		sesion.setAttribute("resultadoPresup", listaResultado);
		// req.setAttribute("totalMes", totalTotal);

	}

	public void actualizaItemReqDet(HttpServletRequest req,
			List<Presw25DTO> lista, List<Presw25DTO> presupItemreq) {
		// resultadoDetalleItemReq, presupItemreq, listarResultadoPresup
		HttpSession sesion = req.getSession();
		List<Presw25DTO> listaRQP = new ArrayList();
		List<Presw25DTO> listaResultado = new ArrayList();

		String area1 = Util.validaParametro(req.getParameter("area1"), "");
		String area2 = Util.validaParametro(req.getParameter("area2"), "");
		String comen1 = Util.validaParametro(req.getParameter("comen1"), "");
		String comen2 = Util.validaParametro(req.getParameter("comen2"), "");
		String comen3 = Util.validaParametro(req.getParameter("comen3"), "");
		String comen4 = Util.validaParametro(req.getParameter("comen4"), "");
		String comen5 = Util.validaParametro(req.getParameter("comen5"), "");
		String comen6 = Util.validaParametro(req.getParameter("comen6"), "");
		String motiv1 = Util.validaParametro(req.getParameter("motiv1"), "");
		String motiv2 = Util.validaParametro(req.getParameter("motiv2"), "");
		String motiv3 = Util.validaParametro(req.getParameter("motiv3"), "");
		String motiv4 = Util.validaParametro(req.getParameter("motiv4"), "");
		String motiv5 = Util.validaParametro(req.getParameter("motiv5"), "");
		String motiv6 = Util.validaParametro(req.getParameter("motiv6"), "");
		int mes = Util.validaParametro(req.getParameter("mes"), 0);
		int numreq = Util.validaParametro(req.getParameter("sucur"), 0);
		int item = Util.validaParametro(req.getParameter("itedoc"), 0);
		int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		int anno = Util.validaParametro(req.getParameter("anno"), 0);

		if (lista.size() > 0) {
			for (Presw25DTO ss : lista) {

				if (ss.getNumreq() == numreq && ss.getMespre() == mes) {
					// System.out.println(ss.getItedoc());

					ss.setTippro("RQP");
					ss.setArea1(area1);
					ss.setArea2(area2);
					ss.setComen1(comen1);
					ss.setComen2(comen2);
					ss.setComen3(comen3);
					ss.setComen4(comen4);
					ss.setComen5(comen5);
					ss.setComen6(comen6);
					ss.setMotiv1(motiv1);
					ss.setMotiv2(motiv2);
					ss.setMotiv3(motiv3);
					ss.setMotiv4(motiv4);
					ss.setMotiv5(motiv5);
					ss.setMotiv6(motiv6);
					ss.setCanman(ss.getCanman());
					ss.setPres01(ss.getPres01());
					ss.setPres02(ss.getPres02());
					ss.setPres03(ss.getPres03());
					listaRQP.add((Presw25DTO) ss);

				} else
					listaRQP.add(ss);

			}

		}
		/* agrega en MAN */

		for (Presw25DTO ss : presupItemreq) {

			if (ss.getCoduni() == unidad && ss.getItedoc() == item
					&& ss.getMespre() == mes && ss.getNumreq() == numreq) {
				ss.setTippro("MAN");
				ss.setComen1(area1);
				ss.setComen2(area2);
				listaResultado.add((Presw25DTO) ss);
			} else
				listaResultado.add(ss);
		}

		if (presupItemreq != null)
			sesion.setAttribute("presupItemreq", listaResultado);
		sesion.setAttribute("resultadoDetalleItemReq", listaRQP);

	}

	public void agregaResultadoPresupOtrosIng(HttpServletRequest req,
			List<Presw25DTO> lista) {
		HttpSession sesion = req.getSession();
		List<Presw25DTO> listaINP = new ArrayList();
		String indexi = Util.validaParametro(req.getParameter("indexi"), "");

		if (lista.size() > 0) {

			int i = 0;
			/* para las observaciones se registra del comen2 al comen6 */
			for (Presw25DTO ss : lista) {
				i++;
				String valor = Util.validaParametro(req.getParameter("valuni"
						+ i), "0");
				valor = valor.replace(".", "");

				Long valuni = Long.parseLong(valor);
				String comen2 = Util.validaParametro(req.getParameter("comen1"
						+ i), "");
				String comen3 = Util.validaParametro(req.getParameter("comen2"
						+ i), "");
				String comen4 = Util.validaParametro(req.getParameter("comen3"
						+ i), "");
				String comen5 = Util.validaParametro(req.getParameter("comen4"
						+ i), "");
				String comen6 = Util.validaParametro(req.getParameter("comen5"
						+ i), "");
				/*
				 * se reciben del comen1 al comen5 para no cambiar la funcion
				 * que distribuye, al grabar lo hace bien
				 */

				Presw25DTO presw25 = ss;

				presw25.setTippro("INP");
				presw25.setTotpre(valuni);
				presw25.setComen2(comen2);
				presw25.setComen3(comen3);
				presw25.setComen4(comen4);
				presw25.setComen5(comen5);
				presw25.setComen6(comen6);
				presw25.setIndexi(indexi);
				listaINP.add((Presw25DTO) presw25);
			}
		}
		sesion.setAttribute("resultadoPresupIngresos", listaINP);
	}

	/* M.A. se agrego control de tope para las cuentas opercionales RESPUE: C */
	public long verificaTopeUnidad(HttpServletRequest req,
			List<Presw25DTO> lista) {
		HttpSession sesion = req.getSession();
		String ene = Util.validaParametro(req.getParameter("mes1"), "0");
		String feb = Util.validaParametro(req.getParameter("mes2"), "0");
		String mar = Util.validaParametro(req.getParameter("mes3"), "0");
		String abr = Util.validaParametro(req.getParameter("mes4"), "0");
		String may = Util.validaParametro(req.getParameter("mes5"), "0");
		String jun = Util.validaParametro(req.getParameter("mes6"), "0");
		String jul = Util.validaParametro(req.getParameter("mes7"), "0");
		String ago = Util.validaParametro(req.getParameter("mes8"), "0");
		String sep = Util.validaParametro(req.getParameter("mes9"), "0");
		String oct = Util.validaParametro(req.getParameter("mes10"), "0");
		String nov = Util.validaParametro(req.getParameter("mes11"), "0");
		String dic = Util.validaParametro(req.getParameter("mes12"), "0");
		ene = ene.replace(".", "");
		feb = feb.replace(".", "");
		mar = mar.replace(".", "");
		abr = abr.replace(".", "");
		may = may.replace(".", "");
		jun = jun.replace(".", "");
		jul = jul.replace(".", "");
		ago = ago.replace(".", "");
		sep = sep.replace(".", "");
		oct = oct.replace(".", "");
		nov = nov.replace(".", "");
		dic = dic.replace(".", "");

		Long mes1 = Long.parseLong(ene);
		Long mes2 = Long.parseLong(feb);
		Long mes3 = Long.parseLong(mar);
		Long mes4 = Long.parseLong(abr);
		Long mes5 = Long.parseLong(may);
		Long mes6 = Long.parseLong(jun);
		Long mes7 = Long.parseLong(jul);
		Long mes8 = Long.parseLong(ago);
		Long mes9 = Long.parseLong(sep);
		Long mes10 = Long.parseLong(oct);
		Long mes11 = Long.parseLong(nov);
		Long mes12 = Long.parseLong(dic);
		Long totpre = new Long(0);
		totpre = mes1 + mes2 + mes3 + mes4 + mes5 + mes6 + mes7 + mes8 + mes9
				+ mes10 + mes11 + mes12;

		// int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		/*
		 * MB 24/10/2012 debido a que suma 2 veces el mismo item es que lo
		 * excluye en la suma, valiendo lo actual
		 */
		// int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		/*
		 * 05/10/2016 MAA - Se cambian nombres de variables 'unidad(es)' por
		 * 'organizacion(es)',cuenta(s) en itedoc
		 */
		String organizacion = Util.validaParametro(req
				.getParameter("organizacion"), "");
		/*
		 * MB 24/10/2012 debido a que suma 2 veces el mismo item es que lo
		 * excluye en la suma, valiendo lo actual
		 */
		String itedoc = Util.validaParametro(req.getParameter("itedoc"), "");

		/*
		 * M.A. se agrego control de tope para las cuentas opercionales RESPUE:
		 * C
		 */
		long topeUnidad = 0;
		String codRespue = "";
		List<Presw25DTO> resultadoCPR = (List<Presw25DTO>) req.getSession()
				.getAttribute("resultadoCPR");
		if (resultadoCPR != null && resultadoCPR.size() > 0) {
			for (Presw25DTO ss : resultadoCPR) {
				// if(ss.getCoduni() == unidad) {
				if (ss.getComen4().trim().equals(organizacion.trim())) {
					topeUnidad = ss.getTotpre();
					codRespue = ss.getRespue();
					break;
				}

			}
		}

		Long sumaUnidad = new Long(0);
		List<Presw25DTO> resultadoPresup = (List<Presw25DTO>) req.getSession()
				.getAttribute("resultadoPresup");
		if (resultadoPresup != null && resultadoPresup.size() > 0) {
			for (Presw25DTO ss : resultadoPresup) {
				// if((ss.getCoduni() == unidad) && (itedoc != ss.getItedoc()))
				// {
				if ((ss.getMotiv1().trim().equals(organizacion.trim()))
						&& (!itedoc.trim().equals(ss.getMotiv2().trim()))) {
					sumaUnidad = sumaUnidad + ss.getTotpre();
				}

			}
			sumaUnidad = sumaUnidad + totpre;
		}

		if (/*
			 * codRespue.trim().equals("C") && esto se comentare� pues es en
			 * todos los casos
			 */sumaUnidad > topeUnidad) {
			return topeUnidad;
		} else
			return 0;

	}

	public void agregaItemActualiza(HttpServletRequest req,
			List<Presw25DTO> lista, int tipoLista) {
		/* MAA itedoc por motiv2 */
		HttpSession sesion = req.getSession();
		String ene = Util.validaParametro(req.getParameter("mes1"), "0");
		String feb = Util.validaParametro(req.getParameter("mes2"), "0");
		String mar = Util.validaParametro(req.getParameter("mes3"), "0");
		String abr = Util.validaParametro(req.getParameter("mes4"), "0");
		String may = Util.validaParametro(req.getParameter("mes5"), "0");
		String jun = Util.validaParametro(req.getParameter("mes6"), "0");
		String jul = Util.validaParametro(req.getParameter("mes7"), "0");
		String ago = Util.validaParametro(req.getParameter("mes8"), "0");
		String sep = Util.validaParametro(req.getParameter("mes9"), "0");
		String oct = Util.validaParametro(req.getParameter("mes10"), "0");
		String nov = Util.validaParametro(req.getParameter("mes11"), "0");
		String dic = Util.validaParametro(req.getParameter("mes12"), "0");
		ene = ene.replace(".", "");
		feb = feb.replace(".", "");
		mar = mar.replace(".", "");
		abr = abr.replace(".", "");
		may = may.replace(".", "");
		jun = jun.replace(".", "");
		jul = jul.replace(".", "");
		ago = ago.replace(".", "");
		sep = sep.replace(".", "");
		oct = oct.replace(".", "");
		nov = nov.replace(".", "");
		dic = dic.replace(".", "");

		Long mes1 = Long.parseLong(ene);
		Long mes2 = Long.parseLong(feb);
		Long mes3 = Long.parseLong(mar);
		Long mes4 = Long.parseLong(abr);
		Long mes5 = Long.parseLong(may);
		Long mes6 = Long.parseLong(jun);
		Long mes7 = Long.parseLong(jul);
		Long mes8 = Long.parseLong(ago);
		Long mes9 = Long.parseLong(sep);
		Long mes10 = Long.parseLong(oct);
		Long mes11 = Long.parseLong(nov);
		Long mes12 = Long.parseLong(dic);
		Long totpre = new Long(0);
		totpre = mes1 + mes2 + mes3 + mes4 + mes5 + mes6 + mes7 + mes8 + mes9
				+ mes10 + mes11 + mes12;

		// 05/10/2016 MAA - Se cambian nombres de variables 'unidad(es)' por
		// 'organizacion(es)',cuenta(s) itedoc por string
		// int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		// int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		String organizacion = Util.validaParametro(req
				.getParameter("organizacion"), "");
		String itedoc = Util.validaParametro(req.getParameter("itedoc"), "");

		List<Presw25DTO> listarResultado = new ArrayList<Presw25DTO>();
		String comen1 = Util.validaParametro(req.getParameter("comen1"), "");
		String comen2 = Util.validaParametro(req.getParameter("comen2"), "");
		String comen3 = Util.validaParametro(req.getParameter("comen3"), "");
		String comen4 = Util.validaParametro(req.getParameter("comen4"), "");
		String comen5 = Util.validaParametro(req.getParameter("comen5"), "");
		String comen6 = Util.validaParametro(req.getParameter("comen6"), "");
		for (Presw25DTO ss : lista) {
			// System.out.println(ss.getItedoc());
			// if(ss.getCoduni() == unidad && ss.getItedoc() == itedoc){
			if ((ss.getMotiv1().trim().equals(organizacion.trim()))
					&& (itedoc.trim().equals(ss.getMotiv2().trim()))) {
				// System.out.println(ss.getItedoc());
				ss.setPres01(mes1);
				ss.setPres02(mes2);
				ss.setPres03(mes3);
				ss.setPres04(mes4);
				ss.setPres05(mes5);
				ss.setPres06(mes6);
				ss.setPres07(mes7);
				ss.setPres08(mes8);
				ss.setPres09(mes9);
				ss.setPres10(mes10);
				ss.setPres11(mes11);
				ss.setPres12(mes12);
				ss.setTotpre(totpre);
				ss.setComen1(comen1);
				ss.setComen2(comen2);
				ss.setComen3(comen3);
				ss.setComen4(comen4);
				ss.setComen5(comen5);
				ss.setComen6(comen6);
				// MAA
				ss.setMotiv2(itedoc.trim());
				ss.setMotiv1(organizacion.trim());

			}
			listarResultado.add(ss);
		}
		if(tipoLista == 1) // especial
			sesion.setAttribute("resultadoPresupEsp", listarResultado);
		else
			sesion.setAttribute("resultadoPresupEsp", listarResultado);
	}

	public void eliminaItem(HttpServletRequest req, List<Presw25DTO> lista, int tipoLista) {
		/* MAA itedoc por motiv2 */
		HttpSession sesion = req.getSession();
		String organizacion = Util.validaParametro(req.getParameter("organizacion"), "");
		String itedoc = Util.validaParametro(req.getParameter("item"), "");
		List<Presw25DTO> listarResultado = new ArrayList<Presw25DTO>();

		for (Presw25DTO ss : lista) {
			// if(ss.getCoduni() == unidad && ss.getItedoc() == itedoc){
			if (ss.getMotiv1().trim().equals(organizacion.trim())
					&& ss.getMotiv2().trim().equals(itedoc)) {
				ss.setPres01(0);
				ss.setPres02(0);
				ss.setPres03(0);
				ss.setPres04(0);
				ss.setPres05(0);
				ss.setPres06(0);
				ss.setPres07(0);
				ss.setPres08(0);
				ss.setPres09(0);
				ss.setPres10(0);
				ss.setPres11(0);
				ss.setPres12(0);
				ss.setTotpre(0);
				ss.setComen1("");
				ss.setTotpre(0);
				// MAA
				ss.setMotiv2(itedoc);
				ss.setMotiv1(organizacion);

			}
			listarResultado.add(ss);
		}
        if(tipoLista == 2) // especial
        	sesion.setAttribute("resultadoPresupEsp", listarResultado);
        else
            sesion.setAttribute("resultadoPresup", listarResultado);
	}

	public void eliminaReq(HttpServletRequest req, List<Presw25DTO> lista,
			List<Presw25DTO> listaDetalle) {
		HttpSession sesion = req.getSession();
		int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		int mes = Util.validaParametro(req.getParameter("mes"), 0);
		int sucur = Util.validaParametro(req.getParameter("sucur"), 0);
		List<Presw25DTO> listarResultado = new ArrayList<Presw25DTO>();
		List<Presw25DTO> listarResultadoDet = new ArrayList<Presw25DTO>();
		for (Presw25DTO ss : lista) {
			if (ss.getCoduni() == unidad && ss.getItedoc() == itedoc
					&& ss.getMespre() == mes && ss.getNumreq() == sucur) {

				ss.setCanman(0);
				ss.setIndmod("D");
				ss.setTotpre(0);

			}
			listarResultado.add(ss);
		}
		if (listaDetalle != null && listaDetalle.size() > 0) {
			for (Presw25DTO ss : listaDetalle) {
				if (ss.getCoduni() == unidad && ss.getItedoc() == itedoc
						&& ss.getMespre() == mes && ss.getNumreq() == sucur) {
					ss.setTippro("RQP");
					ss.setIndmod("D");
					ss.setCanman(0);
					ss.setTotpre(0);

				}
				listarResultadoDet.add(ss);
			}
		}
		sesion.setAttribute("presupItemreq", listarResultado);
		sesion.setAttribute("resultadoDetalleItemReq", listarResultadoDet);
	}

	/*
	 * public boolean saveMatriz(List<Presw25DTO> lista, int unidad, int a�o,
	 * int rutUsuario){ PreswBean preswbean = new PreswBean("PRP",
	 * unidad,0,a�o,0, rutUsuario);
	 * 
	 * return preswbean.ingreso_presw25((List<Presw25DTO> )lista); }
	 */
	// MAA se cambia a organizacion
	public boolean saveMatriz(List<Presw25DTO> lista, String organizacion,
			int a�o, int rutUsuario, String tipo) {
		PreswBean preswbean = new PreswBean(tipo, 0, 0, a�o, 0, rutUsuario);
		return preswbean.ingreso_presw25((List<Presw25DTO>) lista);
	}

	// public boolean saveREQ(List<Presw25DTO> lista, int unidad, int a�o, int
	// rutUsuario, int mes){
	public boolean saveREQ(List<Presw25DTO> lista, String organizacion,
			int a�o, int rutUsuario, int mes) {
		boolean retorno = false;
		List<Presw25DTO> listaRQP = new ArrayList();
		// int item = 0;
		String item = "0";
		int numreq = 0;
		int mesAnt = 1;
		if (lista != null && lista.size() > 0) {
			for (Presw25DTO ss : lista) {
				if (numreq != ss.getNumreq() /* && mesAnt == ss.getMespre() */) {
					ss.setIndexi("I");
					/*
					 * if(ss.getCanman() == 0) ss.setIndmod("D");
					 */
				}
				numreq = ss.getNumreq();
				// item = ss.getItedoc();
				item = ss.getComen4();
				mesAnt = ss.getMespre();
				if (ss.getValpr1() == 1) {

					ss.setNumreq(0);

				}
				listaRQP.add(ss);
				// System.out.println(ss.getIndexi()+ " ind : "+
				// ss.getIndmod()+" ss.getMespre(): " + ss.getMespre()+"
				// ss.getCanman(): "+ss.getCanman()+" numreq: "+numreq+ "
				// ss.getNumreq(): "+ss.getNumreq());
			}
		}
		// PreswBean preswbean = new PreswBean("RQP", unidad,item,a�o,mes,
		// rutUsuario);
		PreswBean preswbean = new PreswBean("RQP", 0, 0, a�o, mes, rutUsuario);
		retorno = preswbean.ingreso_presw25((List<Presw25DTO>) listaRQP);

		return retorno;
	}

	public boolean saveDetalleREQ(List<Presw25DTO> lista, int rutUsuario) {
		boolean retorno = false;

		for (Presw25DTO ss : lista) {
			PreswBean preswbean = new PreswBean("REQ", ss.getCoduni(), ss
					.getItedoc(), ss.getAnopre(), ss.getMespre(), rutUsuario);
			preswbean.setSucur(ss.getNumreq());
			retorno = preswbean.ingreso_presw25((List<Presw25DTO>) lista);
			if (!retorno)
				break;
		}
		return retorno;
	}

	/*
	 * se modifica con banner public boolean savePresupNomina(List<Presw25DTO>
	 * lista, int unidad, int a�o, int rutUsuario){ PreswBean preswbean = new
	 * PreswBean("COP", unidad,0,a�o,0, rutUsuario);
	 * 
	 * return preswbean.ingreso_presw25((List<Presw25DTO> )lista); }
	 */
	public boolean savePresupNomina(List<Presw25DTO> lista,
			String organizacion, int a�o, int rutUsuario) {
		PreswBean preswbean = new PreswBean("COP", 0, 0, a�o, 0, rutUsuario);

		return preswbean.ingreso_presw25((List<Presw25DTO>) lista);
	}

	/*
	 * ahora se elimin� ingreso a las vacantes 24/09/2010 public boolean
	 * savePresupVacante(List<Presw25DTO> lista, int unidad, int a�o, int
	 * rutUsuario){ PreswBean preswbean = new PreswBean("GNV", unidad,0,a�o,0,
	 * rutUsuario);
	 * 
	 * return preswbean.ingreso_presw25((List<Presw25DTO> )lista); }
	 */
	/*
	 * public boolean savePresupRemuneracion(List<Presw25DTO> lista, int
	 * unidad, int a�o, int rutUsuario){ PreswBean preswbean = new
	 * PreswBean("ASP", unidad,0,a�o,0, rutUsuario);
	 * 
	 * return preswbean.ingreso_presw25((List<Presw25DTO> )lista); }
	 */
	public boolean savePresupRemuneracion(List<Presw25DTO> lista,
			String organizacion, int a�o, int rutUsuario) {
		PreswBean preswbean = new PreswBean("ASP", 0, 0, a�o, 0, rutUsuario);
		return preswbean.ingreso_presw25((List<Presw25DTO>) lista);
	}

	public boolean savePresupOtros(List<Presw25DTO> lista, int a�o,
			int rutUsuario) {
		PreswBean preswbean = new PreswBean("INP", 0, 0, a�o, 0, rutUsuario);

		return preswbean.ingreso_presw25((List<Presw25DTO>) lista);
	}

	public boolean saveIndicadores(String tipo, List<Presw25DTO> lista,
			String dv, int rutUsuario) {
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(dv);
		// System.out.println("saveIndicadores nomTipo: "+tipo);
		return preswbean.ingreso_presw25((List<Presw25DTO>) lista);
	}

	public boolean saveNotificacion(String tipo, List<Presw25DTO> lista,
			String dv, int rutUsuario, String tipoNotificacion, int coduni) {
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setTipcue(tipoNotificacion);
		preswbean.setCoduni(coduni);
		preswbean.setDigide(dv);

		return preswbean.ingreso_presw25((List<Presw25DTO>) lista);
	}

	public boolean saveValorIndicadores(String tipo, List<Presw25DTO> lista,
			String dv, int rutUsuario) {
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(dv);

		return preswbean.ingreso_presw25((List<Presw25DTO>) lista);
	}

	public Vector getlistaReqResumenExcel(HttpServletRequest req) {
		// MAA se cambia unidad por organizacion y itedoc a String
		// int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		String itedoc = Util.validaParametro(req.getParameter("itedoc"), "");
		int a�o = Util.validaParametro(req.getParameter("anno"), 0);
		// int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		String organizacion = Util.validaParametro(req
				.getParameter("organizacion"), "");
		int rutUsuario = Integer.parseInt(req.getSession().getAttribute(
				"rutUsuario")
				+ "");

		Vector vec_datos = new Vector();
		Vector vec_d = new Vector();
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		// String nomUnidad = "";
		String nomOrganizacion = "";
		long total = 0;
		long totalTotal = 0;
		long enero = 0;
		long febrero = 0;
		long marzo = 0;
		long abril = 0;
		long mayo = 0;
		long junio = 0;
		long julio = 0;
		long agosto = 0;
		long septiembre = 0;
		long octubre = 0;
		long noviembre = 0;
		long diciembre = 0;
		// PreswBean preswbean = new PreswBean("PRE", unidad,0,a�o,0,
		// rutUsuario);
		PreswBean preswbean = new PreswBean("PRE", 0, 0, a�o, 0, rutUsuario);
		preswbean.setCajero(organizacion);
		NumberFormat nf2 = NumberFormat.getInstance(Locale.GERMAN);

		if (preswbean != null) {
			listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
		}

		long totalAgrup = 0;
		Vector agrup = null;
		Vector agrup1 = new Vector();
		Vector agrup2 = new Vector();
		Vector agrup3 = new Vector();
		Vector agrup4 = new Vector();
		long total_ENE = 0;
		long total_FEB = 0;
		long total_MAR = 0;
		long total_ABR = 0;
		long total_MAY = 0;
		long total_JUN = 0;
		long total_JUL = 0;
		long total_AGO = 0;
		long total_SEP = 0;
		long total_OCT = 0;
		long total_NOV = 0;
		long total_DIC = 0;
		long agrup_ENE = 0;
		long agrup_FEB = 0;
		long agrup_MAR = 0;
		long agrup_ABR = 0;
		long agrup_MAY = 0;
		long agrup_JUN = 0;
		long agrup_JUL = 0;
		long agrup_AGO = 0;
		long agrup_SEP = 0;
		long agrup_OCT = 0;
		long agrup_NOV = 0;
		long agrup_DIC = 0;
		for (Presw25DTO ss : listaPresw25) {
			total = ss.getPres01() + ss.getPres02() + ss.getPres03()
					+ ss.getPres04() + ss.getPres05() + ss.getPres06()
					+ ss.getPres07() + ss.getPres08() + ss.getPres09()
					+ ss.getPres10() + ss.getPres11() + ss.getPres12();
			total_ENE = ss.getPres01();
			total_FEB = ss.getPres02();
			total_MAR = ss.getPres03();
			total_ABR = ss.getPres04();
			total_MAY = ss.getPres05();
			total_JUN = ss.getPres06();
			total_JUL = ss.getPres07();
			total_AGO = ss.getPres08();
			total_SEP = ss.getPres09();
			total_OCT = ss.getPres10();
			total_NOV = ss.getPres11();
			total_DIC = ss.getPres12();
			if (!ss.getMotiv2().trim().equals(ss.getMotiv3().trim())) {
				totalAgrup = totalAgrup + total;
				agrup_ENE = agrup_ENE + total_ENE;
				agrup_FEB = agrup_FEB + total_FEB;
				agrup_MAR = agrup_MAR + total_MAR;
				agrup_ABR = agrup_ABR + total_ABR;
				agrup_MAY = agrup_MAY + total_MAY;
				agrup_JUN = agrup_JUN + total_JUN;
				agrup_JUL = agrup_JUL + total_JUL;
				agrup_AGO = agrup_AGO + total_AGO;
				agrup_SEP = agrup_SEP + total_SEP;
				agrup_OCT = agrup_OCT + total_OCT;
				agrup_NOV = agrup_NOV + total_NOV;
				agrup_DIC = agrup_DIC + total_DIC;
			} else {
				agrup = new Vector();
				agrup.addElement(totalAgrup);
				agrup.addElement(agrup_ENE);
				agrup.addElement(agrup_FEB);
				agrup.addElement(agrup_MAR);
				agrup.addElement(agrup_ABR);
				agrup.addElement(agrup_MAY);
				agrup.addElement(agrup_JUN);
				agrup.addElement(agrup_JUL);
				agrup.addElement(agrup_AGO);
				agrup.addElement(agrup_SEP);
				agrup.addElement(agrup_OCT);
				agrup.addElement(agrup_NOV);
				agrup.addElement(agrup_DIC);
				if (ss.getMotiv2().trim().equals("2"))
					agrup1 = agrup;
				else if (ss.getMotiv2().trim().equals("3"))
					agrup2 = agrup;
				else if (ss.getMotiv2().trim().equals("4"))
					agrup3 = agrup;

				totalAgrup = 0;
				agrup_ENE = 0;
				agrup_FEB = 0;
				agrup_MAR = 0;
				agrup_ABR = 0;
				agrup_MAY = 0;
				agrup_JUN = 0;
				agrup_JUL = 0;
				agrup_AGO = 0;
				agrup_SEP = 0;
				agrup_OCT = 0;
				agrup_NOV = 0;
				agrup_DIC = 0;

			}
		}
		agrup = new Vector();
		agrup.addElement(totalAgrup);
		agrup.addElement(agrup_ENE);
		agrup.addElement(agrup_FEB);
		agrup.addElement(agrup_MAR);
		agrup.addElement(agrup_ABR);
		agrup.addElement(agrup_MAY);
		agrup.addElement(agrup_JUN);
		agrup.addElement(agrup_JUL);
		agrup.addElement(agrup_AGO);
		agrup.addElement(agrup_SEP);
		agrup.addElement(agrup_OCT);
		agrup.addElement(agrup_NOV);
		agrup.addElement(agrup_DIC);
		agrup4 = agrup;

		// System.out.println("vecAgrup "+agrup1);
		for (Presw25DTO ss : listaPresw25) {

			total = ss.getPres01() + ss.getPres02() + ss.getPres03()
					+ ss.getPres04() + ss.getPres05() + ss.getPres06()
					+ ss.getPres07() + ss.getPres08() + ss.getPres09()
					+ ss.getPres10() + ss.getPres11() + ss.getPres12();
			// if(total > 0){
			if (total > 0
					|| ss.getMotiv2().trim().equals(ss.getMotiv3().trim())) {
				if (!ss.getMotiv2().trim().equals(ss.getMotiv3().trim()))
					// vec_datos.add(String.valueOf(ss.getItedoc()));
					vec_datos.add(ss.getMotiv2());
				else
					vec_datos.add("");
				vec_datos.add(ss.getDesite());

				agrup = new Vector();
				if (ss.getMotiv2().trim().equals(ss.getMotiv3().trim())) {
					if (ss.getMotiv2().trim().equals("1"))
						agrup = agrup1;
					else if (ss.getMotiv2().trim().equals("2"))
						agrup = agrup2;
					else if (ss.getMotiv2().trim().equals("3"))
						agrup = agrup3;
					else if (ss.getMotiv2().trim().equals("4"))
						agrup = agrup4;
                    if(agrup.size() > 0){
					vec_datos.add(nf2.format(agrup.get(0)));
					vec_datos.add(nf2.format(agrup.get(1)));
					vec_datos.add(nf2.format(agrup.get(2)));
					vec_datos.add(nf2.format(agrup.get(3)));
					vec_datos.add(nf2.format(agrup.get(4)));
					vec_datos.add(nf2.format(agrup.get(5)));
					vec_datos.add(nf2.format(agrup.get(6)));
					vec_datos.add(nf2.format(agrup.get(7)));
					vec_datos.add(nf2.format(agrup.get(8)));
					vec_datos.add(nf2.format(agrup.get(9)));
					vec_datos.add(nf2.format(agrup.get(10)));
					vec_datos.add(nf2.format(agrup.get(11)));
					vec_datos.add(nf2.format(agrup.get(12)));
					vec_datos.add("");
                    }
				} else {
					vec_datos.add(nf2.format(total));
					vec_datos.add(nf2.format(ss.getPres01()));
					vec_datos.add(nf2.format(ss.getPres02()));
					vec_datos.add(nf2.format(ss.getPres03()));
					vec_datos.add(nf2.format(ss.getPres04()));
					vec_datos.add(nf2.format(ss.getPres05()));
					vec_datos.add(nf2.format(ss.getPres06()));
					vec_datos.add(nf2.format(ss.getPres07()));
					vec_datos.add(nf2.format(ss.getPres08()));
					vec_datos.add(nf2.format(ss.getPres09()));
					vec_datos.add(nf2.format(ss.getPres10()));
					vec_datos.add(nf2.format(ss.getPres11()));
					vec_datos.add(nf2.format(ss.getPres12()));
					vec_datos.add(ss.getComen1().toString().trim()
							+ ss.getComen2().toString().trim()
							+ ss.getComen3().toString().trim()
							+ ss.getComen4().toString().trim()
							+ ss.getComen5().toString().trim()
							+ ss.getComen6().toString().trim());
				}
				// nomUnidad = ss.getCoduni() + " - " + ss.getDesuni().trim();
				nomOrganizacion = ss.getMotiv1().trim() + " - "
						+ ss.getDesuni().trim();
				enero += ss.getPres01();
				febrero += ss.getPres02();
				marzo += ss.getPres03();
				abril += ss.getPres04();
				mayo += ss.getPres05();
				junio += ss.getPres06();
				julio += ss.getPres07();
				agosto += ss.getPres08();
				septiembre += ss.getPres09();
				octubre += ss.getPres10();
				noviembre += ss.getPres11();
				diciembre += ss.getPres12();
				totalTotal += total;
			}
		}
		vec_datos.add("");
		vec_datos.add("Total $");
		vec_datos.add(nf2.format(totalTotal));
		vec_datos.add(nf2.format(enero));
		vec_datos.add(nf2.format(febrero));
		vec_datos.add(nf2.format(marzo));
		vec_datos.add(nf2.format(abril));
		vec_datos.add(nf2.format(mayo));
		vec_datos.add(nf2.format(junio));
		vec_datos.add(nf2.format(julio));
		vec_datos.add(nf2.format(agosto));
		vec_datos.add(nf2.format(septiembre));
		vec_datos.add(nf2.format(octubre));
		vec_datos.add(nf2.format(noviembre));
		vec_datos.add(nf2.format(diciembre));
		// vec_datos.add(nomUnidad);
		vec_datos.add(nomOrganizacion);
		return vec_datos;
	}

	public void getlistaPresupItemreq(HttpServletRequest req) {
		int a�o = Util.validaParametro(req.getParameter("anno"), 0);
		int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
		int rutUsuario = Integer.parseInt(req.getSession().getAttribute(
				"rutUsuario")
				+ "");
		HttpSession sesion = req.getSession();

		/* agrega requerimiento */
		String nomItem = "";
		String nomMes = "";
		String nomUnidad = "";
		int itedoc = 41;
		Vector vecTotales = new Vector();
		Vector vec_d = new Vector();
		List<Presw25DTO> presupItemreq = new ArrayList<Presw25DTO>();
		List<Presw25DTO> presupItemreqDet = new ArrayList<Presw25DTO>();

		PreswBean preswbean = new PreswBean("MAN", unidad, itedoc, a�o, 0,
				rutUsuario);
		if (preswbean != null) {
			presupItemreq = (List<Presw25DTO>) preswbean.consulta_presw25();
		}

		Long en = new Long(0);
		Long fe = new Long(0);
		Long ma = new Long(0);
		Long ab = new Long(0);
		Long my = new Long(0);
		Long jn = new Long(0);
		Long jl = new Long(0);
		Long ag = new Long(0);
		Long se = new Long(0);
		Long oc = new Long(0);
		Long no = new Long(0);
		Long di = new Long(0);
		Vector listaReqResumen = new Vector();
		Vector listaReqDetResumen = new Vector();

		if (presupItemreq != null && presupItemreq.size() > 0) {
			Vector meses = new Vector();
			int numMes = 1;
			// while (numMes < 13) {
			for (Presw25DTO ss : presupItemreq) {
				Presw25DTO presw25 = ss;

				if (presw25.getCoduni() == unidad
						&& presw25.getItedoc() == itedoc
						&& presw25.getAnopre() == a�o && ss.getTotpre() > 0
				/* && numMes == ss.getMespre() */) {
					nomItem = ss.getItedoc() + " - " + ss.getDesite();
					nomUnidad = ss.getCoduni() + " - " + ss.getDesuni();
					switch (ss.getMespre()) {
					case 1: {
						en = en + presw25.getTotpre();
						nomMes = "Enero";
						break;
					}
					case 2: {
						fe = fe + presw25.getTotpre();
						nomMes = "Febrero";
						break;
					}

					case 3: {
						ma = ma + presw25.getTotpre();
						nomMes = "Marzo";
						break;
					}

					case 4: {
						ab = ab + presw25.getTotpre();
						nomMes = "Abril";
						break;
					}

					case 5: {
						my = my + presw25.getTotpre();
						nomMes = "Mayo";
						break;
					}

					case 6: {
						jn = jn + presw25.getTotpre();
						nomMes = "Junio";
						break;
					}

					case 7: {
						jl = jl + presw25.getTotpre();
						nomMes = "Julio";
						break;
					}

					case 8: {
						ag = ag + presw25.getTotpre();
						nomMes = "Agosto";
						break;
					}

					case 9: {
						se = se + presw25.getTotpre();
						nomMes = "Septiembre";
						break;
					}

					case 10: {
						oc = oc + presw25.getTotpre();
						nomMes = "Octubre";
						break;
					}

					case 11: {
						no = no + presw25.getTotpre();
						nomMes = "Noviembre";
						break;
					}

					case 12: {
						di = di + presw25.getTotpre();
						nomMes = "Diciembre";
						break;
					}

					}

					vec_d = new Vector();
					vec_d.addElement(ss.getMespre());
					vec_d.addElement(nomMes);
					vec_d.addElement(ss.getComen1().trim());
					vec_d.addElement(ss.getComen2().trim());
					vec_d.addElement(ss.getTotpre());
					switch (ss.getMespre()) {
					case 1:
						vec_d.addElement(en);
						break;
					case 2:
						vec_d.addElement(fe);
						break;
					case 3:
						vec_d.addElement(ma);
						break;
					case 4:
						vec_d.addElement(ab);
						break;
					case 5:
						vec_d.addElement(my);
						break;
					case 6:
						vec_d.addElement(jn);
						break;
					case 7:
						vec_d.addElement(jl);
						break;
					case 8:
						vec_d.addElement(ag);
						break;
					case 9:
						vec_d.addElement(se);
						break;
					case 10:
						vec_d.addElement(oc);
						break;
					case 11:
						vec_d.addElement(no);
						break;
					case 12:
						vec_d.addElement(di);
						break;
					}
					listaReqResumen.addElement(vec_d);

					preswbean = new PreswBean("REQ", unidad, itedoc, a�o, ss
							.getMespre(), rutUsuario);
					if (preswbean != null) {
						preswbean.setSucur(ss.getNumreq());
						presupItemreqDet = new ArrayList<Presw25DTO>();
						presupItemreqDet = (List<Presw25DTO>) preswbean
								.consulta_presw25();
					}
					// }

					// }

					// numMes++;
					// }
					// }
					nomMes = "";
					if (presupItemreqDet.size() > 0) {
						for (Presw25DTO pi : presupItemreqDet) {
							switch (pi.getMespre()) {
							case 1:
								nomMes = "Enero";
								break;
							case 2:
								nomMes = "Febrero";
								break;
							case 3:
								nomMes = "Marzo";
								break;
							case 4:
								nomMes = "Abril";
								break;
							case 5:
								nomMes = "Mayo";
								break;
							case 6:
								nomMes = "Junio";
								break;
							case 7:
								nomMes = "Julio";
								break;
							case 8:
								nomMes = "Agosto";
								break;
							case 9:
								nomMes = "Septiembre";
								break;
							case 10:
								nomMes = "Octubre";
								break;
							case 11:
								nomMes = "Noviembre";
								break;
							case 12:
								nomMes = "Diciembre";
								break;
							}
							vec_d = new Vector();
							vec_d.addElement(pi.getCoduni() + " - "
									+ pi.getDesuni()); // 0 UNIDAD
							vec_d.addElement(pi.getItedoc() + " - "
									+ pi.getDesite()); // 1 - ITEM
							vec_d.addElement(nomMes.concat(" - N� "
									+ pi.getNumreq() + ""));// 3 - requerimiento
							vec_d.addElement(pi.getArea1().trim()
									+ pi.getArea2().trim());// 2 - AREA
							vec_d.addElement(pi.getComen1().trim()
									+ pi.getComen2().trim()
									+ pi.getComen3().trim()
									+ pi.getComen4().trim()
									+ pi.getComen5().trim()
									+ pi.getComen6().trim()); // 4 -
																// DESCRIPCION
							vec_d.addElement(pi.getMotiv1().trim()
									+ pi.getMotiv2().trim()
									+ pi.getMotiv3().trim()
									+ pi.getMotiv4().trim()
									+ pi.getMotiv5().trim()
									+ pi.getMotiv6().trim()); // 5 - MOTIVO
							vec_d.addElement(pi.getCodman()); // 6 - ITEM
							vec_d.addElement(pi.getTipcon()); // 7 - DETALLE
							if (pi.getCodman().trim().equals("COTIZACION")
									&& pi.getPres03() > 0) {
								String fecha = pi.getPres02() + "";
								vec_d.addElement(pi.getPres01()); // 9 -
																	// cotizacion
								vec_d.addElement(pi.getPres03()); // 10 -
																	// monto
								vec_d.addElement(fecha.substring(7) + "/"
										+ fecha.substring(4, 6) + "/"
										+ fecha.substring(0, 4)); // 10 -
																	// fecha
							} else {
								vec_d.addElement(pi.getCodigo()); // 8 -
																	// UNIDAD
								vec_d.addElement(pi.getValuni()); // 9 - COSTO
								vec_d.addElement(pi.getCanman()); // 10 -
																	// CANTIDAD
							}
							vec_d.addElement(pi.getNumreq()); // 11 - NUMREQ
							vec_d.addElement(pi.getMespre()); // 12 - MES
							if (pi.getCanman() > 0 || pi.getPres03() > 0)
								listaReqDetResumen.addElement(vec_d);
						}
					}
				}
			}
		}
		Long totalAnual = en + fe + ma + ab + my + jn + jl + ag + se + oc + no
				+ di;
		vec_d = new Vector();
		vec_d.addElement(13);
		vec_d.addElement("Total Anual");
		vec_d.addElement("");
		vec_d.addElement("");
		vec_d.addElement("");
		vec_d.addElement(totalAnual);
		if (totalAnual > 0)
			listaReqResumen.addElement(vec_d);
		/* fin requerimiento */
		// System.out.println(listaReqDetResumen);
		sesion.setAttribute("listaReqResumen", listaReqResumen);
		sesion.setAttribute("listaReqDetResumen", listaReqDetResumen);

	}

	public void agregaCorrientePDF(HttpServletRequest req,
			Vector<String> lista, Vector<String> lista2, Vector<String> lista3) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listaCorrientePDF", lista);
		sesion.setAttribute("listaReqResumenPDF", lista2);
		sesion.setAttribute("listaReqDetResumenPDF", lista3);
	}

	public void agregaCorrienteExcel(HttpServletRequest req,
			Vector<String> lista, Vector<String> lista2, Vector<String> lista3) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listaCorrienteExcel", lista);
		sesion.setAttribute("listaReqResumenExcel", lista2);
		sesion.setAttribute("listaReqDetResumenExcel", lista3);

	}

	/*
	 * public void agregaAccionweb(HttpServletRequest req, String opcion, int
	 * valor){ req.setAttribute(opcion, valor); }
	 */
	public void agregaResumenAnualExcel(HttpServletRequest req,
			Vector<String> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listaResumenAnualExcel", lista);
	}
	public void agregaResumenAnualExceltotales(HttpServletRequest req,
			long gastos, long presu) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("gastos", gastos);
		sesion.setAttribute("presupuestos", presu);
	}

	public void agregaDisponibilidadExcel(HttpServletRequest req,
			Vector<String> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listaDisponibilidadExcel", lista);
	}

	public void agregaInformeExcel(HttpServletRequest req, Vector<String> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("consultaDocExcel", lista);
	}

	public void agregaInformePDF(HttpServletRequest req, Vector<String> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("consultaDocPDF", lista);
	}

	public void agregaOtrosIngPDF(HttpServletRequest req, Vector<String> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listaOtrosIngPDF", lista);

	}

	public void agregaRemuneracionPDF(HttpServletRequest req,
			Vector<String> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listaRemuneracionPDF", lista);

	}

	public void agregaNominaPDF(HttpServletRequest req, Vector<String> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listaNominaPDF", lista);
	}

	public boolean getPeriodoProcesos() throws Exception {
		return presupuestoDaoB.getPeriodoProcesos();
	}

	public void agregaMenuControl(HttpServletRequest req, String menu) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("menu", menu);
	}

	public void agregaEjecucionRemuneracionExportar(HttpServletRequest req,
			Vector<String> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("ejecucionRemuneracionExportar", lista);
	}

	public void agregaEjecucionMensualExportar(HttpServletRequest req,
			Vector<String> lista1, Vector<String> lista2, Vector<String> lista3) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("ejecucionMensualExportarET1", lista1);
		sesion.setAttribute("ejecucionMensualExportarET2", lista2);
		sesion.setAttribute("ejecucionMensualExportarET3", lista3);
	}

	
	public void agregaCuentaExportar(HttpServletRequest req , Vector<String> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("ejecucionMensualExportarET1", lista);
		//sesion.setAttribute("ejecucionMensualExportarET2", lista2);
		//sesion.setAttribute("ejecucionMensualExportarET3", lista3);
	}
	
	public void agregaEjecucionExcel(HttpServletRequest req,
			Vector<String> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("informeEjecucionExcel", lista);
	}

	public void agregaMovDisponibleExcel(HttpServletRequest req,
			Vector<String> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("informeMovDisponibleExcel", lista);

	}

	public void agregaMovEjecucionExcel(HttpServletRequest req,
			Vector<String> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("informeMovEjecucionExcel", lista);

	}

	public void agregaListado(HttpServletRequest req, List<Presw25DTO> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listado", lista);

	}

	public List<Cocof17DTO> getListaSedeAlterada(AccionWeb accionweb)
			throws Exception {
		CocofBean cocofBean = new CocofBean();
		List<Cocof17DTO> listCocofBean = new ArrayList<Cocof17DTO>();
		listCocofBean = cocofBean.buscar_todos_cocof17();
		/*
		 * Pedro Peralta Santiba�ez: las sedes deben salir 1 2 3 7 4 y 6 central
		 * vi�a conce sanjoaco vitacura y aca
		 */
		List<Cocof17DTO> lista = new ArrayList<Cocof17DTO>();
		Cocof17DTO cocof17DTO = new Cocof17DTO();
		for (Cocof17DTO ss : listCocofBean) {
			if (ss.getCodsuc().toString().trim().equals("1")
					|| ss.getCodsuc().toString().trim().equals("2")
					|| ss.getCodsuc().toString().trim().equals("3")
					|| ss.getCodsuc().toString().trim().equals("7")) {
				cocof17DTO = new Cocof17DTO();
				cocof17DTO.setCodsuc(ss.getCodsuc());
				cocof17DTO.setNomsuc(ss.getNomsuc());
				lista.add(cocof17DTO);
			}
		}
		for (Cocof17DTO ss : listCocofBean) {
			if (ss.getCodsuc().toString().trim().equals("4")
					|| ss.getCodsuc().toString().trim().equals("6")) {
				cocof17DTO = new Cocof17DTO();
				cocof17DTO.setCodsuc(ss.getCodsuc());
				cocof17DTO.setNomsuc(ss.getNomsuc());
				lista.add(cocof17DTO);
			}
		}
		return lista;
	}

	// public List<Presw25DTO> getListaPresw25(String tipo, int rutUsuario, int
	// anno, String dv, int sucur, int codUni, int numDoc){
	public List<Presw25DTO> getListaSede(int rut, String dv, int anno) {
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();

		lista = this.getListaPresw25("ANO", rut, anno, dv, 0, 0, 0);
		return lista;
	}

	public List<Presw25DTO> getListadepartamento(int rut, String dv, int anno,
			int sede) {
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		lista = this.getListaPresw25("ANS", rut, anno, dv, sede, 0, 0);

		return lista;
	}

	public List<Presw25DTO> getListaCarrera(int rut, String dv, int anno,
			int sede, int departamento) {
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		lista = this.getListaPresw25("ASD", rut, anno, dv, sede, departamento,
				0);
		return lista;
	}

	public List<Presw25DTO> getListaMencion(int rut, String dv, int anno,
			int sede, int departamento, int carrera) {
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		lista = this.getListaPresw25("ASC", rut, anno, dv, sede, departamento,
				carrera);
		return lista;
	}

	public void getDetalleIngresosPregrado(HttpServletRequest req, int rut,
			String dv, int anno, int sede, int departamento, int carrera,
			int mencion) {
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		PreswBean preswbean = new PreswBean("STP", 0, 0, 0, 0, rut);
		preswbean.setDigide(dv);
		preswbean.setAnopar(anno);
		preswbean.setSucur(sede);
		preswbean.setCoduni(departamento);
		preswbean.setNumdoc(carrera);
		preswbean.setNumcom(mencion);

		Collection<Presw25DTO> listaPresw25 = (Collection<Presw25DTO>) preswbean
				.consulta_presw25();
		MathTool mathtool = new MathTool();
		for (Presw25DTO ss : listaPresw25) {
			presw25DTO = new Presw25DTO();
			presw25DTO.setDesite(ss.getDesite());
			presw25DTO.setPres01(ss.getPres01());
			presw25DTO.setPres02(ss.getPres02());
			presw25DTO.setPres03(ss.getPres03());
			presw25DTO.setPres04(ss.getPres04());
			presw25DTO.setPres05(ss.getPres05());
			presw25DTO.setPres06(ss.getPres06());
			presw25DTO.setPres07(ss.getPres07());
			lista.add(presw25DTO);
		}
		HttpSession sesion = req.getSession();
		sesion.setAttribute("presupIngresosPregrado", lista);
		sesion.setAttribute("listado", lista);

	}

	public void agregaListaSede(HttpServletRequest req, Vector<String> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listaSede", lista);
	}

	public void agregaPresupIngreso(HttpServletRequest req, Vector<String> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listadoPresupIngreso", lista);

	}

	public void agregaPresupSerie(HttpServletRequest req, Vector<String> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listadoPresupSerie", lista);

	}

	public void agregaIndicadores(HttpServletRequest req, Vector<String> lista) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("listadoIndicadores", lista);

	}

	public List<Presw25DTO> retornaTotalIngresos(List<Presw25DTO> listaPresw25,
			int expresado) {
		List<Presw25DTO> listaTotalIngreso = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		if (expresado == 2) {

			for (Presw25DTO ss : listaPresw25) {
				if (ss.getDesite().trim().equals("TOTAL INGRESOS")) {
					presw25DTO = new Presw25DTO();
					presw25DTO.setPres01(ss.getPres01());
					presw25DTO.setPres02(ss.getPres02());
					presw25DTO.setPres03(ss.getPres03());
					presw25DTO.setPres04(ss.getPres04());
					presw25DTO.setPres05(ss.getPres05());
					presw25DTO.setPres06(ss.getPres06());
					presw25DTO.setPres07(ss.getPres07());
					listaTotalIngreso.add(presw25DTO);
				}
			}
		} else {
			presw25DTO = new Presw25DTO();
			presw25DTO.setPres01(1);
			presw25DTO.setPres02(1);
			presw25DTO.setPres03(1);
			presw25DTO.setPres04(1);
			presw25DTO.setPres05(1);
			presw25DTO.setPres06(1);
			presw25DTO.setPres07(1);
			listaTotalIngreso.add(presw25DTO);
		}
		return listaTotalIngreso;
	}

	public List<Presw25DTO> getListaEjeEstrategico() {
		List<Presw25DTO> listaEjeEstrategico = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Liderazgo en Ingenier�a, Ciencia y Tecno");
		presw25DTO.setComen2("Liderazgo en Ingenieria, Ciencia y Tecno");
		listaEjeEstrategico.add(presw25DTO);
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Universidad para toda la vida");
		presw25DTO.setComen2("Universidad para toda la vida");
		listaEjeEstrategico.add(presw25DTO);
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Generar valor a las empresas");
		presw25DTO.setComen2("Generar valor a las empresas");
		listaEjeEstrategico.add(presw25DTO);
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Relaci�n de largo plazo con los ex-alumn");
		presw25DTO.setComen2("Relacion de largo plazo con los ex-alumn");
		listaEjeEstrategico.add(presw25DTO);
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Compromiso Solidario");
		presw25DTO.setComen2("Compromiso Solidario");
		listaEjeEstrategico.add(presw25DTO);
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Gesti�n Competitiva");
		presw25DTO.setComen2("Gestion Competitiva");
		listaEjeEstrategico.add(presw25DTO);
		return listaEjeEstrategico;
	}

	public List<Presw25DTO> getListaAreaAcreditacion() {
		List<Presw25DTO> listaAreaAcreditacion = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Gesti�n Institucional");
		presw25DTO.setComen2("Gestion Institucional");
		listaAreaAcreditacion.add(presw25DTO);
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Docencia de Pregrado");
		presw25DTO.setComen2("Docencia de Pregrado");
		listaAreaAcreditacion.add(presw25DTO);
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Investigaci�n");
		presw25DTO.setComen2("Investigacion");
		listaAreaAcreditacion.add(presw25DTO);
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Postgrados");
		presw25DTO.setComen2("Postgrados");
		listaAreaAcreditacion.add(presw25DTO);
		presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Vinculaci�n con el medio");
		presw25DTO.setComen2("Vinculacion con el medio");
		listaAreaAcreditacion.add(presw25DTO);
		return listaAreaAcreditacion;
	}

	public List<Presw25DTO> getListaTipoActividad() {
		List<Presw25DTO> listaTipoActividad = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Equipamiento de laboratorios L1 $ 5MM");
		presw25DTO.setComen2("Equipamiento de laboratorios L1 $ 5MM");
		listaTipoActividad.add(presw25DTO);
		presw25DTO = new Presw25DTO();
		/*presw25DTO.setComen1("Equipamiento de laboratorios L2 $ 20MM");
		presw25DTO.setComen2("Equipamiento de laboratorios L2 $ 20MM");*/
		presw25DTO.setComen1("Equipamiento de laboratorios L2 $25MM");
		presw25DTO.setComen2("Equipamiento de laboratorios L2 $25MM");
		listaTipoActividad.add(presw25DTO);
		presw25DTO = new Presw25DTO();
		/*
		 * presw25DTO.setComen1("Mejoramiento de Indicadores de Pregrado:
		 * Retenci�n y Titulaci�n Oportuna"); presw25DTO.setComen2("Mejoramiento
		 * Indicador Pregr: Ret y Tit ");
		 */
		presw25DTO.setComen1("Mantenci�n y remodelaci�n");
		presw25DTO.setComen2("Mantencion y remodelacion");

		listaTipoActividad.add(presw25DTO);
		
	//	presw25DTO = new Presw25DTO();
		
		/*
		 * presw25DTO.setComen1("Planes de mejora de Acreditaci�n de Programas
		 * de Pregrado y Postgrados"); presw25DTO.setComen2("Mejora Acredit
		 * Programas Pre y Postgrado");
		 */
	/*	presw25DTO.setComen1("Planes de mejora de Acreditaci�n");
		presw25DTO.setComen2("Planes de mejora de Acreditacion");

		listaTipoActividad.add(presw25DTO);
		
		*/
		
		
		
		
		
	/*	presw25DTO = new Presw25DTO();
		presw25DTO.setComen1("Aumento de Dotaci�n de Profesores");
		presw25DTO.setComen2("Aumento de Dotacion de Profesores");
		listaTipoActividad.add(presw25DTO);*/
		presw25DTO = new Presw25DTO();
		/*
		 * presw25DTO.setComen1("Programas Proyecto Estrat�gico Institucional
		 * 2014-2018"); presw25DTO.setComen2("Programas Proy Estrateg Inst
		 * 2014-2018");
		 */
		presw25DTO
				.setComen1("Proyectos de Desarrollo Unidades de Direcci�n Superior");
		presw25DTO.setComen2("Proyectos Desa Unid de Direcc Superior");
		listaTipoActividad.add(presw25DTO);

		presw25DTO = new Presw25DTO();
		presw25DTO
				.setComen1("Proyectos de Desarrollo para la mejora de campus o Sedes");
		presw25DTO.setComen2("Proy de Desa para mejora campus o Sede");
		listaTipoActividad.add(presw25DTO);

		return listaTipoActividad;
	}

	public List<Presw25DTO> agregaDatosActividad(HttpServletRequest req,
			String codgru, String descripcionActiv, String objetivoActiv,
			String ejeActiv, String areaActiv, int rutide, String digide,
			String nombreResponsable, int a�o, int numActividad, String accion,
			long totalActiv, String tipoActividad) {
		HttpSession sesion = req.getSession();

		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();

		Presw25DTO presw25 = new Presw25DTO();
		String parte1 = "";
		String parte2 = "";
		String motiv1 = "";
		String motiv2 = "";
		if (nombreResponsable.length() > 40)
			nombreResponsable = nombreResponsable.substring(0, 39);
		if (descripcionActiv.trim().length() > 40) {
			parte1 = descripcionActiv.substring(0, 39);
			parte2 = descripcionActiv.substring(39);
		} else
			parte1 = descripcionActiv.trim();
		if (objetivoActiv.trim().length() > 40) {
			motiv1 = objetivoActiv.substring(0, 39);
			motiv2 = objetivoActiv.substring(40);
		} else
			motiv1 = objetivoActiv.trim();
		presw25.setComen1(parte1);
		presw25.setComen2(parte2);
		// presw25.setComen3(objetivoActiv);
		presw25.setMotiv1(motiv1);// objetivoActiv
		presw25.setMotiv2(motiv2);// objetivoActiv
		presw25.setComen4(ejeActiv);
		presw25.setComen5(areaActiv);
		presw25.setComen6(tipoActividad);
		presw25.setPres01(totalActiv);
		presw25.setRutide(rutide);
		presw25.setDigide(digide);
		presw25.setNomfun(nombreResponsable.trim());
		presw25.setAnopre(a�o);
		presw25.setMespre(numActividad);
		presw25.setDesite(accion);
		presw25.setCodgru(codgru);
		lista.add(presw25);

		return lista;
	}

	public List<Presw25DTO> agregaObservacionPlan(HttpServletRequest req,
			String motiv1, String motiv2, String motiv3, String motiv4,
			String motiv5, String motiv6, String comen1, int a�o) {
		HttpSession sesion = req.getSession();

		int rutUsuario = Integer.parseInt(Util.validaParametro(sesion
				.getAttribute("rutUsuario")
				+ "", "0"));
		String dv = Util.validaParametro(sesion.getAttribute("dv") + "", "");

		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();

		Presw25DTO presw25 = new Presw25DTO();

		presw25.setMotiv1(motiv1);
		presw25.setMotiv2(motiv2);
		presw25.setMotiv3(motiv3);
		presw25.setMotiv4(motiv4);
		presw25.setMotiv5(motiv5);
		presw25.setMotiv6(motiv6);
		presw25.setComen1(comen1);
		presw25.setRutide(rutUsuario);
		presw25.setDigide(dv);
		presw25.setAnopre(a�o);
		lista.add(presw25);

		return lista;
	}

	public boolean saveObservacionPlan(List<Presw25DTO> lista, int rut,
			String dv, int a�o) {
		PreswBean preswbean = new PreswBean("GPD", 0, 0, 0, 0, rut);
		preswbean.setDigide(dv);
		preswbean.setAnopar(a�o);
		return preswbean.ingreso_presw25((List<Presw25DTO>) lista);
	}

	public boolean savePresupActividad(List<Presw25DTO> lista, int rut,
			String dv, int a�o, String codgru) {
		PreswBean preswbean = new PreswBean("GDD", 0, 0, 0, 0, rut);
		preswbean.setDigide(dv);
		preswbean.setAnopar(a�o);
		preswbean.setCajero(codgru);
		return preswbean.ingreso_presw25((List<Presw25DTO>) lista);
	}

	/* plan de desarrollo */
	public void eliminaItemPlan(HttpServletRequest req, List<Presw25DTO> lista) {
		HttpSession sesion = req.getSession();
		int codActividad = Util.validaParametro(req
				.getParameter("codActividad"), 0);
		// int itedoc = Util.validaParametro(req.getParameter("item"), 0);
		// MAA
		String itedoc = Util.validaParametro(req.getParameter("item"), "");
		List<Presw25DTO> listarResultado = new ArrayList<Presw25DTO>();
		for (Presw25DTO ss : lista) {
			// if(ss.getNumreq() == codActividad && ss.getItedoc() == itedoc){
			if (ss.getNumreq() == codActividad
					&& ss.getCodman().trim().equals(itedoc.trim())) {
				ss.setPres01(0);
				ss.setPres02(0);
				ss.setPres03(0);
				ss.setPres04(0);
				ss.setPres05(0);
				ss.setPres06(0);
				ss.setPres07(0);
				ss.setPres08(0);
				ss.setPres09(0);
				ss.setPres10(0);
				ss.setPres11(0);
				ss.setPres12(0);
				ss.setTotpre(0);
				ss.setComen1("");
				ss.setTotpre(0);
				// MAA
				ss.setCodman(itedoc);

			}
			listarResultado.add(ss);
		}

		sesion.setAttribute("resultadoPresup", listarResultado);
	}

	public void agregaItemActualizaPlan(HttpServletRequest req,
			List<Presw25DTO> lista) {
		HttpSession sesion = req.getSession();

		String ene = Util.validaParametro(req.getParameter("mes1"), "0");
		String feb = Util.validaParametro(req.getParameter("mes2"), "0");
		String mar = Util.validaParametro(req.getParameter("mes3"), "0");
		String abr = Util.validaParametro(req.getParameter("mes4"), "0");
		String may = Util.validaParametro(req.getParameter("mes5"), "0");
		String jun = Util.validaParametro(req.getParameter("mes6"), "0");
		String jul = Util.validaParametro(req.getParameter("mes7"), "0");
		String ago = Util.validaParametro(req.getParameter("mes8"), "0");
		String sep = Util.validaParametro(req.getParameter("mes9"), "0");
		String oct = Util.validaParametro(req.getParameter("mes10"), "0");
		String nov = Util.validaParametro(req.getParameter("mes11"), "0");
		String dic = Util.validaParametro(req.getParameter("mes12"), "0");
		ene = ene.replace(".", "");
		feb = feb.replace(".", "");
		mar = mar.replace(".", "");
		abr = abr.replace(".", "");
		may = may.replace(".", "");
		jun = jun.replace(".", "");
		jul = jul.replace(".", "");
		ago = ago.replace(".", "");
		sep = sep.replace(".", "");
		oct = oct.replace(".", "");
		nov = nov.replace(".", "");
		dic = dic.replace(".", "");

		Long mes1 = Long.parseLong(ene);
		Long mes2 = Long.parseLong(feb);
		Long mes3 = Long.parseLong(mar);
		Long mes4 = Long.parseLong(abr);
		Long mes5 = Long.parseLong(may);
		Long mes6 = Long.parseLong(jun);
		Long mes7 = Long.parseLong(jul);
		Long mes8 = Long.parseLong(ago);
		Long mes9 = Long.parseLong(sep);
		Long mes10 = Long.parseLong(oct);
		Long mes11 = Long.parseLong(nov);
		Long mes12 = Long.parseLong(dic);
		Long totpre = new Long(0);
		totpre = mes1 + mes2 + mes3 + mes4 + mes5 + mes6 + mes7 + mes8 + mes9
				+ mes10 + mes11 + mes12;

		int codActividad = Util.validaParametro(req
				.getParameter("codActividad"), 0);
		String itedoc = Util.validaParametro(req.getParameter("itedoc"), "");
		// int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		List<Presw25DTO> listarResultado = new ArrayList<Presw25DTO>();
		String comen1 = Util.validaParametro(req.getParameter("comen1"), "");
		String comen2 = Util.validaParametro(req.getParameter("comen2"), "");
		String comen3 = Util.validaParametro(req.getParameter("comen3"), "");
		String comen4 = Util.validaParametro(req.getParameter("comen4"), "");
		String comen5 = Util.validaParametro(req.getParameter("comen5"), "");
		String comen6 = Util.validaParametro(req.getParameter("comen6"), "");
		for (Presw25DTO ss : lista) {
			// System.out.println(ss.getItedoc());
			// if(ss.getNumreq() == codActividad && ss.getItedoc() == itedoc){
			if (ss.getNumreq() == codActividad
					&& ss.getCodman().trim().equals(itedoc.trim())) {

				// System.out.println(ss.getItedoc());
				ss.setPres01(mes1);
				ss.setPres02(mes2);
				ss.setPres03(mes3);
				ss.setPres04(mes4);
				ss.setPres05(mes5);
				ss.setPres06(mes6);
				ss.setPres07(mes7);
				ss.setPres08(mes8);
				ss.setPres09(mes9);
				ss.setPres10(mes10);
				ss.setPres11(mes11);
				ss.setPres12(mes12);
				ss.setTotpre(totpre);
				ss.setComen1(comen1);
				ss.setComen2(comen2);
				ss.setComen3(comen3);
				ss.setComen4(comen4);
				ss.setComen5(comen5);
				ss.setComen6(comen6);

			}
			listarResultado.add(ss);
		}
		sesion.setAttribute("resultadoPresup", listarResultado);
	}

	public void eliminaReqPlan(HttpServletRequest req, List<Presw25DTO> lista,
			List<Presw25DTO> listaDetalle) {
		HttpSession sesion = req.getSession();
		int codActividad = Util.validaParametro(req
				.getParameter("codActividad"), 0);
		int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		int mes = Util.validaParametro(req.getParameter("mes"), 0);
		int sucur = Util.validaParametro(req.getParameter("sucur"), 0);
		List<Presw25DTO> listarResultado = new ArrayList<Presw25DTO>();
		List<Presw25DTO> listarResultadoDet = new ArrayList<Presw25DTO>();
		for (Presw25DTO ss : lista) {
			if (ss.getItedoc() == itedoc && ss.getMespre() == mes
					&& ss.getNumreq() == sucur) {

				ss.setCanman(0);
				ss.setIndmod("D");
				ss.setTotpre(0);

			}
			listarResultado.add(ss);
		}
		if (listaDetalle != null && listaDetalle.size() > 0) {
			for (Presw25DTO ss : listaDetalle) {
				if (ss.getItedoc() == itedoc && ss.getMespre() == mes
						&& ss.getNumreq() == sucur) {
					ss.setTippro("RPQ");
					ss.setIndmod("D");
					ss.setCanman(0);
					ss.setTotpre(0);

				}
				listarResultadoDet.add(ss);
			}
		}
		sesion.setAttribute("presupItemreq", listarResultado);
		sesion.setAttribute("resultadoDetalleItemReq", listarResultadoDet);
	}

	public void agregaDetalleItemReqSesionPlan(HttpServletRequest req,
			List<Presw25DTO> lista, List<Presw25DTO> lista2) {
		HttpSession sesion = req.getSession();
		int rutUsuario = Util.validaParametro(Integer.parseInt(req.getSession()
				.getAttribute("rutUsuario")
				+ ""), 0);
		int a�o = Util.validaParametro(req.getParameter("anno"), 0);
		int codActividad = Util.validaParametro(req
				.getParameter("codActividad"), 0);
		int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		int mes = Util.validaParametro(req.getParameter("mes"), 0);
		int sucur = Util.validaParametro(req.getParameter("sucur"), 0);
		int numrq = 0;
		Long totalMensual = Util.validaParametro(req.getParameter("total"),
				new Long(0));
		List<Presw25DTO> lista3 = new ArrayList<Presw25DTO>();
		List<Presw25DTO> lista4 = new ArrayList<Presw25DTO>();

		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();

		/*
		 * primero verificar si es requerimiento nuevo, se coloca como n�mero
		 * 1000
		 */
		if (sucur == 0) {
			if (lista2.size() > 0) {
				for (Presw25DTO ss : lista2) {
					if (ss.getNumreq() > numrq)
						numrq = ss.getNumreq();
				}
				numrq++;
			} else
				numrq = 1000;
			/* y agregarlo a la lista en lista3 */
			for (Presw25DTO ss : lista) {
				ss.setNumreq(numrq);
				ss.setValpr1(1); // esto es solo para marcar que es nuevo
									// requerimiento
				ss.setCanman(0);
				lista3.add(ss);
			}
		} else {
			numrq = sucur;
			for (Presw25DTO ss : lista) {
				lista3.add(ss);
			}
		}

		boolean agrega = false;
		/*
		 * recorre la lista de sesi�n y ve si ya existe sino lo agrega a lista4
		 * que tiene la suma final
		 */
		if (lista2 != null && lista2.size() > 0) {
			for (Presw25DTO ss : lista2) {
				if (ss.getNumreq() == numrq && ss.getItedoc() == itedoc) {
					for (Presw25DTO rs : lista3) {
						if (/* rs.getCoduni() == ss.getCoduni() && */rs
								.getItedoc() == ss.getItedoc()
								&& rs.getNumreq() == ss.getNumreq()
								&& rs.getMespre() == ss.getMespre()
								&& rs.getCodman() == ss.getCodman()) {
							lista4.add(rs);
							agrega = true;
						}

					}
				} else {
					lista4.add(ss);

				}
			}
		} else {
			for (Presw25DTO rs : lista3) {
				lista4.add(rs);
			}
			agrega = true;
		}

		if (!agrega) {
			for (Presw25DTO rs : lista3) {
				lista4.add(rs);
			}
		}

		sesion.setAttribute("listaPresw25", lista3);
		sesion.setAttribute("resultadoDetalleItemReq", lista4);

	}

	public void agregaItemReqDetPlan(HttpServletRequest req,
			List<Presw25DTO> lista, List<Presw25DTO> presupItemreq,
			List<Presw25DTO> listarResultadoPresup) {
		// resultadoDetalleItemReq, presupItemreq, listarResultadoPresup
		HttpSession sesion = req.getSession();
		List<Presw25DTO> listaRQP = new ArrayList();
		List<Presw25DTO> listaResultado = new ArrayList();

		String area1 = Util.validaParametro(req.getParameter("area1"), "");
		String area2 = Util.validaParametro(req.getParameter("area2"), "");
		String comen1 = Util.validaParametro(req.getParameter("comen1"), "");
		String comen2 = Util.validaParametro(req.getParameter("comen2"), "");
		String comen3 = Util.validaParametro(req.getParameter("comen3"), "");
		String comen4 = Util.validaParametro(req.getParameter("comen4"), "");
		String comen5 = Util.validaParametro(req.getParameter("comen5"), "");
		String comen6 = Util.validaParametro(req.getParameter("comen6"), "");
		String motiv1 = Util.validaParametro(req.getParameter("motiv1"), "");
		String motiv2 = Util.validaParametro(req.getParameter("motiv2"), "");
		String motiv3 = Util.validaParametro(req.getParameter("motiv3"), "");
		String motiv4 = Util.validaParametro(req.getParameter("motiv4"), "");
		String motiv5 = Util.validaParametro(req.getParameter("motiv5"), "");
		String motiv6 = Util.validaParametro(req.getParameter("motiv6"), "");
		String codgru = Util.validaParametro(req.getParameter("codgru"), "");
		Long total = Util.validaParametro(req.getParameter("totalRequ"),
				new Long(0));
		int mes = Util.validaParametro(req.getParameter("mes"), 0);
		int numreq = Util.validaParametro(req.getParameter("sucur"), 0);
		int item = Util.validaParametro(req.getParameter("itedoc"), 0);
		int codActividad = Util.validaParametro(req
				.getParameter("codActividad"), 0);
		String nomActividad = Util.validaParametro(req
				.getParameter("nomActividad"), "");
		int anno = Util.validaParametro(req.getParameter("anno"), 0);
		Long totalTotal = Util.validaParametro(req.getParameter("totalMes"),
				new Long(0));
		int i = Util.validaParametro(req.getParameter("item"), 0);
		int itemElimina = Util.validaParametro(req.getParameter("itemElimina"),
				0);
		if (itemElimina > 0)
			i = itemElimina;
		int j = 0;
		long pres03 = 0;
		long pres01 = 0;
		String fecha = "";
		int pres02 = 0;

		int valpr = 0;
		String desite = "";
		String desuni = "";
		int canman = 0;
		long valorReq = 0;
		if (lista.size() > 0) {
			for (Presw25DTO ss : lista) {
				pres02 = 0;
				pres03 = 0;
				pres01 = 0;

				if (ss.getNumreq() == numreq && ss.getMespre() == mes) {
					// System.out.println(ss.getItedoc());
					j++;
					if (j == i) {
						if (itemElimina > 0) {
							if (ss.getCodman().trim().equals("COTIZACION")) {
								valorReq = ss.getPres03();
								ss.setCanman(0);
							} else
								valorReq = (ss.getCanman() * ss.getValuni());
							totalTotal -= valorReq;
							total -= valorReq;
							ss.setIndmod("D");
						} else
							valorReq = 0;
						canman = Util.validaParametro(req
								.getParameter("canman"), 0);
						pres03 = Util.validaParametro(req
								.getParameter("pres03"), 0);
						pres01 = Util.validaParametro(req
								.getParameter("pres01"), 0);
						fecha = Util.validaParametro(req
								.getParameter("fechaInicio"), "");
						if (!fecha.trim().equals(""))
							pres02 = Integer.parseInt(fecha.substring(6)
									+ fecha.substring(3, 5)
									+ fecha.substring(0, 2));

						if (pres03 > 0) {
							valorReq = pres03;
							// canman = 1;
						} else {
							valorReq = (canman * ss.getValuni());
						}
						total += valorReq;
						totalTotal = totalTotal + valorReq;

					} else
						canman = ss.getCanman();
					// if(canman > 0) {
					ss.setTippro("RPQ");
					ss.setArea1(area1);
					ss.setArea2(area2);
					ss.setComen1(comen1);
					ss.setComen2(comen2);
					ss.setComen3(comen3);
					ss.setComen4(comen4);
					ss.setComen5(comen5);
					ss.setComen6(comen6);
					ss.setMotiv1(motiv1);
					ss.setMotiv2(motiv2);
					ss.setMotiv3(motiv3);
					ss.setMotiv4(motiv4);
					ss.setMotiv5(motiv5);
					ss.setMotiv6(motiv6);
					ss.setCanman(canman);
					ss.setPres01(pres01);
					ss.setPres02(pres02);
					ss.setPres03(pres03);
					ss.setCodgru(codgru);
					listaRQP.add((Presw25DTO) ss);
					valpr = ss.getValpr1();
					desite = ss.getDesite();
					desuni = ss.getDesuni();
					// }

				} else
					listaRQP.add(ss);

			}

		}
		/* agrega en MAN */
		Presw25DTO presw25 = new Presw25DTO();
		presw25.setTippro("MNA");
		presw25.setItedoc(item);
		presw25.setAnopre(anno);
		presw25.setMespre(mes);
		presw25.setNumreq(numreq);
		presw25.setValpr1(valpr);
		presw25.setDesite(desite);
		presw25.setComen1(area1);
		presw25.setComen2(area2);
		presw25.setDesuni(desuni);
		presw25.setTotpre(total);
		presw25.setCodgru(codgru);
		if (total == 0)
			presw25.setIndmod("D");
		if (presupItemreq != null && presupItemreq.size() > 0) {
			i = -1;
			boolean agrega = false;
			for (Presw25DTO ss : presupItemreq) {
				i++;
				if (ss.getItedoc() == item && ss.getMespre() == mes
						&& ss.getNumreq() == numreq) {
					presupItemreq.set(i, presw25);
					agrega = true;
				}
			}
			if (!agrega)
				presupItemreq.add(presw25);
		} else
			presupItemreq.add(presw25);

		Long totalItemReq = new Long(0);
		Long ene = new Long(0);
		Long feb = new Long(0);
		Long mar = new Long(0);
		Long abr = new Long(0);
		Long may = new Long(0);
		Long jun = new Long(0);
		Long jul = new Long(0);
		Long ago = new Long(0);
		Long sep = new Long(0);
		Long oct = new Long(0);
		Long nov = new Long(0);
		Long dic = new Long(0);

		if (listarResultadoPresup.size() > 0) {
			for (Presw25DTO ss : presupItemreq) { // agregar todo lo que est�
													// en mantencion al presup
				if (ss.getItedoc() == item) {
					switch (ss.getMespre()) {
					case 1:
						ene = ene + ss.getTotpre();
						break;
					case 2:
						feb = feb + ss.getTotpre();
						break;
					case 3:
						mar = mar + ss.getTotpre();
						break;
					case 4:
						abr = abr + ss.getTotpre();
						break;
					case 5:
						may = may + ss.getTotpre();
						break;
					case 6:
						jun = jun + ss.getTotpre();
						break;
					case 7:
						jul = jul + ss.getTotpre();
						break;
					case 8:
						ago = ago + ss.getTotpre();
						break;
					case 9:
						sep = sep + ss.getTotpre();
						break;
					case 10:
						oct = oct + ss.getTotpre();
						break;
					case 11:
						nov = nov + ss.getTotpre();
						break;
					case 12:
						dic = dic + ss.getTotpre();
						break;
					}
					totalItemReq = totalItemReq + ss.getTotpre();
				}
			}

			i = -1;
			for (Presw25DTO ss : listarResultadoPresup) {
				i++;
				if (ss.getItedoc() == item) {
					ss.setItedoc(item);
					ss.setPres01(ene);
					ss.setPres02(feb);
					ss.setPres03(mar);
					ss.setPres04(abr);
					ss.setPres05(may);
					ss.setPres06(jun);
					ss.setPres07(jul);
					ss.setPres08(ago);
					ss.setPres09(sep);
					ss.setPres10(oct);
					ss.setPres11(nov);
					ss.setPres12(dic);
					ss.setTotpre(Long.parseLong(String.valueOf(ss.getPres01()
							+ ss.getPres02() + ss.getPres03() + ss.getPres04()
							+ ss.getPres05() + ss.getPres06() + ss.getPres07()
							+ ss.getPres08() + ss.getPres09() + ss.getPres10()
							+ ss.getPres11() + ss.getPres12())));
				}
				listaResultado.add(ss);
				// System.out.println("result: " + ss.getTotpre());
			}

		}
		/*
		 * System.out.println("tama�o presupItemreq: "+presupItemreq.size());
		 * for (Presw25DTO ss : presupItemreq){
		 * 
		 * System.out.println("ss "+ss.getMespre()+"cant " +
		 * ss.getPres01()+"req: "+ ss.getNumreq()); System.out.println("ss
		 * "+ss.getMespre()+"cant " + ss.getPres02()+"req: "+ ss.getNumreq());
		 * System.out.println("ss "+ss.getMespre()+"cant " +
		 * ss.getPres03()+"req: "+ ss.getNumreq());
		 *  } System.out.println("presupItemreq desp **:
		 * "+presupItemreq.size());
		 */
		if (presupItemreq != null)
			sesion.setAttribute("presupItemreq", presupItemreq);
		sesion.setAttribute("resultadoDetalleItemReq", listaRQP);

		sesion.setAttribute("resultadoPresup", listaResultado);
		// req.setAttribute("totalMes", totalTotal);

	}

	public void actualizaItemReqDetPlan(HttpServletRequest req,
			List<Presw25DTO> lista, List<Presw25DTO> presupItemreq) {
		// resultadoDetalleItemReq, presupItemreq, listarResultadoPresup
		HttpSession sesion = req.getSession();
		List<Presw25DTO> listaRQP = new ArrayList();
		List<Presw25DTO> listaResultado = new ArrayList();

		String area1 = Util.validaParametro(req.getParameter("area1"), "");
		String area2 = Util.validaParametro(req.getParameter("area2"), "");
		String comen1 = Util.validaParametro(req.getParameter("comen1"), "");
		String comen2 = Util.validaParametro(req.getParameter("comen2"), "");
		String comen3 = Util.validaParametro(req.getParameter("comen3"), "");
		String comen4 = Util.validaParametro(req.getParameter("comen4"), "");
		String comen5 = Util.validaParametro(req.getParameter("comen5"), "");
		String comen6 = Util.validaParametro(req.getParameter("comen6"), "");
		String motiv1 = Util.validaParametro(req.getParameter("motiv1"), "");
		String motiv2 = Util.validaParametro(req.getParameter("motiv2"), "");
		String motiv3 = Util.validaParametro(req.getParameter("motiv3"), "");
		String motiv4 = Util.validaParametro(req.getParameter("motiv4"), "");
		String motiv5 = Util.validaParametro(req.getParameter("motiv5"), "");
		String motiv6 = Util.validaParametro(req.getParameter("motiv6"), "");
		int mes = Util.validaParametro(req.getParameter("mes"), 0);
		int numreq = Util.validaParametro(req.getParameter("sucur"), 0);
		int item = Util.validaParametro(req.getParameter("itedoc"), 0);
		int codActividad = Util.validaParametro(req
				.getParameter("codActividad"), 0);
		int anno = Util.validaParametro(req.getParameter("anno"), 0);

		if (lista.size() > 0) {
			for (Presw25DTO ss : lista) {

				if (ss.getNumreq() == numreq && ss.getMespre() == mes) {
					// System.out.println(ss.getItedoc());

					ss.setTippro("RPQ");
					ss.setArea1(area1);
					ss.setArea2(area2);
					ss.setComen1(comen1);
					ss.setComen2(comen2);
					ss.setComen3(comen3);
					ss.setComen4(comen4);
					ss.setComen5(comen5);
					ss.setComen6(comen6);
					ss.setMotiv1(motiv1);
					ss.setMotiv2(motiv2);
					ss.setMotiv3(motiv3);
					ss.setMotiv4(motiv4);
					ss.setMotiv5(motiv5);
					ss.setMotiv6(motiv6);
					ss.setCanman(ss.getCanman());
					ss.setPres01(ss.getPres01());
					ss.setPres02(ss.getPres02());
					ss.setPres03(ss.getPres03());
					listaRQP.add((Presw25DTO) ss);

				} else
					listaRQP.add(ss);

			}

		}
		/* agrega en MAN */

		for (Presw25DTO ss : presupItemreq) {

			if (ss.getItedoc() == item && ss.getMespre() == mes
					&& ss.getNumreq() == numreq) {
				ss.setTippro("MNA");
				ss.setComen1(area1);
				ss.setComen2(area2);
				listaResultado.add((Presw25DTO) ss);
			} else
				listaResultado.add(ss);
		}

		if (presupItemreq != null)
			sesion.setAttribute("presupItemreq", listaResultado);
		sesion.setAttribute("resultadoDetalleItemReq", listaRQP);

	}

	public void agregaRegistraMatrizPlan(HttpServletRequest req,
			List<Presw25DTO> lista, List<Presw25DTO> lista2) {
		HttpSession sesion = req.getSession();
		List<Presw25DTO> listaPRP = new ArrayList();
		List<Presw25DTO> listaCPR = new ArrayList();
		String indexi = Util.validaParametro(req.getParameter("indexi"), "");
		int codActividad = Util.validaParametro(req
				.getParameter("codActividad"), 0);
		String codgru = Util.validaParametro(req.getParameter("codgru"), "");
		for (Presw25DTO ss : lista) {
			Presw25DTO presw25 = ss;
			presw25.setTippro("PGD");
			presw25.setIndexi(indexi);
			presw25.setCodgru(codgru);
			listaPRP.add((Presw25DTO) presw25);
		}
		for (Presw25DTO ss : lista2) {
			Presw25DTO presw25 = ss;
			if (ss.getNumreq() == codActividad)
				presw25.setIndexi(indexi);
			listaCPR.add((Presw25DTO) presw25);
		}
		sesion.setAttribute("resultadoPresup", listaPRP);
		sesion.setAttribute("resultadoCPR", listaCPR);

	}

	public boolean saveMatrizPlan(List<Presw25DTO> lista, int codActividad,
			int a�o, int rutUsuario, String dv) {
		PreswBean preswbean = new PreswBean("PGD", 0, 0, a�o, 0, rutUsuario);
		preswbean.setDigide(dv);
		preswbean.setMespar(codActividad);
		for (Presw25DTO ss : lista) {
			ss.setDesite(ss.getCodman());
		}

		return preswbean.ingreso_presw25((List<Presw25DTO>) lista);
	}

	public boolean saveREQPlan(List<Presw25DTO> lista, int codActividad,
			int a�o, int rutUsuario, int mes, String dv, String codgru) {
		boolean retorno = false;
		List<Presw25DTO> listaRQP = new ArrayList();
		int item = 0;
		int numreq = 0;
		int mesAnt = 1;
		if (lista != null && lista.size() > 0) {
			for (Presw25DTO ss : lista) {
				if (numreq != ss.getNumreq() /* && mesAnt == ss.getMespre() */) {
					ss.setIndexi("I");
					/*
					 * if(ss.getCanman() == 0) ss.setIndmod("D");
					 */
				}
				numreq = ss.getNumreq();
				item = ss.getItedoc();
				mesAnt = ss.getMespre();
				if (ss.getValpr1() == 1) {

					ss.setNumreq(0);

				}
				ss.setTotpre(codActividad);
				ss.setRutide(rutUsuario);
				ss.setDigide(dv);
				ss.setCodgru(codgru);
				listaRQP.add(ss);
				// System.out.println(ss.getIndexi()+ " ind : "+
				// ss.getIndmod()+" ss.getMespre(): " + ss.getMespre()+"
				// ss.getCanman(): "+ss.getCanman()+" numreq: "+numreq+ "
				// ss.getNumreq(): "+ss.getNumreq());
			}
		}
		PreswBean preswbean = new PreswBean("RPQ", 0, item, a�o, mes,
				rutUsuario);
		preswbean.setDigide(dv);
		preswbean.setCodban(codActividad);
		preswbean.setCajero(codgru);
		retorno = preswbean.ingreso_presw25((List<Presw25DTO>) listaRQP);

		return retorno;
	}

	public Vector getlistaReqResumenExcelPlan(HttpServletRequest req) {
		// int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
		String itedoc = Util.validaParametro(req.getParameter("itedoc"), "");
		int a�o = Util.validaParametro(req.getParameter("anno"), 0);
		int codActividad = Util.validaParametro(req
				.getParameter("codActividad"), 0);
		String nomActividad = Util.validaParametro(req
				.getParameter("nomActividad"), "");
		int rutUsuario = Integer.parseInt(req.getSession().getAttribute(
				"rutUsuario")
				+ "");
		String dv = Util.validaParametro(req.getSession().getAttribute("dv")
				+ "", "");
		String codgru = Util.validaParametro(req.getParameter("codgru"), "");

		Vector vec_datos = new Vector();
		Vector vec_d = new Vector();
		List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
		long total = 0;
		long totalTotal = 0;
		long enero = 0;
		long febrero = 0;
		long marzo = 0;
		long abril = 0;
		long mayo = 0;
		long junio = 0;
		long julio = 0;
		long agosto = 0;
		long septiembre = 0;
		long octubre = 0;
		long noviembre = 0;
		long diciembre = 0;
		PreswBean preswbean = new PreswBean("PRD", 0, 0, a�o, 0, rutUsuario);
		NumberFormat nf2 = NumberFormat.getInstance(Locale.GERMAN);

		if (preswbean != null) {
			preswbean.setDigide(dv);
			preswbean.setRutide(rutUsuario);
			preswbean.setMespar(codActividad);
			preswbean.setCajero(codgru);
			listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
		}

		long totalAgrup = 0;
		Vector agrup = null;
		Vector agrup1 = new Vector();
		Vector agrup2 = new Vector();
		Vector agrup3 = new Vector();
		Vector agrup4 = new Vector();
		long total_ENE = 0;
		long total_FEB = 0;
		long total_MAR = 0;
		long total_ABR = 0;
		long total_MAY = 0;
		long total_JUN = 0;
		long total_JUL = 0;
		long total_AGO = 0;
		long total_SEP = 0;
		long total_OCT = 0;
		long total_NOV = 0;
		long total_DIC = 0;
		long agrup_ENE = 0;
		long agrup_FEB = 0;
		long agrup_MAR = 0;
		long agrup_ABR = 0;
		long agrup_MAY = 0;
		long agrup_JUN = 0;
		long agrup_JUL = 0;
		long agrup_AGO = 0;
		long agrup_SEP = 0;
		long agrup_OCT = 0;
		long agrup_NOV = 0;
		long agrup_DIC = 0;

		for (Presw25DTO ss : listaPresw25) {
			if (!ss.getMotiv3().trim().equals("")) {
				total = ss.getPres01() + ss.getPres02() + ss.getPres03()
						+ ss.getPres04() + ss.getPres05() + ss.getPres06()
						+ ss.getPres07() + ss.getPres08() + ss.getPres09()
						+ ss.getPres10() + ss.getPres11() + ss.getPres12();
				total_ENE = ss.getPres01();
				total_FEB = ss.getPres02();
				total_MAR = ss.getPres03();
				total_ABR = ss.getPres04();
				total_MAY = ss.getPres05();
				total_JUN = ss.getPres06();
				total_JUL = ss.getPres07();
				total_AGO = ss.getPres08();
				total_SEP = ss.getPres09();
				total_OCT = ss.getPres10();
				total_NOV = ss.getPres11();
				total_DIC = ss.getPres12();
				if (!ss.getCodman().trim().equals(ss.getMotiv3().trim())) {
					totalAgrup = totalAgrup + total;
					agrup_ENE = agrup_ENE + total_ENE;
					agrup_FEB = agrup_FEB + total_FEB;
					agrup_MAR = agrup_MAR + total_MAR;
					agrup_ABR = agrup_ABR + total_ABR;
					agrup_MAY = agrup_MAY + total_MAY;
					agrup_JUN = agrup_JUN + total_JUN;
					agrup_JUL = agrup_JUL + total_JUL;
					agrup_AGO = agrup_AGO + total_AGO;
					agrup_SEP = agrup_SEP + total_SEP;
					agrup_OCT = agrup_OCT + total_OCT;
					agrup_NOV = agrup_NOV + total_NOV;
					agrup_DIC = agrup_DIC + total_DIC;
				} else {
					agrup = new Vector();
					agrup.addElement(totalAgrup);
					agrup.addElement(agrup_ENE);
					agrup.addElement(agrup_FEB);
					agrup.addElement(agrup_MAR);
					agrup.addElement(agrup_ABR);
					agrup.addElement(agrup_MAY);
					agrup.addElement(agrup_JUN);
					agrup.addElement(agrup_JUL);
					agrup.addElement(agrup_AGO);
					agrup.addElement(agrup_SEP);
					agrup.addElement(agrup_OCT);
					agrup.addElement(agrup_NOV);
					agrup.addElement(agrup_DIC);
					if (ss.getCodman().trim().equals("2"))
						agrup1 = agrup;
					else if (ss.getCodman().trim().equals("3"))
						agrup2 = agrup;
					else if (ss.getCodman().trim().equals("4"))
						agrup3 = agrup;

					totalAgrup = 0;
					agrup_ENE = 0;
					agrup_FEB = 0;
					agrup_MAR = 0;
					agrup_ABR = 0;
					agrup_MAY = 0;
					agrup_JUN = 0;
					agrup_JUL = 0;
					agrup_AGO = 0;
					agrup_SEP = 0;
					agrup_OCT = 0;
					agrup_NOV = 0;
					agrup_DIC = 0;

				}
			}
		}
		agrup = new Vector();
		agrup.addElement(totalAgrup);
		agrup.addElement(agrup_ENE);
		agrup.addElement(agrup_FEB);
		agrup.addElement(agrup_MAR);
		agrup.addElement(agrup_ABR);
		agrup.addElement(agrup_MAY);
		agrup.addElement(agrup_JUN);
		agrup.addElement(agrup_JUL);
		agrup.addElement(agrup_AGO);
		agrup.addElement(agrup_SEP);
		agrup.addElement(agrup_OCT);
		agrup.addElement(agrup_NOV);
		agrup.addElement(agrup_DIC);
		agrup4 = agrup;

		for (Presw25DTO ss : listaPresw25) {
			if (!ss.getMotiv3().trim().equals("")) {
				total = ss.getPres01() + ss.getPres02() + ss.getPres03()
						+ ss.getPres04() + ss.getPres05() + ss.getPres06()
						+ ss.getPres07() + ss.getPres08() + ss.getPres09()
						+ ss.getPres10() + ss.getPres11() + ss.getPres12();
				// if(total > 0){
				if (total > 0
						|| ss.getCodman().trim().equals(ss.getMotiv3().trim())) {
					if (!ss.getCodman().trim().equals(ss.getMotiv3().trim()))
						// vec_datos.add(String.valueOf(ss.getItedoc()));
						vec_datos.add(ss.getCodman());
					else
						vec_datos.add("");
					vec_datos.add(ss.getDesite());

					agrup = new Vector();
					if (ss.getCodman().trim().equals(ss.getMotiv3().trim())) {
						if (ss.getCodman().trim().equals("1"))
							agrup = agrup1;
						else if (ss.getCodman().trim().equals("2"))
							agrup = agrup2;
						else if (ss.getCodman().trim().equals("3"))
							agrup = agrup3;
						else if (ss.getCodman().trim().equals("4"))
							agrup = agrup4;

						vec_datos.add(nf2.format(agrup.get(0)));
						vec_datos.add(nf2.format(agrup.get(1)));
						vec_datos.add(nf2.format(agrup.get(2)));
						vec_datos.add(nf2.format(agrup.get(3)));
						vec_datos.add(nf2.format(agrup.get(4)));
						vec_datos.add(nf2.format(agrup.get(5)));
						vec_datos.add(nf2.format(agrup.get(6)));
						vec_datos.add(nf2.format(agrup.get(7)));
						vec_datos.add(nf2.format(agrup.get(8)));
						vec_datos.add(nf2.format(agrup.get(9)));
						vec_datos.add(nf2.format(agrup.get(10)));
						vec_datos.add(nf2.format(agrup.get(11)));
						vec_datos.add(nf2.format(agrup.get(12)));
						vec_datos.add("");
					} else {
						vec_datos.add(nf2.format(total));
						vec_datos.add(nf2.format(ss.getPres01()));
						vec_datos.add(nf2.format(ss.getPres02()));
						vec_datos.add(nf2.format(ss.getPres03()));
						vec_datos.add(nf2.format(ss.getPres04()));
						vec_datos.add(nf2.format(ss.getPres05()));
						vec_datos.add(nf2.format(ss.getPres06()));
						vec_datos.add(nf2.format(ss.getPres07()));
						vec_datos.add(nf2.format(ss.getPres08()));
						vec_datos.add(nf2.format(ss.getPres09()));
						vec_datos.add(nf2.format(ss.getPres10()));
						vec_datos.add(nf2.format(ss.getPres11()));
						vec_datos.add(nf2.format(ss.getPres12()));
						vec_datos.add(ss.getComen1().toString().trim()
								+ ss.getComen2().toString().trim()
								+ ss.getComen3().toString().trim()
								+ ss.getComen4().toString().trim()
								+ ss.getComen5().toString().trim()
								+ ss.getComen6().toString().trim());
					}

					enero += ss.getPres01();
					febrero += ss.getPres02();
					marzo += ss.getPres03();
					abril += ss.getPres04();
					mayo += ss.getPres05();
					junio += ss.getPres06();
					julio += ss.getPres07();
					agosto += ss.getPres08();
					septiembre += ss.getPres09();
					octubre += ss.getPres10();
					noviembre += ss.getPres11();
					diciembre += ss.getPres12();
					totalTotal += total;
				}
			}
		}
		vec_datos.add("");
		vec_datos.add("Total $");
		vec_datos.add(nf2.format(totalTotal));
		vec_datos.add(nf2.format(enero));
		vec_datos.add(nf2.format(febrero));
		vec_datos.add(nf2.format(marzo));
		vec_datos.add(nf2.format(abril));
		vec_datos.add(nf2.format(mayo));
		vec_datos.add(nf2.format(junio));
		vec_datos.add(nf2.format(julio));
		vec_datos.add(nf2.format(agosto));
		vec_datos.add(nf2.format(septiembre));
		vec_datos.add(nf2.format(octubre));
		vec_datos.add(nf2.format(noviembre));
		vec_datos.add(nf2.format(diciembre));
		vec_datos.add(nomActividad);
		return vec_datos;
	}

	public void getlistaPresupItemreqPlan(HttpServletRequest req) {
		int a�o = Util.validaParametro(req.getParameter("anno"), 0);
		int codActividad = Util.validaParametro(req
				.getParameter("codActividad"), 0);
		String nomActividad = Util.validaParametro(req
				.getParameter("nomActividad"), "");
		int rutUsuario = Integer.parseInt(req.getSession().getAttribute(
				"rutUsuario")
				+ "");
		String dv = Util.validaParametro(req.getSession().getAttribute("dv")
				+ "", "");
		HttpSession sesion = req.getSession();

		/* agrega requerimiento */
		String nomItem = "";
		String nomMes = "";
		int itedoc = 41;
		Vector vecTotales = new Vector();
		Vector vec_d = new Vector();
		List<Presw25DTO> presupItemreq = new ArrayList<Presw25DTO>();
		List<Presw25DTO> presupItemreqDet = new ArrayList<Presw25DTO>();

		PreswBean preswbean = new PreswBean("MNA", 0, itedoc, a�o, 0,
				rutUsuario);
		if (preswbean != null) {
			preswbean.setDigide(dv);
			preswbean.setRutide(rutUsuario);
			preswbean.setMespar(codActividad);
			presupItemreq = (List<Presw25DTO>) preswbean.consulta_presw25();
		}

		Long en = new Long(0);
		Long fe = new Long(0);
		Long ma = new Long(0);
		Long ab = new Long(0);
		Long my = new Long(0);
		Long jn = new Long(0);
		Long jl = new Long(0);
		Long ag = new Long(0);
		Long se = new Long(0);
		Long oc = new Long(0);
		Long no = new Long(0);
		Long di = new Long(0);
		Vector listaReqResumen = new Vector();
		Vector listaReqDetResumen = new Vector();

		if (presupItemreq != null && presupItemreq.size() > 0) {
			Vector meses = new Vector();
			int numMes = 1;
			// while (numMes < 13) {
			for (Presw25DTO ss : presupItemreq) {
				Presw25DTO presw25 = ss;

				if (presw25.getItedoc() == itedoc && presw25.getAnopre() == a�o
						&& ss.getTotpre() > 0
				/* && numMes == ss.getMespre() */) {
					nomItem = ss.getItedoc() + " - " + ss.getDesite();
					switch (ss.getMespre()) {
					case 1: {
						en = en + presw25.getTotpre();
						nomMes = "Enero";
						break;
					}
					case 2: {
						fe = fe + presw25.getTotpre();
						nomMes = "Febrero";
						break;
					}

					case 3: {
						ma = ma + presw25.getTotpre();
						nomMes = "Marzo";
						break;
					}

					case 4: {
						ab = ab + presw25.getTotpre();
						nomMes = "Abril";
						break;
					}

					case 5: {
						my = my + presw25.getTotpre();
						nomMes = "Mayo";
						break;
					}

					case 6: {
						jn = jn + presw25.getTotpre();
						nomMes = "Junio";
						break;
					}

					case 7: {
						jl = jl + presw25.getTotpre();
						nomMes = "Julio";
						break;
					}

					case 8: {
						ag = ag + presw25.getTotpre();
						nomMes = "Agosto";
						break;
					}

					case 9: {
						se = se + presw25.getTotpre();
						nomMes = "Septiembre";
						break;
					}

					case 10: {
						oc = oc + presw25.getTotpre();
						nomMes = "Octubre";
						break;
					}

					case 11: {
						no = no + presw25.getTotpre();
						nomMes = "Noviembre";
						break;
					}

					case 12: {
						di = di + presw25.getTotpre();
						nomMes = "Diciembre";
						break;
					}

					}

					vec_d = new Vector();
					vec_d.addElement(ss.getMespre());
					vec_d.addElement(nomMes);
					vec_d.addElement(ss.getComen1().trim());
					vec_d.addElement(ss.getComen2().trim());
					vec_d.addElement(ss.getTotpre());
					switch (ss.getMespre()) {
					case 1:
						vec_d.addElement(en);
						break;
					case 2:
						vec_d.addElement(fe);
						break;
					case 3:
						vec_d.addElement(ma);
						break;
					case 4:
						vec_d.addElement(ab);
						break;
					case 5:
						vec_d.addElement(my);
						break;
					case 6:
						vec_d.addElement(jn);
						break;
					case 7:
						vec_d.addElement(jl);
						break;
					case 8:
						vec_d.addElement(ag);
						break;
					case 9:
						vec_d.addElement(se);
						break;
					case 10:
						vec_d.addElement(oc);
						break;
					case 11:
						vec_d.addElement(no);
						break;
					case 12:
						vec_d.addElement(di);
						break;
					}
					listaReqResumen.addElement(vec_d);

					preswbean = new PreswBean("RQA", 0, itedoc, a�o, ss
							.getMespre(), rutUsuario);
					if (preswbean != null) {
						preswbean.setSucur(ss.getNumreq());
						preswbean.setDigide(dv);
						preswbean.setRutide(rutUsuario);
						preswbean.setCodban(codActividad);

						presupItemreqDet = new ArrayList<Presw25DTO>();
						presupItemreqDet = (List<Presw25DTO>) preswbean
								.consulta_presw25();
					}
					// }

					// }

					// numMes++;
					// }
					// }
					nomMes = "";
					if (presupItemreqDet.size() > 0) {
						for (Presw25DTO pi : presupItemreqDet) {
							switch (pi.getMespre()) {
							case 1:
								nomMes = "Enero";
								break;
							case 2:
								nomMes = "Febrero";
								break;
							case 3:
								nomMes = "Marzo";
								break;
							case 4:
								nomMes = "Abril";
								break;
							case 5:
								nomMes = "Mayo";
								break;
							case 6:
								nomMes = "Junio";
								break;
							case 7:
								nomMes = "Julio";
								break;
							case 8:
								nomMes = "Agosto";
								break;
							case 9:
								nomMes = "Septiembre";
								break;
							case 10:
								nomMes = "Octubre";
								break;
							case 11:
								nomMes = "Noviembre";
								break;
							case 12:
								nomMes = "Diciembre";
								break;
							}
							vec_d = new Vector();
							vec_d.addElement(pi.getCoduni() + " - "
									+ pi.getDesuni()); // 0 UNIDAD
							vec_d.addElement(pi.getItedoc() + " - "
									+ pi.getDesite()); // 1 - ITEM
							vec_d.addElement(nomMes.concat(" - N� "
									+ pi.getNumreq() + ""));// 3 - requerimiento
							vec_d.addElement(pi.getArea1().trim()
									+ pi.getArea2().trim());// 2 - AREA
							vec_d.addElement(pi.getComen1().trim()
									+ pi.getComen2().trim()
									+ pi.getComen3().trim()
									+ pi.getComen4().trim()
									+ pi.getComen5().trim()
									+ pi.getComen6().trim()); // 4 -
																// DESCRIPCION
							vec_d.addElement(pi.getMotiv1().trim()
									+ pi.getMotiv2().trim()
									+ pi.getMotiv3().trim()
									+ pi.getMotiv4().trim()
									+ pi.getMotiv5().trim()
									+ pi.getMotiv6().trim()); // 5 - MOTIVO
							vec_d.addElement(pi.getCodman()); // 6 - ITEM
							vec_d.addElement(pi.getTipcon()); // 7 - DETALLE
							if (pi.getCodman().trim().equals("COTIZACION")
									&& pi.getPres03() > 0) {
								String fecha = pi.getPres02() + "";
								vec_d.addElement(pi.getPres01()); // 9 -
																	// cotizacion
								vec_d.addElement(pi.getPres03()); // 10 -
																	// monto
								vec_d.addElement(fecha.substring(7) + "/"
										+ fecha.substring(4, 6) + "/"
										+ fecha.substring(0, 4)); // 10 -
																	// fecha
							} else {
								vec_d.addElement(pi.getCodigo()); // 8 -
																	// UNIDAD
								vec_d.addElement(pi.getValuni()); // 9 - COSTO
								vec_d.addElement(pi.getCanman()); // 10 -
																	// CANTIDAD
							}
							vec_d.addElement(pi.getNumreq()); // 11 - NUMREQ
							vec_d.addElement(pi.getMespre()); // 12 - MES
							if (pi.getCanman() > 0 || pi.getPres03() > 0)
								listaReqDetResumen.addElement(vec_d);
						}
					}
				}
			}
		}
		Long totalAnual = en + fe + ma + ab + my + jn + jl + ag + se + oc + no
				+ di;
		vec_d = new Vector();
		vec_d.addElement(13);
		vec_d.addElement("Total Anual");
		vec_d.addElement("");
		vec_d.addElement("");
		vec_d.addElement("");
		vec_d.addElement(totalAnual);
		if (totalAnual > 0)
			listaReqResumen.addElement(vec_d);
		/* fin requerimiento */
		// System.out.println(listaReqDetResumen);
		sesion.setAttribute("listaReqResumen", listaReqResumen);
		sesion.setAttribute("listaReqDetResumen", listaReqDetResumen);

	}

	/* agrega en MAN */
	/*
	 * Presw25DTO presw25 = new Presw25DTO(); presw25.setTippro("MAN");
	 * presw25.setCoduni(unidad); presw25.setItedoc(item);
	 * presw25.setAnopre(anno); presw25.setMespre(mes);
	 * presw25.setNumreq(numreq); presw25.setValpr1(valpr);
	 * presw25.setDesite(desite); presw25.setComen1(area1);
	 * presw25.setComen2(area2); presw25.setDesuni(desuni);
	 * presw25.setTotpre(total); if(total == 0) presw25.setIndmod("D");
	 * if(presupItemreq != null && presupItemreq.size() > 0){ i = -1; boolean
	 * agrega = false; for (Presw25DTO ss : presupItemreq){ i++;
	 * if(ss.getCoduni() == unidad && ss.getItedoc() == item && ss.getMespre() ==
	 * mes && ss.getNumreq() == numreq){ presupItemreq.set(i, presw25); agrega =
	 * true; } } if(!agrega) presupItemreq.add(presw25); } else
	 * presupItemreq.add(presw25);
	 * 
	 * Long totalItemReq = new Long(0); Long ene = new Long(0); Long feb = new
	 * Long(0); Long mar = new Long(0); Long abr = new Long(0); Long may = new
	 * Long(0); Long jun = new Long(0); Long jul = new Long(0); Long ago = new
	 * Long(0); Long sep = new Long(0); Long oct = new Long(0); Long nov = new
	 * Long(0); Long dic = new Long(0);
	 * 
	 * if(listarResultadoPresup.size() > 0){ for (Presw25DTO ss :
	 * presupItemreq){ // agregar todo lo que est� en mantencion al presup
	 * if(ss.getItedoc() == item && ss.getCoduni() == unidad ) { switch
	 * (ss.getMespre()){ case 1: ene = ene + ss.getTotpre(); break; case 2: feb =
	 * feb + ss.getTotpre(); break; case 3: mar = mar + ss.getTotpre(); break;
	 * case 4: abr = abr + ss.getTotpre(); break; case 5: may = may +
	 * ss.getTotpre(); break; case 6: jun = jun + ss.getTotpre(); break; case 7:
	 * jul = jul + ss.getTotpre(); break; case 8: ago = ago + ss.getTotpre();
	 * break; case 9: sep = sep + ss.getTotpre(); break; case 10: oct = oct +
	 * ss.getTotpre(); break; case 11: nov = nov + ss.getTotpre(); break; case
	 * 12: dic = dic + ss.getTotpre(); break; } totalItemReq = totalItemReq +
	 * ss.getTotpre(); } }
	 * 
	 * i =-1; for (Presw25DTO ss : listarResultadoPresup){ i++;
	 * if(ss.getItedoc() == item && ss.getCoduni() == unidad){
	 * ss.setItedoc(item); ss.setPres01(ene); ss.setPres02(feb);
	 * ss.setPres03(mar); ss.setPres04(abr); ss.setPres05(may);
	 * ss.setPres06(jun); ss.setPres07(jul); ss.setPres08(ago);
	 * ss.setPres09(sep); ss.setPres10(oct); ss.setPres11(nov);
	 * ss.setPres12(dic);
	 * ss.setTotpre(Long.parseLong(String.valueOf(ss.getPres01() +
	 * ss.getPres02() + ss.getPres03() + ss.getPres04() + ss.getPres05() +
	 * ss.getPres06() + ss.getPres07() + ss.getPres08() + ss.getPres09() +
	 * ss.getPres10() + ss.getPres11() + ss.getPres12()))); }
	 * listaResultado.add(ss); //System.out.println("result: " +
	 * ss.getTotpre()); }
	 *  }
	 */
	/*
	 * System.out.println("tama�o presupItemreq: "+presupItemreq.size()); for
	 * (Presw25DTO ss : presupItemreq){
	 * 
	 * System.out.println("ss "+ss.getMespre()+"cant " + ss.getPres01()+"req: "+
	 * ss.getNumreq()); System.out.println("ss "+ss.getMespre()+"cant " +
	 * ss.getPres02()+"req: "+ ss.getNumreq()); System.out.println("ss
	 * "+ss.getMespre()+"cant " + ss.getPres03()+"req: "+ ss.getNumreq());
	 *  } System.out.println("presupItemreq desp **: "+presupItemreq.size());
	 */
	/*
	 * if(presupItemreq != null )
	 * sesion.setAttribute("presupItemreq",presupItemreq);
	 * sesion.setAttribute("resultadoDetalleItemReq",listaRQP);
	 * 
	 * sesion.setAttribute("resultadoPresup", listaResultado);
	 */
	// req.setAttribute("totalMes", totalTotal);

	public List<Presw25DTO> getListaControlPlanes(String tipo, int rutUsuario,
			String dv) {
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(dv);

		Collection<Presw25DTO> listaPresw25 = (Collection<Presw25DTO>) preswbean
				.consulta_presw25();
		for (Presw25DTO ss : listaPresw25) {
			lista.add(ss);
		}

		return lista;
	}

	public List<Presw25DTO> getControlPlanes(String tipo, int rutUsuario,
			String dv, int anno) {
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(dv);
		preswbean.setAnopar(anno);

		Collection<Presw25DTO> listaPresw25 = (Collection<Presw25DTO>) preswbean
				.consulta_presw25();
		for (Presw25DTO ss : listaPresw25) {
			lista.add(ss);
		}

		return lista;
	}

	public List<Presw25DTO> getListaActividadesControlPlanes(String tipo,
			int rutUsuario, String dv, int codUni) {
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(dv);
		preswbean.setCajero(codUni + "");

		Collection<Presw25DTO> listaPresw25 = (Collection<Presw25DTO>) preswbean
				.consulta_presw25();
		for (Presw25DTO ss : listaPresw25) {
			lista.add(ss);
		}

		return lista;
	}

	public List<Presw25DTO> getListaPlanesDesarrollo(String tipo,
			int rutUsuario, String dv, String cajero, int codUni) {
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(dv);
		preswbean.setCajero(cajero);
		preswbean.setCoduni(codUni);

		Collection<Presw25DTO> listaPresw25 = (Collection<Presw25DTO>) preswbean
				.consulta_presw25();
		for (Presw25DTO ss : listaPresw25) {
			lista.add(ss);
		}

		return lista;
	}

	public List<Presw25DTO> getActividadesPlanesDesarrollo(String tipo,
			int rutUsuario, String dv, String cajero, int anno) {
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(dv);
		preswbean.setCajero(cajero);
		preswbean.setAnopar(anno);

		Collection<Presw25DTO> listaPresw25 = (Collection<Presw25DTO>) preswbean
				.consulta_presw25();
		for (Presw25DTO ss : listaPresw25) {
			lista.add(ss);
		}

		return lista;
	}

	public List<Cocow36DTO> getListaValorIndicadores(String tipo,
			int rutUsuario, String dv, String indicador, int codUni) {
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();

		Presw19DTO presw19DTO = new Presw19DTO();
		presw19DTO.setTippro(tipo);
		presw19DTO.setRutide(new BigDecimal(rutUsuario));
		presw19DTO.setDigide(dv);
		presw19DTO.setCoduni(new BigDecimal(codUni));

		Presw25DTO presw25DTO = new Presw25DTO();
		presw25DTO.setTippro(tipo);
		presw25DTO.setComen1(indicador);
		presw25DTO.setCoduni(codUni);

		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		List<Cocow36DTO> listaValorIndicadores = new ArrayList<Cocow36DTO>();
		CocowBean cocowBean = new CocowBean();

		listCocow36DTO = cocowBean.buscar_cocow36(presw19DTO, presw25DTO);
		for (Cocow36DTO ss : listCocow36DTO) {
			if (ss.getNomcam().trim().equals("DETIND")) {
				Cocow36DTO cocow36DTO = new Cocow36DTO();
				cocow36DTO.setNomcam(ss.getNomcam());
				cocow36DTO.setValalf(ss.getValalf());// codigo,
				cocow36DTO.setValnu2(ss.getValnu2());// fecha
				cocow36DTO.setResval(ss.getResval());// por
				listaValorIndicadores.add(cocow36DTO);
			}
			if (ss.getNomcam().trim().equals("DETTEX")) {
				Cocow36DTO cocow36DTO = new Cocow36DTO();
				cocow36DTO.setNomcam(ss.getNomcam());
				cocow36DTO.setValalf(ss.getValalf());// INGRESO POR
				cocow36DTO.setValnu2(ss.getValnu2());// fecha CREACION
				cocow36DTO.setResval(ss.getResval());// DESCRIPCION
				listaValorIndicadores.add(cocow36DTO);
			}
			if (ss.getNomcam().trim().equals("DETMET")) {
				Cocow36DTO cocow36DTO = new Cocow36DTO();
				cocow36DTO.setNomcam(ss.getNomcam());
				cocow36DTO.setValalf(ss.getValalf());// INGRESO POR
				cocow36DTO.setValnu2(ss.getValnu2());// fecha CREACION
				cocow36DTO.setResval(ss.getResval());// DESCRIPCION
				listaValorIndicadores.add(cocow36DTO);
			}
		}

		return listaValorIndicadores;
	}

	public List<Cocow36DTO> getListaIndicadores(int rutUsuario, String dv,
			int codCuenta) {
		String nomTipo = "PIA";
		Presw19DTO preswbean19DTO = new Presw19DTO();
		preswbean19DTO.setTippro(nomTipo);
		preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
		preswbean19DTO.setDigide(dv);
		preswbean19DTO.setCoduni(new BigDecimal(codCuenta));
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		List<Cocow36DTO> listaIndicadores = new ArrayList<Cocow36DTO>();
		CocowBean cocowBean = new CocowBean();
		listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
		if (listCocow36DTO != null && listCocow36DTO.size() > 0) {
			for (Cocow36DTO ss : listCocow36DTO) {
				if (ss.getNomcam().trim().equals("DETIND")) {
					Cocow36DTO cocow36DTO = new Cocow36DTO();
					cocow36DTO.setValalf(ss.getValalf());// codigo,
					cocow36DTO.setValnu2(ss.getValnu2());// fecha
					cocow36DTO.setResval(ss.getResval());// por
					listaIndicadores.add(cocow36DTO);
				}
			}
		}
		return listaIndicadores;
	}

	public List<Presw18DTO> getListaEjecucionPorItem(int codCuenta, int anno) {
		String nomTipo = "PAI";
		PreswBean preswbean = null;
		Collection<Presw19DTO> lista = null;
		preswbean = new PreswBean(nomTipo, 0, 0, 0, 0, 0, 0, "", 0, "", "", 0,
				0, 0, 0, "", "");
		preswbean.setCoduni(codCuenta);
		preswbean.setAnopar(anno);
		preswbean.setMespar(12);
		List<Presw18DTO> listaPRESW18 = (List<Presw18DTO>) preswbean
				.consulta_presw18();
		return listaPRESW18;
	}

	public List<Presw18DTO> getListaEjecPorItemDetalle(String nomTipo,
			int codCuenta, int anno, int rutUsuario, String dv, int itedoc) {
		PreswBean preswbean = new PreswBean(nomTipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(dv);
		preswbean.setCoduni(codCuenta);
		preswbean.setAnopar(anno);
		preswbean.setItedoc(itedoc);
		List<Presw18DTO> listaPRESW18 = (List<Presw18DTO>) preswbean
				.consulta_presw18();
		return listaPRESW18;
	}

	public void agregaPlanDesarrolloExcel(HttpServletRequest req,
			Vector<String> lista, int annoControl) {
		HttpSession sesion = req.getSession();
		sesion.setAttribute("planDesarrolloExcel", lista);
		if (annoControl > 0)
			sesion.setAttribute("annoControl", annoControl + "");
	}

	public boolean getVerificaMemo(String tipo, String comen1) {
		PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		preswbean = new PreswBean(tipo, 0, 0, 0, 0, 0, 0, "", 0, "", "", 0, 0,
				0, 0, "", "");
		Presw25DTO presw25 = new Presw25DTO();
		presw25.setTippro(tipo);
		presw25.setComen1(comen1);
		lista = (Collection<Presw18DTO>) preswbean.ingreso_doc_presw25(presw25);
		boolean existe = false;
		for (Presw18DTO ss : lista) {
			if (ss.getItedoc() == 1)
				existe = true;
		}

		return existe;
	}

	public boolean getVerificaCuenta(String tipo, int cuenta) {
		PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		preswbean = new PreswBean(tipo, 0, 0, 0, 0, 0, 0, "", 0, "", "", 0, 0,
				0, 0, "", "");
		preswbean.setCoduni(cuenta);
		lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		boolean existe = false;
		for (Presw18DTO ss : lista) {
			if (!ss.getDesuni().trim().equals(""))
				existe = true;
		}

		return existe;
	}

	public List<Presw18DTO> getVerificaRutCuenta(String tipo, int rut, String dv) {
		PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		preswbean = new PreswBean(tipo, 0, 0, 0, 0, 0, 0, "", 0, "", "", 0, 0,
				0, 0, "", "");
		preswbean.setRutide(rut);
		preswbean.setDigide(dv);
		lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		List<Presw18DTO> listaRut = new ArrayList<Presw18DTO>();
		for (Presw18DTO ss : lista) {
			listaRut.add(ss);
		}

		return listaRut;
	}

	public List<Presw18DTO> getVerificaAsignacionCuenta(String tipo, int rut,
			String dv, int codUni, String tipoIngreso) {
		PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		preswbean = new PreswBean(tipo, 0, 0, 0, 0, 0, 0, "", 0, "", "", 0, 0,
				0, 0, "", "");
		preswbean.setRutide(rut);
		preswbean.setDigide(dv);
		preswbean.setCoduni(codUni);
		preswbean.setTipcue(tipoIngreso);
		lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		List<Presw18DTO> listaRut = new ArrayList<Presw18DTO>();
		for (Presw18DTO ss : lista) {
			listaRut.add(ss);
		}

		return listaRut;
	}

	public void getAgregaListaSolicitaCuenta(HttpServletRequest req,
			int rutnum, String dvrut, String nomFuncionario,
			String tipoIngreso, String nomTipoIngreso) {
		HttpSession sesion = req.getSession();
		List<Cocow36DTO> listaSolicitudes = (List<Cocow36DTO>) sesion
				.getAttribute("listaSolicitudesCuenta");

		if (listaSolicitudes != null && listaSolicitudes.size() > 0) {
			sesion.removeAttribute("listaSolicitudesCuenta");
			sesion.setAttribute("listaSolicitudesCuenta", null);

		}
		if (listaSolicitudes == null)
			listaSolicitudes = new ArrayList<Cocow36DTO>();
		Cocow36DTO cocow36DTO = new Cocow36DTO();
		cocow36DTO.setValnu1(rutnum);
		cocow36DTO.setValalf(dvrut);
		cocow36DTO.setResval(nomFuncionario);
		cocow36DTO.setAccion(tipoIngreso);
		listaSolicitudes.add(cocow36DTO);

		sesion.setAttribute("listaSolicitudesCuenta", listaSolicitudes);

	}

	public void getAgregaListaSolicitaAsignacion(HttpServletRequest req,
			int rutnum, String dvrut, String nomFuncionario,
			String tipoIngreso, String nomTipoIngreso, int cuenta,
			String nomCuenta) {
		HttpSession sesion = req.getSession();
		List<Cocow36DTO> listaSolicitudes = (List<Cocow36DTO>) sesion
				.getAttribute("listaSolicitudesAsignacion");

		if (listaSolicitudes != null && listaSolicitudes.size() > 0) {
			sesion.removeAttribute("listaSolicitudesAsignacion");
			sesion.setAttribute("listaSolicitudesAsignacion", null);

		}
		if (listaSolicitudes == null)
			listaSolicitudes = new ArrayList<Cocow36DTO>();
		Cocow36DTO cocow36DTO = new Cocow36DTO();
		cocow36DTO.setValnu1(rutnum);
		cocow36DTO.setValalf(dvrut);
		cocow36DTO.setResval(nomFuncionario);
		cocow36DTO.setAccion(tipoIngreso);
		cocow36DTO.setValnu2(cuenta);
		listaSolicitudes.add(cocow36DTO);

		sesion.setAttribute("listaSolicitudesAsignacion", listaSolicitudes);

	}

	public void getEliminaListaSolicitaCuenta(HttpServletRequest req) {
		HttpSession sesion = req.getSession();
		int rutide = Util.validaParametro(req.getParameter("rutide"), 0);

		List<Cocow36DTO> listaSolicitudes = (List<Cocow36DTO>) sesion
				.getAttribute("listaSolicitudesCuenta");
		List<Cocow36DTO> listaCuenta = new ArrayList<Cocow36DTO>();
		if (listaSolicitudes != null && listaSolicitudes.size() > 0)
			sesion.removeAttribute("listaSolicitudesCuenta");
		if (listaSolicitudes != null && listaSolicitudes.size() > 0) {
			for (Cocow36DTO ss : listaSolicitudes) {
				if (ss.getValnu1() == rutide)
					continue;
				else {
					Cocow36DTO cocow36DTO = new Cocow36DTO();
					cocow36DTO.setValnu1(ss.getValnu1());
					cocow36DTO.setValalf(ss.getValalf());
					cocow36DTO.setAccion(ss.getAccion());
					cocow36DTO.setResval(ss.getResval());
					listaCuenta.add(cocow36DTO);
				}
			}

		}
		sesion.setAttribute("listaSolicitudesCuenta", listaCuenta);

	}

	public void getEliminaListaSolicitaAsignacion(HttpServletRequest req) {
		HttpSession sesion = req.getSession();
		int rutide = Util.validaParametro(req.getParameter("rutide"), 0);
		int coduni = Util.validaParametro(req.getParameter("coduni"), 0);

		List<Cocow36DTO> listaSolicitudes = (List<Cocow36DTO>) sesion
				.getAttribute("listaSolicitudesAsignacion");
		List<Cocow36DTO> listaCuenta = new ArrayList<Cocow36DTO>();
		if (listaSolicitudes != null && listaSolicitudes.size() > 0)
			sesion.removeAttribute("listaSolicitudesAsignacion");
		if (listaSolicitudes != null && listaSolicitudes.size() > 0) {
			for (Cocow36DTO ss : listaSolicitudes) {
				if ((ss.getValnu1() == rutide) && (ss.getValnu2() == coduni))
					continue;
				else {
					Cocow36DTO cocow36DTO = new Cocow36DTO();
					cocow36DTO.setValnu1(ss.getValnu1());
					cocow36DTO.setValnu2(ss.getValnu2());
					cocow36DTO.setValalf(ss.getValalf());
					cocow36DTO.setAccion(ss.getAccion());
					cocow36DTO.setResval(ss.getResval());
					listaCuenta.add(cocow36DTO);
				}
			}
		}
		sesion.setAttribute("listaSolicitudesAsignacion", listaCuenta);

	}

	public List<Cocow36DTO> getCreaCocow36(HttpServletRequest req,
			int rutUsuario, String dv, String tipo) {
		String numMemo = Util.validaParametro(req.getParameter("numMemo"), "");
		int numCuenta = Util.validaParametro(req.getParameter("numCuenta"), 0);
		String nombreCuenta = Util.validaParametro(req
				.getParameter("nombreCuenta"), "");
		String motiv1 = Util.validaParametro(req
				.getParameter("motivoApertura1"), "");
		String motiv2 = Util.validaParametro(req
				.getParameter("motivoApertura2"), "");
		String motiv3 = Util.validaParametro(req
				.getParameter("motivoApertura3"), "");
		String motiv4 = Util.validaParametro(req
				.getParameter("motivoApertura4"), "");
		int rutnum = Util.validaParametro(req.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(req.getParameter("dvrut"), "");
		int rutnumAut = Util.validaParametro(req.getParameter("rutnumAut"), 0);
		String dvrutAut = Util
				.validaParametro(req.getParameter("dvrutAut"), "");
		List<Cocow36DTO> listaSolicitudes = (List<Cocow36DTO>) req.getSession()
				.getAttribute("listaSolicitudesCuenta");

		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		// String tipo = "GSP";
		String nomcam = "";
		long valnu1 = 0;
		long valnu2 = 0;
		String valalf = "";

		/* prepara archivo para grabar, llenar listCocow36DTO */
		nomcam = "MEMSOL";
		valnu1 = 0;
		valnu2 = 0;
		valalf = numMemo;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "RUTING";
		valnu1 = rutUsuario;
		valnu2 = 0;
		valalf = "";
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "DIGING";
		valnu1 = 0;
		valnu2 = 0;
		valalf = dv;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "RUTAUT";
		valnu1 = rutnumAut;
		valnu2 = 0;
		valalf = "";
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "DIGAUT";
		valnu1 = 0;
		valnu2 = 0;
		valalf = dvrutAut;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "CODUNI";
		valnu1 = numCuenta;
		valnu2 = 0;
		valalf = "";
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "DESUNI";
		valnu1 = 0;
		valnu2 = 0;
		valalf = nombreCuenta;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "RUTRES";
		valnu1 = rutnum;
		valnu2 = 0;
		valalf = "";
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "DIGRES";
		valnu1 = 0;
		valnu2 = 0;
		valalf = dvrut;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "MOTAP1";
		valnu1 = 0;
		valnu2 = 0;
		valalf = motiv1;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "MOTAP2";
		valnu1 = 0;
		valnu2 = 0;
		valalf = motiv2;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "MOTAP3";
		valnu1 = 0;
		valnu2 = 0;
		valalf = motiv3;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "MOTAP4";
		valnu1 = 0;
		valnu2 = 0;
		valalf = motiv4;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		if (listaSolicitudes != null && listaSolicitudes.size() > 0)
			for (Cocow36DTO ss : listaSolicitudes) {
				nomcam = "DETACC";
				valnu1 = ss.getValnu1();
				valnu2 = 0;
				valalf = ss.getValalf();

				Cocow36DTO cocow36DTO = new Cocow36DTO();
				cocow36DTO.setTiping(tipo);
				cocow36DTO.setNomcam(nomcam);
				cocow36DTO.setValnu1(valnu1); // rut acceso
				cocow36DTO.setValalf(valalf); // digito verificador
				cocow36DTO.setResval(ss.getAccion());// accion I/A
				listCocow36DTO.add(cocow36DTO);
			}

		return listCocow36DTO;
	}

	public int getRegistraCuenta(HttpServletRequest req, int rutide,
			String digide, String tipo, int numDoc) {
		int numCuenta = 0;
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		listCocow36DTO = this.getCreaCocow36(req, rutide, digide, tipo);
		Ingreso_Documento ingresoDocumento = new Ingreso_Documento(tipo,
				rutide, digide);
		ingresoDocumento.setNumdoc(numDoc);

		numCuenta = ingresoDocumento.Ingresar_Documento(listCocow36DTO);

		return numCuenta;
	}

	public List<Cocow36DTO> getAgregaLista(List<Cocow36DTO> lista,
			String tiping, String nomcam, long valnu1, long valnu2,
			String valalf) {
		Cocow36DTO cocow36DTO = new Cocow36DTO();
		cocow36DTO.setTiping(tiping);
		cocow36DTO.setNomcam(nomcam);
		cocow36DTO.setValnu1(valnu1);
		cocow36DTO.setValnu2(valnu2);
		cocow36DTO.setValalf(valalf);
		lista.add(cocow36DTO);
		return lista;

	}

	/* solicitudes de cuenta */

	public List<Presw18DTO> getListaPresw18(String tipo, int rutUsuario,
			String dv) {
		List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
		Presw18DTO presw18DTO = new Presw18DTO();
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(dv);

		Collection<Presw18DTO> listaPresw18 = (Collection<Presw18DTO>) preswbean
				.consulta_presw18();
		for (Presw18DTO ss : listaPresw18) {
			presw18DTO = new Presw18DTO();
			presw18DTO.setNumdoc(ss.getNumdoc());
			presw18DTO.setNomtip(ss.getNomtip());
			presw18DTO.setCoduni(ss.getCoduni());
			presw18DTO.setDesuni(ss.getDesuni());
			presw18DTO.setDesite(ss.getDesite());
			presw18DTO.setNompro(ss.getNompro());
			presw18DTO.setFecdoc(ss.getFecdoc());
			presw18DTO.setRutide(ss.getRutide());
			presw18DTO.setIndpro(ss.getIndpro());
			lista.add(presw18DTO);
		}

		return lista;
	}

	public void agregaSesion(HttpServletRequest req, List<Presw18DTO> lista,
			String nomLista) {
		// public void agregaUnidad(HttpServletRequest req, Collection<Presw18>
		// lista){
		HttpSession sesion = req.getSession();

		sesion.setAttribute(nomLista, lista);

	}

	public boolean saveRecepciona(int numdoc, int rutUsuario, String digide,
			String tipo, String accion) {
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(digide);
		preswbean.setNumdoc(numdoc);
		if (!accion.trim().equals(""))
			preswbean.setTipcue(accion);

		return preswbean.ingreso_presw19();
	}

	public boolean saveRechazaCuenta(int numdoc, int rutUsuario, String digide,
			String tipo, String glosa) {
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(digide);
		preswbean.setNumdoc(numdoc);
		preswbean.ingreso_presw19();

		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		presw25DTO.setPres01(numdoc);
		presw25DTO.setComen1(glosa);
		presw25DTO.setRutide(rutUsuario);
		presw25DTO.setDigide(digide);

		lista.add(presw25DTO);
		return preswbean.ingreso_presw25((List<Presw25DTO>) lista);
	}

	public List<TipoUnidad> getTipoUnidad() {
		TipoUnidad tipoUnidad = new TipoUnidad();
		List<TipoUnidad> listTipoUnidad = new ArrayList<TipoUnidad>();
		tipoUnidad = new TipoUnidad();
		tipoUnidad.setCodigo("O");
		tipoUnidad.setNombre("Operaci�n");
		listTipoUnidad.add(tipoUnidad);

		tipoUnidad = new TipoUnidad();
		tipoUnidad.setCodigo("R");
		tipoUnidad.setNombre("Remuneraci�n");
		listTipoUnidad.add(tipoUnidad);

		tipoUnidad = new TipoUnidad();
		tipoUnidad.setCodigo("E");
		tipoUnidad.setNombre("Personal");
		listTipoUnidad.add(tipoUnidad);

		tipoUnidad = new TipoUnidad();
		tipoUnidad.setCodigo("I");
		tipoUnidad.setNombre("Inversi�n");
		listTipoUnidad.add(tipoUnidad);

		tipoUnidad = new TipoUnidad();
		tipoUnidad.setCodigo("P");
		tipoUnidad.setNombre("Postgrado e Investigaci�n");
		listTipoUnidad.add(tipoUnidad);

		tipoUnidad = new TipoUnidad();
		tipoUnidad.setCodigo("S");
		tipoUnidad.setNombre("Plan Estrat�gico");
		listTipoUnidad.add(tipoUnidad);

		return listTipoUnidad;

	}

	public List<TipoCuenta> getTipoCuenta() {
		TipoCuenta tipoCuenta = new TipoCuenta();
		List<TipoCuenta> listTipoCuenta = new ArrayList<TipoCuenta>();

		tipoCuenta = new TipoCuenta();
		tipoCuenta.setCodigo("P");
		tipoCuenta.setNombre("Presupuesto");
		listTipoCuenta.add(tipoCuenta);

		tipoCuenta = new TipoCuenta();
		tipoCuenta.setCodigo("I");
		tipoCuenta.setNombre("Ingresos propios");
		listTipoCuenta.add(tipoCuenta);

		tipoCuenta = new TipoCuenta();
		tipoCuenta.setCodigo("Y");
		tipoCuenta.setNombre("Proyectos");
		listTipoCuenta.add(tipoCuenta);

		tipoCuenta = new TipoCuenta();
		tipoCuenta.setCodigo("T");
		tipoCuenta.setNombre("Traspasos");
		listTipoCuenta.add(tipoCuenta);

		tipoCuenta = new TipoCuenta();
		tipoCuenta.setCodigo("V");
		tipoCuenta.setNombre("Valores de Cobro");
		listTipoCuenta.add(tipoCuenta);

		return listTipoCuenta;
	}

	public List<TipoPresupuesto> getTipoPresupuesto() {
		TipoPresupuesto tipoPresupuesto = new TipoPresupuesto();
		List<TipoPresupuesto> listTipoPresupuesto = new ArrayList<TipoPresupuesto>();

		tipoPresupuesto = new TipoPresupuesto();
		tipoPresupuesto.setCodigo("C");
		tipoPresupuesto.setNombre("Continuidad Operacional");
		listTipoPresupuesto.add(tipoPresupuesto);

		tipoPresupuesto = new TipoPresupuesto();
		tipoPresupuesto.setCodigo("P");
		tipoPresupuesto.setNombre("Programa Anual ");
		listTipoPresupuesto.add(tipoPresupuesto);

		tipoPresupuesto = new TipoPresupuesto();
		tipoPresupuesto.setCodigo("D");
		tipoPresupuesto.setNombre("Plan de Desarrollo");
		listTipoPresupuesto.add(tipoPresupuesto);

		tipoPresupuesto = new TipoPresupuesto();
		tipoPresupuesto.setCodigo("R");
		tipoPresupuesto.setNombre("Remuneraci�n");
		listTipoPresupuesto.add(tipoPresupuesto);

		return listTipoPresupuesto;
	}

	public List<Cocow36DTO> getCreaCocow36Asignacion(HttpServletRequest req,
			int rutUsuario, String dv, String tipo) {
		String numMemo = Util.validaParametro(req.getParameter("numMemo"), "");
		String comentario = Util.validaParametro(
				req.getParameter("comentario"), "");
		List<Cocow36DTO> listaSolicitudes = (List<Cocow36DTO>) req.getSession()
				.getAttribute("listaSolicitudesAsignacion");

		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();

		String nomcam = "";
		long valnu1 = 0;
		long valnu2 = 0;
		String valalf = "";

		/* prepara archivo para grabar, llenar listCocow36DTO */
		nomcam = "MEMSOL";
		valnu1 = 0;
		valnu2 = 0;
		valalf = numMemo;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "RUTING";
		valnu1 = rutUsuario;
		valnu2 = 0;
		valalf = "";
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "DIGING";
		valnu1 = 0;
		valnu2 = 0;
		valalf = dv;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "COMENT";
		valnu1 = 0;
		valnu2 = 0;
		valalf = comentario;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		for (Cocow36DTO ss : listaSolicitudes) {
			nomcam = "DETACC";
			valnu1 = ss.getValnu1();
			valnu2 = ss.getValnu2();
			valalf = ss.getValalf();

			Cocow36DTO cocow36DTO = new Cocow36DTO();
			cocow36DTO.setTiping(tipo);
			cocow36DTO.setNomcam(nomcam);
			cocow36DTO.setValnu1(valnu1); // rut acceso
			cocow36DTO.setValnu2(valnu2);// cuenta
			cocow36DTO.setValalf(valalf); // digito verificador
			cocow36DTO.setResval(ss.getAccion());// accion I/A
			listCocow36DTO.add(cocow36DTO);
		}

		return listCocow36DTO;
	}

	public List<Cocow36DTO> getCreaCocow36RechazaAsignacion(
			HttpServletRequest req, int rutUsuario, String dv, String tipo) {
		String numDoc = Util.validaParametro(req.getParameter("numDoc"), "");
		String motivoRechazo = Util.validaParametro(req
				.getParameter("motivoRechazo"), "");

		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();

		String nomcam = "";
		long valnu1 = 0;
		long valnu2 = 0;
		String valalf = "";

		/* prepara archivo para grabar, llenar listCocow36DTO */
		nomcam = "COMENT";
		valnu1 = 0;
		valnu2 = 0;
		valalf = motivoRechazo;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		return listCocow36DTO;
	}

	public List<Cocow36DTO> getCreaCocow36RechazaConfirmacion(
			HttpServletRequest req, int rutUsuario, String dv, String tipo) {
		String numDoc = Util.validaParametro(req.getParameter("numDoc"), "");
		String motivoRechazo1 = Util.validaParametro(req
				.getParameter("motivoRechazo1"), "");
		String motivoRechazo2 = Util.validaParametro(req
				.getParameter("motivoRechazo2"), "");
		String motivoRechazo3 = Util.validaParametro(req
				.getParameter("motivoRechazo3"), "");
		String motivoRechazo4 = Util.validaParametro(req
				.getParameter("motivoRechazo4"), "");
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();

		String nomcam = "";
		long valnu1 = 0;
		long valnu2 = 0;
		String valalf = "";
		if (!motivoRechazo1.trim().equals("")) {
			/* prepara archivo para grabar, llenar listCocow36DTO */
			nomcam = "COMEN1";
			valnu1 = 0;
			valnu2 = 0;
			valalf = motivoRechazo1;
			listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam,
					valnu1, valnu2, valalf);

			nomcam = "COMEN2";
			valnu1 = 0;
			valnu2 = 0;
			valalf = motivoRechazo2;
			listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam,
					valnu1, valnu2, valalf);

			nomcam = "COMEN3";
			valnu1 = 0;
			valnu2 = 0;
			valalf = motivoRechazo3;
			listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam,
					valnu1, valnu2, valalf);

			nomcam = "COMEN4";
			valnu1 = 0;
			valnu2 = 0;
			valalf = motivoRechazo4;
			listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam,
					valnu1, valnu2, valalf);
		}

		return listCocow36DTO;
	}

	public int getRegistraAsignacion(HttpServletRequest req, int rutide,
			String digide, String tipo) {
		int numCuenta = 0;
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		listCocow36DTO = this.getCreaCocow36Asignacion(req, rutide, digide,
				tipo);
		Ingreso_Documento ingresoDocumento = new Ingreso_Documento(tipo,
				rutide, digide);

		numCuenta = ingresoDocumento.Ingresar_Documento(listCocow36DTO);

		return numCuenta;
	}

	public int getApruebaRehazaAsignacion(HttpServletRequest req, int rutide,
			String digide, String tipo, int numDoc, String tipcue) {
		int numCuenta = 0;
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		listCocow36DTO = this.getCreaCocow36RechazaAsignacion(req, rutide,
				digide, tipo);
		Ingreso_Documento ingresoDocumento = new Ingreso_Documento(tipo,
				rutide, digide);
		ingresoDocumento.setNumdoc(numDoc);
		ingresoDocumento.setTipcue(tipcue);
		numCuenta = ingresoDocumento.Ingresar_Documento(listCocow36DTO);

		return numCuenta;
	}

	public int getApruebaRehaza(HttpServletRequest req, int rutide,
			String digide, String tipo, int numDoc, String tipcue) {
		int numCuenta = 0;
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		listCocow36DTO = this.getCreaCocow36RechazaAsignacion(req, rutide,
				digide, tipo);
		Ingreso_Documento ingresoDocumento = new Ingreso_Documento(tipo,
				rutide, digide);
		ingresoDocumento.setNumdoc(numDoc);
		ingresoDocumento.setTipcue(tipcue);
		numCuenta = ingresoDocumento.Ingresar_Documento(listCocow36DTO);

		return numCuenta;
	}

	public int getApruebaRehazaDIR(HttpServletRequest req, int rutide,
			String digide, String tipo, int numDoc) {
		int numCuenta = 0;
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		listCocow36DTO = this.getCreaCocow36RechazaConfirmacion(req, rutide,
				digide, tipo);
		Ingreso_Documento ingresoDocumento = new Ingreso_Documento(tipo,
				rutide, digide);
		ingresoDocumento.setNumdoc(numDoc);
		numCuenta = ingresoDocumento.Ingresar_Documento(listCocow36DTO);

		return numCuenta;
	}

	/* historial DIPRES */
	public List<Presw18DTO> getListaHistorial(String tipo, int rutUsuario,
			String dv, int codUni, int numdoc, int numcom) {
		List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
		Presw18DTO presw18DTO = new Presw18DTO();
		PreswBean preswbean = new PreswBean(tipo, 0, 0, 0, 0, rutUsuario);
		preswbean.setDigide(dv);
		preswbean.setCoduni(codUni);
		preswbean.setNumdoc(numdoc);
		preswbean.setNumcom(numcom);

		Collection<Presw18DTO> listaPresw18 = (Collection<Presw18DTO>) preswbean
				.consulta_presw18();
		for (Presw18DTO ss : listaPresw18) {
			presw18DTO = new Presw18DTO();
			presw18DTO.setNumdoc(ss.getNumdoc()); // numero solicitud
			presw18DTO.setNomtip(ss.getNomtip()); // tipo solicitud
			presw18DTO.setFecdoc(ss.getFecdoc()); // fecha solicitud
			presw18DTO.setCoduni(ss.getCoduni()); // cod unidad
			presw18DTO.setDesuni(ss.getDesuni()); // nom unidad
			presw18DTO.setRutide(ss.getRutide()); // rut solicitante
			presw18DTO.setIndpro(ss.getIndpro()); // digito solicitante
			presw18DTO.setDesite(ss.getDesite()); // nombre solicitante
			presw18DTO.setNompro(ss.getNompro()); // estado solicitud
			lista.add(presw18DTO);
		}

		return lista;
	}

	public int getRegistraAprobacionDipres(HttpServletRequest req, int rutide,
			String digide, String tipo) {
		int numCuenta = 0;
		int numDoc = Util.validaParametro(req.getParameter("numDoc"), 0);

		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		listCocow36DTO = this.getCreaCocow36AprobacionDipres(req, rutide,
				digide, tipo);
		Ingreso_Documento ingresoDocumento = new Ingreso_Documento(tipo,
				rutide, digide);
		ingresoDocumento.setNumdoc(numDoc);
		numCuenta = ingresoDocumento.Ingresar_Documento(listCocow36DTO);

		return numCuenta;
	}

	public List<Cocow36DTO> getCreaCocow36AprobacionDipres(
			HttpServletRequest req, int rutUsuario, String dv, String tipo) {
		int numDoc = Util.validaParametro(req.getParameter("numDoc"), 0);
		int numCuenta = Util.validaParametro(req.getParameter("numCuenta"), 0);
		String nombreCuenta = Util.validaParametro(req
				.getParameter("nombreCuenta"), "");
		int sedeUnidad = Util
				.validaParametro(req.getParameter("sedeUnidad"), 1);
		String tipoUnidad = Util.validaParametro(
				req.getParameter("tipoUnidad"), "");
		String ingresoDatos = Util.validaParametro(req
				.getParameter("ingresoDatos"), "");
		String unidadMadre = Util.validaParametro(req
				.getParameter("unidadMadre"), "");
		String tipoPresupuesto = Util.validaParametro(req
				.getParameter("tipoPresupuesto"), "");
		long valorCobro = Util.validaParametro(req.getParameter("valorCobro"),
				0);
		String tipoCuenta = Util.validaParametro(
				req.getParameter("tipoCuenta"), "");

		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		List<Cocow36DTO> listaSolicitudesCuenta = new ArrayList<Cocow36DTO>();
		listaSolicitudesCuenta = (List<Cocow36DTO>) req.getSession()
				.getAttribute("listaSolicitudesCuenta");

		String nomcam = "";
		long valnu1 = 0;
		long valnu2 = 0;
		String valalf = "";

		/* prepara archivo para grabar, llenar listCocow36DTO */
		nomcam = "CODUNI";
		valnu1 = numCuenta;
		valnu2 = 0;
		valalf = "";
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "DESUNI";
		valnu1 = 0;
		valnu2 = 0;
		valalf = nombreCuenta;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "CODSED";
		valnu1 = sedeUnidad;
		valnu2 = 0;
		valalf = "";
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "OPEREM";
		valnu1 = 0;
		valnu2 = 0;
		valalf = tipoUnidad;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "INGDAT";
		valnu1 = 0;
		valnu2 = 0;
		valalf = ingresoDatos;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "CASING";
		valnu1 = 0;
		valnu2 = 0;
		valalf = unidadMadre;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "INDAUT";
		valnu1 = 0;
		valnu2 = 0;
		valalf = tipoPresupuesto;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "CODAG2";
		valnu1 = valorCobro;
		valnu2 = 0;
		valalf = "";
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "TIPCTA";
		valnu1 = 0;
		valnu2 = 0;
		valalf = tipoCuenta;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tipo, nomcam, valnu1,
				valnu2, valalf);

		if (listaSolicitudesCuenta != null && listaSolicitudesCuenta.size() > 0) {
			for (Cocow36DTO ss : listaSolicitudesCuenta) {
				nomcam = "DETACC";
				valnu1 = ss.getValnu1();
				valnu2 = 0;
				valalf = ss.getValalf();

				Cocow36DTO cocow36DTO = new Cocow36DTO();
				cocow36DTO.setTiping(tipo);
				cocow36DTO.setNomcam(nomcam);
				cocow36DTO.setValnu1(valnu1); // rut acceso
				cocow36DTO.setValalf(valalf); // digito verificador
				cocow36DTO.setResval(ss.getAccion());// accion I/A
				listCocow36DTO.add(cocow36DTO);
			}
		}

		return listCocow36DTO;
	}

	public String formateoNumeroSinDecimales(Double numero) {
		/* formatear n�mero */
		Locale l = new Locale("es", "CL");
		DecimalFormat formatter1 = (DecimalFormat) DecimalFormat.getInstance(l);
		formatter1.applyPattern("###,###,###");

		// formatter1.format(rs.getDouble("RUTNUM")) /*sin decimales*/
		return formatter1.format(numero);
	}

	public Collection<Pres18POJO> getCuentas(String rutnumdv) {
		//System.out.println(rutnumdv);
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		Calendar fecha = Calendar.getInstance();
		int a�o = fecha.get(Calendar.YEAR);
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		try {
			String sql = ""
					+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , (SUM(fg.FGBOPAL_00_adopt_bud + fg.fgbopal_00_bud_adjt +fg.FGBOPAL_01_adopt_bud + fg.fgbopal_01_bud_adjt + fg.FGBOPAL_02_adopt_bud + fg.fgbopal_02_bud_adjt + fg.FGBOPAL_03_adopt_bud + fg.fgbopal_03_bud_adjt + "
					+ " fg.FGBOPAL_04_adopt_bud + fg.fgbopal_04_bud_adjt + fg.FGBOPAL_05_adopt_bud + fg.fgbopal_05_bud_adjt + fg.FGBOPAL_06_adopt_bud + fg.fgbopal_06_bud_adjt + fg.FGBOPAL_07_adopt_bud + fg.fgbopal_07_bud_adjt + "
					+ " fg.FGBOPAL_08_adopt_bud + fg.fgbopal_08_bud_adjt + fg.FGBOPAL_09_adopt_bud + fg.fgbopal_09_bud_adjt + fg.FGBOPAL_10_adopt_bud + fg.fgbopal_10_bud_adjt + fg.FGBOPAL_11_adopt_bud + fg.fgbopal_11_bud_adjt + fg.FGBOPAL_12_adopt_bud + fg.fgbopal_12_bud_adjt) - (SUM(fg.fgbopal_00_ytd_actv + fg.fgbopal_00_encumb + fg.fgbopal_00_bud_rsrv + "
					+ " fg.fgbopal_01_ytd_actv + fg.fgbopal_01_encumb + fg.fgbopal_01_bud_rsrv + fg.fgbopal_02_ytd_actv + fg.fgbopal_02_encumb + fg.fgbopal_02_bud_rsrv + fg.fgbopal_03_ytd_actv + fg.fgbopal_03_encumb + fg.fgbopal_03_bud_rsrv +"
					+ " fg.fgbopal_04_ytd_actv + fg.fgbopal_04_encumb + fg.fgbopal_04_bud_rsrv + fg.fgbopal_05_ytd_actv + fg.fgbopal_05_encumb + fg.fgbopal_05_bud_rsrv + fg.fgbopal_06_ytd_actv + fg.fgbopal_06_encumb + fg.fgbopal_06_bud_rsrv +"
					+ " fg.fgbopal_07_ytd_actv + fg.fgbopal_07_encumb + fg.fgbopal_07_bud_rsrv +fg.fgbopal_08_ytd_actv + fg.fgbopal_08_encumb + fg.fgbopal_08_bud_rsrv + fg.fgbopal_09_ytd_actv + fg.fgbopal_09_encumb + fg.fgbopal_09_bud_rsrv +"
					+ " fg.fgbopal_10_ytd_actv + fg.fgbopal_10_encumb + fg.fgbopal_10_bud_rsrv +fg.fgbopal_11_ytd_actv + fg.fgbopal_11_encumb + fg.fgbopal_11_bud_rsrv + fg.fgbopal_12_ytd_actv + fg.fgbopal_12_encumb + fg.fgbopal_12_bud_rsrv))) saldo , ff.ftvfund_title  "
					+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff "
					+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
					+ a�osql
					+ "'  "
					+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title ";
			//System.out.println("Combobox sql " + sql);
			//System.out.println("Se va a conectar ");
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			//System.out.println("Conecto ");
			while (res.next()) {
				Pres18POJO ss = new Pres18POJO();
				// String aux = res.getString(1);
				ss.setCoduni(res.getString(1)); // nombre + apellidos
				ss.setDesuni(res.getString(2));
				ss.setAcumum(res.getLong(3));

				ss.setNomtip(res.getString(4));
				lista.add(ss);
			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error moduloPresupuestoB.getCuentas: "
					+ e.getMessage());
		}
		return lista;
	}
/*
	public Collection<Pres18POJO> getCuentasVigentes_Respaldo(String rut) {

		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		Calendar fecha = Calendar.getInstance();
		int a�o = fecha.get(Calendar.YEAR);
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		String nomusuario = "";
		try {
			nomusuario = getPIDM(rut);
		} catch (Exception ex) {

		}
		try {
			String sql = ""
					+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , (SUM(fg.FGBOPAL_00_adopt_bud + fg.fgbopal_00_bud_adjt +fg.FGBOPAL_01_adopt_bud + fg.fgbopal_01_bud_adjt + fg.FGBOPAL_02_adopt_bud + fg.fgbopal_02_bud_adjt + fg.FGBOPAL_03_adopt_bud + fg.fgbopal_03_bud_adjt + "
					+ " fg.FGBOPAL_04_adopt_bud + fg.fgbopal_04_bud_adjt + fg.FGBOPAL_05_adopt_bud + fg.fgbopal_05_bud_adjt + fg.FGBOPAL_06_adopt_bud + fg.fgbopal_06_bud_adjt + fg.FGBOPAL_07_adopt_bud + fg.fgbopal_07_bud_adjt + "
					+ " fg.FGBOPAL_08_adopt_bud + fg.fgbopal_08_bud_adjt + fg.FGBOPAL_09_adopt_bud + fg.fgbopal_09_bud_adjt + fg.FGBOPAL_10_adopt_bud + fg.fgbopal_10_bud_adjt + fg.FGBOPAL_11_adopt_bud + fg.fgbopal_11_bud_adjt + fg.FGBOPAL_12_adopt_bud + fg.fgbopal_12_bud_adjt) - (SUM(fg.fgbopal_00_ytd_actv + fg.fgbopal_00_encumb + fg.fgbopal_00_bud_rsrv + "
					+ " fg.fgbopal_01_ytd_actv + fg.fgbopal_01_encumb + fg.fgbopal_01_bud_rsrv + fg.fgbopal_02_ytd_actv + fg.fgbopal_02_encumb + fg.fgbopal_02_bud_rsrv + fg.fgbopal_03_ytd_actv + fg.fgbopal_03_encumb + fg.fgbopal_03_bud_rsrv +"
					+ " fg.fgbopal_04_ytd_actv + fg.fgbopal_04_encumb + fg.fgbopal_04_bud_rsrv + fg.fgbopal_05_ytd_actv + fg.fgbopal_05_encumb + fg.fgbopal_05_bud_rsrv + fg.fgbopal_06_ytd_actv + fg.fgbopal_06_encumb + fg.fgbopal_06_bud_rsrv +"
					+ " fg.fgbopal_07_ytd_actv + fg.fgbopal_07_encumb + fg.fgbopal_07_bud_rsrv +fg.fgbopal_08_ytd_actv + fg.fgbopal_08_encumb + fg.fgbopal_08_bud_rsrv + fg.fgbopal_09_ytd_actv + fg.fgbopal_09_encumb + fg.fgbopal_09_bud_rsrv +"
					+ " fg.fgbopal_10_ytd_actv + fg.fgbopal_10_encumb + fg.fgbopal_10_bud_rsrv +fg.fgbopal_11_ytd_actv + fg.fgbopal_11_encumb + fg.fgbopal_11_bud_rsrv + fg.fgbopal_12_ytd_actv + fg.fgbopal_12_encumb + fg.fgbopal_12_bud_rsrv))) saldo , ff.ftvfund_title  "
					+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff ,forusor foru ,fobprof fob "
					+ " where fob.fobprof_user_id =   foru.forusor_USER_ID_ENTERED AND foru.forusor_USER_ID_ENTERED LIKE '%"
					+ nomusuario
					+ "%' and ft.FTVORGN_STATUS_IND = 'A' AND ft.FTVORGN_DATA_ENTRY_IND = 'Y' AND fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
					+ a�osql
					+ "'  "
					+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title ";
			//System.out.println("Combobox sql " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();

			while (res.next()) {
				Pres18POJO ss = new Pres18POJO();
				// String aux = res.getString(1);
				ss.setCoduni(res.getString(1)); // nombre + apellidos
				ss.setDesuni(res.getString(2));
				ss.setAcumum(res.getLong(3));

				ss.setNomtip(res.getString(4));
				lista.add(ss);
			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error moduloPresupuestoB.getCuentasVigentes_Respaldo: "
					+ e.getMessage());
		}
		return lista;
	}
*/
	/*
	public Collection<Pres18POJO> getCuentasVigentes_RespaldoV2(String rut) {

		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;
		//System.out.println("Entra a  getCuentasVigentes");
		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		Calendar fecha = Calendar.getInstance();
		int a�o = fecha.get(Calendar.YEAR);
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		String nomusuario = "";
		try {
			nomusuario = getPIDM(rut);
		} catch (Exception ex) {

		}
		try {
			String sql = ""
					+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , (SUM(fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT) - SUM(fg.FGBOPAL_12_YTD_ACTV + fg.FGBOPAL_12_ENCUMB + fg.FGBOPAL_12_BUD_RSRV)) saldo , ff.ftvfund_title  "
					+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff ,forusor foru ,fobprof fob "
					+ " where fob.fobprof_user_id =   foru.forusor_USER_ID_ENTERED AND foru.forusor_USER_ID_ENTERED LIKE '%"
					+ nomusuario
					+ "%' and ft.FTVORGN_STATUS_IND = 'A' AND ft.FTVORGN_DATA_ENTRY_IND = 'Y' AND fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
					+ a�osql
					+ "'  "
					+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title ORDER BY FGBOPAL_ORGN_CODE";
			//System.out.println("Combobox sql v2" + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();

			while (res.next()) {
				Pres18POJO ss = new Pres18POJO();
				// String aux = res.getString(1);
				ss.setCoduni(res.getString(1)); // nombre + apellidos
				ss.setDesuni(res.getString(2));
				ss.setAcumum(res.getLong(3));

				ss.setNomtip(res.getString(4));
				lista.add(ss);
			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error moduloPresupuestoB.getCuentasVigentes_RespaldoV2: "
					+ e.getMessage());
		}
		return lista;
	}
*/
	public Collection<Pres18POJO> getCuentasVigentes(String rut) {

		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;
		//System.out.println("Entra a  getCuentasVigentes");
		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		Calendar fecha = Calendar.getInstance();
		int a�o = fecha.get(Calendar.YEAR);
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		String nomusuario = "";
		try {
			nomusuario = getPIDM(rut);
		} catch (Exception ex) {

		}
		String mes_query = "";
		int mes = fecha.get(Calendar.MONTH);

		try {
			if (mes == 0) {
				mes_query = " (SUM(fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_00_BUD_ADJT) - SUM(fg.FGBOPAL_00_YTD_ACTV + fg.FGBOPAL_00_ENCUMB + fg.FGBOPAL_00_BUD_RSRV)) saldo ,";
			} else if (mes == 1) {
				mes_query = " (SUM(fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_01_BUD_ADJT) - SUM(fg.FGBOPAL_01_YTD_ACTV + fg.FGBOPAL_01_ENCUMB + fg.FGBOPAL_01_BUD_RSRV)) saldo ,";
			} else if (mes == 2) {
				mes_query = " (SUM(fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT) - SUM(fg.FGBOPAL_02_YTD_ACTV + fg.FGBOPAL_02_ENCUMB + fg.FGBOPAL_02_BUD_RSRV)) saldo ,";
			} else if (mes == 3) {
				mes_query = " (SUM(fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT) - SUM(fg.FGBOPAL_03_YTD_ACTV + fg.FGBOPAL_03_ENCUMB + fg.FGBOPAL_03_BUD_RSRV)) saldo ,";
			} else if (mes == 4) {
				mes_query = " (SUM(fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT) - SUM(fg.FGBOPAL_04_YTD_ACTV + fg.FGBOPAL_04_ENCUMB + fg.FGBOPAL_04_BUD_RSRV)) saldo ,";
			} else if (mes == 5) {
				mes_query = " (SUM(fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT) - SUM(fg.FGBOPAL_05_YTD_ACTV + fg.FGBOPAL_05_ENCUMB + fg.FGBOPAL_05_BUD_RSRV)) saldo ,";
			} else if (mes == 6) {
				mes_query = " (SUM(fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT) - SUM(fg.FGBOPAL_06_YTD_ACTV + fg.FGBOPAL_06_ENCUMB + fg.FGBOPAL_06_BUD_RSRV)) saldo ,";
			} else if (mes == 7) {
				mes_query = " (SUM(fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT) - SUM(fg.FGBOPAL_07_YTD_ACTV + fg.FGBOPAL_07_ENCUMB + fg.FGBOPAL_07_BUD_RSRV)) saldo ,";
			} else if (mes == 8) {
				mes_query = " (SUM(fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT) - SUM(fg.FGBOPAL_08_YTD_ACTV + fg.FGBOPAL_08_ENCUMB + fg.FGBOPAL_08_BUD_RSRV)) saldo ,";
			} else if (mes == 9) {
				mes_query = " (SUM(fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT) - SUM(fg.FGBOPAL_09_YTD_ACTV + fg.FGBOPAL_09_ENCUMB + fg.FGBOPAL_09_BUD_RSRV)) saldo ,";
			} else if (mes == 10) {
				mes_query = " (SUM(fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT) - SUM(fg.FGBOPAL_10_YTD_ACTV + fg.FGBOPAL_10_ENCUMB + fg.FGBOPAL_10_BUD_RSRV)) saldo ,";
			} else if (mes == 11) {
				mes_query = " (SUM(fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT) - SUM(fg.FGBOPAL_11_YTD_ACTV + fg.FGBOPAL_11_ENCUMB + fg.FGBOPAL_11_BUD_RSRV)) saldo ,";
			} else if (mes == 12) {
				mes_query = " (SUM(fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT) - SUM(fg.FGBOPAL_12_YTD_ACTV + fg.FGBOPAL_12_ENCUMB + fg.FGBOPAL_12_BUD_RSRV)) saldo ,";
			}

			String sql = ""
					+ " SELECT DISTINCT fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
					+ mes_query
					+ " ff.ftvfund_title "
					+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff ,forusor foru ,fobprof fob, USM_ORG_ACCESO acc "
					+ " where fob.fobprof_user_id =   foru.forusor_USER_ID_ENTERED AND foru.forusor_USER_ID_ENTERED LIKE '%"
					+ nomusuario
					+ "%' AND foru.forusor_USER_ID_ENTERED = acc.ORGACCESO_USER_ID_ENTERED and ft.FTVORGN_STATUS_IND = 'A' AND ft.FTVORGN_DATA_ENTRY_IND = 'Y' AND fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
					+ a�osql
					+ "'  AND fg.FGBOPAL_ORGN_CODE = acc.ORGACCESO_ORGN_CODE "
					+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title ";
			//System.out.println("Combobox sql " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();

			while (res.next()) {
				Pres18POJO ss = new Pres18POJO();
				// String aux = res.getString(1);
				ss.setCoduni(res.getString(1)); // nombre + apellidos
				ss.setDesuni(res.getString(2));
				ss.setAcumum(res.getLong(3));

				ss.setNomtip(res.getString(4));
				lista.add(ss);
			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error moduloPresupuestoB.getCuentasVigentes: "
					+ e.getMessage());
		}
		return lista;
	}
/*
	public Collection<Presw21POJO> getRemuneraciones_RESPALDO2311(int a�oIN, String org,
			String estamento) {
		//System.out.println("Entra a getReumuneraciones valor mes ");
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Presw21POJO> lista = new ArrayList<Presw21POJO>();
		Calendar fecha = Calendar.getInstance();
		String a�osql;
		if (a�oIN == 0) {
			int a�o = fecha.get(Calendar.YEAR);
			a�osql = String.valueOf(a�o);
			a�osql = a�osql.substring(2);
		} else {
			int a�o = a�oIN;
			a�osql = String.valueOf(a�o);
			a�osql = a�osql.substring(2);
			a�o = 0;
			a�o = Integer.parseInt(a�osql);
		}
		try {
			String condicion;
			String sql = "";
			if (estamento.equals("T") || (estamento.isEmpty())) {
				condicion = " AND FGBOPAL_ACCT_CODE IN ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006')	  ";
			} else {
				condicion = " AND FGBOPAL_ACCT_CODE  = '" + estamento + "'";
			}
			sql = ""

					+ "	SELECT 	"
					+ "	  SUM(fg.FGBOPAL_00_adopt_bud + fg.fgbopal_00_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_00_ytd_actv  + fg.fgbopal_00_encumb + fg.fgbopal_00_bud_rsrv) GASTOS ,	"
					+ " SUM((FGBOPAL_00_ADOPT_BUD ) + (FGBOPAL_00_BUD_ADJT ) - FGBOPAL_00_YTD_ACTV )saldo_mes ,"
					+ "	  00 Mes	" + "	FROM FGBOPAL fg ,	" + "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ condicion
					+ "	GROUP BY 00"
					+ "	  UNION ALL	"
					+ "	  SELECT 	"

					+ "	  SUM(fg.FGBOPAL_01_adopt_bud + fg.fgbopal_01_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_01_ytd_actv  + fg.fgbopal_01_encumb + fg.fgbopal_01_bud_rsrv) GASTOS ,	"
					+ "     SUM(fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT ) - sum (fg.FGBOPAL_01_YTD_ACTV + fg.FGBOPAL_01_ENCUMB + fg.FGBOPAL_01_BUD_RSRV) saldo_mes ,"
					+ "	  01 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ condicion
					+ "	GROUP BY 01	"
					+ "	  UNION ALL	"
					+ "	  SELECT 	"

					+ "	  SUM(fg.FGBOPAL_02_adopt_bud + fg.fgbopal_02_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_02_ytd_actv  + fg.fgbopal_02_encumb + fg.fgbopal_02_bud_rsrv) GASTOS ,	"
					+ " ((SUM(FG.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT) - SUM(FG.FGBOPAL_00_ADOPT_BUD + FG.FGBOPAL_01_ADOPT_BUD) + SUM(FG.FGBOPAL_00_BUD_ADJT + FG.FGBOPAL_01_BUD_ADJT)) - (SUM(fg.FGBOPAL_02_YTD_ACTV + fg.FGBOPAL_02_ENCUMB + fg.FGBOPAL_02_BUD_RSRV) - SUM(fg.FGBOPAL_01_YTD_ACTV + fg.FGBOPAL_01_ENCUMB + fg.FGBOPAL_01_BUD_RSRV)))  saldo_mes ,"
					+ "	  02 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ condicion
					+ "	GROUP BY 02	"
					+ "	  UNION ALL	"
					+ "	  SELECT 	"

					+ "	  SUM(fg.FGBOPAL_03_adopt_bud + fg.fgbopal_03_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_03_ytd_actv  + fg.fgbopal_03_encumb + fg.fgbopal_03_bud_rsrv) GASTOS ,	"
					+ " (sum(FGBOPAL_03_ADOPT_BUD + FGBOPAL_03_BUD_ADJT) - sum( FGBOPAL_02_ADOPT_BUD + FGBOPAL_02_BUD_ADJT)) -(SUM(FGBOPAL_03_YTD_ACTV + FGBOPAL_03_ENCUMB + FGBOPAL_03_BUD_RSRV) - SUM(FGBOPAL_02_YTD_ACTV + FGBOPAL_02_ENCUMB + FGBOPAL_02_BUD_RSRV)) saldo_mes  ,"
					+ "	  03 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ condicion
					+ "	GROUP BY 03	"
					+ "	  UNION ALL	"
					+ "	  SELECT 	"

					+ "	  SUM(fg.FGBOPAL_04_adopt_bud + fg.fgbopal_04_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_04_ytd_actv  + fg.fgbopal_04_encumb + fg.fgbopal_04_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_04_ADOPT_BUD) + SUM(FG.FGBOPAL_04_BUD_ADJT))-(SUM(FG.FGBOPAL_03_ADOPT_BUD + FG.FGBOPAL_03_BUD_ADJT))- (sum(fg.FGBOPAL_04_YTD_ACTV + fg.FGBOPAL_04_ENCUMB + fg.FGBOPAL_04_BUD_RSRV) - SUM(fg.FGBOPAL_03_YTD_ACTV  +  fg.FGBOPAL_03_ENCUMB + fg.FGBOPAL_03_BUD_RSRV)) saldo_mes  ,"
					+ "	  04 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ condicion
					+ "	GROUP BY 04	"
					+ "	  UNION ALL	"
					+ "	  SELECT 	"

					+ "	  SUM(fg.FGBOPAL_05_adopt_bud + fg.fgbopal_05_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_05_ytd_actv  + fg.fgbopal_05_encumb + fg.fgbopal_05_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_05_ADOPT_BUD) + SUM(FG.FGBOPAL_05_BUD_ADJT))-(SUM(FG.FGBOPAL_04_ADOPT_BUD + FG.FGBOPAL_04_BUD_ADJT))- (sum(fg.FGBOPAL_05_YTD_ACTV + fg.FGBOPAL_05_ENCUMB + fg.FGBOPAL_05_BUD_RSRV) - SUM(fg.FGBOPAL_04_YTD_ACTV  +  fg.FGBOPAL_04_ENCUMB + fg.FGBOPAL_04_BUD_RSRV)) saldo_mes  ,"
					+ "	  05 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ condicion
					+ "	GROUP BY 05	"
					+ "	  UNION ALL	"
					+ "	  SELECT 	"

					+ "	  SUM(fg.FGBOPAL_06_adopt_bud + fg.fgbopal_06_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_06_ytd_actv  + fg.fgbopal_06_encumb + fg.fgbopal_06_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_06_ADOPT_BUD) + SUM(FG.FGBOPAL_06_BUD_ADJT))-(SUM(FG.FGBOPAL_05_ADOPT_BUD + FG.FGBOPAL_05_BUD_ADJT))- (sum(fg.FGBOPAL_06_YTD_ACTV + fg.FGBOPAL_06_ENCUMB + fg.FGBOPAL_06_BUD_RSRV) - SUM(fg.FGBOPAL_05_YTD_ACTV  +  fg.FGBOPAL_05_ENCUMB + fg.FGBOPAL_05_BUD_RSRV)) saldo_mes  ,"
					+ "	  06 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ condicion
					+ "	GROUP BY 06	"
					+ "	  UNION ALL	"
					+ "	  SELECT 	"

					+ "	  SUM(fg.FGBOPAL_07_adopt_bud + fg.fgbopal_07_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_07_ytd_actv  + fg.fgbopal_07_encumb + fg.fgbopal_07_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_07_ADOPT_BUD) + SUM(FG.FGBOPAL_07_BUD_ADJT))-(SUM(FG.FGBOPAL_06_ADOPT_BUD + FG.FGBOPAL_06_BUD_ADJT))- (sum(fg.FGBOPAL_07_YTD_ACTV + fg.FGBOPAL_07_ENCUMB + fg.FGBOPAL_07_BUD_RSRV) - SUM(fg.FGBOPAL_06_YTD_ACTV  +  fg.FGBOPAL_06_ENCUMB + fg.FGBOPAL_06_BUD_RSRV)) saldo_mes  ,"
					+ "	  07 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ condicion
					+ "	GROUP BY 07	"
					+ "	  UNION ALL	"
					+ "	  SELECT 	"

					+ "	  SUM(fg.FGBOPAL_08_adopt_bud + fg.fgbopal_08_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_08_ytd_actv  + fg.fgbopal_08_encumb + fg.fgbopal_08_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_08_ADOPT_BUD) + SUM(FG.FGBOPAL_08_BUD_ADJT))-(SUM(FG.FGBOPAL_07_ADOPT_BUD + FG.FGBOPAL_07_BUD_ADJT))- (sum(fg.FGBOPAL_08_YTD_ACTV + fg.FGBOPAL_08_ENCUMB + fg.FGBOPAL_08_BUD_RSRV) - SUM(fg.FGBOPAL_07_YTD_ACTV  +  fg.FGBOPAL_07_ENCUMB + fg.FGBOPAL_07_BUD_RSRV)) saldo_mes  ,"
					+ "	  08 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ condicion
					+ "	GROUP BY 08	"
					+ "	  UNION ALL	"
					+ "	  SELECT 	"

					+ "	  SUM(fg.FGBOPAL_09_adopt_bud + fg.fgbopal_09_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_09_ytd_actv  + fg.fgbopal_09_encumb + fg.fgbopal_09_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_09_ADOPT_BUD) + SUM(FG.FGBOPAL_09_BUD_ADJT))-(SUM(FG.FGBOPAL_08_ADOPT_BUD + FG.FGBOPAL_08_BUD_ADJT))- (sum(fg.FGBOPAL_09_YTD_ACTV + fg.FGBOPAL_09_ENCUMB + fg.FGBOPAL_09_BUD_RSRV) - SUM(fg.FGBOPAL_08_YTD_ACTV  +  fg.FGBOPAL_08_ENCUMB + fg.FGBOPAL_08_BUD_RSRV)) saldo_mes  ,"
					+ "	  09 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ condicion
					+ "	GROUP BY 09	"
					+ "	  UNION ALL	"
					+ "	  SELECT 	"

					+ "	  SUM(fg.FGBOPAL_10_adopt_bud + fg.fgbopal_10_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_10_ytd_actv  + fg.fgbopal_10_encumb + fg.fgbopal_10_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_10_ADOPT_BUD) + SUM(FG.FGBOPAL_10_BUD_ADJT))-(SUM(FG.FGBOPAL_09_ADOPT_BUD + FG.FGBOPAL_09_BUD_ADJT))- (sum(fg.FGBOPAL_10_YTD_ACTV + fg.FGBOPAL_10_ENCUMB + fg.FGBOPAL_10_BUD_RSRV) - SUM(fg.FGBOPAL_09_YTD_ACTV  +  fg.FGBOPAL_09_ENCUMB + fg.FGBOPAL_09_BUD_RSRV)) saldo_mes  ,"
					+ "	  10 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ condicion
					+ "	GROUP BY 10"
					+ "	  UNION ALL	"
					+ "	  SELECT 	"

					+ "	  SUM(fg.FGBOPAL_11_adopt_bud + fg.fgbopal_11_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_11_ytd_actv  + fg.fgbopal_11_encumb + fg.fgbopal_11_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_11_ADOPT_BUD) + SUM(FG.FGBOPAL_11_BUD_ADJT))-(SUM(FG.FGBOPAL_10_ADOPT_BUD + FG.FGBOPAL_10_BUD_ADJT))- (sum(fg.FGBOPAL_11_YTD_ACTV + fg.FGBOPAL_11_ENCUMB + fg.FGBOPAL_11_BUD_RSRV) - SUM(fg.FGBOPAL_10_YTD_ACTV  +  fg.FGBOPAL_10_ENCUMB + fg.FGBOPAL_10_BUD_RSRV)) saldo_mes  ,"
					+ "	  11 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ condicion
					+ "	GROUP BY 11	"
					+ "	  UNION ALL  	"
					+ "	SELECT 	"

					+ "	  SUM(fg.FGBOPAL_12_adopt_bud + fg.fgbopal_12_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_12_ytd_actv  + fg.fgbopal_12_encumb + fg.fgbopal_12_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_12_ADOPT_BUD) + SUM(FG.FGBOPAL_12_BUD_ADJT))-(SUM(FG.FGBOPAL_11_ADOPT_BUD + FG.FGBOPAL_11_BUD_ADJT))- (sum(fg.FGBOPAL_12_YTD_ACTV + fg.FGBOPAL_12_ENCUMB + fg.FGBOPAL_12_BUD_RSRV) - SUM(fg.FGBOPAL_11_YTD_ACTV  +  fg.FGBOPAL_11_ENCUMB + fg.FGBOPAL_11_BUD_RSRV)) saldo_mes  ,"
					+ "	  12 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ condicion + "	GROUP BY  12 	";
			//System.out.println("Query getReumuneraciones sql " + sql);

			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			long acumulado = 0;
			long gastos = 0;

			while (res.next()) {
				Presw21POJO ss = new Presw21POJO();
				// String aux = res.getString(1);
			
				String nomMes = "";
				int mes = res.getInt(4);
				switch (mes) {
				case 0:
					nomMes = "Saldo inicial";
					break;
				case 1:
					nomMes = "Enero";
					break;
				case 2:
					nomMes = "Febrero";
					break;
				case 3:
					nomMes = "Marzo";
					break;
				case 4:
					nomMes = "Abril";
					break;
				case 5:
					nomMes = "Mayo";
					break;
				case 6:
					nomMes = "Junio";
					break;
				case 7:
					nomMes = "Julio";
					break;
				case 8:
					nomMes = "Agosto";
					break;
				case 9:
					nomMes = "Septiembre";
					break;
				case 10:
					nomMes = "Octubre";
					break;
				case 11:
					nomMes = "Noviembre";
					break;
				case 12:
					nomMes = "Diciembre";
					break;
				}
				ss.setGlosa1(nomMes);
				ss.setValor1(res.getLong(1));
				ss.setValor2(res.getLong(2));
				ss.setValor3(res.getLong(3));
				// ss.setPresac(acumulado);/*
				// ss.setUsadac(gastos);
				lista.add(ss);
			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error getdetalleEjecucion.getVerificaRut: "
					+ e.getMessage());
		}
		return lista;
	}
*/
	public Collection<Pres18POJO> getRemuneraciones(int a�oIN, String org,String estamento) {
		//System.out.println("Entra a getReumuneraciones valor estamento  " + estamento);
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;
		String cuentas = "";
		
		if (estamento.equals("T"))
			cuentas = " AND FGBTRND_ACCT_CODE IN ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5A0010','5A0011','5A0012','5AA001','5AA002','5AA003','5AA004','5AA005','5AA006','5AA007','5AA008','5AA009','5AA010','5AA011','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006','5B0007','5B0008','5B0009','5BB001','5BB002','5BB003')" ;
		else if(estamento.equals("5ACADOC"))
			cuentas = " AND FGBTRND_ACCT_CODE IN ('5A0001','5A0002','5AA005','5AA006','5A0007','5A0008','5A0009','5A0010','5A0011','5A0012','5AA001','5AA002','5AA008','5AA009')";
		else if(estamento.equals("5APODOC"))
			cuentas = " AND FGBTRND_ACCT_CODE IN ('5A0003','5AA004','5AA011')";
		else if(estamento.equals("5AREMVR"))
			cuentas = " AND FGBTRND_ACCT_CODE IN ('5A0004','5A0005','5A0006','5AA003','5AA007','5AA010')";
		else if(estamento.equals("5BADMIN"))
			cuentas = " AND FGBTRND_ACCT_CODE IN ('5BB001','5BB002','5BB003','5B0001','5B0004','5B0005','5B0006','5B0007','5B0008','5B0009')";
		else if(estamento.equals("5BREMVR"))
			cuentas = " AND FGBTRND_ACCT_CODE IN ('5B0002','5B0003')";
	//	else cuentas = " AND FGBTRND_ACCT_CODE = '"+ estamento+"'";
		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		Calendar fecha = Calendar.getInstance();
		String a�osql;
		if (a�oIN == 0) {
			int a�o = fecha.get(Calendar.YEAR);
			a�osql = String.valueOf(a�o);
			a�osql = a�osql.substring(2);
		} else {
			int a�o = a�oIN;
			a�osql = String.valueOf(a�o);
			a�osql = a�osql.substring(2);
			a�o = 0;
			a�o = Integer.parseInt(a�osql);
		}
		try {
			String sql = ""
				+ "SELECT FGBTRND_ORGN_CODE,"
				+ "  FTVORGN_TITLE,"
				+ "  FGBTRND_POSTING_PERIOD ,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Saldo_Inicial_A, "
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0)) " 
				+ " + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) )Presupuesto_B ,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Traspasos_Presupuesto_C ,"
				+ " SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0)) traspasos_servicio_d,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0))+ SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0))  " 
				+ " - SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) )GASTO ,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)) SAC"
				+ " FROM FIMSMGR.FGBTRND,"
				+ " FIMSMGR.FTVFUND,"
				+ " FIMSMGR.FTVORGN,"
				+ " FIMSMGR.FTVACCT,"
				+ " FIMSMGR.FTVPROG"
				+ " WHERE FGBTRND_ACCT_CODE <> '4BD007' AND FGBTRND_FUND_CODE = FTVFUND_FUND_CODE"
				+ " AND FGBTRND_ORGN_CODE = FTVORGN_ORGN_CODE"
				+ " AND FGBTRND_PROG_CODE = FTVPROG_PROG_CODE"
				+ " AND FGBTRND_ACCT_CODE = FTVACCT_ACCT_CODE"
				+ " AND FGBTRND_ACTIVITY_DATE >= FTVFUND_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE < FTVFUND_NCHG_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE >= FTVORGN_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE < FTVORGN_NCHG_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE >= FTVPROG_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE < FTVPROG_NCHG_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE >= FTVACCT_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE < FTVACCT_NCHG_DATE"
				+ " AND FTVFUND_FTYP_CODE IN ('CA','CD','CN','FI','FR')"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ " AND FTVFUND_DATA_ENTRY_IND = 'Y'"
				+ " AND FTVORGN_DATA_ENTRY_IND = 'Y'"
				+ " AND FTVACCT_DATA_ENTRY_IND = 'Y'"
				+ " AND FTVPROG_DATA_ENTRY_IND = 'Y'"
				+ " AND FGBTRND.FGBTRND_LEDC_CODE = 'FINANC'"
				+ " AND FGBTRND_LEDGER_IND = 'O'"
				+ " AND FGBTRND_FSYR_CODE = '"
				+ a�osql
				+ "'"
				+ " AND FGBTRND_ORGN_CODE = '"
				+ org
				+ "'"
				+ cuentas
				+ " GROUP BY FGBTRND_ORGN_CODE, FGBTRND_POSTING_PERIOD,ftvorgn_title"
				+ " ORDER BY FGBTRND_POSTING_PERIOD";
		//	System.out.println("Query getReumuneraciones sql " + sql);

			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			long acumulado = 0;
			long gastos = 0;

	
			
			 acumulado = 0;
			 gastos = 0;
			int mes_consulta = 0;
			int mes_sql = 0;
			long presu_acum = 0;
			long gasto_acum = 0;
			int n=res.getRow();
			int sw = 0;
			//System.out.println("CANTIDAD REGISTROS " + n);
			while (res.next()) {
				
				sw = 0 ;
				
				mes_sql = res.getInt(3);
			while (mes_consulta < 13 && (sw == 0)) {
				Pres18POJO ss = new Pres18POJO();
				//System.out.println("MES DE CONSULTA  = " + mes_consulta );
				/*
				 * if (mes_sql == 0 ) { // saldo inicial
				 * 
				 *  } else
				 */if (mes_sql == mes_consulta) {
					 //System.out.println("mes sql = mes consulta  " );
					ss.setCoduni(res.getString(1)); // cod cuenta
					ss.setDesuni(res.getString(2)); // titulo cta
					ss.setPresum(res.getLong(5)); // presupuesto
					presu_acum = presu_acum + ss.getPresum();
					gastos = res.getLong(8);
					ss.setUsadom(gastos - res.getLong(7)
							- res.getLong(6) - res.getLong(9)); // gastos
					gasto_acum = gasto_acum + ss.getUsadom();
					
					if (gastos > 0) {
						ss.setIddigi("S");

					} else {
						ss.setIddigi("N");

					}
					ss.setNummes(mes_sql);
					ss.setSaldoMes(ss.getPresum() - ss.getUsadom()); // saldo
																		// mes
					ss.setPresac(presu_acum); // pres acumulado
					ss.setUsadac(gasto_acum); // gasto acumulado
					ss.setTotal((presu_acum - gasto_acum)); // total
				//	System.out.println("total " + ss.getTotal());
					// porcentaje ss.setPorc1(0); //
					//System.out.println("Va a cargar porcentajes mes = mes" );
					if (ss.getPresum() != 0 && ss.getUsadom() != 0)
						ss.setPorc1(ss.getUsadom() / ss.getPresum()* 100);
					else
						ss.setPorc1(0);

					if (ss.getUsadac() != 0 && ss.getPresac() != 0)
						ss.setPorc2(ss.getUsadac() / ss.getPresac()* 100);
					else
						ss.setPorc2(0);
					
					sw = 1 ;
					//System.out.println("Fin  cargar porcentajes mes = mes" );

				} else {
					//System.out.println("  mes  sql " + mes_sql  + " <> mes consulta " +  mes_consulta );
					ss.setCoduni(res.getString(1)); // cod cuenta
					ss.setDesuni(res.getString(2)); // titulo cta
					ss.setPresum(0); // presupuesto
					ss.setUsadom(0); // gastos
					ss.setIddigi("N");
					ss.setPorc1(0); // porc1
					ss.setNummes(mes_consulta);
					ss.setSaldoMes(0); // saldo mes
					ss.setPresac(presu_acum); // pres acumulado
					ss.setUsadac(gasto_acum); // gasto acumulado
					ss.setTotal((presu_acum - gasto_acum)); // total
					ss.setPorc2(0); // porc2
				
					//System.out.println("Fin seteo datos mes <> mes" );
				}
				
			
					 lista.add(ss);
						mes_consulta = mes_consulta + 1;
				

			}
		
		
			
		}
		//relleno de meses
		if (mes_sql < 12) {
			
			mes_sql = mes_sql+1 ;
			for(int i = mes_sql; i <= 12; i = i + 1) {
				Pres18POJO ss = new Pres18POJO();
				//System.out.println("  mes  sql " + mes_sql  + " <> mes consulta " +  mes_consulta );
				ss.setCoduni(org); // cod cuenta
				ss.setDesuni(""); // titulo cta
				ss.setPresum(0); // presupuesto
				ss.setUsadom(0); // gastos
				ss.setIddigi("N");
				ss.setPorc1(0); // porc1
				//System.out.println("Valor i-mes " + i);
				ss.setNummes(i);						
				ss.setSaldoMes(0); // saldo mes
				ss.setPresac(presu_acum); // pres acumulado
				ss.setUsadac(gasto_acum); // gasto acumulado
				ss.setTotal((presu_acum - gasto_acum)); // total
				ss.setPorc2(0); // porc2
				
				lista.add(ss);
				
			} 
		} 

			
			
			
			
			
			
			
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error getdetalleEjecucion.getVerificaRut: "+ e.getMessage());
		}
		return lista;
	}
/*
	public Collection<Presw21POJO> getRemuneraciones_respaldo(int a�oIN,
			String org, String estamento) {
		//System.out.println("Entra a getReumuneraciones valor mes ");
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Presw21POJO> lista = new ArrayList<Presw21POJO>();
		Calendar fecha = Calendar.getInstance();
		String a�osql;
		if (a�oIN == 0) {
			int a�o = fecha.get(Calendar.YEAR);
			a�osql = String.valueOf(a�o);
			a�osql = a�osql.substring(2);
		} else {
			int a�o = a�oIN;
			a�osql = String.valueOf(a�o);
			a�osql = a�osql.substring(2);
			a�o = 0;
			a�o = Integer.parseInt(a�osql);
		}
		try {
			String condicion;
			String sql = "";
			if (estamento.equals("T") || (estamento.isEmpty())) {
				condicion = " WHERE FGBOPAL_ACCT_CODE IN ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006')	  ";
			} else {
				condicion = " WHERE FGBOPAL_ACCT_CODE  = '" + estamento + "'";
			}
			sql = ""
					+ "	SELECT  * 	"
					+ "	FROM 	"
					+ "	(	"
					+ "	SELECT fg.FGBOPAL_ACCT_CODE,	"
					+ "	  ft.FTVACCT_TITLE CUENTA ,	"
					+ "	  SUM(fg.FGBOPAL_00_adopt_bud + fg.fgbopal_00_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_00_ytd_actv  + fg.fgbopal_00_encumb + fg.fgbopal_00_bud_rsrv) GASTOS ,	"
					+ " SUM((FGBOPAL_00_ADOPT_BUD ) + (FGBOPAL_00_BUD_ADJT ) - FGBOPAL_00_YTD_ACTV )saldo_mes ,"
					+ "	  00 Mes	" + "	FROM FGBOPAL fg ,	" + "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ "	AND FT.FTVACCT_ACCT_CODE   = FG.FGBOPAL_ACCT_CODE	"
					+ "	GROUP BY fg.FGBOPAL_ORGN_CODE, ft.FTVACCT_TITLE, fg.FGBOPAL_ACCT_CODE ,00	"
					+ "	  UNION ALL	"
					+ "	  SELECT fg.FGBOPAL_ACCT_CODE,	"
					+ "	  ft.FTVACCT_TITLE CUENTA ,	"
					+ "	  SUM(fg.FGBOPAL_01_adopt_bud + fg.fgbopal_01_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_01_ytd_actv  + fg.fgbopal_01_encumb + fg.fgbopal_01_bud_rsrv) GASTOS ,	"
					+ "     SUM(fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT ) - sum (fg.FGBOPAL_01_YTD_ACTV + fg.FGBOPAL_01_ENCUMB + fg.FGBOPAL_01_BUD_RSRV) saldo_mes ,"
					+ "	  01 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ "	AND FT.FTVACCT_ACCT_CODE   = FG.FGBOPAL_ACCT_CODE	"
					+ "	GROUP BY fg.FGBOPAL_ORGN_CODE ,	"
					+ "	  ft.FTVACCT_TITLE ,	"
					+ "	  fg.FGBOPAL_ACCT_CODE,	"
					+ "	1	"
					+ "	  UNION ALL	"
					+ "	  SELECT fg.FGBOPAL_ACCT_CODE,	"
					+ "	  ft.FTVACCT_TITLE CUENTA ,	"
					+ "	  SUM(fg.FGBOPAL_02_adopt_bud + fg.fgbopal_02_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_02_ytd_actv  + fg.fgbopal_02_encumb + fg.fgbopal_02_bud_rsrv) GASTOS ,	"
					+ " ((SUM(FG.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT) - SUM(FG.FGBOPAL_00_ADOPT_BUD + FG.FGBOPAL_01_ADOPT_BUD) + SUM(FG.FGBOPAL_00_BUD_ADJT + FG.FGBOPAL_01_BUD_ADJT)) - (SUM(fg.FGBOPAL_02_YTD_ACTV + fg.FGBOPAL_02_ENCUMB + fg.FGBOPAL_02_BUD_RSRV) - SUM(fg.FGBOPAL_01_YTD_ACTV + fg.FGBOPAL_01_ENCUMB + fg.FGBOPAL_01_BUD_RSRV)))  saldo_mes ,"
					+ "	  02 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ "	AND FT.FTVACCT_ACCT_CODE   = FG.FGBOPAL_ACCT_CODE	"
					+ "	GROUP BY fg.FGBOPAL_ORGN_CODE ,	"
					+ "	  ft.FTVACCT_TITLE ,	"
					+ "	  fg.FGBOPAL_ACCT_CODE,	"
					+ "	2	"
					+ "	  UNION ALL	"
					+ "	  SELECT fg.FGBOPAL_ACCT_CODE,	"
					+ "	  ft.FTVACCT_TITLE CUENTA ,	"
					+ "	  SUM(fg.FGBOPAL_03_adopt_bud + fg.fgbopal_03_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_03_ytd_actv  + fg.fgbopal_03_encumb + fg.fgbopal_03_bud_rsrv) GASTOS ,	"
					+ " (sum(FGBOPAL_03_ADOPT_BUD + FGBOPAL_03_BUD_ADJT) - sum( FGBOPAL_02_ADOPT_BUD + FGBOPAL_02_BUD_ADJT)) -(SUM(FGBOPAL_03_YTD_ACTV + FGBOPAL_03_ENCUMB + FGBOPAL_03_BUD_RSRV) - SUM(FGBOPAL_02_YTD_ACTV + FGBOPAL_02_ENCUMB + FGBOPAL_02_BUD_RSRV)) saldo_mes  ,"
					+ "	  03 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ "	AND FT.FTVACCT_ACCT_CODE   = FG.FGBOPAL_ACCT_CODE	"
					+ "	GROUP BY fg.FGBOPAL_ORGN_CODE ,	"
					+ "	  ft.FTVACCT_TITLE ,	"
					+ "	  fg.FGBOPAL_ACCT_CODE,	"
					+ "	3	"
					+ "	  UNION ALL	"
					+ "	  SELECT fg.FGBOPAL_ACCT_CODE,	"
					+ "	  ft.FTVACCT_TITLE CUENTA ,	"
					+ "	  SUM(fg.FGBOPAL_04_adopt_bud + fg.fgbopal_04_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_04_ytd_actv  + fg.fgbopal_04_encumb + fg.fgbopal_04_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_04_ADOPT_BUD) + SUM(FG.FGBOPAL_04_BUD_ADJT))-(SUM(FG.FGBOPAL_03_ADOPT_BUD + FG.FGBOPAL_03_BUD_ADJT))- (sum(fg.FGBOPAL_04_YTD_ACTV + fg.FGBOPAL_04_ENCUMB + fg.FGBOPAL_04_BUD_RSRV) - SUM(fg.FGBOPAL_03_YTD_ACTV  +  fg.FGBOPAL_03_ENCUMB + fg.FGBOPAL_03_BUD_RSRV)) saldo_mes  ,"
					+ "	  04 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ "	AND FT.FTVACCT_ACCT_CODE   = FG.FGBOPAL_ACCT_CODE	"
					+ "	GROUP BY fg.FGBOPAL_ORGN_CODE ,	"
					+ "	  ft.FTVACCT_TITLE ,	"
					+ "	  fg.FGBOPAL_ACCT_CODE,	"
					+ "	4	"
					+ "	  UNION ALL	"
					+ "	  SELECT fg.FGBOPAL_ACCT_CODE,	"
					+ "	  ft.FTVACCT_TITLE CUENTA ,	"
					+ "	  SUM(fg.FGBOPAL_05_adopt_bud + fg.fgbopal_05_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_05_ytd_actv  + fg.fgbopal_05_encumb + fg.fgbopal_05_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_05_ADOPT_BUD) + SUM(FG.FGBOPAL_05_BUD_ADJT))-(SUM(FG.FGBOPAL_04_ADOPT_BUD + FG.FGBOPAL_04_BUD_ADJT))- (sum(fg.FGBOPAL_05_YTD_ACTV + fg.FGBOPAL_05_ENCUMB + fg.FGBOPAL_05_BUD_RSRV) - SUM(fg.FGBOPAL_04_YTD_ACTV  +  fg.FGBOPAL_04_ENCUMB + fg.FGBOPAL_04_BUD_RSRV)) saldo_mes  ,"
					+ "	  05 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ "	AND FT.FTVACCT_ACCT_CODE   = FG.FGBOPAL_ACCT_CODE	"
					+ "	GROUP BY fg.FGBOPAL_ORGN_CODE ,	"
					+ "	  ft.FTVACCT_TITLE ,	"
					+ "	  fg.FGBOPAL_ACCT_CODE,	"
					+ "	5	"
					+ "	  UNION ALL	"
					+ "	  SELECT fg.FGBOPAL_ACCT_CODE,	"
					+ "	  ft.FTVACCT_TITLE CUENTA ,	"
					+ "	  SUM(fg.FGBOPAL_06_adopt_bud + fg.fgbopal_06_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_06_ytd_actv  + fg.fgbopal_06_encumb + fg.fgbopal_06_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_06_ADOPT_BUD) + SUM(FG.FGBOPAL_06_BUD_ADJT))-(SUM(FG.FGBOPAL_05_ADOPT_BUD + FG.FGBOPAL_05_BUD_ADJT))- (sum(fg.FGBOPAL_06_YTD_ACTV + fg.FGBOPAL_06_ENCUMB + fg.FGBOPAL_06_BUD_RSRV) - SUM(fg.FGBOPAL_05_YTD_ACTV  +  fg.FGBOPAL_05_ENCUMB + fg.FGBOPAL_05_BUD_RSRV)) saldo_mes  ,"
					+ "	  06 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ "	AND FT.FTVACCT_ACCT_CODE   = FG.FGBOPAL_ACCT_CODE	"
					+ "	GROUP BY fg.FGBOPAL_ORGN_CODE ,	"
					+ "	  ft.FTVACCT_TITLE ,	"
					+ "	  fg.FGBOPAL_ACCT_CODE,	"
					+ "	6	"
					+ "	  UNION ALL	"
					+ "	  SELECT fg.FGBOPAL_ACCT_CODE,	"
					+ "	  ft.FTVACCT_TITLE CUENTA ,	"
					+ "	  SUM(fg.FGBOPAL_07_adopt_bud + fg.fgbopal_07_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_07_ytd_actv  + fg.fgbopal_07_encumb + fg.fgbopal_07_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_07_ADOPT_BUD) + SUM(FG.FGBOPAL_07_BUD_ADJT))-(SUM(FG.FGBOPAL_06_ADOPT_BUD + FG.FGBOPAL_06_BUD_ADJT))- (sum(fg.FGBOPAL_07_YTD_ACTV + fg.FGBOPAL_07_ENCUMB + fg.FGBOPAL_07_BUD_RSRV) - SUM(fg.FGBOPAL_06_YTD_ACTV  +  fg.FGBOPAL_06_ENCUMB + fg.FGBOPAL_06_BUD_RSRV)) saldo_mes  ,"
					+ "	  07 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ "	AND FT.FTVACCT_ACCT_CODE   = FG.FGBOPAL_ACCT_CODE	"
					+ "	GROUP BY fg.FGBOPAL_ORGN_CODE ,	"
					+ "	  ft.FTVACCT_TITLE ,	"
					+ "	  fg.FGBOPAL_ACCT_CODE,	"
					+ "	7	"
					+ "	  UNION ALL	"
					+ "	  SELECT fg.FGBOPAL_ACCT_CODE,	"
					+ "	  ft.FTVACCT_TITLE CUENTA ,	"
					+ "	  SUM(fg.FGBOPAL_08_adopt_bud + fg.fgbopal_08_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_08_ytd_actv  + fg.fgbopal_08_encumb + fg.fgbopal_08_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_08_ADOPT_BUD) + SUM(FG.FGBOPAL_08_BUD_ADJT))-(SUM(FG.FGBOPAL_07_ADOPT_BUD + FG.FGBOPAL_07_BUD_ADJT))- (sum(fg.FGBOPAL_08_YTD_ACTV + fg.FGBOPAL_08_ENCUMB + fg.FGBOPAL_08_BUD_RSRV) - SUM(fg.FGBOPAL_07_YTD_ACTV  +  fg.FGBOPAL_07_ENCUMB + fg.FGBOPAL_07_BUD_RSRV)) saldo_mes  ,"
					+ "	  08 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ "	AND FT.FTVACCT_ACCT_CODE   = FG.FGBOPAL_ACCT_CODE	"
					+ "	GROUP BY fg.FGBOPAL_ORGN_CODE ,	"
					+ "	  ft.FTVACCT_TITLE ,	"
					+ "	  fg.FGBOPAL_ACCT_CODE,	"
					+ "	8	"
					+ "	  UNION ALL	"
					+ "	  SELECT fg.FGBOPAL_ACCT_CODE,	"
					+ "	  ft.FTVACCT_TITLE CUENTA ,	"
					+ "	  SUM(fg.FGBOPAL_09_adopt_bud + fg.fgbopal_09_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_09_ytd_actv  + fg.fgbopal_09_encumb + fg.fgbopal_09_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_09_ADOPT_BUD) + SUM(FG.FGBOPAL_09_BUD_ADJT))-(SUM(FG.FGBOPAL_08_ADOPT_BUD + FG.FGBOPAL_08_BUD_ADJT))- (sum(fg.FGBOPAL_09_YTD_ACTV + fg.FGBOPAL_09_ENCUMB + fg.FGBOPAL_09_BUD_RSRV) - SUM(fg.FGBOPAL_08_YTD_ACTV  +  fg.FGBOPAL_08_ENCUMB + fg.FGBOPAL_08_BUD_RSRV)) saldo_mes  ,"
					+ "	  09 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ "	AND FT.FTVACCT_ACCT_CODE   = FG.FGBOPAL_ACCT_CODE	"
					+ "	GROUP BY fg.FGBOPAL_ORGN_CODE ,	"
					+ "	  ft.FTVACCT_TITLE ,	"
					+ "	  fg.FGBOPAL_ACCT_CODE,	"
					+ "	9	"
					+ "	  UNION ALL	"
					+ "	  SELECT fg.FGBOPAL_ACCT_CODE,	"
					+ "	  ft.FTVACCT_TITLE CUENTA ,	"
					+ "	  SUM(fg.FGBOPAL_10_adopt_bud + fg.fgbopal_10_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_10_ytd_actv  + fg.fgbopal_10_encumb + fg.fgbopal_10_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_10_ADOPT_BUD) + SUM(FG.FGBOPAL_10_BUD_ADJT))-(SUM(FG.FGBOPAL_09_ADOPT_BUD + FG.FGBOPAL_09_BUD_ADJT))- (sum(fg.FGBOPAL_10_YTD_ACTV + fg.FGBOPAL_10_ENCUMB + fg.FGBOPAL_10_BUD_RSRV) - SUM(fg.FGBOPAL_09_YTD_ACTV  +  fg.FGBOPAL_09_ENCUMB + fg.FGBOPAL_09_BUD_RSRV)) saldo_mes  ,"
					+ "	  10 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ "	AND FT.FTVACCT_ACCT_CODE   = FG.FGBOPAL_ACCT_CODE	"
					+ "	GROUP BY fg.FGBOPAL_ORGN_CODE ,	"
					+ "	  ft.FTVACCT_TITLE ,	"
					+ "	  fg.FGBOPAL_ACCT_CODE,	"
					+ "	10	"
					+ "	  UNION ALL	"
					+ "	  SELECT fg.FGBOPAL_ACCT_CODE,	"
					+ "	  ft.FTVACCT_TITLE CUENTA ,	"
					+ "	  SUM(fg.FGBOPAL_11_adopt_bud + fg.fgbopal_11_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_11_ytd_actv  + fg.fgbopal_11_encumb + fg.fgbopal_11_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_11_ADOPT_BUD) + SUM(FG.FGBOPAL_11_BUD_ADJT))-(SUM(FG.FGBOPAL_10_ADOPT_BUD + FG.FGBOPAL_10_BUD_ADJT))- (sum(fg.FGBOPAL_11_YTD_ACTV + fg.FGBOPAL_11_ENCUMB + fg.FGBOPAL_11_BUD_RSRV) - SUM(fg.FGBOPAL_10_YTD_ACTV  +  fg.FGBOPAL_10_ENCUMB + fg.FGBOPAL_10_BUD_RSRV)) saldo_mes  ,"
					+ "	  11 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ "	AND FT.FTVACCT_ACCT_CODE   = FG.FGBOPAL_ACCT_CODE	"
					+ "	GROUP BY fg.FGBOPAL_ORGN_CODE ,	"
					+ "	  ft.FTVACCT_TITLE ,	"
					+ "	  fg.FGBOPAL_ACCT_CODE,	"
					+ "	11	"
					+ "	  UNION ALL  	"
					+ "	SELECT fg.FGBOPAL_ACCT_CODE,	"
					+ "	  ft.FTVACCT_TITLE CUENTA ,	"
					+ "	  SUM(fg.FGBOPAL_12_adopt_bud + fg.fgbopal_12_bud_adjt ) PRESUPUESTO ,	"
					+ "	  SUM(fg.fgbopal_12_ytd_actv  + fg.fgbopal_12_encumb + fg.fgbopal_12_bud_rsrv) GASTOS ,	"
					+ " (SUM(FG.FGBOPAL_12_ADOPT_BUD) + SUM(FG.FGBOPAL_12_BUD_ADJT))-(SUM(FG.FGBOPAL_11_ADOPT_BUD + FG.FGBOPAL_11_BUD_ADJT))- (sum(fg.FGBOPAL_12_YTD_ACTV + fg.FGBOPAL_12_ENCUMB + fg.FGBOPAL_12_BUD_RSRV) - SUM(fg.FGBOPAL_11_YTD_ACTV  +  fg.FGBOPAL_11_ENCUMB + fg.FGBOPAL_11_BUD_RSRV)) saldo_mes  ,"
					+ "	  12 Mes	"
					+ "	FROM FGBOPAL fg ,	"
					+ "	  FTVACCT ft	"
					+ "	WHERE fg.fgbopal_ledc_code = 'FINANC'	"
					+ "	AND fg.FGBOPAL_FSYR_CODE  =  '"
					+ a�osql
					+ "'	"
					+ "	AND FG.FGBOPAL_ORGN_CODE   = '"
					+ org
					+ "'	"
					+ "	AND FT.FTVACCT_ACCT_CODE   = FG.FGBOPAL_ACCT_CODE	"
					+ "	GROUP BY fg.FGBOPAL_ORGN_CODE ,ft.FTVACCT_TITLE ,fg.FGBOPAL_ACCT_CODE, 12 	"
					+ "	 )  	"
					// + " WHERE FGBOPAL_ACCT_CODE IN
					// ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006')
					// "
					+ condicion + "	 ORDER BY MES ASC ";
			//System.out.println("Query getReumuneraciones " + sql);

			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			long acumulado = 0;
			long gastos = 0;

			while (res.next()) {
				Presw21POJO ss = new Presw21POJO();
				// String aux = res.getString(1);
			
				String nomMes = "";
				int mes = res.getInt(6);
				switch (mes) {
				case 0:
					nomMes = "Saldo inicial";
					break;
				case 1:
					nomMes = "Enero";
					break;
				case 2:
					nomMes = "Febrero";
					break;
				case 3:
					nomMes = "Marzo";
					break;
				case 4:
					nomMes = "Abril";
					break;
				case 5:
					nomMes = "Mayo";
					break;
				case 6:
					nomMes = "Junio";
					break;
				case 7:
					nomMes = "Julio";
					break;
				case 8:
					nomMes = "Agosto";
					break;
				case 9:
					nomMes = "Septiembre";
					break;
				case 10:
					nomMes = "Octubre";
					break;
				case 11:
					nomMes = "Noviembre";
					break;
				case 12:
					nomMes = "Diciembre";
					break;
				}
				ss.setGlosa1(nomMes);
				ss.setValor1(res.getLong(3));
				ss.setValor2(res.getLong(4));
				ss.setValor3(res.getLong(5));
				// ss.setPresac(acumulado);/*
				// ss.setUsadac(gastos);
				lista.add(ss);
			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error getdetalleEjecucion.getVerificaRut: "+ e.getMessage());
		}
		return lista;
	}
	*/
	
/*
	public Collection<Pres18POJO> getdetalleEjecucion(int a�oIN, String org,
			int mes) {
		//System.out.println("Entra a getdetalleEjecucion valor mes " + mes);
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
		String sql = "";
		try {			
			if (mes == -1) {
				mes = 12 ;
			} 				
			sql = ""
				+ "	SELECT FGBTRND_ACCT_CODE ,	"
				+ "	FTVACCT_TITLE,	"
				+ "	SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +	"
				+ "	SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +	"
				+ "	SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Presupuesto ,	"
				+ "	(	"
				+ "	SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0))+ 	"
				+ "	SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) - 	"
				+ "	SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD08',FGBTRND.FGBTRND_TRANS_AMT,0),0)) - 	"
				+ "	SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)) - 	"
				+ "	SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)) -	"
				+ "	SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0))	"
				+ "	)GASTOS	"
				+ "	FROM FIMSMGR.FGBTRND,	"
				+ "	FIMSMGR.FTVFUND,	"
				+ "	FIMSMGR.FTVORGN,	"
				+ "	FIMSMGR.FTVACCT,	"
				+ "	FIMSMGR.FTVPROG	"
				+ " WHERE FGBTRND_FUND_CODE       = FTVFUND_FUND_CODE"
				+ " AND FGBTRND_ORGN_CODE         = FTVORGN_ORGN_CODE"
				+ " AND FGBTRND_PROG_CODE         = FTVPROG_PROG_CODE"
				+ " AND FGBTRND_ACCT_CODE         = FTVACCT_ACCT_CODE"
				+ " AND FGBTRND_ACTIVITY_DATE    >= FTVFUND_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE     < FTVFUND_NCHG_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE    >= FTVORGN_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE     < FTVORGN_NCHG_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE    >= FTVPROG_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE     < FTVPROG_NCHG_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE    >= FTVACCT_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE     < FTVACCT_NCHG_DATE"
				+ " AND FTVFUND_FTYP_CODE        IN ('CA','CD','CN','FI','FR')"
				+ " AND FTVFUND_DATA_ENTRY_IND    = 'Y'"
				+ " AND FTVORGN_DATA_ENTRY_IND    = 'Y'"
				+ " AND FTVACCT_DATA_ENTRY_IND    = 'Y'"
				+ " AND FTVPROG_DATA_ENTRY_IND    = 'Y'"
				+ " AND FGBTRND.FGBTRND_LEDC_CODE = 'FINANC'"
				+ " AND FGBTRND_LEDGER_IND        = 'O'"
				+ " AND FGBTRND_FSYR_CODE         = '"+a�o+"'"
				+ " AND FGBTRND_ORGN_CODE         = '"+org+"'"
				+ " AND FGBTRND_POSTING_PERIOD BETWEEN  0 AND  "+mes+""
				+ " GROUP BY FGBTRND_ACCT_CODE,  FTVACCT_TITLE "
				+ "ORDER BY FGBTRND_ACCT_CODE" ;	
			
			System.out.println("Query cuentas getdetalleEjecucion " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			long acumulado = 0;
			long gastos = 0;
			
			while (res.next()) {
				Pres18POJO ss = new Pres18POJO();
				// String aux = res.getString(1);
				ss.setItedoc(res.getString(1)); // nombre + apellidos
				ss.setDesite(res.getString(2));
				// System.out.println(" DATOS ERROR " + res.getString(1) + " //
				// " + res.getString(2));
				ss.setPresum(res.getLong(3));
				acumulado = acumulado + res.getLong(3);
				ss.setUsadom(res.getLong(4));
				gastos = gastos + res.getLong(4);
				ss.setPresac(acumulado);
				ss.setUsadac(gastos);
				lista.add(ss);
			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error getdetalleEjecucion.getVerificaRut: "
					+ e.getMessage());
		}
		return lista;
	}

	*/
	public Collection<Pres18POJO> getdetalleEjecucion(int a�oIN, String org,
			int mes) {
		//System.out.println("Entra a getdetalleEjecucion valor mes " + mes);
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		ArrayList<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		HashMap<String, Pres18POJO> listContrato = new HashMap<String, Pres18POJO>() ;
		a�o = Integer.parseInt(a�osql);
		String sql = "";
		try {			
			if (mes == -1) {
				mes = 12 ;
			} 				
			sql = ""
				+"	SELECT '5ACADOC' ORGN, "
				+"	  'GASTO REM ACAD�MICOS' TITULO,"
				+"	   SUM(PRESUPUESTO),"
				+"	  SUM(GASTOS)"
				+"	FROM"
				+"	  (SELECT FGBTRND_ACCT_CODE ORGN,"
				+"	    FTVACCT_TITLE,"
				+"	    SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0))  + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) Presupuesto ,"
				+"	    ( SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0))                                  + SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) - (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))) - SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)) - SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)) - SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0))) - SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) )GASTOS"
				+"	  FROM FIMSMGR.FGBTRND,"
				+"	    FIMSMGR.FTVFUND,"
				+"	    FIMSMGR.FTVORGN,"
				+"	    FIMSMGR.FTVACCT,"
				+"	    FIMSMGR.FTVPROG"
				+"	  WHERE FGBTRND_FUND_CODE       = FTVFUND_FUND_CODE"
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+"	  AND FGBTRND_ORGN_CODE         = FTVORGN_ORGN_CODE"
				+"	  AND FGBTRND_PROG_CODE         = FTVPROG_PROG_CODE"
				+"	  AND FGBTRND_ACCT_CODE         = FTVACCT_ACCT_CODE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVFUND_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVFUND_NCHG_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVORGN_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVORGN_NCHG_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVPROG_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVPROG_NCHG_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVACCT_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVACCT_NCHG_DATE"
				+"	  AND FTVFUND_FTYP_CODE        IN ('CA','CD','CN','FI','FR')"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+"	  AND FTVFUND_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FTVORGN_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FTVACCT_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FTVPROG_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FGBTRND.FGBTRND_LEDC_CODE = 'FINANC'"
				+"	  AND FGBTRND_LEDGER_IND        = 'O'"
				+"	  AND FGBTRND_FSYR_CODE         = '"+a�o+"'"
				+"	  AND FGBTRND_ORGN_CODE         = '"+org+"'"
				+"	AND FGBTRND_POSTING_PERIOD BETWEEN  ? AND  ?"
				+"	  GROUP BY FGBTRND_ACCT_CODE,"
				+"	    FTVACCT_TITLE"
				+"	  ORDER BY FGBTRND_ACCT_CODE"
				+"	  ) SIIF"
				+"	WHERE ORGN IN ('5A0001','5A0002','5AA005','5AA006','5A0007','5A0008','5A0009','5A0010','5A0011','5A0012','5AA001','5AA002','5AA008','5AA009')"
				+"	GROUP BY '5ACADOC'"
				+"	UNION ALL"
				+"	SELECT '5APODOC' ORGN, "
				+"	  'GASTO REM APOYOS ACA Y DOC' TITULO,"
				+"	  SUM(PRESUPUESTO),"
				+"	  SUM(GASTOS)"
				+"	FROM"
				+"	  (SELECT FGBTRND_ACCT_CODE ORGN,"
				+"	    FTVACCT_TITLE,"
				+"	    SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0))  + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) Presupuesto ,"
				+"	    ( SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0))                                  + SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) - (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))) - SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)) - SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)) - SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0))) - SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) )GASTOS"
				+"	  FROM FIMSMGR.FGBTRND,"
				+"	    FIMSMGR.FTVFUND,"
				+"	    FIMSMGR.FTVORGN,"
				+"	    FIMSMGR.FTVACCT,"
				+"	    FIMSMGR.FTVPROG"
				+"	  WHERE FGBTRND_FUND_CODE       = FTVFUND_FUND_CODE"
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+"	  AND FGBTRND_ORGN_CODE         = FTVORGN_ORGN_CODE"
				+"	  AND FGBTRND_PROG_CODE         = FTVPROG_PROG_CODE"
				+"	  AND FGBTRND_ACCT_CODE         = FTVACCT_ACCT_CODE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVFUND_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVFUND_NCHG_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVORGN_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVORGN_NCHG_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVPROG_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVPROG_NCHG_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVACCT_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVACCT_NCHG_DATE"
				+"	  AND FTVFUND_FTYP_CODE        IN ('CA','CD','CN','FI','FR')"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+"	  AND FTVFUND_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FTVORGN_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FTVACCT_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FTVPROG_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FGBTRND.FGBTRND_LEDC_CODE = 'FINANC'"
				+"	  AND FGBTRND_LEDGER_IND        = 'O'"
				+"	  AND FGBTRND_FSYR_CODE         = '"+a�o+"'"
				+"	  AND FGBTRND_ORGN_CODE         = '"+org+"'"
				+"	AND FGBTRND_POSTING_PERIOD BETWEEN  ? AND  ?"
				+"	  GROUP BY FGBTRND_ACCT_CODE,"
				+"	    FTVACCT_TITLE"
				+"	  ORDER BY FGBTRND_ACCT_CODE"
				+"	  ) SIIF"
				+"	WHERE ORGN IN ('5A0003','5AA004','5AA011')"
				+"	GROUP BY '5APODOC'"
				+"	UNION ALL"
				+"	SELECT '5AREMVR' ORGN, "
				+"	  'GASTO REM HON, PT Y AYU' TITULO,"
				+"	   SUM(PRESUPUESTO),"
				+"	  SUM(GASTOS)"
				+"	FROM"
				+"	  (SELECT FGBTRND_ACCT_CODE ORGN,"
				+"	    FTVACCT_TITLE,"
				+"	    SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0))  + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) Presupuesto ,"
				+"	    ( SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0))                                  + SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) - (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))) - SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)) - SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)) - SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0))) - SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) )GASTOS"
				+"	  FROM FIMSMGR.FGBTRND,"
				+"	    FIMSMGR.FTVFUND,"
				+"	    FIMSMGR.FTVORGN,"
				+"	    FIMSMGR.FTVACCT,"
				+"	    FIMSMGR.FTVPROG"
				+"	  WHERE FGBTRND_FUND_CODE = FTVFUND_FUND_CODE"
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+"	  AND FGBTRND_ORGN_CODE         = FTVORGN_ORGN_CODE"
				+"	  AND FGBTRND_PROG_CODE         = FTVPROG_PROG_CODE"
				+"	  AND FGBTRND_ACCT_CODE         = FTVACCT_ACCT_CODE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVFUND_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVFUND_NCHG_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVORGN_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVORGN_NCHG_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVPROG_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVPROG_NCHG_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVACCT_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVACCT_NCHG_DATE"
				+"	  AND FTVFUND_FTYP_CODE        IN ('CA','CD','CN','FI','FR')"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+"	  AND FTVFUND_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FTVORGN_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FTVACCT_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FTVPROG_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FGBTRND.FGBTRND_LEDC_CODE = 'FINANC'"
				+"	  AND FGBTRND_LEDGER_IND        = 'O'"
				+"	  AND FGBTRND_FSYR_CODE         = '"+a�o+"'"
				+"	  AND FGBTRND_ORGN_CODE         = '"+org+"'"
				+"	AND FGBTRND_POSTING_PERIOD BETWEEN  ? AND  ?"
				+"	  GROUP BY FGBTRND_ACCT_CODE,"
				+"	    FTVACCT_TITLE"
				+"	  ORDER BY FGBTRND_ACCT_CODE"
				+"	  ) SIIF"
				+"	WHERE ORGN IN ('5A0004','5A0005','5A0006','5AA003','5AA007','5AA010')"
				+"	GROUP BY '5AREMVR'"
				+"	UNION ALL"
				+"	SELECT '5BADMIN' ORGN, "
				+"	  'GASTO REM NOMINA ADMINISTRATIVOS' TITULO,"
				+"	   SUM(PRESUPUESTO),"
				+"	  SUM(GASTOS)"
				+"	FROM"
				+"	  (SELECT FGBTRND_ACCT_CODE ORGN,"
				+"	    FTVACCT_TITLE,"
				+"	    SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0))  + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) Presupuesto ,"
				+"	    ( SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0))                                  + SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) - (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))) - SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)) - SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)) - SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0))) - SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) )GASTOS"
				+"	  FROM FIMSMGR.FGBTRND,"
				+"	    FIMSMGR.FTVFUND,"
				+"	    FIMSMGR.FTVORGN,"
				+"	    FIMSMGR.FTVACCT,"
				+"	    FIMSMGR.FTVPROG"
				+"	  WHERE FGBTRND_FUND_CODE       = FTVFUND_FUND_CODE"
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+"	  AND FGBTRND_ORGN_CODE         = FTVORGN_ORGN_CODE"
				+"	  AND FGBTRND_PROG_CODE         = FTVPROG_PROG_CODE"
				+"	  AND FGBTRND_ACCT_CODE         = FTVACCT_ACCT_CODE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVFUND_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVFUND_NCHG_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVORGN_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVORGN_NCHG_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVPROG_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVPROG_NCHG_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVACCT_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVACCT_NCHG_DATE"
				+"	  AND FTVFUND_FTYP_CODE        IN ('CA','CD','CN','FI','FR')"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+"	  AND FTVFUND_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FTVORGN_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FTVACCT_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FTVPROG_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FGBTRND.FGBTRND_LEDC_CODE = 'FINANC'"
				+"	  AND FGBTRND_LEDGER_IND        = 'O'"
				+"	  AND FGBTRND_FSYR_CODE         = '"+a�o+"'"
				+"	  AND FGBTRND_ORGN_CODE         = '"+org+"'"
				+"	AND FGBTRND_POSTING_PERIOD BETWEEN  ? AND  ?"
				+"	  GROUP BY FGBTRND_ACCT_CODE,"
				+"	    FTVACCT_TITLE"
				+"	  ORDER BY FGBTRND_ACCT_CODE"
				+"	  ) SIIF"
				+"	WHERE ORGN IN ('5BB001','5BB002','5BB003','5B0001','5B0004','5B0005','5B0006','5B0007','5B0008','5B0009')"
				+"	GROUP BY '5BADMIN'"
				+"	UNION ALL"
				+"	SELECT '5BREMVR' ORGN, "
				+"	  'GASTO REM VAR ADMINISTRATIVOS' TITULO,"
				+"	  SUM(PRESUPUESTO),"
				+"	  SUM(GASTOS)"
				+"	FROM"
				+"	  (SELECT FGBTRND_ACCT_CODE ORGN,"
				+"	    FTVACCT_TITLE,"
				+"	    SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0))  + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) Presupuesto ,"
				+"	    ( SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0))                                  + SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) - (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))) - SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)) - SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)) - SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0))) - SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) )GASTOS"
				+"	  FROM FIMSMGR.FGBTRND,"
				+"	    FIMSMGR.FTVFUND,"
				+"	    FIMSMGR.FTVORGN,"
				+"	    FIMSMGR.FTVACCT,"
				+"	    FIMSMGR.FTVPROG"
				+"	  WHERE FGBTRND_FUND_CODE       = FTVFUND_FUND_CODE"
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+"	  AND FGBTRND_ORGN_CODE         = FTVORGN_ORGN_CODE"
				+"	  AND FGBTRND_PROG_CODE         = FTVPROG_PROG_CODE"
				+"	  AND FGBTRND_ACCT_CODE         = FTVACCT_ACCT_CODE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVFUND_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVFUND_NCHG_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVORGN_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVORGN_NCHG_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVPROG_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVPROG_NCHG_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE    >= FTVACCT_EFF_DATE"
				+"	  AND FGBTRND_ACTIVITY_DATE     < FTVACCT_NCHG_DATE"
				+"	  AND FTVFUND_FTYP_CODE        IN ('CA','CD','CN','FI','FR')"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+"	  AND FTVFUND_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FTVORGN_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FTVACCT_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FTVPROG_DATA_ENTRY_IND    = 'Y'"
				+"	  AND FGBTRND.FGBTRND_LEDC_CODE = 'FINANC'"
				+"	  AND FGBTRND_LEDGER_IND        = 'O'"
				+"	  AND FGBTRND_FSYR_CODE         = '"+a�o+"'"
				+"	  AND FGBTRND_ORGN_CODE         = '"+org+"'"
				+"	AND FGBTRND_POSTING_PERIOD BETWEEN  ? AND  ?"
				+"	  GROUP BY FGBTRND_ACCT_CODE,"
				+"	    FTVACCT_TITLE"
				+"	  ORDER BY FGBTRND_ACCT_CODE"
				+"	  ) SIIF"
				+"	WHERE ORGN IN ('5B0002','5B0003')"
				+"	GROUP BY '5BREMVR'"
				+"	UNION ALL"
				+"	SELECT ORGN,TITULO ,PRESUPUESTO,GASTOS FROM ("
				+"	SELECT FGBTRND_ACCT_CODE ORGN ,"
				+"	  FTVACCT_TITLE TITULO,"
				+"	  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0))  + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) Presupuesto ,"
				+"	  ( SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0))                                  + SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) - (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))) - SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)) - SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)) - SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0))) - SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) )GASTOS"
				+"	FROM FIMSMGR.FGBTRND,"
				+"	  FIMSMGR.FTVFUND,"
				+"	  FIMSMGR.FTVORGN,"
				+"	  FIMSMGR.FTVACCT,"
				+"	  FIMSMGR.FTVPROG"
				+"	WHERE FGBTRND_FUND_CODE       = FTVFUND_FUND_CODE"
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+"	AND FGBTRND_ORGN_CODE         = FTVORGN_ORGN_CODE"
				+"	AND FGBTRND_PROG_CODE         = FTVPROG_PROG_CODE"
				+"	AND FGBTRND_ACCT_CODE         = FTVACCT_ACCT_CODE"
				+"	AND FGBTRND_ACTIVITY_DATE    >= FTVFUND_EFF_DATE"
				+"	AND FGBTRND_ACTIVITY_DATE     < FTVFUND_NCHG_DATE"
				+"	AND FGBTRND_ACTIVITY_DATE    >= FTVORGN_EFF_DATE"
				+"	AND FGBTRND_ACTIVITY_DATE     < FTVORGN_NCHG_DATE"
				+"	AND FGBTRND_ACTIVITY_DATE    >= FTVPROG_EFF_DATE"
				+"	AND FGBTRND_ACTIVITY_DATE     < FTVPROG_NCHG_DATE"
				+"	AND FGBTRND_ACTIVITY_DATE    >= FTVACCT_EFF_DATE"
				+"	AND FGBTRND_ACTIVITY_DATE     < FTVACCT_NCHG_DATE"
				+"	AND FTVFUND_FTYP_CODE        IN ('CA','CD','CN','FI','FR')"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+"	AND FTVFUND_DATA_ENTRY_IND    = 'Y'"
				+"	AND FTVORGN_DATA_ENTRY_IND    = 'Y'"
				+"	AND FTVACCT_DATA_ENTRY_IND    = 'Y'"
				+"	AND FTVPROG_DATA_ENTRY_IND    = 'Y'"
				+"	AND FGBTRND.FGBTRND_LEDC_CODE = 'FINANC'"
				+"	AND FGBTRND_LEDGER_IND        = 'O'"
				+"	AND FGBTRND_FSYR_CODE         = '"+a�o+"'"
				+"	AND FGBTRND_ORGN_CODE         = '"+org+"'"
				+"	AND FGBTRND_POSTING_PERIOD BETWEEN  ? AND  ?"
				+"	GROUP BY FGBTRND_ACCT_CODE,"
				+"	  FTVACCT_TITLE"
				+"	ORDER BY ORGN) SIIF_NO_REM"
				+"	WHERE ORGN NOT LIKE '5%'";

	
			
			
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			sent.setInt(1,0);
			sent.setInt(2,12);
			sent.setInt(3,0);
			sent.setInt(4,12);
			sent.setInt(5,0);
			sent.setInt(6,12);
			sent.setInt(7,0);
			sent.setInt(8,12);
			sent.setInt(9,0);
			sent.setInt(10,12);
			sent.setInt(11,0);
			sent.setInt(12,12);
			
			res = sent.executeQuery();
		//	System.out.println("Query cuentas getdetalleEjecucion " + sql);
			long acumulado = 0;
			long gastos = 0;
			
			while (res.next()) {
				Pres18POJO ss = new Pres18POJO();
				// String aux = res.getString(1);
				ss.setItedoc(res.getString(1)); // nombre + apellidos
				ss.setDesite(res.getString(2));
				
				/*ss.setPresum(res.getLong(3));
				acumulado = acumulado + res.getLong(3);
				ss.setUsadom(res.getLong(4));
				gastos = gastos + res.getLong(4);
				*/
				ss.setPreano(res.getLong(3)); //anual
				
				listContrato.put(res.getString(1), ss);
				
			}
			res.close();
			//	sent.setInt(0,mes);
			int mes_sql = 0;
			if(mes == 1) 
				mes_sql = mes - 1 ;
			else mes_sql = mes;
			
			sent.setInt(1,mes_sql);
			sent.setInt(2,mes);
			sent.setInt(3,mes_sql);
			sent.setInt(4,mes);
			sent.setInt(5,mes_sql);
			sent.setInt(6,mes);
			sent.setInt(7,mes_sql);
			sent.setInt(8,mes);
			sent.setInt(9,mes_sql);
			sent.setInt(10,mes);
			sent.setInt(11,mes_sql);
			sent.setInt(12,mes);
		
			res = sent.executeQuery();
			while (res.next()) {
								
				Pres18POJO ss = listContrato.get(res.getString(1));
			
				ss.setPresum(res.getLong(3));
				//acumulado = acumulado + res.getLong(3);
				ss.setUsadom(res.getLong(4));
		
			}
			
			res.close();
		//	System.out.println("Valor mes " + mes );
			sent.setInt(1,0);
			sent.setInt(2,mes);
			sent.setInt(3,0);
			sent.setInt(4,mes);
			sent.setInt(5,0);
			sent.setInt(6,mes);
			sent.setInt(7,0);
			sent.setInt(8,mes);
			sent.setInt(9,0);
			sent.setInt(10,mes);
			sent.setInt(11,0);
			sent.setInt(12,mes);
			res = sent.executeQuery();
			while (res.next()) {								
				Pres18POJO ss = listContrato.get(res.getString(1));		
				ss.setPresac(res.getLong(3));
				ss.setUsadac(res.getLong(4));									
			}
							
	         
			lista.addAll(listContrato.values()) ;
			Collections.sort(lista);
			
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error getdetalleEjecucion.getVerificaRut: "+ e.getMessage());
		}
		return lista;
	}
	
/*
	public Collection<Pres18POJO> getdetalleEjecucion(int a�oIN, String org,
			int mes) {
		System.out.println("Entra a getdetalleEjecucion valor mes " + mes);
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
		try {
			String sql = "";
			if (mes == -1) {
				sql = ""
						+ "SELECT  fg.FGBOPAL_ACCT_CODE, ft.FTVACCT_TITLE CUENTA , SUM(fg.FGBOPAL_00_adopt_bud + fg.fgbopal_00_bud_adjt +fg.FGBOPAL_01_adopt_bud + fg.fgbopal_01_bud_adjt + fg.FGBOPAL_02_adopt_bud + fg.fgbopal_02_bud_adjt + fg.FGBOPAL_03_adopt_bud + fg.fgbopal_03_bud_adjt + "
						+ " fg.FGBOPAL_04_adopt_bud + fg.fgbopal_04_bud_adjt + fg.FGBOPAL_05_adopt_bud + fg.fgbopal_05_bud_adjt + fg.FGBOPAL_06_adopt_bud + fg.fgbopal_06_bud_adjt + fg.FGBOPAL_07_adopt_bud + fg.fgbopal_07_bud_adjt + "
						+ " fg.FGBOPAL_08_adopt_bud + fg.fgbopal_08_bud_adjt + fg.FGBOPAL_09_adopt_bud + fg.fgbopal_09_bud_adjt + fg.FGBOPAL_10_adopt_bud + fg.fgbopal_10_bud_adjt + fg.FGBOPAL_11_adopt_bud + fg.fgbopal_11_bud_adjt + fg.FGBOPAL_12_adopt_bud + fg.fgbopal_12_bud_adjt) PRESUPUESTO , SUM(fg.fgbopal_00_ytd_actv + fg.fgbopal_00_encumb + fg.fgbopal_00_bud_rsrv + "
						+ " fg.fgbopal_01_ytd_actv + fg.fgbopal_01_encumb + fg.fgbopal_01_bud_rsrv + fg.fgbopal_02_ytd_actv + fg.fgbopal_02_encumb + fg.fgbopal_02_bud_rsrv + fg.fgbopal_03_ytd_actv + fg.fgbopal_03_encumb + fg.fgbopal_03_bud_rsrv +"
						+ " fg.fgbopal_04_ytd_actv + fg.fgbopal_04_encumb + fg.fgbopal_04_bud_rsrv + fg.fgbopal_05_ytd_actv + fg.fgbopal_05_encumb + fg.fgbopal_05_bud_rsrv + fg.fgbopal_06_ytd_actv + fg.fgbopal_06_encumb + fg.fgbopal_06_bud_rsrv +"
						+ " fg.fgbopal_07_ytd_actv + fg.fgbopal_07_encumb + fg.fgbopal_07_bud_rsrv +fg.fgbopal_08_ytd_actv + fg.fgbopal_08_encumb + fg.fgbopal_08_bud_rsrv + fg.fgbopal_09_ytd_actv + fg.fgbopal_09_encumb + fg.fgbopal_09_bud_rsrv +"
						+ " fg.fgbopal_10_ytd_actv + fg.fgbopal_10_encumb + fg.fgbopal_10_bud_rsrv +fg.fgbopal_11_ytd_actv + fg.fgbopal_11_encumb + fg.fgbopal_11_bud_rsrv + fg.fgbopal_12_ytd_actv + fg.fgbopal_12_encumb + fg.fgbopal_12_bud_rsrv) GASTOS "
						+ " FROM FGBOPAL fg ,FTVACCT ft  "
						+ " where  fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�osql
						+ "' AND FG.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' and FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE ,   ft.FTVACCT_TITLE , fg.FGBOPAL_ACCT_CODE";
			} else {
				if (mes == 0) {
					System.out.println("inicial");
					sql = ""
							+ "SELECT  fg.FGBOPAL_ACCT_CODE, ft.FTVACCT_TITLE CUENTA , SUM(fg.FGBOPAL_00_adopt_bud + fg.fgbopal_00_bud_adjt ) PRESUPUESTO ,  "
							+ " SUM(fg.fgbopal_00_ytd_actv + fg.fgbopal_00_encumb + fg.fgbopal_00_bud_rsrv) GASTOS "
							+ " FROM FGBOPAL fg ,FTVACCT ft  "
							+ " where  fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
							+ a�osql
							+ "' AND FG.FGBOPAL_ORGN_CODE = '"
							+ org
							+ "' and FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE "
							+ " GROUP BY fg.FGBOPAL_ORGN_CODE ,   ft.FTVACCT_TITLE , fg.FGBOPAL_ACCT_CODE";
				} else if (mes == 1) {
					System.out.println("enero");
					sql = ""
							+ "SELECT  fg.FGBOPAL_ACCT_CODE, ft.FTVACCT_TITLE CUENTA , SUM(fg.FGBOPAL_01_adopt_bud + fg.fgbopal_01_bud_adjt ) PRESUPUESTO ,  "
							+ " SUM(fg.fgbopal_01_ytd_actv + fg.fgbopal_01_encumb + fg.fgbopal_01_bud_rsrv) GASTOS "
							+ " FROM FGBOPAL fg ,FTVACCT ft  "
							+ " where  fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
							+ a�osql
							+ "' AND FG.FGBOPAL_ORGN_CODE = '"
							+ org
							+ "' and FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE "
							+ " GROUP BY fg.FGBOPAL_ORGN_CODE ,   ft.FTVACCT_TITLE , fg.FGBOPAL_ACCT_CODE";
				} else if (mes == 2) {
					System.out.println("febrero");
					sql = ""
							+ "SELECT  fg.FGBOPAL_ACCT_CODE, ft.FTVACCT_TITLE CUENTA , SUM(fg.FGBOPAL_02_adopt_bud + fg.fgbopal_02_bud_adjt ) PRESUPUESTO ,  "
							+ " SUM(fg.fgbopal_02_ytd_actv + fg.fgbopal_02_encumb + fg.fgbopal_02_bud_rsrv) GASTOS "
							+ " FROM FGBOPAL fg ,FTVACCT ft  "
							+ " where  fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
							+ a�osql
							+ "' AND FG.FGBOPAL_ORGN_CODE = '"
							+ org
							+ "' and FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE "
							+ " GROUP BY fg.FGBOPAL_ORGN_CODE ,   ft.FTVACCT_TITLE , fg.FGBOPAL_ACCT_CODE";
				} else if (mes == 3) {
					System.out.println("marzo");
					sql = ""
							+ "SELECT  fg.FGBOPAL_ACCT_CODE, ft.FTVACCT_TITLE CUENTA , SUM(fg.FGBOPAL_03_adopt_bud + fg.fgbopal_03_bud_adjt ) PRESUPUESTO ,  "
							+ " SUM(fg.fgbopal_03_ytd_actv + fg.fgbopal_03_encumb + fg.fgbopal_03_bud_rsrv) GASTOS "
							+ " FROM FGBOPAL fg ,FTVACCT ft  "
							+ " where  fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
							+ a�osql
							+ "' AND FG.FGBOPAL_ORGN_CODE = '"
							+ org
							+ "' and FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE "
							+ " GROUP BY fg.FGBOPAL_ORGN_CODE ,   ft.FTVACCT_TITLE , fg.FGBOPAL_ACCT_CODE";
				} else if (mes == 4) {
					System.out.println("abril");
					sql = ""
							+ "SELECT  fg.FGBOPAL_ACCT_CODE, ft.FTVACCT_TITLE CUENTA , SUM(fg.FGBOPAL_04_adopt_bud + fg.fgbopal_04_bud_adjt ) PRESUPUESTO ,  "
							+ " SUM(fg.fgbopal_04_ytd_actv + fg.fgbopal_04_encumb + fg.fgbopal_04_bud_rsrv) GASTOS "
							+ " FROM FGBOPAL fg ,FTVACCT ft  "
							+ " where  fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
							+ a�osql
							+ "' AND FG.FGBOPAL_ORGN_CODE = '"
							+ org
							+ "' and FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE "
							+ " GROUP BY fg.FGBOPAL_ORGN_CODE ,   ft.FTVACCT_TITLE , fg.FGBOPAL_ACCT_CODE";
				} else if (mes == 5) {
					System.out.println("mayo");
					sql = ""
							+ "SELECT  fg.FGBOPAL_ACCT_CODE, ft.FTVACCT_TITLE CUENTA , SUM(fg.FGBOPAL_05_adopt_bud + fg.fgbopal_05_bud_adjt ) PRESUPUESTO ,  "
							+ " SUM(fg.fgbopal_05_ytd_actv + fg.fgbopal_05_encumb + fg.fgbopal_05_bud_rsrv) GASTOS "
							+ " FROM FGBOPAL fg ,FTVACCT ft  "
							+ " where  fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
							+ a�osql
							+ "' AND FG.FGBOPAL_ORGN_CODE = '"
							+ org
							+ "' and FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE "
							+ " GROUP BY fg.FGBOPAL_ORGN_CODE ,   ft.FTVACCT_TITLE , fg.FGBOPAL_ACCT_CODE";
				} else if (mes == 6) {
					System.out.println("junio");
					sql = ""
							+ "SELECT  fg.FGBOPAL_ACCT_CODE, ft.FTVACCT_TITLE CUENTA , SUM(fg.FGBOPAL_06_adopt_bud + fg.fgbopal_06_bud_adjt ) PRESUPUESTO ,  "
							+ " SUM(fg.fgbopal_06_ytd_actv + fg.fgbopal_06_encumb + fg.fgbopal_06_bud_rsrv) GASTOS "
							+ " FROM FGBOPAL fg ,FTVACCT ft  "
							+ " where  fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
							+ a�osql
							+ "' AND FG.FGBOPAL_ORGN_CODE = '"
							+ org
							+ "' and FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE "
							+ " GROUP BY fg.FGBOPAL_ORGN_CODE ,   ft.FTVACCT_TITLE , fg.FGBOPAL_ACCT_CODE";
				} else if (mes == 7) {
					System.out.println("julio");
					sql = ""
							+ "SELECT  fg.FGBOPAL_ACCT_CODE, ft.FTVACCT_TITLE CUENTA , SUM(fg.FGBOPAL_07_adopt_bud + fg.fgbopal_07_bud_adjt ) PRESUPUESTO ,  "
							+ " SUM(fg.fgbopal_07_ytd_actv + fg.fgbopal_07_encumb + fg.fgbopal_07_bud_rsrv) GASTOS "
							+ " FROM FGBOPAL fg ,FTVACCT ft  "
							+ " where  fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
							+ a�osql
							+ "' AND FG.FGBOPAL_ORGN_CODE = '"
							+ org
							+ "' and FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE "
							+ " GROUP BY fg.FGBOPAL_ORGN_CODE ,   ft.FTVACCT_TITLE , fg.FGBOPAL_ACCT_CODE";
				} else if (mes == 8) {
					System.out.println("agosto");
					sql = ""
							+ "SELECT  fg.FGBOPAL_ACCT_CODE, ft.FTVACCT_TITLE CUENTA , SUM(fg.FGBOPAL_08_adopt_bud + fg.fgbopal_08_bud_adjt ) PRESUPUESTO ,  "
							+ " SUM(fg.fgbopal_08_ytd_actv + fg.fgbopal_08_encumb + fg.fgbopal_08_bud_rsrv) GASTOS "
							+ " FROM FGBOPAL fg ,FTVACCT ft  "
							+ " where  fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
							+ a�osql
							+ "' AND FG.FGBOPAL_ORGN_CODE = '"
							+ org
							+ "' and FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE "
							+ " GROUP BY fg.FGBOPAL_ORGN_CODE ,   ft.FTVACCT_TITLE , fg.FGBOPAL_ACCT_CODE";
				} else if (mes == 9) {
					System.out.println("septiembre");
					sql = ""
							+ "SELECT  fg.FGBOPAL_ACCT_CODE, ft.FTVACCT_TITLE CUENTA , SUM(fg.FGBOPAL_09_adopt_bud + fg.fgbopal_09_bud_adjt ) PRESUPUESTO ,  "
							+ " SUM(fg.fgbopal_09_ytd_actv + fg.fgbopal_09_encumb + fg.fgbopal_09_bud_rsrv) GASTOS "
							+ " FROM FGBOPAL fg ,FTVACCT ft  "
							+ " where  fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
							+ a�osql
							+ "' AND FG.FGBOPAL_ORGN_CODE = '"
							+ org
							+ "' and FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE "
							+ " GROUP BY fg.FGBOPAL_ORGN_CODE ,   ft.FTVACCT_TITLE , fg.FGBOPAL_ACCT_CODE";
				} else if (mes == 10) {
					System.out.println("OCtubre");
					sql = ""
							+ "SELECT  fg.FGBOPAL_ACCT_CODE, ft.FTVACCT_TITLE CUENTA , SUM(fg.FGBOPAL_10_adopt_bud + fg.fgbopal_10_bud_adjt ) PRESUPUESTO ,  "
							+ " SUM(fg.fgbopal_10_ytd_actv + fg.fgbopal_10_encumb + fg.fgbopal_10_bud_rsrv) GASTOS "
							+ " FROM FGBOPAL fg ,FTVACCT ft  "
							+ " where  fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
							+ a�osql
							+ "' AND FG.FGBOPAL_ORGN_CODE = '"
							+ org
							+ "' and FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE "
							+ " GROUP BY fg.FGBOPAL_ORGN_CODE ,   ft.FTVACCT_TITLE , fg.FGBOPAL_ACCT_CODE";
				} else if (mes == 11) {
					System.out.println("Noviembre");
					sql = ""
							+ "SELECT  fg.FGBOPAL_ACCT_CODE, ft.FTVACCT_TITLE CUENTA , SUM(fg.FGBOPAL_11_adopt_bud + fg.fgbopal_11_bud_adjt ) PRESUPUESTO ,  "
							+ " SUM(fg.fgbopal_11_ytd_actv + fg.fgbopal_11_encumb + fg.fgbopal_11_bud_rsrv) GASTOS "
							+ " FROM FGBOPAL fg ,FTVACCT ft  "
							+ " where  fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
							+ a�osql
							+ "' AND FG.FGBOPAL_ORGN_CODE = '"
							+ org
							+ "' and FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE "
							+ " GROUP BY fg.FGBOPAL_ORGN_CODE ,   ft.FTVACCT_TITLE , fg.FGBOPAL_ACCT_CODE";
				} else if (mes == 12) {
					System.out.println("Diciembre");
					sql = ""
							+ "SELECT  fg.FGBOPAL_ACCT_CODE, ft.FTVACCT_TITLE CUENTA , SUM(fg.FGBOPAL_12_adopt_bud + fg.fgbopal_12_bud_adjt ) PRESUPUESTO ,  "
							+ " SUM(fg.fgbopal_12_ytd_actv + fg.fgbopal_12_encumb + fg.fgbopal_12_bud_rsrv) GASTOS "
							+ " FROM FGBOPAL fg ,FTVACCT ft  "
							+ " where  fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
							+ a�osql
							+ "' AND FG.FGBOPAL_ORGN_CODE = '"
							+ org
							+ "' and FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE "
							+ " GROUP BY fg.FGBOPAL_ORGN_CODE ,   ft.FTVACCT_TITLE , fg.FGBOPAL_ACCT_CODE";
				}
			}

			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			long acumulado = 0;
			long gastos = 0;
			System.out.println("Query cuentas " + sql);
			while (res.next()) {
				Pres18POJO ss = new Pres18POJO();
				// String aux = res.getString(1);
				ss.setItedoc(res.getString(1)); // nombre + apellidos
				ss.setDesite(res.getString(2));
				// System.out.println(" DATOS ERROR " + res.getString(1) + " //
				// " + res.getString(2));
				ss.setPresum(res.getLong(3));
				acumulado = acumulado + res.getLong(3);
				ss.setUsadom(res.getLong(4));
				gastos = gastos + res.getLong(4);
				ss.setPresac(acumulado);
				ss.setUsadac(gastos);
				lista.add(ss);
			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error getdetalleEjecucion.getVerificaRut: "
					+ e.getMessage());
		}
		return lista;
	}
*/
/*	
	public Collection<Pres18POJO> getDetalles_respaldo_antes_agrupar(String org, int a�oIN, int mes) {
		// DETALLE DISPONIBILIDAD - Detalle de Movimientos
		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
		
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		String mesfinal = "";
		if (mes < 10) {
			mesfinal = "0" + Integer.toString(mes);
		} else {
			mesfinal = Integer.toString(mes);
		}
		String fecha_condicion = "";
		if (mes == 1) 
			fecha_condicion =  " and FGBTRND.FGBTRND_POSTING_PERIOD BETWEEN 0 AND "+ mes +" ";
		else
			fecha_condicion =  " and FGBTRND.FGBTRND_POSTING_PERIOD = "+ mes +" ";
		try {
			String sql = "";

			sql = "SELECT FGBTRND.FGBTRND_ACCT_CODE, "
				+ " DOC.FVVDOCT_DESC,	"
				+ " FGBTRND.FGBTRND_DOC_CODE,	"
				+ " DOC.FVRDCIN_DOCT_CODE,	"
				+ " DOC.FABINVH_VEND_INV_CODE,	"
				+ " TO_CHAR(FGBTRNH.FGBTRNH_TRANS_DATE,'dd/mm/yy HH24:mi:ss'),	"
				+ " FGBTRNH.FGBTRNH_TRANS_DESC,	"
				+ " CASE	"
				+ " WHEN FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5) THEN FGBTRND.FGBTRND_TRANS_AMT*(-1)	"
				+ " ELSE FGBTRND.FGBTRND_TRANS_AMT	"
				+ " END FGBTRND_TRANS_AMT,	"
				+ " NVL(DOC.FABINVH_OPEN_PAID_IND,'SI'),     	"
				+ " DOC.SPRIDEN_ID	"
				+ " FROM FIMSMGR.FGBTRND FGBTRND,	"
				+ " FIMSMGR.FTVACCT FTVACCT,	"
				+ " FIMSMGR.FTVFUND FTVFUND,	"
				+ " FIMSMGR.FTVORGN FTVORGN,	"
				+ " FIMSMGR.FTVPROG FTVPROG,	"
				+ " FIMSMGR.FGBTRNH FGBTRNH,	"
				+ " (SELECT DISTINCT FABINVH.FABINVH_VEND_PIDM,	"
				+ " FABINVH.FABINVH_VEND_INV_CODE,	"
				+ " FABINVH.FABINVH_OPEN_PAID_IND,	"
				+ " FABINVH.FABINVH_TGRP_CODE,	"
				+ " FVRDCIN.FVRDCIN_DOCT_CODE,	"
				+ " FVVDOCT.FVVDOCT_DESC,	"
				+ " SPRIDEN.SPRIDEN_ID,	"
				+ " FABINVH.FABINVH_CODE	"
				+ " FROM FIMSMGR.FABINVH FABINVH,	"
				+ " FIMSMGR.FVRDCIN FVRDCIN,	"
				+ " FIMSMGR.FVVDOCT FVVDOCT,	"
				+ " SATURN.SPRIDEN SPRIDEN	"
				+ " WHERE FABINVH.FABINVH_CODE = FVRDCIN.FVRDCIN_INVH_CODE	"
				+ " AND FVRDCIN.FVRDCIN_DOCT_CODE = FVVDOCT.FVVDOCT_CODE	"
				+ " AND FABINVH.FABINVH_VEND_PIDM = SPRIDEN.SPRIDEN_PIDM	"
				+ " AND SPRIDEN.SPRIDEN_CHANGE_IND IS NULL) DOC	"
				+ " WHERE FGBTRND.FGBTRND_ORGN_CODE = FTVORGN.FTVORGN_ORGN_CODE	"
				+ " AND FGBTRND.FGBTRND_FUND_CODE = FTVFUND.FTVFUND_FUND_CODE	"
				+ " AND FGBTRND.FGBTRND_PROG_CODE = FTVPROG.FTVPROG_PROG_CODE	"
				+ " AND FGBTRND.FGBTRND_ACCT_CODE = FTVACCT.FTVACCT_ACCT_CODE	"
				+ " AND FGBTRND.FGBTRND_FUND_CODE = FGBTRNH.FGBTRNH_FUND_CODE	"
				+ " AND FGBTRND.FGBTRND_PROG_CODE = FGBTRNH.FGBTRNH_PROG_CODE	"
				+ " AND FGBTRND.FGBTRND_ACCT_CODE = FTVACCT.FTVACCT_ACCT_CODE	"
				+ " AND FGBTRND.FGBTRND_ORGN_CODE = FGBTRNH.FGBTRNH_ORGN_CODE	"
				+ " AND FGBTRND.FGBTRND_LEDC_CODE = FGBTRNH.FGBTRNH_LEDC_CODE	"
				+ " AND FGBTRND.FGBTRND_DOC_CODE  = FGBTRNH.FGBTRNH_DOC_CODE	"
				+ " AND FGBTRND.FGBTRND_RUCL_CODE = FGBTRNH.FGBTRNH_RUCL_CODE	"
				+ " AND FGBTRND.FGBTRND_DOC_SEQ_CODE  = FGBTRNH.FGBTRNH_DOC_SEQ_CODE	"
				+ " AND FGBTRND.FGBTRND_SEQ_NUM  = FGBTRNH.FGBTRNH_SEQ_NUM	"
				+ " AND FGBTRND.FGBTRND_FSYR_CODE  = FGBTRNH.FGBTRNH_FSYR_CODE	"
				+ " AND FGBTRND.FGBTRND_SUBMISSION_NUMBER  = FGBTRNH.FGBTRNH_SUBMISSION_NUMBER	"
				+ " AND FGBTRND.FGBTRND_ITEM_NUM  = FGBTRNH.FGBTRNH_ITEM_NUM	"
				+ " AND FGBTRND.FGBTRND_DOC_CODE = DOC.FABINVH_CODE (+)	"
				+ " AND FGBTRND.FGBTRND_ACTIVITY_DATE >= FTVFUND.FTVFUND_EFF_DATE	"
				+ " AND FGBTRND.FGBTRND_ACTIVITY_DATE < FTVFUND.FTVFUND_NCHG_DATE	"
				+ " AND FGBTRND.FGBTRND_ACTIVITY_DATE >= FTVORGN.FTVORGN_EFF_DATE	"
				+ " AND FGBTRND.FGBTRND_ACTIVITY_DATE < FTVORGN.FTVORGN_NCHG_DATE	"
				+ " AND FGBTRND.FGBTRND_ACTIVITY_DATE >= FTVPROG.FTVPROG_EFF_DATE	"
				+ " AND FGBTRND.FGBTRND_ACTIVITY_DATE < FTVPROG.FTVPROG_NCHG_DATE	"
				+ " AND FGBTRND.FGBTRND_ACTIVITY_DATE >= FTVACCT.FTVACCT_EFF_DATE	"
				+ " AND FGBTRND.FGBTRND_ACTIVITY_DATE < FTVACCT.FTVACCT_NCHG_DATE	"
				+ " and FGBTRND.FGBTRND_LEDC_CODE ='FINANC'	"
				+ " and FGBTRND.FGBTRND_LEDGER_IND ='O'	"
				+  fecha_condicion
				+ " and FGBTRND.FGBTRND_FSYR_CODE = '"+ a�o +"'	"
				+ " and FTVFUND.FTVFUND_FTYP_CODE IN ('CA','CD','CN','FI','FR')	"
				+ " and FGBTRND.FGBTRND_ORGN_CODE ='"+ org +"'	"
				+ " and (FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)	"
				+ " or (FGBTRND.FGBTRND_FIELD_CODE = 2 and FGBTRND.FGBTRND_RUCL_CODE in ('BD02','BD08','BD09'))	"
				+ " OR (FGBTRND.FGBTRND_FIELD_CODE = 1 AND FGBTRND.FGBTRND_RUCL_CODE  = 'BD01' ))"
				+ " ORDER BY FGBTRND.FGBTRND_POSTING_PERIOD, FGBTRNH.FGBTRNH_TRANS_DATE	";
			//System.out.println("SQL Detalle disponible " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			int sw = 0;
			String simbolo = "";
			int total_final = 0;
			String pago = "";
			String aux = "";
			while (res.next()) {
				// String aux = res.getString(1);
				Pres18POJO ss = new Pres18POJO();
				sw = 1;
				aux = res.getString(1) ;
				
				if (aux == null) 
					aux = " ";
				ss.setItedoc(aux);
				aux = " ";
				aux = res.getString(2) ;
				if (aux == null) 
					aux = " ";
				ss.setNomtip(aux);
				aux = " ";
				aux = res.getString(3) ;
				if (aux == null) 
					aux = " ";
				ss.setNumdoc(aux);
				
				aux = " ";
				aux = res.getString(4) ;
				if (aux == null) 
					aux = " ";
				
				ss.setTipodoc(aux);
				aux = " ";
				aux = res.getString(5) ;
				if (aux == null) 
					aux = " ";
				
				ss.setNumdocbanner(aux);
				aux = " ";
				aux = res.getString(6) ;
				if (aux == null) 
					aux = " ";				
				
				ss.setFecdoc(aux);
				aux = " ";
				aux = res.getString(7) ;
				if (aux == null) 
					aux = " ";
				
				ss.setNompro(aux);
			
				
				int auxvalor = res.getInt(8);
				ss.setValdoc(auxvalor);
				ss.setNummes(mes);
				
				// calcula total
				
				
				  pago = res.getString(9) ;
				  
				  if(pago.endsWith("SI"))
					  pago = " " ;
				  else if (pago.equals("P"))
					  pago = "Pagado";
				  else if (pago.equals("O"))
					  pago = "No Pagado";
				  
					ss.setPagos(pago);
					ss.setDesuni(pago);
					//System.out.println ("Valor RUT " + res.getString(10));
					aux = " " ;
					aux = res.getString(10);
					if (aux == null) 
						aux = " ";
					ss.setRut(aux);
				// ss.setTotal_fin(0);
				// ss.setTotal_fin(total_final);
				lista.add(ss);
				//System.out.println(ss.getRut());
			}
			if (sw == 0) {
				Pres18POJO ss = new Pres18POJO();
				ss.setItedoc(org);
				ss.setCoduni(org);

			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB getDetalles: " + e.getMessage());
		}
		return lista;
	}

	*/
	
	/*
	public Collection<Pres18POJO> getDetalles_RESP(String org, int a�oIN,
			int mes) {

		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
	
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		String mesfinal = "";
		if (mes < 10) {
			mesfinal = "0" + Integer.toString(mes);
		} else {
			mesfinal = Integer.toString(mes);
		}
		try {
			String sql = "";

			sql = ""
					+ " SELECT FG.FGBTRND_ACCT_CODE, fv.FTVACCT_TITLE , FD.FTVDTYP_DESC ,FG.FGBTRND_DOC_CODE,to_char(FG.FGBTRND_ACTIVITY_DATE,'dd/mm/yy HH24:mi:ss'),FG.FGBTRND_PROG_CODE ,FG.FGBTRND_TRANS_AMT "
					+ " from fgbtrnd FG , ftvfund FT , ftvacct FV , FTVDTYP FD "
					+ " WHERE FG.FGBTRND_DOC_SEQ_CODE = FD.FTVDTYP_SEQ_NUM AND  FG.FGBTRND_ACCT_CODE = fv.FTVACCT_ACCT_CODE and FG.fgbtrnd_FUND_CODE = FT.FTVFUND_FUND_CODE AND FG.fgbtrnd_POSTING_PERIOD = '"
					+ mesfinal + "' AND FG.FGBTRND_ORGN_CODE = '" + org
					+ "' and FG.FGBTRND_FSYR_CODE = '" + a�o + "'";

			//System.out.println("SQL Detalle " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			long acumulado = 0;
			long gastos = 0;

			while (res.next()) {
				// String aux = res.getString(1);
				Pres18POJO ss = new Pres18POJO();


				ss.setItedoc(res.getString(1));

				ss.setDesite(res.getString(2));

				ss.setNomtip(res.getString(3));
				ss.setNumdoc(res.getString(4));
				ss.setFecdoc(res.getString(5));
				ss.setNompro(res.getString(6));			
				ss.setValdoc(res.getInt(7));
				

				ss.setNummes(mes);

				lista.add(ss);
			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB getDetalles: " + e.getMessage());
		}
		return lista;
	}
*/
	public int getDetallesTotal(String org, int a�oIN, int mes) {
		int total = 0;
		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
		
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		String mesfinal = "";
		if (mes < 10) {
			mesfinal = "0" + Integer.toString(mes);
		} else {
			mesfinal = Integer.toString(mes);
		}
		try {
			String sql = "";

			sql = ""
					+ " SELECT DISTINCT FG.FGBTRND_TRANS_AMT,FG.FGBTRND_FIELD_CODE "
					+ " from fgbtrnd FG "
					+ " LEFT JOIN FVRDCIN fvr ON  fvr.FVRDCIN_INVH_CODE    = fg.FGBTRND_DOC_CODE "
					+ " LEFT JOIN FABINVH fab ON  fg.FGBTRND_DOC_CODE       = fab.FABINVH_CODE,"
					+ "  ftvfund FT ,ftvacct FV ,FTVDTYP FD ,FGBTRNH FH "
					+ " WHERE FG.FGBTRND_DOC_SEQ_CODE = FD.FTVDTYP_SEQ_NUM AND FGBTRND_ACCT_CODE <> '4BD007' AND  FG.FGBTRND_ACCT_CODE = fv.FTVACCT_ACCT_CODE and FG.fgbtrnd_FUND_CODE = FT.FTVFUND_FUND_CODE AND FG.fgbtrnd_POSTING_PERIOD = '"
					+ mesfinal
					+ "' AND FG.FGBTRND_ORGN_CODE = '"
					+ org
					+ "' and FG.FGBTRND_FSYR_CODE = '"
					+ a�o
					+ "'"
					+ " and FH.FGBTRNH_RUCL_CODE = FG.FGBTRND_RUCL_CODE  and FH.FGBTRNH_SEQ_NUM = FG.FGBTRND_SEQ_NUM and fg.FGBTRND_DOC_CODE = fh.FGBTRNH_DOC_CODE and FG.FGBTRND_ACCT_CODE = FH.FGBTRNH_ACCT_CODE and FG.FGBTRND_ORGN_CODE = FH.FGBTRNH_ORGN_CODE and (fg.FGBTRND_LEDGER_IND = 'O' ) AND (fg.FGBTRND_FIELD_CODE    = 3 OR fg.FGBTRND_FIELD_CODE  = 4 OR fg.FGBTRND_FIELD_CODE  = 5  OR fg.FGBTRND_FIELD_CODE = 2  OR fg.FGBTRND_FIELD_CODE = 1) and fg.FGBTRND_LEDC_CODE = 'FINANC'  AND fh.FGBTRNH_LEDC_CODE = 'FINANC'  "; // and
																																																																																																																																				// fg.FGBTRND_DOC_SEQ_CODE
																																																																																																																																				// =
																																																																																																																																				// 3";

			//System.out.println("SQL Detalle " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			int sw = 0;
			String simbolo = "";
			int total_final = 0;
			while (res.next()) {
				// String aux = res.getString(1);

				sw = 1;

				simbolo = "";
				// calcula total
				simbolo = res.getString(2);
				if (simbolo.equals("03") || simbolo.equals("04")
						|| simbolo.equals("05")) {
					total_final = total_final - res.getInt(1);
				} else {
					total_final = total_final + res.getInt(1);
				}
				/*
				 * if (simbolo.equals("+") || simbolo.equals("C")) {
				 *  } else if(simbolo.equals("-") || simbolo.equals("D")) {
				 *  }
				 */
				// ss.setTotal_fin(0);

			}
			if (sw == 0) {
				total = 0;
			} else
				total = total_final;
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB getDetalles: " + e.getMessage());
		}
		return total;

	}

	public Collection<Pres18POJO> getDetalles(String org, int a�oIN, int mes, String cuenta) {
		
		//System.out.println("Entra a detalles valor cuenta " + cuenta);
		// DETALLE DISPONIBILIDAD - Detalle de Movimientos
		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
		
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		String mesfinal = "";
		if (mes < 10) {
			mesfinal = "0" + Integer.toString(mes);
		} else {
			mesfinal = Integer.toString(mes);
		}
		String fecha_condicion = "";
		String fecha_condicion_ingreso = "";
		String condicion = "";
		String condicion_cta = "";
		if(cuenta.equals("-1"))
			condicion = " WHERE ORGN NOT LIKE '5%'" ;
		else {
			condicion = " WHERE ORGN = '"+ cuenta +"' AND ORGN NOT LIKE '5%'" ;
			condicion_cta = " AND ORGN = '"+ cuenta +"'";
		}
		if (mes == 1) 
			fecha_condicion =  " and FGBTRND.FGBTRND_POSTING_PERIOD BETWEEN 0 AND "+ mes +" ";
		else
			fecha_condicion =  " and FGBTRND.FGBTRND_POSTING_PERIOD = "+ mes +" ";
		try {
			String sql = "";
			fecha_condicion_ingreso =  fecha_condicion.replace("FGBTRND.FGBTRND_POSTING_PERIOD", "MES");

			sql = "Select '5ACADOC' ORGN,'GASTO REM ACAD�MICOS'DETALLE , '' CODE,'' DOCT,'' BANNER,'' FECHA,'GASTO POR REMUNERACIONES' DESCR,SUM( FGBTRND_TRANS_AMT),'' RESP ,'' SPRIDEN from ("
				+ "	SELECT "
				+ "	  FGBTRND.FGBTRND_ACCT_CODE ORGN, "
				+ "	  CASE"
				+ "	    WHEN FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)"
				+ "	    THEN FGBTRND.FGBTRND_TRANS_AMT   *(-1)"
				+ "	    ELSE FGBTRND.FGBTRND_TRANS_AMT"
				+ "	  END  FGBTRND_TRANS_AMT  "
				+ "	FROM FIMSMGR.FGBTRND FGBTRND,"
				+ "	  FIMSMGR.FTVACCT FTVACCT,"
				+ "	  FIMSMGR.FTVFUND FTVFUND,"
				+ "	  FIMSMGR.FTVORGN FTVORGN,"
				+ "	  FIMSMGR.FTVPROG FTVPROG,"
				+ "	  FIMSMGR.FGBTRNH FGBTRNH,"
				+ "	  (SELECT DISTINCT FABINVH.FABINVH_VEND_PIDM,"
				+ "	    FABINVH.FABINVH_VEND_INV_CODE,"
				+ "	    FABINVH.FABINVH_OPEN_PAID_IND,"
				+ "	    FABINVH.FABINVH_TGRP_CODE,"
				+ "	    FVRDCIN.FVRDCIN_DOCT_CODE,"
				+ "	    FVVDOCT.FVVDOCT_DESC,"
				+ "	    SPRIDEN.SPRIDEN_ID,"
				+ "	    FABINVH.FABINVH_CODE"
				+ "	  FROM FIMSMGR.FABINVH FABINVH,"
				+ "	    FIMSMGR.FVRDCIN FVRDCIN,"
				+ "	    FIMSMGR.FVVDOCT FVVDOCT,"
				+ "	    SATURN.SPRIDEN SPRIDEN"
				+ "	  WHERE FABINVH.FABINVH_CODE      = FVRDCIN.FVRDCIN_INVH_CODE"
				+ "	  AND FVRDCIN.FVRDCIN_DOCT_CODE   = FVVDOCT.FVVDOCT_CODE"
				+ "	  AND FABINVH.FABINVH_VEND_PIDM   = SPRIDEN.SPRIDEN_PIDM"
				+ "	  AND SPRIDEN.SPRIDEN_CHANGE_IND IS NULL"
				+ "	  ) DOC"
				+ "	WHERE FGBTRND.FGBTRND_ORGN_CODE       = FTVORGN.FTVORGN_ORGN_CODE"
				+ " AND FGBTRND.FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FTVFUND.FTVFUND_FUND_CODE"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FTVPROG.FTVPROG_PROG_CODE"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FGBTRNH.FGBTRNH_FUND_CODE"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FGBTRNH.FGBTRNH_PROG_CODE"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE"
				+ "	AND FGBTRND.FGBTRND_ORGN_CODE         = FGBTRNH.FGBTRNH_ORGN_CODE"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         = FGBTRNH.FGBTRNH_LEDC_CODE"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = FGBTRNH.FGBTRNH_DOC_CODE"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE         = FGBTRNH.FGBTRNH_RUCL_CODE"
				+ "	AND FGBTRND.FGBTRND_DOC_SEQ_CODE      = FGBTRNH.FGBTRNH_DOC_SEQ_CODE"
				+ "	AND FGBTRND.FGBTRND_SEQ_NUM           = FGBTRNH.FGBTRNH_SEQ_NUM"
				+ "	AND FGBTRND.FGBTRND_FSYR_CODE         = FGBTRNH.FGBTRNH_FSYR_CODE"
				+ "	AND FGBTRND.FGBTRND_SUBMISSION_NUMBER = FGBTRNH.FGBTRNH_SUBMISSION_NUMBER"
				+ "	AND FGBTRND.FGBTRND_ITEM_NUM          = FGBTRNH.FGBTRNH_ITEM_NUM"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = DOC.FABINVH_CODE (+)"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVFUND.FTVFUND_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVFUND.FTVFUND_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVORGN.FTVORGN_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVORGN.FTVORGN_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVPROG.FTVPROG_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVPROG.FTVPROG_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVACCT.FTVACCT_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVACCT.FTVACCT_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         ='FINANC'"
				+ "	AND FGBTRND.FGBTRND_LEDGER_IND        ='O'"
				+ 	fecha_condicion
				+ "	and FGBTRND.FGBTRND_FSYR_CODE = '"+ a�o +"'"
				+ "	AND FTVFUND.FTVFUND_FTYP_CODE   IN ('CA','CD','CN','FI','FR')"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1','JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ "	and FGBTRND.FGBTRND_ORGN_CODE ='"+ org +"'"
				+ "	AND (FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)"
				+ "	OR (FGBTRND.FGBTRND_FIELD_CODE   = 2"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE   IN ('BD02','BD08','BD09','BD12','BD13'))"
				+ "	OR (FGBTRND.FGBTRND_FIELD_CODE   = 1"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE    = 'BD01' ))"
				+ "	ORDER BY FGBTRND.FGBTRND_POSTING_PERIOD,"
				+ "	  FGBTRNH.FGBTRNH_TRANS_DATE"
				+ "	  )  SIIF_REMUNERACIONES "
				+ "	  WHERE ORGN IN ('5A0001','5A0002','5AA005','5AA006','5A0007','5A0008','5A0009','5A0010','5A0011','5A0012','5AA001','5AA002','5AA008','5AA009')"
				+ condicion_cta
				+ "	  UNION ALL"
				
				+ "	Select '5APODOC' ORGN,'GASTO REM APOYOS ACA Y DOC' DETALLE , '' CODE,'' DOCT,'' BANNER,'' FECHA,'GASTO POR REMUNERACIONES' DESCR,SUM( FGBTRND_TRANS_AMT),'1' RESP ,'' SPRIDEN from ("
				+ "	SELECT "
				+ "	  FGBTRND.FGBTRND_ACCT_CODE ORGN, "
				+ "	  CASE"
				+ "	    WHEN FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)"
				+ "	    THEN FGBTRND.FGBTRND_TRANS_AMT   *(-1)"
				+ "	    ELSE FGBTRND.FGBTRND_TRANS_AMT"
				+ "	  END  FGBTRND_TRANS_AMT  "
				+ "	FROM FIMSMGR.FGBTRND FGBTRND,"
				+ "	  FIMSMGR.FTVACCT FTVACCT,"
				+ "	  FIMSMGR.FTVFUND FTVFUND,"
				+ "	  FIMSMGR.FTVORGN FTVORGN,"
				+ "	  FIMSMGR.FTVPROG FTVPROG,"
				+ "	  FIMSMGR.FGBTRNH FGBTRNH,"
				+ "	  (SELECT DISTINCT FABINVH.FABINVH_VEND_PIDM,"
				+ "	    FABINVH.FABINVH_VEND_INV_CODE,"
				+ "	    FABINVH.FABINVH_OPEN_PAID_IND,"
				+ "	    FABINVH.FABINVH_TGRP_CODE,"
				+ "	    FVRDCIN.FVRDCIN_DOCT_CODE,"
				+ "	    FVVDOCT.FVVDOCT_DESC,"
				+ "	    SPRIDEN.SPRIDEN_ID,"
				+ "	    FABINVH.FABINVH_CODE"
				+ "	  FROM FIMSMGR.FABINVH FABINVH,"
				+ "	    FIMSMGR.FVRDCIN FVRDCIN,"
				+ "	    FIMSMGR.FVVDOCT FVVDOCT,"
				+ "	    SATURN.SPRIDEN SPRIDEN"
				+ "	  WHERE FABINVH.FABINVH_CODE      = FVRDCIN.FVRDCIN_INVH_CODE"
				+ "	  AND FVRDCIN.FVRDCIN_DOCT_CODE   = FVVDOCT.FVVDOCT_CODE"
				+ "	  AND FABINVH.FABINVH_VEND_PIDM   = SPRIDEN.SPRIDEN_PIDM"
				+ "	  AND SPRIDEN.SPRIDEN_CHANGE_IND IS NULL"
				+ "	  ) DOC"
				+ "	WHERE FGBTRND.FGBTRND_ORGN_CODE       = FTVORGN.FTVORGN_ORGN_CODE"
				+ " AND FGBTRND.FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FTVFUND.FTVFUND_FUND_CODE"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FTVPROG.FTVPROG_PROG_CODE"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FGBTRNH.FGBTRNH_FUND_CODE"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FGBTRNH.FGBTRNH_PROG_CODE"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE"
				+ "	AND FGBTRND.FGBTRND_ORGN_CODE         = FGBTRNH.FGBTRNH_ORGN_CODE"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         = FGBTRNH.FGBTRNH_LEDC_CODE"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = FGBTRNH.FGBTRNH_DOC_CODE"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE         = FGBTRNH.FGBTRNH_RUCL_CODE"
				+ "	AND FGBTRND.FGBTRND_DOC_SEQ_CODE      = FGBTRNH.FGBTRNH_DOC_SEQ_CODE"
				+ "	AND FGBTRND.FGBTRND_SEQ_NUM           = FGBTRNH.FGBTRNH_SEQ_NUM"
				+ "	AND FGBTRND.FGBTRND_FSYR_CODE         = FGBTRNH.FGBTRNH_FSYR_CODE"
				+ "	AND FGBTRND.FGBTRND_SUBMISSION_NUMBER = FGBTRNH.FGBTRNH_SUBMISSION_NUMBER"
				+ "	AND FGBTRND.FGBTRND_ITEM_NUM          = FGBTRNH.FGBTRNH_ITEM_NUM"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = DOC.FABINVH_CODE (+)"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVFUND.FTVFUND_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVFUND.FTVFUND_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVORGN.FTVORGN_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVORGN.FTVORGN_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVPROG.FTVPROG_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVPROG.FTVPROG_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVACCT.FTVACCT_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVACCT.FTVACCT_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         ='FINANC'"
				+ "	AND FGBTRND.FGBTRND_LEDGER_IND        ='O'"
				+ 	fecha_condicion
				+ "	and FGBTRND.FGBTRND_FSYR_CODE = '"+ a�o +"'"
				+ "	AND FTVFUND.FTVFUND_FTYP_CODE   IN ('CA','CD','CN','FI','FR')"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1','JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ "	and FGBTRND.FGBTRND_ORGN_CODE ='"+ org +"'"
				+ "	AND (FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)"
				+ "	OR (FGBTRND.FGBTRND_FIELD_CODE   = 2"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE   IN ('BD02','BD08','BD09','BD12','BD13'))"
				+ "	OR (FGBTRND.FGBTRND_FIELD_CODE   = 1"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE    = 'BD01' ))"
				+ "	ORDER BY FGBTRND.FGBTRND_POSTING_PERIOD,"
				+ "	  FGBTRNH.FGBTRNH_TRANS_DATE"
				+ "	  )  SIIF_REMUNERACIONES "
				+ "	  WHERE ORGN IN ('5A0003','5AA004','5AA011')"
				+ condicion_cta
				+ "	  UNION ALL"
			
				+ "	Select '5AREMVR' ORGN,'GASTO REM HON, PT Y AYU' DETALLE , '' CODE,'' DOCT,'' BANNER,'' FECHA,'GASTO POR REMUNERACIONES' DESCR,SUM( FGBTRND_TRANS_AMT),'' RESP ,'' SPRIDEN from ("
				+ "	SELECT "
				+ "	  FGBTRND.FGBTRND_ACCT_CODE ORGN, "
				+ "	  CASE"
				+ "	    WHEN FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)"
				+ "	    THEN FGBTRND.FGBTRND_TRANS_AMT   *(-1)"
				+ "	    ELSE FGBTRND.FGBTRND_TRANS_AMT"
				+ "	  END  FGBTRND_TRANS_AMT  "
				+ "	FROM FIMSMGR.FGBTRND FGBTRND,"
				+ "	  FIMSMGR.FTVACCT FTVACCT,"
				+ "	  FIMSMGR.FTVFUND FTVFUND,"
				+ "	  FIMSMGR.FTVORGN FTVORGN,"
				+ "	  FIMSMGR.FTVPROG FTVPROG,"
				+ "	  FIMSMGR.FGBTRNH FGBTRNH,"
				+ "	  (SELECT DISTINCT FABINVH.FABINVH_VEND_PIDM,"
				+ "	    FABINVH.FABINVH_VEND_INV_CODE,"
				+ "	    FABINVH.FABINVH_OPEN_PAID_IND,"
				+ "	    FABINVH.FABINVH_TGRP_CODE,"
				+ "	    FVRDCIN.FVRDCIN_DOCT_CODE,"
				+ "	    FVVDOCT.FVVDOCT_DESC,"
				+ "	    SPRIDEN.SPRIDEN_ID,"
				+ "	    FABINVH.FABINVH_CODE"
				+ "	  FROM FIMSMGR.FABINVH FABINVH,"
				+ "	    FIMSMGR.FVRDCIN FVRDCIN,"
				+ "	    FIMSMGR.FVVDOCT FVVDOCT,"
				+ "	    SATURN.SPRIDEN SPRIDEN"
				+ "	  WHERE FABINVH.FABINVH_CODE      = FVRDCIN.FVRDCIN_INVH_CODE"
				+ "	  AND FVRDCIN.FVRDCIN_DOCT_CODE   = FVVDOCT.FVVDOCT_CODE"
				+ "	  AND FABINVH.FABINVH_VEND_PIDM   = SPRIDEN.SPRIDEN_PIDM"
				+ "	  AND SPRIDEN.SPRIDEN_CHANGE_IND IS NULL"
				+ "	  ) DOC"
				+ "	WHERE FGBTRND.FGBTRND_ORGN_CODE       = FTVORGN.FTVORGN_ORGN_CODE"
				+ " AND FGBTRND.FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FTVFUND.FTVFUND_FUND_CODE"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FTVPROG.FTVPROG_PROG_CODE"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FGBTRNH.FGBTRNH_FUND_CODE"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FGBTRNH.FGBTRNH_PROG_CODE"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE"
				+ "	AND FGBTRND.FGBTRND_ORGN_CODE         = FGBTRNH.FGBTRNH_ORGN_CODE"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         = FGBTRNH.FGBTRNH_LEDC_CODE"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = FGBTRNH.FGBTRNH_DOC_CODE"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE         = FGBTRNH.FGBTRNH_RUCL_CODE"
				+ "	AND FGBTRND.FGBTRND_DOC_SEQ_CODE      = FGBTRNH.FGBTRNH_DOC_SEQ_CODE"
				+ "	AND FGBTRND.FGBTRND_SEQ_NUM           = FGBTRNH.FGBTRNH_SEQ_NUM"
				+ "	AND FGBTRND.FGBTRND_FSYR_CODE         = FGBTRNH.FGBTRNH_FSYR_CODE"
				+ "	AND FGBTRND.FGBTRND_SUBMISSION_NUMBER = FGBTRNH.FGBTRNH_SUBMISSION_NUMBER"
				+ "	AND FGBTRND.FGBTRND_ITEM_NUM          = FGBTRNH.FGBTRNH_ITEM_NUM"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = DOC.FABINVH_CODE (+)"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVFUND.FTVFUND_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVFUND.FTVFUND_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVORGN.FTVORGN_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVORGN.FTVORGN_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVPROG.FTVPROG_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVPROG.FTVPROG_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVACCT.FTVACCT_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVACCT.FTVACCT_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         ='FINANC'"
				+ "	AND FGBTRND.FGBTRND_LEDGER_IND        ='O'"
				+ 	fecha_condicion
				+ "	and FGBTRND.FGBTRND_FSYR_CODE = '"+ a�o +"'"
				+ "	AND FTVFUND.FTVFUND_FTYP_CODE   IN ('CA','CD','CN','FI','FR')"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1','JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ "	and FGBTRND.FGBTRND_ORGN_CODE ='"+ org +"'"
				+ "	AND (FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)"
				+ "	OR (FGBTRND.FGBTRND_FIELD_CODE   = 2"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE   IN ('BD02','BD08','BD09','BD12','BD13'))"
				+ "	OR (FGBTRND.FGBTRND_FIELD_CODE   = 1"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE    = 'BD01' ))"
				+ "	ORDER BY FGBTRND.FGBTRND_POSTING_PERIOD,"
				+ "	  FGBTRNH.FGBTRNH_TRANS_DATE"
				+ "	  )  SIIF_REMUNERACIONES "
				+ "	  WHERE ORGN IN ('5A0004','5A0005','5A0006','5AA003','5AA007','5AA010')"
				+ condicion_cta
				+ "	  UNION ALL"
			
				+ "	Select '5BADMIN' ORGN,'GASTO REM NOMINA ADMINISTRATIVOS' DETALLE , '' CODE,'' DOCT,'' BANNER,'' FECHA,'GASTO POR REMUNERACIONES' DESCR,SUM( FGBTRND_TRANS_AMT),'' RESP ,'' SPRIDEN from ("
				+ "	SELECT "
				+ "	  FGBTRND.FGBTRND_ACCT_CODE ORGN, "
				+ "	  CASE"
				+ "	    WHEN FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)"
				+ "	    THEN FGBTRND.FGBTRND_TRANS_AMT   *(-1)"
				+ "	    ELSE FGBTRND.FGBTRND_TRANS_AMT"
				+ "	  END  FGBTRND_TRANS_AMT  "
				+ "	FROM FIMSMGR.FGBTRND FGBTRND,"
				+ "	  FIMSMGR.FTVACCT FTVACCT,"
				+ "	  FIMSMGR.FTVFUND FTVFUND,"
				+ "	  FIMSMGR.FTVORGN FTVORGN,"
				+ "	  FIMSMGR.FTVPROG FTVPROG,"
				+ "	  FIMSMGR.FGBTRNH FGBTRNH,"
				+ "	  (SELECT DISTINCT FABINVH.FABINVH_VEND_PIDM,"
				+ "	    FABINVH.FABINVH_VEND_INV_CODE,"
				+ "	    FABINVH.FABINVH_OPEN_PAID_IND,"
				+ "	    FABINVH.FABINVH_TGRP_CODE,"
				+ "	    FVRDCIN.FVRDCIN_DOCT_CODE,"
				+ "	    FVVDOCT.FVVDOCT_DESC,"
				+ "	    SPRIDEN.SPRIDEN_ID,"
				+ "	    FABINVH.FABINVH_CODE"
				+ "	  FROM FIMSMGR.FABINVH FABINVH,"
				+ "	    FIMSMGR.FVRDCIN FVRDCIN,"
				+ "	    FIMSMGR.FVVDOCT FVVDOCT,"
				+ "	    SATURN.SPRIDEN SPRIDEN"
				+ "	  WHERE FABINVH.FABINVH_CODE      = FVRDCIN.FVRDCIN_INVH_CODE"
				+ "	  AND FVRDCIN.FVRDCIN_DOCT_CODE   = FVVDOCT.FVVDOCT_CODE"
				+ "	  AND FABINVH.FABINVH_VEND_PIDM   = SPRIDEN.SPRIDEN_PIDM"
				+ "	  AND SPRIDEN.SPRIDEN_CHANGE_IND IS NULL"
				+ "	  ) DOC"
				+ "	WHERE FGBTRND.FGBTRND_ORGN_CODE       = FTVORGN.FTVORGN_ORGN_CODE"
				+ " AND FGBTRND.FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FTVFUND.FTVFUND_FUND_CODE"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FTVPROG.FTVPROG_PROG_CODE"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FGBTRNH.FGBTRNH_FUND_CODE"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FGBTRNH.FGBTRNH_PROG_CODE"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE"
				+ "	AND FGBTRND.FGBTRND_ORGN_CODE         = FGBTRNH.FGBTRNH_ORGN_CODE"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         = FGBTRNH.FGBTRNH_LEDC_CODE"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = FGBTRNH.FGBTRNH_DOC_CODE"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE         = FGBTRNH.FGBTRNH_RUCL_CODE"
				+ "	AND FGBTRND.FGBTRND_DOC_SEQ_CODE      = FGBTRNH.FGBTRNH_DOC_SEQ_CODE"
				+ "	AND FGBTRND.FGBTRND_SEQ_NUM           = FGBTRNH.FGBTRNH_SEQ_NUM"
				+ "	AND FGBTRND.FGBTRND_FSYR_CODE         = FGBTRNH.FGBTRNH_FSYR_CODE"
				+ "	AND FGBTRND.FGBTRND_SUBMISSION_NUMBER = FGBTRNH.FGBTRNH_SUBMISSION_NUMBER"
				+ "	AND FGBTRND.FGBTRND_ITEM_NUM          = FGBTRNH.FGBTRNH_ITEM_NUM"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = DOC.FABINVH_CODE (+)"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVFUND.FTVFUND_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVFUND.FTVFUND_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVORGN.FTVORGN_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVORGN.FTVORGN_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVPROG.FTVPROG_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVPROG.FTVPROG_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVACCT.FTVACCT_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVACCT.FTVACCT_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         ='FINANC'"
				+ "	AND FGBTRND.FGBTRND_LEDGER_IND        ='O'"
				+ 	fecha_condicion
				+ "	and FGBTRND.FGBTRND_FSYR_CODE = '"+ a�o +"'"
				+ "	AND FTVFUND.FTVFUND_FTYP_CODE   IN ('CA','CD','CN','FI','FR')"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1','JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ "	and FGBTRND.FGBTRND_ORGN_CODE ='"+ org +"'"
				+ "	AND (FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)"
				+ "	OR (FGBTRND.FGBTRND_FIELD_CODE   = 2"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE   IN ('BD02','BD08','BD09','BD12','BD13'))"
				+ "	OR (FGBTRND.FGBTRND_FIELD_CODE   = 1"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE    = 'BD01' ))"
				+ "	ORDER BY FGBTRND.FGBTRND_POSTING_PERIOD,"
				+ "	  FGBTRNH.FGBTRNH_TRANS_DATE"
				+ "	  )  SIIF_REMUNERACIONES "
				+ "	  WHERE ORGN IN ('5BB001','5BB002','5BB003','5B0001','5B0004','5B0005','5B0006','5B0007','5B0008','5B0009')"
				+ condicion_cta
				+ "	  UNION ALL"
			
				+ "	Select '5BREMVR' ORGN,'GASTO REM VAR ADMINISTRATIVOS' DETALLE , '' CODE,'' DOCT,'' BANNER,'' FECHA,'GASTO POR REMUNERACIONES' DESCR,SUM( FGBTRND_TRANS_AMT),'' RESP ,'' SPRIDEN from ("
				+ "	SELECT "
				+ "	  FGBTRND.FGBTRND_ACCT_CODE ORGN, "
				+ "	  CASE"
				+ "	    WHEN FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)"
				+ "	    THEN FGBTRND.FGBTRND_TRANS_AMT   *(-1)"
				+ "	    ELSE FGBTRND.FGBTRND_TRANS_AMT"
				+ "	  END  FGBTRND_TRANS_AMT  "
				+ "	FROM FIMSMGR.FGBTRND FGBTRND,"
				+ "	  FIMSMGR.FTVACCT FTVACCT,"
				+ "	  FIMSMGR.FTVFUND FTVFUND,"
				+ "	  FIMSMGR.FTVORGN FTVORGN,"
				+ "	  FIMSMGR.FTVPROG FTVPROG,"
				+ "	  FIMSMGR.FGBTRNH FGBTRNH,"
				+ "	  (SELECT DISTINCT FABINVH.FABINVH_VEND_PIDM,"
				+ "	    FABINVH.FABINVH_VEND_INV_CODE,"
				+ "	    FABINVH.FABINVH_OPEN_PAID_IND,"
				+ "	    FABINVH.FABINVH_TGRP_CODE,"
				+ "	    FVRDCIN.FVRDCIN_DOCT_CODE,"
				+ "	    FVVDOCT.FVVDOCT_DESC,"
				+ "	    SPRIDEN.SPRIDEN_ID,"
				+ "	    FABINVH.FABINVH_CODE"
				+ "	  FROM FIMSMGR.FABINVH FABINVH,"
				+ "	    FIMSMGR.FVRDCIN FVRDCIN,"
				+ "	    FIMSMGR.FVVDOCT FVVDOCT,"
				+ "	    SATURN.SPRIDEN SPRIDEN"
				+ "	  WHERE FABINVH.FABINVH_CODE      = FVRDCIN.FVRDCIN_INVH_CODE"
				+ "	  AND FVRDCIN.FVRDCIN_DOCT_CODE   = FVVDOCT.FVVDOCT_CODE"
				+ "	  AND FABINVH.FABINVH_VEND_PIDM   = SPRIDEN.SPRIDEN_PIDM"
				+ "	  AND SPRIDEN.SPRIDEN_CHANGE_IND IS NULL"
				+ "	  ) DOC"
				+ "	WHERE FGBTRND.FGBTRND_ORGN_CODE       = FTVORGN.FTVORGN_ORGN_CODE"
				+ " AND FGBTRND.FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FTVFUND.FTVFUND_FUND_CODE"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FTVPROG.FTVPROG_PROG_CODE"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FGBTRNH.FGBTRNH_FUND_CODE"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FGBTRNH.FGBTRNH_PROG_CODE"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE"
				+ "	AND FGBTRND.FGBTRND_ORGN_CODE         = FGBTRNH.FGBTRNH_ORGN_CODE"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         = FGBTRNH.FGBTRNH_LEDC_CODE"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = FGBTRNH.FGBTRNH_DOC_CODE"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE         = FGBTRNH.FGBTRNH_RUCL_CODE"
				+ "	AND FGBTRND.FGBTRND_DOC_SEQ_CODE      = FGBTRNH.FGBTRNH_DOC_SEQ_CODE"
				+ "	AND FGBTRND.FGBTRND_SEQ_NUM           = FGBTRNH.FGBTRNH_SEQ_NUM"
				+ "	AND FGBTRND.FGBTRND_FSYR_CODE         = FGBTRNH.FGBTRNH_FSYR_CODE"
				+ "	AND FGBTRND.FGBTRND_SUBMISSION_NUMBER = FGBTRNH.FGBTRNH_SUBMISSION_NUMBER"
				+ "	AND FGBTRND.FGBTRND_ITEM_NUM          = FGBTRNH.FGBTRNH_ITEM_NUM"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = DOC.FABINVH_CODE (+)"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVFUND.FTVFUND_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVFUND.FTVFUND_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVORGN.FTVORGN_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVORGN.FTVORGN_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVPROG.FTVPROG_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVPROG.FTVPROG_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVACCT.FTVACCT_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVACCT.FTVACCT_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         ='FINANC'"
				+ "	AND FGBTRND.FGBTRND_LEDGER_IND        ='O'"
				+ 	fecha_condicion
				+ "	and FGBTRND.FGBTRND_FSYR_CODE = '"+ a�o +"'"
				+ "	AND FTVFUND.FTVFUND_FTYP_CODE   IN ('CA','CD','CN','FI','FR')"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1','JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ "	and FGBTRND.FGBTRND_ORGN_CODE ='"+ org +"'"
				+ "	AND (FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)"
				+ "	OR (FGBTRND.FGBTRND_FIELD_CODE   = 2"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE   IN ('BD02','BD08','BD09','BD12','BD13'))"
				+ "	OR (FGBTRND.FGBTRND_FIELD_CODE   = 1"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE    = 'BD01' ))"
				+ "	ORDER BY FGBTRND.FGBTRND_POSTING_PERIOD,"
				+ "	  FGBTRNH.FGBTRNH_TRANS_DATE"
				+ "	  )  SIIF_REMUNERACIONES "
				+ "	  WHERE ORGN IN ('5B0002','5B0003')"
				+ condicion_cta
				+ "	  UNION ALL"
				+ "	  "
				+ "	  SELECT ORGN,DETALLE,CODE,DOCT,BANNER,FECHA,DESCRIPCION,FGBTRND_TRANS_AMT,RESP,SPRIDEN from (SELECT FGBTRND.FGBTRND_ACCT_CODE ORGN,"
				+ "	  DOC.FVVDOCT_DESC DETALLE,"
				+ "	  FGBTRND.FGBTRND_DOC_CODE CODE ,"
				+ "	  DOC.FVRDCIN_DOCT_CODE DOCT,"
				+ "	  DOC.FABINVH_VEND_INV_CODE BANNER,"
				+ "	  TO_CHAR(FGBTRNH.FGBTRNH_TRANS_DATE,'dd/mm/yy HH24:mi:ss') FECHA,"
				+ "	  FGBTRNH.FGBTRNH_TRANS_DESC DESCRIPCION,"
				+ "	  CASE"
				+ "	    WHEN FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)"
				+ "	    THEN FGBTRND.FGBTRND_TRANS_AMT   *(-1)"
				+ "	    ELSE FGBTRND.FGBTRND_TRANS_AMT"
				+ "	  END  FGBTRND_TRANS_AMT  ,"
				+ "	  NVL(DOC.FABINVH_OPEN_PAID_IND,'SI') RESP,"
				+ "	  DOC.SPRIDEN_ID SPRIDEN "
				+ "	FROM FIMSMGR.FGBTRND FGBTRND,"
				+ "	  FIMSMGR.FTVACCT FTVACCT,"
				+ "	  FIMSMGR.FTVFUND FTVFUND,"
				+ "	  FIMSMGR.FTVORGN FTVORGN,"
				+ "	  FIMSMGR.FTVPROG FTVPROG,"
				+ "	  FIMSMGR.FGBTRNH FGBTRNH,"
				+ "	  (SELECT DISTINCT FABINVH.FABINVH_VEND_PIDM,"
				+ "	    FABINVH.FABINVH_VEND_INV_CODE,"
				+ "	    FABINVH.FABINVH_OPEN_PAID_IND,"
				+ "	    FABINVH.FABINVH_TGRP_CODE,"
				+ "	    FVRDCIN.FVRDCIN_DOCT_CODE,"
				+ "	    FVVDOCT.FVVDOCT_DESC,"
				+ "	    SPRIDEN.SPRIDEN_ID,"
				+ "	    FABINVH.FABINVH_CODE"
				+ "	  FROM FIMSMGR.FABINVH FABINVH,"
				+ "	    FIMSMGR.FVRDCIN FVRDCIN,"
				+ "	    FIMSMGR.FVVDOCT FVVDOCT,"
				+ "	    SATURN.SPRIDEN SPRIDEN"
				+ "	  WHERE FABINVH.FABINVH_CODE      = FVRDCIN.FVRDCIN_INVH_CODE"
				+ "	  AND FVRDCIN.FVRDCIN_DOCT_CODE   = FVVDOCT.FVVDOCT_CODE"
				+ "	  AND FABINVH.FABINVH_VEND_PIDM   = SPRIDEN.SPRIDEN_PIDM"
				+ "	  AND SPRIDEN.SPRIDEN_CHANGE_IND IS NULL"
				+ "	  ) DOC"
				+ "	WHERE FGBTRND.FGBTRND_ORGN_CODE       = FTVORGN.FTVORGN_ORGN_CODE"
				+ " AND FGBTRND.FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FTVFUND.FTVFUND_FUND_CODE"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FTVPROG.FTVPROG_PROG_CODE"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FGBTRNH.FGBTRNH_FUND_CODE"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FGBTRNH.FGBTRNH_PROG_CODE"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE"
				+ "	AND FGBTRND.FGBTRND_ORGN_CODE         = FGBTRNH.FGBTRNH_ORGN_CODE"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         = FGBTRNH.FGBTRNH_LEDC_CODE"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = FGBTRNH.FGBTRNH_DOC_CODE"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE         = FGBTRNH.FGBTRNH_RUCL_CODE"
				+ "	AND FGBTRND.FGBTRND_DOC_SEQ_CODE      = FGBTRNH.FGBTRNH_DOC_SEQ_CODE"
				+ "	AND FGBTRND.FGBTRND_SEQ_NUM           = FGBTRNH.FGBTRNH_SEQ_NUM"
				+ "	AND FGBTRND.FGBTRND_FSYR_CODE         = FGBTRNH.FGBTRNH_FSYR_CODE"
				+ "	AND FGBTRND.FGBTRND_SUBMISSION_NUMBER = FGBTRNH.FGBTRNH_SUBMISSION_NUMBER"
				+ "	AND FGBTRND.FGBTRND_ITEM_NUM          = FGBTRNH.FGBTRNH_ITEM_NUM"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = DOC.FABINVH_CODE (+)"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVFUND.FTVFUND_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVFUND.FTVFUND_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVORGN.FTVORGN_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVORGN.FTVORGN_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVPROG.FTVPROG_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVPROG.FTVPROG_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVACCT.FTVACCT_EFF_DATE"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVACCT.FTVACCT_NCHG_DATE"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         ='FINANC'"
				+ "	AND FGBTRND.FGBTRND_LEDGER_IND        ='O'"
				+ 	fecha_condicion
				+ "	and FGBTRND.FGBTRND_FSYR_CODE = '"+ a�o +"'"
				+ "	AND FTVFUND.FTVFUND_FTYP_CODE   IN ('CA','CD','CN','FI','FR')"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1','JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ "	and FGBTRND.FGBTRND_ORGN_CODE ='"+ org +"'"
				+ "	AND (FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)"
				+ "	OR (FGBTRND.FGBTRND_FIELD_CODE   = 2"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE   IN ('BD02','BD08','BD09','BD12','BD13'))"
				+ "	OR (FGBTRND.FGBTRND_FIELD_CODE   = 1"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE    = 'BD01' ))"
				+ "	ORDER BY FGBTRND.FGBTRND_POSTING_PERIOD,"
				+ "	  FGBTRNH.FGBTRNH_TRANS_DATE ) SIIF  "
				+ condicion ;
			//	+ "	  GROUP BY ORGN,DETALLE,CODE,DOCT,BANNER,FECHA,DESCRIPCION,FGBTRND_TRANS_AMT,RESP,SPRIDEN";
				

		//	System.out.println("SQL Detalle disponible " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			int sw = 0;
			String simbolo = "";
			int total_final = 0;
			String pago = "";
			String aux = "";
			while (res.next()) {
				// String aux = res.getString(1);
				Pres18POJO ss = new Pres18POJO();
				sw = 1;
				if (res.getString(8) != null ) {
				aux = res.getString(1) ;
				
				if (aux == null) 
					aux = " ";
				ss.setItedoc(aux);
				aux = " ";
				aux = res.getString(2) ;
				if (aux == null) 
					aux = " ";
				ss.setNomtip(aux);
				aux = " ";
				aux = res.getString(3) ;
				if (aux == null) 
					aux = " ";
				ss.setNumdoc(aux);
				
				aux = " ";
				aux = res.getString(4) ;
				if (aux == null) 
					aux = " ";
				
				ss.setTipodoc(aux);
				aux = " ";
				aux = res.getString(5) ;
				if (aux == null) 
					aux = " ";
				
				ss.setNumdocbanner(aux);
				aux = " ";
				aux = res.getString(6) ;
				if (aux == null) 
					aux = " ";				
				
				ss.setFecdoc(aux);
				aux = " ";
				aux = res.getString(7) ;
				if (aux == null) 
					aux = " ";
				
				ss.setNompro(aux);
			/*	aux = " ";
				aux = res.getString(8) ;
				if (aux == null) 
					aux = " ";
				System.out.println ("Valor 8 " + res.getString(8));
				ss.setNompro(aux); */
				
				int auxvalor = res.getInt(8);
				ss.setValdoc(auxvalor);
				ss.setNummes(mes);
				
				// calcula total
				
				
				  pago = res.getString(9) ;
				 // System.out.println ("pago " + pago + " Cuenta " + res.getString(1));
				 if (pago == null) 
					 pago = " " ;
				 else {
				  if(pago.equals("SI"))
					  pago = " " ;
				  else if (pago.equals("P"))
					  pago = "Pagado";
				  else if (pago.equals("O"))
					  pago = "No Pagado";
				 }
				 
					ss.setPagos(pago);
					ss.setDesuni(pago);
					//System.out.println ("Valor RUT " + res.getString(10));
					aux = " " ;
					aux = res.getString(10);
					if (aux == null) 
						aux = " ";
					ss.setRut(aux);
				// ss.setTotal_fin(0);
				// ss.setTotal_fin(total_final);
				lista.add(ss);
			//	System.out.println(ss.getRut());
			}
			}
			if (sw == 0) {
				Pres18POJO ss = new Pres18POJO();
				ss.setItedoc(org);
				ss.setCoduni(org);

			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB getDetalles: " + e.getMessage());
		}
		return lista;
	}

	

	//public Collection<Pres18POJO> getDetalles(String org, int a�oIN, int mes) {
	/*
	public Collection<Pres18POJO> getDetallesRespaldo18_12_17(String org, int a�oIN, int mes) {
		// DETALLE DISPONIBILIDAD - Detalle de Movimientos
		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
	//	System.out.println("Entra a getDetalles ORG " + org + " a�o " + a�o	+ " mes " + mes);
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		String mesfinal = "";
		if (mes < 10) {
			mesfinal = "0" + Integer.toString(mes);
		} else {
			mesfinal = Integer.toString(mes);
		}
		try {
			String sql = "";

			
			sql = " SELECT DISTINCT FG.FGBTRND_ACCT_CODE, "
					+ "fv.FTVACCT_TITLE , "
					+ "FD.FTVDTYP_DESC ,  "
					+ "fg.FGBTRND_DOC_CODE banner, "
					+ "NVL(fvr.FVRDCIN_DOCT_CODE, ' ') , "
					+ "NVL(fab.FABINVH_VEND_INV_CODE, ' '), "
					+ "TO_CHAR(Fh.FGBTRNH_TRANS_DATE,'dd/mm/yy HH24:mi:ss') fecha_banner,"
					+ " FGBTRNH_TRANS_DESC IDENTIFICADOR , "					
					+ " FG.FGBTRND_TRANS_AMT , "
					+ "FG.FGBTRND_FIELD_CODE,"
					+ " NVL(fab.FABINVH_OPEN_PAID_IND,'SI'),"
					+ " NVL(SPR.SPRIDEN_ID ,' ')"
					+ " FROM SPRIDEN SPR ,fgbtrnd FG"
					+ " LEFT JOIN FVRDCIN fvr	ON fvr.FVRDCIN_INVH_CODE = fg.FGBTRND_DOC_CODE"
					+ " LEFT JOIN FABINVH fab	ON fg.FGBTRND_DOC_CODE = fab.FABINVH_CODE,"
					+ " ftvfund FT , ftvacct FV , FTVDTYP FD , FGBTRNH FH"
					+ " WHERE SPR.SPRIDEN_PIDM = FGBTRNH_VENDOR_PIDM AND FG.FGBTRND_DOC_SEQ_CODE = FD.FTVDTYP_SEQ_NUM AND FG.FGBTRND_ACCT_CODE = fv.FTVACCT_ACCT_CODE AND FG.fgbtrnd_FUND_CODE = FT.FTVFUND_FUND_CODE AND FG.fgbtrnd_POSTING_PERIOD = '"
					+ mesfinal
					+ "'	AND FG.FGBTRND_ORGN_CODE = '"
					+ org
					+ "' AND FG.FGBTRND_FSYR_CODE = '"
					+ a�o
					+ "'"
					+ " AND FH.FGBTRNH_RUCL_CODE      = FG.FGBTRND_RUCL_CODE"
					+ " AND FH.FGBTRNH_SEQ_NUM        = FG.FGBTRND_SEQ_NUM"
					+ " AND fg.FGBTRND_DOC_CODE       = fh.FGBTRNH_DOC_CODE"
					+ " AND FG.FGBTRND_ACCT_CODE      = FH.FGBTRNH_ACCT_CODE"
					+ " AND FG.FGBTRND_ORGN_CODE      = FH.FGBTRNH_ORGN_CODE"
					+ " AND (fg.FGBTRND_LEDGER_IND    = 'O' )"
					+ " AND (fg.FGBTRND_FIELD_CODE    = 3"
					+ " OR fg.FGBTRND_FIELD_CODE      = 4"
					+ " OR fg.FGBTRND_FIELD_CODE      = 5"
					+ " OR (fg.FGBTRND_FIELD_CODE      = 2 and (fg.FGBTRND_RUCL_CODE = 'BD02' or fg.FGBTRND_RUCL_CODE = 'BD08' or fg.FGBTRND_RUCL_CODE = 'BD09'))) "
					+  " AND fg.FGBTRND_LEDC_CODE      = 'FINANC'"
					+ " AND fh.FGBTRNH_LEDC_CODE      = 'FINANC'"
					+ " ORDER BY fecha_banner asc";
		//	System.out.println("SQL Detalle disponible " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			int sw = 0;
			String simbolo = "";
			int total_final = 0;
			String pago = "";
			while (res.next()) {
				// String aux = res.getString(1);
				Pres18POJO ss = new Pres18POJO();
				sw = 1;

				ss.setItedoc(res.getString(1));
				ss.setDesite(res.getString(2));
				ss.setNomtip(res.getString(3));
				ss.setNumdoc(res.getString(4));
				ss.setTipodoc(res.getString(5));
				ss.setNumdocbanner(res.getString(6));
				ss.setFecdoc(res.getString(7));
				ss.setNompro(res.getString(8));

				int aux = res.getInt(9);
 
				ss.setNummes(mes);
				simbolo = "";
				// calcula total
				simbolo = res.getString(10);
				if (simbolo.equals("03") || simbolo.equals("04")
						|| simbolo.equals("05")) {
					aux = aux * (-1);
					ss.setValdoc(aux);
				} else {
					//	
					ss.setValdoc(aux);
				}
				
				  pago = res.getString(11) ;
				  
				  if(pago.endsWith("SI"))
					  pago = "" ;
				  else if (pago.equals("P"))
					  pago = "Pagado";
				  else if (pago.equals("O"))
					  pago = "No Pagado";
				  
					ss.setPagos(pago);
					ss.setDesuni(pago);
					ss.setRut(res.getString(12));
				// ss.setTotal_fin(0);
				// ss.setTotal_fin(total_final);
				lista.add(ss);
			}
			if (sw == 0) {
				Pres18POJO ss = new Pres18POJO();
				ss.setItedoc(org);
				ss.setCoduni(org);

			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB getDetalles: " + e.getMessage());
		}
		return lista;
	}
*/
	/*
	public Collection<Pres18POJO> getMovimientos_respaldo_sin_agrupar(String org, int a�oIN,
			int mesInic, int mesFin) {
		if (mesInic == 1) mesInic = 0;

		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
		
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		String mesfinal = "";
		String mesinicio = "";
		String operacion = "";
		if (mesInic < 10) {
			mesinicio = "0" + Integer.toString(mesInic);
		} else {
			mesinicio = Integer.toString(mesInic);
		}
		if (mesFin < 10) {
			mesfinal = "0" + Integer.toString(mesFin);
		} else {
			mesfinal = Integer.toString(mesFin);
		}
		try {
			String sql = "";

			sql = ""
				+ " SELECT nvl (DOC.FVRDCIN_DOCT_CODE || '-' ||    DOC.FVRDCIN_DOCT_CODE,' ') documento,"
				+ "        NVL (DOC.SPRIDEN_ID,' ') RUT,"
				+ "        DOC.FABINVH_VEND_INV_CODE,"
				+ "         FGBTRND.FGBTRND_DOC_CODE,"
				+ "         TO_CHAR(FGBTRNH_TRANS_DATE,'dd/mm/yy HH24:mi:ss') FECHA,"
				+ "         FGBTRND_RUCL_CODE ,"
				+ "         FGBTRND.FGBTRND_ACCT_CODE CUENTA,"
				+ "        FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,"
				+ "         FGBTRNH_TRANS_DESC IDENTIFICADOR,"
				+ "         CASE"
				+ "  WHEN FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5) THEN FGBTRND.FGBTRND_TRANS_AMT*(-1)"
				+ "  ELSE FGBTRND.FGBTRND_TRANS_AMT"
				+ "        END FGBTRND_TRANS_AMT ,"
				+ "        FGBTRND.FGBTRND_POSTING_PERIOD MES,"
				+ "         FGBTRND.FGBTRND_USER_ID      "
				+ " FROM FIMSMGR.FGBTRND FGBTRND,"
				+ "        FIMSMGR.FTVACCT FTVACCT,"
				+ "        FIMSMGR.FTVFUND FTVFUND,"
				+ "        FIMSMGR.FTVORGN FTVORGN,"
				+ "        FIMSMGR.FTVPROG FTVPROG,"
				+ "        FIMSMGR.FGBTRNH FGBTRNH,"
				+ "        (SELECT DISTINCT FABINVH.FABINVH_VEND_PIDM,"
				+ " FABINVH.FABINVH_VEND_INV_CODE,"
				+ " FABINVH.FABINVH_OPEN_PAID_IND,"
				+ " FABINVH.FABINVH_TGRP_CODE,"
				+ " FVRDCIN.FVRDCIN_DOCT_CODE,"
				+ " FVVDOCT.FVVDOCT_DESC,"
				+ " SPRIDEN.SPRIDEN_ID,"
				+ " FABINVH.FABINVH_CODE"
				+ "         FROM FIMSMGR.FABINVH FABINVH,"
				+ " FIMSMGR.FVRDCIN FVRDCIN,"
				+ " FIMSMGR.FVVDOCT FVVDOCT,"
				+ " SATURN.SPRIDEN SPRIDEN"
				+ "         WHERE FABINVH.FABINVH_CODE = FVRDCIN.FVRDCIN_INVH_CODE"
				+ " AND FVRDCIN.FVRDCIN_DOCT_CODE = FVVDOCT.FVVDOCT_CODE"
				+ " AND FABINVH.FABINVH_VEND_PIDM = SPRIDEN.SPRIDEN_PIDM"
				+ " AND SPRIDEN.SPRIDEN_CHANGE_IND IS NULL) DOC"
				+ " WHERE FGBTRND.FGBTRND_ORGN_CODE = FTVORGN.FTVORGN_ORGN_CODE"
				+ " AND FGBTRND.FGBTRND_FUND_CODE = FTVFUND.FTVFUND_FUND_CODE"
				+ " AND FGBTRND.FGBTRND_PROG_CODE = FTVPROG.FTVPROG_PROG_CODE"
				+ " AND FGBTRND.FGBTRND_ACCT_CODE = FTVACCT.FTVACCT_ACCT_CODE"
				+ " AND FGBTRND.FGBTRND_FUND_CODE = FGBTRNH.FGBTRNH_FUND_CODE"
				+ " AND FGBTRND.FGBTRND_PROG_CODE = FGBTRNH.FGBTRNH_PROG_CODE"
				+ " AND FGBTRND.FGBTRND_ACCT_CODE = FTVACCT.FTVACCT_ACCT_CODE"
				+ " AND FGBTRND.FGBTRND_ORGN_CODE = FGBTRNH.FGBTRNH_ORGN_CODE"
				+ " AND FGBTRND.FGBTRND_LEDC_CODE = FGBTRNH.FGBTRNH_LEDC_CODE"
				+ " AND FGBTRND.FGBTRND_DOC_CODE  = FGBTRNH.FGBTRNH_DOC_CODE"
				+ " AND FGBTRND.FGBTRND_RUCL_CODE = FGBTRNH.FGBTRNH_RUCL_CODE"
				+ " AND FGBTRND.FGBTRND_DOC_SEQ_CODE  = FGBTRNH.FGBTRNH_DOC_SEQ_CODE"
				+ " AND FGBTRND.FGBTRND_SEQ_NUM  = FGBTRNH.FGBTRNH_SEQ_NUM"
				+ " AND FGBTRND.FGBTRND_FSYR_CODE  = FGBTRNH.FGBTRNH_FSYR_CODE"
				+ " AND FGBTRND.FGBTRND_SUBMISSION_NUMBER  = FGBTRNH.FGBTRNH_SUBMISSION_NUMBER"
				+ " AND FGBTRND.FGBTRND_ITEM_NUM  = FGBTRNH.FGBTRNH_ITEM_NUM"
				+ " AND FGBTRND.FGBTRND_DOC_CODE = DOC.FABINVH_CODE (+)"
				+ " AND FGBTRND.FGBTRND_ACTIVITY_DATE >= FTVFUND.FTVFUND_EFF_DATE"
				+ " AND FGBTRND.FGBTRND_ACTIVITY_DATE < FTVFUND.FTVFUND_NCHG_DATE"
				+ " AND FGBTRND.FGBTRND_ACTIVITY_DATE >= FTVORGN.FTVORGN_EFF_DATE"
				+ " AND FGBTRND.FGBTRND_ACTIVITY_DATE < FTVORGN.FTVORGN_NCHG_DATE"
				+ " AND FGBTRND.FGBTRND_ACTIVITY_DATE >= FTVPROG.FTVPROG_EFF_DATE"
				+ " AND FGBTRND.FGBTRND_ACTIVITY_DATE < FTVPROG.FTVPROG_NCHG_DATE"
				+ " AND FGBTRND.FGBTRND_ACTIVITY_DATE >= FTVACCT.FTVACCT_EFF_DATE"
				+ " AND FGBTRND.FGBTRND_ACTIVITY_DATE < FTVACCT.FTVACCT_NCHG_DATE"
				+ " and FGBTRND.FGBTRND_LEDC_CODE ='FINANC'"
				+ " and FGBTRND.FGBTRND_LEDGER_IND ='O'"
				+ " and FGBTRND.FGBTRND_POSTING_PERIOD BETWEEN "+mesInic+" AND "+ mesFin +""
				+ " and FGBTRND.FGBTRND_FSYR_CODE = '"+a�o +"'"
				+ " and FTVFUND.FTVFUND_FTYP_CODE IN ('CA','CD','CN','FI','FR')"
				+ " and FGBTRND.FGBTRND_ORGN_CODE ='"+ org +"'"
				+ " and FGBTRND.FGBTRND_FIELD_CODE IN (1,2,3,4,5)"
				+ " ORDER BY FGBTRND.FGBTRND_POSTING_PERIOD, FGBTRNH.FGBTRNH_TRANS_DATE" ;

		//	System.out.println("SQL getMovimientos " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			int sw = 0;
			String simbolo = "";
			int total_final = 0;
			int mes_anterior = 0;
			long total_aux_cargo = 0;
			long total_aux_abono = 0;
			int sw_cambio_mes = 0;
			int sw_cambiafila = 0;
			String aux = "" ;
			while (res.next()) {
				Pres18POJO ss = new Pres18POJO();
				sw = 1;

				int mes_actual = res.getInt(11);
			//	System.out.println("mes_actual " + mes_actual + " documento " + res.getString(3));
				if (mes_actual == mes_anterior) {
					aux = res.getString(1) ;
					if ((aux == null) && (aux.isEmpty()))
						aux = " ";
					else if (aux.trim().equals("-"))
						aux = " ";	
					ss.setNomtip(aux);
					aux = "";
					aux = res.getString(2) ;					
					if ((aux == null) && (aux.isEmpty()))
						aux = " ";
					ss.setRut(aux) ;
					aux = "";
					aux = res.getString(3) ;					
					if (aux == null)
						aux = " ";
					ss.setNumdoc(aux);
					aux = "";
					aux = res.getString(4) ;					
					if (aux == null)
						aux = " ";
					ss.setNumdocbanner(aux);// ; <- banner
					aux = "";
					aux = res.getString(5) ;					
					if (aux == null)
						aux = " ";
					ss.setFecdoc(aux);
					aux = "";
					aux = res.getString(6) ;					
					if (aux == null)
						aux = " ";
					ss.setCoduni(aux);
					aux = "";
					
					aux = res.getString(7) ;					
					if (aux == null)
						aux = " ";
					ss.setItedoc(aux);
					aux = "";
					aux = res.getString(8) ;					
					if (aux == null)
						aux = " ";
					ss.setUnidad(aux);
				
					
					aux = "";
					aux = res.getString(9) ;					
					if (aux == null)
						aux = " ";
					ss.setDesuni(aux);
				
					ss.setUsadac(res.getInt(10));
				//	ss.setPresac(res.getInt(10));
				/*	int aux_suma = res.getInt(8);
					if (simbolo.equals("03") || simbolo.equals("04")
							|| simbolo.equals("05")) {
						// aux = aux * (-1); Abonos
						ss.setUsadac(res.getInt(8));
						total_aux_abono = total_aux_abono + aux_suma;
					} else { // cargo
						//	
						ss.setPresac(res.getInt(8));
						total_aux_cargo = total_aux_cargo + aux_suma;
					} 
					ss.setDesite(String.valueOf(mes_actual));
					ss.setIndprc("");
					ss.setNompro(res.getString(12));
					// ss.setNummes(res.getInt(8));
					simbolo = "";
					
					
					// calcula total
					lista.add(ss);

				//} else if ((mes_actual != mes_anterior) && (mes_anterior > 0)) {
				} else if ((mes_actual != mes_anterior) ) {
					Pres18POJO ss2 = new Pres18POJO();
					ss2.setNomtip("");
					// ss.setNumdocbanner(res.getString(2));); <- banner

					//

					aux = res.getString(1) ;
					if ((aux == null) && (aux.isEmpty()))
						aux = " ";
					else if (aux.trim().equals("-"))
						aux = " ";	
					ss.setNomtip(aux);
					aux = "";
					aux = res.getString(2) ;					
					if ((aux == null) && (aux.isEmpty()))
						aux = " ";
					ss.setRut(aux) ;
					aux = "";
					aux = res.getString(3) ;					
					if (aux == null)
						aux = " ";
					ss.setNumdoc(aux);
					aux = "";
					aux = res.getString(4) ;					
					if (aux == null)
						aux = " ";
					ss.setNumdocbanner(aux);// ; <- banner
					aux = "";
					aux = res.getString(5) ;					
					if (aux == null)
						aux = " ";
					ss.setFecdoc(aux);
					aux = "";
					aux = res.getString(6) ;					
					if (aux == null)
						aux = " ";
					ss.setCoduni(aux);
					aux = "";
					
					aux = res.getString(7) ;					
					if (aux == null)
						aux = " ";
					ss.setItedoc(aux);
					aux = "";
					aux = res.getString(8) ;					
					if (aux == null)
						aux = " ";
					ss.setUnidad(aux);
				
					aux = "";
					aux = res.getString(9) ;					
					if (aux == null)
						aux = " ";
					ss.setDesuni(aux);
				
					ss.setUsadac(res.getInt(10));
					ss.setDesite(String.valueOf(mes_actual));
					ss.setIndprc("");
					ss.setNompro(res.getString(12));
					lista.add(ss);
					//

				} else if (mes_anterior == 0) {
					mes_anterior = mes_actual;
				}
			}
			if (sw == 0) {
				Pres18POJO ss = new Pres18POJO();
				ss.setItedoc(org);
				ss.setCoduni(org);

			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB getDetalles: " + e.getMessage());
		}
		return lista;
	}

*/
	public Collection<Pres18POJO> getMovimientos(String org, int a�oIN,
			int mesInic, int mesFin) {
		if (mesInic == 1) mesInic = 0;

		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
		
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		String mesfinal = "";
		String mesinicio = "";
		String operacion = "";
		if (mesInic < 10) {
			mesinicio = "0" + Integer.toString(mesInic);
		} else {
			mesinicio = Integer.toString(mesInic);
		}
		if (mesFin < 10) {
			mesfinal = "0" + Integer.toString(mesFin);
		} else {
			mesfinal = Integer.toString(mesFin);
		}
		try {
			String sql = "";

			sql = ""

				+ "	SELECT documento,RUT,DOC,BANNER, FECHA, MOV,'5ACADOC' CUENTA ,'GASTO REM ACAD�MICOS' DESCRIPCION_CUENTA,'GASTOS POR REMUNERACION' IDENTIFICADOR ,SUM(FGBTRND_TRANS_AMT),MES,IDE from	"
				+ "	(	"
				+ "	SELECT NVL (DOC.FVRDCIN_DOCT_CODE	"
				+ "	  || '-'	"
				+ "	  || DOC.FVRDCIN_DOCT_CODE,' ') documento,	"
				+ "	  NVL (DOC.SPRIDEN_ID,' ') RUT,	"
				+ "	  DOC.FABINVH_VEND_INV_CODE DOC,	"
				+ "	  FGBTRND.FGBTRND_DOC_CODE BANNER,	"
				+ "	  TO_CHAR(FGBTRNH_TRANS_DATE,'dd/mm/yy HH24:mi:ss') FECHA,	"
				+ "	  FGBTRND_RUCL_CODE MOV ,	"
				+ "	  FGBTRND.FGBTRND_ACCT_CODE CUENTA,	"
				+ "	  FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,	"
				+ "	  FGBTRNH_TRANS_DESC IDENTIFICADOR ,	"
				+ "	  CASE	"
				+ "	    WHEN FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)	"
				+ "	    THEN FGBTRND.FGBTRND_TRANS_AMT   *(-1)	"
				+ "	    ELSE FGBTRND.FGBTRND_TRANS_AMT 	"
				+ "	  END FGBTRND_TRANS_AMT 	"
				+ "	    ,	"
				+ "	  FGBTRND.FGBTRND_POSTING_PERIOD MES,	"
				+ "	  FGBTRND.FGBTRND_USER_ID IDE	"
				+ "	FROM FIMSMGR.FGBTRND FGBTRND,	"
				+ "	  FIMSMGR.FTVACCT FTVACCT,	"
				+ "	  FIMSMGR.FTVFUND FTVFUND,	"
				+ "	  FIMSMGR.FTVORGN FTVORGN,	"
				+ "	  FIMSMGR.FTVPROG FTVPROG,	"
				+ "	  FIMSMGR.FGBTRNH FGBTRNH,	"
				+ "	  (SELECT DISTINCT FABINVH.FABINVH_VEND_PIDM,	"
				+ "	    FABINVH.FABINVH_VEND_INV_CODE,	"
				+ "	    FABINVH.FABINVH_OPEN_PAID_IND,	"
				+ "	    FABINVH.FABINVH_TGRP_CODE,	"
				+ "	    FVRDCIN.FVRDCIN_DOCT_CODE,	"
				+ "	    FVVDOCT.FVVDOCT_DESC,	"
				+ "	    SPRIDEN.SPRIDEN_ID,	"
				+ "	    FABINVH.FABINVH_CODE	"
				+ "	  FROM FIMSMGR.FABINVH FABINVH,	"
				+ "	    FIMSMGR.FVRDCIN FVRDCIN,	"
				+ "	    FIMSMGR.FVVDOCT FVVDOCT,	"
				+ "	    SATURN.SPRIDEN SPRIDEN	"
				+ "	  WHERE FABINVH.FABINVH_CODE      = FVRDCIN.FVRDCIN_INVH_CODE	"
				+ "	  AND FVRDCIN.FVRDCIN_DOCT_CODE   = FVVDOCT.FVVDOCT_CODE	"
				+ "	  AND FABINVH.FABINVH_VEND_PIDM   = SPRIDEN.SPRIDEN_PIDM	"
				+ "	  AND SPRIDEN.SPRIDEN_CHANGE_IND IS NULL	"
				+ "	  ) DOC	"
				+ "	WHERE FGBTRND.FGBTRND_ORGN_CODE       = FTVORGN.FTVORGN_ORGN_CODE	"
				+ " AND FGBTRND.FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FTVFUND.FTVFUND_FUND_CODE	"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FTVPROG.FTVPROG_PROG_CODE	"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FGBTRNH.FGBTRNH_FUND_CODE	"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FGBTRNH.FGBTRNH_PROG_CODE	"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND.FGBTRND_ORGN_CODE         = FGBTRNH.FGBTRNH_ORGN_CODE	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         = FGBTRNH.FGBTRNH_LEDC_CODE	"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = FGBTRNH.FGBTRNH_DOC_CODE	"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE         = FGBTRNH.FGBTRNH_RUCL_CODE	"
				+ "	AND FGBTRND.FGBTRND_DOC_SEQ_CODE      = FGBTRNH.FGBTRNH_DOC_SEQ_CODE	"
				+ "	AND FGBTRND.FGBTRND_SEQ_NUM           = FGBTRNH.FGBTRNH_SEQ_NUM	"
				+ "	AND FGBTRND.FGBTRND_FSYR_CODE         = FGBTRNH.FGBTRNH_FSYR_CODE	"
				+ "	AND FGBTRND.FGBTRND_SUBMISSION_NUMBER = FGBTRNH.FGBTRNH_SUBMISSION_NUMBER	"
				+ "	AND FGBTRND.FGBTRND_ITEM_NUM          = FGBTRNH.FGBTRNH_ITEM_NUM	"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = DOC.FABINVH_CODE (+)	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVFUND.FTVFUND_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVFUND.FTVFUND_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVORGN.FTVORGN_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVORGN.FTVORGN_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVPROG.FTVPROG_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVPROG.FTVPROG_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVACCT.FTVACCT_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVACCT.FTVACCT_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         ='FINANC'	"
				+ "	AND FGBTRND.FGBTRND_LEDGER_IND        ='O'	"
				+ " and FGBTRND.FGBTRND_POSTING_PERIOD BETWEEN "+mesInic+" AND "+ mesFin +""
				+ " and FGBTRND.FGBTRND_FSYR_CODE = '"+a�o +"'"
				+ "	AND FTVFUND.FTVFUND_FTYP_CODE  IN ('CA','CD','CN','FI','FR')"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"	
				+ " and FGBTRND.FGBTRND_ORGN_CODE ='"+ org +"'"
				+ "	AND FGBTRND.FGBTRND_FIELD_CODE IN (1,2,3,4,5)	"
				+ "	  ) SIIF	"
				+ "	WHERE CUENTA IN ('5A0001','5A0002','5AA005','5AA006','5A0007','5A0008','5A0009','5A0010','5A0011','5A0012','5AA001','5AA002','5AA008','5AA009') 	"
				+ "	group by documento, RUT, DOC, BANNER, FECHA, MOV, '5ACADOC', 'GASTO',  MES, IDE	"
				+ "	UNION ALL	"
				+ "	SELECT documento,RUT,DOC,BANNER, FECHA, MOV,'5APODOC' CUENTA ,'GASTO REM APOYOS ACA Y DOC' DESCRIPCION_CUENTA,'GASTOS POR REMUNERACION' IDENTIFICADOR ,SUM(FGBTRND_TRANS_AMT),MES,IDE from	"
				+ "	(	"
				+ "	SELECT NVL (DOC.FVRDCIN_DOCT_CODE	"
				+ "	  || '-'	"
				+ "	  || DOC.FVRDCIN_DOCT_CODE,' ') documento,	"
				+ "	  NVL (DOC.SPRIDEN_ID,' ') RUT,	"
				+ "	  DOC.FABINVH_VEND_INV_CODE DOC,	"
				+ "	  FGBTRND.FGBTRND_DOC_CODE BANNER,	"
				+ "	  TO_CHAR(FGBTRNH_TRANS_DATE,'dd/mm/yy HH24:mi:ss') FECHA,	"
				+ "	  FGBTRND_RUCL_CODE MOV ,	"
				+ "	  FGBTRND.FGBTRND_ACCT_CODE CUENTA,	"
				+ "	  FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,	"
				+ "	  FGBTRNH_TRANS_DESC IDENTIFICADOR ,	"
				+ "	  CASE	"
				+ "	    WHEN FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)	"
				+ "	    THEN FGBTRND.FGBTRND_TRANS_AMT   *(-1)	"
				+ "	    ELSE FGBTRND.FGBTRND_TRANS_AMT 	"
				+ "	  END FGBTRND_TRANS_AMT 	"
				+ "	    ,	"
				+ "	  FGBTRND.FGBTRND_POSTING_PERIOD MES,	"
				+ "	  FGBTRND.FGBTRND_USER_ID IDE	"
				+ "	FROM FIMSMGR.FGBTRND FGBTRND,	"
				+ "	  FIMSMGR.FTVACCT FTVACCT,	"
				+ "	  FIMSMGR.FTVFUND FTVFUND,	"
				+ "	  FIMSMGR.FTVORGN FTVORGN,	"
				+ "	  FIMSMGR.FTVPROG FTVPROG,	"
				+ "	  FIMSMGR.FGBTRNH FGBTRNH,	"
				+ "	  (SELECT DISTINCT FABINVH.FABINVH_VEND_PIDM,	"
				+ "	    FABINVH.FABINVH_VEND_INV_CODE,	"
				+ "	    FABINVH.FABINVH_OPEN_PAID_IND,	"
				+ "	    FABINVH.FABINVH_TGRP_CODE,	"
				+ "	    FVRDCIN.FVRDCIN_DOCT_CODE,	"
				+ "	    FVVDOCT.FVVDOCT_DESC,	"
				+ "	    SPRIDEN.SPRIDEN_ID,	"
				+ "	    FABINVH.FABINVH_CODE	"
				+ "	  FROM FIMSMGR.FABINVH FABINVH,	"
				+ "	    FIMSMGR.FVRDCIN FVRDCIN,	"
				+ "	    FIMSMGR.FVVDOCT FVVDOCT,	"
				+ "	    SATURN.SPRIDEN SPRIDEN	"
				+ "	  WHERE FABINVH.FABINVH_CODE      = FVRDCIN.FVRDCIN_INVH_CODE	"
				+ "	  AND FVRDCIN.FVRDCIN_DOCT_CODE   = FVVDOCT.FVVDOCT_CODE	"
				+ "	  AND FABINVH.FABINVH_VEND_PIDM   = SPRIDEN.SPRIDEN_PIDM	"
				+ "	  AND SPRIDEN.SPRIDEN_CHANGE_IND IS NULL	"
				+ "	  ) DOC	"
				+ "	WHERE FGBTRND.FGBTRND_ORGN_CODE       = FTVORGN.FTVORGN_ORGN_CODE	"
				+ " AND FGBTRND.FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FTVFUND.FTVFUND_FUND_CODE	"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FTVPROG.FTVPROG_PROG_CODE	"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FGBTRNH.FGBTRNH_FUND_CODE	"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FGBTRNH.FGBTRNH_PROG_CODE	"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND.FGBTRND_ORGN_CODE         = FGBTRNH.FGBTRNH_ORGN_CODE	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         = FGBTRNH.FGBTRNH_LEDC_CODE	"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = FGBTRNH.FGBTRNH_DOC_CODE	"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE         = FGBTRNH.FGBTRNH_RUCL_CODE	"
				+ "	AND FGBTRND.FGBTRND_DOC_SEQ_CODE      = FGBTRNH.FGBTRNH_DOC_SEQ_CODE	"
				+ "	AND FGBTRND.FGBTRND_SEQ_NUM           = FGBTRNH.FGBTRNH_SEQ_NUM	"
				+ "	AND FGBTRND.FGBTRND_FSYR_CODE         = FGBTRNH.FGBTRNH_FSYR_CODE	"
				+ "	AND FGBTRND.FGBTRND_SUBMISSION_NUMBER = FGBTRNH.FGBTRNH_SUBMISSION_NUMBER	"
				+ "	AND FGBTRND.FGBTRND_ITEM_NUM          = FGBTRNH.FGBTRNH_ITEM_NUM	"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = DOC.FABINVH_CODE (+)	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVFUND.FTVFUND_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVFUND.FTVFUND_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVORGN.FTVORGN_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVORGN.FTVORGN_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVPROG.FTVPROG_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVPROG.FTVPROG_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVACCT.FTVACCT_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVACCT.FTVACCT_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         ='FINANC'	"
				+ "	AND FGBTRND.FGBTRND_LEDGER_IND        ='O'	"
				+ " and FGBTRND.FGBTRND_POSTING_PERIOD BETWEEN "+mesInic+" AND "+ mesFin +""
				+ " and FGBTRND.FGBTRND_FSYR_CODE = '"+a�o +"'"
				+ "	AND FTVFUND.FTVFUND_FTYP_CODE  IN ('CA','CD','CN','FI','FR')	"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ " and FGBTRND.FGBTRND_ORGN_CODE ='"+ org +"'"
				+ "	AND FGBTRND.FGBTRND_FIELD_CODE IN (1,2,3,4,5)	"
				+ "	  ) SIIF	"
				+ "	WHERE CUENTA IN ('5A0003','5AA004','5AA011')	"
				+ "	group by documento, RUT, DOC, BANNER, FECHA, MOV, '5APODOC', 'GASTO',  MES, IDE	"
				+ "	UNION ALL	"
				+ "	SELECT documento,RUT,DOC,BANNER, FECHA, MOV,'5AREMVR' CUENTA , 'GASTO REM HON, PT Y AYU' DESCRIPCION_CUENTA,'GASTOS POR REMUNERACION' IDENTIFICADOR ,SUM(FGBTRND_TRANS_AMT),MES,IDE from	"
				+ "	(	"
				+ "	SELECT NVL (DOC.FVRDCIN_DOCT_CODE	"
				+ "	  || '-'	"
				+ "	  || DOC.FVRDCIN_DOCT_CODE,' ') documento,	"
				+ "	  NVL (DOC.SPRIDEN_ID,' ') RUT,	"
				+ "	  DOC.FABINVH_VEND_INV_CODE DOC,	"
				+ "	  FGBTRND.FGBTRND_DOC_CODE BANNER,	"
				+ "	  TO_CHAR(FGBTRNH_TRANS_DATE,'dd/mm/yy HH24:mi:ss') FECHA,	"
				+ "	  FGBTRND_RUCL_CODE MOV ,	"
				+ "	  FGBTRND.FGBTRND_ACCT_CODE CUENTA,	"
				+ "	  FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,	"
				+ "	  FGBTRNH_TRANS_DESC IDENTIFICADOR ,	"
				+ "	  CASE	"
				+ "	    WHEN FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)	"
				+ "	    THEN FGBTRND.FGBTRND_TRANS_AMT   *(-1)	"
				+ "	    ELSE FGBTRND.FGBTRND_TRANS_AMT 	"
				+ "	  END FGBTRND_TRANS_AMT 	"
				+ "	    ,	"
				+ "	  FGBTRND.FGBTRND_POSTING_PERIOD MES,	"
				+ "	  FGBTRND.FGBTRND_USER_ID IDE	"
				+ "	FROM FIMSMGR.FGBTRND FGBTRND,	"
				+ "	  FIMSMGR.FTVACCT FTVACCT,	"
				+ "	  FIMSMGR.FTVFUND FTVFUND,	"
				+ "	  FIMSMGR.FTVORGN FTVORGN,	"
				+ "	  FIMSMGR.FTVPROG FTVPROG,	"
				+ "	  FIMSMGR.FGBTRNH FGBTRNH,	"
				+ "	  (SELECT DISTINCT FABINVH.FABINVH_VEND_PIDM,	"
				+ "	    FABINVH.FABINVH_VEND_INV_CODE,	"
				+ "	    FABINVH.FABINVH_OPEN_PAID_IND,	"
				+ "	    FABINVH.FABINVH_TGRP_CODE,	"
				+ "	    FVRDCIN.FVRDCIN_DOCT_CODE,	"
				+ "	    FVVDOCT.FVVDOCT_DESC,	"
				+ "	    SPRIDEN.SPRIDEN_ID,	"
				+ "	    FABINVH.FABINVH_CODE	"
				+ "	  FROM FIMSMGR.FABINVH FABINVH,	"
				+ "	    FIMSMGR.FVRDCIN FVRDCIN,	"
				+ "	    FIMSMGR.FVVDOCT FVVDOCT,	"
				+ "	    SATURN.SPRIDEN SPRIDEN	"
				+ "	  WHERE FABINVH.FABINVH_CODE      = FVRDCIN.FVRDCIN_INVH_CODE	"
				+ "	  AND FVRDCIN.FVRDCIN_DOCT_CODE   = FVVDOCT.FVVDOCT_CODE	"
				+ "	  AND FABINVH.FABINVH_VEND_PIDM   = SPRIDEN.SPRIDEN_PIDM	"
				+ "	  AND SPRIDEN.SPRIDEN_CHANGE_IND IS NULL	"
				+ "	  ) DOC	"
				+ "	WHERE FGBTRND.FGBTRND_ORGN_CODE       = FTVORGN.FTVORGN_ORGN_CODE	"
				+ " AND FGBTRND.FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FTVFUND.FTVFUND_FUND_CODE	"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FTVPROG.FTVPROG_PROG_CODE	"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FGBTRNH.FGBTRNH_FUND_CODE	"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FGBTRNH.FGBTRNH_PROG_CODE	"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND.FGBTRND_ORGN_CODE         = FGBTRNH.FGBTRNH_ORGN_CODE	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         = FGBTRNH.FGBTRNH_LEDC_CODE	"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = FGBTRNH.FGBTRNH_DOC_CODE	"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE         = FGBTRNH.FGBTRNH_RUCL_CODE	"
				+ "	AND FGBTRND.FGBTRND_DOC_SEQ_CODE      = FGBTRNH.FGBTRNH_DOC_SEQ_CODE	"
				+ "	AND FGBTRND.FGBTRND_SEQ_NUM           = FGBTRNH.FGBTRNH_SEQ_NUM	"
				+ "	AND FGBTRND.FGBTRND_FSYR_CODE         = FGBTRNH.FGBTRNH_FSYR_CODE	"
				+ "	AND FGBTRND.FGBTRND_SUBMISSION_NUMBER = FGBTRNH.FGBTRNH_SUBMISSION_NUMBER	"
				+ "	AND FGBTRND.FGBTRND_ITEM_NUM          = FGBTRNH.FGBTRNH_ITEM_NUM	"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = DOC.FABINVH_CODE (+)	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVFUND.FTVFUND_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVFUND.FTVFUND_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVORGN.FTVORGN_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVORGN.FTVORGN_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVPROG.FTVPROG_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVPROG.FTVPROG_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVACCT.FTVACCT_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVACCT.FTVACCT_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         ='FINANC'	"
				+ "	AND FGBTRND.FGBTRND_LEDGER_IND        ='O'	"
				+ " and FGBTRND.FGBTRND_POSTING_PERIOD BETWEEN "+mesInic+" AND "+ mesFin +""
				+ " and FGBTRND.FGBTRND_FSYR_CODE = '"+a�o +"'"
				+ "	AND FTVFUND.FTVFUND_FTYP_CODE  IN ('CA','CD','CN','FI','FR')	"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ " and FGBTRND.FGBTRND_ORGN_CODE ='"+ org +"'"
				+ "	AND FGBTRND.FGBTRND_FIELD_CODE IN (1,2,3,4,5)	"
				+ "	  ) SIIF	"
				+ "	WHERE CUENTA IN ('5A0004','5A0005','5A0006','5AA003','5AA007','5AA010')	"
				+ "	group by documento, RUT, DOC, BANNER, FECHA, MOV, '5AREMVR', 'GASTO',  MES, IDE	"
				+ "	UNION ALL	"
				+ "	SELECT documento,RUT,DOC,BANNER, FECHA, MOV,'5BADMIN' CUENTA ,'GASTO REM NOMINA ADMINISTRATIVOS' DESCRIPCION_CUENTA,'GASTOS POR REMUNERACION' IDENTIFICADOR ,SUM(FGBTRND_TRANS_AMT),MES,IDE from	"
				+ "	(	"
				+ "	SELECT NVL (DOC.FVRDCIN_DOCT_CODE	"
				+ "	  || '-'	"
				+ "	  || DOC.FVRDCIN_DOCT_CODE,' ') documento,	"
				+ "	  NVL (DOC.SPRIDEN_ID,' ') RUT,	"
				+ "	  DOC.FABINVH_VEND_INV_CODE DOC,	"
				+ "	  FGBTRND.FGBTRND_DOC_CODE BANNER,	"
				+ "	  TO_CHAR(FGBTRNH_TRANS_DATE,'dd/mm/yy HH24:mi:ss') FECHA,	"
				+ "	  FGBTRND_RUCL_CODE MOV ,	"
				+ "	  FGBTRND.FGBTRND_ACCT_CODE CUENTA,	"
				+ "	  FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,	"
				+ "	  FGBTRNH_TRANS_DESC IDENTIFICADOR ,	"
				+ "	  CASE	"
				+ "	    WHEN FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)	"
				+ "	    THEN FGBTRND.FGBTRND_TRANS_AMT   *(-1)	"
				+ "	    ELSE FGBTRND.FGBTRND_TRANS_AMT 	"
				+ "	  END FGBTRND_TRANS_AMT 	"
				+ "	    ,	"
				+ "	  FGBTRND.FGBTRND_POSTING_PERIOD MES,	"
				+ "	  FGBTRND.FGBTRND_USER_ID IDE	"
				+ "	FROM FIMSMGR.FGBTRND FGBTRND,	"
				+ "	  FIMSMGR.FTVACCT FTVACCT,	"
				+ "	  FIMSMGR.FTVFUND FTVFUND,	"
				+ "	  FIMSMGR.FTVORGN FTVORGN,	"
				+ "	  FIMSMGR.FTVPROG FTVPROG,	"
				+ "	  FIMSMGR.FGBTRNH FGBTRNH,	"
				+ "	  (SELECT DISTINCT FABINVH.FABINVH_VEND_PIDM,	"
				+ "	    FABINVH.FABINVH_VEND_INV_CODE,	"
				+ "	    FABINVH.FABINVH_OPEN_PAID_IND,	"
				+ "	    FABINVH.FABINVH_TGRP_CODE,	"
				+ "	    FVRDCIN.FVRDCIN_DOCT_CODE,	"
				+ "	    FVVDOCT.FVVDOCT_DESC,	"
				+ "	    SPRIDEN.SPRIDEN_ID,	"
				+ "	    FABINVH.FABINVH_CODE	"
				+ "	  FROM FIMSMGR.FABINVH FABINVH,	"
				+ "	    FIMSMGR.FVRDCIN FVRDCIN,	"
				+ "	    FIMSMGR.FVVDOCT FVVDOCT,	"
				+ "	    SATURN.SPRIDEN SPRIDEN	"
				+ "	  WHERE FABINVH.FABINVH_CODE      = FVRDCIN.FVRDCIN_INVH_CODE	"
				+ "	  AND FVRDCIN.FVRDCIN_DOCT_CODE   = FVVDOCT.FVVDOCT_CODE	"
				+ "	  AND FABINVH.FABINVH_VEND_PIDM   = SPRIDEN.SPRIDEN_PIDM	"
				+ "	  AND SPRIDEN.SPRIDEN_CHANGE_IND IS NULL	"
				+ "	  ) DOC	"
				+ "	WHERE FGBTRND.FGBTRND_ORGN_CODE       = FTVORGN.FTVORGN_ORGN_CODE	"
				+ " AND FGBTRND.FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FTVFUND.FTVFUND_FUND_CODE	"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FTVPROG.FTVPROG_PROG_CODE	"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FGBTRNH.FGBTRNH_FUND_CODE	"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FGBTRNH.FGBTRNH_PROG_CODE	"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND.FGBTRND_ORGN_CODE         = FGBTRNH.FGBTRNH_ORGN_CODE	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         = FGBTRNH.FGBTRNH_LEDC_CODE	"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = FGBTRNH.FGBTRNH_DOC_CODE	"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE         = FGBTRNH.FGBTRNH_RUCL_CODE	"
				+ "	AND FGBTRND.FGBTRND_DOC_SEQ_CODE      = FGBTRNH.FGBTRNH_DOC_SEQ_CODE	"
				+ "	AND FGBTRND.FGBTRND_SEQ_NUM           = FGBTRNH.FGBTRNH_SEQ_NUM	"
				+ "	AND FGBTRND.FGBTRND_FSYR_CODE         = FGBTRNH.FGBTRNH_FSYR_CODE	"
				+ "	AND FGBTRND.FGBTRND_SUBMISSION_NUMBER = FGBTRNH.FGBTRNH_SUBMISSION_NUMBER	"
				+ "	AND FGBTRND.FGBTRND_ITEM_NUM          = FGBTRNH.FGBTRNH_ITEM_NUM	"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = DOC.FABINVH_CODE (+)	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVFUND.FTVFUND_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVFUND.FTVFUND_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVORGN.FTVORGN_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVORGN.FTVORGN_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVPROG.FTVPROG_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVPROG.FTVPROG_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVACCT.FTVACCT_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVACCT.FTVACCT_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         ='FINANC'	"
				+ "	AND FGBTRND.FGBTRND_LEDGER_IND        ='O'	"
				+ " and FGBTRND.FGBTRND_POSTING_PERIOD BETWEEN "+mesInic+" AND "+ mesFin +""
				+ " and FGBTRND.FGBTRND_FSYR_CODE = '"+a�o +"'"
				+ "	AND FTVFUND.FTVFUND_FTYP_CODE  IN ('CA','CD','CN','FI','FR')	"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ " and FGBTRND.FGBTRND_ORGN_CODE ='"+ org +"'"
				+ "	AND FGBTRND.FGBTRND_FIELD_CODE IN (1,2,3,4,5)	"
				+ "	  ) SIIF	"
				+ "	WHERE CUENTA IN ('5BB001','5BB002','5BB003','5B0001','5B0004','5B0005','5B0006','5B0007','5B0008','5B0009')	"
				+ "	group by documento, RUT, DOC, BANNER, FECHA, MOV, '5BADMIN', 'GASTO',  MES, IDE	"
				+ "	UNION ALL	"
				+ "	SELECT documento,RUT,DOC,BANNER, FECHA, MOV,'5BREMVR' CUENTA ,'GASTO REM VAR ADMINISTRATIVOS' DESCRIPCION_CUENTA,'GASTOS POR REMUNERACION' IDENTIFICADOR ,SUM(FGBTRND_TRANS_AMT),MES,IDE from	"
				+ "	(	"
				+ "	SELECT NVL (DOC.FVRDCIN_DOCT_CODE	"
				+ "	  || '-'	"
				+ "	  || DOC.FVRDCIN_DOCT_CODE,' ') documento,	"
				+ "	  NVL (DOC.SPRIDEN_ID,' ') RUT,	"
				+ "	  DOC.FABINVH_VEND_INV_CODE DOC,	"
				+ "	  FGBTRND.FGBTRND_DOC_CODE BANNER,	"
				+ "	  TO_CHAR(FGBTRNH_TRANS_DATE,'dd/mm/yy HH24:mi:ss') FECHA,	"
				+ "	  FGBTRND_RUCL_CODE MOV ,	"
				+ "	  FGBTRND.FGBTRND_ACCT_CODE CUENTA,	"
				+ "	  FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,	"
				+ "	  FGBTRNH_TRANS_DESC IDENTIFICADOR ,	"
				+ "	  CASE	"
				+ "	    WHEN FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)	"
				+ "	    THEN FGBTRND.FGBTRND_TRANS_AMT   *(-1)	"
				+ "	    ELSE FGBTRND.FGBTRND_TRANS_AMT 	"
				+ "	  END FGBTRND_TRANS_AMT 	"
				+ "	    ,	"
				+ "	  FGBTRND.FGBTRND_POSTING_PERIOD MES,	"
				+ "	  FGBTRND.FGBTRND_USER_ID IDE	"
				+ "	FROM FIMSMGR.FGBTRND FGBTRND,	"
				+ "	  FIMSMGR.FTVACCT FTVACCT,	"
				+ "	  FIMSMGR.FTVFUND FTVFUND,	"
				+ "	  FIMSMGR.FTVORGN FTVORGN,	"
				+ "	  FIMSMGR.FTVPROG FTVPROG,	"
				+ "	  FIMSMGR.FGBTRNH FGBTRNH,	"
				+ "	  (SELECT DISTINCT FABINVH.FABINVH_VEND_PIDM,	"
				+ "	    FABINVH.FABINVH_VEND_INV_CODE,	"
				+ "	    FABINVH.FABINVH_OPEN_PAID_IND,	"
				+ "	    FABINVH.FABINVH_TGRP_CODE,	"
				+ "	    FVRDCIN.FVRDCIN_DOCT_CODE,	"
				+ "	    FVVDOCT.FVVDOCT_DESC,	"
				+ "	    SPRIDEN.SPRIDEN_ID,	"
				+ "	    FABINVH.FABINVH_CODE	"
				+ "	  FROM FIMSMGR.FABINVH FABINVH,	"
				+ "	    FIMSMGR.FVRDCIN FVRDCIN,	"
				+ "	    FIMSMGR.FVVDOCT FVVDOCT,	"
				+ "	    SATURN.SPRIDEN SPRIDEN	"
				+ "	  WHERE FABINVH.FABINVH_CODE      = FVRDCIN.FVRDCIN_INVH_CODE	"
				+ "	  AND FVRDCIN.FVRDCIN_DOCT_CODE   = FVVDOCT.FVVDOCT_CODE	"
				+ "	  AND FABINVH.FABINVH_VEND_PIDM   = SPRIDEN.SPRIDEN_PIDM	"
				+ "	  AND SPRIDEN.SPRIDEN_CHANGE_IND IS NULL	"
				+ "	  ) DOC	"
				+ "	WHERE FGBTRND.FGBTRND_ORGN_CODE       = FTVORGN.FTVORGN_ORGN_CODE	"
				+ " AND FGBTRND.FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FTVFUND.FTVFUND_FUND_CODE	"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FTVPROG.FTVPROG_PROG_CODE	"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FGBTRNH.FGBTRNH_FUND_CODE	"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FGBTRNH.FGBTRNH_PROG_CODE	"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND.FGBTRND_ORGN_CODE         = FGBTRNH.FGBTRNH_ORGN_CODE	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         = FGBTRNH.FGBTRNH_LEDC_CODE	"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = FGBTRNH.FGBTRNH_DOC_CODE	"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE         = FGBTRNH.FGBTRNH_RUCL_CODE	"
				+ "	AND FGBTRND.FGBTRND_DOC_SEQ_CODE      = FGBTRNH.FGBTRNH_DOC_SEQ_CODE	"
				+ "	AND FGBTRND.FGBTRND_SEQ_NUM           = FGBTRNH.FGBTRNH_SEQ_NUM	"
				+ "	AND FGBTRND.FGBTRND_FSYR_CODE         = FGBTRNH.FGBTRNH_FSYR_CODE	"
				+ "	AND FGBTRND.FGBTRND_SUBMISSION_NUMBER = FGBTRNH.FGBTRNH_SUBMISSION_NUMBER	"
				+ "	AND FGBTRND.FGBTRND_ITEM_NUM          = FGBTRNH.FGBTRNH_ITEM_NUM	"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = DOC.FABINVH_CODE (+)	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVFUND.FTVFUND_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVFUND.FTVFUND_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVORGN.FTVORGN_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVORGN.FTVORGN_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVPROG.FTVPROG_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVPROG.FTVPROG_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVACCT.FTVACCT_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVACCT.FTVACCT_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         ='FINANC'	"
				+ "	AND FGBTRND.FGBTRND_LEDGER_IND        ='O'	"
				+ " and FGBTRND.FGBTRND_POSTING_PERIOD BETWEEN "+mesInic+" AND "+ mesFin +""
				+ " and FGBTRND.FGBTRND_FSYR_CODE = '"+a�o +"'"
				+ "	AND FTVFUND.FTVFUND_FTYP_CODE  IN ('CA','CD','CN','FI','FR')	"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ " and FGBTRND.FGBTRND_ORGN_CODE ='"+ org +"'"
				+ "	AND FGBTRND.FGBTRND_FIELD_CODE IN (1,2,3,4,5)	"
				+ "	  ) SIIF	"
				+ "	WHERE CUENTA IN ('5B0002','5B0003')	"
				+ "	group by documento, RUT, DOC, BANNER, FECHA, MOV, '5BREMVR', 'GASTO',  MES, IDE	"
				+ "	UNION ALL	"
				+ "	SELECT * FROM (	"
				+ "	SELECT NVL (DOC.FVRDCIN_DOCT_CODE	"
				+ "	  || '-'	"
				+ "	  || DOC.FVRDCIN_DOCT_CODE,' ') documento,	"
				+ "	  NVL (DOC.SPRIDEN_ID,' ') RUT,	"
				+ "	  DOC.FABINVH_VEND_INV_CODE,	"
				+ "	  FGBTRND.FGBTRND_DOC_CODE,	"
				+ "	  TO_CHAR(FGBTRNH_TRANS_DATE,'dd/mm/yy HH24:mi:ss') FECHA,	"
				+ "	  FGBTRND_RUCL_CODE ,	"
				+ "	  FGBTRND.FGBTRND_ACCT_CODE CUENTA,	"
				+ "	  FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,	"
				+ "	  FGBTRNH_TRANS_DESC IDENTIFICADOR,	"
				+ "	  CASE	"
				+ "	    WHEN FGBTRND.FGBTRND_FIELD_CODE IN (3,4,5)	"
				+ "	    THEN FGBTRND.FGBTRND_TRANS_AMT   *(-1)	"
				+ "	    ELSE FGBTRND.FGBTRND_TRANS_AMT	"
				+ "	  END FGBTRND_TRANS_AMT ,	"
				+ "	  FGBTRND.FGBTRND_POSTING_PERIOD MES,	"
				+ "	  FGBTRND.FGBTRND_USER_ID	"
				+ "	FROM FIMSMGR.FGBTRND FGBTRND,	"
				+ "	  FIMSMGR.FTVACCT FTVACCT,	"
				+ "	  FIMSMGR.FTVFUND FTVFUND,	"
				+ "	  FIMSMGR.FTVORGN FTVORGN,	"
				+ "	  FIMSMGR.FTVPROG FTVPROG,	"
				+ "	  FIMSMGR.FGBTRNH FGBTRNH,	"
				+ "	  (SELECT DISTINCT FABINVH.FABINVH_VEND_PIDM,	"
				+ "	    FABINVH.FABINVH_VEND_INV_CODE,	"
				+ "	    FABINVH.FABINVH_OPEN_PAID_IND,	"
				+ "	    FABINVH.FABINVH_TGRP_CODE,	"
				+ "	    FVRDCIN.FVRDCIN_DOCT_CODE,	"
				+ "	    FVVDOCT.FVVDOCT_DESC,	"
				+ "	    SPRIDEN.SPRIDEN_ID,	"
				+ "	    FABINVH.FABINVH_CODE	"
				+ "	  FROM FIMSMGR.FABINVH FABINVH,	"
				+ "	    FIMSMGR.FVRDCIN FVRDCIN,	"
				+ "	    FIMSMGR.FVVDOCT FVVDOCT,	"
				+ "	    SATURN.SPRIDEN SPRIDEN	"
				+ "	  WHERE FABINVH.FABINVH_CODE      = FVRDCIN.FVRDCIN_INVH_CODE	"
				+ "	  AND FVRDCIN.FVRDCIN_DOCT_CODE   = FVVDOCT.FVVDOCT_CODE	"
				+ "	  AND FABINVH.FABINVH_VEND_PIDM   = SPRIDEN.SPRIDEN_PIDM	"
				+ "	  AND SPRIDEN.SPRIDEN_CHANGE_IND IS NULL	"
				+ "	  ) DOC	"
				+ "	WHERE FGBTRND.FGBTRND_ORGN_CODE       = FTVORGN.FTVORGN_ORGN_CODE	"
				+ " AND FGBTRND.FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FTVFUND.FTVFUND_FUND_CODE	"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FTVPROG.FTVPROG_PROG_CODE	"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND.FGBTRND_FUND_CODE         = FGBTRNH.FGBTRNH_FUND_CODE	"
				+ "	AND FGBTRND.FGBTRND_PROG_CODE         = FGBTRNH.FGBTRNH_PROG_CODE	"
				+ "	AND FGBTRND.FGBTRND_ACCT_CODE         = FTVACCT.FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND.FGBTRND_ORGN_CODE         = FGBTRNH.FGBTRNH_ORGN_CODE	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         = FGBTRNH.FGBTRNH_LEDC_CODE	"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = FGBTRNH.FGBTRNH_DOC_CODE	"
				+ "	AND FGBTRND.FGBTRND_RUCL_CODE         = FGBTRNH.FGBTRNH_RUCL_CODE	"
				+ "	AND FGBTRND.FGBTRND_DOC_SEQ_CODE      = FGBTRNH.FGBTRNH_DOC_SEQ_CODE	"
				+ "	AND FGBTRND.FGBTRND_SEQ_NUM           = FGBTRNH.FGBTRNH_SEQ_NUM	"
				+ "	AND FGBTRND.FGBTRND_FSYR_CODE         = FGBTRNH.FGBTRNH_FSYR_CODE	"
				+ "	AND FGBTRND.FGBTRND_SUBMISSION_NUMBER = FGBTRNH.FGBTRNH_SUBMISSION_NUMBER	"
				+ "	AND FGBTRND.FGBTRND_ITEM_NUM          = FGBTRNH.FGBTRNH_ITEM_NUM	"
				+ "	AND FGBTRND.FGBTRND_DOC_CODE          = DOC.FABINVH_CODE (+)	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVFUND.FTVFUND_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVFUND.FTVFUND_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVORGN.FTVORGN_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVORGN.FTVORGN_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVPROG.FTVPROG_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVPROG.FTVPROG_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE    >= FTVACCT.FTVACCT_EFF_DATE	"
				+ "	AND FGBTRND.FGBTRND_ACTIVITY_DATE     < FTVACCT.FTVACCT_NCHG_DATE	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE         ='FINANC'	"
				+ "	AND FGBTRND.FGBTRND_LEDGER_IND        ='O'	"
				+ " and FGBTRND.FGBTRND_POSTING_PERIOD BETWEEN "+mesInic+" AND "+ mesFin +""
				+ " and FGBTRND.FGBTRND_FSYR_CODE = '"+a�o +"'"
				+ "	AND FTVFUND.FTVFUND_FTYP_CODE  IN ('CA','CD','CN','FI','FR')	"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ " and FGBTRND.FGBTRND_ORGN_CODE ='"+ org +"'"
				+ "	AND FGBTRND.FGBTRND_FIELD_CODE IN (1,2,3,4,5)	"
				+ "	ORDER BY FGBTRND.FGBTRND_POSTING_PERIOD	"
				+ "	) NO_REM 	"
				+ "	WHERE  CUENTA NOT LIKE '5%'	ORDER BY MES"
 ;

			//System.out.println("SQL getMovimientos " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			int sw = 0;
			String simbolo = "";
			int total_final = 0;
			int mes_anterior = 0;
			long total_aux_cargo = 0;
			long total_aux_abono = 0;
			int sw_cambio_mes = 0;
			int sw_cambiafila = 0;
			String aux = "" ;
			while (res.next()) {
				Pres18POJO ss = new Pres18POJO();
				sw = 1;

				int mes_actual = res.getInt(11);
				//System.out.println("mes_actual " + mes_actual + " documento " + res.getString(3));
				if (mes_actual == mes_anterior) {
					aux = res.getString(1) ;
					if ((aux == null) && (aux.isEmpty()))
						aux = " ";
					else if (aux.trim().equals("-"))
						aux = " ";	
					ss.setNomtip(aux);
					aux = "";
					aux = res.getString(2) ;					
					if ((aux == null) && (aux.isEmpty()))
						aux = " ";
					ss.setRut(aux) ;
					aux = "";
					aux = res.getString(3) ;					
					if (aux == null)
						aux = " ";
					ss.setNumdoc(aux);
					aux = "";
					aux = res.getString(4) ;					
					if (aux == null)
						aux = " ";
					ss.setNumdocbanner(aux);// ; <- banner
					aux = "";
					aux = res.getString(5) ;					
					if (aux == null)
						aux = " ";
					ss.setFecdoc(aux);
					aux = "";
					aux = res.getString(6) ;					
					if (aux == null)
						aux = " ";
					ss.setCoduni(aux);
					aux = "";
					
					aux = res.getString(7) ;					
					if (aux == null)
						aux = " ";
					ss.setItedoc(aux);
					aux = "";
					aux = res.getString(8) ;					
					if (aux == null)
						aux = " ";
					ss.setUnidad(aux);
				/*	if (res.getString(6).trim().equals("6AJ004")
							&& (res.getInt(10) == 2)) {
						sw = sw + 1;
					} */
					
					aux = "";
					aux = res.getString(9) ;					
					if (aux == null)
						aux = " ";
					ss.setDesuni(aux);
				
					ss.setUsadac(res.getInt(10));
				//	ss.setPresac(res.getInt(10));
				/*	int aux_suma = res.getInt(8);
					if (simbolo.equals("03") || simbolo.equals("04")
							|| simbolo.equals("05")) {
						// aux = aux * (-1); Abonos
						ss.setUsadac(res.getInt(8));
						total_aux_abono = total_aux_abono + aux_suma;
					} else { // cargo
						//	
						ss.setPresac(res.getInt(8));
						total_aux_cargo = total_aux_cargo + aux_suma;
					} */
					ss.setDesite(String.valueOf(mes_actual));
					ss.setIndprc("");
					ss.setNompro(res.getString(12));
					// ss.setNummes(res.getInt(8));
					simbolo = "";
					
					
					// calcula total
					lista.add(ss);

				//} else if ((mes_actual != mes_anterior) && (mes_anterior > 0)) {
				} else if ((mes_actual != mes_anterior) ) {
					Pres18POJO ss2 = new Pres18POJO();
					ss2.setNomtip("");
					// ss.setNumdocbanner(res.getString(2));); <- banner
				/*	ss2.setNumdoc("");
					ss2.setFecdoc("");
					ss2.setCoduni("");
					ss2.setUnidad("");
					ss2.setItedoc("");
					ss2.setDesuni("");
					ss2.setIndprc("1");
					ss2.setAcumum(total_aux_cargo);
					ss2.setUsadom(total_aux_abono);
					ss2.setNompro("");
					ss.setRut("");
					lista.add(ss2);
					sw_cambio_mes = 0;
					total_aux_abono = 0;
					total_aux_cargo = 0;
					mes_anterior = mes_actual;
*/
					//

					aux = res.getString(1) ;
					if ((aux == null) && (aux.isEmpty()))
						aux = " ";
					else if (aux.trim().equals("-"))
						aux = " ";	
					ss.setNomtip(aux);
					aux = "";
					aux = res.getString(2) ;					
					if ((aux == null) && (aux.isEmpty()))
						aux = " ";
					ss.setRut(aux) ;
					aux = "";
					aux = res.getString(3) ;					
					if (aux == null)
						aux = " ";
					ss.setNumdoc(aux);
					aux = "";
					aux = res.getString(4) ;					
					if (aux == null)
						aux = " ";
					ss.setNumdocbanner(aux);// ; <- banner
					aux = "";
					aux = res.getString(5) ;					
					if (aux == null)
						aux = " ";
					ss.setFecdoc(aux);
					aux = "";
					aux = res.getString(6) ;					
					if (aux == null)
						aux = " ";
					ss.setCoduni(aux);
					aux = "";
					
					aux = res.getString(7) ;					
					if (aux == null)
						aux = " ";
					ss.setItedoc(aux);
					aux = "";
					aux = res.getString(8) ;					
					if (aux == null)
						aux = " ";
					ss.setUnidad(aux);
				
					aux = "";
					aux = res.getString(9) ;					
					if (aux == null)
						aux = " ";
					ss.setDesuni(aux);
				
					ss.setUsadac(res.getInt(10));
					ss.setDesite(String.valueOf(mes_actual));
					ss.setIndprc("");
					ss.setNompro(res.getString(12));
					lista.add(ss);
					//

				} else if (mes_anterior == 0) {
					mes_anterior = mes_actual;
				}
			}
			if (sw == 0) {
				Pres18POJO ss = new Pres18POJO();
				ss.setItedoc(org);
				ss.setCoduni(org);

			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB getDetalles: " + e.getMessage());
		}
		return lista;
	}
	//public Collection<Pres18POJO> getMovimientos(String org, int a�oIN,int mesInic, int mesFin) {
	/*
		public Collection<Pres18POJO> getMovimientos_RESPALDO_18_12_17(String org, int a�oIN,int mesInic, int mesFin) {
		if (mesInic == 1) mesInic = 0;

		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);

		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		String mesfinal = "";
		String mesinicio = "";
		String operacion = "";
		if (mesInic < 10) {
			mesinicio = "0" + Integer.toString(mesInic);
		} else {
			mesinicio = Integer.toString(mesInic);
		}
		if (mesFin < 10) {
			mesfinal = "0" + Integer.toString(mesFin);
		} else {
			mesfinal = Integer.toString(mesFin);
		}
		try {
			String sql = "";

			sql = ""
					+ " SELECT DISTINCT  NVL(fvr.FVRDCIN_DOCT_CODE, 'N/E') ||'-' || FD.FTVDTYP_DESC TIPO_DOCUMENTO, fg.FGBTRND_DOC_CODE banner ,NVL(fab.FABINVH_VEND_INV_CODE, ' ') NUM_DOCUMENTO, TO_CHAR(Fh.FGBTRNH_TRANS_DATE,'dd/mm/yy HH24:mi:ss') FECHA, fg.FGBTRND_RUCL_CODE CONTR_CUENTA, FG.FGBTRND_ACCT_CODE CUENTA, "
					+ " FH.FGBTRNH_TRANS_DESC IDENTIFICADOR_GLOSA,FG.FGBTRND_TRANS_AMT ,FG.FGBTRND_FIELD_CODE,FG.fgbtrnd_POSTING_PERIOD MES ,FG.FGBTRND_USER_ID DIGIT, FGBTRND_DR_CR_IND ,NVL(SPR.SPRIDEN_ID ,' ') "
					+ " FROM SPRIDEN SPR, fgbtrnd FG LEFT JOIN FVRDCIN fvr ON fvr.FVRDCIN_INVH_CODE = fg.FGBTRND_DOC_CODE "
					+ " LEFT JOIN FABINVH fab ON fg.FGBTRND_DOC_CODE = fab.FABINVH_CODE, "
					+ " ftvfund FT ,ftvacct FV ,FTVDTYP FD ,FGBTRNH FH"
					+ "  WHERE SPR.SPRIDEN_PIDM = FGBTRNH_VENDOR_PIDM AND FG.FGBTRND_DOC_SEQ_CODE = FD.FTVDTYP_SEQ_NUM AND FG.FGBTRND_ACCT_CODE      = fv.FTVACCT_ACCT_CODE AND FG.fgbtrnd_FUND_CODE      = FT.FTVFUND_FUND_CODE AND FG.fgbtrnd_POSTING_PERIOD BETWEEN  '"
					+ mesinicio
					+ "' AND '"
					+ mesfinal
					+ "' "
					+ "  AND FG.FGBTRND_ORGN_CODE = '"
					+ org
					+ "' and FG.FGBTRND_FSYR_CODE = '"
					+ a�o
					+ "'"
					+ " AND FH.FGBTRNH_RUCL_CODE      = FG.FGBTRND_RUCL_CODE AND FH.FGBTRNH_SEQ_NUM        = FG.FGBTRND_SEQ_NUM AND fg.FGBTRND_DOC_CODE       = fh.FGBTRNH_DOC_CODE AND FG.FGBTRND_ACCT_CODE      = FH.FGBTRNH_ACCT_CODE AND FG.FGBTRND_ORGN_CODE      = FH.FGBTRNH_ORGN_CODE AND (fg.FGBTRND_LEDGER_IND    = 'O' )"
					+ " AND (fg.FGBTRND_FIELD_CODE    = 3 OR fg.FGBTRND_FIELD_CODE      = 4 OR fg.FGBTRND_FIELD_CODE      = 5 OR fg.FGBTRND_FIELD_CODE      = 2 OR fg.FGBTRND_FIELD_CODE      = 1) AND fg.FGBTRND_LEDC_CODE      = 'FINANC' AND fh.FGBTRNH_LEDC_CODE      = 'FINANC'  AND FG.fgbtrnd_POSTING_PERIOD BETWEEN "
					+ mesInic + " AND " + mesFin
					+ " ORDER BY FG.fgbtrnd_POSTING_PERIOD , fecha";

			//System.out.println("SQL getMovimientos " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			int sw = 0;
			String simbolo = "";
			int total_final = 0;
			int mes_anterior = 0;
			long total_aux_cargo = 0;
			long total_aux_abono = 0;
			int sw_cambio_mes = 0;
			int sw_cambiafila = 0;

			while (res.next()) {
				Pres18POJO ss = new Pres18POJO();
				sw = 1;

				int mes_actual = res.getInt(10);

				if (mes_actual == mes_anterior) {
					ss.setNomtip(res.getString(1));
					ss.setNumdocbanner(res.getString(2));// ; <- banner
					ss.setNumdoc(res.getString(3));
					ss.setFecdoc(res.getString(4));
					ss.setCoduni(res.getString(5));

					if (res.getString(6).trim().equals("6AJ004")
							&& (res.getInt(10) == 2)) {
						sw = sw + 1;
					}
					ss.setItedoc(res.getString(6));
					ss.setDesuni(res.getString(7));
					simbolo = res.getString(9); // simbolo

					int aux_suma = res.getInt(8);
					if (simbolo.equals("03") || simbolo.equals("04")
							|| simbolo.equals("05")) {
						// aux = aux * (-1); Abonos
						ss.setUsadac(res.getInt(8));
						total_aux_abono = total_aux_abono + aux_suma;
					} else { // cargo
						//	
						ss.setPresac(res.getInt(8));
						total_aux_cargo = total_aux_cargo + aux_suma;
					}
					ss.setDesite(String.valueOf(mes_actual));
					ss.setIndprc("");
					ss.setNompro(res.getString(11));
					// ss.setNummes(res.getInt(8));
					simbolo = "";
					operacion = res.getString(12).trim();
					//System.out.println ("Operacion SQL " + operacion);
					if (operacion.equals("+") || (operacion.equals("C"))) {
						operacion= "";
						operacion = "Abono" ;
						}
					else if (operacion.equals("-")|| (operacion.equals("D"))) {
						operacion= "";
						operacion = "Cargo";
					} else System.out.println ("********* operacion ****** " + operacion);
					
					ss.setOperacion(operacion);
					ss.setRut(res.getString(13)) ;
					// calcula total
					lista.add(ss);

				} else if ((mes_actual != mes_anterior) && (mes_anterior > 0)) {

					Pres18POJO ss2 = new Pres18POJO();
					ss2.setNomtip("");
					// ss.setNumdocbanner(res.getString(2));); <- banner
					ss2.setNumdoc("");
					ss2.setFecdoc("");
					ss2.setCoduni("");
					ss2.setItedoc("");
					ss2.setDesuni("");
					ss2.setIndprc("1");
					ss2.setAcumum(total_aux_cargo);
					ss2.setUsadom(total_aux_abono);
					ss2.setNompro("");
					ss.setRut("");
					lista.add(ss2);
					sw_cambio_mes = 0;
					total_aux_abono = 0;
					total_aux_cargo = 0;
					mes_anterior = mes_actual;

					//

					ss.setNomtip(res.getString(1));
					// ss.setNumdocbanner(res.getString(2));); <- banner
					ss.setNumdoc(res.getString(3));
					ss.setFecdoc(res.getString(4));
					ss.setCoduni(res.getString(5));

					if (res.getString(6).trim().equals("6AJ004")
							&& (res.getInt(10) == 2)) {
						sw = sw + 1;
					}
					ss.setItedoc(res.getString(6));
					ss.setDesuni(res.getString(7));
					simbolo = res.getString(9); // simbolo

					int aux_suma = res.getInt(8);
					if (simbolo.equals("03") || simbolo.equals("04")
							|| simbolo.equals("05")) {
						// aux = aux * (-1); Abonos
						ss.setUsadac(res.getInt(8));
						total_aux_abono = total_aux_abono + aux_suma;
					} else { // cargo
						//	
						ss.setPresac(res.getInt(8));
						total_aux_cargo = total_aux_cargo + aux_suma;
					}
					ss.setDesite(String.valueOf(mes_actual));
					ss.setIndprc("");
					ss.setNompro(res.getString(11));
					// ss.setNummes(res.getInt(8));
					simbolo = "";
					operacion = res.getString(12).trim();
					if (operacion.equals("+") || (operacion.equals("C"))) {
						operacion= "";
						operacion = "Abono" ;
						}
					else if (operacion.equals("-")|| (operacion.equals("D"))) {
						operacion= "";
						operacion = "Cargo";
					} else System.out.println ("********* operacion ****** " + operacion);
					
					ss.setOperacion(operacion);
					// calcula total
					lista.add(ss);
					//

				} else if (mes_anterior == 0) {
					mes_anterior = mes_actual;
				}
			}
			if (sw == 0) {
				Pres18POJO ss = new Pres18POJO();
				ss.setItedoc(org);
				ss.setCoduni(org);

			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB getDetalles: " + e.getMessage());
		}
		return lista;
	}

	*/
/*
	public Collection<Presw25POJO> getEjecMensual_Respaldo_sin_agrupar(String org, int a�oIN, int mes) {
		if (a�oIN == 0) {
			Calendar fecha = Calendar.getInstance();
			a�oIN = fecha.get(Calendar.YEAR);
		}
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);

		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Presw25POJO> lista = new ArrayList<Presw25POJO>();

		try {
			String sql = "";

			sql = ""
				+" SELECT FGBTRND_ACCT_CODE,"
				+" FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,"
				//+" FGBTRND_POSTING_PERIOD,"
				+" ( SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0) *-1) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD08',FGBTRND.FGBTRND_TRANS_AMT,0),0)*-1) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)*-1)+ SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)*-1) + SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0)) + SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) ) Gasto ," 
				+" ( SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) ) Presupuesto"
				+" FROM FIMSMGR.FGBTRND,"
				+" FIMSMGR.FTVFUND,"
				+" FIMSMGR.FTVORGN,"
				+" FIMSMGR.FTVACCT,"
				+" FIMSMGR.FTVPROG"
				+" WHERE FGBTRND_FUND_CODE=FTVFUND_FUND_CODE "
				+" AND FGBTRND_ORGN_CODE=FTVORGN_ORGN_CODE "
				+" AND FGBTRND_PROG_CODE=FTVPROG_PROG_CODE "
				+" AND FGBTRND_ACCT_CODE=FTVACCT_ACCT_CODE "
				+" AND FGBTRND_ACTIVITY_DATE >= FTVFUND_EFF_DATE "
				+" AND FGBTRND_ACTIVITY_DATE < FTVFUND_NCHG_DATE "
				+" AND FGBTRND_ACTIVITY_DATE >= FTVORGN_EFF_DATE "
				+" AND FGBTRND_ACTIVITY_DATE < FTVORGN_NCHG_DATE "
				+" AND FGBTRND_ACTIVITY_DATE >= FTVPROG_EFF_DATE "
				+" AND FGBTRND_ACTIVITY_DATE < FTVPROG_NCHG_DATE "
				+" AND FGBTRND_ACTIVITY_DATE >= FTVACCT_EFF_DATE "
				+" AND FGBTRND_ACTIVITY_DATE < FTVACCT_NCHG_DATE "
				+" AND FTVFUND_FTYP_CODE IN('CA','CD','CN','FI','FR') "
				+" AND FTVFUND_DATA_ENTRY_IND='Y' "
				+" AND FTVORGN_DATA_ENTRY_IND='Y' "
				+" AND FTVACCT_DATA_ENTRY_IND='Y' "
				+" AND FTVPROG_DATA_ENTRY_IND='Y' "
				+" AND FGBTRND.FGBTRND_LEDC_CODE='FINANC' "
				+" AND FGBTRND_LEDGER_IND='O' "
				+" AND FGBTRND_FSYR_CODE= '"+ a�osql +"' "
				+" AND FGBTRND_ORGN_CODE='"+org +"' AND FGBTRND_POSTING_PERIOD BETWEEN 0 AND "+mes +" "
				+" GROUP BY FGBTRND_ACCT_CODE, FTVACCT_TITLE "
				+" ORDER BY FGBTRND_ACCT_CODE" ;

		//	System.out.println("SQL getEjecMensual new " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			long acumulado = 0;
			long gastos = 0;

			while (res.next()) {
				// String aux = res.getString(1);
				Presw25POJO ss = new Presw25POJO();
				// ss.setCoditem(res.getString(1));
				ss.setCoduni(res.getString(1));
				ss.setGlobal(res.getString(2));
				//ss.setMes(res.getString(2)) ;
				ss.setGasto1(res.getLong(4));
				ss.setGasto2(res.getLong(3));

				lista.add(ss);
			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB getDetalles: " + e.getMessage());
		}
		return lista;
	}
*/
	public Collection<Presw25POJO> getEjecMensual(String org, int a�oIN, int mes) {
		if (a�oIN == 0) {
			Calendar fecha = Calendar.getInstance();
			a�oIN = fecha.get(Calendar.YEAR);
		}
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
		
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Presw25POJO> lista = new ArrayList<Presw25POJO>();
		long saldo_inicial = 0;
		try {
			String sql = "";

			sql = ""
				+ "	Select  '5ACADOC' CUENTA, 'GASTO REM ACAD�MICOS' DESCRIPCION_CUENTA ,SUM(GASTO),SUM(PRESUPUESTO),SUM(TRASPASO) from (	"
				+ "	SELECT FGBTRND_ACCT_CODE CUENTA,	"
				+ "	  FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,	"
				+ "	(SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)*-1)+ "
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)*-1) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) *-1) + " 
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0)) + "
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) ) Gasto , "
				+ "	  (SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)  ) + "
				+ " (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))) ) traspaso, "
				+ "	  ( SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +"
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) ) Presupuesto"
				+ "	FROM FIMSMGR.FGBTRND,	"
				+ "	  FIMSMGR.FTVFUND,	"
				+ "	  FIMSMGR.FTVORGN,	"
				+ "	  FIMSMGR.FTVACCT,	"
				+ "	  FIMSMGR.FTVPROG	"
				+ "	WHERE FGBTRND_FUND_CODE      =FTVFUND_FUND_CODE	"
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND_ORGN_CODE        =FTVORGN_ORGN_CODE	"
				+ "	AND FGBTRND_PROG_CODE        =FTVPROG_PROG_CODE	"
				+ "	AND FGBTRND_ACCT_CODE        =FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVFUND_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVFUND_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVORGN_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVORGN_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVPROG_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVPROG_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVACCT_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVACCT_NCHG_DATE	"
				+ "	AND FTVFUND_FTYP_CODE       IN('CA','CD','CN','FI','FR')	"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ "	AND FTVFUND_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVORGN_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVACCT_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVPROG_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE='FINANC'	"
				+ "	AND FGBTRND_LEDGER_IND       ='O'	"
				+" AND FGBTRND_FSYR_CODE= '"+ a�osql +"' "
				+" AND FGBTRND_ORGN_CODE='"+org +"'	"
				+ "	AND FGBTRND_POSTING_PERIOD BETWEEN 0 AND "+mes +" "
				+ "	GROUP BY FGBTRND_ACCT_CODE,	"
				+ "	  FTVACCT_TITLE	"
				+ "	ORDER BY FGBTRND_ACCT_CODE ) SIIF	"
				+ "	WHERE CUENTA IN ('5A0001','5A0002','5AA005','5AA006','5A0007','5A0008','5A0009','5A0010','5A0011','5A0012','5AA001','5AA002','5AA008','5AA009')   	"
				+ "	UNION ALL	"
				+ "	select '5APODOC' CUENTA, 'GASTO REM APOYOS ACA Y DOC' DESCRIPCION_CUENTA ,SUM(GASTO),SUM(PRESUPUESTO),SUM(TRASPASO) from (	"
				+ "	SELECT FGBTRND_ACCT_CODE CUENTA,	"
				+ "	  FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,	"
				+ "	(SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)*-1)+ "
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)*-1) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) *-1) + " 
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0)) + "
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) ) Gasto , "
				+ "	  (SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)  ) + "
				+ " (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))) ) traspaso, "
				+ "	  ( SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +"
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) ) Presupuesto"
				+ "	FROM FIMSMGR.FGBTRND,	"
				+ "	  FIMSMGR.FTVFUND,	"
				+ "	  FIMSMGR.FTVORGN,	"
				+ "	  FIMSMGR.FTVACCT,	"
				+ "	  FIMSMGR.FTVPROG	"
				+ "	WHERE FGBTRND_FUND_CODE      =FTVFUND_FUND_CODE	"
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND_ORGN_CODE        =FTVORGN_ORGN_CODE	"
				+ "	AND FGBTRND_PROG_CODE        =FTVPROG_PROG_CODE	"
				+ "	AND FGBTRND_ACCT_CODE        =FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVFUND_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVFUND_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVORGN_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVORGN_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVPROG_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVPROG_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVACCT_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVACCT_NCHG_DATE	"
				+ "	AND FTVFUND_FTYP_CODE       IN('CA','CD','CN','FI','FR')	"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ "	AND FTVFUND_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVORGN_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVACCT_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVPROG_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE='FINANC'	"
				+ "	AND FGBTRND_LEDGER_IND       ='O'	"
				+" AND FGBTRND_FSYR_CODE= '"+ a�osql +"' "
				+" AND FGBTRND_ORGN_CODE='"+org +"'	"
				+ "	AND FGBTRND_POSTING_PERIOD BETWEEN 0 AND "+mes +" "
				+ "	GROUP BY FGBTRND_ACCT_CODE,	"
				+ "	  FTVACCT_TITLE	"
				+ "	ORDER BY FGBTRND_ACCT_CODE ) SIIF	"
				+ "	WHERE CUENTA IN ('5A0003','5AA004','5AA011')	"
				+ "	UNION ALL	"
				+ "	select '5AREMVR' CUENTA, 'GASTO REM HON, PT Y AYU' DESCRIPCION_CUENTA ,SUM(GASTO),SUM(PRESUPUESTO),SUM(TRASPASO) from (	"
				+ "	SELECT FGBTRND_ACCT_CODE CUENTA,	"
				+ "	  FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,	"
				+ "	(SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)*-1)+ "
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)*-1) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) *-1) + " 
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0)) + "
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) ) Gasto , "
				+ "	  (SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)  ) + "
				+ " (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))) ) traspaso, "
				+ "	  ( SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +"
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) ) Presupuesto"
				+ "	FROM FIMSMGR.FGBTRND,	"
				+ "	  FIMSMGR.FTVFUND,	"
				+ "	  FIMSMGR.FTVORGN,	"
				+ "	  FIMSMGR.FTVACCT,	"
				+ "	  FIMSMGR.FTVPROG	"
				+ "	WHERE FGBTRND_FUND_CODE      =FTVFUND_FUND_CODE	"
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND_ORGN_CODE        =FTVORGN_ORGN_CODE	"
				+ "	AND FGBTRND_PROG_CODE        =FTVPROG_PROG_CODE	"
				+ "	AND FGBTRND_ACCT_CODE        =FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVFUND_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVFUND_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVORGN_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVORGN_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVPROG_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVPROG_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVACCT_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVACCT_NCHG_DATE	"
				+ "	AND FTVFUND_FTYP_CODE       IN('CA','CD','CN','FI','FR')	"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ "	AND FTVFUND_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVORGN_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVACCT_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVPROG_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE='FINANC'	"
				+ "	AND FGBTRND_LEDGER_IND       ='O'	"
				+" AND FGBTRND_FSYR_CODE= '"+ a�osql +"' "
				+" AND FGBTRND_ORGN_CODE='"+org +"'	"
				+ "	AND FGBTRND_POSTING_PERIOD BETWEEN 0 AND "+mes +" "
				+ "	GROUP BY FGBTRND_ACCT_CODE,	"
				+ "	  FTVACCT_TITLE	"
				+ "	ORDER BY FGBTRND_ACCT_CODE ) SIIF	"
				+ "	WHERE CUENTA IN ('5A0004','5A0005','5A0006','5AA003','5AA007','5AA010')	"
				+ "	UNION ALL	"
				+ "	select '5BADMIN' CUENTA, 'GASTO REM NOMINA ADMINISTRATIVOS' DESCRIPCION_CUENTA ,SUM(GASTO),SUM(PRESUPUESTO),SUM(TRASPASO) from (	"
				+ "	SELECT FGBTRND_ACCT_CODE CUENTA,	"
				+ "	  FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,	"
				+ "	(SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)*-1)+ "
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)*-1) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) *-1) + " 
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0)) + "
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) ) Gasto , "
				+ "	  (SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)  ) + "
				+ " (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))) ) traspaso, "
				+ "	  ( SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +"
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) ) Presupuesto"
				+ "	FROM FIMSMGR.FGBTRND,	"
				+ "	  FIMSMGR.FTVFUND,	"
				+ "	  FIMSMGR.FTVORGN,	"
				+ "	  FIMSMGR.FTVACCT,	"
				+ "	  FIMSMGR.FTVPROG	"
				+ "	WHERE FGBTRND_FUND_CODE      =FTVFUND_FUND_CODE	"
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND_ORGN_CODE        =FTVORGN_ORGN_CODE	"
				+ "	AND FGBTRND_PROG_CODE        =FTVPROG_PROG_CODE	"
				+ "	AND FGBTRND_ACCT_CODE        =FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVFUND_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVFUND_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVORGN_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVORGN_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVPROG_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVPROG_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVACCT_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVACCT_NCHG_DATE	"
				+ "	AND FTVFUND_FTYP_CODE       IN('CA','CD','CN','FI','FR')	"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ "	AND FTVFUND_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVORGN_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVACCT_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVPROG_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE='FINANC'	"
				+ "	AND FGBTRND_LEDGER_IND       ='O'	"
				+" AND FGBTRND_FSYR_CODE= '"+ a�osql +"' "
				+" AND FGBTRND_ORGN_CODE='"+org +"'	"
				+ "	AND FGBTRND_POSTING_PERIOD BETWEEN 0 AND "+mes +" "
				+ "	GROUP BY FGBTRND_ACCT_CODE,	"
				+ "	  FTVACCT_TITLE	"
				+ "	ORDER BY FGBTRND_ACCT_CODE ) SIIF	"
				+ "	WHERE CUENTA IN ('5BB001','5BB002','5BB003','5B0001','5B0004','5B0005','5B0006','5B0007','5B0008','5B0009')	"
				+ "	UNION ALL	"
				+ "	select '5BREMVR' CUENTA, 'GASTO REM VAR ADMINISTRATIVOS' DESCRIPCION_CUENTA ,SUM(GASTO),SUM(PRESUPUESTO),SUM(TRASPASO) from (	"
				+ "	SELECT FGBTRND_ACCT_CODE CUENTA,	"
				+ "	  FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,	"
				+ "	(SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)*-1)+ "
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)*-1) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) *-1) + " 
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0)) + "
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) ) Gasto , "
				+ "	  (SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)  ) + "
				//+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD08',FGBTRND.FGBTRND_TRANS_AMT,0),0))  ) traspaso,"
				+ " (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))) ) traspaso, "
				+ "	  ( SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +"
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) ) Presupuesto"
				+ "	FROM FIMSMGR.FGBTRND,	"
				+ "	  FIMSMGR.FTVFUND,	"
				+ "	  FIMSMGR.FTVORGN,	"
				+ "	  FIMSMGR.FTVACCT,	"
				+ "	  FIMSMGR.FTVPROG	"
				+ "	WHERE FGBTRND_FUND_CODE      =FTVFUND_FUND_CODE	"
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND_ORGN_CODE        =FTVORGN_ORGN_CODE	"
				+ "	AND FGBTRND_PROG_CODE        =FTVPROG_PROG_CODE	"
				+ "	AND FGBTRND_ACCT_CODE        =FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVFUND_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVFUND_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVORGN_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVORGN_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVPROG_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVPROG_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVACCT_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVACCT_NCHG_DATE	"
				+ "	AND FTVFUND_FTYP_CODE       IN('CA','CD','CN','FI','FR')	"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ "	AND FTVFUND_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVORGN_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVACCT_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVPROG_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE='FINANC'	"
				+ "	AND FGBTRND_LEDGER_IND       ='O'	"
				+" AND FGBTRND_FSYR_CODE= '"+ a�osql +"' "
				+" AND FGBTRND_ORGN_CODE='"+org +"'	"
				+ "	AND FGBTRND_POSTING_PERIOD BETWEEN 0 AND "+mes +" "
				+ "	GROUP BY FGBTRND_ACCT_CODE,	"
				+ "	  FTVACCT_TITLE	"
				+ "	ORDER BY FGBTRND_ACCT_CODE ) SIIF	"
				+ "	WHERE CUENTA IN ('5B0002','5B0003')	"
				+ "	UNION ALL	"
				+ "	Select CUENTA,DESCRIPCION_CUENTA,GASTO,PRESUPUESTO,TRASPASO from (	"
				+ "	SELECT FGBTRND_ACCT_CODE CUENTA,	"
				+ "	  FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,	"
				+ "	(SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)*-1)+ "
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)*-1) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) *-1) + " 
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0)) + "
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) ) Gasto , "
				+ "	  (SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)  ) + "
				//+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD08',FGBTRND.FGBTRND_TRANS_AMT,0),0))  ) traspaso,"
				+ " (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))) ) traspaso, "
				+ "	  ( SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +"
				+ "	  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) ) Presupuesto"
				+ "	FROM FIMSMGR.FGBTRND,	"
				+ "	  FIMSMGR.FTVFUND,	"
				+ "	  FIMSMGR.FTVORGN,	"
				+ "	  FIMSMGR.FTVACCT,	"
				+ "	  FIMSMGR.FTVPROG	"
				+ "	WHERE FGBTRND_FUND_CODE      =FTVFUND_FUND_CODE	"
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+ "	AND FGBTRND_ORGN_CODE        =FTVORGN_ORGN_CODE	"
				+ "	AND FGBTRND_PROG_CODE        =FTVPROG_PROG_CODE	"
				+ "	AND FGBTRND_ACCT_CODE        =FTVACCT_ACCT_CODE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVFUND_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVFUND_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVORGN_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVORGN_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVPROG_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVPROG_NCHG_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE   >= FTVACCT_EFF_DATE	"
				+ "	AND FGBTRND_ACTIVITY_DATE    < FTVACCT_NCHG_DATE	"
				+ "	AND FTVFUND_FTYP_CODE       IN('CA','CD','CN','FI','FR')	"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ "	AND FTVFUND_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVORGN_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVACCT_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FTVPROG_DATA_ENTRY_IND   ='Y'	"
				+ "	AND FGBTRND.FGBTRND_LEDC_CODE='FINANC'	"
				+ "	AND FGBTRND_LEDGER_IND       ='O'	"
				+" AND FGBTRND_FSYR_CODE= '"+ a�osql +"' "
				+" AND FGBTRND_ORGN_CODE='"+org +"'	"
				+ "	AND FGBTRND_POSTING_PERIOD BETWEEN 0 AND "+mes +" "
				+ "	GROUP BY FGBTRND_ACCT_CODE,	"
				+ "	  FTVACCT_TITLE	"
				+ "	ORDER BY FGBTRND_ACCT_CODE ) NO_REMUN	"
				+ "	WHERE CUENTA NOT LIKE '5%'	";



		//	System.out.println("SQL getEjecMensual new " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			long acumulado = 0;
			long gastos = 0;
			String nomMes= "";
			String unidad ="";
			while (res.next()) {
				// String aux = res.getString(1);
				Presw25POJO ss = new Presw25POJO();
				// ss.setCoditem(res.getString(1));
				unidad = res.getString(1);			
				if (unidad.equals("6BF005")) {
					//saldo inicial
					
					saldo_inicial = res.getLong(3) ;
					
					ss.setCoduni(unidad);
					ss.setGlobal(res.getString(2));
					ss.setSaldoinicial(saldo_inicial);
				
					//lista.add(ss);
				}
				else if  (unidad.equals("INGRESO_ANUAL")) {
					ss.setAnual(res.getLong(3));





					ss.setAnno(a�oIN);
					ss.setMes(nomMes);
				}
					
				
				else {
				//
					if  (unidad.equals("11A000")) 
						ss.setIngreso_mensual(res.getLong(3));
					
					ss.setCoduni(unidad);
					ss.setGlobal(res.getString(2));
				ss.setGasto1(res.getLong(4));
				ss.setGasto2(res.getLong(3));
				ss.setGasto3(res.getLong(5) );
				ss.setGasto4( res.getLong(4) + (res.getLong(5)) - res.getLong(3));
				ss.setAnno(a�oIN);
				
				switch (mes) {
				case 0:
					nomMes = "Saldo inicial";					
					break;
				case 1:
					nomMes = "Enero";
					break;
				case 2:
					nomMes = "Febrero";
					break;
				case 3:
					nomMes = "Marzo";
					break;
				case 4:
					nomMes = "Abril";
					break;
				case 5:
					nomMes = "Mayo";
					break;
				case 6:
					nomMes = "Junio";
					break;
				case 7:
					nomMes = "Julio";
					break;
				case 8:
					nomMes = "Agosto";
					break;
				case 9:
					nomMes = "Septiembre";
					break;
				case 10:
					nomMes = "Octubre";
					break;
				case 11:
					nomMes = "Noviembre";
					break;
				case 12:
					nomMes = "Diciembre";
					break;
				}
				ss.setMes(nomMes);
				}
				lista.add(ss);
			//	}
			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB getDetalles: " + e.getMessage());
		}
		return lista;
	}

	
	public Collection<ItemPOJO> getEstamentos(String org, int a�oIN) {
		if (a�oIN == 0) {
			Calendar fecha = Calendar.getInstance();
			a�oIN = fecha.get(Calendar.YEAR);
		}
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);

		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<ItemPOJO> lista = new ArrayList<ItemPOJO>();

		try {
			String sql = "";

			sql = ""
					+ " select distinct ft.FTVACCT_TITLE "
					+ " from FGBOPAL fg, FTVACCT ft    "
					+ " where fg.FGBOPAL_acct_code like '5%' and fg.FGBOPAL_acct_code = ft.FTVACCT_ACCT_CODE and fg.FGBOPAL_ORGN_CODE = '"
					+ org + "' and fg.FGBOPAL_FSYR_CODE = '" + a�o + "'";

			//System.out.println("SQL getEstamentos " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			long acumulado = 0;
			long gastos = 0;

			while (res.next()) {
				// String aux = res.getString(1);
				ItemPOJO ss = new ItemPOJO();
				// ss.setCoditem(res.getString(1));
				ss.setNomite(res.getString(1));

				lista.add(ss);
			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB getEstamentos: " + e.getMessage());
		}
		return lista;
	}
	

	public Collection<Pres18POJO>  getCargaOrgTemporal(ArrayList<Pres18POJO> org, int anno, int mes) {
		Boolean resp = false;
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		//System.out.println("Entra a getCargaOrgTemporal");
		Collection<Pres18POJO> listaItem =  new ArrayList<Pres18POJO>();
		Pres18POJO pres18POJO = new Pres18POJO();
		try {
			
			String query = "INSERT ALL ";
			String queryInsert = "";
			String queryBody = "";
			for (Pres18POJO ss : org) {
			//	System.out.println("Cuenta " + ss.getCoduni().trim());
				queryBody = queryBody + " INTO TMP_ORGANIZACIONES(TMP_ORGANIZACIONES_ORGN) VALUES ('"+ss.getCoduni().trim() +"') " ;
				
			}
			queryInsert = query + " " +queryBody + " SELECT * FROM DUAL" ;
		//	System.out.println(" getCargaOrgTemporal masivo " + queryInsert);
			con.getConexion().setAutoCommit(false);
			sent = con.getConexion().prepareStatement(queryInsert);
			int res = sent.executeUpdate();
			// Obtiene informacion de las organizaciones
		//	System.out.println("  antes listaItem: "+listaItem.size());
			listaItem = getSaldos( anno, con.getConexion(), mes) ;
		//	System.out.println(" despues listaItem: "+listaItem.size());
			//con.getConexion().setAutoCommit(true);
			con.getConexion().commit();
            sent.close();
			con.close();
		} catch (Exception ex) {
			System.out.println("Error sipB getCargaOrgTemporal: " + ex.getMessage());
		}
		return listaItem;
	}
	/*
	public Collection<Pres18POJO> getCuentasDetalles_RESP_Original(String org,
			int a�oIN, int mes, String cuenta) {

		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
		
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();

		try {
			String sql = "";
			if (cuenta.equals("-1")) {
				sql = ""
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_00_adopt_bud + fg.fgbopal_00_bud_adjt ) presu , SUM(fg.fgbopal_00_ytd_actv + fg.fgbopal_00_encumb + fg.fgbopal_00_bud_rsrv ) gasto  ,0 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title  "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_01_adopt_bud + fg.fgbopal_01_bud_adjt ) presu , SUM(fg.fgbopal_01_ytd_actv + fg.fgbopal_01_encumb + fg.fgbopal_01_bud_rsrv ) gasto  ,1 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title  "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_02_adopt_bud + fg.fgbopal_02_bud_adjt ) presu , SUM(fg.fgbopal_02_ytd_actv + fg.fgbopal_02_encumb + fg.fgbopal_02_bud_rsrv ) gasto , 2 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title  "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_03_adopt_bud + fg.fgbopal_03_bud_adjt ) presu , SUM(fg.fgbopal_03_ytd_actv + fg.fgbopal_03_encumb + fg.fgbopal_03_bud_rsrv ) gasto  ,3 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title  "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_04_adopt_bud + fg.fgbopal_04_bud_adjt ) presu , SUM(fg.fgbopal_04_ytd_actv + fg.fgbopal_04_encumb + fg.fgbopal_04_bud_rsrv ) gasto  , 4 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title  "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_05_adopt_bud + fg.fgbopal_05_bud_adjt ) presu , SUM(fg.fgbopal_05_ytd_actv + fg.fgbopal_05_encumb + fg.fgbopal_05_bud_rsrv ) gasto  , 5 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title  "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_06_adopt_bud + fg.fgbopal_06_bud_adjt ) presu , SUM(fg.fgbopal_06_ytd_actv + fg.fgbopal_06_encumb + fg.fgbopal_06_bud_rsrv ) gasto   , 6 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title  "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_07_adopt_bud + fg.fgbopal_07_bud_adjt ) presu , SUM(fg.fgbopal_07_ytd_actv + fg.fgbopal_07_encumb + fg.fgbopal_07_bud_rsrv ) gasto  ,7 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title  "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_08_adopt_bud + fg.fgbopal_08_bud_adjt ) presu , SUM(fg.fgbopal_08_ytd_actv + fg.fgbopal_08_encumb + fg.fgbopal_08_bud_rsrv ) gasto   ,8 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title  "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_09_adopt_bud + fg.fgbopal_09_bud_adjt ) presu , SUM(fg.fgbopal_09_ytd_actv + fg.fgbopal_09_encumb + fg.fgbopal_09_bud_rsrv ) gasto   ,9 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title  "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_10_adopt_bud + fg.fgbopal_10_bud_adjt ) presu , SUM(fg.fgbopal_10_ytd_actv + fg.fgbopal_10_encumb + fg.fgbopal_10_bud_rsrv ) gasto   ,10 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title  "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_11_adopt_bud + fg.fgbopal_11_bud_adjt ) presu , SUM(fg.fgbopal_11_ytd_actv + fg.fgbopal_11_encumb + fg.fgbopal_11_bud_rsrv ) gasto  , 11 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title  "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_12_adopt_bud + fg.fgbopal_12_bud_adjt ) presu , SUM(fg.fgbopal_12_ytd_actv + fg.fgbopal_12_encumb + fg.fgbopal_12_bud_rsrv ) gasto  , 12 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title  ";
			} else {
				sql = ""
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_00_adopt_bud + fg.fgbopal_00_bud_adjt ) presu , SUM(fg.fgbopal_00_ytd_actv + fg.fgbopal_00_encumb + fg.fgbopal_00_bud_rsrv ) gasto, 0 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_01_adopt_bud + fg.fgbopal_01_bud_adjt ) presu , SUM(fg.fgbopal_01_ytd_actv + fg.fgbopal_01_encumb + fg.fgbopal_01_bud_rsrv ) gasto, 1 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_02_adopt_bud + fg.fgbopal_02_bud_adjt ) presu , SUM(fg.fgbopal_02_ytd_actv + fg.fgbopal_02_encumb + fg.fgbopal_02_bud_rsrv ) gasto, ff.ftvfund_title , 2 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_03_adopt_bud + fg.fgbopal_03_bud_adjt ) presu , SUM(fg.fgbopal_03_ytd_actv + fg.fgbopal_03_encumb + fg.fgbopal_03_bud_rsrv ) gasto, 3 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_04_adopt_bud + fg.fgbopal_04_bud_adjt ) presu , SUM(fg.fgbopal_04_ytd_actv + fg.fgbopal_04_encumb + fg.fgbopal_04_bud_rsrv ) gasto,  4 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_05_adopt_bud + fg.fgbopal_05_bud_adjt ) presu , SUM(fg.fgbopal_05_ytd_actv + fg.fgbopal_05_encumb + fg.fgbopal_05_bud_rsrv ) gasto,  5 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_06_adopt_bud + fg.fgbopal_06_bud_adjt ) presu , SUM(fg.fgbopal_06_ytd_actv + fg.fgbopal_06_encumb + fg.fgbopal_06_bud_rsrv ) gasto, ff.ftvfund_title   , 6 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_07_adopt_bud + fg.fgbopal_07_bud_adjt ) presu , SUM(fg.fgbopal_07_ytd_actv + fg.fgbopal_07_encumb + fg.fgbopal_07_bud_rsrv ) gasto, 7 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_08_adopt_bud + fg.fgbopal_08_bud_adjt ) presu , SUM(fg.fgbopal_08_ytd_actv + fg.fgbopal_08_encumb + fg.fgbopal_08_bud_rsrv ) gasto, ff.ftvfund_title   ,8 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_09_adopt_bud + fg.fgbopal_09_bud_adjt ) presu , SUM(fg.fgbopal_09_ytd_actv + fg.fgbopal_09_encumb + fg.fgbopal_09_bud_rsrv ) gasto, ff.ftvfund_title   ,9 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_10_adopt_bud + fg.fgbopal_10_bud_adjt ) presu , SUM(fg.fgbopal_10_ytd_actv + fg.fgbopal_10_encumb + fg.fgbopal_10_bud_rsrv ) gasto, ff.ftvfund_title   ,10 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_11_adopt_bud + fg.fgbopal_11_bud_adjt ) presu , SUM(fg.fgbopal_11_ytd_actv + fg.fgbopal_11_encumb + fg.fgbopal_11_bud_rsrv ) gasto,  11 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title "
						+ " UNION ALL "
						+ " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , SUM(fg.FGBOPAL_12_adopt_bud + fg.fgbopal_12_bud_adjt ) presu , SUM(fg.fgbopal_12_ytd_actv + fg.fgbopal_12_encumb + fg.fgbopal_12_bud_rsrv ) gasto,  12 mes "
						+ "FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title ,  ff.ftvfund_title ";
			}

			//System.out.println("Union sql " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			long acumulado = 0;
			long gastos = 0;
			while (res.next()) {
				// String aux = res.getString(1);
				Pres18POJO ss = new Pres18POJO();
				ss.setCoduni(res.getString(1)); // nombre + apellidos
				ss.setDesuni(res.getString(2)); // titulo
				ss.setPresum(res.getLong(3));
				acumulado = acumulado + res.getLong(3);
				ss.setPresac(acumulado);
				ss.setUsadom(res.getLong(4));
				gastos = gastos + res.getLong(4);
				if (gastos > 0) {
					ss.setIddigi("S");
					// System.out.println ("Valor setIddigi S" );
				} else {
					ss.setIddigi("N");
					// System.out.println ("Valor setIddigi N" );
				}
				ss.setUsadac(gastos);
				ss.setNummes(res.getInt(5));
				lista.add(ss);
			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB getCuentasDetalles: "+ e.getMessage());
		}
		return lista;
	}
*/
	public int getLeeOrgTemporal() {
		int sw = 0;
		try {
			ConexionBanner con = new ConexionBanner();
			PreparedStatement sent = null;
			ResultSet res = null;
			String sql = "";

			sql = ""
					+ " SELECT COUNT(*) FROM TMP_ORGANIZACIONES ";
																																																																																																																																	// =
																																																																																																																																			// 3";
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();

			if (res.next()) {
				sw = res.getInt(1);
			}
			//System.out.println("No entro a while verificaDetalle");
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB getLeeOrgTemporal: " + e.getMessage());
		}
		return sw;
	}
	public int getBorraOrgTemporal() {
		int sw = 0;
		try {
			ConexionBanner con = new ConexionBanner();
			PreparedStatement sent = null;
			ResultSet res = null;
			String sql = "";

			sql = ""
					+ " EXECUTE IMMEDIATE 'TRUNCATE TABLE TMP_ORGANIZACIONES' ";
																																																																																																																																	// =
																																																																																																																																			// 3";
			sent = con.conexion().prepareStatement("TRUNCATE TABLE TMP_ORGANIZACIONES");
			// sent.setString(1,rutnumdv);
			sw = sent.executeUpdate();

		
			//System.out.println("No entro a while verificaDetalle");
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB getBorraOrgTemporal: " + e.getMessage());
		}
		return sw;
	}
	public Collection<Pres18POJO> getSaldos(int a�oIN, Connection cone, int mes) {
		/*if (a�oIN == 0) {
			Calendar fecha = Calendar.getInstance();
			a�oIN = fecha.get(Calendar.YEAR);
		}*/
		if (a�oIN != 0 ) {
		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
		a�oIN = a�o;
		} 
		Pres18POJO pres18POJO = new Pres18POJO();
		PreparedStatement sent = null;
		ResultSet res = null;
		List<Pres18POJO> listaSaldo = new ArrayList<Pres18POJO>();
	
		try {
			String sql = "";
			
				sql = ""					
					+"select "
					+" FGBTRND_ORGN_CODE,"
					+" FTVORGN_TITLE,"
					+"  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,?,FGBTRND.FGBTRND_TRANS_AMT,0),0))+ "
					+"  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,?,FGBTRND.FGBTRND_TRANS_AMT,0),0))+ "
					+"  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,?,FGBTRND.FGBTRND_TRANS_AMT,0),0))+ "
					+"  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,?,FGBTRND.FGBTRND_TRANS_AMT,0),0))+  "
					+"  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,?,FGBTRND.FGBTRND_TRANS_AMT,0),0))+ "
					+"  (SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,?,FGBTRND.FGBTRND_TRANS_AMT,0),0))+ "
					+ " SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,?,fgbtrnd.fgbtrnd_trans_amt,0),0))) + "
					+"  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,?,FGBTRND.FGBTRND_TRANS_AMT,0),0)) +"
					+ " SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,?,fgbtrnd.fgbtrnd_trans_amt,0),0) ) +"
					+ "  SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,?,fgbtrnd.fgbtrnd_trans_amt,0),0) ) "
					+ " -"
					+"  SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0))- "
					+"  SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) saldo "
					+"  ,FTVFUND_TITLE fondo"
					+" FROM FIMSMGR.FGBTRND,"
					+"  FIMSMGR.FTVFUND,"
					+"  FIMSMGR.FTVORGN,"
					+"  FIMSMGR.FTVACCT,"
					+"  FIMSMGR.FTVPROG,"
					+"  TMP_ORGANIZACIONES"
					+" WHERE FGBTRND_FUND_CODE      =FTVFUND_FUND_CODE"
					+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
					+" AND FGBTRND_ORGN_CODE        =FTVORGN_ORGN_CODE"
					+" AND FGBTRND_PROG_CODE        =FTVPROG_PROG_CODE"
					+" AND FGBTRND_ACCT_CODE        =FTVACCT_ACCT_CODE"
					+" AND FGBTRND_ACTIVITY_DATE   >=FTVFUND_EFF_DATE"
					+" AND FGBTRND_ACTIVITY_DATE    <FTVFUND_NCHG_DATE"
					+" AND FGBTRND_ACTIVITY_DATE   >=FTVORGN_EFF_DATE"
					+" AND FGBTRND_ACTIVITY_DATE    <FTVORGN_NCHG_DATE"
					+" AND FGBTRND_ACTIVITY_DATE   >=FTVPROG_EFF_DATE"
					+" AND FGBTRND_ACTIVITY_DATE    <FTVPROG_NCHG_DATE"
					+" AND FGBTRND_ACTIVITY_DATE   >=FTVACCT_EFF_DATE"
					+" AND FGBTRND_ACTIVITY_DATE    <FTVACCT_NCHG_DATE"
					+" AND FTVFUND_FTYP_CODE       IN(?,?,?,?,?)"
					+ " AND NOT ( FGBTRND_RUCL_CODE IN (?,?,?,?,?,?,?,?) AND "
					+ "       FTVACCT_ATYP_CODE IN "
					+ "         (SELECT FTVATYP_ATYP_CODE "
					+ "          FROM FTVATYP"
					+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = ?) )"
					+" AND FTVFUND_DATA_ENTRY_IND   = ? "
					+" AND FTVORGN_DATA_ENTRY_IND   =?"
					+" AND FTVACCT_DATA_ENTRY_IND   =?"
					+" AND FTVPROG_DATA_ENTRY_IND   =?"
					+ " AND FTVORGN_STATUS_IND = ?"
				//	+ " AND FTVORGN_FUND_CODE_DEF = fgbtrnd_fund_code"
					+" AND FGBTRND.FGBTRND_LEDC_CODE= ? "
					+" AND FGBTRND_LEDGER_IND       =? "
					+" AND FGBTRND_FSYR_CODE        =? "
					+ " AND  FGBTRND_POSTING_PERIOD BETWEEN ? AND ? "
				//	+" AND FGBTRND_ORGN_CODE       IN('3B21GE') "
					+" AND FGBTRND_ORGN_CODE       = TMP_ORGANIZACIONES_ORGN "
					+" GROUP BY FGBTRND_ORGN_CODE, FTVFUND_TITLE, FTVORGN_TITLE ORDER BY FGBTRND_ORGN_CODE ASC";
			//System.out.println("Saldo " + sql);
			sent = cone.prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			
			sent.setString(1, "BD01");
			sent.setString(2, "BD05");
			sent.setString(3, "BD07");
			sent.setString(4, "BD06");
			sent.setString(5, "BD02");
			sent.setString(6, "BD08");
			sent.setString(7, "BD13");
			sent.setString(8, "BD09");
			sent.setString(9, "BD11");
			sent.setString(10, "BD12");
			sent.setString(11, "CA");
			sent.setString(12, "CD");
			sent.setString(13, "CN"); 
			sent.setString(14, "FI");
			sent.setString(15, "FR");
			sent.setString(16, "APS1");
			sent.setString(17, "APS2");
			sent.setString(18, "APS3"); 
			sent.setString(19, "APS4");
			sent.setString(20, "CHS1");
			sent.setString(21, "CSS1");
			sent.setString(22, "JE15");
			sent.setString(23, "CR05"); 
			sent.setString(24, "50");
			sent.setString(25, "Y");
			sent.setString(26, "Y");
			sent.setString(27, "Y");
			sent.setString(28, "Y");
			sent.setString(29, "A");
			sent.setString(30, "FINANC");
			sent.setString(31, "O");
			sent.setInt(32, a�oIN);
			sent.setInt(33, 0);
			sent.setInt(34, mes);



			res = sent.executeQuery();
			long acumulado = 0;
			long gastos = 0;
			while (res.next()) {
				// String aux = res.getString(1);
				Pres18POJO ss = new Pres18POJO();
				ss.setCoduni(res.getString(1)); // codigo organizacion
				ss.setDesuni(res.getString(2)) ; // nombre organizacion
				ss.setAcumum(res.getLong(3));    // saldo
				ss.setNomtip(res.getString(4)); // nombre fondo
				listaSaldo.add(ss);
			}
			
		/*	sent = cone.prepareStatement("TRUNCATE TABLE TMP_ORGANIZACIONES");
			// sent.setString(1,rutnumdv);
			sent.executeUpdate();
*/
			
			res.close();
			sent.close();
		
		} catch (SQLException e) {
			System.out.println("Error sipB : "+ e.getMessage());
		}
		return listaSaldo;
	}

	
	
	public int verificaDetalle(String org, int a�oIN, int mes) {
		int sw = 0;
		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
		
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		String mesfinal = "";
		if (mes < 10) {
			mesfinal = "0" + Integer.toString(mes);
		} else {
			mesfinal = Integer.toString(mes);
		}
		try {
			String sql = "";

			sql = ""
					+ " SELECT FG.FGBTRND_ACCT_CODE,fv.FTVACCT_TITLE , FD.FTVDTYP_DESC ,fg.FGBTRND_DOC_CODE ,to_char(Fh.FGBTRNH_TRANS_DATE,'dd/mm/yy HH24:mi:ss'), "
					+ " FH.FGBTRNH_TRANS_DESC ,FG.FGBTRND_TRANS_AMT   "
					+ " from fgbtrnd FG , ftvfund FT , ftvacct FV , FTVDTYP FD  ,FGBTRNH FH "
					// + " WHERE FG.FGBTRND_DOC_SEQ_CODE = FD.FTVDTYP_SEQ_NUM
					// AND FG.FGBTRND_ACCT_CODE = fv.FTVACCT_ACCT_CODE and
					// FG.fgbtrnd_FUND_CODE = FT.FTVFUND_FUND_CODE AND
					// FG.fgbtrnd_POSTING_PERIOD = '"+ mesfinal + "' AND
					// FG.FGBTRND_ORGN_CODE = '" + org + "' and
					// FG.FGBTRND_FSYR_CODE = '"+ a�o+"'"
					+ " WHERE FG.FGBTRND_DOC_SEQ_CODE = FD.FTVDTYP_SEQ_NUM AND FGBTRND_ACCT_CODE <> '4BD007' AND  FG.FGBTRND_ACCT_CODE = fv.FTVACCT_ACCT_CODE and FG.fgbtrnd_FUND_CODE = FT.FTVFUND_FUND_CODE  AND FG.FGBTRND_ORGN_CODE = '"
					+ org
					+ "' and FG.FGBTRND_FSYR_CODE = '"
					+ a�o
					+ "'"
					+ " and FH.FGBTRNH_RUCL_CODE = FG.FGBTRND_RUCL_CODE  and FH.FGBTRNH_SEQ_NUM = FG.FGBTRND_SEQ_NUM and fg.FGBTRND_DOC_CODE = fh.FGBTRNH_DOC_CODE and FG.FGBTRND_ACCT_CODE = FH.FGBTRNH_ACCT_CODE and FG.FGBTRND_ORGN_CODE = FH.FGBTRNH_ORGN_CODE and (fg.FGBTRND_LEDGER_IND = 'O' ) AND (fg.FGBTRND_FIELD_CODE = 3 OR fg.FGBTRND_FIELD_CODE  = 4 OR fg.FGBTRND_FIELD_CODE  = 5  OR fg.FGBTRND_FIELD_CODE = 2  OR fg.FGBTRND_FIELD_CODE = 1) and fg.FGBTRND_LEDC_CODE = 'FINANC' AND fh.FGBTRNH_LEDC_CODE = 'FINANC' "; // and
																																																																																																																																			// fg.FGBTRND_DOC_SEQ_CODE
																																																																																																																																			// =
																																																																																																																																			// 3";
			//System.out.println("SQL verificaDetalle " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();

			//System.out.println("Va a entrar al while verificaDetalle");
			while (res.next()) {
				sw = 1;
				//System.out.println("Si entro a while verificaDetalle");
			}
			//System.out.println("No entro a while verificaDetalle");
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB verificaDetalle: " + e.getMessage());
		}
		return sw;
	}
/*
	public Collection<Pres18POJO> getCuentasDetalles_RespaldoV2(String org,
			int a�oIN, int mes, String cuenta) {

		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
	
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;
		int sw = 0;
		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		try {
			//System.out.println("Va a llamar a verificaDetalle");
			sw = verificaDetalle(org, a�oIN, mes);
			//System.out.println("Resultado Va a llamar a verificaDetalle " + sw);
		} catch (Exception ex) {

		}

		try {
			String sql = "";
			if (cuenta.equals("-1")) {
				sql = ""
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ " SUM((FGBOPAL_00_ADOPT_BUD ) + (FGBOPAL_00_BUD_ADJT)) presu"
						+ " , SUM(fg.FGBOPAL_00_YTD_ACTV) gasto  ,"
						+ " ROUND(SUM(FG.FGBOPAL_01_YTD_ACTV) / NULLIF( (SUM(FG.FGBOPAL_00_ADOPT_BUD + FG.FGBOPAL_01_ADOPT_BUD) + SUM(FG.FGBOPAL_00_BUD_ADJT + FG.FGBOPAL_01_BUD_ADJT)),0 )*100,2 )  porc   "
						+ " , 0 mes,"
						+ " SUM((FGBOPAL_00_ADOPT_BUD ) + (FGBOPAL_00_BUD_ADJT ) - FGBOPAL_00_YTD_ACTV )saldo_mes ,"
						+ " SUM (FGBOPAL_00_ADOPT_BUD + FGBOPAL_00_BUD_ADJT) pre_acum,"
						+ " sum (FGBOPAL_00_YTD_ACTV) gasto_acum ,"
						+ " round(SUM(fg.FGBOPAL_01_YTD_ACTV) / NULLIF( (SUM(fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_01_BUD_ADJT) *100),0),2) porc2 ,"
						+ " SUM((FGBOPAL_00_ADOPT_BUD ) - FGBOPAL_00_YTD_ACTV) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff   "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'"
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 0   "
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ " SUM((fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)) presu"
						// + " SUM((FGBOPAL_01_ADOPT_BUD ) +
						// (FGBOPAL_01_BUD_ADJT)) presu"
						+ " , SUM(fg.FGBOPAL_01_YTD_ACTV) gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_01_YTD_ACTV) / NULLIF( (SUM(fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + SUM(fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)),0 )*100 ),2) porc "
						+ " , 1 mes,"
						// + " SUM((fg.FGBOPAL_00_ADOPT_BUD +
						// fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT +
						// fg.FGBOPAL_01_BUD_ADJT) - fg.FGBOPAL_01_YTD_ACTV
						// )saldo_mes ,"
						+ " SUM (fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_01_BUD_ADJT) pre_acum,"
						+ " SUM((fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)) presu_acum ,"
						+ " sum (fg.FGBOPAL_01_YTD_ACTV) gasto_acum ,"
						+ " ROUND((sum(FGBOPAL_01_YTD_ACTV) / NULLIF( SUM(FGBOPAL_01_ADOPT_BUD + FGBOPAL_01_BUD_ADJT),0) ) *100,2 ) porc2, "
						+ " SUM((fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_01_BUD_ADJT) - fg.FGBOPAL_01_YTD_ACTV) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'"
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 1   "
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT) - (fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)) presu"
						// + " SUM((FGBOPAL_02_ADOPT_BUD ) +
						// (FGBOPAL_02_BUD_ADJT)) presu"
						+ " , (SUM(FGBOPAL_02_YTD_ACTV) - SUM(FGBOPAL_01_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_02_YTD_ACTV) - SUM(fg.FGBOPAL_01_YTD_ACTV) ) / NULLIF( ((  (SUM(fg.FGBOPAL_02_ADOPT_BUD) + SUM(fg.FGBOPAL_02_BUD_ADJT))) - ((SUM(fg.FGBOPAL_00_ADOPT_BUD) + SUM(fg.FGBOPAL_01_ADOPT_BUD))) + ((SUM(fg.FGBOPAL_00_BUD_ADJT) + SUM(fg.FGBOPAL_01_BUD_ADJT)))),0) *100  ,2) porc"
						+ " , 2 mes,"
						+ " SUM((FG.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT) - (FG.FGBOPAL_00_ADOPT_BUD + FG.FGBOPAL_01_ADOPT_BUD) + (FG.FGBOPAL_00_BUD_ADJT + FG.FGBOPAL_01_BUD_ADJT) - FG.FGBOPAL_02_YTD_ACTV) saldo_mes ,"
						+ " SUM (fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT) pre_acum,"
						// + " SUM((fg.FGBOPAL_02_ADOPT_BUD +
						// fg.FGBOPAL_02_BUD_ADJT) - (fg.FGBOPAL_00_ADOPT_BUD +
						// fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT +
						// fg.FGBOPAL_01_BUD_ADJT)) pre_acum, "
						+ " sum (fg.FGBOPAL_02_YTD_ACTV) gasto_acum ,"
						+ " ROUND((sum(fg.FGBOPAL_02_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT),0)) ,2 ) *100  porc2 ,"
						+ "   (SUM (fg.FGBOPAL_02_ADOPT_BUD) + sum(fg.FGBOPAL_02_BUD_ADJT)) - sum(fg.FGBOPAL_02_YTD_ACTV) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'"
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 2 "
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT) - ( fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_03_ADOPT_BUD ) +
						// (FGBOPAL_03_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_03_YTD_ACTV) - sum(fg.FGBOPAL_02_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_03_YTD_ACTV) - SUM(fg.FGBOPAL_02_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_03_ADOPT_BUD) + SUM(fg.FGBOPAL_03_BUD_ADJT)) - (SUM(fg.FGBOPAL_02_ADOPT_BUD) + SUM(fg.FGBOPAL_02_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 3 mes,"
						+ " (SUM(FG.FGBOPAL_03_ADOPT_BUD) + SUM(FG.FGBOPAL_03_BUD_ADJT))-(SUM(FG.FGBOPAL_02_ADOPT_BUD + FG.FGBOPAL_02_BUD_ADJT))- sum(FG.FGBOPAL_03_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_03_ADOPT_BUD ) + (FGBOPAL_03_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_03_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_03_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT) - SUM(fg.FGBOPAL_03_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'"
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 3  "
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT) - ( fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_04_ADOPT_BUD ) +
						// (FGBOPAL_04_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_04_YTD_ACTV) - sum(fg.FGBOPAL_03_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_04_YTD_ACTV) - SUM(fg.FGBOPAL_03_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_04_ADOPT_BUD) + SUM(fg.FGBOPAL_04_BUD_ADJT)) - (SUM(fg.FGBOPAL_03_ADOPT_BUD) + SUM(fg.FGBOPAL_03_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 4 mes,"
						+ " (SUM(FG.FGBOPAL_04_ADOPT_BUD) + SUM(FG.FGBOPAL_04_BUD_ADJT))-(SUM(FG.FGBOPAL_03_ADOPT_BUD + FG.FGBOPAL_03_BUD_ADJT))- sum(FG.FGBOPAL_04_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_04_ADOPT_BUD ) + (FGBOPAL_04_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_04_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_04_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT) - SUM(fg.FGBOPAL_04_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'"
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 4  "
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT) - ( fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_05_ADOPT_BUD ) +
						// (FGBOPAL_05_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_05_YTD_ACTV) - sum(fg.FGBOPAL_04_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_05_YTD_ACTV) - SUM(fg.FGBOPAL_04_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_05_ADOPT_BUD) + SUM(fg.FGBOPAL_05_BUD_ADJT)) - (SUM(fg.FGBOPAL_04_ADOPT_BUD) + SUM(fg.FGBOPAL_04_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 5 mes,"
						+ " (SUM(FG.FGBOPAL_05_ADOPT_BUD) + SUM(FG.FGBOPAL_05_BUD_ADJT))-(SUM(FG.FGBOPAL_04_ADOPT_BUD + FG.FGBOPAL_04_BUD_ADJT))- sum(FG.FGBOPAL_05_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_05_ADOPT_BUD ) + (FGBOPAL_05_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_05_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_05_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT) - SUM(fg.FGBOPAL_05_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'"
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,5 "
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT) - ( fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_06_ADOPT_BUD ) +
						// (FGBOPAL_06_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_06_YTD_ACTV) - sum(fg.FGBOPAL_05_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_06_YTD_ACTV) - SUM(fg.FGBOPAL_05_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_06_ADOPT_BUD) + SUM(fg.FGBOPAL_06_BUD_ADJT)) - (SUM(fg.FGBOPAL_05_ADOPT_BUD) + SUM(fg.FGBOPAL_05_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 6 mes,"
						+ " (SUM(FG.FGBOPAL_06_ADOPT_BUD) + SUM(FG.FGBOPAL_06_BUD_ADJT))-(SUM(FG.FGBOPAL_05_ADOPT_BUD + FG.FGBOPAL_05_BUD_ADJT))- sum(FG.FGBOPAL_06_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_06_ADOPT_BUD ) + (FGBOPAL_06_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_06_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_06_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT) - SUM(fg.FGBOPAL_06_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'"
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,6"
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT) - ( fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_07_ADOPT_BUD ) +
						// (FGBOPAL_07_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_07_YTD_ACTV) - sum(fg.FGBOPAL_06_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_07_YTD_ACTV) - SUM(fg.FGBOPAL_06_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_07_ADOPT_BUD) + SUM(fg.FGBOPAL_07_BUD_ADJT)) - (SUM(fg.FGBOPAL_06_ADOPT_BUD) + SUM(fg.FGBOPAL_06_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 7 mes,"
						+ " (SUM(FG.FGBOPAL_07_ADOPT_BUD) + SUM(FG.FGBOPAL_07_BUD_ADJT))-(SUM(FG.FGBOPAL_06_ADOPT_BUD + FG.FGBOPAL_06_BUD_ADJT))- sum(FG.FGBOPAL_07_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_07_ADOPT_BUD ) + (FGBOPAL_07_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_07_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_07_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT) - SUM(fg.FGBOPAL_07_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'"
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,7"
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT) - ( fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_08_ADOPT_BUD ) +
						// (FGBOPAL_08_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_08_YTD_ACTV) - sum(fg.FGBOPAL_07_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_08_YTD_ACTV) - SUM(fg.FGBOPAL_07_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_08_ADOPT_BUD) + SUM(fg.FGBOPAL_08_BUD_ADJT)) - (SUM(fg.FGBOPAL_07_ADOPT_BUD) + SUM(fg.FGBOPAL_07_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 8 mes,"
						+ " (SUM(FG.FGBOPAL_08_ADOPT_BUD) + SUM(FG.FGBOPAL_08_BUD_ADJT))-(SUM(FG.FGBOPAL_07_ADOPT_BUD + FG.FGBOPAL_07_BUD_ADJT))- sum(FG.FGBOPAL_08_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_08_ADOPT_BUD ) + (FGBOPAL_08_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_08_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_08_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT) - SUM(fg.FGBOPAL_08_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'"
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,8"
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT) - ( fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_09_ADOPT_BUD ) +
						// (FGBOPAL_09_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_09_YTD_ACTV) - sum(fg.FGBOPAL_08_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_09_YTD_ACTV) - SUM(fg.FGBOPAL_08_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_09_ADOPT_BUD) + SUM(fg.FGBOPAL_09_BUD_ADJT)) - (SUM(fg.FGBOPAL_08_ADOPT_BUD) + SUM(fg.FGBOPAL_08_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 9 mes,"
						+ " (SUM(FG.FGBOPAL_09_ADOPT_BUD) + SUM(FG.FGBOPAL_09_BUD_ADJT))-(SUM(FG.FGBOPAL_08_ADOPT_BUD + FG.FGBOPAL_08_BUD_ADJT))- sum(FG.FGBOPAL_09_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_09_ADOPT_BUD ) + (FGBOPAL_09_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_09_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_09_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT) - SUM(fg.FGBOPAL_09_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'"
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,9"
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT) - ( fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_10_ADOPT_BUD ) +
						// (FGBOPAL_10_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_10_YTD_ACTV) - sum(fg.FGBOPAL_09_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_10_YTD_ACTV) - SUM(fg.FGBOPAL_09_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_10_ADOPT_BUD) + SUM(fg.FGBOPAL_10_BUD_ADJT)) - (SUM(fg.FGBOPAL_09_ADOPT_BUD) + SUM(fg.FGBOPAL_09_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 10 mes,"
						+ " (SUM(FG.FGBOPAL_10_ADOPT_BUD) + SUM(FG.FGBOPAL_10_BUD_ADJT))-(SUM(FG.FGBOPAL_09_ADOPT_BUD + FG.FGBOPAL_09_BUD_ADJT))- sum(FG.FGBOPAL_10_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_10_ADOPT_BUD ) + (FGBOPAL_10_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_10_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_10_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT) - SUM(fg.FGBOPAL_10_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'"
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,10"
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT) - ( fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_11_ADOPT_BUD ) +
						// (FGBOPAL_11_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_11_YTD_ACTV) - sum(fg.FGBOPAL_10_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_11_YTD_ACTV) - SUM(fg.FGBOPAL_10_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_11_ADOPT_BUD) + SUM(fg.FGBOPAL_11_BUD_ADJT)) - (SUM(fg.FGBOPAL_10_ADOPT_BUD) + SUM(fg.FGBOPAL_10_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 11 mes,"
						+ " (SUM(FG.FGBOPAL_11_ADOPT_BUD) + SUM(FG.FGBOPAL_11_BUD_ADJT))-(SUM(FG.FGBOPAL_10_ADOPT_BUD + FG.FGBOPAL_10_BUD_ADJT))- sum(FG.FGBOPAL_11_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_11_ADOPT_BUD ) + (FGBOPAL_11_BUD_ADJT))  pre_acum,"
						+ " sum (fg.FGBOPAL_11_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_11_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT) - SUM(fg.FGBOPAL_11_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'"
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,11"
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT) - ( fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_12_ADOPT_BUD ) +
						// (FGBOPAL_12_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_12_YTD_ACTV) - sum(fg.FGBOPAL_11_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_12_YTD_ACTV) - SUM(fg.FGBOPAL_11_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_12_ADOPT_BUD) + SUM(fg.FGBOPAL_12_BUD_ADJT)) - (SUM(fg.FGBOPAL_11_ADOPT_BUD) + SUM(fg.FGBOPAL_11_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 12 mes,"
						+ " (SUM(FG.FGBOPAL_12_ADOPT_BUD) + SUM(FG.FGBOPAL_12_BUD_ADJT))-(SUM(FG.FGBOPAL_11_ADOPT_BUD + FG.FGBOPAL_11_BUD_ADJT))- sum(FG.FGBOPAL_12_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_12_ADOPT_BUD ) + (FGBOPAL_12_BUD_ADJT))  pre_acum,"
						+ " sum (fg.FGBOPAL_12_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_12_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT) - SUM(fg.FGBOPAL_12_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'"
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,12"

				;

			} else {
				sql = ""
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ " SUM((FGBOPAL_00_ADOPT_BUD ) + (FGBOPAL_00_BUD_ADJT)) presu"
						+ " , SUM(fg.FGBOPAL_00_YTD_ACTV) gasto  ,"
						+ "  ROUND(SUM(FG.FGBOPAL_01_YTD_ACTV) / NULLIF( (SUM(FG.FGBOPAL_00_ADOPT_BUD + FG.FGBOPAL_01_ADOPT_BUD) + SUM(FG.FGBOPAL_00_BUD_ADJT + FG.FGBOPAL_01_BUD_ADJT)),0 )*100,2 )  porc   "
						+ " , 0 mes,"
						+ " SUM((FGBOPAL_00_ADOPT_BUD ) + (FGBOPAL_00_BUD_ADJT ) - FGBOPAL_00_YTD_ACTV )saldo_mes ,"
						+ " SUM (FGBOPAL_00_ADOPT_BUD + FGBOPAL_00_BUD_ADJT) pre_acum,"
						+ " sum (FGBOPAL_00_YTD_ACTV) gasto_acum ,"
						+ " round(SUM(fg.FGBOPAL_01_YTD_ACTV) / NULLIF( (SUM(fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_01_BUD_ADJT) *100),0),2) porc2 ,"
						+ " SUM((FGBOPAL_00_ADOPT_BUD ) - FGBOPAL_00_YTD_ACTV) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff   "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 0   "
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ " SUM((fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)) presu"
						// + " SUM((FGBOPAL_01_ADOPT_BUD ) +
						// (FGBOPAL_01_BUD_ADJT)) presu"
						+ " , SUM(fg.FGBOPAL_01_YTD_ACTV) gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_01_YTD_ACTV) / NULLIF( (SUM(fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + SUM(fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)),0 )*100 ),2) porc "
						+ " , 1 mes,"
						+ " SUM((fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT) - fg.FGBOPAL_01_YTD_ACTV )saldo_mes ,"
						+ " SUM((FGBOPAL_01_ADOPT_BUD ) + (FGBOPAL_01_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_01_YTD_ACTV) gasto_acum ,"
						+ " ROUND((sum(FGBOPAL_01_YTD_ACTV) / NULLIF( SUM(FGBOPAL_01_ADOPT_BUD + FGBOPAL_01_BUD_ADJT),0) ) *100,2 ) porc2 ,"
						+ " SUM((fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_01_BUD_ADJT) - fg.FGBOPAL_01_YTD_ACTV) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 1   "
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT) - (fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)) presu"
						// + " SUM((FGBOPAL_02_ADOPT_BUD ) +
						// (FGBOPAL_02_BUD_ADJT)) presu"
						+ " , (SUM(FGBOPAL_02_YTD_ACTV) - SUM(FGBOPAL_01_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_02_YTD_ACTV) - SUM(fg.FGBOPAL_01_YTD_ACTV) ) / NULLIF( ((  (SUM(fg.FGBOPAL_02_ADOPT_BUD) + SUM(fg.FGBOPAL_02_BUD_ADJT))) - ((SUM(fg.FGBOPAL_00_ADOPT_BUD) + SUM(fg.FGBOPAL_01_ADOPT_BUD))) + ((SUM(fg.FGBOPAL_00_BUD_ADJT) + SUM(fg.FGBOPAL_01_BUD_ADJT)))),0) *100  ,2) porc"
						+ " , 2 mes,"
						+ " SUM((FG.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT) - (FG.FGBOPAL_00_ADOPT_BUD + FG.FGBOPAL_01_ADOPT_BUD) + (FG.FGBOPAL_00_BUD_ADJT + FG.FGBOPAL_01_BUD_ADJT) - FG.FGBOPAL_02_YTD_ACTV) saldo_mes ,"
						+ "  SUM((FGBOPAL_02_ADOPT_BUD ) + (FGBOPAL_02_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_02_YTD_ACTV) gasto_acum ,"
						+ " ROUND((sum(fg.FGBOPAL_02_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT),0)) ,2 ) *100  porc2 ,"
						+ "   (SUM (fg.FGBOPAL_02_ADOPT_BUD) + sum(fg.FGBOPAL_02_BUD_ADJT)) - sum(fg.FGBOPAL_02_YTD_ACTV) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 2 "
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT) - ( fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_03_ADOPT_BUD ) +
						// (FGBOPAL_03_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_03_YTD_ACTV) - sum(fg.FGBOPAL_02_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_03_YTD_ACTV) - SUM(fg.FGBOPAL_02_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_03_ADOPT_BUD) + SUM(fg.FGBOPAL_03_BUD_ADJT)) - (SUM(fg.FGBOPAL_02_ADOPT_BUD) + SUM(fg.FGBOPAL_02_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 3 mes,"
						+ " (SUM(FG.FGBOPAL_03_ADOPT_BUD) + SUM(FG.FGBOPAL_03_BUD_ADJT))-(SUM(FG.FGBOPAL_02_ADOPT_BUD + FG.FGBOPAL_02_BUD_ADJT))- sum(FG.FGBOPAL_03_YTD_ACTV) saldo_mes  ,"
						+ " SUM((FGBOPAL_03_ADOPT_BUD ) + (FGBOPAL_03_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_03_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_03_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT) - SUM(fg.FGBOPAL_03_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 3  "
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT) - ( fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_04_ADOPT_BUD ) +
						// (FGBOPAL_04_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_04_YTD_ACTV) - sum(fg.FGBOPAL_03_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_04_YTD_ACTV) - SUM(fg.FGBOPAL_03_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_04_ADOPT_BUD) + SUM(fg.FGBOPAL_04_BUD_ADJT)) - (SUM(fg.FGBOPAL_03_ADOPT_BUD) + SUM(fg.FGBOPAL_03_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 4 mes,"
						+ " (SUM(FG.FGBOPAL_04_ADOPT_BUD) + SUM(FG.FGBOPAL_04_BUD_ADJT))-(SUM(FG.FGBOPAL_03_ADOPT_BUD + FG.FGBOPAL_03_BUD_ADJT))- sum(FG.FGBOPAL_04_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_04_ADOPT_BUD ) + (FGBOPAL_04_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_04_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_04_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT) - SUM(fg.FGBOPAL_04_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 4  "
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT) - ( fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_05_ADOPT_BUD ) +
						// (FGBOPAL_05_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_05_YTD_ACTV) - sum(fg.FGBOPAL_04_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_05_YTD_ACTV) - SUM(fg.FGBOPAL_04_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_05_ADOPT_BUD) + SUM(fg.FGBOPAL_05_BUD_ADJT)) - (SUM(fg.FGBOPAL_04_ADOPT_BUD) + SUM(fg.FGBOPAL_04_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 5 mes,"
						+ " (SUM(FG.FGBOPAL_05_ADOPT_BUD) + SUM(FG.FGBOPAL_05_BUD_ADJT))-(SUM(FG.FGBOPAL_04_ADOPT_BUD + FG.FGBOPAL_04_BUD_ADJT))- sum(FG.FGBOPAL_05_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_05_ADOPT_BUD ) + (FGBOPAL_05_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_05_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_05_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT) - SUM(fg.FGBOPAL_05_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,5 "
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT) - ( fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_06_ADOPT_BUD ) +
						// (FGBOPAL_06_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_06_YTD_ACTV) - sum(fg.FGBOPAL_05_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_06_YTD_ACTV) - SUM(fg.FGBOPAL_05_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_06_ADOPT_BUD) + SUM(fg.FGBOPAL_06_BUD_ADJT)) - (SUM(fg.FGBOPAL_05_ADOPT_BUD) + SUM(fg.FGBOPAL_05_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 6 mes,"
						+ " (SUM(FG.FGBOPAL_06_ADOPT_BUD) + SUM(FG.FGBOPAL_06_BUD_ADJT))-(SUM(FG.FGBOPAL_05_ADOPT_BUD + FG.FGBOPAL_05_BUD_ADJT))- sum(FG.FGBOPAL_06_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_06_ADOPT_BUD ) + (FGBOPAL_06_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_06_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_06_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT) - SUM(fg.FGBOPAL_06_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,6"
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT) - ( fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_07_ADOPT_BUD ) +
						// (FGBOPAL_07_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_07_YTD_ACTV) - sum(fg.FGBOPAL_06_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_07_YTD_ACTV) - SUM(fg.FGBOPAL_06_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_07_ADOPT_BUD) + SUM(fg.FGBOPAL_07_BUD_ADJT)) - (SUM(fg.FGBOPAL_06_ADOPT_BUD) + SUM(fg.FGBOPAL_06_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 7 mes,"
						+ " (SUM(FG.FGBOPAL_07_ADOPT_BUD) + SUM(FG.FGBOPAL_07_BUD_ADJT))-(SUM(FG.FGBOPAL_06_ADOPT_BUD + FG.FGBOPAL_06_BUD_ADJT))- sum(FG.FGBOPAL_07_YTD_ACTV) saldo_mes  ,"
						+ " SUM((FGBOPAL_07_ADOPT_BUD ) + (FGBOPAL_07_BUD_ADJT))  pre_acum,"
						+ " sum (fg.FGBOPAL_07_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_07_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT) - SUM(fg.FGBOPAL_07_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,7"
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT) - ( fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_08_ADOPT_BUD ) +
						// (FGBOPAL_08_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_08_YTD_ACTV) - sum(fg.FGBOPAL_07_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_08_YTD_ACTV) - SUM(fg.FGBOPAL_07_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_08_ADOPT_BUD) + SUM(fg.FGBOPAL_08_BUD_ADJT)) - (SUM(fg.FGBOPAL_07_ADOPT_BUD) + SUM(fg.FGBOPAL_07_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 8 mes,"
						+ " (SUM(FG.FGBOPAL_08_ADOPT_BUD) + SUM(FG.FGBOPAL_08_BUD_ADJT))-(SUM(FG.FGBOPAL_07_ADOPT_BUD + FG.FGBOPAL_07_BUD_ADJT))- sum(FG.FGBOPAL_08_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_08_ADOPT_BUD ) + (FGBOPAL_08_BUD_ADJT))  pre_acum,"
						+ " sum (fg.FGBOPAL_08_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_08_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT) - SUM(fg.FGBOPAL_08_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,8"
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT) - ( fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_09_ADOPT_BUD ) +
						// (FGBOPAL_09_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_09_YTD_ACTV) - sum(fg.FGBOPAL_08_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_09_YTD_ACTV) - SUM(fg.FGBOPAL_08_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_09_ADOPT_BUD) + SUM(fg.FGBOPAL_09_BUD_ADJT)) - (SUM(fg.FGBOPAL_08_ADOPT_BUD) + SUM(fg.FGBOPAL_08_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 9 mes,"
						+ " (SUM(FG.FGBOPAL_09_ADOPT_BUD) + SUM(FG.FGBOPAL_09_BUD_ADJT))-(SUM(FG.FGBOPAL_08_ADOPT_BUD + FG.FGBOPAL_08_BUD_ADJT))- sum(FG.FGBOPAL_09_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_09_ADOPT_BUD ) + (FGBOPAL_09_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_09_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_09_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT) - SUM(fg.FGBOPAL_09_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,9"
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT) - ( fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_10_ADOPT_BUD ) +
						// (FGBOPAL_10_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_10_YTD_ACTV) - sum(fg.FGBOPAL_09_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_10_YTD_ACTV) - SUM(fg.FGBOPAL_09_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_10_ADOPT_BUD) + SUM(fg.FGBOPAL_10_BUD_ADJT)) - (SUM(fg.FGBOPAL_09_ADOPT_BUD) + SUM(fg.FGBOPAL_09_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 10 mes,"
						+ " (SUM(FG.FGBOPAL_10_ADOPT_BUD) + SUM(FG.FGBOPAL_10_BUD_ADJT))-(SUM(FG.FGBOPAL_09_ADOPT_BUD + FG.FGBOPAL_09_BUD_ADJT))- sum(FG.FGBOPAL_10_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_10_ADOPT_BUD ) + (FGBOPAL_10_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_10_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_10_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT) - SUM(fg.FGBOPAL_10_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,10"
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT) - ( fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_11_ADOPT_BUD ) +
						// (FGBOPAL_11_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_11_YTD_ACTV) - sum(fg.FGBOPAL_10_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_11_YTD_ACTV) - SUM(fg.FGBOPAL_10_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_11_ADOPT_BUD) + SUM(fg.FGBOPAL_11_BUD_ADJT)) - (SUM(fg.FGBOPAL_10_ADOPT_BUD) + SUM(fg.FGBOPAL_10_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 11 mes,"
						+ " (SUM(FG.FGBOPAL_11_ADOPT_BUD) + SUM(FG.FGBOPAL_11_BUD_ADJT))-(SUM(FG.FGBOPAL_10_ADOPT_BUD + FG.FGBOPAL_10_BUD_ADJT))- sum(FG.FGBOPAL_11_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_11_ADOPT_BUD ) + (FGBOPAL_11_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_11_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_11_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT) - SUM(fg.FGBOPAL_11_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,11"
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT) - ( fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_12_ADOPT_BUD ) +
						// (FGBOPAL_12_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_12_YTD_ACTV) - sum(fg.FGBOPAL_11_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_12_YTD_ACTV) - SUM(fg.FGBOPAL_11_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_12_ADOPT_BUD) + SUM(fg.FGBOPAL_12_BUD_ADJT)) - (SUM(fg.FGBOPAL_11_ADOPT_BUD) + SUM(fg.FGBOPAL_11_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 12 mes,"
						+ " (SUM(FG.FGBOPAL_12_ADOPT_BUD) + SUM(FG.FGBOPAL_12_BUD_ADJT))-(SUM(FG.FGBOPAL_11_ADOPT_BUD + FG.FGBOPAL_11_BUD_ADJT))- sum(FG.FGBOPAL_12_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_12_ADOPT_BUD ) + (FGBOPAL_12_BUD_ADJT))  pre_acum,"
						+ " sum (fg.FGBOPAL_12_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_12_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT) - SUM(fg.FGBOPAL_12_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,12";
			}

			//System.out.println("Union Big sql " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			long acumulado = 0;
			long gastos = 0;
			while (res.next()) {
				// String aux = res.getString(1);
				Pres18POJO ss = new Pres18POJO();
				ss.setCoduni(res.getString(1)); // cod cuenta
				ss.setDesuni(res.getString(2)); // titulo cta
				ss.setPresum(res.getLong(3)); // presupuesto

				// acumulado = acumulado + res.getLong(3);
				// ss.setPresac(acumulado);
				ss.setUsadom(res.getLong(4)); // gastos

				
				// ss.setUsadom(res.getLong(4)); // gastos
				ss.setPorc1(res.getFloat(5)); // porc1
				ss.setNummes(res.getInt(6)); // mes
				ss.setSaldoMes(res.getLong(7)); // saldo mes
				ss.setPresac(res.getLong(8)); // pres acumulado
				ss.setUsadac(res.getLong(9)); // gasto acumulado
				ss.setPorc2(res.getFloat(10)); // porc2
				ss.setTotal(res.getLong(11)); // total

				if (sw > 0) {
					ss.setIddigi("S");

				} else {
					ss.setIddigi("N");

				}
				lista.add(ss);

			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB getCuentasDetalles: "+ e.getMessage());
		}
		return lista;
	}
*/
	/*
	public Collection<Pres18POJO> getCuentasDetalles_RESPALDO_070318(String org, int a�oIN,
			int mes, String cuenta) {
		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;
		int sw = 0;
		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
	//	System.out.println("Va a llamar a verificaDetalle");
	
		String condicion = "";
		if (cuenta.equals("-1"))
			condicion = " AND FGBTRND_ORGN_CODE IN('"+org +"')" ;
		else
			condicion = " AND FGBTRND_ORGN_CODE IN('"+org +"') AND FGBTRND_ACCT_CODE = '"+ cuenta +"'" ;
		try {
			
			String sql = ""
				+ "SELECT FGBTRND_ORGN_CODE,"
				+ "  FTVORGN_TITLE,"
				+ "  FGBTRND_POSTING_PERIOD ,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Saldo_Inicial_A, "
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Presupuesto_B ,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Traspasos_Presupuesto_C ,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD08',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Traspasos_Servicio_D, "
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0))+ SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) GASTO ,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)) SAC,"
				+ " FGBTRND_RUCL_CODE,FGBTRND_ACCT_CODE"
				+ " FROM FIMSMGR.FGBTRND,"
				+ "  FIMSMGR.FTVFUND,"
				+ "  FIMSMGR.FTVORGN,"
				+ "  FIMSMGR.FTVACCT,"
				+ "  FIMSMGR.FTVPROG"
				+ " WHERE FGBTRND_FUND_CODE      =FTVFUND_FUND_CODE"
				+ " AND FGBTRND_ORGN_CODE        =FTVORGN_ORGN_CODE"
				+ " AND FGBTRND_PROG_CODE        =FTVPROG_PROG_CODE"
				+ " AND FGBTRND_ACCT_CODE        =FTVACCT_ACCT_CODE"
				+ " AND FGBTRND_ACTIVITY_DATE   >=FTVFUND_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE    <FTVFUND_NCHG_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE   >=FTVORGN_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE    <FTVORGN_NCHG_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE   >=FTVPROG_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE    <FTVPROG_NCHG_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE   >=FTVACCT_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE    <FTVACCT_NCHG_DATE"
				+ " AND FTVFUND_FTYP_CODE       IN('CA','CD','CN','FI','FR')"
				+ " AND FTVFUND_DATA_ENTRY_IND   ='Y'"
				+ " AND FTVORGN_DATA_ENTRY_IND   ='Y'"
				+ " AND FTVACCT_DATA_ENTRY_IND   ='Y'"
				+ " AND FTVPROG_DATA_ENTRY_IND   ='Y'"
				+ " AND FGBTRND.FGBTRND_LEDC_CODE='FINANC'"
				+ " AND FGBTRND_LEDGER_IND       ='O'"
				+ " AND FGBTRND_FSYR_CODE        ='"+ a�o +"'"
				+ condicion
				+ " GROUP BY FGBTRND_ORGN_CODE, FTVFUND_TITLE, FTVORGN_TITLE, FGBTRND_POSTING_PERIOD, 0,FGBTRND_RUCL_CODE,FGBTRND_ACCT_CODE"
				+ " ORDER BY FGBTRND_POSTING_PERIOD asc";

			//System.out.println("getCuentasDetalles " + sql);
			//System.out.println("cuenta " + cuenta);
			
				

				sent = con.conexion().prepareStatement(sql);
				// sent.setString(1,rutnumdv);
				res = sent.executeQuery();
				long acumulado = 0;
				long gastos = 0;
				int mes_consulta = 0;
				int mes_sql = 0;
				long presu_acum = 0;
				long gasto_acum = 0;
				int n=res.getRow();
				Boolean saldoinicial = true;
				//System.out.println("CANTIDAD REGISTROS " + n);
				long saldoini = 0 ;
				while (res.next()) {
					
					sw = 0 ;
					
					mes_sql = res.getInt(3);
					//System.out.println("Entra a while  mes   " + mes_sql + " mes consulta " + mes_consulta + " valor ws " + sw);
					while (mes_consulta < 13 && (sw == 0)) {
						Pres18POJO ss = new Pres18POJO();
					
					
						
					//	if (res.getLong(4) > 0) {
				
						
					//	if ((saldoinicial)&& (mes_sql == 0)) {
						if ((res.getString(10).equals("BD01"))&& (res.getString(11).equals("6BF005"))) { //SALDO INICIAL
							saldoini = res.getLong(4);
				
							ss.setCoduni(res.getString(1)); // cod cuenta
							ss.setDesuni(res.getString(2)); // titulo cta
							ss.setPresum(0); // presupuesto
							ss.setUsadom(saldoini *-1); // gastos
							ss.setIddigi("N");
							ss.setPorc1(0); // porc1
							ss.setNummes(mes_consulta);
							ss.setSaldoMes(saldoini); // saldo mes
							ss.setPresac(0); // pres acumulado
							ss.setUsadac(saldoini * -1);
							ss.setPorc2(0); // porc2
							ss.setTotal((saldoini)); // total
							// lista.add(ss);
							sw = 1 ;
							 saldoinicial = true ;
						} else saldoinicial = false ;
						
						if ((mes_sql == mes_consulta)&& (saldoinicial == false)){ //&& (mes_sql > 0)) {
							 
							ss.setCoduni(res.getString(1)); // cod cuenta
							ss.setDesuni(res.getString(2)); // titulo cta
							ss.setPresum(res.getLong(5)); // presupuesto
							presu_acum = presu_acum + ss.getPresum();
							gastos = res.getLong(8);
							if (mes_sql == 1) {
								ss.setUsadom(gastos - res.getLong(7)
										- res.getLong(6) - res.getLong(9) - res.getLong(4) + saldoini); // gastos
							}
							else {
							ss.setUsadom(gastos - res.getLong(7)
									- res.getLong(6) - res.getLong(9) - res.getLong(4)); // gastos
							}
							gasto_acum = gasto_acum + ss.getUsadom();
							if (gastos > 0) {
								ss.setIddigi("S");

							} else {
								ss.setIddigi("N");

							}
							ss.setNummes(mes_sql);
							ss.setSaldoMes(ss.getPresum() - ss.getUsadom()); // saldo
																				// mes
							ss.setPresac(presu_acum); // pres acumulado
							ss.setUsadac(gasto_acum); // gasto acumulado
							ss.setTotal((presu_acum - gasto_acum)); // total
							// porcentaje ss.setPorc1(0); //
							
							if (ss.getPresum() != 0 && ss.getUsadom() != 0)
								ss.setPorc1((ss.getUsadom() / ss.getPresum())* 100);
							else
								ss.setPorc1(0);

							if (ss.getUsadac() != 0 && ss.getPresac() != 0)
								ss.setPorc2((ss.getUsadac() / ss.getPresac())* 100);
							else
								ss.setPorc2(0);
							
							sw = 1 ;
							

						} else if (saldoinicial == false) {
							
							ss.setCoduni(res.getString(1)); // cod cuenta
							ss.setDesuni(res.getString(2)); // titulo cta
							ss.setPresum(0); // presupuesto
							ss.setUsadom(0); // gastos
							ss.setIddigi("N");
							ss.setPorc1(0); // porc1
							ss.setNummes(mes_consulta);
							ss.setSaldoMes(0); // saldo mes
							ss.setPresac(presu_acum); // pres acumulado
							ss.setUsadac(gasto_acum);
							ss.setPorc2(0); // porc2
							ss.setTotal((presu_acum - gasto_acum)); // total
							
						}
						
					//	if(saldoinicial) saldoinicial = false;
						saldoinicial = false;
							 lista.add(ss);
								mes_consulta = mes_consulta + 1;
						

					}
				
				
					
				}
				//relleno de meses
				if (mes_sql < 12) {
					
					mes_sql = mes_sql+1 ;
					for(int i = mes_sql; i <= 12; i = i + 1) {
						Pres18POJO ss = new Pres18POJO();
						//System.out.println("  mes  sql " + mes_sql  + " <> mes consulta " +  mes_consulta );
						ss.setCoduni(org); // cod cuenta
						ss.setDesuni(""); // titulo cta
						ss.setPresum(0); // presupuesto
						ss.setUsadom(0); // gastos
						ss.setIddigi("N");
						ss.setPorc1(0); // porc1
						//System.out.println("Valor i-mes " + i);
						ss.setNummes(i);						
						ss.setSaldoMes(0); // saldo mes
						ss.setPresac(0); // pres acumulado
						ss.setUsadac(0); // gasto acumulado
						ss.setPorc2(0); // porc2
						ss.setTotal(0); // total
						lista.add(ss);
						
					} 
				} 
				
			
			
				
				
				res.close();
				sent.close();
				con.close();
			
		} catch (SQLException e) {
			System.out.println("Error sipB getCuentasDetalles: "+ e.getMessage());
		}
		return lista;
	}

	*/
	/*
	public Collection<Pres18POJO> getCuentasDetalles_RespaldoProduccion(String org, int a�oIN,
			int mes, String cuenta) {
		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;
		int sw = 0;
		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		//System.out.println("Va a llamar a verificaDetalle");
	
		String condicion = "";
		if (cuenta.equals("-1") || (cuenta.isEmpty()) )
			condicion = " AND FGBTRND_ORGN_CODE IN('"+org +"')" ;
		else
			condicion = " AND FGBTRND_ORGN_CODE IN('"+org +"') AND FGBTRND_ACCT_CODE = '"+ cuenta +"'" ;
		try {
		//	condicion = " AND FGBTRND_ORGN_CODE IN('OC5014')" ;
			String sql = ""
				+ "SELECT * FROM ( "
				+ "SELECT FGBTRND_ORGN_CODE,"
				+ "  FTVORGN_TITLE,"
				+ "  REPLACE(FGBTRND_POSTING_PERIOD,'00','01' ) FECHA,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Saldo_Inicial_A,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD11',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Presupuesto_B ,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD12',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Traspasos_Presupuesto_C,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD08',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Traspasos_Servicio_D,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0))+ SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) GASTO ,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)) SAC "
				+ "FROM FIMSMGR.FGBTRND,"
				+ "  FIMSMGR.FTVFUND,"
				+ "  FIMSMGR.FTVORGN,"
				+ "  FIMSMGR.FTVACCT,"
				+ "  FIMSMGR.FTVPROG "
				+ " WHERE FGBTRND_FUND_CODE      =FTVFUND_FUND_CODE"
				+ " AND FGBTRND_ORGN_CODE        =FTVORGN_ORGN_CODE"
				+ " AND FGBTRND_PROG_CODE        =FTVPROG_PROG_CODE"
				+ " AND FGBTRND_ACCT_CODE        =FTVACCT_ACCT_CODE"
				+ " AND FGBTRND_ACTIVITY_DATE   >=FTVFUND_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE    <FTVFUND_NCHG_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE   >=FTVORGN_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE    <FTVORGN_NCHG_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE   >=FTVPROG_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE    <FTVPROG_NCHG_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE   >=FTVACCT_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE    <FTVACCT_NCHG_DATE"
		
				+ " AND FTVFUND_FTYP_CODE       IN('CA','CD','CN','FI','FR')"
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ " FTVACCT_ATYP_CODE IN "
				+ " (SELECT FTVATYP_ATYP_CODE "
				+ "  FROM FTVATYP"
				+ "  WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ " AND FTVFUND_DATA_ENTRY_IND   ='Y'"
				+ " AND FTVORGN_DATA_ENTRY_IND   ='Y'"
				+ " AND FTVACCT_DATA_ENTRY_IND   ='Y'"
				+ " AND FTVPROG_DATA_ENTRY_IND   ='Y'"
				+ " AND FGBTRND.FGBTRND_LEDC_CODE='FINANC'"
				+ " AND FGBTRND_LEDGER_IND       ='O'"
				+ " AND FGBTRND_FSYR_CODE        ='"+ a�o +"'"
				+ condicion
				+ " AND  NOT (FGBTRND_RUCL_CODE IN 'BD01' AND FGBTRND_ACCT_CODE IN '6BF005')"
				+ " GROUP BY FGBTRND_ORGN_CODE,"
				+ "  FTVFUND_TITLE,"
				+ "  FTVORGN_TITLE,"
				+ "  REPLACE(FGBTRND_POSTING_PERIOD,'00','01'),"
				+ " 0"
				+ " UNION ALL "
				+ " SELECT 'INICIAL', "
				+ "  FTVORGN_TITLE, "
				+ "  FGBTRND_POSTING_PERIOD ,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Saldo_Inicial_A,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD11',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Presupuesto_B ,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD12',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Traspasos_Presupuesto_C,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD08',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Traspasos_Servicio_D,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0))+ SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) GASTO ,"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)) SAC "
				+ "FROM FIMSMGR.FGBTRND,"
				+ "  FIMSMGR.FTVFUND,"
				+ "  FIMSMGR.FTVORGN,"
				+ "  FIMSMGR.FTVACCT,"
				+ "  FIMSMGR.FTVPROG"
				+ " WHERE FGBTRND_FUND_CODE      =FTVFUND_FUND_CODE  "
				+ " AND FGBTRND_ORGN_CODE        =FTVORGN_ORGN_CODE "
				+ "AND FGBTRND_PROG_CODE        =FTVPROG_PROG_CODE "
				+ "AND FGBTRND_ACCT_CODE        =FTVACCT_ACCT_CODE "
				+ "AND FGBTRND_ACTIVITY_DATE   >=FTVFUND_EFF_DATE "
				+ "AND FGBTRND_ACTIVITY_DATE    <FTVFUND_NCHG_DATE "
				+ "AND FGBTRND_ACTIVITY_DATE   >=FTVORGN_EFF_DATE "
				+ "AND FGBTRND_ACTIVITY_DATE    <FTVORGN_NCHG_DATE  "
				+ "AND FGBTRND_ACTIVITY_DATE   >=FTVPROG_EFF_DATE "
				+ "AND FGBTRND_ACTIVITY_DATE    <FTVPROG_NCHG_DATE "
				+ "AND FGBTRND_ACTIVITY_DATE   >=FTVACCT_EFF_DATE "
				+ "AND FGBTRND_ACTIVITY_DATE    <FTVACCT_NCHG_DATE "
				
				+ "AND FTVFUND_FTYP_CODE       IN('CA','CD','CN','FI','FR') "
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ " FTVACCT_ATYP_CODE IN "
				+ " (SELECT FTVATYP_ATYP_CODE "
				+ "  FROM FTVATYP"
				+ "  WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ "AND FTVFUND_DATA_ENTRY_IND   ='Y' "
				+ "AND FTVORGN_DATA_ENTRY_IND   ='Y' "
				+ "AND FTVACCT_DATA_ENTRY_IND   ='Y' "
				+ "AND FTVPROG_DATA_ENTRY_IND   ='Y' "
				+ "AND FGBTRND.FGBTRND_LEDC_CODE='FINANC' "
				+ "AND FGBTRND_LEDGER_IND       ='O' "
				+ " AND FGBTRND_FSYR_CODE        ='"+ a�o +"'"
				+ condicion
				+ "AND (FGBTRND_RUCL_CODE = 'BD01' AND FGBTRND_ACCT_CODE = '6BF005') "
				+ " AND FGBTRND_FIELD_CODE IN ('01','02','03','04','05')"
				+ "GROUP BY 'INICIAL', "
				+ "  FTVFUND_TITLE, "
				+ "  FTVORGN_TITLE, "
				+ "  FGBTRND_POSTING_PERIOD, "
				+ "0 "
				+ ") DISPO ORDER BY  CASE WHEN FGBTRND_ORGN_CODE = 'INICIAL' THEN 'INICIAL' END ,FECHA ASC ";
				
				


		//	System.out.println("getCuentasDetalles " + sql);
		//	//System.out.println("cuenta " + cuenta);
			
				

				sent = con.conexion().prepareStatement(sql);
				// sent.setString(1,rutnumdv);
				res = sent.executeQuery();
				long acumulado = 0;
				long gastos = 0;
				int mes_consulta = 0;
				int mes_sql = 0;
				long presu_acum = 0;
				long gasto_acum = 0;
				int n=res.getRow();
				Boolean saldoinicial = false;
				//int mes_anterior = -1;
				int mes_guarda = 0 ;
				Pres18POJO mes_anterior = new Pres18POJO();
				long saldoini = 0 ;
				Boolean tieneinicial = false;
				long inicialregla = 0;
				while (res.next()) {
					
					sw = 0 ;
					
					mes_sql = res.getInt(3);
				//	System.out.println("Entra a while  mes   " + mes_sql + " mes consulta " + mes_consulta + " valor ws " + sw);
					while (mes_consulta < 13 && (sw == 0)) {
						Pres18POJO ss = new Pres18POJO();
					
					if(mes_sql == 0) mes_consulta = mes_sql;
					
						if (res.getString(1).equals("INICIAL") ) { //SALDO INICIAL
							saldoini = res.getLong(4);
							ss.setCoduni(res.getString(1)); // cod cuenta
							ss.setDesuni(res.getString(2)); // titulo cta
							ss.setPresum(0); // presupuesto
							ss.setUsadom(saldoini *-1); // gastos
							ss.setIddigi("N");
							ss.setPorc1(0); // porc1
							ss.setNummes(mes_consulta);
							//ss.setNummes(0);
							ss.setSaldoMes(saldoini); // saldo mes
							ss.setPresac(0); // pres acumulado
							ss.setUsadac(saldoini);
							ss.setPorc2(0); // porc2
							ss.setTotal((saldoini)); // total
							inicialregla = saldoini;
							mes_guarda = mes_sql;
							 lista.add(ss);
							sw = 1 ;
							 saldoinicial = true ;
							 tieneinicial = true;
						} else saldoinicial = false ;
						
					if ((mes_sql == mes_consulta)&& (saldoinicial == false)){ //&& (mes_sql > 0)) {
							 
							ss.setCoduni(res.getString(1)); // cod cuenta
							ss.setDesuni(res.getString(2)); // titulo cta
							ss.setPresum(res.getLong(5)); // presupuesto
							presu_acum = presu_acum + ss.getPresum();
							gastos = res.getLong(8);
							
								ss.setUsadom(gastos - res.getLong(7)
										- res.getLong(6) - res.getLong(9) - res.getLong(4) );//+ saldoini); // gastos
							
							
							if  (mes_sql != 0) {
							gasto_acum = gasto_acum + ss.getUsadom();
							mes_guarda = mes_sql;
							}
							else if (tieneinicial)
								mes_guarda =  1;
							else mes_guarda =  0;
							//else gasto_acum = ss.getUsadom() ; 
							//gasto_acum = gasto_acum + ss.getUsadom();
							if (gastos > 0) {
								ss.setIddigi("S");

							} else {
								ss.setIddigi("N");

							}
							ss.setNummes(mes_guarda);
							ss.setSaldoMes(ss.getPresum() - ss.getUsadom()); // saldo
																				// mes
							ss.setPresac(presu_acum); // pres acumulado
						//	System.out.println("Gasto acumulado " + gasto_acum);
							ss.setUsadac(gasto_acum);
							// gasto acumulado
							
							if(inicialregla != 0) {
								ss.setTotal((presu_acum - gasto_acum + inicialregla )); 
								inicialregla = 0;	
							} else
							ss.setTotal((presu_acum - gasto_acum)); // total
							
							// porcentaje ss.setPorc1(0); //
							
							if (ss.getPresum() != 0 && ss.getUsadom() != 0)
								ss.setPorc1((ss.getUsadom() / ss.getPresum())* 100);
							else
								ss.setPorc1(0);

							if (ss.getUsadac() != 0 && ss.getPresac() != 0)
								ss.setPorc2((ss.getUsadac() / ss.getPresac())* 100);
							else
								ss.setPorc2(0);
							
							sw = 1 ;
							
							
						} else if (saldoinicial == false) {
							
						 	ss.setCoduni(res.getString(1)); // cod cuenta
							ss.setDesuni(res.getString(2)); // titulo cta
							ss.setPresum(0); // presupuesto
							ss.setUsadom(0); // gastos
							ss.setIddigi("N");
							ss.setPorc1(0); // porc1
							ss.setNummes(mes_consulta);
							ss.setSaldoMes(0); // saldo mes
							ss.setPresac(presu_acum); // pres acumulado
							ss.setUsadac(gasto_acum);
							ss.setPorc2(0); // porc2
							ss.setTotal((presu_acum - gasto_acum)); // total
							
						}
						
					
						
					
					saldoinicial = false;
					Boolean guardalista = true;
						if ((tieneinicial == false)&&(mes_guarda == 0)) {
							 lista.add(ss);
							 guardalista = false;
						}
						if(mes_anterior.getNummes() != ss.getNummes() && saldoinicial == false && guardalista){
							 lista.add(ss);
						}else{
							mes_anterior.setPresum(mes_anterior.getPresum()+ss.getPresum());
							mes_anterior.setUsadom(mes_anterior.getUsadom()+ss.getUsadom());
							mes_anterior.setPresac(mes_anterior.getPresac()+ss.getPresac());
							mes_anterior.setUsadac(mes_anterior.getUsadac()+ss.getUsadac());
						}
							
							 mes_anterior = ss;			
							mes_consulta = mes_consulta + 1;

					}
				
				
					
				}
				//relleno de meses
			//	if (mes_sql < 12) {
				if ((mes_sql < 12)&& (mes_consulta <12)) {
					
					mes_sql = mes_sql+1 ;
					for(int i = mes_sql; i <= 12; i = i + 1) {
						Pres18POJO ss = new Pres18POJO();
						//System.out.println("  mes  sql " + mes_sql  + " <> mes consulta " +  mes_consulta );
						ss.setCoduni(org); // cod cuenta
						ss.setDesuni(""); // titulo cta
						ss.setPresum(0); // presupuesto
						ss.setUsadom(0); // gastos
						ss.setIddigi("N");
						ss.setPorc1(0); // porc1
						//System.out.println("Valor i-mes " + i);
						ss.setNummes(i);						
						ss.setSaldoMes(0); // saldo mes
						ss.setPresac(0); // pres acumulado
						ss.setUsadac(0); // gasto acumulado
						ss.setPorc2(0); // porc2
						ss.setTotal(0); // total
						lista.add(ss);
						
					} 
				} 
				
			
			
				
				
				res.close();
				sent.close();
				con.close();
			
		} catch (SQLException e) {
			System.out.println("Error sipB getCuentasDetalles: "+ e.getMessage());
		}
		return lista;
	}

	*/
	public Collection<Pres18POJO> getCuentasDetalles(String org, int a�oIN,
			int mes, String cuenta,String cuentas_excepciones, Boolean reitemizacion) {
		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;
		int sw = 0;
		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		//System.out.println("Va a llamar a verificaDetalle");
		/*
		 * try { System.out.println("Va a llamar a verificaDetalle"); sw =
		 * verificaDetalle(org,a�oIN,mes); System.out.println("Resultado Va a
		 * llamar a verificaDetalle " + sw); }catch (Exception ex){
		 *  }
		 */
		String condicion = "";
		String condicion_mes = "";
		if (cuenta.equals("-1") || (cuenta.isEmpty()) )
			condicion = " AND FGBTRND_ORGN_CODE IN('"+org +"')" ;
		else
			condicion = " AND FGBTRND_ORGN_CODE IN('"+org +"') AND FGBTRND_ACCT_CODE = '"+ cuenta +"'" ;
		
		if (reitemizacion)
			condicion_mes = " AND fgbtrnd_posting_period BETWEEN "+ mes +" and 12 ";
		else
			condicion_mes = " -- AND fgbtrnd_posting_period BETWEEN "+mes +" and 12 ";
		try {
		//	condicion = " AND FGBTRND_ORGN_CODE IN('OC5014')" ;
		
			String sql = ""
				+ " 	SELECT"
				+ "     *"
				+ " FROM"
				+ "     ("
				+ "         SELECT CASE"
				+ "            WHEN fgbtrnd_rucl_code = 'BD01' AND fgbtrnd_acct_code = '6BF005'"
				+ "                 THEN 'INICIAL'"
				+ "             ELSE"
				+ "                 fgbtrnd_orgn_code"
				+ "             END fgbtrnd_orgn_code,"
				+ "             ftvorgn_title,"
				+ "             DECODE(fgbtrnd_posting_period,'00','01',fgbtrnd_posting_period) fecha,"
				+ "             SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD01',fgbtrnd.fgbtrnd_trans_amt,0),0) ) saldo_inicial_a,"
				+ "             SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD05',fgbtrnd.fgbtrnd_trans_amt,0),0) ) + SUM(DECODE(fgbtrnd_field_code,2,"
				+ " DECODE(fgbtrnd_rucl_code,'BD07',fgbtrnd.fgbtrnd_trans_amt,0),0) ) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD06',"
				+ " fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0))  presupuesto_b,"
				+ "             SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD02',fgbtrnd.fgbtrnd_trans_amt,0),0) ) traspasos_presupuesto_c,"
				+ " SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0)) traspasos_servicio_d,"
				+ "             ( SUM(DECODE(fgbtrnd_field_code,3,fgbtrnd.fgbtrnd_trans_amt,0) ) + SUM(DECODE(fgbtrnd_field_code,4,fgbtrnd.fgbtrnd_trans_amt,5,fgbtrnd"
				+ " .fgbtrnd_trans_amt,0)) - SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0)) )  gasto,"
				+ "             SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD09',fgbtrnd.fgbtrnd_trans_amt,0),0) ) sac"
				+ "         FROM"
				+ "             fimsmgr.fgbtrnd,"
				+ "             fimsmgr.ftvfund,"
				+ "             fimsmgr.ftvorgn,"
				+ "             fimsmgr.ftvacct,"
				+ "             fimsmgr.ftvprog"
				+ "         WHERE"
				+ "             fgbtrnd_fund_code = ftvfund_fund_code"
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+ "             AND fgbtrnd_orgn_code = ftvorgn_orgn_code"
				+ "             AND fgbtrnd_prog_code = ftvprog_prog_code"
				+ "             AND fgbtrnd_acct_code = ftvacct_acct_code"
				+ "             AND fgbtrnd_activity_date >= ftvfund_eff_date"
				+ "             AND fgbtrnd_activity_date < ftvfund_nchg_date"
				+ "             AND fgbtrnd_activity_date >= ftvorgn_eff_date"
				+ "             AND fgbtrnd_activity_date < ftvorgn_nchg_date"
				+ "             AND fgbtrnd_activity_date >= ftvprog_eff_date"
				+ "             AND fgbtrnd_activity_date < ftvprog_nchg_date"
				+ "             AND fgbtrnd_activity_date >= ftvacct_eff_date"
				+ "             AND fgbtrnd_activity_date < ftvacct_nchg_date"
				+ "             AND ftvfund_ftyp_code IN ('CA','CD','CN','FI','FR')"
				+ "             AND NOT (fgbtrnd_rucl_code IN ('APS1','APS2','APS3','APS4','CHS1','CSS1','JE15')"
				+ "                 AND ftvacct_atyp_code IN ("
				+ "                     SELECT ftvatyp_atyp_code"
				+ "                     FROM ftvatyp"
				+ "                     WHERE ftvatyp_internal_atyp_code = '50'"
				+ "                 )"
				+ "             )"
				+ "             AND ftvfund_data_entry_ind = 'Y'"
				+ "             AND ftvorgn_data_entry_ind = 'Y'"
				+ "             AND ftvacct_data_entry_ind = 'Y'"
				+ "             AND ftvprog_data_entry_ind = 'Y'"
				+ "             AND fgbtrnd.fgbtrnd_ledc_code = 'FINANC'"
				+ "             AND fgbtrnd_ledger_ind = 'O'"
				+ "				AND FGBTRND_FSYR_CODE        ='"+ a�o +"'"
				+ condicion_mes
				+ " AND FGBTRND_ACCT_CODE NOT IN ( " + cuentas_excepciones + ") "
				+ 	condicion
				+ "         GROUP BY"
				+ "              CASE"
				+ "             WHEN fgbtrnd_rucl_code = 'BD01' AND fgbtrnd_acct_code = '6BF005'"
				+ "                 THEN 'INICIAL'"
				+ "             ELSE"
				+ "                 fgbtrnd_orgn_code"
				+ "             END,"
				+ "             ftvorgn_title,"
				+ "             DECODE(fgbtrnd_posting_period,'00','01',fgbtrnd_posting_period)"
				+ "             ) dispo"
				+ " ORDER BY"
				+ "     CASE"
				+ "         WHEN fgbtrnd_orgn_code = 'INICIAL' THEN 'INICIAL'"
				+ "     END,"
				+ "     fecha ASC";
				
				

		//	System.out.println("cuenta " + sql);
			
				

				sent = con.conexion().prepareStatement(sql);
				/*	sent.setString(1,"BD01");
				sent.setString(2,"6BF005");
					sent.setString(3,"BD01");
				sent.setString(4,"BD05");
				sent.setString(5,"BD07");
				sent.setString(6,"BD06");
				sent.setString(7,"BD02");
				sent.setString(8,"BD08");
				sent.setString(9,"BD09");
				sent.setString(10,"CA");
				sent.setString(11,"CD");
				sent.setString(12,"CN");
				sent.setString(13,"FI");
				sent.setString(14,"FR");
				sent.setString(15,"APS1");
				sent.setString(16,"APS2");
				sent.setString(17,"APS3");
				sent.setString(18,"APS4");
				sent.setString(19,"CHS1");
				sent.setString(20,"CSS1");
				sent.setString(21,"JE15");
				sent.setString(22,"50");
				sent.setString(23,"Y");
				sent.setString(24,"Y");
				sent.setString(25,"Y");
				sent.setString(26,"Y");
				sent.setString(27,"FINANC");
				sent.setString(28,"O");
				sent.setString(29,"18");
				sent.setString(30,"BD01");
				sent.setString(31,"6BF005");
				sent.setString(32,"INICIAL");
				sent.setString(33,"INICIAL");*/

				
				res = sent.executeQuery();
				long acumulado = 0;
				long gastos = 0;
				int mes_consulta = 0;
				int mes_sql = 0;
				long presu_acum = 0;
				long gasto_acum = 0;
				int n=res.getRow();
				Boolean saldoinicial = false;
				//int mes_anterior = -1;
				int mes_guarda = 0 ;
				Pres18POJO mes_anterior = new Pres18POJO();
				long saldoini = 0 ;
				Boolean tieneinicial = false;
				Boolean tieneUnsoloRegistro = false;
				long inicialregla = 0;
				while (res.next()) {
					
					sw = 0 ;
					
					mes_sql = res.getInt(3);
				//	System.out.println("Entra a while  mes   " + mes_sql + " mes consulta " + mes_consulta + " valor ws " + sw);
					while (mes_consulta < 13 && (sw == 0)) {
						Pres18POJO ss = new Pres18POJO();
					
						/*System.out.println(res.getString(1));
						System.out.println(mes_consulta);
						System.out.println(mes_sql);*/
					if(mes_sql == 0) mes_consulta = mes_sql;
					
						if (res.getString(1).equals("INICIAL") ) { //SALDO INICIAL
							tieneUnsoloRegistro = true;
							saldoini = res.getLong(4);
							ss.setCoduni(res.getString(1)); // cod cuenta
							ss.setDesuni(res.getString(2)); // titulo cta
							ss.setPresum(0); // presupuesto
							ss.setUsadom(saldoini *-1); // gastos
							ss.setIddigi("N");
							ss.setPorc1(0); // porc1
							ss.setNummes(mes_consulta);
							//ss.setNummes(0);
							ss.setSaldoMes(saldoini); // saldo mes
							ss.setPresac(0); // pres acumulado
							ss.setUsadac(saldoini);
							ss.setPorc2(0); // porc2
							ss.setTotal((saldoini)); // total
							inicialregla = saldoini;
							mes_guarda = mes_sql;
							 lista.add(ss);
							sw = 1 ;
							 saldoinicial = true ;
							 tieneinicial = true;
						} else {
							saldoinicial = false ;
							tieneUnsoloRegistro = false;
						}
							
						
					if ((mes_sql == mes_consulta)&& (saldoinicial == false)){ //&& (mes_sql > 0)) {
							 
							ss.setCoduni(res.getString(1)); // cod cuenta
							ss.setDesuni(res.getString(2)); // titulo cta
							ss.setPresum(res.getLong(5)); // presupuesto
							presu_acum = presu_acum + ss.getPresum();
							gastos = res.getLong(8);
							
								ss.setUsadom(gastos - res.getLong(7)
										- res.getLong(6) - res.getLong(9) - res.getLong(4) );//+ saldoini); // gastos
							
							
							if  (mes_sql != 0) {
							gasto_acum = gasto_acum + ss.getUsadom();
							mes_guarda = mes_sql;
							}
							else if (tieneinicial)
								mes_guarda =  1;
							else mes_guarda =  0;
							//else gasto_acum = ss.getUsadom() ; 
							//gasto_acum = gasto_acum + ss.getUsadom();
							if (gastos > 0) {
								ss.setIddigi("S");

							} else {
								ss.setIddigi("N");

							}
							ss.setNummes(mes_guarda);
							ss.setSaldoMes(ss.getPresum() - ss.getUsadom()); // saldo
																				// mes
							ss.setPresac(presu_acum); // pres acumulado
						//	System.out.println("Gasto acumulado " + gasto_acum);
							ss.setUsadac(gasto_acum);
							// gasto acumulado
							
							if(inicialregla != 0) {
								ss.setTotal((presu_acum - gasto_acum + inicialregla )); 
								inicialregla = 0;	
							} else
							ss.setTotal((presu_acum - gasto_acum)); // total
							
							// porcentaje ss.setPorc1(0); //
							
							if (ss.getPresum() != 0 && ss.getUsadom() != 0)
								ss.setPorc1((ss.getUsadom() / ss.getPresum())* 100);
							else
								ss.setPorc1(0);

							if (ss.getUsadac() != 0 && ss.getPresac() != 0)
								ss.setPorc2((ss.getUsadac() / ss.getPresac())* 100);
							else
								ss.setPorc2(0);
							
							sw = 1 ;
							
							
						} else if (saldoinicial == false) {
							
						 	ss.setCoduni(res.getString(1)); // cod cuenta
							ss.setDesuni(res.getString(2)); // titulo cta
							ss.setPresum(0); // presupuesto
							ss.setUsadom(0); // gastos
							ss.setIddigi("N");
							ss.setPorc1(0); // porc1
							ss.setNummes(mes_consulta);
							ss.setSaldoMes(0); // saldo mes
							ss.setPresac(presu_acum); // pres acumulado
							ss.setUsadac(gasto_acum);
							ss.setPorc2(0); // porc2
							ss.setTotal((presu_acum - gasto_acum)); // total
							
						}
						
					
						
					
					saldoinicial = false;
					Boolean guardalista = true;
						if ((tieneinicial == false)&&(mes_guarda == 0)) {
							 lista.add(ss);
							 guardalista = false;
						}
						if(mes_anterior.getNummes() != ss.getNummes() && saldoinicial == false && guardalista){
							 lista.add(ss);
						}else{
							mes_anterior.setPresum(mes_anterior.getPresum()+ss.getPresum());
							mes_anterior.setUsadom(mes_anterior.getUsadom()+ss.getUsadom());
							mes_anterior.setPresac(mes_anterior.getPresac()+ss.getPresac());
							mes_anterior.setUsadac(mes_anterior.getUsadac()+ss.getUsadac());
						}
							
							 mes_anterior = ss;			
							mes_consulta = mes_consulta + 1;

					}
				
				
					
				}
				//relleno de meses
				
			//	if (mes_sql < 12) {
				if ((mes_sql < 12)&& (mes_consulta <12)) {
					if (!tieneUnsoloRegistro)
					mes_sql = mes_sql+1 ;
					for(int i = mes_sql; i <= 12; i = i + 1) {
						Pres18POJO ss = new Pres18POJO();
						//System.out.println("  mes  sql " + mes_sql  + " <> mes consulta " +  mes_consulta );
						ss.setCoduni(org); // cod cuenta
						ss.setDesuni(""); // titulo cta
						ss.setPresum(0); // presupuesto
						ss.setUsadom(0); // gastos
						ss.setIddigi("N");
						ss.setPorc1(0); // porc1
						//System.out.println("Valor i-mes " + i);
						ss.setNummes(i);						
						ss.setSaldoMes(0); // saldo mes
						ss.setPresac(0); // pres acumulado
						ss.setUsadac(0); // gasto acumulado
						ss.setPorc2(0); // porc2
						ss.setTotal(0); // total
						lista.add(ss);
						
					} 
				} 
				
			
			
				
				
				res.close();
				sent.close();
				con.close();
			
		} catch (SQLException e) {
			System.out.println("Error sipB getCuentasDetalles: "+ e.getMessage());
		}
		return lista;
	}

	/*
	
	public Collection<Presw25POJO> getDetalleAnual_RESPALDO_18_12_17(int a�oIN, String org) {
		//System.out.println("Entra a getDetalleAnual valor mes ");
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Presw25POJO> lista = new ArrayList<Presw25POJO>();
		
		Calendar fecha = Calendar.getInstance();
		String a�osql;
		if (a�oIN == 0) {
			int a�o = fecha.get(Calendar.YEAR);
			a�osql = String.valueOf(a�o);
			a�osql = a�osql.substring(2);
		} else {
			int a�o = a�oIN;
			a�osql = String.valueOf(a�o);
			a�osql = a�osql.substring(2);
			a�o = 0;
			a�o = Integer.parseInt(a�osql);
		}
		try {
			String sql = ""
				+ " SELECT  FGBTRND_ACCT_CODE, FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,"
				+ " FGBTRND_POSTING_PERIOD,"
				+ " ("
				+ " SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +"
				+ " SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD08',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +"
				+ " SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +"
				+ " SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0)) +"
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0))        "
				+ " ) Gasto ,"
				+ " ("
				+ "  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +"
				+ " SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0))"
				+ "   "
				+ " ) Presupuesto"
				+ " FROM FIMSMGR.FGBTRND,"
				+ " FIMSMGR.FTVFUND,"
				+ " FIMSMGR.FTVORGN,"
				+ " FIMSMGR.FTVACCT,"
				+ " FIMSMGR.FTVPROG     "
				+ " WHERE"
				+ "  FGBTRND_FUND_CODE = FTVFUND_FUND_CODE"
				+ " AND FGBTRND_ORGN_CODE = FTVORGN_ORGN_CODE"
				+ " AND FGBTRND_PROG_CODE = FTVPROG_PROG_CODE"
				+ " AND FGBTRND_ACCT_CODE = FTVACCT_ACCT_CODE  "
				+ " AND FGBTRND_ACTIVITY_DATE >= FTVFUND_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE < FTVFUND_NCHG_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE >= FTVORGN_EFF_DATE  "
				+ " AND FGBTRND_ACTIVITY_DATE < FTVORGN_NCHG_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE >= FTVPROG_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE < FTVPROG_NCHG_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE >= FTVACCT_EFF_DATE"
				+ " AND FGBTRND_ACTIVITY_DATE < FTVACCT_NCHG_DATE "
				+ " AND FTVFUND_FTYP_CODE IN ('CA','CD','CN','FI','FR') "
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+ " AND FTVFUND_DATA_ENTRY_IND = 'Y'"
				+ " AND FTVORGN_DATA_ENTRY_IND = 'Y'"
				+ " AND FTVACCT_DATA_ENTRY_IND = 'Y'"
				+ " AND FTVPROG_DATA_ENTRY_IND = 'Y'"
				+ " AND FGBTRND.FGBTRND_LEDC_CODE = 'FINANC'"
				+ " AND FGBTRND_LEDGER_IND = 'O'"
				+ " AND FGBTRND_FSYR_CODE = '"+ a�osql +"'"
				+ " AND FGBTRND_ORGN_CODE = '"+ org+"'"
				+ " GROUP BY FGBTRND_ACCT_CODE,  FTVACCT.FTVACCT_TITLE  ,FGBTRND_POSTING_PERIOD   "
				+ " ORDER BY FGBTRND_POSTING_PERIOD " ;
			//System.out.println("Query getDetalleAnual Detalle Anual de Gastos sql " + sql);

			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			long acumulado = 0;
			long gastos = 0;
			int periodo = 0 ;
			while (res.next()) {
				Presw25POJO ss = new Presw25POJO();
				
				String nomMes = "";
				int mes = res.getInt(2);
				switch (mes) {
				case 0:
					nomMes = "Saldo inicial";					
					break;
				case 1:
					nomMes = "Enero";
					//ss.setGasto1(res.getLong(3) + ss.getGasto1());
					break;
				case 2:
					nomMes = "Febrero";
					break;
				case 3:
					nomMes = "Marzo";
					break;
				case 4:
					nomMes = "Abril";
					break;
				case 5:
					nomMes = "Mayo";
					break;
				case 6:
					nomMes = "Junio";
					break;
				case 7:
					nomMes = "Julio";
					break;
				case 8:
					nomMes = "Agosto";
					break;
				case 9:
					nomMes = "Septiembre";
					break;
				case 10:
					nomMes = "Octubre";
					break;
				case 11:
					nomMes = "Noviembre";
					break;
				case 12:
					nomMes = "Diciembre";
					break;
				}
				ss.setMes(nomMes);
				ss.setCoduni(res.getString(1));
				if (mes == 0){					
					ss.setGasto(res.getLong(3));
				}else if (mes == 1){					
					ss.setGasto1(res.getLong(3));
				}else if (mes == 2){					
					ss.setGasto2(res.getLong(3));
				}else if (mes == 3){					
					ss.setGasto3(res.getLong(3));
				}else if (mes == 4){					
					ss.setGasto4(res.getLong(3));
				}else if (mes == 5){					
					ss.setGasto5(res.getLong(3));
				}else if (mes == 6){					
					ss.setGasto6(res.getLong(3));
				}else if (mes == 07){					
					ss.setGasto7(res.getLong(3));
				}else if (mes == 8){					
					ss.setGasto8(res.getLong(3));
				}else if (mes == 9){					
					ss.setGasto9(res.getLong(3));
				}else if (mes == 10){					
					ss.setGasto10(res.getLong(3));
				}else if (mes == 11){					
					ss.setGasto11(res.getLong(3));
				}else if (mes == 12){					
					ss.setGasto12(res.getLong(3));
				}else if  ((mes > periodo) && (periodo == 0)){					
					ss.setGasto(res.getLong(3));
				} else periodo = periodo + 1 ;
				
				
				
				// ss.setPresac(acumulado);/*
				// ss.setUsadac(gastos);
				lista.add(ss);
			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error getDetalleAnual Detalle Anual de Gastos: "+ e.getMessage());
		}
		return lista;
	}

*/
	public Collection<Presw25POJO> getDetalleAnual(int a�oIN, String org) {
		//System.out.println("Entra a getDetalleAnual valor mes ");
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<Presw25POJO> lista = new ArrayList<Presw25POJO>();
		
		Calendar fecha = Calendar.getInstance();
		String a�osql;
		if (a�oIN == 0) {
			int a�o = fecha.get(Calendar.YEAR);
			a�osql = String.valueOf(a�o);
			a�osql = a�osql.substring(2);
		} else {
			int a�o = a�oIN;
			a�osql = String.valueOf(a�o);
			a�osql = a�osql.substring(2);
			a�o = 0;
			a�o = Integer.parseInt(a�osql);
		}
		long saldo_inicial = 0;
		try {
			String sql = ""
				+"    SELECT * FROM (   "
				+"    select '5ACADOC' CUENTA, 'GASTO REM ACAD�MICOS' DESCRIPCION_CUENTA, MES,SUM (GASTO),SUM(PRESUPUESTO)   "
				+"    from (   "
				+"    SELECT FGBTRND_ACCT_CODE CUENTA,   "
				+"      FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,   "
				+"      FGBTRND_POSTING_PERIOD MES,   "
				+"      ( SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) +   "
				//+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD08',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) +    "
				+ " (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))  *-1 )+"
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) +    "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) * -1)  +  "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0)) +    "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) ) Gasto,   "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +    "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) +  "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Presupuesto   "
				+"    FROM FIMSMGR.FGBTRND,   "
				+"      FIMSMGR.FTVFUND,   "
				+"      FIMSMGR.FTVORGN,   "
				+"      FIMSMGR.FTVACCT,   "
				+"      FIMSMGR.FTVPROG   "
				+"    WHERE FGBTRND_FUND_CODE       = FTVFUND_FUND_CODE   "
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+"    AND FGBTRND_ORGN_CODE         = FTVORGN_ORGN_CODE   "
				+"    AND FGBTRND_PROG_CODE         = FTVPROG_PROG_CODE   "
				+"    AND FGBTRND_ACCT_CODE         = FTVACCT_ACCT_CODE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVFUND_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVFUND_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVORGN_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVORGN_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVPROG_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVPROG_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVACCT_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVACCT_NCHG_DATE   "
				+"    AND FTVFUND_FTYP_CODE        IN ('CA','CD','CN','FI','FR')   "
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+"    AND FTVFUND_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVORGN_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVACCT_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVPROG_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FGBTRND.FGBTRND_LEDC_CODE = 'FINANC'   "
				+"    AND FGBTRND_LEDGER_IND        = 'O'   "
				+ " AND FGBTRND_FSYR_CODE = '"+ a�osql +"'"
				+ " AND FGBTRND_ORGN_CODE = '"+ org+"' group by FGBTRND_ACCT_CODE, FTVACCT.FTVACCT_TITLE, FGBTRND_POSTING_PERIOD ) SIIF   "
				+"      WHERE CUENTA IN ('5A0001','5A0002','5AA005','5AA006','5A0007','5A0008','5A0009','5A0010','5A0011','5A0012','5AA001','5AA002','5AA008','5AA009')      "
				+"    GROUP BY '5ACADOC','GASTO',MES   "
				+"    UNION ALL   "
				+"    select '5APODOC' CUENTA, 'GASTO REM APOYOS ACA Y DOC' DESCRIPCION_CUENTA, MES,SUM (GASTO),SUM(PRESUPUESTO)   "
				+"    from (   "
				+"    SELECT FGBTRND_ACCT_CODE CUENTA,   "
				+"      FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,   "
				+"      FGBTRND_POSTING_PERIOD MES,   "
				+"      ( SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) +   "
				+ " (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))  *-1 )+"
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) +    "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) * -1)  +  "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0)) +    "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) ) Gasto,   "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +   "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) +  "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Presupuesto   "
				+"    FROM FIMSMGR.FGBTRND,   "
				+"      FIMSMGR.FTVFUND,   "
				+"      FIMSMGR.FTVORGN,   "
				+"      FIMSMGR.FTVACCT,   "
				+"      FIMSMGR.FTVPROG   "
				+"    WHERE FGBTRND_FUND_CODE       = FTVFUND_FUND_CODE   "
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+"    AND FGBTRND_ORGN_CODE         = FTVORGN_ORGN_CODE   "
				+"    AND FGBTRND_PROG_CODE         = FTVPROG_PROG_CODE   "
				+"    AND FGBTRND_ACCT_CODE         = FTVACCT_ACCT_CODE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVFUND_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVFUND_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVORGN_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVORGN_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVPROG_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVPROG_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVACCT_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVACCT_NCHG_DATE   "
				+"    AND FTVFUND_FTYP_CODE        IN ('CA','CD','CN','FI','FR')   "
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+"    AND FTVFUND_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVORGN_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVACCT_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVPROG_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FGBTRND.FGBTRND_LEDC_CODE = 'FINANC'   "
				+"    AND FGBTRND_LEDGER_IND        = 'O'   "
				+ " AND FGBTRND_FSYR_CODE = '"+ a�osql +"'"
				+ " AND FGBTRND_ORGN_CODE = '"+ org+"' group by FGBTRND_ACCT_CODE, FTVACCT.FTVACCT_TITLE, FGBTRND_POSTING_PERIOD ) SIIF   "
				+"      WHERE CUENTA IN ('5A0003','5AA004','5AA011')   "
				+"    GROUP BY '5APODOC','GASTO',MES   "
				+"    UNION ALL  "
				+"    select '5AREMVR' CUENTA, 'GASTO REM HON, PT Y AYU' DESCRIPCION_CUENTA, MES,SUM (GASTO),SUM(PRESUPUESTO)   "
				+"    from (   "
				+"    SELECT FGBTRND_ACCT_CODE CUENTA,   "
				+"      FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,   "
				+"      FGBTRND_POSTING_PERIOD MES,   "
				+"      ( SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) +    "
				+ " (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))  *-1 )+"
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) +    "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) * -1)  +  "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0)) +    "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) ) Gasto,   "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +    "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) +  "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Presupuesto   "
				+"    FROM FIMSMGR.FGBTRND,   "
				+"      FIMSMGR.FTVFUND,   "
				+"      FIMSMGR.FTVORGN,   "
				+"      FIMSMGR.FTVACCT,   "
				+"      FIMSMGR.FTVPROG   "
				+"    WHERE FGBTRND_FUND_CODE       = FTVFUND_FUND_CODE   "
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+"    AND FGBTRND_ORGN_CODE         = FTVORGN_ORGN_CODE   "
				+"    AND FGBTRND_PROG_CODE         = FTVPROG_PROG_CODE   "
				+"    AND FGBTRND_ACCT_CODE         = FTVACCT_ACCT_CODE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVFUND_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVFUND_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVORGN_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVORGN_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVPROG_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVPROG_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVACCT_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVACCT_NCHG_DATE   "
				+"    AND FTVFUND_FTYP_CODE        IN ('CA','CD','CN','FI','FR')   "
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+"    AND FTVFUND_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVORGN_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVACCT_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVPROG_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FGBTRND.FGBTRND_LEDC_CODE = 'FINANC'   "
				+"    AND FGBTRND_LEDGER_IND        = 'O'   "
				+ " AND FGBTRND_FSYR_CODE = '"+ a�osql +"'"
				+ " AND FGBTRND_ORGN_CODE = '"+ org+"' group by FGBTRND_ACCT_CODE, FTVACCT.FTVACCT_TITLE, FGBTRND_POSTING_PERIOD ) SIIF   "
				+"      WHERE CUENTA IN ('5A0004','5A0005','5A0006','5AA003','5AA007','5AA010')   "
				+"    GROUP BY '5AREMVR','GASTO',MES   "
				+"    UNION ALL  "
				+"    select '5BADMIN' CUENTA, 'GASTO REM NOMINA ADMINISTRATIVOS' DESCRIPCION_CUENTA, MES,SUM (GASTO),SUM(PRESUPUESTO)   "
				+"    from (   "
				+"    SELECT FGBTRND_ACCT_CODE CUENTA,   "
				+"      FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,   "
				+"      FGBTRND_POSTING_PERIOD MES,   "
				+"      ( SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) +    "
				//+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD08',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) +    "
				+ " (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))  *-1 )+"
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) +    "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) * -1)  +  "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0)) +    "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) ) Gasto,   "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +    "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) +  "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Presupuesto   "
				+"    FROM FIMSMGR.FGBTRND,   "
				+"      FIMSMGR.FTVFUND,   "
				+"      FIMSMGR.FTVORGN,   "
				+"      FIMSMGR.FTVACCT,   "
				+"      FIMSMGR.FTVPROG   "
				+"    WHERE FGBTRND_FUND_CODE       = FTVFUND_FUND_CODE   "
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+"    AND FGBTRND_ORGN_CODE         = FTVORGN_ORGN_CODE   "
				+"    AND FGBTRND_PROG_CODE         = FTVPROG_PROG_CODE   "
				+"    AND FGBTRND_ACCT_CODE         = FTVACCT_ACCT_CODE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVFUND_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVFUND_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVORGN_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVORGN_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVPROG_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVPROG_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVACCT_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVACCT_NCHG_DATE   "
				+"    AND FTVFUND_FTYP_CODE        IN ('CA','CD','CN','FI','FR')   "
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+"    AND FTVFUND_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVORGN_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVACCT_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVPROG_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FGBTRND.FGBTRND_LEDC_CODE = 'FINANC'   "
				+"    AND FGBTRND_LEDGER_IND        = 'O'   "
				+ " AND FGBTRND_FSYR_CODE = '"+ a�osql +"'"
				+ " AND FGBTRND_ORGN_CODE = '"+ org+"' group by FGBTRND_ACCT_CODE, FTVACCT.FTVACCT_TITLE, FGBTRND_POSTING_PERIOD ) SIIF   "
				+"      WHERE CUENTA IN ('5BB001','5BB002','5BB003','5B0001','5B0004','5B0005','5B0006','5B0007','5B0008','5B0009')   "
				+"    GROUP BY '5BADMIN','GASTO',MES   "
				+"    UNION ALL   "
				+"    select '5BREMVR' CUENTA, 'GASTO REM VAR ADMINISTRATIVOS' DESCRIPCION_CUENTA, MES,SUM (GASTO),SUM(PRESUPUESTO)   "
				+"    from (   "
				+"    SELECT FGBTRND_ACCT_CODE CUENTA,   "
				+"      FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,   "
				+"      FGBTRND_POSTING_PERIOD MES,   "
				+"      ( SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) +   "
				//+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD08',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) +   "
				+ " (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))  *-1 )+"
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) +    "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) * -1)  +  "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0)) +   "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) ) Gasto,   "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +    "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) +  "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Presupuesto   "
				+"    FROM FIMSMGR.FGBTRND,   "
				+"      FIMSMGR.FTVFUND,   "
				+"      FIMSMGR.FTVORGN,   "
				+"      FIMSMGR.FTVACCT,   "
				+"      FIMSMGR.FTVPROG   "
				+"    WHERE FGBTRND_FUND_CODE       = FTVFUND_FUND_CODE   "
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+"    AND FGBTRND_ORGN_CODE         = FTVORGN_ORGN_CODE   "
				+"    AND FGBTRND_PROG_CODE         = FTVPROG_PROG_CODE   "
				+"    AND FGBTRND_ACCT_CODE         = FTVACCT_ACCT_CODE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVFUND_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVFUND_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVORGN_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVORGN_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVPROG_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVPROG_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVACCT_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVACCT_NCHG_DATE   "
				+"    AND FTVFUND_FTYP_CODE        IN ('CA','CD','CN','FI','FR')   "
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+"    AND FTVFUND_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVORGN_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVACCT_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVPROG_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FGBTRND.FGBTRND_LEDC_CODE = 'FINANC'   "
				+"    AND FGBTRND_LEDGER_IND        = 'O'   "
				+ " AND FGBTRND_FSYR_CODE = '"+ a�osql +"'"
				+ " AND FGBTRND_ORGN_CODE = '"+ org+"' group by FGBTRND_ACCT_CODE, FTVACCT.FTVACCT_TITLE, FGBTRND_POSTING_PERIOD ) SIIF   "
				+"      WHERE CUENTA IN ('5B0002','5B0003')   "
				+"    GROUP BY '5BREMVR','GASTO',MES   "
				+"      UNION ALL   "
				+"      SELECT CUENTA, DESCRIPCION_CUENTA, MES, GASTO,PRESUPUESTO FROM (   "
				+"      SELECT FGBTRND_ACCT_CODE CUENTA,   "
				+"      FTVACCT.FTVACCT_TITLE DESCRIPCION_CUENTA,   "
				+"      FGBTRND_POSTING_PERIOD MES,   "
				+"      ( SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) +    "
				//+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD08',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) +    "
				+ " (SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD08',fgbtrnd.fgbtrnd_trans_amt,0),0)) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD13',fgbtrnd.fgbtrnd_trans_amt,0),0))  *-1 )+"
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD09',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) +    "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)* -1) + SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,'BD12',fgbtrnd.fgbtrnd_trans_amt,0),0) * -1)  +  "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0)) +    "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) ) Gasto,   "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +   "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD07',FGBTRND.FGBTRND_TRANS_AMT,0),0)) + SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,'BD11',fgbtrnd.fgbtrnd_trans_amt,0),0) ) +  "
				+"      SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD06',FGBTRND.FGBTRND_TRANS_AMT,0),0)) Presupuesto   "
				+"    FROM FIMSMGR.FGBTRND,   "
				+"      FIMSMGR.FTVFUND,   "
				+"      FIMSMGR.FTVORGN,   "
				+"      FIMSMGR.FTVACCT,   "
				+"      FIMSMGR.FTVPROG   "
				+"    WHERE FGBTRND_FUND_CODE       = FTVFUND_FUND_CODE   "
				+ " AND FGBTRND_ACCT_CODE <> '4BD007'"
				+"    AND FGBTRND_ORGN_CODE         = FTVORGN_ORGN_CODE   "
				+"    AND FGBTRND_PROG_CODE         = FTVPROG_PROG_CODE   "
				+"    AND FGBTRND_ACCT_CODE         = FTVACCT_ACCT_CODE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVFUND_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVFUND_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVORGN_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVORGN_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVPROG_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVPROG_NCHG_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE    >= FTVACCT_EFF_DATE   "
				+"    AND FGBTRND_ACTIVITY_DATE     < FTVACCT_NCHG_DATE   "
				+"    AND FTVFUND_FTYP_CODE        IN ('CA','CD','CN','FI','FR')   "
				+ " AND NOT ( FGBTRND_RUCL_CODE IN ('APS1', 'APS2', 'APS3', 'APS4', 'CHS1', 'CSS1', 'JE15','CR05') AND "
				+ "       FTVACCT_ATYP_CODE IN "
				+ "         (SELECT FTVATYP_ATYP_CODE "
				+ "          FROM FTVATYP"
				+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = '50') )"
				+"    AND FTVFUND_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVORGN_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVACCT_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FTVPROG_DATA_ENTRY_IND    = 'Y'   "
				+"    AND FGBTRND.FGBTRND_LEDC_CODE = 'FINANC'   "
				+"    AND FGBTRND_LEDGER_IND        = 'O'   "
				+ " AND FGBTRND_FSYR_CODE = '"+ a�osql +"'"
				+ " AND FGBTRND_ORGN_CODE = '"+ org+"'   "
				+"    GROUP BY FGBTRND_ACCT_CODE,   "
				+"      FTVACCT.FTVACCT_TITLE ,   "
				+"      FGBTRND_POSTING_PERIOD   "
				+"    ) NO_REM    "
				+"    WHERE CUENTA NOT LIKE '5%' ) RESULTADO ORDER BY  CUENTA,MES   ";
			
			//System.out.println("Query getDetalleAnual Detalle Anual de Gastos sql " + sql);

			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			long acumulado = 0;
			long gastos = 0;
			int periodo = 0 ;
			String  unidad_aux = "";
			String aux = "";
			String aux_detalle = "";
			Boolean primera = true;
			Boolean guardo = false;
			Boolean anterior = false;
			Presw25POJO ss = new Presw25POJO();
			
			while (res.next()) {
				if (guardo)  guardo = false;
				unidad_aux = res.getString(1);
			/*	if (!guardo) 
				unidad_aux = res.getString(1);
				else guardo = false;
				*/
				if (primera){
					
					aux = unidad_aux;
					primera = false;
				} 
					
				////System.out.println("Cuenta "  + unidad_aux + " unidad " + aux + " suma acumulada " + ss.getPresupuesto());
			
			if(unidad_aux.equals(aux)) {
				int mes = res.getInt(3);				
				long gasto = res.getLong(4) ;
				long presupuesto = res.getLong(5) ;
				
				ss.setPresupuesto(ss.getPresupuesto() + presupuesto) ;
				ss.setGastoTotal(ss.getGastoTotal() + gasto);
				//System.out.println("Cuenta "  + unidad_aux + " Pesupuesto " + presupuesto + " suma acumulada " + ss.getPresupuesto());
				if (mes == 0){					
					ss.setGasto(res.getLong(4));
					ss.setPresupuesto(res.getLong(5));
				}else if (mes == 1){					
					ss.setGasto1(res.getLong(4));
					ss.setPresupuesto1(res.getLong(5));
				}else if (mes == 2){					
					ss.setGasto2(res.getLong(4));
					ss.setPresupuesto2(res.getLong(5));
				}else if (mes == 3){					
					ss.setGasto3(res.getLong(4));
					ss.setPresupuesto3(res.getLong(5));
				}else if (mes == 4){					
					ss.setGasto4(res.getLong(4));
					ss.setPresupuesto4(res.getLong(5));
				}else if (mes == 5){					
					ss.setGasto5(res.getLong(4));
					ss.setPresupuesto5(res.getLong(5));
				}else if (mes == 6){					
					ss.setGasto6(res.getLong(4));
					ss.setPresupuesto6(res.getLong(5));
				}else if (mes == 07){					
					ss.setGasto7(res.getLong(4));
					ss.setPresupuesto7(res.getLong(5));
				}else if (mes == 8){					
					ss.setGasto8(res.getLong(4));
					ss.setPresupuesto8(res.getLong(5));
				}else if (mes == 9){					
					ss.setGasto9(res.getLong(4));
					ss.setPresupuesto9(res.getLong(5));
				}else if (mes == 10){					
					ss.setGasto10(res.getLong(4));
					ss.setPresupuesto10(res.getLong(5));
				}else if (mes == 11){					
					ss.setGasto11(res.getLong(4));
					ss.setPresupuesto11(res.getLong(5));
				}else if (mes == 12){					
					ss.setGasto12(res.getLong(4));
					ss.setPresupuesto12(res.getLong(5));
				}else if  ((mes > periodo) && (periodo == 0)){					
					ss.setGasto(ss.getGasto() + res.getLong(4));
					ss.setPresupuesto(ss.getPresupuesto() + res.getLong(5));
				} else periodo = periodo + 1 ;
				
			}else{
				//res.previous();
				ss.setCoduni(aux);
				ss.setUnidad(aux_detalle);
				lista.add(ss);
				ss = new Presw25POJO();
				 guardo = true;
				 aux = unidad_aux;
				// guardo registro actual 
				 ss.setCoduni(aux);
				 ss.setUnidad(res.getString(2));
				 long gasto = res.getLong(4) ;
					long presupuesto = res.getLong(5) ;
				//System.out.println("Cuenta "  + unidad_aux + " Pesupuesto " + presupuesto + " suma acumulada " + ss.getPresupuesto());
					ss.setPresupuesto(presupuesto) ;
					int mes = res.getInt(3);	
					if (mes == 0){					
						ss.setGasto(res.getLong(4));
						ss.setPresupuesto(res.getLong(5));
					}else if (mes == 1){					
						ss.setGasto1(res.getLong(4));
						ss.setPresupuesto1(res.getLong(5));
					}else if (mes == 2){					
						ss.setGasto2(res.getLong(4));
						ss.setPresupuesto2(res.getLong(5));
					}else if (mes == 3){					
						ss.setGasto3(res.getLong(4));
						ss.setPresupuesto3(res.getLong(5));
					}else if (mes == 4){					
						ss.setGasto4(res.getLong(4));
						ss.setPresupuesto4(res.getLong(5));
					}else if (mes == 5){					
						ss.setGasto5(res.getLong(4));
						ss.setPresupuesto5(res.getLong(5));
					}else if (mes == 6){					
						ss.setGasto6(res.getLong(4));
						ss.setPresupuesto6(res.getLong(5));
					}else if (mes == 07){					
						ss.setGasto7(res.getLong(4));
						ss.setPresupuesto7(res.getLong(5));
					}else if (mes == 8){					
						ss.setGasto8(res.getLong(4));
						ss.setPresupuesto8(res.getLong(5));
					}else if (mes == 9){					
						ss.setGasto9(res.getLong(4));
						ss.setPresupuesto9(res.getLong(5));
					}else if (mes == 10){					
						ss.setGasto10(res.getLong(4));
						ss.setPresupuesto10(res.getLong(5));
					}else if (mes == 11){					
						ss.setGasto11(res.getLong(4));
						ss.setPresupuesto11(res.getLong(5));
					}else if (mes == 12){					
						ss.setGasto12(res.getLong(4));
						}
					
					unidad_aux = res.getString(1);
					//lista.add(ss);
			}
			if (!guardo)		
			aux = unidad_aux;
			aux_detalle = res.getString(2) ;
		/*	if(res.last()){
				ss.setCoduni(res.getString(1));
				ss.setUnidad(res.getString(2));
				lista.add(ss);
			}
				
			*/	
				
				// ss.setPresac(acumulado);/*
				// ss.setUsadac(gastos);
				
			}
			lista.add(ss);
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error getDetalleAnual Detalle Anual de Gastos: "+ e.getMessage());
		}
		return lista;
	}

	
	
	/*
	 * public Collection<Pres18POJO> getCuentasDetalles(String org, int
	 * a�oIN,int mes, String cuenta){
	 * 
	 * Calendar fecha = Calendar.getInstance(); int a�o = a�oIN; String a�osql =
	 * String.valueOf(a�o); a�osql = a�osql.substring(2); a�o = 0; a�o =
	 * Integer.parseInt(a�osql); //System.out.println("Entra a getCuentasDetalles
	 * ORG " + org + " a�o " + a�o + " mes " + mes + " cuenta " + cuenta);
	 * ConexionBanner con = new ConexionBanner(); PreparedStatement sent = null;
	 * ResultSet res = null; int sw = 0 ; List<Pres18POJO> lista = new
	 * ArrayList<Pres18POJO>(); try { //System.out.println("Va a llamar a
	 * verificaDetalle"); sw = verificaDetalle(org,a�oIN,mes);
	 * //System.out.println("Resultado Va a llamar a verificaDetalle " + sw);
	 * }catch (Exception ex){
	 *  }
	 * 
	 * try{ String sql = "" ; if (cuenta.equals("-1")) { sql = "" + " SELECT
	 * fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM(fg.FGBOPAL_00_ADOPT_BUD ) + SUM(fg.FGBOPAL_00_BUD_ADJT) presu" + " ,
	 * SUM(fg.FGBOPAL_00_YTD_ACTV) gasto ," + "
	 * ROUND(SUM(FG.FGBOPAL_01_YTD_ACTV) / NULLIF( (SUM(FG.FGBOPAL_00_ADOPT_BUD +
	 * FG.FGBOPAL_01_ADOPT_BUD) + SUM(FG.FGBOPAL_00_BUD_ADJT +
	 * FG.FGBOPAL_01_BUD_ADJT)),0 )*100,2 ) porc " + " , 0 mes," + "
	 * SUM((FGBOPAL_00_ADOPT_BUD ) + (FGBOPAL_00_BUD_ADJT ) -
	 * FGBOPAL_00_YTD_ACTV )saldo_mes ," + " SUM (FGBOPAL_00_ADOPT_BUD +
	 * FGBOPAL_00_BUD_ADJT) pre_acum," + " sum (FGBOPAL_00_YTD_ACTV) gasto_acum ," + "
	 * round(SUM(fg.FGBOPAL_01_YTD_ACTV) / NULLIF( (SUM(fg.FGBOPAL_01_ADOPT_BUD +
	 * fg.FGBOPAL_01_BUD_ADJT) *100),0),2) porc2 ," + "
	 * SUM((FGBOPAL_00_ADOPT_BUD ) - FGBOPAL_00_YTD_ACTV) total" + " FROM
	 * FGBOPAL fg ,ftvorgn ft , ftvfund ff " + " where fg.FGBOPAL_ORGN_CODE =
	 * ft.ftvorgn_orgn_code and ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and
	 * fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "'
	 * and fg.FGBOPAL_ORGN_CODE = '"+ org + "'" + " GROUP BY
	 * fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 0 " + " UNION ALL " + " SELECT
	 * fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM(fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) +
	 * SUM(fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT) presu" //+ "
	 * SUM((FGBOPAL_01_ADOPT_BUD ) + (FGBOPAL_01_BUD_ADJT)) presu" + " ,
	 * SUM(fg.FGBOPAL_01_YTD_ACTV + fg.FGBOPAL_01_ENCUMB +
	 * fg.FGBOPAL_01_BUD_RSRV) gasto ," + " ROUND((SUM(fg.FGBOPAL_01_YTD_ACTV +
	 * fg.FGBOPAL_01_ENCUMB + fg.FGBOPAL_01_BUD_RSRV) / NULLIF(
	 * (SUM(fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) +
	 * SUM(fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)),0 )*100 ),2) porc " + " ,
	 * 1 mes," + " SUM(fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD +
	 * fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT ) - sum
	 * (fg.FGBOPAL_01_YTD_ACTV + fg.FGBOPAL_01_ENCUMB + fg.FGBOPAL_01_BUD_RSRV)
	 * saldo_mes ," + " SUM (fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_01_BUD_ADJT)
	 * pre_acum," //+ " SUM((fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) +
	 * (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)) presu_acum ," + " sum
	 * (fg.FGBOPAL_01_YTD_ACTV + fg.FGBOPAL_01_ENCUMB + fg.FGBOPAL_01_BUD_RSRV)
	 * gasto_acum ," + " ROUND((sum(fg.FGBOPAL_01_YTD_ACTV +
	 * fg.FGBOPAL_01_ENCUMB + fg.FGBOPAL_01_BUD_RSRV) / NULLIF(
	 * SUM(FGBOPAL_01_ADOPT_BUD + FGBOPAL_01_BUD_ADJT),0) ) *100,2 ) porc2, " + "
	 * (SUM(fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_01_BUD_ADJT) - SUM
	 * (FG.FGBOPAL_01_YTD_ACTV + FG.FGBOPAL_01_ENCUMB + FG.FGBOPAL_01_BUD_RSRV))
	 * total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff " + " where
	 * fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =
	 * fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code = 'FINANC' and
	 * fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE = '"+ org +
	 * "'" + " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 1 " + " UNION
	 * ALL " + " SELECT fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT) -
	 * (fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) +
	 * (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_02_ADOPT_BUD ) + (FGBOPAL_02_BUD_ADJT)) presu" + " ,
	 * (SUM(FG.FGBOPAL_02_YTD_ACTV + FG.FGBOPAL_02_ENCUMB +
	 * FG.FGBOPAL_02_BUD_RSRV) - SUM(FG.FGBOPAL_01_YTD_ACTV +
	 * FG.FGBOPAL_01_ENCUMB + FG.FGBOPAL_01_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FGBOPAL_02_YTD_ACTV + FG.FGBOPAL_02_ENCUMB +
	 * FG.FGBOPAL_02_BUD_RSRV) - sum(FG.FGBOPAL_01_YTD_ACTV +
	 * FG.FGBOPAL_01_ENCUMB + FG.FGBOPAL_01_BUD_RSRV) ) / NULLIF( ((
	 * (SUM(fg.FGBOPAL_02_ADOPT_BUD) + SUM(fg.FGBOPAL_02_BUD_ADJT))) -
	 * ((SUM(fg.FGBOPAL_00_ADOPT_BUD) + SUM(fg.FGBOPAL_01_ADOPT_BUD))) +
	 * ((SUM(fg.FGBOPAL_00_BUD_ADJT) + SUM(fg.FGBOPAL_01_BUD_ADJT)))),0) *100
	 * ,2) porc" + " , 2 mes," + " ((SUM(FG.FGBOPAL_02_ADOPT_BUD +
	 * fg.FGBOPAL_02_BUD_ADJT) - SUM(FG.FGBOPAL_00_ADOPT_BUD +
	 * FG.FGBOPAL_01_ADOPT_BUD) + SUM(FG.FGBOPAL_00_BUD_ADJT +
	 * FG.FGBOPAL_01_BUD_ADJT)) - (SUM(fg.FGBOPAL_02_YTD_ACTV +
	 * fg.FGBOPAL_02_ENCUMB + fg.FGBOPAL_02_BUD_RSRV) -
	 * SUM(fg.FGBOPAL_01_YTD_ACTV + fg.FGBOPAL_01_ENCUMB +
	 * fg.FGBOPAL_01_BUD_RSRV))) saldo_mes ," + " SUM (fg.FGBOPAL_02_ADOPT_BUD +
	 * fg.FGBOPAL_02_BUD_ADJT) pre_acum," //+ " SUM((fg.FGBOPAL_02_ADOPT_BUD +
	 * fg.FGBOPAL_02_BUD_ADJT) - (fg.FGBOPAL_00_ADOPT_BUD +
	 * fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT +
	 * fg.FGBOPAL_01_BUD_ADJT)) pre_acum, " + " sum (fg.FGBOPAL_02_YTD_ACTV +
	 * fg.FGBOPAL_02_ENCUMB + fg.FGBOPAL_02_BUD_RSRV) gasto_acum ," + "
	 * ROUND((sum(fg. FGBOPAL_02_YTD_ACTV + fg.FGBOPAL_02_ENCUMB +
	 * fg.FGBOPAL_02_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_02_ADOPT_BUD +
	 * fg.FGBOPAL_02_BUD_ADJT),0)) ,2 ) *100 porc2 ," + " (SUM
	 * (fg.FGBOPAL_02_ADOPT_BUD) + sum(fg.FGBOPAL_02_BUD_ADJT)) -
	 * sum(fg.FGBOPAL_02_YTD_ACTV + fg.FGBOPAL_02_ENCUMB +
	 * fg.FGBOPAL_02_BUD_RSRV) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "'" + " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 2 " + "
	 * UNION ALL " + " SELECT fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT) - (
	 * fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_03_ADOPT_BUD ) + (FGBOPAL_03_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_03_YTD_ACTV + fg.FGBOPAL_03_ENCUMB +
	 * fg.FGBOPAL_03_BUD_RSRV) - sum(fg.FGBOPAL_02_YTD_ACTV +
	 * fg.FGBOPAL_02_ENCUMB + fg. FGBOPAL_02_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(fg.FGBOPAL_03_YTD_ACTV + fg.FGBOPAL_03_ENCUMB +
	 * fg.FGBOPAL_03_BUD_RSRV) - SUM(fg.FGBOPAL_02_YTD_ACTV +
	 * fg.FGBOPAL_02_ENCUMB + fg.FGBOPAL_02_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_03_ADOPT_BUD) + SUM(fg.FGBOPAL_03_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_02_ADOPT_BUD) + SUM(fg.FGBOPAL_02_BUD_ADJT))),0) *100 ,2)
	 * porc" + " , 3 mes," + " (sum(FGBOPAL_03_ADOPT_BUD + FGBOPAL_03_BUD_ADJT) -
	 * sum( FGBOPAL_02_ADOPT_BUD + FGBOPAL_02_BUD_ADJT))
	 * -(SUM(FGBOPAL_03_YTD_ACTV + FGBOPAL_03_ENCUMB + FGBOPAL_03_BUD_RSRV) -
	 * SUM(FGBOPAL_02_YTD_ACTV + FGBOPAL_02_ENCUMB + FGBOPAL_02_BUD_RSRV))
	 * saldo_mes ," + " SUM((FGBOPAL_03_ADOPT_BUD ) + (FGBOPAL_03_BUD_ADJT))
	 * pre_acum," + " sum (fg.FGBOPAL_03_YTD_ACTV + fg.FGBOPAL_03_ENCUMB +
	 * fg.FGBOPAL_03_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(fg.FGBOPAL_03_YTD_ACTV + FG.FGBOPAL_03_ENCUMB +
	 * FG.FGBOPAL_03_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_03_ADOPT_BUD +
	 * fg.FGBOPAL_03_BUD_ADJT),0)) *100 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT) -
	 * SUM(fg.FGBOPAL_03_YTD_ACTV + FG.FGBOPAL_03_ENCUMB +
	 * FG.FGBOPAL_03_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "'" + " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 3 " + "
	 * UNION ALL " + " SELECT fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT) - (
	 * fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_04_ADOPT_BUD ) + (FGBOPAL_04_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_04_YTD_ACTV + fg.FGBOPAL_04_ENCUMB +
	 * fg.FGBOPAL_04_BUD_RSRV) - sum(fg.FGBOPAL_03_YTD_ACTV +
	 * fg.FGBOPAL_03_ENCUMB + fg.FGBOPAL_03_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(fg.FGBOPAL_04_YTD_ACTV + fg.FGBOPAL_04_ENCUMB +
	 * fg.FGBOPAL_04_BUD_RSRV) - SUM(fg.FGBOPAL_03_YTD_ACTV +
	 * fg.FGBOPAL_03_ENCUMB + fg.FGBOPAL_03_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_04_ADOPT_BUD) + SUM(fg.FGBOPAL_04_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_03_ADOPT_BUD) + SUM(fg.FGBOPAL_03_BUD_ADJT))),0) *100 ,2)
	 * porc" + " , 4 mes," + " (SUM(FG.FGBOPAL_04_ADOPT_BUD) +
	 * SUM(FG.FGBOPAL_04_BUD_ADJT))-(SUM(FG.FGBOPAL_03_ADOPT_BUD +
	 * FG.FGBOPAL_03_BUD_ADJT))- (sum(fg.FGBOPAL_04_YTD_ACTV +
	 * fg.FGBOPAL_04_ENCUMB + fg.FGBOPAL_04_BUD_RSRV) -
	 * SUM(fg.FGBOPAL_03_YTD_ACTV + fg.FGBOPAL_03_ENCUMB +
	 * fg.FGBOPAL_03_BUD_RSRV)) saldo_mes ," + " SUM((FGBOPAL_04_ADOPT_BUD ) +
	 * (FGBOPAL_04_BUD_ADJT)) pre_acum," + " sum (fg.FGBOPAL_04_YTD_ACTV +
	 * fg.FGBOPAL_04_ENCUMB + fg.FGBOPAL_04_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(fg.FGBOPAL_04_YTD_ACTV + fg.FGBOPAL_04_ENCUMB +
	 * fg.FGBOPAL_04_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_04_ADOPT_BUD +
	 * fg.FGBOPAL_04_BUD_ADJT),0)) *100 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT) -
	 * SUM(fg.FGBOPAL_04_YTD_ACTV + fg.FGBOPAL_04_ENCUMB +
	 * fg.FGBOPAL_04_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "'" + " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 4 " + "
	 * UNION ALL " + " SELECT fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT) - (
	 * fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_05_ADOPT_BUD ) + (FGBOPAL_05_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_05_YTD_ACTV + fg.FGBOPAL_05_ENCUMB +
	 * fg.FGBOPAL_05_BUD_RSRV) - sum(fg.FGBOPAL_04_YTD_ACTV +
	 * fg.FGBOPAL_04_ENCUMB + fg.FGBOPAL_04_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FG.FGBOPAL_05_YTD_ACTV + FG.FGBOPAL_05_ENCUMB +
	 * FG.FGBOPAL_05_BUD_RSRV) - SUM(FG.FGBOPAL_04_YTD_ACTV +
	 * FG.FGBOPAL_04_ENCUMB + FG.FGBOPAL_04_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_05_ADOPT_BUD) + SUM(fg.FGBOPAL_05_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_04_ADOPT_BUD) + SUM(fg.FGBOPAL_04_BUD_ADJT))),0) *100 ,2)
	 * porc" + " , 5 mes," + " (sum(FG.FGBOPAL_05_ADOPT_BUD +
	 * FG.FGBOPAL_05_BUD_ADJT) - SUM( FG.FGBOPAL_04_ADOPT_BUD +
	 * FG.FGBOPAL_04_BUD_ADJT) - (SUM(FG.FGBOPAL_05_YTD_ACTV +
	 * FG.FGBOPAL_05_ENCUMB + FG.FGBOPAL_05_BUD_RSRV) -
	 * SUM(FG.FGBOPAL_04_YTD_ACTV + FG.FGBOPAL_04_ENCUMB +
	 * FG.FGBOPAL_04_BUD_RSRV))) saldo_mes ," + " SUM((FGBOPAL_05_ADOPT_BUD ) +
	 * (FGBOPAL_05_BUD_ADJT)) pre_acum," + " sum (FG.FGBOPAL_05_YTD_ACTV +
	 * FG.FGBOPAL_05_ENCUMB + FG.FGBOPAL_05_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(FG.FGBOPAL_05_YTD_ACTV + FG.FGBOPAL_05_ENCUMB +
	 * FG.FGBOPAL_05_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_05_ADOPT_BUD +
	 * fg.FGBOPAL_05_BUD_ADJT),0)) *100 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT) -
	 * SUM(FG.FGBOPAL_05_YTD_ACTV + FG.FGBOPAL_05_ENCUMB +
	 * FG.FGBOPAL_05_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "'" + " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,5 " + "
	 * UNION ALL " + " SELECT fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT) - (
	 * fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_06_ADOPT_BUD ) + (FGBOPAL_06_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_06_YTD_ACTV + fg.FGBOPAL_06_ENCUMB +
	 * fg.FGBOPAL_06_BUD_RSRV) - sum(fg.FGBOPAL_05_YTD_ACTV +
	 * fg.FGBOPAL_05_ENCUMB + fg.FGBOPAL_05_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FG.FGBOPAL_06_YTD_ACTV + FG.FGBOPAL_06_ENCUMB +
	 * FG.FGBOPAL_06_BUD_RSRV) - SUM(FG.FGBOPAL_05_YTD_ACTV +
	 * FG.FGBOPAL_05_ENCUMB + FG.FGBOPAL_05_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_06_ADOPT_BUD) + SUM(fg.FGBOPAL_06_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_05_ADOPT_BUD) + SUM(fg.FGBOPAL_05_BUD_ADJT))),0) *100 ,2)
	 * porc" + " , 6 mes," + " (sum(FG.FGBOPAL_06_ADOPT_BUD +
	 * FG.FGBOPAL_06_BUD_ADJT) - SUM( FG.FGBOPAL_05_ADOPT_BUD +
	 * FG.FGBOPAL_05_BUD_ADJT) - (SUM(FG.FGBOPAL_06_YTD_ACTV +
	 * FG.FGBOPAL_06_ENCUMB + FG.FGBOPAL_06_BUD_RSRV) -
	 * SUM(FG.FGBOPAL_05_YTD_ACTV + FG.FGBOPAL_05_ENCUMB +
	 * FG.FGBOPAL_05_BUD_RSRV))) saldo_mes ," + " SUM((FGBOPAL_06_ADOPT_BUD ) +
	 * (FGBOPAL_06_BUD_ADJT)) pre_acum," + " sum (FG.FGBOPAL_06_YTD_ACTV +
	 * FG.FGBOPAL_06_ENCUMB + FG.FGBOPAL_06_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(FG.FGBOPAL_06_YTD_ACTV + FG.FGBOPAL_06_ENCUMB +
	 * FG.FGBOPAL_06_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_06_ADOPT_BUD +
	 * fg.FGBOPAL_06_BUD_ADJT),0)) *100 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT) -
	 * SUM(FG.FGBOPAL_06_YTD_ACTV + FG.FGBOPAL_06_ENCUMB +
	 * FG.FGBOPAL_06_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "'" + " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,6" + "
	 * UNION ALL " + " SELECT fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT) - (
	 * fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_07_ADOPT_BUD ) + (FGBOPAL_07_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_07_YTD_ACTV + fg.FGBOPAL_07_ENCUMB +
	 * fg.FGBOPAL_07_BUD_RSRV) - sum(fg.FGBOPAL_06_YTD_ACTV +
	 * fg.FGBOPAL_06_ENCUMB + fg.FGBOPAL_06_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FG.FGBOPAL_07_YTD_ACTV + FG.FGBOPAL_07_ENCUMB +
	 * FG.FGBOPAL_07_BUD_RSRV) - SUM(FG.FGBOPAL_06_YTD_ACTV +
	 * FG.FGBOPAL_06_ENCUMB + FG.FGBOPAL_06_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_07_ADOPT_BUD) + SUM(fg.FGBOPAL_07_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_06_ADOPT_BUD) + SUM(fg.FGBOPAL_06_BUD_ADJT))),0) *100 ,2)
	 * porc" + " , 7 mes," + " (sum(FG.FGBOPAL_07_ADOPT_BUD +
	 * FG.FGBOPAL_07_BUD_ADJT) - SUM( FG.FGBOPAL_06_ADOPT_BUD +
	 * FG.FGBOPAL_06_BUD_ADJT) - (SUM(FG.FGBOPAL_07_YTD_ACTV +
	 * FG.FGBOPAL_07_ENCUMB + FG.FGBOPAL_07_BUD_RSRV) -
	 * SUM(fg.FGBOPAL_06_YTD_ACTV + fg.FGBOPAL_06_ENCUMB +
	 * FG.FGBOPAL_06_BUD_RSRV))) saldo_mes ," + " SUM((FGBOPAL_07_ADOPT_BUD ) +
	 * (FGBOPAL_07_BUD_ADJT)) pre_acum," + " sum (FG.FGBOPAL_07_YTD_ACTV +
	 * FG.FGBOPAL_07_ENCUMB + FG.FGBOPAL_07_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(FG.FGBOPAL_07_YTD_ACTV + FG.FGBOPAL_07_ENCUMB +
	 * FG.FGBOPAL_07_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_07_ADOPT_BUD +
	 * fg.FGBOPAL_07_BUD_ADJT),0)) *100 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT) -
	 * SUM(FG.FGBOPAL_07_YTD_ACTV + FG.FGBOPAL_07_ENCUMB +
	 * FG.FGBOPAL_07_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "'" + " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,7" + "
	 * UNION ALL " + " SELECT fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT) - (
	 * fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_08_ADOPT_BUD ) + (FGBOPAL_08_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_08_YTD_ACTV + fg.FGBOPAL_08_ENCUMB +
	 * fg.FGBOPAL_08_BUD_RSRV) - sum(fg.FGBOPAL_07_YTD_ACTV +
	 * fg.FGBOPAL_07_ENCUMB + fg.FGBOPAL_07_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FG.FGBOPAL_08_YTD_ACTV + FG.FGBOPAL_08_ENCUMB +
	 * FG.FGBOPAL_08_BUD_RSRV) - SUM(FG.FGBOPAL_07_YTD_ACTV +
	 * FG.FGBOPAL_07_ENCUMB + FG.FGBOPAL_07_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_08_ADOPT_BUD) + SUM(fg.FGBOPAL_08_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_07_ADOPT_BUD) + SUM(fg.FGBOPAL_07_BUD_ADJT))),0) *100 ,2)
	 * porc" + " , 8 mes," + " (sum(FG.FGBOPAL_08_ADOPT_BUD +
	 * FG.FGBOPAL_08_BUD_ADJT) - SUM( FG.FGBOPAL_07_ADOPT_BUD +
	 * FG.FGBOPAL_07_BUD_ADJT) - (SUM(FG.FGBOPAL_08_YTD_ACTV +
	 * FG.FGBOPAL_08_ENCUMB + FG.FGBOPAL_08_BUD_RSRV) -
	 * SUM(FG.FGBOPAL_07_YTD_ACTV + FG.FGBOPAL_07_ENCUMB +
	 * FG.FGBOPAL_07_BUD_RSRV))) saldo_mes ," + " SUM((FGBOPAL_08_ADOPT_BUD ) +
	 * (FGBOPAL_08_BUD_ADJT)) pre_acum," + " sum (FG.FGBOPAL_08_YTD_ACTV +
	 * FG.FGBOPAL_08_ENCUMB + FG.FGBOPAL_08_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(FG.FGBOPAL_08_YTD_ACTV + FG.FGBOPAL_08_ENCUMB +
	 * FG.FGBOPAL_08_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_08_ADOPT_BUD +
	 * fg.FGBOPAL_08_BUD_ADJT),0)) *100 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT) -
	 * SUM(FG.FGBOPAL_08_YTD_ACTV + FG.FGBOPAL_08_ENCUMB +
	 * FG.FGBOPAL_08_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "'" + " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,8" + "
	 * UNION ALL " + " SELECT fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT) - (
	 * fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_09_ADOPT_BUD ) + (FGBOPAL_09_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_09_YTD_ACTV + fg.FGBOPAL_09_ENCUMB +
	 * fg.FGBOPAL_09_BUD_RSRV) - sum(fg.FGBOPAL_08_YTD_ACTV +
	 * fg.FGBOPAL_08_ENCUMB + fg.FGBOPAL_08_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FG.FGBOPAL_09_YTD_ACTV + FG.FGBOPAL_09_ENCUMB +
	 * FG.FGBOPAL_09_BUD_RSRV) - SUM(FG.FGBOPAL_08_YTD_ACTV +
	 * FG.FGBOPAL_08_ENCUMB + FG.FGBOPAL_08_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_09_ADOPT_BUD) + SUM(fg.FGBOPAL_09_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_08_ADOPT_BUD) + SUM(fg.FGBOPAL_08_BUD_ADJT))),0) *100 ,2)
	 * porc" + " , 9 mes," + " (sum(FG.FGBOPAL_09_ADOPT_BUD +
	 * FG.FGBOPAL_09_BUD_ADJT) - SUM( FG.FGBOPAL_08_ADOPT_BUD +
	 * FG.FGBOPAL_08_BUD_ADJT) - (SUM(FG.FGBOPAL_09_YTD_ACTV +
	 * FG.FGBOPAL_09_ENCUMB + FG.FGBOPAL_09_BUD_RSRV) -
	 * SUM(FG.FGBOPAL_08_YTD_ACTV + FG.FGBOPAL_08_ENCUMB +
	 * FG.FGBOPAL_08_BUD_RSRV))) saldo_mes ," + " SUM((FGBOPAL_09_ADOPT_BUD ) +
	 * (FGBOPAL_09_BUD_ADJT)) pre_acum," + " sum (FG.FGBOPAL_09_YTD_ACTV +
	 * FG.FGBOPAL_09_ENCUMB + FG.FGBOPAL_09_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(FG.FGBOPAL_09_YTD_ACTV + FG.FGBOPAL_09_ENCUMB +
	 * FG.FGBOPAL_09_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_09_ADOPT_BUD +
	 * fg.FGBOPAL_09_BUD_ADJT),0)) *100 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT) -
	 * SUM(FG.FGBOPAL_09_YTD_ACTV + FG.FGBOPAL_09_ENCUMB +
	 * FG.FGBOPAL_09_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "'" + " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,9" + "
	 * UNION ALL " + " SELECT fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT) - (
	 * fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_10_ADOPT_BUD ) + (FGBOPAL_10_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_10_YTD_ACTV + fg.FGBOPAL_10_ENCUMB +
	 * fg.FGBOPAL_10_BUD_RSRV) - sum(fg.FGBOPAL_09_YTD_ACTV +
	 * fg.FGBOPAL_09_ENCUMB + fg.FGBOPAL_09_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FG.FGBOPAL_10_YTD_ACTV + FG.FGBOPAL_10_ENCUMB +
	 * FG.FGBOPAL_10_BUD_RSRV) - SUM(FG.FGBOPAL_09_YTD_ACTV +
	 * FG.FGBOPAL_09_ENCUMB + FG.FGBOPAL_09_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_10_ADOPT_BUD) + SUM(fg.FGBOPAL_10_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_09_ADOPT_BUD) + SUM(fg.FGBOPAL_09_BUD_ADJT))),0) *100 ,2)
	 * porc" + " , 10 mes," + " (sum(FG.FGBOPAL_10_ADOPT_BUD +
	 * FG.FGBOPAL_10_BUD_ADJT) - SUM( FG.FGBOPAL_09_ADOPT_BUD +
	 * FG.FGBOPAL_09_BUD_ADJT) - (SUM(FG.FGBOPAL_10_YTD_ACTV +
	 * FG.FGBOPAL_10_ENCUMB + FG.FGBOPAL_10_BUD_RSRV) -
	 * SUM(FG.FGBOPAL_09_YTD_ACTV + FG.FGBOPAL_09_ENCUMB +
	 * FG.FGBOPAL_09_BUD_RSRV))) saldo_mes ," + " SUM((FGBOPAL_10_ADOPT_BUD ) +
	 * (FGBOPAL_10_BUD_ADJT)) pre_acum," + " sum (FG.FGBOPAL_10_YTD_ACTV +
	 * FG.FGBOPAL_10_ENCUMB + FG.FGBOPAL_10_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(FG.FGBOPAL_10_YTD_ACTV + FG.FGBOPAL_10_ENCUMB +
	 * FG.FGBOPAL_10_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_10_ADOPT_BUD +
	 * fg.FGBOPAL_10_BUD_ADJT),0)) *100 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT) -
	 * SUM(FG.FGBOPAL_10_YTD_ACTV + FG.FGBOPAL_10_ENCUMB +
	 * FG.FGBOPAL_10_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "'" + " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,10" + "
	 * UNION ALL " + " SELECT fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT) - (
	 * fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_11_ADOPT_BUD ) + (FGBOPAL_11_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_11_YTD_ACTV + fg.FGBOPAL_11_ENCUMB +
	 * fg.FGBOPAL_11_BUD_RSRV) - sum(fg.FGBOPAL_10_YTD_ACTV +
	 * fg.FGBOPAL_10_ENCUMB + fg.FGBOPAL_10_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FG.FGBOPAL_11_YTD_ACTV + FG.FGBOPAL_11_ENCUMB +
	 * FG.FGBOPAL_11_BUD_RSRV) - SUM(FG.FGBOPAL_10_YTD_ACTV +
	 * FG.FGBOPAL_10_ENCUMB + FG.FGBOPAL_10_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_11_ADOPT_BUD) + SUM(fg.FGBOPAL_11_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_10_ADOPT_BUD) + SUM(fg.FGBOPAL_10_BUD_ADJT))),0) *110 ,2)
	 * porc" + " , 11 mes," + " (sum(FG.FGBOPAL_11_ADOPT_BUD +
	 * FG.FGBOPAL_11_BUD_ADJT) - SUM( FG.FGBOPAL_10_ADOPT_BUD +
	 * FG.FGBOPAL_10_BUD_ADJT) - (SUM(FG.FGBOPAL_11_YTD_ACTV +
	 * FG.FGBOPAL_11_ENCUMB + FG.FGBOPAL_11_BUD_RSRV) -
	 * SUM(FG.FGBOPAL_10_YTD_ACTV + FG.FGBOPAL_10_ENCUMB +
	 * FG.FGBOPAL_10_BUD_RSRV))) saldo_mes ," + " SUM((FGBOPAL_11_ADOPT_BUD ) +
	 * (FGBOPAL_11_BUD_ADJT)) pre_acum," + " sum (FG.FGBOPAL_11_YTD_ACTV +
	 * FG.FGBOPAL_11_ENCUMB + FG.FGBOPAL_11_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(FG.FGBOPAL_11_YTD_ACTV + FG.FGBOPAL_11_ENCUMB +
	 * FG.FGBOPAL_11_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_11_ADOPT_BUD +
	 * fg.FGBOPAL_11_BUD_ADJT),0)) *110 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT) -
	 * SUM(FG.FGBOPAL_11_YTD_ACTV + FG.FGBOPAL_11_ENCUMB +
	 * FG.FGBOPAL_11_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "'" + " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,11" + "
	 * UNION ALL " + " SELECT fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT) - (
	 * fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_12_ADOPT_BUD ) + (FGBOPAL_12_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_12_YTD_ACTV + fg.FGBOPAL_12_ENCUMB +
	 * fg.FGBOPAL_12_BUD_RSRV) - sum(fg.FGBOPAL_11_YTD_ACTV +
	 * fg.FGBOPAL_11_ENCUMB + fg.FGBOPAL_11_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FG.FGBOPAL_12_YTD_ACTV + FG.FGBOPAL_12_ENCUMB +
	 * FG.FGBOPAL_12_BUD_RSRV) - SUM(FG.FGBOPAL_11_YTD_ACTV +
	 * FG.FGBOPAL_11_ENCUMB + FG.FGBOPAL_11_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_12_ADOPT_BUD) + SUM(fg.FGBOPAL_12_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_11_ADOPT_BUD) + SUM(fg.FGBOPAL_11_BUD_ADJT))),0) *120 ,2)
	 * porc" + " , 12 mes," + " (sum(FG.FGBOPAL_12_ADOPT_BUD +
	 * FG.FGBOPAL_12_BUD_ADJT) - SUM( FG.FGBOPAL_11_ADOPT_BUD +
	 * FG.FGBOPAL_11_BUD_ADJT) - (SUM(FG.FGBOPAL_12_YTD_ACTV +
	 * FG.FGBOPAL_12_ENCUMB + FG.FGBOPAL_12_BUD_RSRV) -
	 * SUM(FG.FGBOPAL_11_YTD_ACTV + FG.FGBOPAL_11_ENCUMB +
	 * FG.FGBOPAL_11_BUD_RSRV))) saldo_mes ," + " SUM((FGBOPAL_12_ADOPT_BUD ) +
	 * (FGBOPAL_12_BUD_ADJT)) pre_acum," + " sum (FG.FGBOPAL_12_YTD_ACTV +
	 * FG.FGBOPAL_12_ENCUMB + FG.FGBOPAL_12_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(FG.FGBOPAL_12_YTD_ACTV + FG.FGBOPAL_12_ENCUMB +
	 * FG.FGBOPAL_12_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_12_ADOPT_BUD +
	 * fg.FGBOPAL_12_BUD_ADJT),0)) *120 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT) -
	 * SUM(FG.FGBOPAL_12_YTD_ACTV + FG.FGBOPAL_12_ENCUMB +
	 * FG.FGBOPAL_12_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "'" + " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,12";
	 *  /* sql = "" + " SELECT" + " FGBTRND_ORGN_CODE," + "
	 * FGBTRND_POSTING_PERIOD," + "
	 * SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0))
	 * AS \"Saldo_Inicial_A\"," + "
	 * SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0))
	 * AS \"Presupuesto_B\"," + "
	 * SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0))
	 * AS \"Traspasos_Presupuesto_C\"," + "
	 * SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD08',FGBTRND.FGBTRND_TRANS_AMT,0),0))
	 * AS \"Traspasos_Servicio_D\"," + "(" + "
	 * SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0)) + " + "
	 * SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0))" + " )
	 * AS \"Gasto\"," + "
	 * (SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD01',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +" + "
	 * SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,'BD05',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +" + "
	 * SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD02',FGBTRND.FGBTRND_TRANS_AMT,0),0)) +" + "
	 * SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,'BD08',FGBTRND.FGBTRND_TRANS_AMT,0),0)) -" + "
	 * SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0)) -" + "
	 * SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0))" + " )
	 * AS \"SUM Gasto\"" + " FROM FIMSMGR.FGBTRND," + " FIMSMGR.FTVFUND," + "
	 * FIMSMGR.FTVORGN," + " FIMSMGR.FTVACCT," + " FIMSMGR.FTVPROG" + " WHERE
	 * FGBTRND_FUND_CODE = FTVFUND_FUND_CODE" + " AND FGBTRND_ORGN_CODE =
	 * FTVORGN_ORGN_CODE" + " AND FGBTRND_PROG_CODE = FTVPROG_PROG_CODE" + " AND
	 * FGBTRND_ACCT_CODE = FTVACCT_ACCT_CODE" + " AND FGBTRND_ACTIVITY_DATE >=
	 * FTVFUND_EFF_DATE" + " AND FGBTRND_ACTIVITY_DATE < FTVFUND_NCHG_DATE" + "
	 * AND FGBTRND_ACTIVITY_DATE >= FTVORGN_EFF_DATE" + " AND
	 * FGBTRND_ACTIVITY_DATE < FTVORGN_NCHG_DATE" + " AND FGBTRND_ACTIVITY_DATE >=
	 * FTVPROG_EFF_DATE" + " AND FGBTRND_ACTIVITY_DATE < FTVPROG_NCHG_DATE" + "
	 * AND FGBTRND_ACTIVITY_DATE >= FTVACCT_EFF_DATE" + " AND
	 * FGBTRND_ACTIVITY_DATE < FTVACCT_NCHG_DATE" + " AND FTVFUND_FTYP_CODE IN
	 * ('CA','CD','CN','FI','FR')" + " AND FTVFUND_DATA_ENTRY_IND = 'Y'" + " AND
	 * FTVORGN_DATA_ENTRY_IND = 'Y'" + " AND FTVACCT_DATA_ENTRY_IND = 'Y'" + "
	 * AND FTVPROG_DATA_ENTRY_IND = 'Y'" + " AND FGBTRND.FGBTRND_LEDC_CODE =
	 * 'FINANC'" + " AND FGBTRND_LEDGER_IND = 'O'" + " AND FGBTRND_FSYR_CODE =
	 * '"+ a�o + "'" + " AND FGBTRND_ORGN_CODE = '"+ org + "'"
	 *  + " GROUP BY FGBTRND_ORGN_CODE, FGBTRND_POSTING_PERIOD" + " --
	 * FTVORGN_TITLE," + " -- FGBTRND_FUND_CODE," + " -- FTVFUND_TITLE," + " --
	 * FGBTRND_ACCT_CODE," + " -- FTVACCT_TITLE," + " -- FGBTRND_PROG_CODE," + " --
	 * FTVPROG_TITLE"
	 *  + " ORDER BY FGBTRND_POSTING_PERIOD"; * /
	 *  } else { sql = "" + " SELECT fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM(fg.FGBOPAL_00_ADOPT_BUD ) + SUM(fg.FGBOPAL_00_BUD_ADJT) presu" + " ,
	 * SUM(fg.FGBOPAL_00_YTD_ACTV) gasto ," + "
	 * ROUND(SUM(FG.FGBOPAL_01_YTD_ACTV) / NULLIF( (SUM(FG.FGBOPAL_00_ADOPT_BUD +
	 * FG.FGBOPAL_01_ADOPT_BUD) + SUM(FG.FGBOPAL_00_BUD_ADJT +
	 * FG.FGBOPAL_01_BUD_ADJT)),0 )*100,2 ) porc " + " , 0 mes," + "
	 * SUM((FGBOPAL_00_ADOPT_BUD ) + (FGBOPAL_00_BUD_ADJT ) -
	 * FGBOPAL_00_YTD_ACTV )saldo_mes ," + " SUM (FGBOPAL_00_ADOPT_BUD +
	 * FGBOPAL_00_BUD_ADJT) pre_acum," + " sum (FGBOPAL_00_YTD_ACTV) gasto_acum ," + "
	 * round(SUM(fg.FGBOPAL_01_YTD_ACTV) / NULLIF( (SUM(fg.FGBOPAL_01_ADOPT_BUD +
	 * fg.FGBOPAL_01_BUD_ADJT) *100),0),2) porc2 ," + "
	 * SUM((FGBOPAL_00_ADOPT_BUD ) - FGBOPAL_00_YTD_ACTV) total" + " FROM
	 * FGBOPAL fg ,ftvorgn ft , ftvfund ff " + " where fg.FGBOPAL_ORGN_CODE =
	 * ft.ftvorgn_orgn_code and ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and
	 * fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "'
	 * and fg.FGBOPAL_ORGN_CODE = '"+ org + "' AND fg.FGBOPAL_ACCT_CODE = '" +
	 * cuenta +"' " + " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 0 " + "
	 * UNION ALL " + " SELECT fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM(fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) +
	 * SUM(fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT) presu" //+ "
	 * SUM((FGBOPAL_01_ADOPT_BUD ) + (FGBOPAL_01_BUD_ADJT)) presu" + " ,
	 * SUM(fg.FGBOPAL_01_YTD_ACTV + fg.FGBOPAL_01_ENCUMB +
	 * fg.FGBOPAL_01_BUD_RSRV) gasto ," + " ROUND((SUM(fg.FGBOPAL_01_YTD_ACTV +
	 * fg.FGBOPAL_01_ENCUMB + fg.FGBOPAL_01_BUD_RSRV) / NULLIF(
	 * (SUM(fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) +
	 * SUM(fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)),0 )*100 ),2) porc " + " ,
	 * 1 mes," + " SUM(fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD +
	 * fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT ) - sum
	 * (fg.FGBOPAL_01_YTD_ACTV + fg.FGBOPAL_01_ENCUMB + fg.FGBOPAL_01_BUD_RSRV)
	 * saldo_mes ," + " SUM (fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_01_BUD_ADJT)
	 * pre_acum," //+ " SUM((fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) +
	 * (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)) presu_acum ," + " sum
	 * (fg.FGBOPAL_01_YTD_ACTV + fg.FGBOPAL_01_ENCUMB + fg.FGBOPAL_01_BUD_RSRV)
	 * gasto_acum ," + " ROUND((sum(fg.FGBOPAL_01_YTD_ACTV +
	 * fg.FGBOPAL_01_ENCUMB + fg.FGBOPAL_01_BUD_RSRV) / NULLIF(
	 * SUM(FGBOPAL_01_ADOPT_BUD + FGBOPAL_01_BUD_ADJT),0) ) *100,2 ) porc2, " + "
	 * (SUM(fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_01_BUD_ADJT) - SUM
	 * (FG.FGBOPAL_01_YTD_ACTV + FG.FGBOPAL_01_ENCUMB + FG.FGBOPAL_01_BUD_RSRV))
	 * total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff " + " where
	 * fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =
	 * fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code = 'FINANC' and
	 * fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE = '"+ org + "'
	 * AND fg.FGBOPAL_ACCT_CODE = '" + cuenta +"' " + " GROUP BY
	 * fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 1 " + " UNION ALL " + " SELECT
	 * fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT) -
	 * (fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) +
	 * (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_02_ADOPT_BUD ) + (FGBOPAL_02_BUD_ADJT)) presu" + " ,
	 * (SUM(FG.FGBOPAL_02_YTD_ACTV + FG.FGBOPAL_02_ENCUMB +
	 * FG.FGBOPAL_02_BUD_RSRV) - SUM(FG.FGBOPAL_01_YTD_ACTV +
	 * FG.FGBOPAL_01_ENCUMB + FG.FGBOPAL_01_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FGBOPAL_02_YTD_ACTV + FG.FGBOPAL_02_ENCUMB +
	 * FG.FGBOPAL_02_BUD_RSRV) - sum(FG.FGBOPAL_01_YTD_ACTV +
	 * FG.FGBOPAL_01_ENCUMB + FG.FGBOPAL_01_BUD_RSRV) ) / NULLIF( ((
	 * (SUM(fg.FGBOPAL_02_ADOPT_BUD) + SUM(fg.FGBOPAL_02_BUD_ADJT))) -
	 * ((SUM(fg.FGBOPAL_00_ADOPT_BUD) + SUM(fg.FGBOPAL_01_ADOPT_BUD))) +
	 * ((SUM(fg.FGBOPAL_00_BUD_ADJT) + SUM(fg.FGBOPAL_01_BUD_ADJT)))),0) *100
	 * ,2) porc" + " , 2 mes," + " ((SUM(FG.FGBOPAL_02_ADOPT_BUD +
	 * fg.FGBOPAL_02_BUD_ADJT) - SUM(FG.FGBOPAL_00_ADOPT_BUD +
	 * FG.FGBOPAL_01_ADOPT_BUD) + SUM(FG.FGBOPAL_00_BUD_ADJT +
	 * FG.FGBOPAL_01_BUD_ADJT)) - (SUM(fg.FGBOPAL_02_YTD_ACTV +
	 * fg.FGBOPAL_02_ENCUMB + fg.FGBOPAL_02_BUD_RSRV) -
	 * SUM(fg.FGBOPAL_01_YTD_ACTV + fg.FGBOPAL_01_ENCUMB +
	 * fg.FGBOPAL_01_BUD_RSRV))) saldo_mes ," + " SUM (fg.FGBOPAL_02_ADOPT_BUD +
	 * fg.FGBOPAL_02_BUD_ADJT) pre_acum," //+ " SUM((fg.FGBOPAL_02_ADOPT_BUD +
	 * fg.FGBOPAL_02_BUD_ADJT) - (fg.FGBOPAL_00_ADOPT_BUD +
	 * fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT +
	 * fg.FGBOPAL_01_BUD_ADJT)) pre_acum, " + " sum (fg.FGBOPAL_02_YTD_ACTV +
	 * fg.FGBOPAL_02_ENCUMB + fg.FGBOPAL_02_BUD_RSRV) gasto_acum ," + "
	 * ROUND((sum(fg. FGBOPAL_02_YTD_ACTV + fg.FGBOPAL_02_ENCUMB +
	 * fg.FGBOPAL_02_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_02_ADOPT_BUD +
	 * fg.FGBOPAL_02_BUD_ADJT),0)) ,2 ) *100 porc2 ," + " (SUM
	 * (fg.FGBOPAL_02_ADOPT_BUD) + sum(fg.FGBOPAL_02_BUD_ADJT)) -
	 * sum(fg.FGBOPAL_02_YTD_ACTV + fg.FGBOPAL_02_ENCUMB +
	 * fg.FGBOPAL_02_BUD_RSRV) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "' AND fg.FGBOPAL_ACCT_CODE = '" + cuenta +"' " + " GROUP BY
	 * fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 2 " + " UNION ALL " + " SELECT
	 * fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT) - (
	 * fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_03_ADOPT_BUD ) + (FGBOPAL_03_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_03_YTD_ACTV + fg.FGBOPAL_03_ENCUMB +
	 * fg.FGBOPAL_03_BUD_RSRV) - sum(fg.FGBOPAL_02_YTD_ACTV +
	 * fg.FGBOPAL_02_ENCUMB + fg. FGBOPAL_02_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(fg.FGBOPAL_03_YTD_ACTV + fg.FGBOPAL_03_ENCUMB +
	 * fg.FGBOPAL_03_BUD_RSRV) - SUM(fg.FGBOPAL_02_YTD_ACTV +
	 * fg.FGBOPAL_02_ENCUMB + fg.FGBOPAL_02_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_03_ADOPT_BUD) + SUM(fg.FGBOPAL_03_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_02_ADOPT_BUD) + SUM(fg.FGBOPAL_02_BUD_ADJT))),0) *100 ,2)
	 * porc" + " , 3 mes," + " (sum(FGBOPAL_03_ADOPT_BUD + FGBOPAL_03_BUD_ADJT) -
	 * sum( FGBOPAL_02_ADOPT_BUD + FGBOPAL_02_BUD_ADJT))
	 * -(SUM(FGBOPAL_03_YTD_ACTV + FGBOPAL_03_ENCUMB + FGBOPAL_03_BUD_RSRV) -
	 * SUM(FGBOPAL_02_YTD_ACTV + FGBOPAL_02_ENCUMB + FGBOPAL_02_BUD_RSRV))
	 * saldo_mes ," + " SUM((FGBOPAL_03_ADOPT_BUD ) + (FGBOPAL_03_BUD_ADJT))
	 * pre_acum," + " sum (fg.FGBOPAL_03_YTD_ACTV + fg.FGBOPAL_03_ENCUMB +
	 * fg.FGBOPAL_03_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(fg.FGBOPAL_03_YTD_ACTV + FG.FGBOPAL_03_ENCUMB +
	 * FG.FGBOPAL_03_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_03_ADOPT_BUD +
	 * fg.FGBOPAL_03_BUD_ADJT),0)) *100 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT) -
	 * SUM(fg.FGBOPAL_03_YTD_ACTV + FG.FGBOPAL_03_ENCUMB +
	 * FG.FGBOPAL_03_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "' AND fg.FGBOPAL_ACCT_CODE = '" + cuenta +"' " + " GROUP BY
	 * fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 3 " + " UNION ALL " + " SELECT
	 * fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT) - (
	 * fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_04_ADOPT_BUD ) + (FGBOPAL_04_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_04_YTD_ACTV + fg.FGBOPAL_04_ENCUMB +
	 * fg.FGBOPAL_04_BUD_RSRV) - sum(fg.FGBOPAL_03_YTD_ACTV +
	 * fg.FGBOPAL_03_ENCUMB + fg.FGBOPAL_03_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(fg.FGBOPAL_04_YTD_ACTV + fg.FGBOPAL_04_ENCUMB +
	 * fg.FGBOPAL_04_BUD_RSRV) - SUM(fg.FGBOPAL_03_YTD_ACTV +
	 * fg.FGBOPAL_03_ENCUMB + fg.FGBOPAL_03_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_04_ADOPT_BUD) + SUM(fg.FGBOPAL_04_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_03_ADOPT_BUD) + SUM(fg.FGBOPAL_03_BUD_ADJT))),0) *100 ,2)
	 * porc" + " , 4 mes," + " (SUM(FG.FGBOPAL_04_ADOPT_BUD) +
	 * SUM(FG.FGBOPAL_04_BUD_ADJT))-(SUM(FG.FGBOPAL_03_ADOPT_BUD +
	 * FG.FGBOPAL_03_BUD_ADJT))- (sum(fg.FGBOPAL_04_YTD_ACTV +
	 * fg.FGBOPAL_04_ENCUMB + fg.FGBOPAL_04_BUD_RSRV) -
	 * SUM(fg.FGBOPAL_03_YTD_ACTV + fg.FGBOPAL_03_ENCUMB +
	 * fg.FGBOPAL_03_BUD_RSRV)) saldo_mes ," + " SUM((FGBOPAL_04_ADOPT_BUD ) +
	 * (FGBOPAL_04_BUD_ADJT)) pre_acum," + " sum (fg.FGBOPAL_04_YTD_ACTV +
	 * fg.FGBOPAL_04_ENCUMB + fg.FGBOPAL_04_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(fg.FGBOPAL_04_YTD_ACTV + fg.FGBOPAL_04_ENCUMB +
	 * fg.FGBOPAL_04_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_04_ADOPT_BUD +
	 * fg.FGBOPAL_04_BUD_ADJT),0)) *100 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT) -
	 * SUM(fg.FGBOPAL_04_YTD_ACTV + fg.FGBOPAL_04_ENCUMB +
	 * fg.FGBOPAL_04_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "' AND fg.FGBOPAL_ACCT_CODE = '" + cuenta +"' " + " GROUP BY
	 * fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 4 " + " UNION ALL " + " SELECT
	 * fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT) - (
	 * fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_05_ADOPT_BUD ) + (FGBOPAL_05_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_05_YTD_ACTV + fg.FGBOPAL_05_ENCUMB +
	 * fg.FGBOPAL_05_BUD_RSRV) - sum(fg.FGBOPAL_04_YTD_ACTV +
	 * fg.FGBOPAL_04_ENCUMB + fg.FGBOPAL_04_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FG.FGBOPAL_05_YTD_ACTV + FG.FGBOPAL_05_ENCUMB +
	 * FG.FGBOPAL_05_BUD_RSRV) - SUM(FG.FGBOPAL_04_YTD_ACTV +
	 * FG.FGBOPAL_04_ENCUMB + FG.FGBOPAL_04_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_05_ADOPT_BUD) + SUM(fg.FGBOPAL_05_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_04_ADOPT_BUD) + SUM(fg.FGBOPAL_04_BUD_ADJT))),0) *100 ,2)
	 * porc" + " , 5 mes," + " (sum(FG.FGBOPAL_05_ADOPT_BUD +
	 * FG.FGBOPAL_05_BUD_ADJT) - SUM( FG.FGBOPAL_04_ADOPT_BUD +
	 * FG.FGBOPAL_04_BUD_ADJT) - (SUM(FG.FGBOPAL_05_YTD_ACTV +
	 * FG.FGBOPAL_05_ENCUMB + FG.FGBOPAL_05_BUD_RSRV) -
	 * SUM(FG.FGBOPAL_04_YTD_ACTV + FG.FGBOPAL_04_ENCUMB +
	 * FG.FGBOPAL_04_BUD_RSRV))) saldo_mes ," + " SUM((FGBOPAL_05_ADOPT_BUD ) +
	 * (FGBOPAL_05_BUD_ADJT)) pre_acum," + " sum (FG.FGBOPAL_05_YTD_ACTV +
	 * FG.FGBOPAL_05_ENCUMB + FG.FGBOPAL_05_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(FG.FGBOPAL_05_YTD_ACTV + FG.FGBOPAL_05_ENCUMB +
	 * FG.FGBOPAL_05_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_05_ADOPT_BUD +
	 * fg.FGBOPAL_05_BUD_ADJT),0)) *100 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT) -
	 * SUM(FG.FGBOPAL_05_YTD_ACTV + FG.FGBOPAL_05_ENCUMB +
	 * FG.FGBOPAL_05_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "' AND fg.FGBOPAL_ACCT_CODE = '" + cuenta +"' " + " GROUP BY
	 * fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,5 " + " UNION ALL " + " SELECT
	 * fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT) - (
	 * fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_06_ADOPT_BUD ) + (FGBOPAL_06_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_06_YTD_ACTV + fg.FGBOPAL_06_ENCUMB +
	 * fg.FGBOPAL_06_BUD_RSRV) - sum(fg.FGBOPAL_05_YTD_ACTV +
	 * fg.FGBOPAL_05_ENCUMB + fg.FGBOPAL_05_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FG.FGBOPAL_06_YTD_ACTV + FG.FGBOPAL_06_ENCUMB +
	 * FG.FGBOPAL_06_BUD_RSRV) - SUM(FG.FGBOPAL_05_YTD_ACTV +
	 * FG.FGBOPAL_05_ENCUMB + FG.FGBOPAL_05_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_06_ADOPT_BUD) + SUM(fg.FGBOPAL_06_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_05_ADOPT_BUD) + SUM(fg.FGBOPAL_05_BUD_ADJT))),0) *100 ,2)
	 * porc" + " , 6 mes," + " (sum(FG.FGBOPAL_06_ADOPT_BUD +
	 * FG.FGBOPAL_06_BUD_ADJT) - SUM( FG.FGBOPAL_05_ADOPT_BUD +
	 * FG.FGBOPAL_05_BUD_ADJT) - (SUM(FG.FGBOPAL_06_YTD_ACTV +
	 * FG.FGBOPAL_06_ENCUMB + FG.FGBOPAL_06_BUD_RSRV) -
	 * SUM(FG.FGBOPAL_05_YTD_ACTV + FG.FGBOPAL_05_ENCUMB +
	 * FG.FGBOPAL_05_BUD_RSRV))) saldo_mes ," + " SUM((FGBOPAL_06_ADOPT_BUD ) +
	 * (FGBOPAL_06_BUD_ADJT)) pre_acum," + " sum (FG.FGBOPAL_06_YTD_ACTV +
	 * FG.FGBOPAL_06_ENCUMB + FG.FGBOPAL_06_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(FG.FGBOPAL_06_YTD_ACTV + FG.FGBOPAL_06_ENCUMB +
	 * FG.FGBOPAL_06_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_06_ADOPT_BUD +
	 * fg.FGBOPAL_06_BUD_ADJT),0)) *100 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT) -
	 * SUM(FG.FGBOPAL_06_YTD_ACTV + FG.FGBOPAL_06_ENCUMB +
	 * FG.FGBOPAL_06_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "' AND fg.FGBOPAL_ACCT_CODE = '" + cuenta +"' " + " GROUP BY
	 * fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,6" + " UNION ALL " + " SELECT
	 * fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT) - (
	 * fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_07_ADOPT_BUD ) + (FGBOPAL_07_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_07_YTD_ACTV + fg.FGBOPAL_07_ENCUMB +
	 * fg.FGBOPAL_07_BUD_RSRV) - sum(fg.FGBOPAL_06_YTD_ACTV +
	 * fg.FGBOPAL_06_ENCUMB + fg.FGBOPAL_06_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FG.FGBOPAL_07_YTD_ACTV + FG.FGBOPAL_07_ENCUMB +
	 * FG.FGBOPAL_07_BUD_RSRV) - SUM(FG.FGBOPAL_06_YTD_ACTV +
	 * FG.FGBOPAL_06_ENCUMB + FG.FGBOPAL_06_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_07_ADOPT_BUD) + SUM(fg.FGBOPAL_07_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_06_ADOPT_BUD) + SUM(fg.FGBOPAL_06_BUD_ADJT))),0) *100 ,2)
	 * porc" + " , 7 mes," + " (sum(FG.FGBOPAL_07_ADOPT_BUD +
	 * FG.FGBOPAL_07_BUD_ADJT) - SUM( FG.FGBOPAL_06_ADOPT_BUD +
	 * FG.FGBOPAL_06_BUD_ADJT) - (SUM(FG.FGBOPAL_07_YTD_ACTV +
	 * FG.FGBOPAL_07_ENCUMB + FG.FGBOPAL_07_BUD_RSRV) -
	 * SUM(fg.FGBOPAL_06_YTD_ACTV + fg.FGBOPAL_06_ENCUMB +
	 * FG.FGBOPAL_06_BUD_RSRV))) saldo_mes ," + " SUM((FGBOPAL_07_ADOPT_BUD ) +
	 * (FGBOPAL_07_BUD_ADJT)) pre_acum," + " sum (FG.FGBOPAL_07_YTD_ACTV +
	 * FG.FGBOPAL_07_ENCUMB + FG.FGBOPAL_07_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(FG.FGBOPAL_07_YTD_ACTV + FG.FGBOPAL_07_ENCUMB +
	 * FG.FGBOPAL_07_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_07_ADOPT_BUD +
	 * fg.FGBOPAL_07_BUD_ADJT),0)) *100 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT) -
	 * SUM(FG.FGBOPAL_07_YTD_ACTV + FG.FGBOPAL_07_ENCUMB +
	 * FG.FGBOPAL_07_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "' AND fg.FGBOPAL_ACCT_CODE = '" + cuenta +"' " + " GROUP BY
	 * fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,7" + " UNION ALL " + " SELECT
	 * fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT) - (
	 * fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_08_ADOPT_BUD ) + (FGBOPAL_08_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_08_YTD_ACTV + fg.FGBOPAL_08_ENCUMB +
	 * fg.FGBOPAL_08_BUD_RSRV) - sum(fg.FGBOPAL_07_YTD_ACTV +
	 * fg.FGBOPAL_07_ENCUMB + fg.FGBOPAL_07_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FG.FGBOPAL_08_YTD_ACTV + FG.FGBOPAL_08_ENCUMB +
	 * FG.FGBOPAL_08_BUD_RSRV) - SUM(FG.FGBOPAL_07_YTD_ACTV +
	 * FG.FGBOPAL_07_ENCUMB + FG.FGBOPAL_07_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_08_ADOPT_BUD) + SUM(fg.FGBOPAL_08_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_07_ADOPT_BUD) + SUM(fg.FGBOPAL_07_BUD_ADJT))),0) *100 ,2)
	 * porc" + " , 8 mes," + " (sum(FG.FGBOPAL_08_ADOPT_BUD +
	 * FG.FGBOPAL_08_BUD_ADJT) - SUM( FG.FGBOPAL_07_ADOPT_BUD +
	 * FG.FGBOPAL_07_BUD_ADJT) - (SUM(FG.FGBOPAL_08_YTD_ACTV +
	 * FG.FGBOPAL_08_ENCUMB + FG.FGBOPAL_08_BUD_RSRV) -
	 * SUM(FG.FGBOPAL_07_YTD_ACTV + FG.FGBOPAL_07_ENCUMB +
	 * FG.FGBOPAL_07_BUD_RSRV))) saldo_mes ," + " SUM((FGBOPAL_08_ADOPT_BUD ) +
	 * (FGBOPAL_08_BUD_ADJT)) pre_acum," + " sum (FG.FGBOPAL_08_YTD_ACTV +
	 * FG.FGBOPAL_08_ENCUMB + FG.FGBOPAL_08_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(FG.FGBOPAL_08_YTD_ACTV + FG.FGBOPAL_08_ENCUMB +
	 * FG.FGBOPAL_08_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_08_ADOPT_BUD +
	 * fg.FGBOPAL_08_BUD_ADJT),0)) *100 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT) -
	 * SUM(FG.FGBOPAL_08_YTD_ACTV + FG.FGBOPAL_08_ENCUMB +
	 * FG.FGBOPAL_08_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "' AND fg.FGBOPAL_ACCT_CODE = '" + cuenta +"' " + " GROUP BY
	 * fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,8" + " UNION ALL " + " SELECT
	 * fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT) - (
	 * fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_09_ADOPT_BUD ) + (FGBOPAL_09_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_09_YTD_ACTV + fg.FGBOPAL_09_ENCUMB +
	 * fg.FGBOPAL_09_BUD_RSRV) - sum(fg.FGBOPAL_08_YTD_ACTV +
	 * fg.FGBOPAL_08_ENCUMB + fg.FGBOPAL_08_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FG.FGBOPAL_09_YTD_ACTV + FG.FGBOPAL_09_ENCUMB +
	 * FG.FGBOPAL_09_BUD_RSRV) - SUM(FG.FGBOPAL_08_YTD_ACTV +
	 * FG.FGBOPAL_08_ENCUMB + FG.FGBOPAL_08_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_09_ADOPT_BUD) + SUM(fg.FGBOPAL_09_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_08_ADOPT_BUD) + SUM(fg.FGBOPAL_08_BUD_ADJT))),0) *100 ,2)
	 * porc" + " , 9 mes," + " (sum(FG.FGBOPAL_09_ADOPT_BUD +
	 * FG.FGBOPAL_09_BUD_ADJT) - SUM( FG.FGBOPAL_08_ADOPT_BUD +
	 * FG.FGBOPAL_08_BUD_ADJT) - (SUM(FG.FGBOPAL_09_YTD_ACTV +
	 * FG.FGBOPAL_09_ENCUMB + FG.FGBOPAL_09_BUD_RSRV) -
	 * SUM(FG.FGBOPAL_08_YTD_ACTV + FG.FGBOPAL_08_ENCUMB +
	 * FG.FGBOPAL_08_BUD_RSRV))) saldo_mes ," + " SUM((FGBOPAL_09_ADOPT_BUD ) +
	 * (FGBOPAL_09_BUD_ADJT)) pre_acum," + " sum (FG.FGBOPAL_09_YTD_ACTV +
	 * FG.FGBOPAL_09_ENCUMB + FG.FGBOPAL_09_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(FG.FGBOPAL_09_YTD_ACTV + FG.FGBOPAL_09_ENCUMB +
	 * FG.FGBOPAL_09_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_09_ADOPT_BUD +
	 * fg.FGBOPAL_09_BUD_ADJT),0)) *100 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT) -
	 * SUM(FG.FGBOPAL_09_YTD_ACTV + FG.FGBOPAL_09_ENCUMB +
	 * FG.FGBOPAL_09_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "' AND fg.FGBOPAL_ACCT_CODE = '" + cuenta +"' " + " GROUP BY
	 * fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,9" + " UNION ALL " + " SELECT
	 * fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT) - (
	 * fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_10_ADOPT_BUD ) + (FGBOPAL_10_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_10_YTD_ACTV + fg.FGBOPAL_10_ENCUMB +
	 * fg.FGBOPAL_10_BUD_RSRV) - sum(fg.FGBOPAL_09_YTD_ACTV +
	 * fg.FGBOPAL_09_ENCUMB + fg.FGBOPAL_09_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FG.FGBOPAL_10_YTD_ACTV + FG.FGBOPAL_10_ENCUMB +
	 * FG.FGBOPAL_10_BUD_RSRV) - SUM(FG.FGBOPAL_09_YTD_ACTV +
	 * FG.FGBOPAL_09_ENCUMB + FG.FGBOPAL_09_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_10_ADOPT_BUD) + SUM(fg.FGBOPAL_10_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_09_ADOPT_BUD) + SUM(fg.FGBOPAL_09_BUD_ADJT))),0) *100 ,2)
	 * porc" + " , 10 mes," + " (sum(FG.FGBOPAL_10_ADOPT_BUD +
	 * FG.FGBOPAL_10_BUD_ADJT) - SUM( FG.FGBOPAL_09_ADOPT_BUD +
	 * FG.FGBOPAL_09_BUD_ADJT) - (SUM(FG.FGBOPAL_10_YTD_ACTV +
	 * FG.FGBOPAL_10_ENCUMB + FG.FGBOPAL_10_BUD_RSRV) -
	 * SUM(FG.FGBOPAL_09_YTD_ACTV + FG.FGBOPAL_09_ENCUMB +
	 * FG.FGBOPAL_09_BUD_RSRV))) saldo_mes ," + " SUM((FGBOPAL_10_ADOPT_BUD ) +
	 * (FGBOPAL_10_BUD_ADJT)) pre_acum," + " sum (FG.FGBOPAL_10_YTD_ACTV +
	 * FG.FGBOPAL_10_ENCUMB + FG.FGBOPAL_10_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(FG.FGBOPAL_10_YTD_ACTV + FG.FGBOPAL_10_ENCUMB +
	 * FG.FGBOPAL_10_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_10_ADOPT_BUD +
	 * fg.FGBOPAL_10_BUD_ADJT),0)) *100 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT) -
	 * SUM(FG.FGBOPAL_10_YTD_ACTV + FG.FGBOPAL_10_ENCUMB +
	 * FG.FGBOPAL_10_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "' AND fg.FGBOPAL_ACCT_CODE = '" + cuenta +"' " + " GROUP BY
	 * fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,10" + " UNION ALL " + " SELECT
	 * fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT) - (
	 * fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_11_ADOPT_BUD ) + (FGBOPAL_11_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_11_YTD_ACTV + fg.FGBOPAL_11_ENCUMB +
	 * fg.FGBOPAL_11_BUD_RSRV) - sum(fg.FGBOPAL_10_YTD_ACTV +
	 * fg.FGBOPAL_10_ENCUMB + fg.FGBOPAL_10_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FG.FGBOPAL_11_YTD_ACTV + FG.FGBOPAL_11_ENCUMB +
	 * FG.FGBOPAL_11_BUD_RSRV) - SUM(FG.FGBOPAL_10_YTD_ACTV +
	 * FG.FGBOPAL_10_ENCUMB + FG.FGBOPAL_10_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_11_ADOPT_BUD) + SUM(fg.FGBOPAL_11_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_10_ADOPT_BUD) + SUM(fg.FGBOPAL_10_BUD_ADJT))),0) *110 ,2)
	 * porc" + " , 11 mes," + " (sum(FG.FGBOPAL_11_ADOPT_BUD +
	 * FG.FGBOPAL_11_BUD_ADJT) - SUM( FG.FGBOPAL_10_ADOPT_BUD +
	 * FG.FGBOPAL_10_BUD_ADJT) - (SUM(FG.FGBOPAL_11_YTD_ACTV +
	 * FG.FGBOPAL_11_ENCUMB + FG.FGBOPAL_11_BUD_RSRV) -
	 * SUM(FG.FGBOPAL_10_YTD_ACTV + FG.FGBOPAL_10_ENCUMB +
	 * FG.FGBOPAL_10_BUD_RSRV))) saldo_mes ," + " SUM((FGBOPAL_11_ADOPT_BUD ) +
	 * (FGBOPAL_11_BUD_ADJT)) pre_acum," + " sum (FG.FGBOPAL_11_YTD_ACTV +
	 * FG.FGBOPAL_11_ENCUMB + FG.FGBOPAL_11_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(FG.FGBOPAL_11_YTD_ACTV + FG.FGBOPAL_11_ENCUMB +
	 * FG.FGBOPAL_11_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_11_ADOPT_BUD +
	 * fg.FGBOPAL_11_BUD_ADJT),0)) *110 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT) -
	 * SUM(FG.FGBOPAL_11_YTD_ACTV + FG.FGBOPAL_11_ENCUMB +
	 * FG.FGBOPAL_11_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "' AND fg.FGBOPAL_ACCT_CODE = '" + cuenta +"' " + " GROUP BY
	 * fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,11" + " UNION ALL " + " SELECT
	 * fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , " + "
	 * SUM((fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT) - (
	 * fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT)) presu" //+ "
	 * SUM((FGBOPAL_12_ADOPT_BUD ) + (FGBOPAL_12_BUD_ADJT)) presu" + " ,
	 * (SUM(fg.FGBOPAL_12_YTD_ACTV + fg.FGBOPAL_12_ENCUMB +
	 * fg.FGBOPAL_12_BUD_RSRV) - sum(fg.FGBOPAL_11_YTD_ACTV +
	 * fg.FGBOPAL_11_ENCUMB + fg.FGBOPAL_11_BUD_RSRV) ) gasto ," + "
	 * ROUND((SUM(FG.FGBOPAL_12_YTD_ACTV + FG.FGBOPAL_12_ENCUMB +
	 * FG.FGBOPAL_12_BUD_RSRV) - SUM(FG.FGBOPAL_11_YTD_ACTV +
	 * FG.FGBOPAL_11_ENCUMB + FG.FGBOPAL_11_BUD_RSRV))/ NULLIF(
	 * ((SUM(fg.FGBOPAL_12_ADOPT_BUD) + SUM(fg.FGBOPAL_12_BUD_ADJT)) -
	 * (SUM(fg.FGBOPAL_11_ADOPT_BUD) + SUM(fg.FGBOPAL_11_BUD_ADJT))),0) *120 ,2)
	 * porc" + " , 12 mes," + " (sum(FG.FGBOPAL_12_ADOPT_BUD +
	 * FG.FGBOPAL_12_BUD_ADJT) - SUM( FG.FGBOPAL_11_ADOPT_BUD +
	 * FG.FGBOPAL_11_BUD_ADJT) - (SUM(FG.FGBOPAL_12_YTD_ACTV +
	 * FG.FGBOPAL_12_ENCUMB + FG.FGBOPAL_12_BUD_RSRV) -
	 * SUM(FG.FGBOPAL_11_YTD_ACTV + FG.FGBOPAL_11_ENCUMB +
	 * FG.FGBOPAL_11_BUD_RSRV))) saldo_mes ," + " SUM((FGBOPAL_12_ADOPT_BUD ) +
	 * (FGBOPAL_12_BUD_ADJT)) pre_acum," + " sum (FG.FGBOPAL_12_YTD_ACTV +
	 * FG.FGBOPAL_12_ENCUMB + FG.FGBOPAL_12_BUD_RSRV) gasto_acum ," + "
	 * ROUND((SUM(FG.FGBOPAL_12_YTD_ACTV + FG.FGBOPAL_12_ENCUMB +
	 * FG.FGBOPAL_12_BUD_RSRV) / NULLIF( SUM(fg.FGBOPAL_12_ADOPT_BUD +
	 * fg.FGBOPAL_12_BUD_ADJT),0)) *120 ,2 ) porc2 ," + "
	 * (SUM(fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT) -
	 * SUM(FG.FGBOPAL_12_YTD_ACTV + FG.FGBOPAL_12_ENCUMB +
	 * FG.FGBOPAL_12_BUD_RSRV)) total" + " FROM FGBOPAL fg ,ftvorgn ft , ftvfund
	 * ff " + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and
	 * ff.ftvfund_fund_code = fg.FGBOPAL_fund_code and fg.fgbopal_ledc_code =
	 * 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"+ a�o + "' and fg.FGBOPAL_ORGN_CODE =
	 * '"+ org + "' AND fg.FGBOPAL_ACCT_CODE = '" + cuenta +"' " + " GROUP BY
	 * fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,12" ; }
	 * 
	 * //System.out.println("disponibilidad mensual " + sql); sent =
	 * con.conexion().prepareStatement(sql); // sent.setString(1,rutnumdv); res =
	 * sent.executeQuery(); long acumulado = 0 ; long gastos = 0 ; while
	 * (res.next()){ //String aux = res.getString(1); Pres18POJO ss = new
	 * Pres18POJO(); ss.setCoduni(res.getString(1)); // cod cuenta
	 * ss.setDesuni(res.getString(2)); //titulo cta
	 * ss.setPresum(res.getLong(3)); // presupuesto
	 *  // acumulado = acumulado + res.getLong(3); // ss.setPresac(acumulado);
	 * ss.setUsadom(res.getLong(4)); // gastos
	 *  /* gastos = gastos + res.getLong(4);
	 * 
	 * if (gastos > 0) { ss.setIddigi("S");
	 *  } else { ss.setIddigi("N");
	 *  } * / //ss.setUsadom(res.getLong(4)); // gastos
	 * ss.setPorc1(res.getFloat(5)); // porc1 ss.setNummes(res.getInt(6)); //
	 * mes ss.setSaldoMes(res.getLong(7)); //saldo mes
	 * ss.setPresac(res.getLong(8)); //pres acumulado
	 * ss.setUsadac(res.getLong(9)); // gasto acumulado
	 * ss.setPorc2(res.getFloat(10)); // porc2 ss.setTotal(res.getLong(11)); //
	 * total
	 * 
	 * if (sw > 0) { ss.setIddigi("S");
	 *  } else { ss.setIddigi("N");
	 *  } lista.add(ss);
	 *  } res.close(); sent.close(); con.close(); } catch (SQLException e){
	 * //System.out.println("Error sipB getCuentasDetalles: " + e.getMessage()); }
	 * return lista; }
	 */

	public Collection<Pres18POJO> getRemuneracion(String org, int a�oIN,
			int mes, String cuenta) {

		Calendar fecha = Calendar.getInstance();
		int a�o = a�oIN;
		String a�osql = String.valueOf(a�o);
		a�osql = a�osql.substring(2);
		a�o = 0;
		a�o = Integer.parseInt(a�osql);
		
		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;
		int sw = 0;
		List<Pres18POJO> lista = new ArrayList<Pres18POJO>();

		try {
			String sql = "";
			if (cuenta.equals("-1")) { // Consulta general (todos los
										// estamentos)
				sql = ""
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ " SUM((FGBOPAL_00_ADOPT_BUD ) + (FGBOPAL_00_BUD_ADJT)) presu"
						+ " , SUM(fg.FGBOPAL_00_YTD_ACTV) gasto  ,"
						+ " ROUND(SUM(FG.FGBOPAL_01_YTD_ACTV) / NULLIF( (SUM(FG.FGBOPAL_00_ADOPT_BUD + FG.FGBOPAL_01_ADOPT_BUD) + SUM(FG.FGBOPAL_00_BUD_ADJT + FG.FGBOPAL_01_BUD_ADJT)),0 )*100,2 )  porc   "
						+ " , 0 mes,"
						+ " SUM((FGBOPAL_00_ADOPT_BUD ) + (FGBOPAL_00_BUD_ADJT ) - FGBOPAL_00_YTD_ACTV )saldo_mes ,"
						+ " SUM (FGBOPAL_00_ADOPT_BUD + FGBOPAL_00_BUD_ADJT) pre_acum,"
						+ " sum (FGBOPAL_00_YTD_ACTV) gasto_acum ,"
						+ " round(SUM(fg.FGBOPAL_01_YTD_ACTV) / NULLIF( (SUM(fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_01_BUD_ADJT) *100),0),2) porc2 ,"
						+ " SUM((FGBOPAL_00_ADOPT_BUD ) - FGBOPAL_00_YTD_ACTV) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff   "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'  and fg.FGBOPAL_ACCT_CODE IN ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006') "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 0   "
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ " SUM((fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)) presu"
						// + " SUM((FGBOPAL_01_ADOPT_BUD ) +
						// (FGBOPAL_01_BUD_ADJT)) presu"
						+ " , SUM(fg.FGBOPAL_01_YTD_ACTV) gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_01_YTD_ACTV) / NULLIF( (SUM(fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + SUM(fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)),0 )*100 ),2) porc "
						+ " , 1 mes,"
						+ " SUM((fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT) - fg.FGBOPAL_01_YTD_ACTV )saldo_mes ,"
						+ " SUM (fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_01_BUD_ADJT) pre_acum,"
						// + " SUM((FGBOPAL_01_ADOPT_BUD ) +
						// (FGBOPAL_01_BUD_ADJT)) presu_acum ,"
						+ " sum (fg.FGBOPAL_01_YTD_ACTV) gasto_acum ,"
						+ " ROUND((sum(FGBOPAL_01_YTD_ACTV) / NULLIF( SUM(FGBOPAL_01_ADOPT_BUD + FGBOPAL_01_BUD_ADJT),0) ) *100,2 ) porc2, "
						+ " SUM((fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_01_BUD_ADJT) - fg.FGBOPAL_01_YTD_ACTV) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'  and fg.FGBOPAL_ACCT_CODE IN ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006') "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 1   "
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT) - (fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)) presu"
						// + " SUM((FGBOPAL_02_ADOPT_BUD ) +
						// (FGBOPAL_02_BUD_ADJT)) presu"
						+ " , (SUM(FGBOPAL_02_YTD_ACTV) - SUM(FGBOPAL_01_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_02_YTD_ACTV) - SUM(fg.FGBOPAL_01_YTD_ACTV) ) / NULLIF( ((  (SUM(fg.FGBOPAL_02_ADOPT_BUD) + SUM(fg.FGBOPAL_02_BUD_ADJT))) - ((SUM(fg.FGBOPAL_00_ADOPT_BUD) + SUM(fg.FGBOPAL_01_ADOPT_BUD))) + ((SUM(fg.FGBOPAL_00_BUD_ADJT) + SUM(fg.FGBOPAL_01_BUD_ADJT)))),0) *100  ,2) porc"
						+ " , 2 mes,"
						// + " SUM((FG.FGBOPAL_02_ADOPT_BUD +
						// fg.FGBOPAL_02_BUD_ADJT) - (FG.FGBOPAL_00_ADOPT_BUD +
						// FG.FGBOPAL_01_ADOPT_BUD) + (FG.FGBOPAL_00_BUD_ADJT +
						// FG.FGBOPAL_01_BUD_ADJT) - FG.FGBOPAL_02_YTD_ACTV)
						// saldo_mes ,"
						+ " SUM (fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT) pre_acum,"
						+ "  SUM((fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT) - (fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)) pre_acum, "
						+ " sum (fg.FGBOPAL_02_YTD_ACTV) gasto_acum ,"
						+ " ROUND((sum(fg.FGBOPAL_02_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT),0)) ,2 ) *100  porc2 ,"
						+ "   (SUM (fg.FGBOPAL_02_ADOPT_BUD) + sum(fg.FGBOPAL_02_BUD_ADJT)) - sum(fg.FGBOPAL_02_YTD_ACTV) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'  and fg.FGBOPAL_ACCT_CODE IN ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006') "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 2 "
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT) - ( fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_03_ADOPT_BUD ) +
						// (FGBOPAL_03_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_03_YTD_ACTV) - sum(fg.FGBOPAL_02_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_03_YTD_ACTV) - SUM(fg.FGBOPAL_02_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_03_ADOPT_BUD) + SUM(fg.FGBOPAL_03_BUD_ADJT)) - (SUM(fg.FGBOPAL_02_ADOPT_BUD) + SUM(fg.FGBOPAL_02_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 3 mes,"
						+ " (SUM(FG.FGBOPAL_03_ADOPT_BUD) + SUM(FG.FGBOPAL_03_BUD_ADJT))-(SUM(FG.FGBOPAL_02_ADOPT_BUD + FG.FGBOPAL_02_BUD_ADJT))- sum(FG.FGBOPAL_03_YTD_ACTV) saldo_mes  ,"
						+ "  SUM(fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT)  pre_acum,"
						+ " sum (fg.FGBOPAL_03_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_03_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT) - SUM(fg.FGBOPAL_03_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'  and fg.FGBOPAL_ACCT_CODE IN ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006') "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 3  "
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT) - ( fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_04_ADOPT_BUD ) +
						// (FGBOPAL_04_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_04_YTD_ACTV) - sum(fg.FGBOPAL_03_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_04_YTD_ACTV) - SUM(fg.FGBOPAL_03_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_04_ADOPT_BUD) + SUM(fg.FGBOPAL_04_BUD_ADJT)) - (SUM(fg.FGBOPAL_03_ADOPT_BUD) + SUM(fg.FGBOPAL_03_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 4 mes,"
						+ " (SUM(FG.FGBOPAL_04_ADOPT_BUD) + SUM(FG.FGBOPAL_04_BUD_ADJT))-(SUM(FG.FGBOPAL_03_ADOPT_BUD + FG.FGBOPAL_03_BUD_ADJT))- sum(FG.FGBOPAL_04_YTD_ACTV) saldo_mes  ,"
						+ "  SUM(fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT)  pre_acum,"
						+ " sum (fg.FGBOPAL_04_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_04_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT) - SUM(fg.FGBOPAL_04_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'  and fg.FGBOPAL_ACCT_CODE IN ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006') "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 4  "
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT) - ( fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_05_ADOPT_BUD ) +
						// (FGBOPAL_05_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_05_YTD_ACTV) - sum(fg.FGBOPAL_04_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_05_YTD_ACTV) - SUM(fg.FGBOPAL_04_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_05_ADOPT_BUD) + SUM(fg.FGBOPAL_05_BUD_ADJT)) - (SUM(fg.FGBOPAL_04_ADOPT_BUD) + SUM(fg.FGBOPAL_04_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 5 mes,"
						+ " (SUM(FG.FGBOPAL_05_ADOPT_BUD) + SUM(FG.FGBOPAL_05_BUD_ADJT))-(SUM(FG.FGBOPAL_04_ADOPT_BUD + FG.FGBOPAL_04_BUD_ADJT))- sum(FG.FGBOPAL_05_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_05_ADOPT_BUD ) + (FGBOPAL_05_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_05_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_05_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT) - SUM(fg.FGBOPAL_05_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'  and fg.FGBOPAL_ACCT_CODE IN ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006') "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,5 "
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT) - ( fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_06_ADOPT_BUD ) +
						// (FGBOPAL_06_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_06_YTD_ACTV) - sum(fg.FGBOPAL_05_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_06_YTD_ACTV) - SUM(fg.FGBOPAL_05_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_06_ADOPT_BUD) + SUM(fg.FGBOPAL_06_BUD_ADJT)) - (SUM(fg.FGBOPAL_05_ADOPT_BUD) + SUM(fg.FGBOPAL_05_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 6 mes,"
						+ " (SUM(FG.FGBOPAL_06_ADOPT_BUD) + SUM(FG.FGBOPAL_06_BUD_ADJT))-(SUM(FG.FGBOPAL_05_ADOPT_BUD + FG.FGBOPAL_05_BUD_ADJT))- sum(FG.FGBOPAL_06_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_06_ADOPT_BUD ) + (FGBOPAL_06_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_06_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_06_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT) - SUM(fg.FGBOPAL_06_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'  and fg.FGBOPAL_ACCT_CODE IN ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006') "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,6"
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT) - ( fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_07_ADOPT_BUD ) +
						// (FGBOPAL_07_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_07_YTD_ACTV) - sum(fg.FGBOPAL_06_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_07_YTD_ACTV) - SUM(fg.FGBOPAL_06_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_07_ADOPT_BUD) + SUM(fg.FGBOPAL_07_BUD_ADJT)) - (SUM(fg.FGBOPAL_06_ADOPT_BUD) + SUM(fg.FGBOPAL_06_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 7 mes,"
						+ " (SUM(FG.FGBOPAL_07_ADOPT_BUD) + SUM(FG.FGBOPAL_07_BUD_ADJT))-(SUM(FG.FGBOPAL_06_ADOPT_BUD + FG.FGBOPAL_06_BUD_ADJT))- sum(FG.FGBOPAL_07_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_07_ADOPT_BUD ) + (FGBOPAL_07_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_07_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_07_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT) - SUM(fg.FGBOPAL_07_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'  and fg.FGBOPAL_ACCT_CODE IN ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006') "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,7"
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT) - ( fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_08_ADOPT_BUD ) +
						// (FGBOPAL_08_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_08_YTD_ACTV) - sum(fg.FGBOPAL_07_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_08_YTD_ACTV) - SUM(fg.FGBOPAL_07_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_08_ADOPT_BUD) + SUM(fg.FGBOPAL_08_BUD_ADJT)) - (SUM(fg.FGBOPAL_07_ADOPT_BUD) + SUM(fg.FGBOPAL_07_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 8 mes,"
						+ " (SUM(FG.FGBOPAL_08_ADOPT_BUD) + SUM(FG.FGBOPAL_08_BUD_ADJT))-(SUM(FG.FGBOPAL_07_ADOPT_BUD + FG.FGBOPAL_07_BUD_ADJT))- sum(FG.FGBOPAL_08_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_08_ADOPT_BUD ) + (FGBOPAL_08_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_08_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_08_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT) - SUM(fg.FGBOPAL_08_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'  and fg.FGBOPAL_ACCT_CODE IN ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006') "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,8"
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT) - ( fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_09_ADOPT_BUD ) +
						// (FGBOPAL_09_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_09_YTD_ACTV) - sum(fg.FGBOPAL_08_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_09_YTD_ACTV) - SUM(fg.FGBOPAL_08_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_09_ADOPT_BUD) + SUM(fg.FGBOPAL_09_BUD_ADJT)) - (SUM(fg.FGBOPAL_08_ADOPT_BUD) + SUM(fg.FGBOPAL_08_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 9 mes,"
						+ " (SUM(FG.FGBOPAL_09_ADOPT_BUD) + SUM(FG.FGBOPAL_09_BUD_ADJT))-(SUM(FG.FGBOPAL_08_ADOPT_BUD + FG.FGBOPAL_08_BUD_ADJT))- sum(FG.FGBOPAL_09_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_09_ADOPT_BUD ) + (FGBOPAL_09_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_09_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_09_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT) - SUM(fg.FGBOPAL_09_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'  and fg.FGBOPAL_ACCT_CODE IN ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006') "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,9"
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT) - ( fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_10_ADOPT_BUD ) +
						// (FGBOPAL_10_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_10_YTD_ACTV) - sum(fg.FGBOPAL_09_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_10_YTD_ACTV) - SUM(fg.FGBOPAL_09_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_10_ADOPT_BUD) + SUM(fg.FGBOPAL_10_BUD_ADJT)) - (SUM(fg.FGBOPAL_09_ADOPT_BUD) + SUM(fg.FGBOPAL_09_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 10 mes,"
						+ " (SUM(FG.FGBOPAL_10_ADOPT_BUD) + SUM(FG.FGBOPAL_10_BUD_ADJT))-(SUM(FG.FGBOPAL_09_ADOPT_BUD + FG.FGBOPAL_09_BUD_ADJT))- sum(FG.FGBOPAL_10_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_10_ADOPT_BUD ) + (FGBOPAL_10_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_10_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_10_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT) - SUM(fg.FGBOPAL_10_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'  and fg.FGBOPAL_ACCT_CODE IN ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006') "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,10"
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT) - ( fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_11_ADOPT_BUD ) +
						// (FGBOPAL_11_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_11_YTD_ACTV) - sum(fg.FGBOPAL_10_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_11_YTD_ACTV) - SUM(fg.FGBOPAL_10_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_11_ADOPT_BUD) + SUM(fg.FGBOPAL_11_BUD_ADJT)) - (SUM(fg.FGBOPAL_10_ADOPT_BUD) + SUM(fg.FGBOPAL_10_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 11 mes,"
						+ " (SUM(FG.FGBOPAL_11_ADOPT_BUD) + SUM(FG.FGBOPAL_11_BUD_ADJT))-(SUM(FG.FGBOPAL_10_ADOPT_BUD + FG.FGBOPAL_10_BUD_ADJT))- sum(FG.FGBOPAL_11_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_11_ADOPT_BUD ) + (FGBOPAL_11_BUD_ADJT))  pre_acum,"
						+ " sum (fg.FGBOPAL_11_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_11_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT) - SUM(fg.FGBOPAL_11_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'  and fg.FGBOPAL_ACCT_CODE IN ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006') "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,11"
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ "  SUM((fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT) - ( fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT))  presu"
						// + " SUM((FGBOPAL_12_ADOPT_BUD ) +
						// (FGBOPAL_12_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_12_YTD_ACTV) - sum(fg.FGBOPAL_11_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_12_YTD_ACTV) - SUM(fg.FGBOPAL_11_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_12_ADOPT_BUD) + SUM(fg.FGBOPAL_12_BUD_ADJT)) - (SUM(fg.FGBOPAL_11_ADOPT_BUD) + SUM(fg.FGBOPAL_11_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 12 mes,"
						+ " (SUM(FG.FGBOPAL_12_ADOPT_BUD) + SUM(FG.FGBOPAL_12_BUD_ADJT))-(SUM(FG.FGBOPAL_11_ADOPT_BUD + FG.FGBOPAL_11_BUD_ADJT))- sum(FG.FGBOPAL_12_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((FGBOPAL_12_ADOPT_BUD ) + (FGBOPAL_12_BUD_ADJT)) presu  pre_acum,"
						+ " sum (fg.FGBOPAL_12_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_12_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT) - SUM(fg.FGBOPAL_12_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "'  and fg.FGBOPAL_ACCT_CODE IN ('5A0001','5A0002','5A0003','5A0004','5A0005','5A0006','5A0007','5A0008','5A0009','5B0001','5B0002','5B0003','5B0004','5B0005','5B0006') "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,12"

				;

			} else { // Por estamento en espec�fico
				sql = ""
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						+ " SUM((FGBOPAL_00_ADOPT_BUD ) + (FGBOPAL_00_BUD_ADJT)) presu"
						+ " , SUM(fg.FGBOPAL_00_YTD_ACTV) gasto  ,"
						+ "  ROUND(SUM(FG.FGBOPAL_01_YTD_ACTV) / NULLIF( (SUM(FG.FGBOPAL_00_ADOPT_BUD + FG.FGBOPAL_01_ADOPT_BUD) + SUM(FG.FGBOPAL_00_BUD_ADJT + FG.FGBOPAL_01_BUD_ADJT)),0 )*100,2 )  porc   "
						+ " , 0 mes,"
						+ " SUM((FGBOPAL_00_ADOPT_BUD ) + (FGBOPAL_00_BUD_ADJT ) - FGBOPAL_00_YTD_ACTV )saldo_mes ,"
						+ " SUM (FGBOPAL_00_ADOPT_BUD + FGBOPAL_00_BUD_ADJT) pre_acum,"
						+ " sum (FGBOPAL_00_YTD_ACTV) gasto_acum ,"
						+ " round(SUM(fg.FGBOPAL_01_YTD_ACTV) / NULLIF( (SUM(fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_01_BUD_ADJT) *100),0),2) porc2 ,"
						+ " SUM((FGBOPAL_00_ADOPT_BUD ) - FGBOPAL_00_YTD_ACTV) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff   "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 0   "
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						// + " SUM((fg.FGBOPAL_00_ADOPT_BUD +
						// fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT +
						// fg.FGBOPAL_01_BUD_ADJT)) presu"
						+ " SUM((FGBOPAL_01_ADOPT_BUD ) + (FGBOPAL_01_BUD_ADJT)) presu"
						+ " , SUM(fg.FGBOPAL_01_YTD_ACTV) gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_01_YTD_ACTV) / NULLIF( (SUM(fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + SUM(fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)),0 )*100 ),2) porc "
						+ " , 1 mes,"
						+ " SUM((fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT) - fg.FGBOPAL_01_YTD_ACTV )saldo_mes ,"
						+ " SUM((fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_01_YTD_ACTV) gasto_acum ,"
						+ " ROUND((sum(FGBOPAL_01_YTD_ACTV) / NULLIF( SUM(FGBOPAL_01_ADOPT_BUD + FGBOPAL_01_BUD_ADJT),0) ) *100,2 ) porc2 ,"
						+ " SUM((fg.FGBOPAL_01_ADOPT_BUD + fg.FGBOPAL_01_BUD_ADJT) - fg.FGBOPAL_01_YTD_ACTV) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 1   "
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						// + " SUM((fg.FGBOPAL_02_ADOPT_BUD +
						// fg.FGBOPAL_02_BUD_ADJT) - (fg.FGBOPAL_00_ADOPT_BUD +
						// fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT +
						// fg.FGBOPAL_01_BUD_ADJT)) presu"
						+ " SUM((FGBOPAL_02_ADOPT_BUD ) + (FGBOPAL_02_BUD_ADJT)) presu"
						+ " , (SUM(FGBOPAL_02_YTD_ACTV) - SUM(FGBOPAL_01_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_02_YTD_ACTV) - SUM(fg.FGBOPAL_01_YTD_ACTV) ) / NULLIF( ((  (SUM(fg.FGBOPAL_02_ADOPT_BUD) + SUM(fg.FGBOPAL_02_BUD_ADJT))) - ((SUM(fg.FGBOPAL_00_ADOPT_BUD) + SUM(fg.FGBOPAL_01_ADOPT_BUD))) + ((SUM(fg.FGBOPAL_00_BUD_ADJT) + SUM(fg.FGBOPAL_01_BUD_ADJT)))),0) *100  ,2) porc"
						+ " , 2 mes,"
						+ " SUM((FG.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT) - (FG.FGBOPAL_00_ADOPT_BUD + FG.FGBOPAL_01_ADOPT_BUD) + (FG.FGBOPAL_00_BUD_ADJT + FG.FGBOPAL_01_BUD_ADJT) - FG.FGBOPAL_02_YTD_ACTV) saldo_mes ,"
						+ "  SUM((fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT) - (fg.FGBOPAL_00_ADOPT_BUD + fg.FGBOPAL_01_ADOPT_BUD) + (fg.FGBOPAL_00_BUD_ADJT + fg.FGBOPAL_01_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_02_YTD_ACTV) gasto_acum ,"
						+ " ROUND((sum(fg.FGBOPAL_02_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT),0)) ,2 ) *100  porc2 ,"
						+ "   (SUM (fg.FGBOPAL_02_ADOPT_BUD) + sum(fg.FGBOPAL_02_BUD_ADJT)) - sum(fg.FGBOPAL_02_YTD_ACTV) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 2 "
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						// + " SUM((fg.FGBOPAL_03_ADOPT_BUD +
						// fg.FGBOPAL_03_BUD_ADJT) - ( fg.FGBOPAL_02_ADOPT_BUD +
						// fg.FGBOPAL_02_BUD_ADJT)) presu"
						+ " SUM((FGBOPAL_03_ADOPT_BUD ) + (FGBOPAL_03_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_03_YTD_ACTV) - sum(fg.FGBOPAL_02_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_03_YTD_ACTV) - SUM(fg.FGBOPAL_02_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_03_ADOPT_BUD) + SUM(fg.FGBOPAL_03_BUD_ADJT)) - (SUM(fg.FGBOPAL_02_ADOPT_BUD) + SUM(fg.FGBOPAL_02_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 3 mes,"
						+ " (SUM(FG.FGBOPAL_03_ADOPT_BUD) + SUM(FG.FGBOPAL_03_BUD_ADJT))-(SUM(FG.FGBOPAL_02_ADOPT_BUD + FG.FGBOPAL_02_BUD_ADJT))- sum(FG.FGBOPAL_03_YTD_ACTV) saldo_mes  ,"
						+ " SUM((fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT) - ( fg.FGBOPAL_02_ADOPT_BUD + fg.FGBOPAL_02_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_03_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_03_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT) - SUM(fg.FGBOPAL_03_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 3  "
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						// + " SUM((fg.FGBOPAL_04_ADOPT_BUD +
						// fg.FGBOPAL_04_BUD_ADJT) - ( fg.FGBOPAL_03_ADOPT_BUD +
						// fg.FGBOPAL_03_BUD_ADJT)) presu"
						+ " SUM((FGBOPAL_04_ADOPT_BUD ) + (FGBOPAL_04_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_04_YTD_ACTV) - sum(fg.FGBOPAL_03_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_04_YTD_ACTV) - SUM(fg.FGBOPAL_03_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_04_ADOPT_BUD) + SUM(fg.FGBOPAL_04_BUD_ADJT)) - (SUM(fg.FGBOPAL_03_ADOPT_BUD) + SUM(fg.FGBOPAL_03_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 4 mes,"
						+ " (SUM(FG.FGBOPAL_04_ADOPT_BUD) + SUM(FG.FGBOPAL_04_BUD_ADJT))-(SUM(FG.FGBOPAL_03_ADOPT_BUD + FG.FGBOPAL_03_BUD_ADJT))- sum(FG.FGBOPAL_04_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT) - ( fg.FGBOPAL_03_ADOPT_BUD + fg.FGBOPAL_03_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_04_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_04_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT) - SUM(fg.FGBOPAL_04_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title, 4  "
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						// + " SUM((fg.FGBOPAL_05_ADOPT_BUD +
						// fg.FGBOPAL_05_BUD_ADJT) - ( fg.FGBOPAL_04_ADOPT_BUD +
						// fg.FGBOPAL_04_BUD_ADJT)) presu"
						+ " SUM((FGBOPAL_05_ADOPT_BUD ) + (FGBOPAL_05_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_05_YTD_ACTV) - sum(fg.FGBOPAL_04_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_05_YTD_ACTV) - SUM(fg.FGBOPAL_04_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_05_ADOPT_BUD) + SUM(fg.FGBOPAL_05_BUD_ADJT)) - (SUM(fg.FGBOPAL_04_ADOPT_BUD) + SUM(fg.FGBOPAL_04_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 5 mes,"
						+ " (SUM(FG.FGBOPAL_05_ADOPT_BUD) + SUM(FG.FGBOPAL_05_BUD_ADJT))-(SUM(FG.FGBOPAL_04_ADOPT_BUD + FG.FGBOPAL_04_BUD_ADJT))- sum(FG.FGBOPAL_05_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT) - ( fg.FGBOPAL_04_ADOPT_BUD + fg.FGBOPAL_04_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_05_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_05_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT) - SUM(fg.FGBOPAL_05_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,5 "
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						// + " SUM((fg.FGBOPAL_06_ADOPT_BUD +
						// fg.FGBOPAL_06_BUD_ADJT) - ( fg.FGBOPAL_05_ADOPT_BUD +
						// fg.FGBOPAL_05_BUD_ADJT)) presu"
						+ " SUM((FGBOPAL_06_ADOPT_BUD ) + (FGBOPAL_06_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_06_YTD_ACTV) - sum(fg.FGBOPAL_05_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_06_YTD_ACTV) - SUM(fg.FGBOPAL_05_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_06_ADOPT_BUD) + SUM(fg.FGBOPAL_06_BUD_ADJT)) - (SUM(fg.FGBOPAL_05_ADOPT_BUD) + SUM(fg.FGBOPAL_05_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 6 mes,"
						+ " (SUM(FG.FGBOPAL_06_ADOPT_BUD) + SUM(FG.FGBOPAL_06_BUD_ADJT))-(SUM(FG.FGBOPAL_05_ADOPT_BUD + FG.FGBOPAL_05_BUD_ADJT))- sum(FG.FGBOPAL_06_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT) - ( fg.FGBOPAL_05_ADOPT_BUD + fg.FGBOPAL_05_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_06_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_06_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT) - SUM(fg.FGBOPAL_06_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,6"
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						// + " SUM((fg.FGBOPAL_07_ADOPT_BUD +
						// fg.FGBOPAL_07_BUD_ADJT) - ( fg.FGBOPAL_06_ADOPT_BUD +
						// fg.FGBOPAL_06_BUD_ADJT)) presu"
						+ " SUM((FGBOPAL_07_ADOPT_BUD ) + (FGBOPAL_07_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_07_YTD_ACTV) - sum(fg.FGBOPAL_06_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_07_YTD_ACTV) - SUM(fg.FGBOPAL_06_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_07_ADOPT_BUD) + SUM(fg.FGBOPAL_07_BUD_ADJT)) - (SUM(fg.FGBOPAL_06_ADOPT_BUD) + SUM(fg.FGBOPAL_06_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 7 mes,"
						+ " (SUM(FG.FGBOPAL_07_ADOPT_BUD) + SUM(FG.FGBOPAL_07_BUD_ADJT))-(SUM(FG.FGBOPAL_06_ADOPT_BUD + FG.FGBOPAL_06_BUD_ADJT))- sum(FG.FGBOPAL_07_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT) - ( fg.FGBOPAL_06_ADOPT_BUD + fg.FGBOPAL_06_BUD_ADJT))  pre_acum,"
						+ " sum (fg.FGBOPAL_07_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_07_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT) - SUM(fg.FGBOPAL_07_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,7"
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						// + " SUM((fg.FGBOPAL_08_ADOPT_BUD +
						// fg.FGBOPAL_08_BUD_ADJT) - ( fg.FGBOPAL_07_ADOPT_BUD +
						// fg.FGBOPAL_07_BUD_ADJT)) presu"
						+ " SUM((FGBOPAL_08_ADOPT_BUD ) + (FGBOPAL_08_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_08_YTD_ACTV) - sum(fg.FGBOPAL_07_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_08_YTD_ACTV) - SUM(fg.FGBOPAL_07_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_08_ADOPT_BUD) + SUM(fg.FGBOPAL_08_BUD_ADJT)) - (SUM(fg.FGBOPAL_07_ADOPT_BUD) + SUM(fg.FGBOPAL_07_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 8 mes,"
						+ " (SUM(FG.FGBOPAL_08_ADOPT_BUD) + SUM(FG.FGBOPAL_08_BUD_ADJT))-(SUM(FG.FGBOPAL_07_ADOPT_BUD + FG.FGBOPAL_07_BUD_ADJT))- sum(FG.FGBOPAL_08_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT) - ( fg.FGBOPAL_07_ADOPT_BUD + fg.FGBOPAL_07_BUD_ADJT))  pre_acum,"
						+ " sum (fg.FGBOPAL_08_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_08_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT) - SUM(fg.FGBOPAL_08_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,8"
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						// + " SUM((fg.FGBOPAL_09_ADOPT_BUD +
						// fg.FGBOPAL_09_BUD_ADJT) - ( fg.FGBOPAL_08_ADOPT_BUD +
						// fg.FGBOPAL_08_BUD_ADJT)) presu"
						+ " SUM((FGBOPAL_09_ADOPT_BUD ) + (FGBOPAL_09_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_09_YTD_ACTV) - sum(fg.FGBOPAL_08_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_09_YTD_ACTV) - SUM(fg.FGBOPAL_08_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_09_ADOPT_BUD) + SUM(fg.FGBOPAL_09_BUD_ADJT)) - (SUM(fg.FGBOPAL_08_ADOPT_BUD) + SUM(fg.FGBOPAL_08_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 9 mes,"
						+ " (SUM(FG.FGBOPAL_09_ADOPT_BUD) + SUM(FG.FGBOPAL_09_BUD_ADJT))-(SUM(FG.FGBOPAL_08_ADOPT_BUD + FG.FGBOPAL_08_BUD_ADJT))- sum(FG.FGBOPAL_09_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT) - ( fg.FGBOPAL_08_ADOPT_BUD + fg.FGBOPAL_08_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_09_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_09_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT) - SUM(fg.FGBOPAL_09_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,9"
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						// + " SUM((fg.FGBOPAL_10_ADOPT_BUD +
						// fg.FGBOPAL_10_BUD_ADJT) - ( fg.FGBOPAL_09_ADOPT_BUD +
						// fg.FGBOPAL_09_BUD_ADJT)) presu"
						+ " SUM((FGBOPAL_10_ADOPT_BUD ) + (FGBOPAL_10_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_10_YTD_ACTV) - sum(fg.FGBOPAL_09_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_10_YTD_ACTV) - SUM(fg.FGBOPAL_09_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_10_ADOPT_BUD) + SUM(fg.FGBOPAL_10_BUD_ADJT)) - (SUM(fg.FGBOPAL_09_ADOPT_BUD) + SUM(fg.FGBOPAL_09_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 10 mes,"
						+ " (SUM(FG.FGBOPAL_10_ADOPT_BUD) + SUM(FG.FGBOPAL_10_BUD_ADJT))-(SUM(FG.FGBOPAL_09_ADOPT_BUD + FG.FGBOPAL_09_BUD_ADJT))- sum(FG.FGBOPAL_10_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT) - ( fg.FGBOPAL_09_ADOPT_BUD + fg.FGBOPAL_09_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_10_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_10_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT) - SUM(fg.FGBOPAL_10_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,10"
						+ " UNION ALL  "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						// + " SUM((fg.FGBOPAL_11_ADOPT_BUD +
						// fg.FGBOPAL_11_BUD_ADJT) - ( fg.FGBOPAL_10_ADOPT_BUD +
						// fg.FGBOPAL_10_BUD_ADJT)) presu"
						+ " SUM((FGBOPAL_11_ADOPT_BUD ) + (FGBOPAL_11_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_11_YTD_ACTV) - sum(fg.FGBOPAL_10_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_11_YTD_ACTV) - SUM(fg.FGBOPAL_10_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_11_ADOPT_BUD) + SUM(fg.FGBOPAL_11_BUD_ADJT)) - (SUM(fg.FGBOPAL_10_ADOPT_BUD) + SUM(fg.FGBOPAL_10_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 11 mes,"
						+ " (SUM(FG.FGBOPAL_11_ADOPT_BUD) + SUM(FG.FGBOPAL_11_BUD_ADJT))-(SUM(FG.FGBOPAL_10_ADOPT_BUD + FG.FGBOPAL_10_BUD_ADJT))- sum(FG.FGBOPAL_11_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT) - ( fg.FGBOPAL_10_ADOPT_BUD + fg.FGBOPAL_10_BUD_ADJT)) pre_acum,"
						+ " sum (fg.FGBOPAL_11_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_11_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT) - SUM(fg.FGBOPAL_11_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,11"
						+ " UNION ALL "
						+ " SELECT   fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , "
						// + " SUM((fg.FGBOPAL_12_ADOPT_BUD +
						// fg.FGBOPAL_12_BUD_ADJT) - ( fg.FGBOPAL_11_ADOPT_BUD +
						// fg.FGBOPAL_11_BUD_ADJT)) presu"
						+ " SUM((FGBOPAL_12_ADOPT_BUD ) + (FGBOPAL_12_BUD_ADJT)) presu"
						+ " , (SUM(fg.FGBOPAL_12_YTD_ACTV) - sum(fg.FGBOPAL_11_YTD_ACTV) )  gasto  ,"
						+ "  ROUND((SUM(fg.FGBOPAL_12_YTD_ACTV) - SUM(fg.FGBOPAL_11_YTD_ACTV))/ NULLIF( ((SUM(fg.FGBOPAL_12_ADOPT_BUD) + SUM(fg.FGBOPAL_12_BUD_ADJT)) - (SUM(fg.FGBOPAL_11_ADOPT_BUD) + SUM(fg.FGBOPAL_11_BUD_ADJT))),0) *100  ,2) porc"
						+ " , 12 mes,"
						+ " (SUM(FG.FGBOPAL_12_ADOPT_BUD) + SUM(FG.FGBOPAL_12_BUD_ADJT))-(SUM(FG.FGBOPAL_11_ADOPT_BUD + FG.FGBOPAL_11_BUD_ADJT))- sum(FG.FGBOPAL_12_YTD_ACTV) saldo_mes  ,"
						+ "  SUM((fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT) - ( fg.FGBOPAL_11_ADOPT_BUD + fg.FGBOPAL_11_BUD_ADJT))  pre_acum,"
						+ " sum (fg.FGBOPAL_12_YTD_ACTV) gasto_acum ,"
						+ " ROUND((SUM(fg.FGBOPAL_12_YTD_ACTV) / NULLIF( SUM(fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT),0)) *100 ,2 )   porc2 ,"
						+ "   (SUM(fg.FGBOPAL_12_ADOPT_BUD + fg.FGBOPAL_12_BUD_ADJT) - SUM(fg.FGBOPAL_12_YTD_ACTV)) total"
						+ " FROM FGBOPAL fg ,ftvorgn ft , ftvfund ff  "
						+ " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code and ff.ftvfund_fund_code =  fg.FGBOPAL_fund_code  and fg.fgbopal_ledc_code = 'FINANC' and fg.FGBOPAL_FSYR_CODE = '"
						+ a�o
						+ "' and fg.FGBOPAL_ORGN_CODE = '"
						+ org
						+ "' AND fg.FGBOPAL_ACCT_CODE = '"
						+ cuenta
						+ "' "
						+ " GROUP BY fg.FGBOPAL_ORGN_CODE, ft.ftvorgn_title,12";
			}

			//System.out.println("Union Big sql " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			long acumulado = 0;
			long gastos = 0;
			while (res.next()) {
				// String aux = res.getString(1);
				Pres18POJO ss = new Pres18POJO();
				ss.setCoduni(res.getString(1)); // cod cuenta
				ss.setDesuni(res.getString(2)); // titulo cta
				ss.setPresum(res.getLong(3)); // presupuesto

				// acumulado = acumulado + res.getLong(3);
				// ss.setPresac(acumulado);
				ss.setUsadom(res.getLong(4)); // gastos

				/*
				 * gastos = gastos + res.getLong(4);
				 * 
				 * if (gastos > 0) { ss.setIddigi("S");
				 *  } else { ss.setIddigi("N");
				 *  }
				 */
				// ss.setUsadom(res.getLong(4)); // gastos
				ss.setPorc1(res.getFloat(5)); // porc1
				ss.setNummes(res.getInt(6)); // mes
				ss.setSaldoMes(res.getLong(7)); // saldo mes
				ss.setPresac(res.getLong(8)); // pres acumulado
				ss.setUsadac(res.getLong(9)); // gasto acumulado
				ss.setPorc2(res.getFloat(10)); // porc2
				ss.setTotal(res.getLong(11)); // total

				if (sw > 0) {
					ss.setIddigi("S");

				} else {
					ss.setIddigi("N");

				}
				lista.add(ss);

			}
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error sipB getRemuneracion: "+ e.getMessage());
		}
		return lista;
	}

	public Collection<Presw21POJO> consulta_presw21(String org) {

		return null;
	}

	public Collection<ItemPOJO> getCuentasorganizacion(String org,String cuentas_excepciones) {

		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<ItemPOJO> lista = new ArrayList<ItemPOJO>();

		try {
			/*
			 * String sql = "" + " select distinct fg.FGBOPAL_ACCT_CODE ,
			 * ft.FTVACCT_TITLE" + " from FGBOPAL fg" + " INNER JOIN FTVACCT ft
			 * ON FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE " + " where
			 * fg.FGBOPAL_ORGN_CODE = '"+ org + "' ";
			 * //System.out.println("Combobox sql " + sql);
			 */
			String sql = ""
					+ " select distinct fg.FGBOPAL_ACCT_CODE , ft.FTVACCT_TITLE"
					+ " from    FGBOPAL fg"
					+ " INNER JOIN FTVACCT ft ON FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE "
					+ "where fg.FGBOPAL_ORGN_CODE = '" + org + "' order by FGBOPAL_ACCT_CODE ";

			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();

			while (res.next()) {
				String cuenta = res.getString(1) ;
				int intIndex = cuentas_excepciones.indexOf(cuenta);
				
				if (intIndex == - 1 ) {
					ItemPOJO ss = new ItemPOJO();
					ss.setCoditem(cuenta);
					ss.setItepre(cuenta);
					ss.setNomite(res.getString(2)); // nombre cuenta organizacion				
					lista.add(ss);
				}
			}
			/*while (res.next()) {
				ItemPOJO ss = new ItemPOJO();
				
				ss.setCoditem(res.getString(1));
				ss.setItepre(res.getString(1));
				ss.setNomite(res.getString(2)); // nombre cuenta organizacion				
				lista.add(ss);
			}*/
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error moduloPresupuestoB.getCuentasorganizacion: "+ e.getMessage());
		}
		return lista;
	}

	public String getPIDM(String rut) {

		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		String resp = "";
		try {

			String sql = "" + " select fo.FOBPROF_USER_ID"
					+ " from spriden sp, fobprof FO"
					+ " where  sp.SPRIDEN_PIDM = fo.FOBPROF_PIDM"
					+ " and sp.SPRIDEN_ID = '" + rut + "'  ";
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();

			while (res.next()) {
				ItemPOJO ss = new ItemPOJO();

				resp = res.getString(1);

			}
			//System.out.println("SQL getPDIM " + sql);
			res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error moduloPresupuestoB.getPIDM: "+ e.getMessage());
		}
		return resp;
	}

	public Collection<ItemPOJO> getCuentasorganizacionAS400(String org) {
		//System.out.println("Modulo AS400");
		ConexionAs400 con = new ConexionAs400();

		List<ItemPOJO> lista = new ArrayList<ItemPOJO>();

		try {
			String sql = "SELECT CUEBAN, DESCUE FROM USMMBP.PRESF202 "
					+ "WHERE CUEBAN = '" + org + "' ";
			/*
			 * PreparedStatement sent = con.conexion().prepareStatement("SELECT
			 * valval FROM VIEW_COCOF100B " + "WHERE numval = ? " + "AND ESTVAL =
			 * 'A'");
			 */
			PreparedStatement sent = con.getConnection().prepareStatement(sql);

			//System.out.println(" AS400 " + sql);

			ResultSet res = sent.executeQuery();

			if (res.next()) {
				ItemPOJO ss = new ItemPOJO();
				ss.setCoditem(res.getString(1));
				ss.setNomtem(res.getString(2)); // nombre cuenta organizacion

			}
			res.close();
			sent.close();
			// con.close();
			con.getConnection().close();
		} catch (SQLException e) {
			System.out.println("Error moduloValePagoB.getValorDeVale: " + e);
		}
		return lista;
	}
	
	public String  getCuentasExcepcionesAS400() {
		//System.out.println("Modulo AS400");
		ConexionAs400 con = new ConexionAs400();

		String cuentas_excepciones = "";

		try {
			String sql = "SELECT CODCUE FROM TRANSFERBT.PRESF117B ";
					
			/*
			 * PreparedStatement sent = con.conexion().prepareStatement("SELECT
			 * valval FROM VIEW_COCOF100B " + "WHERE numval = ? " + "AND ESTVAL =
			 * 'A'");
			 */
			PreparedStatement sent = con.getConnection().prepareStatement(sql);

			//System.out.println(" AS400 " + sql);

			ResultSet res = sent.executeQuery();
			Boolean primero = true;
			while (res.next()) {
				
				if (primero) {
					cuentas_excepciones  = "'" + (res.getString(1)) + "'"; // cuenta
				
					primero = false;
				} else 
					cuentas_excepciones  = cuentas_excepciones + ',' +  "'" + (res.getString(1)) + "'"; // cuenta
				
								

			}
			res.close();
			sent.close();
			// con.close();
			con.getConnection().close();
		} catch (SQLException e) {
			System.out.println("Error getCuentasExcepcionesAS400: " + e);
		}
		
		
		return cuentas_excepciones;
	}

	public String eliminaCaracteresString(String sTexto) {
		/* elimina acentos del string */
		String linea = "";
		if (!sTexto.trim().equals("")) {
			for (int x = 0; x < sTexto.length(); x++) {
				if (sTexto.charAt(x) == '\'' || (sTexto.charAt(x) == '\n')
						|| sTexto.charAt(x) == '\t') {

					linea += ' ';
				} else
					linea += sTexto.charAt(x);
			}
		}

		return linea;
	}

	public PRESF48B getPRESF43B(int a�o, String codorg) throws SQLException {
		ConexionAs400 con = new ConexionAs400();
		// //System.out.println(""");
		PRESF48B pres48B = new PRESF48B();
		try {
			String query = " SELECT ANOPRE, CODORG, REMVA1, REMVA2,	REMVA3,	REMVA4,	CANPA1,	CANPA2,	ALUAT1,	ALUAT2,	HORPT1,	HORPT2,"
					+ "		COSPT1,	COSPT2,	HORHO1,	HORHO2,	COSHO1,	COSHO2,	HORFT1,	HORFT2,	COSFT1,	COSFT2,	HORAY1,	HORAY2,"
					+ " 	    COSAY1,	COSAY2,	ESTADO,	FECACE"
					+ " FROM USMMBP.PRESF48B PRESF48B"
					+ " WHERE PRESF48B.ANOPRE = "
					+ a�o
					+ " AND PRESF48B.CODORG = '" + codorg.trim() + "'";
			/*
			 * String query = " SELECT ANOPRE, CODORG, REMVA1, REMVA2, REMVA3,
			 * REMVA4, CANPA1, CANPA2, ALUAT1, ALUAT2, HORPT1, HORPT2," + "
			 * COSPT1, COSPT2, HORHO1, HORHO2, COSHO1, COSHO2, HORFT1, HORFT2,
			 * COSFT1, COSFT2, HORAY1, HORAY2," + " COSAY1, COSAY2, ESTADO,
			 * FECACE" + " FROM USMMBP.PRESF48B PRESF48B" + " WHERE
			 * PRESF48B.ANOPRE = 2017" + " AND PRESF48B.CODORG = 'AC2113'";
			 */

			//System.out.println(query);
			PreparedStatement sent = con.getConnection()
					.prepareStatement(query);

			ResultSet res = sent.executeQuery();
			while (res.next()) {
				pres48B = new PRESF48B();
				pres48B.setANOPRE(res.getInt(1));
				pres48B.setCODORG(res.getString(2));
				pres48B.setREMVA1(res.getLong(3));
				pres48B.setREMVA2(res.getLong(4));
				pres48B.setREMVA3(res.getLong(5));
				pres48B.setREMVA4(res.getLong(6));
				pres48B.setCANPA1(res.getLong(7));
				pres48B.setCANPA2(res.getLong(8));
				pres48B.setALUAT1(res.getLong(9));
				pres48B.setALUAT2(res.getLong(10));
				pres48B.setHORPT1(res.getLong(11));
				pres48B.setHORPT2(res.getLong(12));
				pres48B.setCOSPT1(res.getLong(13));
				pres48B.setCOSPT2(res.getLong(14));
				pres48B.setHORHO1(res.getLong(15));
				pres48B.setHORHO2(res.getLong(16));
				pres48B.setCOSHO1(res.getLong(17));
				pres48B.setCOSHO2(res.getLong(18));
				pres48B.setHORFT1(res.getLong(19));
				pres48B.setHORFT2(res.getLong(20));
				pres48B.setCOSFT1(res.getLong(21));
				pres48B.setCOSFT2(res.getLong(22));
				pres48B.setHORAY1(res.getLong(23));
				pres48B.setHORAY2(res.getLong(24));
				pres48B.setCOSAY1(res.getLong(25));
				pres48B.setCOSAY2(res.getLong(26));
				pres48B.setESTADO(res.getString(27));
				pres48B.setFECACE(res.getInt(28));
			}
			res.close();
			sent.close();
		} catch (SQLException e) {
			System.out
					.println("Error en moduloPresupuestoInterceptor.getPRESF43B:"
							+ e.toString());
		}
		return pres48B;
	}

	public int savePRESF48B(PRESF48B presf48B, String indexi)
			throws SQLException {
		// //System.out.println(""");
		PRESF48B pres48B = new PRESF48B();

		int error = 0;
		try {
			ConexionAs400 con = new ConexionAs400();
			con.getConnection().setAutoCommit(false);

			String query1 = " UPDATE USMMBP.PRESF48B SET" + " CANPA1 = "
					+ presf48B.getCANPA1() + ", CANPA2 = "
					+ presf48B.getCANPA2() + ", ALUAT1 = "
					+ presf48B.getALUAT1() + ", ALUAT2 = "
					+ presf48B.getALUAT2() + ", HORPT1 = "
					+ presf48B.getHORPT1() + ", HORPT2 = "
					+ presf48B.getHORPT2() + ", COSPT1 = "
					+ presf48B.getCOSPT1() + ", COSPT2 = "
					+ presf48B.getCOSPT2() + ", HORHO1 = "
					+ presf48B.getHORHO1() + ", HORHO2 = "
					+ presf48B.getHORHO2() + ", COSHO1 = "
					+ presf48B.getCOSHO1() + ", COSHO2 = "
					+ presf48B.getCOSHO2() + ", HORFT1 = "
					+ presf48B.getHORFT1() + ", HORFT2 = "
					+ presf48B.getHORFT2() + ", COSFT1 = "
					+ presf48B.getCOSFT1() + ", COSFT2 = "
					+ presf48B.getCOSFT2() + ", HORAY1 = "
					+ presf48B.getHORAY1() + ", HORAY2 = "
					+ presf48B.getHORAY2() + ", COSAY1 = "
					+ presf48B.getCOSAY1() + ", COSAY2 = "
					+ presf48B.getCOSAY2() + ", ESTADO = '"
					+ presf48B.getESTADO() + "'" + ", FECACE = "
					+ presf48B.getFECACE() + " WHERE ANOPRE = "
					+ presf48B.getANOPRE() + " AND CODORG = '"
					+ presf48B.getCODORG().trim() + "'";

			// //System.out.println(query1);
			PreparedStatement sent2 = con.getConnection().prepareStatement(
					query1);
			int res = sent2.executeUpdate();
			if (res < 0)
				error = -1;
			sent2.close();
			if (error >= 0) {

				String query2 = " UPDATE USMMBP.PRESL204A SET" + " EDTPRO = '"
						+ indexi + "'" + " WHERE CODORG = '"
						+ presf48B.getCODORG().trim() + "'";

				PreparedStatement sent3 = con.getConnection().prepareStatement(
						query2);
				//System.out.println(query2) ;
				int res2 = sent3.executeUpdate();
				if (res2 < 0)
					error = -2;
				sent3.close();

			}
			//System.out.println("error: " + error);
			if (error == 0)
				con.getConnection().commit();
			else
				con.getConnection().rollback();

			con.getConnection().close();
		} catch (SQLException e) {
			System.out
					.println("Error en moduloPrespuesto.savePRESF48B PRESF48B : "
							+ e);
		}

		return error;
	}	
		
		public int getObtienePIDM(String rutnumdv){
			ConexionBanner con = new ConexionBanner();
			int pidm = 0;
			
			   String rut = rutnumdv.substring(0,rutnumdv.length()-1);
		       String dig = rutnumdv.substring(rutnumdv.length()-1,rutnumdv.length());
		       if (dig.equals("k")) rutnumdv = rut+"K";
			
			try{
							      
				PreparedStatement sent = con.conexion().prepareStatement("SELECT SPRIDEN_PIDM " +
		                                                                 "FROM SPRIDEN " + 
		                                                                 "WHERE spriden_id = ? " +
		                                                                 "AND spriden_change_ind is null ");
				sent.setString(1,rutnumdv);
				ResultSet res = sent.executeQuery();

				if (res.next()){
					pidm = res.getInt(1); // pidem
				}
				res.close();
				sent.close();
				con.close();
			}
			catch (SQLException e){
				System.out.println("Error moduloPresupuestoB.getObtienePIDM: " + e);
			}
			return pidm;
		} 
		
		 public Collection<Presw21DTO> getDatosMovFactura(int pidm, int numDoc) {
			  Collection<Presw21DTO> listaPresw21 = new ArrayList<Presw21DTO>();
			  ConexionBanner con = new ConexionBanner();
			  PreparedStatement sent;
			 // pidm   = 99552;
			 // numDoc = 82054;
			    
			  try {
				  sent = con.conexion().prepareStatement(" SELECT TBRACCD_INVOICE_NUMBER, TBRACCD_RECEIPT_NUMBER, TBRACCD_PERIOD, " +
						                                 " TO_NUMBER(TO_CHAR(TBRACCD_ENTRY_DATE,'DDMMYYYY')), " +
						                                 " TBRACCD_AMOUNT, TBRACCD_DESC, TBBDETC_TYPE_IND, TBBDETC_DCAT_CODE, TBRACCD_TRAN_NUMBER " +
	                                                     " FROM TBRACCD, TBBDETC " + 
	                                                     " WHERE TBRACCD_PIDM = " + pidm +
	                                                     " AND TBRACCD_INVOICE_NUMBER =  " + numDoc +
	                                                     " AND TBRACCD_DETAIL_CODE = TBBDETC_DETAIL_CODE");	      
			
				  ResultSet res = sent.executeQuery();
				  
				  if (!res.isBeforeFirst() && !res.isAfterLast()) { // si no hay resultado
					  numDoc = numDoc + 30000000;
					  sent = con.conexion().prepareStatement(" SELECT (TBRACCD_INVOICE_NUMBER-30000000), TBRACCD_RECEIPT_NUMBER, TBRACCD_PERIOD, " +
                              								 " TO_NUMBER(TO_CHAR(TBRACCD_ENTRY_DATE,'DDMMYYYY')), " +
                              								 " TBRACCD_AMOUNT, TBRACCD_DESC, TBBDETC_TYPE_IND, TBBDETC_DCAT_CODE, TBRACCD_TRAN_NUMBER " +
                              								 " FROM TBRACCD, TBBDETC " + 
                              								 " WHERE TBRACCD_PIDM = " + pidm +
                              								 " AND TBRACCD_INVOICE_NUMBER =  " + numDoc +
                              								 " AND TBRACCD_DETAIL_CODE = TBBDETC_DETAIL_CODE");	
					  res = sent.executeQuery(); 
				  }

				  while (res.next()) {
					 Presw21DTO presw21C = new Presw21DTO();
					 presw21C.setNumdoc(res.getInt(1)); // numero documento 
					 presw21C.setTipdoc(res.getString(8)); // Tipo de documento
					 presw21C.setFecdoc(res.getInt(4)); // Fecha
					 presw21C.setNrodoc(res.getInt(1));
					 presw21C.setTipmov(res.getString(8));
					 presw21C.setValor1(res.getLong(5)); // valor  0 iva
					 presw21C.setGlosa1(res.getString(6)); // descipci�n
					 presw21C.setDigide(res.getString(7).trim()); // dice si es cargo o pago
					 listaPresw21.add(presw21C);
					 
					 try {
						  sent = con.conexion().prepareStatement(" SELECT TBRAPPL_PAY_TRAN_NUMBER, TBRAPPL_AMOUNT " +
								                                 " FROM TBRAPPL " +
								                                 " WHERE TBRAPPL_PIDM = " + pidm +
	                                                             " AND TBRAPPL_CHG_TRAN_NUMBER = " + res.getInt(9));
										  
						  ResultSet res2 = sent.executeQuery();

						  if (res2.next()) {
							  sent = con.conexion().prepareStatement(" SELECT TBRACCD_DOCUMENT_NUMBER, TBRACCD_RECEIPT_NUMBER, TBRACCD_PERIOD, " +
		                                                             " TO_NUMBER(TO_CHAR(TBRACCD_ENTRY_DATE,'DDMMYYYY')), " +
		                                                             " TBRACCD_AMOUNT, TBRACCD_DESC, TBBDETC_TYPE_IND, TBBDETC_DCAT_CODE, TBRACCD_TRAN_NUMBER " +
	                                                                 " FROM TBRACCD, TBBDETC " + 
	                                                                 " WHERE TBRACCD_PIDM = " + pidm +
	                                                                 " AND TBRACCD_TRAN_NUMBER =  " + res2.getInt(1) +
	                                                                 " AND TBRACCD_DETAIL_CODE = TBBDETC_DETAIL_CODE");
													  
							  
	                          ResultSet res3 = sent.executeQuery();
	                          while (res3.next()) {
	                        	  Presw21DTO presw21P = new Presw21DTO();
	                        	  presw21P.setNumdoc(res3.getInt(2)); // numero documento 
	                        	  presw21P.setTipdoc(res.getString(3)); // Tipo de documento // se mantiene el tipo del CARGO
	                        	  presw21P.setFecdoc(res3.getInt(4)); // Fecha
	                        	  presw21P.setNrodoc(res3.getInt(2)); // nrodoc
	                        	  presw21P.setTipmov(res3.getString(8)); // tipmov
	                         	  presw21P.setValor1(res2.getLong(2)); // valor del pago de TBRAPPL
	         				      presw21P.setGlosa1(res3.getString(6)); // descipci�n
	         				      presw21P.setDigide(res3.getString(7).trim()); // dice si es cargo o pago
	         				      listaPresw21.add(presw21P);
							  
						    }
	                        res3.close();
						  }
						  res2.close();
						
			                                                     
				  }	catch (SQLException e){
			             System.out.println("Error moduloPrespuestoB.getDatosMovFactura: " + e);
		          }	 
				 } 
				  
				  
				 
			    res.close();
	            sent.close();
	            con.close();
	         }
	         catch (SQLException e){
	             System.out.println("Error moduloPrespuestoB.getDatosMovFactura: " + e);
	          }
	      return listaPresw21;
		 }
		 public List<Presw25DTO> getOrganizaciones(String exipre, int rutUsuario, String dv, int a�o){
			 // tipo = S presup corriente y plan desarrollo;
			 // tipo = I presup programas especiales;
			List<Presw25DTO> listaPRESW25 = null;
			List<Presw25DTO> listaOrg =  new ArrayList<Presw25DTO>();
			PreswBean preswbean = new PreswBean("CPR", 0,0,a�o,0, rutUsuario);
			preswbean.setDigide(dv);
		
			if(preswbean != null) {
				listaPRESW25 = (List<Presw25DTO>) preswbean.consulta_presw25();
				for (Presw25DTO ss : listaPRESW25){
					if (ss.getExipre().trim().equals(exipre.trim()))
						listaOrg.add(ss);
				}
				
			}
			return listaOrg;
		 }
		 
		 public Vector getlistaEspecialPDF(HttpServletRequest req) {
				// MAA se cambia unidad por organizacion y itedoc a String
				// int itedoc = Util.validaParametro(req.getParameter("itedoc"), 0);
				String itedoc = Util.validaParametro(req.getParameter("itedoc"), "");
				int a�o = Util.validaParametro(req.getParameter("anno"), 0);
				// int unidad = Util.validaParametro(req.getParameter("unidad"), 0);
				String organizacion = Util.validaParametro(req
						.getParameter("organizacion"), "");
				int rutUsuario = Integer.parseInt(req.getSession().getAttribute(
						"rutUsuario")
						+ "");

				Vector vec_datos = new Vector();
				Vector vec_d = new Vector();
				List<Presw25DTO> listaPresw25 = new ArrayList<Presw25DTO>();
				// String nomUnidad = "";
				String nomOrganizacion = "";
				long total = 0;
				long totalTotal = 0;
				long enero = 0;
				long febrero = 0;
				long marzo = 0;
				long abril = 0;
				long mayo = 0;
				long junio = 0;
				long julio = 0;
				long agosto = 0;
				long septiembre = 0;
				long octubre = 0;
				long noviembre = 0;
				long diciembre = 0;
				// PreswBean preswbean = new PreswBean("PRE", unidad,0,a�o,0,
				// rutUsuario);
				PreswBean preswbean = new PreswBean("PRE", 0, 0, a�o, 0, rutUsuario);
				preswbean.setCajero(organizacion);
				NumberFormat nf2 = NumberFormat.getInstance(Locale.GERMAN);

				if (preswbean != null) {
					listaPresw25 = (List<Presw25DTO>) preswbean.consulta_presw25();
				}

				long totalAgrup = 0;
				Vector agrup = null;
				Vector agrup20 = new Vector();
				Vector agrup21 = new Vector();
				Vector agrup22 = new Vector();
				Vector agrup23 = new Vector();
				Vector agrup24 = new Vector();
				Vector agrup1 = new Vector();
				Vector agrup2 = new Vector();
				Vector agrup3 = new Vector();
				Vector agrup4 = new Vector();
				long total_ENE = 0;
				long total_FEB = 0;
				long total_MAR = 0;
				long total_ABR = 0;
				long total_MAY = 0;
				long total_JUN = 0;
				long total_JUL = 0;
				long total_AGO = 0;
				long total_SEP = 0;
				long total_OCT = 0;
				long total_NOV = 0;
				long total_DIC = 0;
				long agrup_ENE = 0;
				long agrup_FEB = 0;
				long agrup_MAR = 0;
				long agrup_ABR = 0;
				long agrup_MAY = 0;
				long agrup_JUN = 0;
				long agrup_JUL = 0;
				long agrup_AGO = 0;
				long agrup_SEP = 0;
				long agrup_OCT = 0;
				long agrup_NOV = 0;
				long agrup_DIC = 0;
				MathTool mathtool = new MathTool();
				for (Presw25DTO ss : listaPresw25) {
					total = ss.getPres01() + ss.getPres02() + ss.getPres03()
							+ ss.getPres04() + ss.getPres05() + ss.getPres06()
							+ ss.getPres07() + ss.getPres08() + ss.getPres09()
							+ ss.getPres10() + ss.getPres11() + ss.getPres12();
					total_ENE = ss.getPres01();
					total_FEB = ss.getPres02();
					total_MAR = ss.getPres03();
					total_ABR = ss.getPres04();
					total_MAY = ss.getPres05();
					total_JUN = ss.getPres06();
					total_JUL = ss.getPres07();
					total_AGO = ss.getPres08();
					total_SEP = ss.getPres09();
					total_OCT = ss.getPres10();
					total_NOV = ss.getPres11();
					total_DIC = ss.getPres12();
					if (total > 0  || (!ss.getMotiv2().trim().equals(ss.getMotiv3().trim()))) {
						totalAgrup = totalAgrup + total;
						agrup_ENE = agrup_ENE + total_ENE;
						agrup_FEB = agrup_FEB + total_FEB;
						agrup_MAR = agrup_MAR + total_MAR;
						agrup_ABR = agrup_ABR + total_ABR;
						agrup_MAY = agrup_MAY + total_MAY;
						agrup_JUN = agrup_JUN + total_JUN;
						agrup_JUL = agrup_JUL + total_JUL;
						agrup_AGO = agrup_AGO + total_AGO;
						agrup_SEP = agrup_SEP + total_SEP;
						agrup_OCT = agrup_OCT + total_OCT;
						agrup_NOV = agrup_NOV + total_NOV;
						agrup_DIC = agrup_DIC + total_DIC;
					} else {
						agrup = new Vector();
						agrup.addElement(totalAgrup);
						agrup.addElement(agrup_ENE);
						agrup.addElement(agrup_FEB);
						agrup.addElement(agrup_MAR);
						agrup.addElement(agrup_ABR);
						agrup.addElement(agrup_MAY);
						agrup.addElement(agrup_JUN);
						agrup.addElement(agrup_JUL);
						agrup.addElement(agrup_AGO);
						agrup.addElement(agrup_SEP);
						agrup.addElement(agrup_OCT);
						agrup.addElement(agrup_NOV);
						agrup.addElement(agrup_DIC);
						if(ss.getMotiv2().trim().equals("1")){
							  agrup20 = agrup; 
							  agrup = new Vector();						  
							  agrup_ENE = (mathtool.round((agrup_ENE * 2) / 10));
							  agrup_FEB = (mathtool.round((agrup_FEB * 2) / 10));
							  agrup_MAR = (mathtool.round((agrup_MAR * 2) / 10));
							  agrup_ABR = (mathtool.round((agrup_ABR * 2) / 10));
							  agrup_MAY = (mathtool.round((agrup_MAY * 2) / 10));
							  agrup_JUN = (mathtool.round((agrup_JUN * 2) / 10));
							  agrup_JUL = (mathtool.round((agrup_JUL * 2) / 10));
							  agrup_AGO = (mathtool.round((agrup_AGO * 2) / 10));
							  agrup_SEP = (mathtool.round((agrup_SEP * 2) / 10));
							  agrup_OCT = (mathtool.round((agrup_OCT * 2) / 10));
							  agrup_NOV = (mathtool.round((agrup_NOV * 2) / 10));
							  agrup_DIC = (mathtool.round((agrup_DIC * 2) / 10));
							  totalAgrup = (agrup_ENE + agrup_FEB + agrup_MAR + agrup_ABR + agrup_MAY + agrup_JUN + agrup_JUL + agrup_AGO + agrup_SEP + agrup_OCT + agrup_NOV + agrup_DIC);
								 
						      agrup21.addElement(totalAgrup);  
						      agrup21.addElement(agrup_ENE);
						      agrup21.addElement(agrup_FEB);
						      agrup21.addElement(agrup_MAR);
						      agrup21.addElement(agrup_ABR);
						      agrup21.addElement(agrup_MAY);
						      agrup21.addElement(agrup_JUN);
						      agrup21.addElement(agrup_JUL);
						      agrup21.addElement(agrup_AGO);
						      agrup21.addElement(agrup_SEP);
						      agrup21.addElement(agrup_OCT);
						      agrup21.addElement(agrup_NOV);
						      agrup21.addElement(agrup_DIC);
				     
						 }
				    	 else if(ss.getMotiv2().trim().equals("2")) agrup1 = agrup;
				    	 else if(ss.getMotiv2().trim().equals("3")) agrup2 = agrup;
						 else if(ss.getMotiv2().trim().equals("4")) agrup3 = agrup;
				    

						totalAgrup = 0;
						agrup_ENE = 0;
						agrup_FEB = 0;
						agrup_MAR = 0;
						agrup_ABR = 0;
						agrup_MAY = 0;
						agrup_JUN = 0;
						agrup_JUL = 0;
						agrup_AGO = 0;
						agrup_SEP = 0;
						agrup_OCT = 0;
						agrup_NOV = 0;
						agrup_DIC = 0;

					}
				}
				agrup = new Vector();
				agrup.addElement(totalAgrup);
				agrup.addElement(agrup_ENE);
				agrup.addElement(agrup_FEB);
				agrup.addElement(agrup_MAR);
				agrup.addElement(agrup_ABR);
				agrup.addElement(agrup_MAY);
				agrup.addElement(agrup_JUN);
				agrup.addElement(agrup_JUL);
				agrup.addElement(agrup_AGO);
				agrup.addElement(agrup_SEP);
				agrup.addElement(agrup_OCT);
				agrup.addElement(agrup_NOV);
				agrup.addElement(agrup_DIC);
				agrup4 = agrup;

				agrup = new Vector();
				 agrup.addElement((Integer.parseInt(agrup20.get(0).toString()) - Integer.parseInt(agrup21.get(0).toString())));
				 agrup.addElement(Integer.parseInt(agrup20.get(1).toString()) - Integer.parseInt(agrup21.get(1).toString()));
				 agrup.addElement(Integer.parseInt(agrup20.get(2).toString()) - Integer.parseInt(agrup21.get(2).toString()));
				 agrup.addElement(Integer.parseInt(agrup20.get(3).toString()) - Integer.parseInt(agrup21.get(3).toString()));
				 agrup.addElement(Integer.parseInt(agrup20.get(4).toString()) - Integer.parseInt(agrup21.get(4).toString()));
				 agrup.addElement(Integer.parseInt(agrup20.get(5).toString()) - Integer.parseInt(agrup21.get(5).toString()));
				 agrup.addElement(Integer.parseInt(agrup20.get(6).toString()) - Integer.parseInt(agrup21.get(6).toString()));
				 agrup.addElement(Integer.parseInt(agrup20.get(7).toString()) - Integer.parseInt(agrup21.get(7).toString()));
				 agrup.addElement(Integer.parseInt(agrup20.get(8).toString()) - Integer.parseInt(agrup21.get(8).toString()));
				 agrup.addElement(Integer.parseInt(agrup20.get(9).toString()) - Integer.parseInt(agrup21.get(9).toString()));
				 agrup.addElement(Integer.parseInt(agrup20.get(10).toString()) - Integer.parseInt(agrup21.get(10).toString()));
				 agrup.addElement(Integer.parseInt(agrup20.get(11).toString()) - Integer.parseInt(agrup21.get(11).toString()));
				 agrup.addElement(Integer.parseInt(agrup20.get(12).toString()) - Integer.parseInt(agrup21.get(12).toString()));
		       
		        agrup22 = agrup;

		        agrup = new Vector();
				 agrup.addElement((Integer.parseInt(agrup1.get(0).toString()) + Integer.parseInt(agrup2.get(0).toString()) + Integer.parseInt(agrup3.get(0).toString()) + Integer.parseInt(agrup4.get(0).toString())));
				 agrup.addElement((Integer.parseInt(agrup1.get(1).toString()) + Integer.parseInt(agrup2.get(1).toString()) + Integer.parseInt(agrup3.get(1).toString()) + Integer.parseInt(agrup4.get(1).toString())));
				 agrup.addElement((Integer.parseInt(agrup1.get(2).toString()) + Integer.parseInt(agrup2.get(2).toString()) + Integer.parseInt(agrup3.get(2).toString()) + Integer.parseInt(agrup4.get(2).toString())));
				 agrup.addElement((Integer.parseInt(agrup1.get(3).toString()) + Integer.parseInt(agrup2.get(3).toString()) + Integer.parseInt(agrup3.get(3).toString()) + Integer.parseInt(agrup4.get(3).toString())));
				 agrup.addElement((Integer.parseInt(agrup1.get(4).toString()) + Integer.parseInt(agrup2.get(4).toString()) + Integer.parseInt(agrup3.get(4).toString()) + Integer.parseInt(agrup4.get(4).toString())));
				 agrup.addElement((Integer.parseInt(agrup1.get(5).toString()) + Integer.parseInt(agrup2.get(5).toString()) + Integer.parseInt(agrup3.get(5).toString()) + Integer.parseInt(agrup4.get(5).toString())));
				 agrup.addElement((Integer.parseInt(agrup1.get(6).toString()) + Integer.parseInt(agrup2.get(6).toString()) + Integer.parseInt(agrup3.get(6).toString()) + Integer.parseInt(agrup4.get(6).toString())));
				 agrup.addElement((Integer.parseInt(agrup1.get(7).toString()) + Integer.parseInt(agrup2.get(7).toString()) + Integer.parseInt(agrup3.get(7).toString()) + Integer.parseInt(agrup4.get(7).toString())));
				 agrup.addElement((Integer.parseInt(agrup1.get(8).toString()) + Integer.parseInt(agrup2.get(8).toString()) + Integer.parseInt(agrup3.get(8).toString()) + Integer.parseInt(agrup4.get(8).toString())));
				 agrup.addElement((Integer.parseInt(agrup1.get(9).toString()) + Integer.parseInt(agrup2.get(9).toString()) + Integer.parseInt(agrup3.get(9).toString()) + Integer.parseInt(agrup4.get(9).toString())));
				 agrup.addElement((Integer.parseInt(agrup1.get(10).toString()) + Integer.parseInt(agrup2.get(10).toString()) + Integer.parseInt(agrup3.get(10).toString()) + Integer.parseInt(agrup4.get(10).toString())));
				 agrup.addElement((Integer.parseInt(agrup1.get(11).toString()) + Integer.parseInt(agrup2.get(11).toString()) + Integer.parseInt(agrup3.get(11).toString()) + Integer.parseInt(agrup4.get(11).toString())));
				 agrup.addElement((Integer.parseInt(agrup1.get(12).toString()) + Integer.parseInt(agrup2.get(12).toString()) + Integer.parseInt(agrup3.get(12).toString()) + Integer.parseInt(agrup4.get(12).toString())));
					
				 agrup23 = agrup;
				 agrup = new Vector();
				 agrup.addElement(Integer.parseInt(agrup22.get(0).toString()) - Integer.parseInt(agrup23.get(0).toString()) );
				 agrup.addElement(Integer.parseInt(agrup22.get(1).toString()) - Integer.parseInt(agrup23.get(1).toString()) );
				 agrup.addElement(Integer.parseInt(agrup22.get(2).toString()) - Integer.parseInt(agrup23.get(2).toString()) );
				 agrup.addElement(Integer.parseInt(agrup22.get(3).toString()) - Integer.parseInt(agrup23.get(3).toString()) );
				 agrup.addElement(Integer.parseInt(agrup22.get(4).toString()) - Integer.parseInt(agrup23.get(4).toString()) );
				 agrup.addElement(Integer.parseInt(agrup22.get(5).toString()) - Integer.parseInt(agrup23.get(5).toString()) );
				 agrup.addElement(Integer.parseInt(agrup22.get(6).toString()) - Integer.parseInt(agrup23.get(6).toString()) );
				 agrup.addElement(Integer.parseInt(agrup22.get(7).toString()) - Integer.parseInt(agrup23.get(7).toString()) );
				 agrup.addElement(Integer.parseInt(agrup22.get(8).toString()) - Integer.parseInt(agrup23.get(8).toString()) );
				 agrup.addElement(Integer.parseInt(agrup22.get(9).toString()) - Integer.parseInt(agrup23.get(9).toString()) );
				 agrup.addElement(Integer.parseInt(agrup22.get(10).toString()) - Integer.parseInt(agrup23.get(10).toString()) );
				 agrup.addElement(Integer.parseInt(agrup22.get(11).toString()) - Integer.parseInt(agrup23.get(11).toString()) );
				 agrup.addElement(Integer.parseInt(agrup22.get(12).toString()) - Integer.parseInt(agrup23.get(12).toString()) );
				 agrup24 = agrup;															

				
				// System.out.println("vecAgrup "+agrup1);
				for (Presw25DTO ss : listaPresw25) {

					total = ss.getPres01() + ss.getPres02() + ss.getPres03()
							+ ss.getPres04() + ss.getPres05() + ss.getPres06()
							+ ss.getPres07() + ss.getPres08() + ss.getPres09()
							+ ss.getPres10() + ss.getPres11() + ss.getPres12();
					// if(total > 0){
					if (total > 0
							|| ss.getMotiv2().trim().equals(ss.getMotiv3().trim())) {
						if (!ss.getMotiv2().trim().equals(ss.getMotiv3().trim()))
							// vec_datos.add(String.valueOf(ss.getItedoc()));
							vec_datos.add(ss.getMotiv2());
						else
							vec_datos.add("");
						vec_datos.add(ss.getDesite());

						agrup = new Vector();
						if (ss.getMotiv2().trim().equals(ss.getMotiv3().trim())) {
							 if(ss.getMotiv2().trim().equals("20")) agrup = agrup20;
					    	 else if(ss.getMotiv2().trim().equals("21")) agrup = agrup21;
					    	 else if(ss.getMotiv2().trim().equals("22")) agrup = agrup22;
					    	 else if(ss.getMotiv2().trim().equals("23")) agrup = agrup23;
					    	 else if(ss.getMotiv2().trim().equals("24")) agrup = agrup24;
					    	 else if(ss.getMotiv2().trim().equals("1")) agrup = agrup1;
					    	 else if(ss.getMotiv2().trim().equals("2")) agrup = agrup2;
					    	 else if(ss.getMotiv2().trim().equals("3")) agrup = agrup3;
							 else if(ss.getMotiv2().trim().equals("4")) agrup = agrup4;	
					

							vec_datos.add(nf2.format(agrup.get(0)));
							vec_datos.add(nf2.format(agrup.get(1)));
							vec_datos.add(nf2.format(agrup.get(2)));
							vec_datos.add(nf2.format(agrup.get(3)));
							vec_datos.add(nf2.format(agrup.get(4)));
							vec_datos.add(nf2.format(agrup.get(5)));
							vec_datos.add(nf2.format(agrup.get(6)));
							vec_datos.add(nf2.format(agrup.get(7)));
							vec_datos.add(nf2.format(agrup.get(8)));
							vec_datos.add(nf2.format(agrup.get(9)));
							vec_datos.add(nf2.format(agrup.get(10)));
							vec_datos.add(nf2.format(agrup.get(11)));
							vec_datos.add(nf2.format(agrup.get(12)));
							vec_datos.add("");
						} else {
							vec_datos.add(nf2.format(total));
							vec_datos.add(nf2.format(ss.getPres01()));
							vec_datos.add(nf2.format(ss.getPres02()));
							vec_datos.add(nf2.format(ss.getPres03()));
							vec_datos.add(nf2.format(ss.getPres04()));
							vec_datos.add(nf2.format(ss.getPres05()));
							vec_datos.add(nf2.format(ss.getPres06()));
							vec_datos.add(nf2.format(ss.getPres07()));
							vec_datos.add(nf2.format(ss.getPres08()));
							vec_datos.add(nf2.format(ss.getPres09()));
							vec_datos.add(nf2.format(ss.getPres10()));
							vec_datos.add(nf2.format(ss.getPres11()));
							vec_datos.add(nf2.format(ss.getPres12()));
							vec_datos.add(ss.getComen1().toString().trim()
									+ ss.getComen2().toString().trim()
									+ ss.getComen3().toString().trim()
									+ ss.getComen4().toString().trim()
									+ ss.getComen5().toString().trim()
									+ ss.getComen6().toString().trim());
						}
						// nomUnidad = ss.getCoduni() + " - " + ss.getDesuni().trim();
						nomOrganizacion = ss.getMotiv1().trim() + " - "
								+ ss.getDesuni().trim();
						if(!ss.getMotiv3().trim().equals("5")){   // esto es para que contabilice solo los ingresos
							
							enero += ss.getPres01();
							febrero += ss.getPres02();
							marzo += ss.getPres03();
							abril += ss.getPres04();
							mayo += ss.getPres05();
							junio += ss.getPres06();
							julio += ss.getPres07();
							agosto += ss.getPres08();
							septiembre += ss.getPres09();
							octubre += ss.getPres10();
							noviembre += ss.getPres11();
							diciembre += ss.getPres12();
							totalTotal += total;
						}
					}
				}
				vec_datos.add("");
				vec_datos.add("Total $");
				vec_datos.add(nf2.format(totalTotal));
				vec_datos.add(nf2.format(enero));
				vec_datos.add(nf2.format(febrero));
				vec_datos.add(nf2.format(marzo));
				vec_datos.add(nf2.format(abril));
				vec_datos.add(nf2.format(mayo));
				vec_datos.add(nf2.format(junio));
				vec_datos.add(nf2.format(julio));
				vec_datos.add(nf2.format(agosto));
				vec_datos.add(nf2.format(septiembre));
				vec_datos.add(nf2.format(octubre));
				vec_datos.add(nf2.format(noviembre));
				vec_datos.add(nf2.format(diciembre));
				// vec_datos.add(nomUnidad);
				vec_datos.add(nomOrganizacion);
				return vec_datos;
			}
		 public Boolean getesMayorA�oPresupuestario(int fecha_hoy) {
	            Boolean respuesta = false;
	            ConexionAs400 con = new ConexionAs400();
	            try {
	                  Calendar fecha = Calendar.getInstance();
	                  
	                  
	                  int fecha_isereies = 0 ;
	                  String query = "select VALNUM  from USMMBP.COCOF16 where CODPAR =  'TP'";
	                  PreparedStatement sent = con.getConnection().prepareStatement(query);
	                  ResultSet res = sent.executeQuery();
	                  while (res.next()) {                     
	                        fecha_isereies = res.getInt(1);                
	                        }
	                  res.close();
	                  sent.close();
	                  
	                  if (fecha_isereies > fecha_hoy)
	                        respuesta = true;
	            } catch (Exception ex) {
	                  System.out.println("Error ModuloPrespuestoB.getesMayorA�oPresupuestario " + ex.getMessage());
	            }
	            
	            
	            return respuesta;
		      }
		 public Pres18POJO getDatosFund(String fund, int anno, int mes) {
			   ConexionBanner con = new ConexionBanner();
				
				Pres18POJO pres18POJO = new Pres18POJO();
				PreparedStatement sent = null;
				ResultSet res = null;
				
				try {
					String sql = "  SELECT   FTVFUND_TITLE fondo" 
								+"	FROM   FIMSMGR.FTVFUND"
								+"	WHERE FTVFUND_FUND_CODE = ?"
								+"	AND FTVFUND_DATA_ENTRY_IND   = 'Y'";
		//	System.out.println("Saldo " + sql);
					sent = con.getConexion().prepareStatement(sql);
					sent.setString(1, fund);
							

					res = sent.executeQuery();
					while (res.next()) {
						pres18POJO = new Pres18POJO();
						pres18POJO.setNompro(fund); // codigo fondo
						pres18POJO.setNomtip(res.getString(1)) ; // nombre fondo
						pres18POJO.setAcumum(0);					
												
					}
				    
					res.close();
					sent.close();
				    con.close();
				} catch (SQLException e) {
					con.close();
					System.out.println("Error getDatosFund : "+ e.getMessage());
				}
				return pres18POJO;
			}
}
