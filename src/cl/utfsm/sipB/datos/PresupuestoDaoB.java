package cl.utfsm.sipB.datos;

import java.util.List;

import cl.utfsm.base.auditoria.AuditoriaServicio;
import cl.utfsm.base.auditoria.DetalleServicio;
import cl.utfsm.base.auditoria.TipoAccionServicio;
import cl.utfsm.sip.PRESW25;
import cl.utfsm.sipB.POJO.Pres18POJO;

public interface PresupuestoDaoB {
	 public void saveAuditoriaServicio (AuditoriaServicio auditoriaServicio);
	 public TipoAccionServicio getTipoAccionServicio(int codigo);
	 public DetalleServicio getDetalleServicio(Long id);
	 public List<PRESW25> getTemporal(int unidad, int rut, int a�o);
	 public void saveTemporal(List<PRESW25> lista);
	 public List<Pres18POJO> getCuentas() throws Exception;
	 public boolean getPeriodoProcesos() throws Exception;
	 
	 

}


