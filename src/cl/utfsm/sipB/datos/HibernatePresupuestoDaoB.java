package cl.utfsm.sipB.datos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import org.hibernate.FlushMode;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cl.utfsm.base.auditoria.AuditoriaServicio;
import cl.utfsm.base.auditoria.DetalleServicio;
import cl.utfsm.base.auditoria.TipoAccionServicio;
import cl.utfsm.base.util.ConexionBanner;
import cl.utfsm.sip.PRESW25;
import cl.utfsm.sip.PeriodoProcesos;
import cl.utfsm.sipB.POJO.Pres18POJO;
import descad.presupuesto.Presw18DTO;

public class HibernatePresupuestoDaoB extends HibernateDaoSupport implements PresupuestoDaoB{
	
	public void saveAuditoriaServicio(AuditoriaServicio auditoriaServicio) {
		getHibernateTemplate().getSessionFactory().getCurrentSession().setFlushMode(FlushMode.AUTO);
		getHibernateTemplate().save(auditoriaServicio);
		getHibernateTemplate().flush();

	}
	public TipoAccionServicio getTipoAccionServicio(int codigo) {
		TipoAccionServicio tipoAccionServicio = new TipoAccionServicio();
		List resultado = getHibernateTemplate().find("from TipoAccionServicio where codigo = " + codigo);
		if (resultado.size() == 0)
			tipoAccionServicio = new TipoAccionServicio();
		else
			tipoAccionServicio = (TipoAccionServicio) resultado.get(0);
	    return tipoAccionServicio;	
	}
	public DetalleServicio getDetalleServicio(Long id) {
		DetalleServicio detalleServicio = new DetalleServicio();
		List resultado = getHibernateTemplate().find("from DetalleServicio where id = " + id);
		if (resultado.size() == 0)
			detalleServicio = new DetalleServicio();
		else
			detalleServicio = (DetalleServicio) resultado.get(0);
	    return detalleServicio;		
	    }
	public List<PRESW25> getTemporal (int unidad, int rut, int a�o){
		List<PRESW25> listaPRESW25 = new ArrayList<PRESW25>();
		List resultado = getHibernateTemplate().find("from tmpPRESW25 where TIPPRO = 'CPR' AND CODUNI = " + unidad + " AND RUTUSU = " + rut +
				" AND ANOPRE = " + a�o);
		for (int i = 0; i < resultado.size(); i++) {
			PRESW25 presw25 =  (PRESW25) resultado.get(0);
			listaPRESW25.add(presw25 );
		}	
		return listaPRESW25;
	}
	public void saveTemporal(List<PRESW25> lista){
		getHibernateTemplate().getSessionFactory().getCurrentSession().setFlushMode(FlushMode.AUTO);
		getHibernateTemplate().save(lista);
		getHibernateTemplate().flush();
	}
	public boolean getPeriodoProcesos() throws Exception {
		boolean existe = false;
		List resultado = getHibernateTemplate().find("FROM PeriodoProcesos p " +
				" WHERE p.sede.codigo = 1" + 
				" AND p.jornada.codigo = 1" +
				" AND p.codigoProceso = 46" +
				" AND TO_CHAR(p.fechaInicio, 'yyyymmdd') <=" +
                " TO_CHAR(SYSDATE, 'yyyymmdd')" +
                " AND TO_CHAR(p.fechaTermino, 'yyyymmdd') >=" +
                " TO_CHAR(SYSDATE, 'yyyymmdd') ");

				
			/*	" AND CONVERT(VARCHAR(8),p.fechaInicio,112) <=" +
                " CONVERT(VARCHAR(8),GETDATE(),112)" +
                " AND CONVERT(VARCHAR(8),p.fechaTermino,112) >=" +
                " CONVERT(VARCHAR(8),GETDATE(),112) ");*/
		if(resultado.size() > 0)
			return existe = true;
		else return existe;
	}
	public List<PeriodoProcesos> getPeriodosProceso() throws Exception {
		List<PeriodoProcesos> periodosProceso = new ArrayList<PeriodoProcesos>();
		List resultado = getHibernateTemplate().find("FROM PeriodoProcesos p   WHERE p.codigoProceso = 46 ORDER BY p.anno desc" );
		if(resultado != null && resultado.size() > 0){
			for (int i = 0; i < resultado.size(); i++) 
				periodosProceso.add((PeriodoProcesos) resultado.get(i));
			return periodosProceso;
		}	
		return null;
	}
	
	/*public List<Pres18POJO> getCuentas ()  {
		ConexionBanner conexion = new ConexionBanner();
		List<Pres18POJO> listObj = new ArrayList<Pres18POJO>();
		try {
			conexion.conexion();
			 String sql = ""
                 + " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , fg.FGBOPAL_00_adopt_bud CUENTAS"
                 + " FROM FGBOPAL fg ,ftvorgn ft"               
                 + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code";
			 
			 List resultado = getHibernateTemplate().find(sql);
				if (resultado.size() > 0)
					for (int i = 0; i < resultado.size(); i++) {
						Pres18POJO resu =  (Pres18POJO) resultado.get(0);
						listObj.add(resu);
					}
			    
			 
		} catch (Exception ex) {
			
		}
		return listObj;
	}
	*/
	public Collection<Pres18POJO> getCuentas(String rutnumdv){		        
		     System.out.println(rutnumdv);
		    ConexionBanner con = new ConexionBanner();
		    PreparedStatement sent = null;
		    ResultSet res = null;
		    String nombre ="";
		   List<Pres18POJO> lista = new ArrayList<Pres18POJO>();
		    try{		
				 String sql = ""
              + " SELECT distinct  fg.FGBOPAL_ORGN_CODE , ft.ftvorgn_title , fg.FGBOPAL_00_adopt_bud CUENTAS"
              + " FROM FGBOPAL fg ,ftvorgn ft"               
              + " where fg.FGBOPAL_ORGN_CODE = ft.ftvorgn_orgn_code";
				 //System.out.println("HibernateBanner sql " + sql);
		      sent = con.conexion().prepareStatement(sql);
		      sent.setString(1,rutnumdv);
		      res = sent.executeQuery();

		      while (res.next()){
		    	  Pres18POJO ss = new Pres18POJO();
			        ss.setDesuni(res.getString(1)); // nombre + apellidos
			        lista.add(ss);
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error HibernatePresupuestoB.getCuentas: " + e);
			}
		    return lista;
	 }
	public List<Pres18POJO> getCuentas() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
