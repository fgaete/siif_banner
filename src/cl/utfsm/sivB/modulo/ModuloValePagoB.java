package cl.utfsm.sivB.modulo;
/**
 * SISTEMA DE VALE DE PAGOS POR INTERNET
 * PROGRAMADOR		: 	M�NICA BARRERA FREZ
 * FECHA ULT.MODIF  : 	09/08/2011
 * UNIDAD DE DESARROLLO INSTITUCIONAL(DTI)   
 * MODIF   BANNER   :   09/12/2016
 */
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Vector;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import oracle.jdbc.OracleTypes;

import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;

import cl.utfsm.POJO.Sede;
import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.ConexionBD;
import cl.utfsm.base.util.ConexionBanner;
import cl.utfsm.conexion.ConexionAs400RecGasto;
import cl.utfsm.conexion.ConexionAs400ValePago;
import cl.utfsm.base.util.Util;
import cl.utfsm.siv.CUENTAPRESUPUESTARIA;
import cl.utfsm.siv.LISTAVALEPAGO;
import cl.utfsm.siv.PRESF61;
import cl.utfsm.siv.datos.ValePagoDao;
import descad.cliente.CocofBean;
import descad.cliente.Ingreso_Documento;
import descad.cliente.MD5;
import descad.cliente.PreswBean;

import descad.documentos.Cocow36DTO;
import descad.presupuesto.Cocof17DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw21DTO;
import descad.presupuesto.Presw25DTO;

public class ModuloValePagoB {
    	ValePagoDao valePagoDao;

		public ValePagoDao getValePagoDao() {
			return valePagoDao;
		}

		public void setValePagoDao(ValePagoDao valePagoDao) {
			this.valePagoDao = valePagoDao;
		}

 public Collection<Presw18DTO> getConsulta(String tippro, int codUnidad, int item, int anno, int mes, int rutIde){
	PreswBean preswbean = new PreswBean(tippro, codUnidad, item, anno, mes, rutIde);
	Collection<Presw18DTO> listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
	return listaPresw18;
 }
 public void getCuentasAutorizadas( HttpServletRequest req, int rutide){
	 HttpSession sesion = req.getSession();
	 Collection<Presw18DTO> listaPresw18 = null;
	 
	 listaPresw18 = this.getConsulta("VC1", 0, 0, 0, 0, rutide);
	 List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
		
	 for (Presw18DTO ss : listaPresw18){
		 Presw18DTO cuentapresupuestaria = new Presw18DTO();
		 cuentapresupuestaria.setNompro(ss.getNompro());
		 cuentapresupuestaria.setNomtip(ss.getNomtip());
		 cuentapresupuestaria.setDesuni(ss.getDesuni());
		 cuentapresupuestaria.setIndprc(ss.getIndprc());
		 lista.add(cuentapresupuestaria);
	 }
	 
	 
	 sesion.setAttribute("cuentasPresupuestarias", lista);
	return ;
	 }
 public boolean getAgregaDistribucionPago( HttpServletRequest req, long valorAPagar, long valorPago, String desuni){
	 HttpSession sesion = req.getSession();
	 List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudesPago");
     long total = 0;
     boolean sobrepasa = false;
 	
	if(listaSolicitudesPago != null && listaSolicitudesPago.size() > 0){
		 for (Presw18DTO ss : listaSolicitudesPago)
				total = total + ss.getUsadom();
	} else {		
		listaSolicitudesPago = new ArrayList<Presw18DTO>();
	}
	total += valorPago;
	if(total <= valorAPagar) {
		 Presw18DTO presw18DTO = new Presw18DTO();
		 presw18DTO.setDesuni(desuni);
		 presw18DTO.setUsadom(valorPago);
		 listaSolicitudesPago.add(presw18DTO);
	 } else sobrepasa = true;	 
	 
	 sesion.setAttribute("listaSolicitudesPago", listaSolicitudesPago);
	 
	return sobrepasa;
	 }	
	
     public List<Cocof17DTO> getListaSede(AccionWeb accionweb) throws Exception {
		/*	esto es en caso de que se muestren todas las sedes */
	 	    CocofBean cocofBean = new CocofBean();
		    List<Cocof17DTO> listCocofBean = new ArrayList<Cocof17DTO>();
		    listCocofBean = cocofBean.buscar_todos_cocof17();	
		  
				
		   
		return listCocofBean;
		}
 
 
	public List<Presw18DTO> getListaSede( HttpServletRequest req) throws Exception {
	/*	esto es en caso de que se muestren las sedes que puede ver el usuario */
		int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
	
		PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		List<Presw18DTO> list = new ArrayList<Presw18DTO>();
		String nombre = "";
		preswbean = new PreswBean("SED",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			   for (Presw18DTO ss : lista){
				   Presw18DTO presw18DTO = new Presw18DTO();
				   presw18DTO.setNummes(ss.getNummes());
				   presw18DTO.setDesuni(ss.getDesuni());
				   list.add(presw18DTO);
			   }	
		   
	return list;
	}
	
	public List<Sede> getListaSedeAutorizadas(int rutUsuario) throws Exception {
		List<Sede> lista = new ArrayList<Sede>();
	    ConexionAs400ValePago con = new ConexionAs400ValePago();
	    Sede sede = new Sede();
		try{
			      
			 PreparedStatement sent = con.getConnection().prepareStatement(" SELECT v1.SUCBAN, v1.DESSUC " +
					                                                       " FROM USMMBP.PRESF207 v1, USMMBP.PRESF206 v2" +
			                    									       " WHERE v1.SUCBAN = v2.SUCBAN " +
			                    										   " AND v2.RUTIDE = ? "); 	
				    	
			 sent.setInt(1,rutUsuario);
			 ResultSet res = sent.executeQuery();
	
			 while (res.next()){
				 sede = new Sede();
				 sede.setCodSuc(res.getInt(1));
				 sede.setNomSuc(res.getString(2).trim()); // 
				 lista.add(sede);
			 }
			 res.close();
			 sent.close();
			 con.getConnection().close();
		   }
		    catch (SQLException e){
			    	System.out.println("Error moduloValePagoB.getListaSedeAutorizadas: " + e);
			}
		 return lista;
		}
	 
	public List<Presw18DTO> getListaTopoPago( HttpServletRequest req) throws Exception {
	/*	se muestran todos los tipos de pago */
		int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
	
		PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		List<Presw18DTO> list = new ArrayList<Presw18DTO>();
		Vector<String> miVector = new Vector<String>();
		Vector vec = new Vector();
		Vector vec2 = new Vector();
		Vector vec3 = new Vector();
			
		String nombre = "";
		preswbean = new PreswBean("TV1",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
			lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		
			   for (Presw18DTO ss : lista){
				//   System.out.println(ss.getDesuni()+" - "+ ss.getTipmov());
				   miVector.add(ss.getDesuni());
				   vec2.add(ss.getTipmov());
				   vec3.add(ss.getNomtip());
				   vec.add(ss.getDesuni());
				 
			   }
			    Collections.sort(miVector);
			    int i = 0;
			   for(String sElemento: miVector){   
				//   System.out.println(sElemento);
				   i = 0;
				   Presw18DTO presw18DTO = new Presw18DTO();
				   presw18DTO.setDesuni(sElemento);
				   i = vec.indexOf(sElemento);
				   presw18DTO.setTipmov(vec2.get(i)+"");
				   presw18DTO.setNomtip(vec3.get(i)+"");
				   list.add(presw18DTO);				
			   }
	   
	return list;
	}
/* public void getListaSede( HttpServletRequest req){
	 HttpSession sesion = req.getSession();
		
	 
	 /*esto me lo debe enviar Rudy -- solo de prueba
	
	    List<LISTAVALEPAGO> listaValePago = new ArrayList<LISTAVALEPAGO>();
        LISTAVALEPAGO valePago = new LISTAVALEPAGO();
        valePago.setNumeroVale("1");
        valePago.setNumeroVale("001");
        valePago.setDescripcion("deascripcion");
        valePago.setDestinatario("Destinatario 1");
        valePago.setFecha(new Date("01/01/2010"));
        valePago.setEstado("Autorizado");
        valePago.setValor(20000);
        listaValePago.add(valePago);
        valePago = new LISTAVALEPAGO();
        valePago.setNumeroVale("2");
        valePago.setNumeroVale("002");
        valePago.setDescripcion("deascripcionv 2");
        valePago.setEstado("Autorizado");
        valePago.setDestinatario("Destinatario 2");
        valePago.setFecha(new Date("30/01/2011"));
        valePago.setValor(40000);
        listaValePago.add(valePago);
        
        valePago = new LISTAVALEPAGO();
        valePago.setNumeroVale("3");
        valePago.setNumeroVale("003");
        valePago.setDescripcion("deascripcion 3");
        valePago.setDestinatario("Destinatario 3");
        valePago.setEstado("Autorizado");
        valePago.setFecha(new Date("03/02/2011"));
        valePago.setValor(30000);
        listaValePago.add(valePago);	 
        
        
	 sesion.setAttribute("listaValePago", listaValePago);*/
/*	return;
	 }*/
 public boolean getIdentificadorDocumento(String identificador, int sede){
		// falta que Rudy indique si existe
	    // la consulta debe realizarse en la tabla cocof100, buscar si existe este identificador en el campo idmemo
	 // lo realizar� con el proceso VMI
	    PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		preswbean = new PreswBean("VMI",0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
		Presw25DTO presw25 = new Presw25DTO();
		presw25.setNumreq(sede);
		presw25.setComen1(identificador);
		lista = (Collection<Presw18DTO>) preswbean.ingreso_doc_presw25(presw25);
	
	    boolean existe = false;
	    if(lista != null && lista.size() > 0)
	    	existe = true;
	 return existe;
	}
 /*
 public Collection<Presw18DTO> getVerificaRut(int rutnum, String dv){
	  	PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		if(rutnum > 0){
			preswbean = new PreswBean("VRU",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
			lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		}
	
	 return lista;
} */
 
 public String getVerificaRut(String rutnumdv){
	   // System.out.println(rutnumdv);
	    ConexionBanner con = new ConexionBanner();
	    String nombre ="";
	    
	    String rut = rutnumdv.substring(0,rutnumdv.length()-1);
	    String dig = rutnumdv.substring(rutnumdv.length()-1,rutnumdv.length());
	    if (dig.equals("k")) rutnumdv = rut+"K";
	    
	    try{
	      /*segun correo de fernando caceres todos deben validarse en FTVVEND*/
	      PreparedStatement sent = con.conexion().prepareStatement(" SELECT spbpers_legal_name FROM spriden, spbpers, FTVVEND " +
	    		                                                   " WHERE spriden_id = ? " +
	    		                                                   " AND spriden_pidm = spbpers_pidm " +
	    		                                                   " AND  spriden_PIDM = FTVVEND_PIDM" +
	    		                                                   " AND spriden_change_ind is null");
	      sent.setString(1,rutnumdv);
	      ResultSet res = sent.executeQuery();

	      if (res.next()){
	        nombre = res.getString(1); // nombre mas apellidos
	      }
	      res.close();
	      sent.close();
	      con.close();
	    }
	    catch (SQLException e){
	    	System.out.println("Error moduloValePagoB.getVerificaRut: " + e);
		}
	 return nombre;
 }
 
 public int getVerificaPidm(String rutnumdv){
	    ConexionBanner con = new ConexionBanner();
	    int pidm = 0;
	    
	    String rut = rutnumdv.substring(0,rutnumdv.length()-1);
	    String dig = rutnumdv.substring(rutnumdv.length()-1,rutnumdv.length());
	    if (dig.equals("k")) rutnumdv = rut+"K";
	    try{
	      
	      PreparedStatement sent = con.conexion().prepareStatement("SELECT spriden_pidm FROM spriden, spbpers " +
	    		                                                   "WHERE spriden_id = ? " +
	    		                                                   "AND spriden_pidm = spbpers_pidm " +
	    		                                                   "AND spriden_change_ind is null");
	      sent.setString(1,rutnumdv);
	      ResultSet res = sent.executeQuery();

	      if (res.next()){
	        pidm = res.getInt(1); // pidm
	      }
	      res.close();
	      sent.close();
	      con.close();
	    }
	    catch (SQLException e){
	    	System.out.println("Error moduloValePagoB.getVerificaPidm: " + e);
		}
	 return pidm;
}
 
 public String getValorDeVale(int numero){
	    //ConexionBD con = new ConexionBD();
	    ConexionAs400ValePago con = new ConexionAs400ValePago();
	    String valor = "0";
	    try{
	      
	   /*   PreparedStatement sent = con.conexion().prepareStatement("SELECT valval FROM VIEW_COCOF100B " +
	    		                                                   "WHERE numval = ? " +
	    		                                                   "AND ESTVAL = 'A'"); */
	      PreparedStatement sent = con.getConnection().prepareStatement("SELECT SUM(valval) FROM USMMBP.COCOF100B " +
                    									    			"WHERE numval = ? " +
                    										    		"AND ESTVAL = 'A'"); 	
	    	
	      sent.setInt(1,numero);
	      ResultSet res = sent.executeQuery();

	      if (res.next()){
	        valor = res.getString(1); // valor de vale
	      }
	      res.close();
	      sent.close();
	     // con.close();
	      con.getConnection().close();
	    }
	    catch (SQLException e){
	    	System.out.println("Error moduloValePagoB.getValorDeVale: " + e);
		}
	 return valor;
}
 public String getValorDeValeSobregiro(int numero){
	    //ConexionBD con = new ConexionBD();
	    ConexionAs400ValePago con = new ConexionAs400ValePago();
	    String valor = "0";
	    try{
	      
	   /*   PreparedStatement sent = con.conexion().prepareStatement("SELECT valval FROM VIEW_COCOF100B " +
	    		                                                   "WHERE numval = ? " +
	    		                                                   "AND ESTVAL = 'A'"); */
	      PreparedStatement sent = con.getConnection().prepareStatement("SELECT SUM(valval) FROM USMMBP.COCOF100B " +
                 									    			"WHERE numval = ? " +
                 										    		"AND ESTVAL = 'T'"); 	
	    	
	      sent.setInt(1,numero);
	      ResultSet res = sent.executeQuery();

	      if (res.next()){
	        valor = res.getString(1); // valor de vale
	      }
	      res.close();
	      sent.close();
	     // con.close();
	      con.getConnection().close();
	    }
	    catch (SQLException e){
	    	System.out.println("Error moduloValePagoB.getValorDeVale: " + e);
		}
	 return valor;
}
 public String getRutOperacion(int numero, String tipOpe){
	    //ConexionBD con = new ConexionBD();
	    ConexionAs400ValePago con = new ConexionAs400ValePago();
	    String rutDv = "";
	    try{
	      
	      PreparedStatement sent = con.getConnection().prepareStatement(" SELECT RUTIDE,DIGIDE FROM USMMBP.COCOF102B " +
                 									    				" WHERE numval = ? " +
                 										    			" AND TIPOPE = ? "); 	
	    	
	      sent.setInt(1,numero);
	      sent.setString(2,tipOpe);
	      ResultSet res = sent.executeQuery();

	      while (res.next()){
	        rutDv = res.getInt(1)+""+res.getString(2); // rut operaci�n
	      }
	      res.close();
	      sent.close();
	     // con.close();
	      con.getConnection().close();
	    }
	    catch (SQLException e){
	    	System.out.println("Error moduloValePagoB.getRutOperacion: " + e);
		}
	 return rutDv;
}
 
 public String getDatoCOCOF102B(int numero){
	   // ConexionBD con = new ConexionBD();
	    ConexionAs400ValePago con = new ConexionAs400ValePago();
	    String fecha = "";
	    try{
	      
	    /*  PreparedStatement sent = con.conexion().prepareStatement("SELECT MIN(fecope) " +
	    		                                                   "FROM VIEW_COCOF102B " +
                                                                   "WHERE numval = ? " +
                                                                   "AND tipope = 'I'"); */
	     PreparedStatement sent = con.getConnection().prepareStatement("SELECT MIN(fecope) " +
                                                                       "FROM USMMBP.COCOF102B " +
                                                                       "WHERE numval = ? " +
                                                                       "AND tipope = 'I'");	
	      sent.setInt(1,numero);
	      ResultSet res = sent.executeQuery();

	      if (res.next()){
	        fecha = res.getString(1); // fecha mas antigua del vale con tipo de operacion I
	      }
	      res.close();
	      sent.close();
	     // con.close();
	      con.getConnection().close();
	    }
	    catch (SQLException e){
	    	System.out.println("Error moduloValePagoB.getDatoCOCOF102B: " + e);
		}
	 return fecha;
}
  
 public Vector getDatosAS(int numero){
    // ConexionBD con = new ConexionBD();
	 ConexionAs400ValePago con = new ConexionAs400ValePago();
     Vector vec = new Vector();
     Vector detalle = null;
     
     try {
       	PreparedStatement sent = con.getConnection().prepareStatement("SELECT distinct v2.codfon, v1.codorg, v3.cueban, v2.codpro,v1.valasi, v.numval, " +
                                                                      "v.glode1, v.glode2, v.tipval, v3.desval, COALESCE(v4.rutpag,v.rutide), COALESCE(v4.digpag,v.digide), " +
                                                                      " 0, v.rutide, v.digide , v.numval " +
                                                                      "FROM USMMBP.COCOF100B v LEFT JOIN TRANSFERBT.COCOF103 v4 ON v.numval = v4.numval, USMMBP.COCOF101B v1, USMMBP.PRESF200 v2 , " +
                                                                      "USMMBP.COCOF105B v3 " +
                                                                      "WHERE v.numval = ? " + 
                                                                      "AND v.numval = v1.numval " +
                                                                      "AND v1.codorg = v2.codorg " +
                                                                      "AND v.tipval = v3.tipval "); 
        sent.setInt(1,numero);
	    ResultSet res = sent.executeQuery();

	    while (res.next()) { 
	       detalle = new Vector();
	       detalle.add(res.getString(1).trim()); // 0 codigo del fondo;
		   detalle.add(res.getString(2).trim()); // 1 codigo de la organizacion;
		   detalle.add(res.getString(3).trim()); // 2 codigo de cuenta
		   detalle.add(res.getString(4).trim()); // 3 codpro
		   detalle.add(res.getString(5).trim()); // 4 valor del vale
		   detalle.add(res.getString(6).trim()); // 5 numero de vale
		   detalle.add(res.getString(7).trim()+" " + res.getString(8).trim()); // 6 descripcion del vale
		   detalle.add(res.getString(9).trim()); // 7 tipo de pago;
		   detalle.add(res.getString(10).trim()); // 8 Descricion del tipo de pago
		   detalle.add(res.getString(11).trim()); // 9 Rut a  vale
		   detalle.add(res.getString(12).trim()); // 10 Digito a vale
		   detalle.add(res.getString(13).trim()); // 11 impuesto
		   detalle.add(res.getString(14).trim()+res.getString(15).trim()); // 12 rut a pago   
		   detalle.add(res.getString(16).trim()); // 13 numdoc
		   
		   detalle.add("0"); // 11 valor del impuesto
		//   detalle.add(this.getBancoOperacion(res.getString(2))); // 11 codigo banco
		   vec.add(detalle);
	     }
	     res.close();
	     sent.close();
	     // con.close();
	     con.getConnection().close();

	    }
	    catch (SQLException e ) {
	      System.out.println("Error en moduloValePagoB.getDatosAS : " + e );
	    }
	    return vec;
}
 
/* public long getBuscaSaldo(int codUni){
	  	PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		long saldoCuenta = 0;
		preswbean = new PreswBean("SCU",0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
		preswbean.setCoduni(codUni);
			lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			   for (Presw18DTO ss : lista){
				   saldoCuenta = ss.getUsadom();
			   }	
	 return saldoCuenta;
	}*/
 
 /*public long getBuscaSaldo(String codOrg, String codCuenta){
	  ConexionBanner con = new ConexionBanner();
	  long saldoCuenta = 0;
	  try{
	  /* mb - 17/04/2017 debido a que el saldo de la cuenta no debe ser lo del mes actual sino lo acumulado hasta el mes actual
	   *  PreparedStatement sent = con.conexion().prepareStatement("SELECT (fgbbavl_sum_adopt_bud+(fgbbavl_sum_bud_adjt-fgbbavl_sum_ytd_actv-fgbbavl_sum_encumb-fgbbavl_sum_bud_rsrv)) " +
	    		                                                 "FROM fgbbavl t1 " +
                                                                 "WHERE t1.fgbbavl_coas_code = 'S' " + 
                                                                 "AND t1.fgbbavl_orgn_code =  ? " +
                                                                 "AND t1.fgbbavl_acct_code = ? " +
                                                                 "AND t1.fgbbavl_activity_date = (SELECT MAX(t2.fgbbavl_activity_date) " + 
                                                                 "FROM fgbbavl t2 " +
                                                                 "WHERE t1.fgbbavl_coas_code = t2.fgbbavl_coas_code " +
                                                                 "AND t1.fgbbavl_orgn_code = t2.fgbbavl_orgn_code " +

                                                                 "AND t1.fgbbavl_acct_code = t2.fgbbavl_acct_code)");*/
		
		//  System.out.println("mesActual: "+mesActual+ " a�oActual: "+a�oActual+" codOrg: "+codOrg+" codCuenta: "+codCuenta);
/*		  String query =   "SELECT sum(fgbbavl_sum_adopt_bud+(fgbbavl_sum_bud_adjt-fgbbavl_sum_ytd_actv-fgbbavl_sum_encumb-fgbbavl_sum_bud_rsrv)) " +
		                   " FROM fgbbavl t1 " +
		                   " WHERE t1.fgbbavl_coas_code = 'S'" + 
		                   " AND t1.fgbbavl_orgn_code =  ? " +
		                   " AND t1.fgbbavl_acct_code = ? " +
		                   " AND FGBBAVL_PERIOD <= '" + mesActual+ "'" +
		                   " AND FGBBAVL_FSYR_CODE = '" + a�oActual+ "'" +
		                   " ORDER BY FGBBAVL_PERIOD";*/
		//este cambio lo solicit� fernando caceres 7/5/2018 
		/*  if(codOrg.trim().equals("RETNOM"))
			  codCuenta = "6AD001";*/
/*	  
		  String mesActual = this.getMesActual();
		  String a�oActual = this.getAnioActual();
		  String fondo = this.getFondoSaldo(codOrg);
		//  System.out.println("fondo: "+ fondo);
		  String query =   
		  " SELECT sum(fgbbavl_sum_adopt_bud+(fgbbavl_sum_bud_adjt-fgbbavl_sum_ytd_actv-fgbbavl_sum_encumb-fgbbavl_sum_bud_rsrv)) " +
          " FROM fgbbavl t1 " +
          " WHERE t1.fgbbavl_coas_code = 'S'" + 
          " AND t1.fgbbavl_orgn_code =  ? " +
          " AND t1.fgbbavl_acct_code = ? " +
          " AND FGBBAVL_PERIOD <= '" + mesActual+ "'" +
          " AND FGBBAVL_FSYR_CODE = '" + a�oActual+ "'" +
         " ORDER BY FGBBAVL_PERIOD";
		// System.out.println("saldo: "+query + "  codOrg: " + codOrg+ " - "+codCuenta);
		  PreparedStatement sent = con.conexion().prepareStatement(query);
		  sent.setString(1,codOrg);
		  sent.setString(2,codCuenta);
		  ResultSet res = sent.executeQuery();

	     if (res.next()){
	    	 saldoCuenta = (res.getString(1) != null && !res.getString(1).trim().equals("null"))?res.getLong(1):0; // Saldo
	    	 
	      } else saldoCuenta = 0;
	     res.close();
	     sent.close();
	     if(!fondo.trim().equals("")) {
	      query =   
			  " SELECT SUM(FGBTRNH_TRANS_AMT) AS TOTAL " +
			  " FROM FIMSMGR.FGBTRNH FGBTRNH,FIMSMGR.FTVORGN FTVORGN,SATURN.SPRIDEN SPRIDEN " +
			  " WHERE  ( FTVORGN.FTVORGN_ORGN_CODE = FGBTRNH.FGBTRNH_ORGN_CODE " +
			  " AND FTVORGN.FTVORGN_FUND_CODE_DEF = FGBTRNH.FGBTRNH_FUND_CODE " +
			  " AND FTVORGN.FTVORGN_PROG_CODE_DEF = FGBTRNH.FGBTRNH_PROG_CODE " +
			  " AND FGBTRNH.FGBTRNH_VENDOR_PIDM = SPRIDEN.SPRIDEN_PIDM ) " +
			  " AND ( FGBTRNH.FGBTRNH_LEDC_CODE ='FINANC' " +
			  " AND FGBTRNH.FGBTRNH_ACCT_CODE LIKE ('11A%') " +
			  " AND FGBTRNH.FGBTRNH_RUCL_CODE IN ('APS1','APS2','APS3','APS4','CHS1','CSS1') " +
			  " AND FGBTRNH.FGBTRNH_FSYR_CODE = '" + a�oActual+ "' ) " +
			  " AND FGBTRNH_ORGN_CODE = '"+ codOrg.trim() +"'" +
			  " AND FGBTRNH_ACCI_CODE = '"+ codCuenta.trim() +"'" +
			  " AND FGBTRNH_FUND_CODE = '" + fondo.trim() + "'" +
			  " AND FGBTRNH.FGBTRNH_POSTING_PERIOD <= '" + mesActual+ "'" +
			  " AND FGBTRNH_FSYR_CODE = '" + a�oActual+ "'" +
			  " ORDER BY FGBTRNH.FGBTRNH_POSTING_PERIOD, " +
              " FGBTRNH.FGBTRNH_DOC_CODE, " +
              " FGBTRNH.FGBTRNH_SEQ_NUM"; 
		//	 System.out.println("saldo: "+query + "  codOrg: " + codOrg+ " - "+codCuenta);
			  PreparedStatement sent2 = con.conexion().prepareStatement(query);
			  ResultSet res2 = sent2.executeQuery();
		     if (res2.next()){
		    	saldoCuenta += (res.getString(1) != null && !res.getString(1).trim().equals("null"))?res2.getLong(1):0; // Saldo
		     } else saldoCuenta += 0;
		     res2.close();
		     sent2.close(); 
	     }
	     con.close();
	     }
	    catch (SQLException e){
	    	System.out.println("Error moduloValePagoB.getBuscaSaldo: " + e);
		} 

	 return saldoCuenta;
}*/

 public long getBuscaSaldo(String codOrg, String codCuenta){
 ConexionBanner con = new ConexionBanner();
 int error = -1;
 long saldoCuenta = 0;
 String codFondo = "";
  
 try{ //invoice_dateDATE, pmt_due_dateDATE, trans_dateDATE,doct_codeVARCHAR2,datos_farinvaVARCHAR2
	 codFondo = this.getFondoSaldo(codOrg);  // MB para prod que no se calcule hasta que no quede aprobado
	  
	 String sql = "{?=call PKG_VALIDACIONES.F_CALCULO_SALDO(?,?,?)}";
	
     CallableStatement cstmt = con.getConexion().prepareCall(sql);
     cstmt.registerOutParameter(1, Types.INTEGER);
     cstmt.setString(2,codOrg);
     cstmt.setString(3, codCuenta);
     cstmt.setInt(4,(codFondo.trim().equals("")?0:1));
	
	 cstmt.executeQuery();
	       
	 saldoCuenta  = cstmt.getLong(1);
	       
	 cstmt.close();
     con.close();
    }
   catch (SQLException e){
   	System.out.println("Error moduloValePagoB.getBuscaSaldo: " + e);
   	error = -5;

	} 
   
   return saldoCuenta;
 }
 
 public String getFondoSaldo(String organizacion){
		 ConexionAs400ValePago con = new ConexionAs400ValePago();
		 String fondo = "";
	     
	     try {
	       	PreparedStatement sent = con.getConnection().prepareStatement("SELECT pr1.codfon" +
	       																" FROM USMMBP.PRESF200 pr1" +
	       																" WHERE pr1.CODFON IN (SELECT pr2.CODFON FROM USMMBP.PRESF112 pr2" +
	       																"                          WHERE pr1.CODFON = pr2.CODFON)" +                         
	       																" AND pr1.CODORG = ?"); 
	        sent.setString(1,organizacion);
		    ResultSet res = sent.executeQuery();

		    if (res.next()) { 
		    	fondo = res.getString(1);
		      }
		     res.close();
		     sent.close();
		     // con.close();
		     con.getConnection().close();

		    }
		    catch (SQLException e ) {
		      System.out.println("Error en moduloValePagoB.getFondoSaldo : " + e );
		    }
		    return fondo;
	}
 
 String codigoBanerVale = "";
 synchronized public int saveCuentasPorPagar(int rutUsuario, String dvUsuario, String numvale, String fecha_invoice,String comen_vale,String tip_val, String desc_val, 
		                                     String valor_vale,String datos, String ingresador, String autorizador, String rutProv, String rutDvDAF, int tipoAut) throws Exception {
//	   System.out.println("saveCuentasPorPagar: ");  
	  ConexionBanner con = new ConexionBanner();
	  CallableStatement sent;
	  int error = -1;
	  String codigoBaner = "";
	  codigoBanerVale = "";
	  String rutdv = String.valueOf(rutUsuario)+dvUsuario;
	     System.out.println("P_USM_CXP_SIIF( "+"'"+rutdv+"','I','"+numvale+"','"+fecha_invoice+"','"+comen_vale+"','"+tip_val+"','"+desc_val+"',"+valor_vale+
	    		            ",'"+datos+"',:v_cod_error,:v_cod_banner,'','"+ingresador+"','"+autorizador+"','"+rutProv+"','"+rutDvDAF+"',"+numvale);
	  try{ //invoice_dateDATE, pmt_due_dateDATE, trans_dateDATE,doct_codeVARCHAR2,datos_farinvaVARCHAR2
		   
		   sent = con.conexion().prepareCall("{call P_USM_CXP_SIIF(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
		   sent.setString(1,rutdv);
	       sent.setString(2,"I");
	       sent.setString(3, numvale);
	       sent.setString(4,fecha_invoice);
	       sent.setString(5,comen_vale);
	       sent.setString(6,tip_val);
	       sent.setString(7,desc_val);
	       sent.setString(8,valor_vale);
	       sent.setString(9,datos);
	       sent.registerOutParameter(10, Types.INTEGER);
	       sent.registerOutParameter(11, Types.VARCHAR);
	       sent.setString(12, "");
	       sent.setString(13,ingresador);
	       sent.setString(14, autorizador);
	       sent.setString(15, rutProv);
	       sent.setString(16,rutDvDAF);
	       sent.setString(17,numvale);
			     
	       sent.executeQuery();
	       error = sent.getInt(10);
	       codigoBaner = sent.getString(11);
	       this.setCodigoBanerVale(codigoBaner);
	     //   System.out.println("call P_USM_CXP(" + rutdv+", I," +numvale+","+fecha_invoice+","+comen_vale+","+tip_val+","+desc_val+","+valor_vale+","+datos+")");
	   	
	      // System.out.println("codigoBaner: "+codigoBaner + " error: "+error);  
	       sent.close(); 
	       con.close();
	     }
	    catch (SQLException e){
	    	System.out.println("Error moduloValePagoB.saveCuentasPorPagar: " + e);
	    	error = -5;
	
		} 
	    
	    if(error == 0) {
	         Calendar fecha = new GregorianCalendar();
		     int a�o = fecha.get(Calendar.YEAR);
		     int mes = fecha.get(Calendar.MONTH)+1;
		     int dia = fecha.get(Calendar.DAY_OF_MONTH);
		     int hora = fecha.get(Calendar.HOUR_OF_DAY);
		     int minuto = fecha.get(Calendar.MINUTE);
		     int segundo = fecha.get(Calendar.SECOND);
		     String mestexto = (mes > 9)?String.valueOf(mes):"0"+String.valueOf(mes);
		     String diatexto = (dia > 9)?String.valueOf(dia):"0"+String.valueOf(dia);
		     String mintexto = (minuto > 9)?String.valueOf(minuto):"0"+String.valueOf(minuto);
		     String segtexto = (segundo > 9)?String.valueOf(segundo):"0"+String.valueOf(segundo); 
		     int fechaAc = Integer.parseInt(String.valueOf(a�o) + mestexto + diatexto);
		     int horaAc = Integer.parseInt(String.valueOf(hora) + mintexto + segtexto);
	         

	         ConexionAs400ValePago con2 = new ConexionAs400ValePago();
	         
	         if (tipoAut == 17) {
	        	 try {
		              
		              PreparedStatement sent2 = con2.getConnection().prepareStatement(" INSERT INTO USMMBP.COCOF102B (" +                                           
																							  " NUMVAL, " +
																							  " FECOPE ," + 
																							  " HOROPE ," + 
																							  " TIPOPE ," + 
																							  " RUTIDE ," + 
																							  " DIGIDE ," + 
																							  " CODORG ," + 
																							  " COMENT )" +           
																							  " VALUES(?,?,?,?,?,?,?,?)");
					
					 sent2.setString(1,numvale);
					 sent2.setInt(2, fechaAc);
					 sent2.setInt(3, horaAc);
					 sent2.setString(4, "S");
					 sent2.setInt(5, Integer.parseInt(rutDvDAF.substring(0,rutDvDAF.length()-1)));
			         sent2.setString(6, rutDvDAF.substring(rutDvDAF.length()-1));
					 //sent2.setInt(5, rutUsuario);
					 //sent2.setString(6, dvUsuario);
					 sent2.setString(7, " ");
					 sent2.setString(8, " ");
						            
		           
		    	    int res = sent2.executeUpdate();
	                if(res < 0){
	                 	error = -14;    	  
	                }
	            }
	        	 catch (SQLException e ) {
     	    	      System.out.println("Error en moduloValePagoB.saveCuentasPorPagar COCOF102B graba estado de sobregiro : " + e );
                     error = -15;  
                       
     	    	    } 
	         }
	         
	         if(error == 0) {
	         	         
	         try {
	              
	              PreparedStatement sent2 = con2.getConnection().prepareStatement(" INSERT INTO USMMBP.COCOF102B (" +                                           
																						  " NUMVAL, " +
																						  " FECOPE ," + 
																						  " HOROPE ," + 
																						  " TIPOPE ," + 
																						  " RUTIDE ," + 
																						  " DIGIDE ," + 
																						  " CODORG ," + 
																						  " COMENT )" +           
																						  " VALUES(?,?,?,?,?,?,?,?)");
				
				 sent2.setString(1,numvale);
				 sent2.setInt(2, fechaAc);
				 sent2.setInt(3, horaAc);
				 sent2.setString(4, "D");
				 sent2.setInt(5, Integer.parseInt(rutDvDAF.substring(0,rutDvDAF.length()-1)));
		         sent2.setString(6, rutDvDAF.substring(rutDvDAF.length()-1));
				 //sent2.setInt(5, rutUsuario);
				 //sent2.setString(6, dvUsuario);
				 sent2.setString(7, " ");
				 sent2.setString(8, codigoBaner);
					            
	           
	    	    int res = sent2.executeUpdate();
                if(res < 0){
                 	error = -12;    	  
                }
                else {
                	try {	
                		sent2 = con2.getConnection().prepareStatement(" UPDATE USMMBP.COCOF100B SET ESTVAL = ? " +
                				                                      " WHERE NUMVAL = ? ");
                		sent2.setString(1,"D");
                		sent2.setString(2,numvale);
                	
                		int res2 = sent2.executeUpdate();
                		if(res2 < 0){
                			error = -13;    	  
                		}
                      }
                    catch (SQLException e ) {
      	    	      System.out.println("Error en moduloValePagoB.saveCuentasPorPagar sobregiro COCOF102B : " + e );
                      error = -6;  
                        
      	    	    } 
                 }
	    	     sent2.close();
	    	     // con.close();
	    	     con2.getConnection().close();

	    	    }
	    	    catch (SQLException e ) {
	    	      System.out.println("Error en moduloValePagoB.saveCuentasPorPagar sobregiro  COCOF100B : " + e );
                  error = -6;  
                  
	    	    } 
	         }  
	       }
	
	 return error;
}
 
 public int getAgregaListaSolicitaVale( HttpServletRequest req, long saldoCuenta){
	 HttpSession sesion = req.getSession();
     long valor = Util.validaParametro(req.getParameter("valor"), 0);
     long valorAPagar = Util.validaParametro(req.getParameter("valorAPagar"), 0);
   //  int cuentaPresup = Util.validaParametro(req.getParameter("cuentaPresup"), 0);
     String cuentaPresup = Util.validaParametro(req.getParameter("cuentaPresup"),"");
 	 String desuni = Util.validaParametro(req.getParameter("desuni"),"");
     
	 List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudes");
	 if(listaSolicitudes != null && listaSolicitudes.size()> 0)
		 sesion.removeAttribute("listaSolicitudes");
	 long total = 0;
	 long totalTotal = 0;
	 int sobrepasa = 0;
	 String muestraSaldo = "";
	 NumberTool numbertool = new NumberTool();
	 muestraSaldo = String.valueOf(numbertool.format(saldoCuenta));
	 if(listaSolicitudes != null && listaSolicitudes.size() > 0){
		 for (Presw18DTO ss: listaSolicitudes){
			// if(ss.getCoduni() == cuentaPresup)
			 if(ss.getNompro().trim().equals(cuentaPresup.trim()))
				 sobrepasa = 3;
			 totalTotal += ss.getUsadom();
		 }
		 totalTotal += valor;
	 } else total = valor;
	 
	 if(sobrepasa == 0) {
	 // if(total > saldoCuenta){ MAA se cambio por la siguiente condicion porque la variable total no siempre tomada en valor 
	 if(valor > saldoCuenta){
			 sobrepasa = 1;
			 muestraSaldo = "<font class=\"Estilo_Rojo\" >" + muestraSaldo + "</font>";
		 }
	// else { lo dej� comentado, ya que la petici�n fue solo enviar mensaje en caso de que el saldo no alcance
		 if(totalTotal <= valorAPagar) {
		if(listaSolicitudes == null)
			listaSolicitudes = new ArrayList<Presw18DTO>();
		 Presw18DTO presw18DTO = new Presw18DTO();
		// presw18DTO.setCoduni(cuentaPresup);
		 presw18DTO.setNompro(cuentaPresup);
		 presw18DTO.setUsadom(valor);
		 presw18DTO.setDesuni(desuni);
		 presw18DTO.setNomtip(muestraSaldo); // MAA lo vamos a guardar en la lista para mostrarlo en el detallesolicitudVale.vm
		// presw18DTO.setNompro(muestraSaldo); // MAA ya no se puede usar este porque este se usa para la organizaci�n 
		 
		 listaSolicitudes.add(presw18DTO);
		 } else sobrepasa = 2;
	 //}
	 }
	 sesion.setAttribute("listaSolicitudes", listaSolicitudes);
	return sobrepasa;	
 }
 public void getEliminaListaSolicitaVale( HttpServletRequest req){
	 HttpSession sesion = req.getSession();
     long valor = Util.validaParametro(req.getParameter("usadom"), 0);
     String cuentaPresup = Util.validaParametro(req.getParameter("codorg"),"");
     String desuni = Util.validaParametro(req.getParameter("desuni"),"");
     
	 List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudes");
	 List<Presw18DTO> listaVales = new ArrayList<Presw18DTO>();
	 if(listaSolicitudes != null && listaSolicitudes.size()> 0)
		 sesion.removeAttribute("listaSolicitudes");
	 if(listaSolicitudes != null && listaSolicitudes.size() > 0){
		 for (Presw18DTO ss: listaSolicitudes){
			 if(ss.getNompro().trim().equals(cuentaPresup.trim()) && ss.getUsadom() == valor && ss.getDesuni().trim().equals(desuni.trim()))
				 continue;
			 else
			 {
				 Presw18DTO presw18DTO = new Presw18DTO();
				 presw18DTO.setCoduni(ss.getCoduni()); 
				 presw18DTO.setUsadom(ss.getUsadom());
				 presw18DTO.setDesuni(ss.getDesuni());
				 presw18DTO.setDesite(ss.getDesite());
				 presw18DTO.setNompro(ss.getNompro());
				 presw18DTO.setNomtip(ss.getNomtip());
				 listaVales.add(presw18DTO);
			 }
		 }
		 
	 }
	// if(listaVales.size() > 0)
	   sesion.setAttribute("listaSolicitudes", listaVales);		
 }
 public void getEliminaListaSolicitaPago( HttpServletRequest req){
	 HttpSession sesion = req.getSession();
     long valor = Util.validaParametro(req.getParameter("usadom"), 0);
     String desuni = Util.validaParametro(req.getParameter("desuni"),"");
     
	 List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO> )sesion.getAttribute("listaSolicitudesPago");
	 List<Presw18DTO> listaVales = new ArrayList<Presw18DTO>();
	 if(listaSolicitudesPago != null && listaSolicitudesPago.size()> 0)
		 sesion.removeAttribute("listaSolicitudesPago");
	 if(listaSolicitudesPago != null && listaSolicitudesPago.size() > 0){
		 for (Presw18DTO ss: listaSolicitudesPago){
			 if(ss.getUsadom() == valor && ss.getDesuni().trim().equals(desuni.trim()))
				 continue;
			 else
			 {
				 Presw18DTO presw18DTO = new Presw18DTO();
				 presw18DTO.setUsadom(ss.getUsadom());
				 presw18DTO.setDesuni(ss.getDesuni());
				 presw18DTO.setRutide(ss.getRutide());
				 presw18DTO.setIddigi(ss.getIddigi());
				 listaVales.add(presw18DTO);
			 }
		 }
		 
	
	 }
	 if(listaVales.size() > 0)
	   sesion.setAttribute("listaSolicitudesPago", listaVales);
		
 }
 public boolean getEliminaValePago( int rut, String dv, int numdoc, String nomTipo, String tipCue ){
	  	PreswBean preswbean = null;
		Collection<Presw18DTO> lista = null;
		long saldoCuenta = 0;
		preswbean = new PreswBean(nomTipo,0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
		preswbean.setRutide(rut);
		preswbean.setDigide(dv);
		preswbean.setNumdoc(numdoc);
		preswbean.setTipcue(tipCue);
		
		return preswbean.ingreso_presw19();	
		
 } 
 
 public List<Cocow36DTO> getAgregaLista(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf){
	 Cocow36DTO cocow36DTO = new Cocow36DTO();
	 cocow36DTO.setTiping(tiping);
	 cocow36DTO.setNomcam(nomcam);
	 cocow36DTO.setValnu1(valnu1);
	 cocow36DTO.setValnu2(valnu2);
	 cocow36DTO.setValalf(valalf);
	 lista.add(cocow36DTO);
	 return lista;
	 
 }
 public List<Cocow36DTO> getAgregaListaMD5(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf, String resval){
	 Cocow36DTO cocow36DTO = new Cocow36DTO();
	 cocow36DTO.setTiping(tiping);
	 cocow36DTO.setNomcam(nomcam);
	 cocow36DTO.setValnu1(valnu1);
	 cocow36DTO.setValnu2(valnu2);
	 cocow36DTO.setValalf(valalf);
	 cocow36DTO.setResval(resval);
	 lista.add(cocow36DTO);
	 return lista;
	 
 }
 public List<Integer> getListaA�o(int a�oActual) {
	 List<Integer> lista = new ArrayList<Integer>();
	
	 for(int i = 2010 ; i <= a�oActual;i++){
		 lista.add(i);
	 }
	  return lista;
   }
 
  public boolean saveEstadoCuenta(int numdoc, String codOrg, int rutUsuario, String digide, String estado){
		PreswBean preswbean = new PreswBean("AUZ",0,0,0,0, rutUsuario);
		preswbean.setDigide(digide);
		preswbean.setNumdoc(numdoc);
		preswbean.setCajero(codOrg);
		preswbean.setTipcue(estado);
	return preswbean.ingreso_presw19();
	}
  
  public boolean saveEstadoVale(int numdoc,int rutUsuario,String digide){
		PreswBean preswbean = new PreswBean("CEV",0,0,0,0, rutUsuario);
		preswbean.setDigide(digide);
		preswbean.setNumdoc(numdoc);

	return preswbean.ingreso_presw19();
	}
 
	public boolean saveAutoriza(int numdoc, int rutUsuario, String digide, String nomTipo, String tipCue){
		PreswBean preswbean = new PreswBean(nomTipo, 0,0,0,0, rutUsuario);
		preswbean.setDigide(digide);
		preswbean.setNumdoc(numdoc);
		preswbean.setTipcue(tipCue);
	return preswbean.ingreso_presw19();
	}
	public boolean saveRecepciona(int numdoc, int rutUsuario, String digide, String tipo){
		PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
		preswbean.setDigide(digide);
		preswbean.setNumdoc(numdoc);
		
	return preswbean.ingreso_presw19();
	}
	public boolean saveRechaza(int numdoc, int rutUsuario, String digide, String tipo, String glosa, String tipCue){
		PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
		preswbean.setDigide(digide);
		preswbean.setNumdoc(numdoc);
		preswbean.ingreso_presw19();
		
		List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		Presw25DTO presw25DTO = new Presw25DTO();
		presw25DTO.setPres01(numdoc);
		presw25DTO.setComen1(glosa);
		presw25DTO.setRutide(rutUsuario);
		presw25DTO.setDigide(digide);
		presw25DTO.setIndexi(tipCue);
		lista.add(presw25DTO);
	return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	}

	public List<Cocow36DTO> getCreaCocow36(HttpServletRequest req, int rutide, String digide){
		List<Presw18DTO>  listaSolicitudesPago = (List<Presw18DTO>) req.getSession().getAttribute("listaSolicitudesPago");
		List<Presw18DTO>  listaSolicitudes = (List<Presw18DTO>) req.getSession().getAttribute("listaSolicitudes");
		String nomIdentificador = Util.validaParametro(req.getParameter("nomIdentificador"), "");
		long valorAPagar = Util.validaParametro(req.getParameter("valorAPagar"), 0);
		long rutnum = Util.validaParametro(req.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(req.getParameter("dvrut"),"");
		String identificadorInterno = Util.validaParametro(req.getParameter("identificadorInterno"),"");	
		String glosa1 = Util.validaParametro(req.getParameter("glosa1"),"");	
		String glosa2 = Util.validaParametro(req.getParameter("glosa2"),"");
		String glosa3 = Util.validaParametro(req.getParameter("glosa3"),"");
		String glosa4 = Util.validaParametro(req.getParameter("glosa4"),"");
		String glosa5 = Util.validaParametro(req.getParameter("glosa5"),"");
		String glosa6 = Util.validaParametro(req.getParameter("glosa6"),"");
		String tipoPago = Util.validaParametro(req.getParameter("tipoPago"),"");
		
		if (!tipoPago.trim().equals("")) {
			int indice = tipoPago.indexOf("@");
			if (indice > -1) tipoPago = tipoPago.substring(0,indice);
		}
		
	//	long sede = Util.validaParametro(req.getParameter("sede"), 0);
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		
	    String tiping = "VAP";
	    String nomcam = "";
	    long valnu1 = 0;
	    long valnu2 = 0;
	    String valalf = "";
	

    /* prepara archivo para grabar, llenar listCocow36DTO*/
    nomcam = "RUTIDE";
    valnu1 = rutnum;
    valnu2 = 0;
    valalf = "";
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    nomcam = "DIGIDE";
    valnu1 = 0;
    valnu2 = 0;
     valalf = (dvrut.trim().equals('k')?dvrut.trim().toUpperCase():dvrut.trim());
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    nomcam = "DESIDE";
    valnu1 = 0;
    valnu2 = 0;
    valalf = this.eliminaAcentosString(nomIdentificador.toUpperCase());
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    nomcam = "CODSUC";
    //valnu1 = sede;
    valnu2 = 0;
    valalf = "";
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    nomcam = "VALPAG";
    valnu1 = valorAPagar;
    valnu2 = 0;
    valalf = "";
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    nomcam = "IDMEMO";
    valnu1 = 0;
    valnu2 = 0;
    valalf = identificadorInterno;
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    nomcam = "GLOSA1";
    valnu1 = 0;
    valnu2 = 0;
    valalf = this.eliminaAcentosString(glosa1.toUpperCase());
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

    nomcam = "GLOSA2";
    valnu1 = 0;
    valnu2 = 0;
    valalf = this.eliminaAcentosString(glosa2.toUpperCase());
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

    nomcam = "GLOSA3";
    valnu1 = 0;
    valnu2 = 0;
    valalf = this.eliminaAcentosString(glosa3.toUpperCase());
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    nomcam = "GLOSA4";
    valnu1 = 0;
    valnu2 = 0;
    valalf = this.eliminaAcentosString(glosa4.toUpperCase());
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

    nomcam = "GLOSA5";
    valnu1 = 0;
    valnu2 = 0;
    valalf = this.eliminaAcentosString(glosa5.toUpperCase());
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

    nomcam = "GLOSA6";
    valnu1 = 0;
    valnu2 = 0;
    valalf = this.eliminaAcentosString(glosa6.toUpperCase());
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    nomcam = "TIPVAL";
    valnu1 = 0;
    valnu2 = 0;
    valalf = tipoPago;
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    /* 25/05/2012 agregar MD5*/
    nomcam = "CODMD5";
    MD5 md5 = new MD5();
    String texto =md5.getMD5(rutnum + dvrut.trim() + nomIdentificador.trim() + valorAPagar) ;
    valnu1 = 0;
    valnu2 = 0;
    valalf = this.eliminaAcentosString(texto.toUpperCase());;
    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    
    // MAA cambio en valalf
    for (Presw18DTO ss : listaSolicitudes){
    	nomcam = "CUENTA";
 	    valnu1 = ss.getCoduni();
 	    valnu2 = ss.getUsadom();
 	    //valalf = "";
 	    valalf = this.eliminaAcentosString(ss.getNompro().trim().toUpperCase());
 	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
    }
		
   if(listaSolicitudesPago != null && listaSolicitudesPago.size() > 0){
	   for (Presw18DTO ss : listaSolicitudesPago){
		    md5 = new MD5();
		    texto = md5.getMD5("0" + "" + ss.getDesuni().trim() + ss.getUsadom()) ;
		 
	    	nomcam = "NOMBEN";
	 	    valnu1 = ss.getUsadom();
	 	    valnu2 = ss.getRutide();
	 	    valalf = ss.getDesuni();
	 	    // agregado 10/05/2013 para indicar el rut del beneficiario, adem�s del nombre
	 		 Cocow36DTO cocow36DTO = new Cocow36DTO();
	 		 cocow36DTO.setTiping(tiping);
	 		 cocow36DTO.setNomcam(nomcam);
	 		 cocow36DTO.setValnu1(valnu1);
	 		 cocow36DTO.setValnu2(valnu2);
	 		 cocow36DTO.setValalf(valalf);
	 		 cocow36DTO.setAccion(ss.getIddigi());
	 		 cocow36DTO.setResval(texto);
	 		 listCocow36DTO.add(cocow36DTO);
	 	   // listCocow36DTO = getAgregaListaMD5(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf,texto );
	    }
	   
   }
	return listCocow36DTO;
	}
	
	synchronized  public int getRegistraVale(HttpServletRequest req, int rutide, String digide){
		int numVale = 0;
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		listCocow36DTO = getCreaCocow36(req, rutide, digide);
		Ingreso_Documento ingresoDocumento = new Ingreso_Documento("GV1",rutide,digide);
			
		numVale = ingresoDocumento.Ingresar_Documento(listCocow36DTO);
		
	
		return numVale;
	}
	// MAA usado por banner
	public boolean getActualizaVale(HttpServletRequest req, int rutide, String digide, int numDoc){
		boolean registra = false;
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		listCocow36DTO = getCreaCocow36(req, rutide, digide);
		Ingreso_Documento ingresoDocumento = new Ingreso_Documento("MV1",rutide,digide);
		ingresoDocumento.setNumdoc(numDoc);
			
		registra = ingresoDocumento.Actualizar_Documento(listCocow36DTO);
		
	
		return registra;
	}
	// MAA este proceso no se usa en SIIF los clientes ahora son ingresados por BANNER FINANZAS
/*	public boolean saveCliente(List<Presw25DTO> lista,int rutUsuario){
		PreswBean preswbean = new PreswBean("CLI", 0, 0, 0, 0, rutUsuario);
		
		return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
	} */
	public String formateoNumeroSinDecimales(Double numero){
		/*formatear n�mero */
	     Locale l = new Locale("es","CL");
	     DecimalFormat formatter1 = (DecimalFormat)DecimalFormat.getInstance(l);
	     formatter1.applyPattern("###,###,###");
	 
	   //  formatter1.format(rs.getDouble("RUTNUM"))   /*sin decimales*/
	 return formatter1.format(numero);
	}
	public String formateoNumeroConDecimales(Double numero){
		/*formatear n�mero */
	     Locale l = new Locale("es","CL");
	     DecimalFormat formatter2 = (DecimalFormat)DecimalFormat.getInstance(l);
	     formatter2.applyPattern("###,###.###");

	   //  formatter2.format(rs.getDouble("UTMSUS"))   /*con decimales*/
	     return formatter2.format(numero);
	}
	
	 public List<Presw18DTO> getConsultaAutoriza(int rutnum, String dv){
		  	PreswBean preswbean = null;
			Collection<Presw18DTO> lista = null;
			List<Presw18DTO> listaAutoriza =  new ArrayList<Presw18DTO>();
			String nombre = "";
			if(rutnum > 0){                // MAA este proceso sigue igual no cambio
				preswbean = new PreswBean("PAU",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
				lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
				   for (Presw18DTO ss : lista){
					   Presw18DTO presw18DTO = new Presw18DTO();
					   presw18DTO.setIndprc(ss.getIndprc());
					   listaAutoriza.add(presw18DTO);
				   }
						
			}
		
		 return listaAutoriza;
		}
	 public boolean saveDeleteIngresador( int rut, String dv, String codorg /*int coduni*/, String tipcue ){
		  	PreswBean preswbean = null;
			Collection<Presw18DTO> lista = null;
			long saldoCuenta = 0;
			preswbean = new PreswBean("AL1",0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
			preswbean.setRutide(rut);
			preswbean.setDigide(dv);
			// preswbean.setCoduni(coduni);
			preswbean.setCajero(codorg);
			preswbean.setTipcue(tipcue);
			
			return preswbean.ingreso_presw19();	
			
	 } 
	 public String eliminaAcentosString(String sTexto){
			/*elimina acentos del string*/
		   String linea = "";  
		    if(!sTexto.trim().equals("")){
		    for (int x=0; x < sTexto.length(); x++) {
		    	  if ((sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') || 
		    	      (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
		    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') || 
		    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
		    		  (sTexto.charAt(x) == '�') || (sTexto.charAt(x) == '�') ||
		    		  sTexto.charAt(x) == '\''){
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'a';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'A';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'e';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'E';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'i';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'I';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'o';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'O';
		    		   if(sTexto.charAt(x) == '�' )
		    			  linea += 'u';
		    		  if(sTexto.charAt(x) == '�' )
		    			  linea += 'U';
		    		  if(sTexto.charAt(x) == '\'')
		    			  linea += ' ';
		    	  }
		    	  else  linea += sTexto.charAt(x);
		    	}				
		    }
		
		 return linea;
		}
	 public List<Presw18DTO> getListaBanco( HttpServletRequest req) throws Exception {
			/*	se muestran todos los bancos */
				int rutUsuario = Integer.parseInt(Util.validaParametro(req.getSession().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(req.getSession().getAttribute("dv")+ "","");
			
				PreswBean preswbean = null;
				Collection<Presw18DTO> lista = null;
				List<Presw18DTO> list = new ArrayList<Presw18DTO>();
				String nombre = "";
				preswbean = new PreswBean("BAN",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
					lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
					   for (Presw18DTO ss : lista){
						   Presw18DTO presw18DTO = new Presw18DTO();
						   presw18DTO.setItedoc(ss.getItedoc());
						   presw18DTO.setDesuni(ss.getDesuni());
						   list.add(presw18DTO);
					   }	
				   
			return list;
			}
	/* public Collection<Presw25DTO> getVerificaRutCliente(int rutnum, String dv){
		  	PreswBean preswbean = null;
			Collection<Presw25DTO> lista = null;
			if(rutnum > 0){
				preswbean = new PreswBean("VCL",0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
				lista = (Collection<Presw25DTO>) preswbean.consulta_presw25();
					}
		
		 return lista;
		}*/
	 
	 public Collection<Presw25DTO> getVerificaRutCliente(String rutnumdv){
		    ConexionBanner con = new ConexionBanner();
		    PreparedStatement sent = null;
		    ResultSet res = null;
		    
		    String rut = rutnumdv.substring(0,rutnumdv.length()-1);
		    String dig = rutnumdv.substring(rutnumdv.length()-1,rutnumdv.length());
		    if (dig.equals("k")) rutnumdv = rut+"K";
		    
		    List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
		    try{
		      
		      sent = con.conexion().prepareStatement("SELECT spbpers_legal_name, spriden_pidm FROM spriden, spbpers, FTVVEND " +
		    		                                 " WHERE spriden_id = ? " +
		    		                                 " AND spriden_pidm = spbpers_pidm " +
		    		                                 " AND  spriden_PIDM = FTVVEND_PIDM" +
		    		                                 " AND spriden_change_ind is null");
		      sent.setString(1,rutnumdv);
		      res = sent.executeQuery();

		      while (res.next()){
		    	Presw25DTO ss = new Presw25DTO();
		        ss.setDesuni(res.getString(1)); // nombre + apellidos
		        
		        sent = con.conexion().prepareStatement("SELECT gxrdird_bank_rout_num, gxvdird_desc, gxrdird_bank_acct_num " +
		        		                               "FROM gxrdird, gxvdird " +
                                                       "WHERE gxrdird_pidm = " + res.getString(2) + 
                                                       "AND gxrdird_bank_rout_num = gxvdird_code_bank_rout_num " +
                                                       "AND gxrdird_priority = 1");	        
		        res = sent.executeQuery();

		        if (res.next()) {
		        	ss.setPres01(res.getInt(1)); // codigo del banco
		        	ss.setComen6(res.getString(2)); // Nombre del banco
		        	ss.setComen5(res.getString(3)); // cuenta de deposito
		        }
		        else {
		        	ss.setPres01(0); // codigo del banco 
		        	ss.setComen6(""); // Nombre del banco
		        	ss.setComen5(""); // cuenta de deposito
		        }
		        
		        lista.add(ss);
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloValePagoB.getVerificaRutCliente: " + e);
			}
		 return lista;
	 }
	 
	 public Collection<Presw18DTO> getVerificaRutIngresador(String rutnumdv){
		    ConexionBanner con = new ConexionBanner();
		    PreparedStatement sent = null;
		    ResultSet res = null;
		    
		    String rut = rutnumdv.substring(0,rutnumdv.length()-1);
		    String dig = rutnumdv.substring(rutnumdv.length()-1,rutnumdv.length());
		    if (dig.equals("k")) rutnumdv = rut+"K";
		    
		    List<Presw18DTO> lista = new ArrayList<Presw18DTO>();;
		    try{
		      
		      sent = con.conexion().prepareStatement("SELECT spbpers_legal_name, spriden_pidm FROM spriden, spbpers " +
		    		                                  "WHERE spriden_id = ? " +
		    		                                  "AND spriden_pidm = spbpers_pidm " +
		    		                                  "AND spriden_change_ind is null");
		      sent.setString(1,rutnumdv);
		      res = sent.executeQuery();

		      while (res.next()){
		    	Presw18DTO ss = new Presw18DTO();
		        ss.setDesuni(res.getString(1)); // nombre + apellidos
		        
	
		        sent = con.conexion().prepareStatement("SELECT pebempl_pidm FROM pebempl " +
                                                       "WHERE pebempl_pidm = " + res.getString(2));
		                		        
		        res = sent.executeQuery();
		        
		        if (res.next()) ss.setIndprc("F"); // Fde funcionario
                else  ss.setIndprc("");         
		        	
		        lista.add(ss);
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloValePagoB.getVerificaRutIngresador: " + e);
			}
		 return lista;
	 }
	 
	 public boolean getExisteGoremal(String rutnumdv){
		 boolean existe = false;
		 int pidm = this.getVerificaPidm(rutnumdv);
		 if (pidm > 0) {
		    ConexionBanner con = new ConexionBanner();
		    PreparedStatement sent = null;
		    ResultSet res = null;
		    
		    try {
		    	//String query = " SELECT goremal_email_address FROM GOREMAL WHERE goremal_pidm = " + pidm + " AND GOREMAL_EMAL_CODE = 'INS1'";
                
                //System.out.println("query " + query);
		    	
			    sent = con.conexion().prepareStatement(" SELECT goremal_email_address " +  
	                                                                     " FROM GOREMAL " + 
	                                                                     " WHERE goremal_pidm = " + pidm +
	                                                                     " AND GOREMAL_EMAL_CODE = 'INS1'");
			    res = sent.executeQuery();
			    if (res.next())          
			       existe = true; 	
			    
			    res.close();
			    sent.close();
			    con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloValePagoB.getExisteGoremal: " + e);
			}
	     }   
	   return existe;
	 }
	 
	 public String getMesActual(){
		 DateTool fechaActual = new DateTool();
		 String mesActual = "";
			
		 String mes = (fechaActual.getMonth()+1)+"";
		 if(mes.length()== 1)
					mes = "0" + mes;
		 mesActual = mes;
		 return mesActual;
	 }
	 public String getAnioActual(){
		 DateTool fechaActual = new DateTool();
		 String a�oActual = String.valueOf(fechaActual.getYear());
			
		 
		 return a�oActual.substring(2,4);
	 }
	 public String getBancoOperacion(String organizacion){
		 
		    ConexionAs400RecGasto con = new ConexionAs400RecGasto();
		    String codBan = "DC";
		    try{
		      
		      PreparedStatement sent = con.getConnection().prepareStatement(" SELECT CODBAN FROM USMMBP.COCBF07B " +
																			" WHERE CODORG LIKE ? "); 			    	
		    
		      sent.setString(1,organizacion.trim());
		      ResultSet res = sent.executeQuery();

		      while (res.next()){
		    	  codBan = res.getString(1); // CODBAN
		      }
		      res.close();
		      sent.close();
		     // con.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloHonorarioB.getBancoOperacion: " + e);
			}
		 return codBan;
		}

	public String getCodigoBanerVale() {
		return codigoBanerVale;
	}

	public void setCodigoBanerVale(String codigoBanerVale) {
		this.codigoBanerVale = codigoBanerVale;
	}
}
