package cl.utfsm.sir.mvc;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.utfsm.sir.modulo.ModuloRemuneracion;

import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.usm.siga.login.LoginLDAPBean;
import descad.presupuesto.Presw21DTO;


import sir.*;


public class RemuneracionInterceptor extends HandlerInterceptorAdapter {
	private ModuloRemuneracion moduloRemuneracion;

	
	static Logger log = Logger.getLogger(RemuneracionInterceptor.class);
	String funcionario = "";
	

	public ModuloRemuneracion getModuloRemuneracion() {
		return moduloRemuneracion;
	}

	public void setModuloRemuneracion(ModuloRemuneracion moduloRemuneracion) {
		this.moduloRemuneracion = moduloRemuneracion;

	}

	public void preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		if (request.getSession().getAttribute("rutUsuario") != null) {
				modelAndView.addObject("inicio", "S");
		

		}
	}

	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
		NumberTool numbertool = new NumberTool();
		MathTool mathtool = new MathTool();
		DateTool dateTool = new DateTool();
		modelAndView.addObject("numbertool", numbertool);
		modelAndView.addObject("mathtool", mathtool);
		modelAndView.addObject("datetool", dateTool);
		modelAndView.addObject("date", new DateTool());
		DateTool fechaActual = new DateTool();


		if (request.getSession().getAttribute("rutUsuario") != null) {
			int rutUsuario = Integer.parseInt(Util.validaParametro(request.getSession().getAttribute("rutUsuario")+ "","0"));
			funcionario = Util.validaParametro(request.getSession().getAttribute("funcionario") + "","");
			List<RemDTO> listaActual = (request.getSession().getAttribute("listaActual") != null)?(List<RemDTO>)request.getSession().getAttribute("listaActual"): new ArrayList<RemDTO>();
			List<RemDTO> listaHistorica = (request.getSession().getAttribute("listaHistorica") != null)?(List<RemDTO>)request.getSession().getAttribute("listaHistorica"): new ArrayList<RemDTO>();
			boolean sueldo = Util.validaParametro(request.getSession().getAttribute("sueldo")+"",false);
			boolean honorario = Util.validaParametro(request.getSession().getAttribute("honorario")+"",false);
			boolean antiguedad = Util.validaParametro(request.getSession().getAttribute("antiguedad")+"",false);
			boolean antiguedadRenta = Util.validaParametro(request.getSession().getAttribute("antiguedadRenta")+"",false);
			boolean antiguedadPeriodo = Util.validaParametro(request.getSession().getAttribute("antiguedadPeriodo")+"",false);
			boolean proyectoFondef = Util.validaParametro(request.getSession().getAttribute("proyectoFondef")+"",false);
			
			modelAndView.addObject("funcionario", funcionario);			
	
			modelAndView.addObject("listaActual", listaActual);
			modelAndView.addObject("listaHistorica", listaHistorica);
			modelAndView.addObject("sueldo", sueldo);
			modelAndView.addObject("honorario", honorario);
			modelAndView.addObject("antiguedad", antiguedad);
			modelAndView.addObject("antiguedadRenta", antiguedadRenta);
			modelAndView.addObject("antiguedadPeriodo", antiguedadPeriodo);
			modelAndView.addObject("proyectoFondef", proyectoFondef);
			
			
		} else
			modelAndView.addObject("inicio", "S");

	}

	public void cerrar(AccionWeb accionweb) throws Exception {
		if (accionweb.getSesion().getAttribute("rutUsuario") != null) {
			accionweb.getSesion().setAttribute("rutUsuario",null);
		}
	}
	
    public void cargarInicio(AccionWeb accionweb) throws Exception {
	 if (accionweb.getSesion().getAttribute("rutUsuario") != null) {
		
		 
		String rutUsuario = Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0");
		int rutOrigen  = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen") +"","0"));
		String Funcionario = Util.validaParametro(accionweb.getSesion().getAttribute("funcionario")+ "","0");
		accionweb.agregarObjeto("funcionario", Funcionario);
		if(rutOrigen > 0){
			accionweb.getSesion().setAttribute("rutUsuario", rutOrigen);
			accionweb.getSesion().setAttribute("nomSimulacion", "");
			rutUsuario = String.valueOf(rutOrigen);
			accionweb.getSesion().removeAttribute("rutOrigen");
			accionweb.getSesion().setAttribute("rutOrigen",null);
		}
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		//String Funcionario = "";
		/*descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
		funcionarioBean.buscar_funcionario(Integer.parseInt(rutUsuario));
		Funcionario = funcionarioBean.getNombre();		
		int resul = 1;*/
		
		Rem_Cert remCert = new Rem_Cert();
		List<RemDTO> listaActual = new ArrayList<RemDTO>();
		boolean sueldo = false;
		boolean honorario = false;
		boolean antiguedad = false;
		boolean antiguedadRenta = false;
		boolean antiguedadPeriodo = false;
		boolean proyectoFondef = false;
	
		remCert.setRut(rutUsuario);
		remCert.setDv(dv);
		listaActual = remCert.getRem();
/*		for (RemDTO ss : listaActual){
			System.out.println(ss.getAno()+ "  mes "+ ss.getMes());
			
		}*/
		 
		 List<RemDTO> listaHistorica = new ArrayList<RemDTO>();
		 remCert = new Rem_Cert();
		 remCert.setRut(rutUsuario);
		 remCert.setDv(dv);
		 listaHistorica = new ArrayList<RemDTO>(remCert.getRem_Hist());
		
		/*for (int i=0;i < listaHistorica.size(); i++ ){
			System.out.println("anoc hist: "+ listaHistorica.get(i).getAno()+"mes: "+ listaHistorica.get(i).getMes()+"   jjj "+ listaHistorica.get(i).getContrato());
		}*/
		sueldo = remCert.getCert_Sueldo();
		honorario = remCert.getCert_Honorarios();
		antiguedad = remCert.getCert_Antiguedad();
		antiguedadRenta = remCert.getCert_Antiguedad_Renta();
		antiguedadPeriodo = remCert.getCert_Antiguedad_Periodos();
		proyectoFondef = remCert.getCert_Proyecto_Fondef();
		moduloRemuneracion.agregaSesion(accionweb.getReq(), rutUsuario, dv, listaActual, listaHistorica, sueldo, honorario, antiguedad, antiguedadRenta, antiguedadPeriodo, proyectoFondef);
		accionweb.agregarObjeto("rutUsuario", rutUsuario);
		accionweb.agregarObjeto("dv", dv);
		accionweb.agregarObjeto("listaActual", listaActual);
		accionweb.agregarObjeto("listaHistorica", listaHistorica);
		accionweb.agregarObjeto("sueldo", sueldo);
		accionweb.agregarObjeto("honorario", honorario);
		accionweb.agregarObjeto("antiguedad", antiguedad);
		accionweb.agregarObjeto("antiguedadRenta", antiguedadRenta);
		accionweb.agregarObjeto("antiguedadPeriodo", antiguedadPeriodo);
		accionweb.agregarObjeto("proyectoFondef", proyectoFondef);
	    accionweb.agregarObjeto("esRemuneracion", 1); // 1 verdadero
	}
}
   
    public void cargarHome(AccionWeb accionweb) throws Exception {
    	int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));

    }

/*public void cargaLogin(AccionWeb accionweb) throws Exception {
		String login = Util.validaParametro(accionweb.getParameter("login"), "");
		String password = Util.validaParametro(accionweb.getParameter("passwd"), "");
		String server = Util.validaParametro(accionweb.getParameter("server"),"");
		
		String rutUsuario = "0";
		String dv = "0";
		
		boolean esLogin = false;
		int resul = 2;
	
		String Funcionario = "";
		if (accionweb.getSesion().getAttribute("rut") != null) {
			accionweb.getSesion().removeAttribute("rut");
			accionweb.getSesion().setAttribute("rut", null);
			accionweb.getSesion().removeAttribute("dv");
			accionweb.getSesion().setAttribute("dv", null);
		}
		if (login.trim().length() > 0 && password.trim().length() > 0) {
			
			LoginLDAPBean loginBeanId = new LoginLDAPBean();
			loginBeanId.setLogin(login);
			loginBeanId.setPasswd(password);
			loginBeanId.setServer(server);
			loginBeanId.setRemoteaddr(accionweb.getReq().getRemoteAddr());
			esLogin = loginBeanId.getUsuario();

			if (esLogin) {
				// Obtiene el RUT del usuario
				rutUsuario = loginBeanId.getRut();
				dv = loginBeanId.getDv();
				descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
				funcionarioBean.buscar_funcionario(Integer.parseInt(rutUsuario));
				Funcionario = funcionarioBean.getNombre();
				moduloRemuneracion.agregaFuncionario(accionweb.getReq(), Funcionario);
			}
			
			
			/* comentar todo esto para war
			
			rutUsuario = "12448517";
	     
			esLogin = true;
			descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
			funcionarioBean.buscar_funcionario(Integer.parseInt(rutUsuario));
			Funcionario = funcionarioBean.getNombre();
				
			moduloRemuneracion.agregaFuncionario(accionweb.getReq(), Funcionario);
		
		/*	comentar todo esto para war 

		} 
		if (esLogin) {
			resul = 1;
			
			Rem_Cert remCert = new Rem_Cert();
			List<RemDTO> listaActual = new ArrayList<RemDTO>();
			boolean sueldo = false;
			boolean honorario = false;
			boolean antiguedad = false;
			boolean antiguedadRenta = false;
			boolean antiguedadPeriodo = false;
			boolean proyectoFondef = false;
		
			remCert.setRut(rutUsuario);
			remCert.setDv(dv);
			listaActual = remCert.getRem();
			/*for (RemDTO ss : listaActual){
				System.out.println(ss.getAno()+ "  mes "+ ss.getMes());
				
			}*/
			 
	/*		 List<RemDTO> listaHistorica = new ArrayList<RemDTO>();
			 remCert = new Rem_Cert();
			 remCert.setRut(rutUsuario);
			 remCert.setDv(dv);
			 listaHistorica = new ArrayList<RemDTO>(remCert.getRem_Hist());
			
			/*for (int i=0;i < listaHistorica.size(); i++ ){
				System.out.println("anoc hist: "+ listaHistorica.get(i).getAno()+"mes: "+ listaHistorica.get(i).getMes()+"   jjj "+ listaHistorica.get(i).getContrato());
			}*/
	/*		sueldo = remCert.getCert_Sueldo();
			honorario = remCert.getCert_Honorarios();
			antiguedad = remCert.getCert_Antiguedad();
			antiguedadRenta = remCert.getCert_Antiguedad_Renta();
			antiguedadPeriodo = remCert.getCert_Antiguedad_Periodos();
			proyectoFondef = remCert.getCert_Proyecto_Fondef();
			moduloRemuneracion.agregaSesion(accionweb.getReq(), rutUsuario, dv, listaActual, listaHistorica, sueldo, honorario, antiguedad, antiguedadRenta, antiguedadPeriodo, proyectoFondef);
			accionweb.agregarObjeto("rutUsuario", rutUsuario);
			accionweb.agregarObjeto("dv", dv);
			accionweb.agregarObjeto("listaActual", listaActual);
			accionweb.agregarObjeto("listaHistorica", listaHistorica);
			accionweb.agregarObjeto("sueldo", sueldo);
			accionweb.agregarObjeto("honorario", honorario);
			accionweb.agregarObjeto("antiguedad", antiguedad);
			accionweb.agregarObjeto("antiguedadRenta", antiguedadRenta);
			accionweb.agregarObjeto("antiguedadPeriodo", antiguedadPeriodo);
			accionweb.agregarObjeto("proyectoFondef", proyectoFondef);
		}

		
		accionweb.agregarObjeto("rutUsuario", rutUsuario);
		accionweb.agregarObjeto("funcionario", Funcionario);
		accionweb.agregarObjeto("esLogin", resul); // 1 verdadero
	

	}
*/

	
    /*
	 * cargaIntranet:
	 * Valida la sesi�n de intranet y sube el rut del usuario
	 */
/*	public void cargaIntranet(AccionWeb accionweb) throws Exception {

		int rutUsuario = 0;
		boolean esLogin = false;
		int resul = 2;
		String Funcionario = "";
		String dv = "";
		
		
		String sid = Util.validaParametro(accionweb.getParameter("sid"), "");
		String ts = Util.validaParametro(accionweb.getParameter("ts"), "");
		accionweb.agregarObjeto("esLogin", "0");
		accionweb.agregarObjeto("esIntranet", "0"); 
		
		if (sid.trim().length() > 0 && ts.trim().length() > 0) {
			accionweb.agregarObjeto("esIntranet", "1"); 
			if (accionweb.getSesion().getAttribute("rutUsuario") != null) {
				accionweb.getSesion().removeAttribute("rutUsuario");
			}
			LoginLDAPBean loginBeanId = new LoginLDAPBean();
			loginBeanId.setRemoteaddr(accionweb.getReq().getRemoteAddr());
			rutUsuario = loginBeanId.getUsuario_Intranet(sid,ts, accionweb.getReq().getRemoteAddr());
			

			if (rutUsuario > 0) {
				accionweb.agregarObjeto("rut", rutUsuario);
				esLogin = true;
				resul = 1;

				descad.cliente.Funcionario funcionarioBean = new descad.cliente.Funcionario();
				funcionarioBean.buscar_funcionario(rutUsuario);
				Funcionario = funcionarioBean.getNombre();
				dv = funcionarioBean.getDv();
				moduloRemuneracion.agregaFuncionario(accionweb.getReq(), Funcionario);
				RemDTO rem = new  RemDTO();
				Rem_Cert remCert = new Rem_Cert();
				List<RemDTO> listaActual = new ArrayList<RemDTO>();
				List<RemDTO> listaHistorica = new ArrayList<RemDTO>();
				boolean sueldo = false;
				boolean honorario = false;
				boolean antiguedad = false;
				boolean antiguedadRenta = false;
				boolean antiguedadPeriodo = false;
				boolean proyectoFondef = false;
				
				remCert.setRut(String.valueOf(rutUsuario));
				remCert.setDv(dv);
				listaActual = remCert.getRem();
			/*	for (RemDTO mm : listaHistorica){
					System.out.println("anoc: "+ mm.getAno()+"mes: "+ mm.getMes());
				}*/
					
	/*			listaHistorica = remCert.getRem_Hist();
			/*	for (RemDTO ss : listaHistorica){
					System.out.println("anoc historica : "+ ss.getAno()+"mes: "+ ss.getMes());
						
				}*/
/*				sueldo = remCert.getCert_Sueldo();
				honorario = remCert.getCert_Honorarios();
				antiguedad = remCert.getCert_Antiguedad();
				antiguedadRenta = remCert.getCert_Antiguedad_Renta();
				antiguedadPeriodo = remCert.getCert_Antiguedad_Periodos();
				proyectoFondef = remCert.getCert_Proyecto_Fondef();
				moduloRemuneracion.agregaSesion(accionweb.getReq(), String.valueOf(rutUsuario), dv, listaActual, listaHistorica, sueldo, honorario, antiguedad, antiguedadRenta, antiguedadPeriodo, proyectoFondef);
				accionweb.agregarObjeto("rut", rutUsuario);
				accionweb.agregarObjeto("dv", dv);
				accionweb.agregarObjeto("listaActual", listaActual);
				accionweb.agregarObjeto("listaHistorica", listaHistorica);
				accionweb.agregarObjeto("sueldo", sueldo);
				accionweb.agregarObjeto("honorario", honorario);
				accionweb.agregarObjeto("antiguedad", antiguedad);
				accionweb.agregarObjeto("antiguedadRenta", antiguedadRenta);
				accionweb.agregarObjeto("antiguedadPeriodo", antiguedadPeriodo);
				accionweb.agregarObjeto("proyectoFondef", proyectoFondef);
				
			}
		}
		else {
			accionweb.agregarObjeto("esIntranet", "0"); 
		}
	
		accionweb.agregarObjeto("funcionario", Funcionario);
		accionweb.agregarObjeto("esLogin", resul); // 1 verdadero
		
	}	

	/*
	 * esIntranet:
	 * Si el sistema es llamado desde la intranet va a leer un request SID y de un TS
	 */
	/*public void esIntranet(AccionWeb accionweb) throws Exception {
		String sid = Util.validaParametro(accionweb.getParameter("sid"), "");
		String ts = Util.validaParametro(accionweb.getParameter("ts"), "");
		if (sid.trim().length() > 0 && ts.trim().length() > 0) {
			accionweb.agregarObjeto("esIntranet", "1"); // 1 verdadero
			cargaIntranet(accionweb);
		} 
		else 
			accionweb.agregarObjeto("esIntranet", "0"); 
	}	*/
	public void cargaRemuneracion(AccionWeb accionweb) throws Exception {
		String Funcionario = Util.validaParametro(accionweb.getSesion().getAttribute("funcionario")+ "","0");
		accionweb.agregarObjeto("funcionario", Funcionario);
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
        String  dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv") + "","");
		List<RemDTO> listaActual = (accionweb.getSesion().getAttribute("listaActual") != null)?(List<RemDTO>)accionweb.getSesion().getAttribute("listaActual"): new ArrayList<RemDTO>();
		List<RemDTO> listaHistorica = (accionweb.getSesion().getAttribute("listaHistorica") != null)?(List<RemDTO>)accionweb.getSesion().getAttribute("listaHistorica"): new ArrayList<RemDTO>();
		boolean sueldo = Util.validaParametro(accionweb.getSesion().getAttribute("sueldo")+"",false);
		boolean honorario = Util.validaParametro(accionweb.getSesion().getAttribute("honorario")+"",false);
		boolean antiguedad = Util.validaParametro(accionweb.getSesion().getAttribute("antiguedad")+"",false);
		boolean antiguedadRenta = Util.validaParametro(accionweb.getSesion().getAttribute("antiguedadRenta")+"",false);
		boolean antiguedadPeriodo = Util.validaParametro(accionweb.getSesion().getAttribute("antiguedadPeriodo")+"",false);
		boolean proyectoFondef = Util.validaParametro(accionweb.getSesion().getAttribute("proyectoFondef")+"",false);
		List<RemDTO> listaAnno = new ArrayList<RemDTO>();
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"),0);
		List<RemDTO> listaContrato = new ArrayList<RemDTO>();
		List<RemDTO> listaLiquidacion = new ArrayList<RemDTO>();
		List<RemDTO> listaMes = new ArrayList<RemDTO>();
		
		String mes = "0";
		String nomMes = "";
		accionweb.agregarObjeto("rutUsuario", rutUsuario);
		accionweb.agregarObjeto("dv", dv);
		accionweb.agregarObjeto("tipo", tipo);
		int i = 0;
		String numMes = "0";
		String numAno = "0";
		String contrato = "0";
		if(tipo == 1){
			for (RemDTO ss : listaActual){
				mes = ss.getMes();
				
				accionweb.agregarObjeto("anno", ss.getAno());
				accionweb.agregarObjeto("mes", mes);
				switch(Integer.parseInt(mes.trim())){
				case 1: nomMes = "Enero" ;
				break;
				case 2: nomMes = "Febrero" ;
				break;
				case 3: nomMes = "Marzo" ;
				break;
				case 4: nomMes = "Abril" ;
				break;
				case 5: nomMes = "Mayo" ;
				break;
				case 6: nomMes = "Junio" ;
				break;
				case 7: nomMes = "Julio" ;
				break;
				case 8: nomMes = "Agosto" ;
				break;
				case 9: nomMes = "Septiembre" ;
				break;
				case 10: nomMes = "Octubre" ;
				break;
				case 11: nomMes = "Noviembre" ;
				break;
				case 12: nomMes = "Diciembre" ;
				break;
				}
				if(i== 0){
					numMes = ss.getMes();
					numAno = ss.getAno();
					contrato = ss.getContrato();
					listaContrato = moduloRemuneracion.getlistaContrato(accionweb.getReq(), listaActual, Integer.parseInt(numAno), Integer.parseInt(numMes));
					listaLiquidacion = moduloRemuneracion.getlistaLiquidacion(accionweb.getReq(), listaActual, Integer.parseInt(numAno), Integer.parseInt(numMes), Integer.parseInt(contrato));
					
				}
				i++;
			}
			
			accionweb.agregarObjeto("nomMes", nomMes);
		}
		if(tipo== 2){
			
			int anno_ant = 0;
			i = 0;
			numMes = "0";
			numAno = "0";
			contrato = "0";
		
			for (RemDTO ss : listaHistorica){
				if(Integer.parseInt(ss.getAno()) != anno_ant){
				RemDTO rem = ss;
				listaAnno.add(ss);
				if(i== 0){
					numMes = ss.getMes();
					numAno = ss.getAno();
					contrato = ss.getContrato();
					listaContrato = moduloRemuneracion.getlistaContrato(accionweb.getReq(), listaHistorica, Integer.parseInt(numAno), Integer.parseInt(numMes));
					listaLiquidacion = moduloRemuneracion.getlistaLiquidacion(accionweb.getReq(), listaHistorica, Integer.parseInt(numAno), Integer.parseInt(numMes), Integer.parseInt(contrato));
					listaMes = moduloRemuneracion.getlistaMes(accionweb.getReq(), listaHistorica, Integer.parseInt(numAno));					
				}
				} 
				anno_ant = Integer.parseInt(ss.getAno());
				i++;
			}
			accionweb.agregarObjeto("listaAnno", listaAnno);
			accionweb.agregarObjeto("listaMes", listaMes);

		} 
	
		accionweb.agregarObjeto("listaContrato", listaContrato);
		accionweb.agregarObjeto("listaLiquidacion", listaLiquidacion);
		accionweb.agregarObjeto("opcionMenu",tipo);
		if(sueldo == true)
			accionweb.agregarObjeto("sueldo", sueldo);
		if(honorario == true)
			accionweb.agregarObjeto("honorario", honorario);
		if(antiguedad == true)
			accionweb.agregarObjeto("antiguedad", antiguedad);
		if(antiguedadRenta == true)
			accionweb.agregarObjeto("antiguedadRenta", antiguedadRenta);
		if(antiguedadPeriodo == true)
			accionweb.agregarObjeto("antiguedadPeriodo", antiguedadPeriodo);
		if(proyectoFondef == true)
			accionweb.agregarObjeto("proyectoFondef", proyectoFondef);
		 accionweb.agregarObjeto("esRemuneracion", 1); // 1 verdadero
	}
	public void cargaContrato(AccionWeb accionweb) throws Exception {
		String Funcionario = Util.validaParametro(accionweb.getSesion().getAttribute("funcionario")+ "","0");
		accionweb.agregarObjeto("funcionario", Funcionario);
		List<RemDTO> listaHistorica = (accionweb.getSesion().getAttribute("listaHistorica") != null)?(List<RemDTO>)accionweb.getSesion().getAttribute("listaHistorica"): new ArrayList<RemDTO>();
		int anno = Util.validaParametro(accionweb.getParameter("anno"),0);
		int mes = Util.validaParametro(accionweb.getParameter("mes"),0);
			
		List<RemDTO> listaContrato = new ArrayList<RemDTO>();
		listaContrato = moduloRemuneracion.getlistaContrato(accionweb.getReq(), listaHistorica, anno, mes)	;

		
		 accionweb.agregarObjeto("listaContrato", listaContrato);
		 accionweb.agregarObjeto("esRemuneracion", 1); // 1 verdadero
	}
	public void cargaLiquidacion(AccionWeb accionweb) throws Exception {
		String Funcionario = Util.validaParametro(accionweb.getSesion().getAttribute("funcionario")+ "","0");
		accionweb.agregarObjeto("funcionario", Funcionario);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"),0);
		List<RemDTO> listaHistorica = new ArrayList<RemDTO>();
		if(tipo == 1)
			listaHistorica = (accionweb.getSesion().getAttribute("listaActual") != null)?(List<RemDTO>)accionweb.getSesion().getAttribute("listaActual"): new ArrayList<RemDTO>();
			else
				listaHistorica = (accionweb.getSesion().getAttribute("listaHistorica") != null)?(List<RemDTO>)accionweb.getSesion().getAttribute("listaHistorica"): new ArrayList<RemDTO>();
					
		int anno = Util.validaParametro(accionweb.getParameter("anno"),0);
		int mes = Util.validaParametro(accionweb.getParameter("mes"),0);
		int contrato = Util.validaParametro(accionweb.getParameter("contrato"),0);
		
		List<RemDTO> listaLiquidacion = new ArrayList<RemDTO>();
		listaLiquidacion = moduloRemuneracion.getlistaLiquidacion(accionweb.getReq(), listaHistorica, anno, mes, contrato);
	
		if(listaLiquidacion.size()>0)
			accionweb.agregarObjeto("listaLiquidacion", listaLiquidacion);
		 accionweb.agregarObjeto("esRemuneracion", 1); // 1 verdadero
	}
	
	public void cargaMes(AccionWeb accionweb) throws Exception {
		String Funcionario = Util.validaParametro(accionweb.getSesion().getAttribute("funcionario")+ "","0");
		accionweb.agregarObjeto("funcionario", Funcionario);
		List<RemDTO> listaHistorica = (accionweb.getSesion().getAttribute("listaHistorica") != null)?(List<RemDTO>)accionweb.getSesion().getAttribute("listaHistorica"): new ArrayList<RemDTO>();
		int anno = Util.validaParametro(accionweb.getParameter("anno"),0);
		int mes_ant = 0;
		List<RemDTO> listaMes = new ArrayList<RemDTO>();
		listaMes = moduloRemuneracion.getlistaMes(accionweb.getReq(), listaHistorica, anno);
	    accionweb.agregarObjeto("listaMes", listaMes);
	    accionweb.agregarObjeto("esRemuneracion", 1); // 1 verdadero
	}
	public void generar(AccionWeb accionweb) throws Exception {
		String Funcionario = Util.validaParametro(accionweb.getSesion().getAttribute("funcionario")+ "","0");
		accionweb.agregarObjeto("funcionario", Funcionario);
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"),0);
		accionweb.agregarObjeto("opcionMenu",tipo);
		 accionweb.agregarObjeto("esRemuneracion", 1); // 1 verdadero	
	}

	public String getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(String funcionario) {
		this.funcionario = funcionario;
	}
	
}
