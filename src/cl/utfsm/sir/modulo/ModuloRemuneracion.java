package cl.utfsm.sir.modulo;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import sir.RemDTO;

import cl.utfsm.base.util.Util;
import cl.utfsm.sir.datos.RemuneracionDao;

public class ModuloRemuneracion {
	RemuneracionDao remuneracionDao;
	
	public void agregaFuncionario(HttpServletRequest req, String funcionario){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("funcionario",funcionario);
		}
	public void agregaSesion(HttpServletRequest req, String rut, 
													 String dv,	
													 List<RemDTO> listaActual, 
													 List<RemDTO> listaHistorica,
													 boolean sueldo,
													 boolean honorario,
													 boolean antiguedad,
													 boolean antiguedadRenta,
													 boolean antiguedadPeriodo,
													 boolean proyectoFondef){
		HttpSession sesion = req.getSession();
		sesion.setAttribute("rut",rut);
		sesion.setAttribute("dv",dv);
		sesion.setAttribute("listaActual", listaActual);
		sesion.setAttribute("listaHistorica", listaHistorica);
		sesion.setAttribute("sueldo", sueldo);
		sesion.setAttribute("honorario", honorario);
		sesion.setAttribute("antiguedad", antiguedad);
		sesion.setAttribute("antiguedadRenta", antiguedadRenta);
		sesion.setAttribute("antiguedadPeriodo", antiguedadPeriodo);
		sesion.setAttribute("proyectoFondef", proyectoFondef);
	}
	public List<RemDTO> getlistaMes(HttpServletRequest req, List<RemDTO> lista, int anno){
		List<RemDTO> listaMes = new ArrayList<RemDTO>();
		Vector vecMes = new Vector();
		 for (RemDTO ss : lista){
			 if( Integer.parseInt(ss.getAno().trim()) == anno){
				 if(vecMes.indexOf(ss.getMes()) < 0){
				 RemDTO rem = new RemDTO();
				// rem = ss;
			
				 rem.setMes(ss.getMes());		
				 
				 switch(Integer.parseInt(ss.getMes())){
				 	case 1: rem.setNommes("Enero") ;
				 			break;
				 	case 2: rem.setNommes("Febrero") ;
				 			break;
				 	case 3: rem.setNommes("Marzo") ;
				 			break;
				 	case 4: rem.setNommes("Abril") ;
				 			break;
				 	case 5: rem.setNommes("Mayo") ;
				 			break;
				 	case 6: rem.setNommes("Junio") ;
				 			break;
				 	case 7: rem.setNommes("Julio") ;
				 			break;
				 	case 8: rem.setNommes("Agosto") ;
				 			break;
				 	case 9: rem.setNommes("Septiembre") ;
				 			break;
				 	case 10: rem.setNommes("Octubre") ;
				 			break;
				 	case 11: rem.setNommes("Noviembre") ;
				 			break;
				 	case 12: rem.setNommes("Diciembre") ;
				 			break;
				 			
				 }
				 listaMes.add(rem);
				 }
				 vecMes.add(ss.getMes());
			 }
		 }
		 return listaMes;
	}
	
	public List<RemDTO> getlistaLiquidacion(HttpServletRequest req, List<RemDTO> lista, int anno, int mes, int contrato){

	List<RemDTO> listaLiquidacion = new ArrayList<RemDTO>();
    if(anno > 0 && mes > 0 && contrato > 0 ){
	 for (RemDTO ss : lista){
		 if( Integer.parseInt(ss.getAno().trim()) == anno && Integer.parseInt(ss.getMes()) == mes && Integer.parseInt(ss.getContrato()) == contrato){
			 RemDTO rem = new RemDTO();	 
			 rem = ss;
			 if(ss.getLiquidacion() != null && ss.getLiquidacion().trim().equals("L")){
				 rem.setLiquidacion(ss.getLiquidacion());
				 rem.setNomLiquidacion("Liquidación");
			 }
			 if(ss.getReliquidacion() != null && ss.getReliquidacion().trim().equals("R")){
				 rem.setReliquidacion(ss.getReliquidacion());
				 rem.setNomReliquidacion("Reliquidación");
			 } 
			 if(ss.getBonificacion() != null && ss.getBonificacion().trim().equals("B")){
				 rem.setBonificacion(ss.getBonificacion());
				 rem.setNomBonificacion("Bonificación");
			 } 
		     listaLiquidacion.add(rem);
	
				
		 }
			
			 }
		
    }
    return listaLiquidacion;
	}
	
	public List<RemDTO> getlistaContrato(HttpServletRequest req, List<RemDTO> lista, int anno, int mes){
		List<RemDTO> listaContrato = new ArrayList<RemDTO>();
		
		 for (RemDTO ss : lista){
			 if( Integer.parseInt(ss.getAno().trim()) == anno && Integer.parseInt(ss.getMes()) == mes){
				 RemDTO rem = new RemDTO();	 
				 rem.setContrato(ss.getContrato());
				 switch(Integer.parseInt(ss.getContrato())){
				     case 1:rem.setNomContrato("Empleado");
				     break;
				     case 2: rem.setNomContrato("Operario");
				     break;
				     case 5:rem.setNomContrato("Honorario");
				     break;
				     case 7: rem.setNomContrato("Ayudante");
				     break;
				     }
				 if(listaContrato.indexOf(rem) < 0)
				     listaContrato.add(rem);
				 }
			
			 }
		 return listaContrato;
	}

	public RemuneracionDao getRemuneracionDao() {
		return remuneracionDao;
	}
	public void setRemuneracionDao(RemuneracionDao remuneracionDao) {
		this.remuneracionDao = remuneracionDao;
	}
}
