package cl.utfsm.rrhhI.datos;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import oracle.jdbc.OracleTypes;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import cl.utfsm.base.util.Util;
import cl.utfsm.conexion.ConexionAs400RRHHI;
import cl.utfsm.conexion.ConexionBannerRRHH;
import cl.utfsm.rrhhI.modulo.Contrato;
import cl.utfsm.rrhhI.modulo.HoraClases;
import cl.utfsm.rrhhI.modulo.Persona;
import cl.utfsm.rrhhI1.datos.ProcesosDAO;
import cl.utfsm.rrhhI1.modulo.EquivalenciaNivel;
import cl.utfsm.rrhhI1.modulo.EquivalenciaPlanta;

public class HibernateIntegracionDao extends HibernateDaoSupport implements IntegracionDao{
	
	public static HashMap<String, String> estados(){
		
			String sql = "select * from TRANSFERbt.appef83";
			ConexionAs400RRHHI con = new ConexionAs400RRHHI();
			HashMap<String, String> list = new HashMap<String, String>();
			try {
				PreparedStatement sent = con.getConnection().prepareStatement(sql);
				ResultSet rs = sent.executeQuery();
				while (rs.next()) {
					list.put(rs.getString("ESTTRA"),rs.getString("DESEST"));					
				}
				con.getConnection().close();
			}catch (SQLException e) {
				System.out.print("SQL error HibernateIntegracionDao.estados: " + e.getMessage());
			} catch (Exception e) {
				System.out.print("Error HibernateIntegracionDao.estados: " + e.getMessage());
			}
		
		return list;
	}
	public  static void asignarDatosPersona(Contrato contr,ResultSet rs)throws SQLException {
		Persona pers = new Persona();
		pers.setRut(rs.getInt("rutide"));
		pers.setDigide(rs.getString("digide"));
		pers.setApepat(rs.getString("apepat_persona"));
		pers.setApemat(rs.getString("apemat_persona"));
		pers.setNombre(rs.getString("nombre_persona"));
		pers.setEstciv(rs.getInt("estciv_persona"));
		pers.setSexo(rs.getInt("sexo_persona"));
		pers.setFecnac(rs.getInt("fecnac_persona"));
		pers.setLugnac(rs.getString("lugnac_persona"));
		pers.setPainac(rs.getString("painac_persona"));
		pers.setNacion(rs.getInt("nacion_persona"));
		pers.setDetnac(rs.getString("detnac_persona"));
		pers.setDomici(rs.getString("domici_persona"));
		pers.setComuna(rs.getInt("comuna_persona"));
		pers.setCiudad(rs.getInt("ciudad_persona"));
		pers.setRegion(rs.getInt("region_persona"));
		pers.setCodgra(rs.getInt("codgra_persona"));
		pers.setGratit(rs.getInt("codtit_persona"));
		pers.setCodtit(rs.getInt("codtit_persona"));
		pers.setTelefo(rs.getString("telefo_persona"));
		pers.setCelula(rs.getString("celula_persona"));
		pers.setCorele(rs.getString("corele_persona"));
		pers.setDomele(rs.getString("domele_persona"));
		contr.setFeccam(rs.getInt("feccam_persona"));
		contr.setPersona(pers);
	}
	public  static void asignarHoras(Contrato contr , ResultSet rs ,boolean ayudante) throws SQLException{
		
		HoraClases horClaPrac = new HoraClases();
		HoraClases horClaTeo = new HoraClases();
		
		horClaPrac.setAsiafl(rs.getString("asialf_horas"));
		horClaPrac.setAsinum(rs.getInt("asibun_horas"));
		horClaPrac.setParale(rs.getString("parale_horas"));	
		horClaTeo.setAsiafl(rs.getString("asialf_horas"));
		horClaTeo.setAsinum(rs.getInt("asibun_horas"));
		horClaTeo.setParale(rs.getString("parale_horas"));
		
		if(horClaPrac.getAsiafl() != null && !horClaPrac.getAsiafl().trim().equals("") && ayudante  ){
			horClaPrac.setCanhor(rs.getInt("CANHOR_horas"));
			contr.getHorasPracticas().add(horClaPrac);
		}else if(horClaPrac.getAsiafl() != null && !horClaPrac.getAsiafl().trim().equals("") && !ayudante ){
			horClaPrac.setCanhor(rs.getInt("canpra_horas"));
			horClaTeo.setCanhor(rs.getInt("canteo_horas"));
			
			if(horClaPrac.getCanhor() > 0){
				contr.getHorasPracticas().add(horClaPrac);
			}
			if(horClaTeo.getCanhor() > 0){
				contr.getHorasTeoricas().add(horClaTeo);
			}			
		}	
	}
    public  static void asingnarDatosContratoNo(HashMap<String, Contrato> listMapAyudantes , ResultSet rs,boolean ayudante,HashMap<String, Contrato> rechasados,HashMap<String, String> listRutDupli)throws SQLException{
		
		Contrato contr = listMapAyudantes.get("N"+rs.getInt("NUMINT"));
		
		if(contr == null){
		    contr = new Contrato();
			//data general
			contr.setTipo(rs.getString("CONANX"));
			contr.setNumeroAnexo("N"+rs.getString("NUMINT"));
			contr.setJornada(rs.getInt("jornad_contrato"));
			contr.setFecant(rs.getInt("fecant_contrato"));
			contr.setFecter(rs.getInt("fecter_contrato"));
			contr.setIndcol(rs.getInt("indcol_contrato"));
			contr.setIndmov(rs.getInt("indmov_contrato"));
			contr.setCodafp(rs.getInt("codafp_contrato"));
			contr.setIndjub(rs.getInt("indjub_contrato"));
			contr.setIndsal(rs.getInt("indsal_contrato"));			
			contr.setValaho(rs.getInt("valaho_contrato"));
			contr.setCodisa(Util.validaParametro(rs.getString("codisa_contrato"), 0));
			contr.setUniisa(rs.getString("uniisa_contrato"));
			contr.setValisa(rs.getInt("valisa_contrato"));
			contr.setAntqui(rs.getInt("antqui_contrato"));
			contr.setInddep(rs.getInt("inddep_contrato"));
			contr.setSermed(rs.getInt("sermed_contrato"));
			contr.setCuomor(rs.getInt("cuomor_contrato"));
			contr.setSindi1(rs.getInt("sindi1_contrato"));
			contr.setTipjer(rs.getString("tipjer_contrato"));
			contr.setHorini(rs.getInt("horini_contrato"));
			contr.setHorfin(rs.getInt("horfin_contrato"));
			contr.setResolu(rs.getInt("resolu_contrato"));
			contr.setFecres(rs.getInt("fecres_contrato"));
			contr.setImpcon(rs.getString("impcon_contrato"));
			contr.setFecing(rs.getInt("fecing_contrato"));
			contr.setSegces(rs.getInt("segces_contrato"));
			contr.setIndbon(rs.getInt("indbon_contrato"));
			contr.setCodban(rs.getInt("codban_contrato"));
			contr.setCueban(rs.getString("cueban_contrato"));
			contr.setFeccon(rs.getInt("feccon_contrato"));
			contr.setFecpas(rs.getInt("fecpas_contrato"));
			contr.setUniaug(rs.getString("uniaug_contrato"));
			contr.setValaug(rs.getInt("valaug_contrato"));
			contr.setFecint(rs.getInt("fecint_contrato"));
			contr.setCodcar(rs.getInt("codcar_contrato"));
			contr.setTipins(rs.getString("TIPINS_AFP"));
			//dotos que devieran tener solicitud
			
			// datos eclusivos de anexo 
			if(contr.getTipo().equals("A")){
				contr.setPaso(0);
				contr.setTipcon(rs.getInt("tipcon_solicitud"));
				contr.setSumfij(rs.getInt("valmes_solicitud"));
				contr.setFecini(rs.getInt("fecini_solicitud"));
				contr.setFecfin(rs.getInt("fecfin_solicitud"));	
				contr.setCodorg(rs.getString("codorg_solicitud"));
				contr.setCodpla(rs.getString("codpla_solicitud"));
				contr.setCanhor(rs.getInt("canhor_solicitud"));
				contr.setCodsuc(rs.getInt("codsuc_solicitud"));
				contr.setServicio(rs.getString("SERPR1_solicitud"));
				contr.setEscfon(0);
				contr.setCodpue(rs.getString("codpue_estsol"));
				contr.setTippue(rs.getString("tippue_estsol"));
			}else if(contr.getTipo().equals("C")){
				contr.setPaso(rs.getInt("numniv_contrato"));
				contr.setTipcon(rs.getInt("tipcon_contrato"));
				contr.setSumfij(rs.getInt("sumfij_contrato"));
				contr.setFecini(rs.getInt("fecini_contrato"));
				contr.setFecfin(rs.getInt("fecfin_contrato"));	
				contr.setCodorg(rs.getString("codorg_contrato"));
				contr.setCodpla(rs.getString("codpla_contrato"));
				contr.setCanhor(rs.getInt("canhor_contrato"));
				contr.setCodsuc(rs.getInt("codsuc_contrato"));
				contr.setEscfon(rs.getInt("escfon_contrato"));
				contr.setCodpue(rs.getString("codpue_contrato"));
				contr.setTippue(rs.getString("tippue_contrato"));
			}
			asignarDatosPersona(contr,rs);			
		}		
		asignarHoras( contr , rs ,false);
		String rut = null;
		rut = listRutDupli.get(rs.getString("rutide"));
		
		if(rut != null){			
			contr.setEstado("I");			
			rechasados.put( contr.getNumeroAnexo(), contr);
		}else if(rs.getInt("fecini_contrato") == 0 && contr.getTipo().equals("C")){
			contr.setEstado("T");			
			rechasados.put( contr.getNumeroAnexo(), contr);
		}else if(contr.getCodpue().trim().equals("")){
			contr.setEstado("G");			
			rechasados.put( contr.getNumeroAnexo(), contr);
		}else if(contr.getCodpla() == null ||contr.getCodpla().trim().equals("") || contr.getCodpla().equals("0")){
			contr.setEstado("N");
			rechasados.put( contr.getNumeroAnexo(), contr);
		}else{
			listMapAyudantes.put(contr.getNumeroAnexo(), contr);
		}
		
	}
	
	public  static void optenerNormales(HashMap<String, Contrato> listMapContratos,HashMap<String, Contrato> rechasado ,HashMap<String,String> listRutDupl){
		
		String sql = " select estsol.numint numint,CONANX CONANX,fecrem,sol.rutide,sol.digide,sucur,sol.fecsol fecsol_solicitud,sol.fecini fecini_solicitud,sol.fecfin fecfin_solicitud "
					+"      ,sol.tipcon tipcon_solicitud,sol.sumfij sumfij_solicitud,sol.SERPR1 SERPR1_solicitud,sol.SERPR2 SERPR2_solicitud,sol.codpla codpla_solicitud "
					+"      ,sol.codcar codcar_solicitud,sol.valmes valmes_solicitud,sol.estsol estsol_solicitud,sol.canhor canhor_solicitud,sol.codorg codorg_solicitud " 
					+"      ,hor.asialf asialf_horas,hor.asinum asibun_horas,hor.parale parale_horas,hor.canteo canteo_horas,hor.valteo valteo_horas,hor.canpra canpra_horas "
					+"      ,hor.valpra valpra_horas,con.numniv numniv_contrato,con.sumfij sumfij_contrato,con.jornad jornad_contrato,con.fecini fecini_contrato "
					+"      ,con.fecfin fecfin_contrato,con.fecant fecant_contrato,con.fecter fecter_contrato,con.indcol indcol_contrato,con.indmov indmov_contrato "
					+"      ,con.codafp codafp_contrato,con.indjub indjub_contrato,con.indsal indsal_contrato,con.valaho valaho_contrato,con.uniaho uniaho_contrato "
					+"      ,con.codisa codisa_contrato,con.uniisa uniisa_contrato,con.valisa valisa_contrato,con.antqui antqui_contrato,con.inddep inddep_contrato "
					+"      ,con.sermed sermed_contrato,con.cuomor cuomor_contrato,con.sindi1 sindi1_contrato,con.tipjer tipjer_contrato,con.horini horini_contrato "
					+"      ,con.horfin horfin_contrato,con.resolu resolu_contrato,con.fecres fecres_contrato,con.impcon impcon_contrato,con.fecing fecing_contrato "
					+"      ,con.segces segces_contrato,con.indbon indbon_contrato,con.codban codban_contrato,con.cueban cueban_contrato,con.feccon feccon_contrato "
					+"      ,con.fecpas fecpas_contrato,con.uniaug uniaug_contrato,con.valaug valaug_contrato,con.fecint fecint_contrato,con.codorg codorg_contrato "
					+"      ,con.tippue tippue_contrato,con.codpue codpue_contrato,pers.apepat apepat_persona,pers.apemat apemat_persona,pers.nombre nombre_persona "
					+"      ,pers.estciv estciv_persona,pers.sexo sexo_persona,pers.fecnac fecnac_persona,pers.lugnac lugnac_persona,pers.painac painac_persona "
					+"      ,pers.nacion nacion_persona,pers.detnac detnac_persona,pers.domici domici_persona,pers.comuna comuna_persona,pers.ciudad ciudad_persona "
					+"      ,pers.region region_persona,pers.codgra codgra_persona,pers.gratit gratit_persona,pers.codtit codtit_persona,pers.telefo telefo_persona "
					+"      ,pers.celula celula_persona,pers.corele corele_persona,pers.domele domele_persona ,con.tipcon tipcon_contrato ,sol.tipcon tipcon_solicitud ,sol.codpla codpla_solicitud, con.codpla codpla_contrato " 
					+"      ,sol.canhor canhor_solicitud ,con.canhor canhor_contrato ,sol.sucur codsuc_solicitud, con.codsuc codsuc_contrato ,con.escfon escfon_contrato" 
					+"      ,pers.feccam feccam_persona , con.codcar codcar_contrato ,estsol.codpue codpue_estsol ,estsol.tippue tippue_estsol , afp.TIPINS TIPINS_AFP "
					+"      ,sol.SERPR1 SERPR1_solicitud "      
					+" from " 
					+" transferbt.APPEF82 estsol "
					+" LEFT JOIN remunerabt.appef09 sol on estsol.numint = sol.numint " 
					+" LEFT JOIN remunerabt.appef11 hor on hor.numint = sol.numint "
					+" LEFT JOIN remunerabt.appef05 con on con.rutide = sol.rutide and con.tipcon = sol.tipcon and sol.FECINI = con.FECINI " 
					+" LEFT JOIN remunerabt.appef01 pers on sol.rutide = pers.rutide "
					+" LEFT JOIN REMUNERABT.APREF06 afp on afp.INSPRE = con.CODAFP "
					+" WHERE ESTTRA = ' ' ";
		ConexionAs400RRHHI con = new ConexionAs400RRHHI();
		try {
			PreparedStatement sent = con.getConnection().prepareStatement(sql);
			try {
				ResultSet rs = sent.executeQuery();
				
				while(rs.next()){
					
						asingnarDatosContratoNo(listMapContratos,rs,false,rechasado,listRutDupl);
					
					
				}
			} catch (SQLException e) {
				System.out.print("SQL error HibernateIntegracionDao.optenerNormales " + e.getMessage());
			}
			con.getConnection().close();
		} catch (SQLException e) {
			System.out.print("Error HibernateIntegracionDao.optenerNormales " + e.getMessage());
		}
		
	}
	public  static void asingnarDatosContratoAy(HashMap<String, Contrato> listMapAyudantes , ResultSet rs,boolean ayudante , HashMap<String, Contrato> rechazados ,HashMap<String, String> listRutDupli)throws SQLException{
		
		Contrato contr = listMapAyudantes.get("A"+rs.getInt("NUMINT"));
		
		if(contr == null){
		    contr = new Contrato();
			//data general
			contr.setTipo(rs.getString("CONANX"));
			contr.setNumeroAnexo("A"+rs.getString("NUMINT"));			
			contr.setJornada(rs.getInt("jornad_contrato"));
			contr.setFecant(rs.getInt("fecant_contrato"));
			contr.setFecter(rs.getInt("fecter_contrato"));
			contr.setIndcol(rs.getInt("indcol_contrato"));
			contr.setIndmov(rs.getInt("indmov_contrato"));
			contr.setCodafp(rs.getInt("codafp_contrato"));
			contr.setTipins(rs.getString("TIPINS_AFP"));
			contr.setIndjub(rs.getInt("indjub_contrato"));
			contr.setIndsal(rs.getInt("indsal_contrato"));			
			contr.setValaho(rs.getInt("valaho_contrato"));
			contr.setCodisa(Util.validaParametro(rs.getString("codisa_contrato"), 0));
			contr.setUniisa(rs.getString("uniisa_contrato"));
			contr.setValisa(rs.getInt("valisa_contrato"));
			contr.setAntqui(rs.getInt("antqui_contrato"));
			contr.setInddep(rs.getInt("inddep_contrato"));
			contr.setSermed(rs.getInt("sermed_contrato"));
			contr.setCuomor(rs.getInt("cuomor_contrato"));
			contr.setSindi1(rs.getInt("sindi1_contrato"));
			contr.setTipjer(rs.getString("tipjer_contrato"));
			contr.setHorini(rs.getInt("horini_contrato"));
			contr.setHorfin(rs.getInt("horfin_contrato"));
			contr.setResolu(rs.getInt("resolu_contrato"));
			contr.setFecres(rs.getInt("fecres_contrato"));
			contr.setImpcon(rs.getString("impcon_contrato"));
			contr.setFecing(rs.getInt("fecing_contrato"));
			contr.setSegces(rs.getInt("segces_contrato"));
			contr.setIndbon(rs.getInt("indbon_contrato"));
			contr.setCodban(rs.getInt("codban_contrato"));
			contr.setCueban(rs.getString("cueban_contrato"));
			contr.setFeccon(rs.getInt("feccon_contrato"));
			contr.setFecpas(rs.getInt("fecpas_contrato"));
			contr.setUniaug(rs.getString("uniaug_contrato"));
			contr.setValaug(rs.getInt("valaug_contrato"));
			contr.setFecint(rs.getInt("fecint_contrato"));
			contr.setCodcar(rs.getInt("codcar_contrato"));
			
			
			//dotos que devieran tener solicitud
			
			// datos eclusivos de anexo 
			if(contr.getTipo().equals("A")){
				contr.setPaso(0);
				contr.setTipcon(rs.getInt("tipcon_solicitud"));
				contr.setSumfij(rs.getInt("valmes_solicitud"));
				contr.setFecini(rs.getInt("fecini_solicitud"));
				contr.setFecfin(rs.getInt("fecfin_solicitud"));	
				contr.setCodorg(rs.getString("codorg_solicitud"));
				contr.setCodpla("3");
				contr.setCanhor(rs.getInt("canhor_solicitud"));
				contr.setCodsuc(rs.getInt("codsuc_solicitud"));
				contr.setServicio(rs.getString("serpr_solicitud"));
				contr.setEscfon(0);
				contr.setCodpue(rs.getString("codpue_estsol"));
				contr.setTippue(rs.getString("tippue_estsol"));
				
			}else if(contr.getTipo().equals("C")){
				contr.setPaso(rs.getInt("numniv_contrato"));
				contr.setTipcon(rs.getInt("tipcon_contrato"));
				contr.setSumfij(rs.getInt("sumfij_contrato"));
				contr.setFecini(rs.getInt("fecini_contrato"));
				contr.setFecfin(rs.getInt("fecfin_contrato"));	
				contr.setCodorg(rs.getString("codorg_contrato"));
				contr.setCodpla(rs.getString("codpla_contrato"));
				contr.setCanhor(rs.getInt("canhor_contrato"));
				contr.setServicio(rs.getString("serpr_contrato"));
				contr.setCodsuc(rs.getInt("codsuc_contrato"));
				contr.setEscfon(rs.getInt("escfon_contrato"));
				contr.setCodpue(rs.getString("codpue_contrato"));
				contr.setTippue(rs.getString("tippue_contrato"));
				
			}
			asignarDatosPersona(contr,rs);			
		}
		asignarHoras( contr , rs ,true);	
		String rut = null;
		rut = listRutDupli.get(rs.getString("rutide"));
		
		if(rut != null){			
			contr.setEstado("I");			
			rechazados.put( contr.getNumeroAnexo(), contr);
		}else if(rs.getInt("fecini_contrato") == 0 && contr.getTipo().equals("C")){
			contr.setEstado("T");			
			rechazados.put( contr.getNumeroAnexo(), contr);
		}else if(contr.getCodpue().trim().equals("")){
			contr.setEstado("G");			
			rechazados.put( contr.getNumeroAnexo(), contr);
		}else if(contr.getCodpla() == null ||contr.getCodpla().trim().equals("") || contr.getCodpla().trim().equals("0")){
			contr.setEstado("N");
			rechazados.put( contr.getNumeroAnexo(), contr);
		}else{
			listMapAyudantes.put(contr.getNumeroAnexo(), contr);
		}	
	}
	public  static void optenerAyudantes(HashMap<String, Contrato> listMapAyudantes,HashMap<String, Contrato> rechasados,HashMap<String, String> listRutDupli){
		
		String sql = "SELECT estsol.numint numint,CONANX CONANX,fecrem,sol.rutide,sol.digide,sucur,sol.fecsol fecsol_solicitud,sol.fecini fecini_solicitud,sol.fecfin fecfin_solicitud "
					+"      ,sol.tipcon tipcon_solicitud,sol.valmes valmes_solicitud,sol.estsol estsol_solicitud,sol.canhor canhor_solicitud,sol.codorg codorg_solicitud "
					+"      ,hor.asialf asialf_horas,hor.asinum asibun_horas,hor.parale parale_horas,hor.CANHOR CANHOR_horas,hor.valhor valhor_horas,con.numniv numniv_contrato "
					+"      ,con.sumfij sumfij_contrato,con.jornad jornad_contrato,con.fecini fecini_contrato,con.fecfin fecfin_contrato,con.fecant fecant_contrato "
					+"      ,con.fecter fecter_contrato,con.indcol indcol_contrato,con.indmov indmov_contrato,con.codafp codafp_contrato,con.indjub indjub_contrato "
					+"      ,con.indsal indsal_contrato,con.valaho valaho_contrato,con.uniaho uniaho_contrato,con.codisa codisa_contrato,con.uniisa uniisa_contrato "
					+"      ,con.valisa valisa_contrato,con.antqui antqui_contrato,con.inddep inddep_contrato,con.sermed sermed_contrato,con.cuomor cuomor_contrato "
					+"      ,con.sindi1 sindi1_contrato,con.tipjer tipjer_contrato,con.horini horini_contrato,con.horfin horfin_contrato,con.resolu resolu_contrato "
					+"      ,con.fecres fecres_contrato,con.impcon impcon_contrato,con.fecing fecing_contrato,con.segces segces_contrato,con.indbon indbon_contrato "
					+"      ,con.codban codban_contrato,con.cueban cueban_contrato,con.feccon feccon_contrato,con.fecpas fecpas_contrato,con.uniaug uniaug_contrato "
					+"      ,con.valaug valaug_contrato,con.fecint fecint_contrato,con.codorg codorg_contrato,con.tippue tippue_contrato,con.codpue codpue_contrato "
					+"      ,pers.apepat apepat_persona,pers.apemat apemat_persona,pers.nombre nombre_persona,pers.estciv estciv_persona,pers.sexo sexo_persona "
					+"      ,pers.fecnac fecnac_persona,pers.lugnac lugnac_persona,pers.painac painac_persona,pers.nacion nacion_persona,pers.detnac detnac_persona "
					+"      ,pers.domici domici_persona,pers.comuna comuna_persona,pers.ciudad ciudad_persona,pers.region region_persona,pers.codgra codgra_persona "
					+"      ,pers.gratit gratit_persona,pers.codtit codtit_persona,pers.telefo telefo_persona,pers.celula celula_persona,pers.corele corele_persona "
					+"      ,pers.domele domele_persona ,con.tipcon tipcon_contrato ,sol.tipcon tipcon_solicitud , con.codpla codpla_contrato ,sol.canhor canhor_solicitud " 
				    +"      ,con.canhor canhor_contrato,sol.nomtra serpr_solicitud, con.serpr1 serpr_contrato,sol.sucur codsuc_solicitud ,con.codsuc codsuc_contrato " 
				    +"      ,con.escfon escfon_contrato,pers.feccam feccam_persona ,con.codcar codcar_contrato ,estsol.codpue codpue_estsol ,estsol.tippue tippue_estsol "
				    +"      ,afp.TIPINS TIPINS_AFP "
					+" FROM transferbt.APPEF81 estsol "
					+" LEFT join remunerabt.appef06 sol on estsol.numint = sol.numint " 
					+" LEFT join remunerabt.appef08 hor on hor.numint = sol.numint "
					+" LEFT join remunerabt.appef05 con on con.rutide = sol.rutide and con.tipcon = sol.tipcon and sol.FECINI = con.FECINI "
					+" LEFT join remunerabt.appef01 pers on sol.rutide = pers.rutide "
					+" LEFT JOIN REMUNERABT.APREF06 afp on afp.INSPRE = con.CODAFP "
					+" where ESTTRA = ' ' ";
		ConexionAs400RRHHI con = new ConexionAs400RRHHI();
		
		try {
			PreparedStatement sent = con.getConnection().prepareStatement(sql);
			try {
				ResultSet rs = sent.executeQuery();
				
				while(rs.next()){					
					asingnarDatosContratoAy(listMapAyudantes,rs,true,rechasados,listRutDupli);					
				}
			} catch (SQLException e) {
				System.out.print("SQL error HibernateIntegracionDao.optenerAyudantes " + e.getMessage());
			}
			con.getConnection().close();
		} catch (SQLException e) {
			System.out.print("Error HibernateIntegracionDao.optenerAyudantes " + e.getMessage());
		}
		
	}
	
	private static void guardarContrato(Contrato contrato ,ConexionBannerRRHH con) throws SQLException{
		
		String sql = "{ call PKG_CONSULTA_USUARIO_RRHH.INS_CON_ANEX_DATOS( "
																    +" PI_NUMERO_ANEXO => ?, " //1
																    +" PI_TIPO_DOC => ?, "//2
																    +" PI_PASO => ?, "//3
																    +" PI_TIPCON => ?, "//4
																    +" PI_SUMFIJ => ?, "//5
																    +" PI_JORNADA => ?, "//6
																    +" PI_FECINI => ?, "//7
																    +" PI_FECFIN => ?, "//8
																    +" PI_FECANT => ?, "//9
																    +" PI_FECTER => ?, "//10
																    +" PI_INDCOL => ?, "//11
																    +" PI_INDMOV => ?, "//12
																    +" PI_CODAFP => ?, "//13
																    +" PI_INDJUB => ?, "//14
																    +" PI_INDSAL => ?, "//15
																    +" PI_VALAHO => ?, "//16
																    +" PI_CODISA => ?, "//17
																    +" PI_UNIISA => ?, "//18
																    +" PI_VALISA => ?, "//19
																    +" PI_ANTQUI => ?, "//20
																    +" PI_INDDEP => ?, "//21
																    +" PI_SERMED => ?, "//22
																    +" PI_CUOMOR => ?, "//23
																    +" PI_SINDI1 => ?, "//24
																    +" PI_TIPJER => ?, "//25
																    +" PI_HORINI => ?, "//26
																    +" PI_HORFIN => ?, "//27
																    +" PI_RESOLU => ?, "//28
																    +" PI_FECRES => ?, "//29
																    +" PI_FECING => ?, "//30
																    +" PI_SEGCES => ?, "//31
																    +" PI_INDBON => ?, "//32
																    +" PI_CODBAN => ?, "//33
																    +" PI_CUEBAN => ?, "//34
																    +" PI_FECCON => ?, "//35
																    +" PI_FECPAS => ?, "//36
																    +" PI_UNIAUG => ?, "//37
																    +" PI_VALAUG => ?, "//38
																    +" PI_FECINT => ?, "//39
																    +" PI_CODORG => ?, "//40
																    +" PI_TIPPUE => ?, "//41
																    +" PI_CODPUE => ?, "//42
																    +" PI_RUT => ?, "//43
																    +" PI_DIGIDE => ?, "//44
																    +" PI_APEPAT => ?, "//45
																    +" PI_APEMAT => ?, "//46
																    +" PI_NOMBRE => ?, "//47
																    +" PI_ESTCIV => ?, "//48
																    +" PI_SEXO => ?, "//49
																    +" PI_FECNAC => ?, "//50
																    +" PI_LUGNAC => ?, "//51
																    +" PI_PAINAC => ?, "//52
																    +" PI_NACION => ?, "//53
																    +" PI_DETNAC => ?, "//54
																    +" PI_DOMICI => ?, "//55
																    +" PI_COMUNA => ?, "//56
																    +" PI_CIUDAD => ?, "//57
																    +" PI_REGION => ?, "//58
																    +" PI_CODGRA => ?, "//59
																    +" PI_GRATIT => ?, "//60
																    +" PI_CODTIT => ?, "//61
																    +" PI_TELEFO => ?, "//62
																    +" PI_CELULA => ?, "//63
																    +" PI_CORELE => ?, "//64
																    +" PI_DOMELE => ?, "//65
																    +" PI_CODPLA => ?, "//66
																    +" PI_CANHOR => ?, "//67
																    +" PI_SERPR  => ?, "//68
																    +" PI_CODSUC => ?, "//69
																    +" PI_ESCFON => ?, "//70
																    +" PI_FECCAM => ?, "//71
																    +" PI_CODCAR => ?, "//72
																    +" PI_TIPSOL => ?, "//73
																    +" PI_TIPINS => ?  "//74
																  +")}"; 
		CallableStatement sent = con.getConexion().prepareCall(sql);
		sent.setInt(1, Integer.parseInt(contrato.getNumeroAnexo().substring(1)));
		sent.setString(2, contrato.getTipo().trim());
		sent.setInt(3,contrato.getPaso());
		sent.setInt(4,contrato.getTipcon());
		sent.setInt(5,contrato.getSumfij());
		sent.setString(6,""+contrato.getJornada());
		sent.setInt(7,contrato.getFecini());
		sent.setInt(8,contrato.getFecfin());
		sent.setInt(9,contrato.getFecant());
		sent.setInt(10,contrato.getFecter());
		sent.setInt(11,contrato.getIndcol());
		sent.setInt(12,contrato.getIndmov());
		sent.setInt(13,contrato.getCodafp());
		sent.setInt(14,contrato.getIndjub());
		sent.setInt(15,contrato.getIndsal());
		sent.setInt(16,contrato.getValaho());
		sent.setInt(17,contrato.getCodisa());
		sent.setString(18,contrato.getUniisa().trim());
		sent.setInt(19,contrato.getValisa());
		sent.setInt(20,contrato.getAntqui());
		sent.setInt(21,contrato.getInddep());
		sent.setInt(22,contrato.getSermed());
		sent.setInt(23,contrato.getCuomor());
		sent.setInt(24,contrato.getSindi1());
		sent.setString(25,contrato.getTipjer().trim());
		sent.setInt(26,contrato.getHorini());
		sent.setInt(27,contrato.getHorfin());
		sent.setInt(28,contrato.getResolu());
		sent.setInt(29,contrato.getFecres());
		sent.setInt(30,contrato.getFecing());
		sent.setInt(31,contrato.getSegces());
		sent.setInt(32,contrato.getIndbon());
		sent.setInt(33,contrato.getCodban());
		sent.setString(34,contrato.getCueban().trim());
		sent.setInt(35,contrato.getFeccon());
		sent.setInt(36,contrato.getFecpas());
		sent.setString(37,contrato.getUniaug().trim());
		sent.setInt(38,contrato.getValaug());
		sent.setInt(39,contrato.getFecint());
		sent.setString(40,contrato.getCodorg().trim());
		sent.setString(41,contrato.getTippue().trim());
		sent.setString(42,contrato.getCodpue().trim());
		sent.setInt(43,contrato.getPersona().getRut());
		sent.setString(44,contrato.getPersona().getDigide().trim());
		sent.setString(45,contrato.getPersona().getApepat().trim());
		sent.setString(46,contrato.getPersona().getApepat().trim());
		sent.setString(47,contrato.getPersona().getNombre().trim());
		sent.setInt(48,contrato.getPersona().getEstciv());
		sent.setInt(49,contrato.getPersona().getSexo());
		sent.setInt(50,contrato.getPersona().getFecnac());
		sent.setString(51,contrato.getPersona().getLugnac().trim());
		sent.setString(52,contrato.getPersona().getPainac().trim());
		sent.setInt(53,contrato.getPersona().getNacion());
		sent.setString(54,contrato.getPersona().getDetnac().trim());
		sent.setString(55,contrato.getPersona().getDomici().trim().replaceAll("\n", " ").replaceAll("\t", " "));
		sent.setInt(56,contrato.getPersona().getComuna());
		sent.setInt(57,contrato.getPersona().getCiudad());
		sent.setInt(58,contrato.getPersona().getRegion());
		sent.setInt(59,contrato.getPersona().getCodgra());
		sent.setInt(60,contrato.getPersona().getGratit());
		sent.setInt(61,contrato.getPersona().getCodtit());
		sent.setString(62,contrato.getPersona().getTelefo().trim());
		sent.setString(63,contrato.getPersona().getCelula().trim());
		sent.setString(64,contrato.getPersona().getCorele().trim());
		sent.setString(65,contrato.getPersona().getDomele().trim());
		sent.setString(66,contrato.getCodpla().trim());
		sent.setInt(67,contrato.getCanhor());
		sent.setString(68,contrato.getServicio());
		sent.setInt(69,contrato.getCodsuc());
		sent.setInt(70,contrato.getEscfon());
		sent.setInt(71,contrato.getFeccam());
		sent.setInt(72,contrato.getCodcar());
		sent.setString(73,contrato.getNumeroAnexo().substring(0,1));
		sent.setString(74, contrato.getTipins());
		sent.execute();
		sent.close();
	}
	private static void guardarHoras(Contrato contrato ,ConexionBannerRRHH con) throws SQLException {
		
		String sql = "{call PKG_CONSULTA_USUARIO_RRHH.INSERT_CON_ANEX_HORAS( "
																	    +" PI_ASIAFL => ?, " //1
																	    +" PI_ASINUM => ?, " //2
																	    +" PI_PARALE => ?, " //3
																	    +" PI_CANHOR => ?, " //4
																	    +" PI_NUMINT => ?, " //5
																	    +" PI_TIPCON => ?, " //6
																	    +" PI_TIPHOR => ?  " //7
																	    +" )}";
		CallableStatement sent = con.getConexion().prepareCall(sql);
		
		if( contrato.getPersona().getRut() == 7850403){
			System.out.print(contrato.getPersona().getRut() + "\n");
		}
		
		for (HoraClases horas : contrato.getHorasPracticas()) {	
			
			
			
			sent.setString(1, horas.getAsiafl());
			sent.setInt(2, horas.getAsinum());
			sent.setString(3, horas.getAsiafl());
			sent.setInt(4, horas.getCanhor());
			sent.setInt(5, Integer.parseInt(contrato.getNumeroAnexo().substring(1)));
			sent.setInt(6, contrato.getTipcon());
			if(contrato.getNumeroAnexo().substring(0,0).equals("A")){
				sent.setString(7,"A");
			}else{
				sent.setString(7,"P");
			}
			sent.execute();
		}
		for (HoraClases horas : contrato.getHorasTeoricas()) {			
			sent.setString(1, horas.getAsiafl());
			sent.setInt(2, horas.getAsinum());
			sent.setString(3, horas.getAsiafl());
			sent.setInt(4, horas.getCanhor());
			sent.setInt(5, Integer.parseInt(contrato.getNumeroAnexo().substring(1)));
			sent.setInt(6, contrato.getTipcon());
			sent.setString(7,"T");
			
			sent.executeBatch();
		}
		 sent.close();
		
	}
	public  static String interface15(ConexionBannerRRHH con,HashMap<String, Contrato> listMapAyudantes)throws SQLException{
		String respuesta = null;
		String sql = "{ call  PKG_INTERFACE_15.P_PASAR_CONTRATO( "
															  +"PO_CON_ANEX_ESTADO => ? "
															  +",mensaje => ? "
															  +") }";
		
			CallableStatement sent = con.getConexion().prepareCall(sql);
			sent.registerOutParameter(1, OracleTypes.CURSOR);
			sent.registerOutParameter(2, OracleTypes.VARCHAR);
			sent.execute();
			
			ResultSet rs = (ResultSet)sent.getObject(1);
			String cantidad = sent.getString(2);
			System.out.print("\n mensajes: "+cantidad+"\n");
			respuesta = cantidad; 
			String tipo ;
			Contrato contr ;
			try {
				while (rs.next()) {
					tipo=rs.getString("TIPSOL");									
					contr = listMapAyudantes.get(tipo+rs.getString("numint"));
					if(contr != null){
						contr.setEstado(rs.getString("ESTADO"));
					}				
				}
			}catch (SQLException e) {
				System.out.print("Error SQL:HibernateIntegracionDao.guardarListadoContrato " +e.getMessage());
				respuesta = "Error SQL:HibernateIntegracionDao.guardarListadoContrato " +e.getMessage()+'\n'+respuesta;
			} catch (Exception e) {
				System.out.print("Error :HibernateIntegracionDao.guardarListadoContrato " +e.getMessage());
				respuesta = "Error :HibernateIntegracionDao.guardarListadoContrato " +e.getMessage()+'\n'+respuesta;
			}
			return respuesta;
	}
	public  static String guardarListadoContrato(HashMap<String, Contrato> listMapAyudantes){
		String confirmacion = null;
		HashMap<String, EquivalenciaPlanta> equiPlanta  = new HashMap<String, EquivalenciaPlanta>();
		HashMap<String, EquivalenciaNivel> equiNivel = new HashMap<String, EquivalenciaNivel>();
		ProcesosDAO.mapEquiAs400ABanner(equiNivel, equiPlanta);
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		
		try {
			Collection<Contrato> contras = listMapAyudantes.values();
			con.getConexion().setAutoCommit(false);
			for (Contrato contr : contras) {
				if(contr != null){
					if(contr.getPaso()> 0){
						contr.setPaso(equiNivel.get(equiPlanta.get(contr.getCodpla()).getCodesc()+"-"+contr.getPaso()).getPaso());
						contr.setSumfij(equiNivel.get(equiPlanta.get(contr.getCodpla()).getCodesc()+"-"+contr.getPaso()).getValor());	
					}
					try {
						contr.setCodpla(equiPlanta.get(contr.getCodpla()).getCodplaBanner());
					} catch (Exception e) {
						System.out.print(contr.getCodpla()+"\n");
					}					
					guardarContrato(contr , con);
					guardarHoras   (contr , con);
				}
			}			
			confirmacion = interface15(con, listMapAyudantes);
			
			
		}catch(SQLException e){
			System.out.print("Error SQL:HibernateIntegracionDao.guardarListadoContrato " +e.getMessage() );
			confirmacion = "Error SQL:HibernateIntegracionDao.guardarListadoContrato " +e.getMessage()+'\n' ;
		}catch (Exception e) {
			System.out.print("Error:HibernateIntegracionDao.guardarListadoContrato " +e.getMessage() );
			confirmacion = "Error:HibernateIntegracionDao.guardarListadoContrato " +e.getMessage()+'\n' ;
		}finally {
			try {
				con.getConexion().commit();
				con.getConexion().close();
			} catch (SQLException e) {
				
				
			}
			
		}
	
			
		
		return confirmacion;
	}
	private static void actuaSolicitud(ArrayList<Contrato> solic ,String tabla ,String estado ,ConexionAs400RRHHI con) throws SQLException{
		
		//System.out.print(tabla+"/"+estado+"\n");
		String sql = " update "+ tabla+" "
					+" set "
					+" ESTTRA = ? "
					+",FECTRA = ? "
					+",USUTRA = ? "
					+" where NUMINT in ( ";
		int count = 1;
		for (int i = 0; i < solic.size(); i++) {
			if(count == 1){
				sql += " ? ";
			}else{
				sql += " ,? ";
			}
			count ++;
		}		
		sql += ")";
		SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");
		Calendar fecha  =  Calendar.getInstance();
		String fecAS = format1.format(fecha.getTime());
		int fecint = Util.validaParametro(fecAS, 0);
		PreparedStatement sent = con.getConnection().prepareStatement(sql);
		sent.setString(1, estado);
		sent.setInt(2, fecint);
		sent.setString(3, "SIIFRRHHI");
		count = 4;
		for (Contrato contrato : solic) {
			
			sent.setInt(count, Util.validaParametro(contrato.getNumeroAnexo().substring(1), 0));
			count ++;
			//System.out.print(contrato.getNumeroAnexo()+"/" +contrato.getNumeroAnexo().substring(1)+"/"+estado +"\n");
		}
		sent.execute();
	    
	}	
	public static void actualizarEstados(HashMap<String, Contrato> listMapAyudantes ,HashMap<String, Contrato> rechasados){
		//System.out.print("actualiza contratos");
		
		HashMap<String, ArrayList<Contrato>> listArrayAyudante = new HashMap<String, ArrayList<Contrato>>();
		listArrayAyudante.put("S", new ArrayList<Contrato>());
		listArrayAyudante.put("F", new ArrayList<Contrato>());
		listArrayAyudante.put("C", new ArrayList<Contrato>());
		listArrayAyudante.put("A", new ArrayList<Contrato>());
		listArrayAyudante.put("D", new ArrayList<Contrato>());
		listArrayAyudante.put("E", new ArrayList<Contrato>());
		listArrayAyudante.put("B", new ArrayList<Contrato>());
		listArrayAyudante.put("H", new ArrayList<Contrato>());
		listArrayAyudante.put("G", new ArrayList<Contrato>());
		listArrayAyudante.put("T", new ArrayList<Contrato>());
		listArrayAyudante.put("N", new ArrayList<Contrato>());
		listArrayAyudante.put("I", new ArrayList<Contrato>());
		listArrayAyudante.put("Y", new ArrayList<Contrato>());
		listArrayAyudante.put("R", new ArrayList<Contrato>());
				
		HashMap<String, ArrayList<Contrato>> listArrayOtros = new HashMap<String, ArrayList<Contrato>>();
		listArrayOtros.put("S", new ArrayList<Contrato>());
		listArrayOtros.put("F", new ArrayList<Contrato>());
		listArrayOtros.put("C", new ArrayList<Contrato>());
		listArrayOtros.put("A", new ArrayList<Contrato>());
		listArrayOtros.put("D", new ArrayList<Contrato>());
		listArrayOtros.put("E", new ArrayList<Contrato>());
		listArrayOtros.put("B", new ArrayList<Contrato>());
		listArrayOtros.put("H", new ArrayList<Contrato>());
		listArrayOtros.put("G", new ArrayList<Contrato>());
		listArrayOtros.put("T", new ArrayList<Contrato>());
		listArrayOtros.put("N", new ArrayList<Contrato>());
		listArrayOtros.put("I", new ArrayList<Contrato>());
		listArrayOtros.put("Y", new ArrayList<Contrato>());
		listArrayOtros.put("R", new ArrayList<Contrato>());
		String tipoTabla = "";
		Contrato recon = new Contrato();
		for (String key : rechasados.keySet()) {
			 tipoTabla = key.substring(0, 1);
			 recon = rechasados.get(key);
			 if(tipoTabla.equals("A")){
				 try{
					 listArrayAyudante.get(recon.getEstado().trim()).add(recon);
				 }catch (Exception e) {
						System.out.print("problema"+ recon.getEstado()+"\n");
				 }
			 }else if(tipoTabla.equals("N")){
				 try{
					 listArrayOtros.get(recon.getEstado()).add(recon);
				}catch (Exception e) {
					System.out.print("problema"+ recon.getEstado()+"\n");
				}				 
			}	
		}
		tipoTabla = "";		
		for(String key : listMapAyudantes.keySet()){
			tipoTabla = key.substring(0, 1);
			recon = listMapAyudantes.get(key);
			if(tipoTabla.equals("A")){
				listArrayAyudante.get(recon.getEstado()).add(recon);
			}else if (tipoTabla.equals("N")){
				listArrayOtros.get(recon.getEstado()).add(recon);			
			}			
		}		
		ConexionAs400RRHHI con = new ConexionAs400RRHHI();
		
		try {
			for (String key : listArrayAyudante.keySet()) {
				
				if(listArrayAyudante.get(key).size()>0){
					//System.out.print("\n solicitud ayudante estado:"+key+" cantidad:"+listArrayAyudante.get(key).size());
					actuaSolicitud( listArrayAyudante.get(key) , "transferbt.appef81" , key , con);
				}
				
			}
			for (String key : listArrayOtros.keySet()) {
				
				if(listArrayOtros.get(key).size()>0){
					//System.out.print("\n solicitud otros estado:"+key+" cantidad:"+listArrayOtros.get(key).size());
					actuaSolicitud( listArrayOtros.get(key) , "transferbt.appef82" , key , con);
				}				
			}			
		con.getConnection().close();	
		}catch (SQLException e) {
				System.out.print("SQLError HibernateIntegracionDao.actualizarEstados: " + e.getMessage());		
		} catch (Exception e) {
				System.out.print("Error HibernateIntegracionDao.actualizarEstados: " + e.getMessage());
		}
			
	}		
	public static HashMap<String, String> listPidDupl(){
		ConexionBannerRRHH con = new ConexionBannerRRHH();
		String sql = "{call PKG_CONSULTA_USUARIO_RRHH.RUT_DUPLICADOS( PO_C_RUT_DUPL => ? )}";
		HashMap<String, String> list = new HashMap<String, String>();
		String rut ;
		try {		
			CallableStatement sent = con.getConexion().prepareCall(sql);
			try {
				sent.registerOutParameter(1, OracleTypes.CURSOR);				
				sent.execute();
				ResultSet rs = (ResultSet)sent.getObject(1);
				while (rs.next()) {
					rut =  rs.getString("SPRIDEN_ID").substring(0,(rs.getString("SPRIDEN_ID").length()-1));
					list.put(rut, rs.getString("SPRIDEN_ID"));					
				}
				con.getConexion().close();
			} catch (SQLException e) {
				con.getConexion().close();
				System.out.print("SQL error HibernateIntegracionDao.listPidDupl " + e.getMessage());
			}catch (Exception e){
				System.out.print("Error HibernateIntegracionDao.listPidDupl " + e.getMessage());
			}
			
		} catch (SQLException e) {
			System.out.print("SQL error HibernateIntegracionDao.listPidDupl " + e.getMessage());
		} catch (Exception e){
			System.out.print("Error HibernateIntegracionDao.listPidDupl " + e.getMessage());
		}
		
		
		
		return list;
	}  
}