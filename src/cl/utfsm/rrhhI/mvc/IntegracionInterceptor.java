package cl.utfsm.rrhhI.mvc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.rrhhG.modulo.GurfeedNomina;
import cl.utfsm.rrhhI.datos.HibernateIntegracionDao;
import cl.utfsm.rrhhI.modulo.Contrato;
import cl.utfsm.rrhhI.modulo.ModuloIntegracion;

public class IntegracionInterceptor  extends HandlerInterceptorAdapter {
	ModuloIntegracion moduloIntegracion;

	public ModuloIntegracion getModuloIntegracion() {
		return moduloIntegracion;
	}

	public void setModuloIntegracion(ModuloIntegracion moduloIntegracion) {
		this.moduloIntegracion = moduloIntegracion;
	}   
	public void cargarMenu(AccionWeb accionweb) throws Exception {		
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		accionweb.agregarObjeto("esIntegracion", 1); 
		accionweb.agregarObjeto("opcionMenu", tipo);		
	}
	public void traspasar(AccionWeb accionweb){
		System.out.print("comienso interface 1.5");
		HashMap<String, Contrato> rechasados = new HashMap<String, Contrato>();
		HashMap<String, Contrato> contrs = new HashMap<String, Contrato>();
		HashMap<String, String> listRutDupl = new HashMap<String, String>();
		listRutDupl = HibernateIntegracionDao.listPidDupl(); 
		//System.out.print(contrs.size()+"\n");
		HibernateIntegracionDao.optenerAyudantes(contrs,rechasados,listRutDupl);
		//System.out.print(contrs.size()+"\n");
		HibernateIntegracionDao.optenerNormales(contrs,rechasados,listRutDupl);
		//System.out.print("\n contratos:"+contrs.size());
		//System.out.print("\n rechasados:"+rechasados.size());
		
		boolean problRutDupli = false;
		
		for (Contrato cont : rechasados.values()) {
			if(cont.getEstado().equals("I")){
				problRutDupli = true;
				break;
			}
		}
		
		if(!problRutDupli){
			HibernateIntegracionDao.guardarListadoContrato(contrs);
			
			Contrato con = new Contrato();
			
			for(String key : contrs.keySet() ) {
				con = contrs.get(key);
				try {
					if(con != null && (con.getEstado().equals("R")
										|| con.getEstado().equals("D")
										|| con.getEstado().equals("E")
										|| con.getEstado().equals("B")
										|| con.getEstado().equals("H")
										|| con.getEstado().equals("G")
										|| con.getEstado().equals("T")
										|| con.getEstado().equals("I")
										|| con.getEstado().equals("N"))){
						rechasados.put(key, con);
					}
				} catch (Exception e) {
					
				}			
			}
			for (String key : rechasados.keySet()) {
				contrs.remove(key);
			}
			HibernateIntegracionDao.actualizarEstados(contrs,rechasados);		
		}
		HashMap<String, String> listEstado = HibernateIntegracionDao.estados();
		
		accionweb.agregarObjeto("estados", listEstado);
		accionweb.getSesion().setAttribute("estados", listEstado);
		accionweb.agregarObjeto("aceptados", contrs);
		accionweb.getSesion().setAttribute("aceptados", contrs);
		accionweb.agregarObjeto("rechasado", rechasados);
		accionweb.getSesion().setAttribute("rechasado", rechasados);
		accionweb.agregarObjeto("proRutDupl", problRutDupli);	
		System.out.print("fin interface 1.5");
	}
	public void expotarExcel(AccionWeb accionWeb){
		System.out.print("paso por aqui");	
		HashMap<String, String> estados = (HashMap<String, String>)accionWeb.getSesion().getAttribute("estados");
		HashMap<String, Contrato> rechasados = (HashMap<String, Contrato>)accionWeb.getSesion().getAttribute("rechasado");
		HashMap<String, Contrato> contrs = (HashMap<String, Contrato>)accionWeb.getSesion().getAttribute("aceptados");
		
		Vector vecRegDatos = new Vector();
		Vector vecDatos = new Vector();
		vecDatos.addElement("RUT");
		vecDatos.addElement("Solicitud");
		vecDatos.addElement("N� solicitud");
		vecDatos.addElement("Tipo solicitud");
		vecDatos.addElement("Raz�n");
		vecRegDatos.addElement(vecDatos);
		
		for (Contrato con : rechasados.values()) {
			vecDatos = new Vector();
			vecDatos.addElement((con.getPersona().getRutFormato() == null) ? "" : con.getPersona().getRutFormato());
			vecDatos.addElement((con.getNumeroAnexo().substring(0,1).equals("N"))  ? "Otros": "Ayudantes");
			vecDatos.addElement((con.getNumeroAnexo().substring(1) == null) ? "" : con.getNumeroAnexo().substring(1));
			vecDatos.addElement((con.getTipo().equals("C")) ? "Contrato" : "Anexo");
			vecDatos.addElement((con.getEstado() == null) ? "": estados.get(con.getEstado()));
						
			vecRegDatos.addElement(vecDatos); 
		}
		DateFormat hourFormat = new SimpleDateFormat(" yyyy-MM-dd 'HH'HH'MM'mm'SS'ss");
		accionWeb.getSesion().setAttribute("fecha", hourFormat.format(new Date()));
		accionWeb.getSesion().setAttribute("contenido", vecRegDatos);
		
	}
}