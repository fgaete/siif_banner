package cl.utfsm.rrhhI.modulo;

import java.util.ArrayList;

public class Contrato {
	
	private Persona persona;
	private ArrayList<HoraClases> horasPracticas;
	private ArrayList<HoraClases> horasTeoricas;
	private String numeroAnexo;
	private String tipoDoc;
	private String codpla;
	private int paso;
	private int tipcon;
	private int sumfij;
	private int jornada;
	private int fecini;
	private int fecfin;
	private int fecant;
	private int fecter;
	private int indcol;
	private int indmov;
	private int codafp;
	private String tipins;
	private int indjub;
	private int indsal;
	private int valaho;
	private int codisa;
	private String uniisa;
	private int valisa;
	private int antqui;
	private int inddep;
	private int sermed;
	private int cuomor;
	private int sindi1;
	private String tipjer;
	private int horini;
	private int horfin;
	private int resolu;
	private int fecres;
	private String impcon;
	private int fecing;
	private int segces;
	private int indbon;
	private int codban;
	private String cueban;
	private int feccon;
	private int fecpas;
	private int feccam;
	private String uniaug;
	private int valaug;
	private int fecint;
	private String codorg;
	private String tippue;
	private String codpue;
	private int canhor;
	private String servicio;
	private int codsuc;
	private int escfon;
	private int codcar;
	private String estado;
	
	public  Contrato(){
		
	}
		
	
	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public int getCodcar() {
		return codcar;
	}

	public void setCodcar(int codcar) {
		this.codcar = codcar;
	}

	public int getFeccam() {
		return feccam;
	}

	public void setFeccam(int feccam) {
		this.feccam = feccam;
	}



	public int getEscfon() {
		return escfon;
	}



	public void setEscfon(int escfon) {
		this.escfon = escfon;
	}



	public int getCodsuc() {
		return codsuc;
	}



	public void setCodsuc(int codsuc) {
		this.codsuc = codsuc;
	}



	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	
	public int getCanhor() {
		return canhor;
	}

	public void setCanhor(int canhor) {
		this.canhor = canhor;
	}

	public String getCodpla() {
		return codpla;
	}

	public void setCodpla(String codpla) {
		this.codpla = codpla;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public ArrayList<HoraClases> getHorasPracticas() {
		if(horasPracticas == null){
			horasPracticas = new ArrayList<HoraClases>();
		}
		return horasPracticas;
	}

	public void setHorasPracticas(ArrayList<HoraClases> horasPracticas) {
		
		this.horasPracticas = horasPracticas;
	}

	public ArrayList<HoraClases> getHorasTeoricas() {
		if(horasTeoricas == null){
			horasTeoricas = new ArrayList<HoraClases>();
		}
		return horasTeoricas;
	}

	public void setHorasTeoricas(ArrayList<HoraClases> horasTeoricas) {
		this.horasTeoricas = horasTeoricas;
	}

	public String getNumeroAnexo() {
		return numeroAnexo;
	}
	
	public void setNumeroAnexo(String numeroAnexo) {
		this.numeroAnexo = numeroAnexo;
	}

	public String getTipo() {
		return tipoDoc;
	}

	public void setTipo(String tipo) {
		this.tipoDoc = tipo;
	}

	public int getPaso() {
		return paso;
	}
	
	public int getTipcon() {
		return tipcon;
	}



	public void setTipcon(int tipcon) {
		this.tipcon = tipcon;
	}



	public void setPaso(int paso) {
		this.paso = paso;
	}

	public int getSumfij() {
		return sumfij;
	}

	public void setSumfij(int sumfij) {
		this.sumfij = sumfij;
	}

	public int getJornada() {
		return jornada;
	}

	public void setJornada(int jornada) {
		this.jornada = jornada;
	}

	public int getFecini() {
		return fecini;
	}

	public void setFecini(int fecini) {
		this.fecini = fecini;
	}

	public int getFecfin() {
		return fecfin;
	}

	public void setFecfin(int fecfin) {
		this.fecfin = fecfin;
	}

	public int getFecant() {
		return fecant;
	}

	public void setFecant(int fecant) {
		this.fecant = fecant;
	}

	public int getFecter() {
		return fecter;
	}

	public void setFecter(int fecter) {
		this.fecter = fecter;
	}

	public int getIndcol() {
		return indcol;
	}

	public void setIndcol(int indcol) {
		this.indcol = indcol;
	}

	public int getIndmov() {
		return indmov;
	}

	public void setIndmov(int indmov) {
		this.indmov = indmov;
	}

	public int getCodafp() {
		return codafp;
	}

	public void setCodafp(int codafp) {
		this.codafp = codafp;
	}
	

	public String getTipins() {
		return tipins;
	}


	public void setTipins(String tipins) {
		this.tipins = tipins;
	}


	public int getIndjub() {
		return indjub;
	}

	public void setIndjub(int indjub) {
		this.indjub = indjub;
	}

	public int getIndsal() {
		return indsal;
	}

	public void setIndsal(int indsal) {
		this.indsal = indsal;
	}

	public int getValaho() {
		return valaho;
	}

	public void setValaho(int valaho) {
		this.valaho = valaho;
	}

	public int getCodisa() {
		return codisa;
	}

	public void setCodisa(int codisa) {
		this.codisa = codisa;
	}

	public String getUniisa() {
		if(uniisa == null){
			uniisa = "";
		}
		return uniisa;
	}

	public void setUniisa(String uniisa) {
		this.uniisa = uniisa;
	}

	public int getValisa() {
		return valisa;
	}

	public void setValisa(int valisa) {
		this.valisa = valisa;
	}

	public int getAntqui() {
		return antqui;
	}

	public void setAntqui(int antqui) {
		this.antqui = antqui;
	}

	public int getInddep() {
		return inddep;
	}

	public void setInddep(int inddep) {
		this.inddep = inddep;
	}

	public int getSermed() {
		return sermed;
	}

	public void setSermed(int sermed) {
		this.sermed = sermed;
	}

	public int getCuomor() {
		return cuomor;
	}

	public void setCuomor(int cuomor) {
		this.cuomor = cuomor;
	}

	public int getSindi1() {
		return sindi1;
	}

	public void setSindi1(int sindi1) {
		this.sindi1 = sindi1;
	}

	public String getTipjer() {
		if(tipjer == null){
			tipjer = "";
		}
		return tipjer;
	}

	public void setTipjer(String tipjer) {
		this.tipjer = tipjer;
	}

	public int getHorini() {
		return horini;
	}

	public void setHorini(int horini) {
		this.horini = horini;
	}

	public int getHorfin() {
		return horfin;
	}

	public void setHorfin(int horfin) {
		this.horfin = horfin;
	}

	public int getResolu() {
		return resolu;
	}

	public void setResolu(int resolu) {
		this.resolu = resolu;
	}

	public int getFecres() {
		return fecres;
	}

	public void setFecres(int fecres) {
		this.fecres = fecres;
	}

	public String getImpcon() {
		return impcon;
	}

	public void setImpcon(String impcon) {
		this.impcon = impcon;
	}

	public int getFecing() {
		return fecing;
	}

	public void setFecing(int fecing) {
		this.fecing = fecing;
	}

	public int getSegces() {
		return segces;
	}

	public void setSegces(int segces) {
		this.segces = segces;
	}

	public int getIndbon() {
		return indbon;
	}

	public void setIndbon(int indbon) {
		this.indbon = indbon;
	}

	public int getCodban() {
		return codban;
	}

	public void setCodban(int codban) {
		this.codban = codban;
	}

	public String getCueban() {
		return cueban;
	}

	public void setCueban(String cueban) {
		if(cueban == null || cueban.equals("0")){
			cueban = "";
		}
		this.cueban = cueban;
		
	}

	public int getFeccon() {
		return feccon;
	}

	public void setFeccon(int feccon) {
		this.feccon = feccon;
	}

	public int getFecpas() {
		return fecpas;
	}

	public void setFecpas(int fecpas) {
		this.fecpas = fecpas;
	}

	public String getUniaug() {
		if(uniaug == null){
			uniaug = "";
		}
		return uniaug;
	}

	public void setUniaug(String uniaug) {
		this.uniaug = uniaug;
	}

	public int getValaug() {
		return valaug;
	}

	public void setValaug(int valaug) {
		this.valaug = valaug;
	}

	public int getFecint() {
		return fecint;
	}

	public void setFecint(int fecint) {
		this.fecint = fecint;
	}

	public String getCodorg() {
		return codorg;
	}

	public void setCodorg(String codorg) {
		this.codorg = codorg;
	}

	public String getTippue() {
		return tippue;
	}

	public void setTippue(String tippue) {
		this.tippue = tippue;
	}

	public String getCodpue() {
		return codpue;
	}

	public void setCodpue(String codpue) {
		this.codpue = codpue;
	}
	
	public int getCantHoraPract(){
		int cantHorasPrac = 0;
		for (HoraClases hr : horasPracticas) {
			cantHorasPrac += hr.getCanhor();
		}		
		return cantHorasPrac;
	}
	
	public int getCantHoraTeo(){
		int cantHorasTeo = 0;
		for (HoraClases hr : horasTeoricas) {
			cantHorasTeo += hr.getCanhor();
		}		
		return cantHorasTeo;
	}
	
}
