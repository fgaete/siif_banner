package cl.utfsm.rrhhI.modulo;

public class Persona {
	
	private int rut;
	private String digide;
	private String apepat;
	private String apemat;
	private String nombre;
	private int estciv;
	private int sexo;
	private int fecnac;
	private String lugnac;
	private String painac;
	private int nacion;
	private String detnac;
	private String domici;
	private int comuna;
	private int ciudad;
	private int region;
	private int codgra;
	private int gratit;
	private int codtit;
	private String telefo;
	private String celula;
	private String Corele;
	private String domele;
	
	public Persona(){
		
	}

	public int getRut() {
		return rut;
	}

	public void setRut(int rut) {
		this.rut = rut;
	}

	public String getDigide() {
		return digide;
	}

	public void setDigide(String digide) {
		this.digide = digide;
	}

	public String getApepat() {
		if(apepat == null){
			apepat = "";
		}
		return apepat;
	}

	public void setApepat(String apepat) {
		this.apepat = apepat;
	}

	public String getApemat() {
		if(apemat == null){
			apemat = "";
		}
		return apemat;
	}

	public void setApemat(String apemat) {
		this.apemat = apemat;
	}

	public String getNombre() {
		if(nombre == null){
			nombre = "";
		}
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEstciv() {
		
		return estciv;
	}

	public void setEstciv(int estciv) {
		this.estciv = estciv;
	}

	public int getSexo() {
		return sexo;
	}

	public void setSexo(int sexo) {
		this.sexo = sexo;
	}

	public int getFecnac() {
		return fecnac;
	}

	public void setFecnac(int fecnac) {
		this.fecnac = fecnac;
	}

	public String getLugnac() {
		if(lugnac == null){
			lugnac = "";
		}
		return lugnac;
	}

	public void setLugnac(String lugnac) {
		this.lugnac = lugnac;
	}

	public String getPainac() {
		if(painac == null){
			painac = "";
		}
		return painac;
	}

	public void setPainac(String painac) {
		this.painac = painac;
	}

	public int getNacion() {
		return nacion;
	}

	public void setNacion(int nacion) {
		this.nacion = nacion;
	}

	public String getDetnac() {
		if(detnac == null){
			detnac = "";
		}		
		return detnac;
	}

	public void setDetnac(String detnac) {
		this.detnac = detnac;
	}

	public String getDomici() {
		if(domici == null){
			domici = "";
		}	
		return domici;
	}

	public void setDomici(String domici) {
		this.domici = domici;
	}

	public int getComuna() {
		return comuna;
	}

	public void setComuna(int comuna) {
		this.comuna = comuna;
	}

	public int getCiudad() {
		return ciudad;
	}

	public void setCiudad(int ciudad) {
		this.ciudad = ciudad;
	}

	public int getRegion() {
		return region;
	}

	public void setRegion(int region) {
		this.region = region;
	}

	public int getCodgra() {
		return codgra;
	}

	public void setCodgra(int codgra) {
		this.codgra = codgra;
	}

	public int getGratit() {
		return gratit;
	}

	public void setGratit(int gratit) {
		this.gratit = gratit;
	}

	public int getCodtit() {
		return codtit;
	}

	public void setCodtit(int codtit) {
		this.codtit = codtit;
	}

	public String getTelefo() {
		if(telefo == null){
			telefo = "";
		}
		return telefo;
	}

	public void setTelefo(String telefo) {
		this.telefo = telefo;
	}

	public String getCelula() {
		if(celula == null){
			celula = "";
		}
		return celula;
	}

	public void setCelula(String celula) {
		this.celula = celula;
	}

	public String getCorele() {
		if(Corele == null){
			Corele = "";
		}
		return Corele;
	}

	public void setCorele(String corele) {
		Corele = corele;
	}

	public String getDomele() {
		if(domele == null){
			domele = "";
		}
		return domele;
	}

	public void setDomele(String domele) {
		this.domele = domele;
	}
	
	public String getRutFormato(){
		
		int largoOriginal = (this.rut+this.digide).length();
		int ultimaPosision  = largoOriginal;
		String rutFormato =  (this.rut+this.digide).substring(0,largoOriginal-1)+"-"+(this.rut+this.digide).substring(largoOriginal-1);
		ultimaPosision --;
		
		while(ultimaPosision > 3){
			
			rutFormato = rutFormato.substring(0,ultimaPosision-3)+"."+rutFormato.substring(ultimaPosision-3);
			ultimaPosision -= 3;			
		}		
		return rutFormato;
	}
	
}
