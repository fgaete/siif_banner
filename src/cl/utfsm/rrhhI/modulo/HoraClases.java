package cl.utfsm.rrhhI.modulo;

public class HoraClases {
	
	private String asiafl;
	private int asinum;
	private String parale;
	private int canhor;
	
	public HoraClases(){
		
	}

	public String getAsiafl() {
		return asiafl;
	}

	public void setAsiafl(String asiafl) {
		this.asiafl = asiafl;
	}

	public int getAsinum() {
		return asinum;
	}

	public void setAsinum(int asinum) {
		this.asinum = asinum;
	}

	public String getParale() {
		return parale;
	}

	public void setParale(String parale) {
		this.parale = parale;
	}

	public int getCanhor() {
		return canhor;
	}

	public void setCanhor(int canhor) {
		this.canhor = canhor;
	}
	
	
	
}
