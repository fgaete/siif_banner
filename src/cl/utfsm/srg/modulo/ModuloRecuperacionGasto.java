package cl.utfsm.srg.modulo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import cl.utfsm.base.util.ConexionBD;
import cl.utfsm.base.util.Util;
import cl.utfsm.srg.datos.RecuperacionGastoDao;
import descad.cliente.Ingreso_Documento;
import descad.cliente.MD5;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.ItemDTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw25DTO;

public class ModuloRecuperacionGasto {
	RecuperacionGastoDao recuperacionGastoDao;
	
	
	 public List<Presw18DTO> getTipoDocumento(){
		 Collection<Presw18DTO> listaPresw18 = null;
		 List<Presw18DTO> lista = new ArrayList<Presw18DTO>();
			 
		 listaPresw18 = this.getConsulta("TVD", 0, 0, 0, 0, 0);
				
		 for (Presw18DTO ss : listaPresw18){
			 Presw18DTO tipoDocumento = new Presw18DTO();
			 tipoDocumento.setTipmov(ss.getTipmov());
			 tipoDocumento.setDesuni(ss.getDesuni());
			 tipoDocumento.setIndprc(ss.getIndprc());// A - afecto ... E - exento
			 lista.add(tipoDocumento);
		 }	/*	 
		 Presw18DTO tipoDocumento = new Presw18DTO();
		 tipoDocumento.setTipmov("t1");
		 tipoDocumento.setDesuni("nomtipo");
		 lista.add(tipoDocumento);
		 
		 tipoDocumento = new Presw18DTO();
		 tipoDocumento.setTipmov("t2");
		 tipoDocumento.setDesuni("nomtipo 2");
		 lista.add(tipoDocumento);*/
		return lista;
		 }
	 public Collection<Presw18DTO> getVerificaRut(int rutnum, String dv, String tipo){
		  	PreswBean preswbean = null;
			Collection<Presw18DTO> lista = null;
			if(rutnum > 0){
				preswbean = new PreswBean(tipo,0,0,0,0, rutnum,0,"",0,dv, "",0,0,0,0,"","");
				lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			}
		
		 return lista;
		}
	 public Collection<Presw18DTO> getConsulta(String tippro, int codUnidad, int item, int anno, int mes, int rutIde){
			PreswBean preswbean = new PreswBean(tippro, codUnidad, item, anno, mes, rutIde);
			Collection<Presw18DTO> listaPresw18 = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			return listaPresw18;
		 }
	
	 public String getValidaDocumento(String tippro, int rutDoc, String dvDoc, int numeroDocumento, String tipoDocumento){
		  	PreswBean preswbean = null;
			Collection<Presw18DTO> lista = null;
			String indprc = "";
			preswbean = new PreswBean(tippro, 0, 0, 0, 0, rutDoc);
			preswbean.setDigide(dvDoc);
			preswbean.setNumdoc(numeroDocumento);
			preswbean.setTipdoc(tipoDocumento);
			lista = (Collection<Presw18DTO>) preswbean.consulta_presw18();
				   for (Presw18DTO ss : lista){
					   indprc = ss.getIndprc();
				   }	
		 return indprc;
		}
	 public void getEliminaListaSolicitaDocumento( HttpServletRequest req){
		 HttpSession sesion = req.getSession();
	     String valalf = Util.validaParametro(req.getParameter("valalf"),"");
	     String resval = Util.validaParametro(req.getParameter("resval"),"");
	     
		 List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO> )sesion.getAttribute("listaSolicitudDocumento");
		 List<Cocow36DTO>  listas = new ArrayList<Cocow36DTO>();
	
		 if(listaSolicitudDocumento != null && listaSolicitudDocumento.size()> 0)
			 sesion.removeAttribute("listaSolicitudDocumento");
		 if(listaSolicitudDocumento != null && listaSolicitudDocumento.size() > 0){
			 for (Cocow36DTO ss: listaSolicitudDocumento){
				 if(ss.getResval().trim().equals(resval.trim()) && ss.getValalf().trim().equals(valalf.trim()))
					 continue;
				 else
				 {
					 Cocow36DTO cocow36DTO = new Cocow36DTO();
					 cocow36DTO.setNomcam(ss.getNomcam());
					 cocow36DTO.setResval(ss.getResval());
					 cocow36DTO.setValalf(ss.getValalf());
					 cocow36DTO.setValnu1(ss.getValnu1());
					 cocow36DTO.setValnu2(ss.getValnu2());
					 listas.add(cocow36DTO);
				 }
			 }
			 
		
		 }
		
		   sesion.setAttribute("listaSolicitudDocumento", listas);
			
	 }
	 public void getEliminaListaSolicitaDistribucion( HttpServletRequest req){
		 HttpSession sesion = req.getSession();
	     int codUnidad = Util.validaParametro(req.getParameter("codUnidad"),0);
	        
		 List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO> )sesion.getAttribute("listaSolicitudDistribucion");
		 List<Cocow36DTO>  listas = new ArrayList<Cocow36DTO>();
	
		 if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size()> 0)
			 sesion.removeAttribute("listaSolicitudDistribucion");
		 if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
			 for (Cocow36DTO ss: listaSolicitudDistribucion){
				 if(ss.getValnu1() == codUnidad)
					 continue;
				 else
				 {
						Cocow36DTO cocow36DTO = new Cocow36DTO();
						cocow36DTO.setNomcam(ss.getNomcam());
						cocow36DTO.setValnu1(ss.getValnu1());
						cocow36DTO.setValnu2(ss.getValnu2());
						cocow36DTO.setValalf(ss.getValalf());
						listas.add(cocow36DTO);
				 }
			 }
			 
		
		 }
		
		   sesion.setAttribute("listaSolicitudDistribucion", listas);
			
	 }
	 
		synchronized  public int getRegistraRecuperacion(HttpServletRequest req, int rutide, String digide){
			int numSolicitud = 0;
			List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
			listCocow36DTO = getCreaCocow36(req, rutide, digide,"GSD");
			Ingreso_Documento ingresoDocumento = new Ingreso_Documento("GSD",rutide,digide);
			numSolicitud = ingresoDocumento.Ingresar_Documento(listCocow36DTO);
			
		
			return numSolicitud;
		}

		public boolean getActualizaRecuperacion(HttpServletRequest req, int rutide, String digide, int numDoc){
			boolean registra = false;
			List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
			listCocow36DTO = getCreaCocow36(req, rutide, digide,"MSR");
			Ingreso_Documento ingresoDocumento = new Ingreso_Documento("MSR",rutide,digide);
			ingresoDocumento.setNumdoc(numDoc);
				
			registra = ingresoDocumento.Actualizar_Documento(listCocow36DTO);
			
		
			return registra;
		}
		
		public List<Cocow36DTO> getCreaCocow36(HttpServletRequest req, int rutide, String digide, String tiping){
			List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO>) req.getSession().getAttribute("listaSolicitudDistribucion");
			List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO>) req.getSession().getAttribute("listaSolicitudDocumento");
		
			long rutnum = Util.validaParametro(req.getParameter("rutnum"), 0);
			String dvrut = Util.validaParametro(req.getParameter("dvrut"),"");
			String identificadorInterno = Util.validaParametro(req.getParameter("identificadorInterno"),"");	
			String glosa1 = Util.validaParametro(req.getParameter("glosa1"),"");	
			String glosa2 = Util.validaParametro(req.getParameter("glosa2"),"");
			String glosa3 = Util.validaParametro(req.getParameter("glosa3"),"");
			String glosa4 = Util.validaParametro(req.getParameter("glosa4"),"");
			String glosa5 = Util.validaParametro(req.getParameter("glosa5"),"");
			String glosa6 = Util.validaParametro(req.getParameter("glosa6"),"");
			long sede = Util.validaParametro(req.getParameter("sede"), 0);
			List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
			if(glosa1.trim().length() > 80)
				glosa1 = glosa1.substring(0,79);
			if(glosa2.trim().length() > 80)
				glosa2 = glosa2.substring(0,79);
			if(glosa3.trim().length() > 80)
				glosa3 = glosa3.substring(0,79);
			if(glosa4.trim().length() > 80)
				glosa4 = glosa4.substring(0,79);
			if(glosa5.trim().length() > 80)
				glosa5 = glosa5.substring(0,79);
			if(glosa6.trim().length() > 80)
				glosa6 = glosa6.substring(0,79);
			
		    //String tiping = "GSD";
		    String nomcam = "";
		    long valnu1 = 0;
		    long valnu2 = 0;
		    String valalf = "";
		    String resval = "";
		    long valrec = 0;
		

	    /* prepara archivo para grabar, llenar listCocow36DTO*/
	    nomcam = "RUTIDE";
	    valnu1 = rutnum;
	    valnu2 = 0;
	    valalf = "";
	    
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval);
	    
	    nomcam = "DIGIDE";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = dvrut;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );
	    
	    nomcam = "CODSUC";
	    valnu1 = sede;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );
	    
	    nomcam = "IDMEMO";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = identificadorInterno;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );
	    
	    nomcam = "DESCR1";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = glosa1;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );

	    nomcam = "DESCR2";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = glosa2;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );

	    nomcam = "DESCR3";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = glosa3;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );
	    
	    nomcam = "DESCR4";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = glosa4;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );

	    nomcam = "DESCR5";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = glosa5;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );

	    nomcam = "DESCR6";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = glosa6;
	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );
	    
	  	    
	    for (Cocow36DTO ss : listaSolicitudDocumento){
	    	nomcam = ss.getNomcam();
	 	    valnu1 = ss.getValnu1();
	 	    valnu2 = ss.getValnu2();
	 	    valalf = ss.getValalf();
	 	    resval = ss.getResval();
	 	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );
	 	   
	    }
	    for (Cocow36DTO ss : listaSolicitudDistribucion){
	    	nomcam = "CUENTA";
	 	    valnu1 = ss.getValnu1();
	 	    valnu2 = ss.getValnu2();
	 	    valalf = "";
	 	    resval = "";
	 	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );
	 	    valrec = valrec + valnu2;
	    }	
	 	nomcam = "VALREC";
 	    valnu1 = valrec;
 	    valnu2 = 0;
 	    valalf = "";
 	    resval = "";
 	    listCocow36DTO = getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf, resval );

		return listCocow36DTO;
		}
		 public List<Cocow36DTO> getAgregaLista(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf, String resval){
			 Cocow36DTO cocow36DTO = new Cocow36DTO();
			 cocow36DTO.setTiping(tiping);
			 cocow36DTO.setNomcam(nomcam);
			 cocow36DTO.setValnu1(valnu1);
			 cocow36DTO.setValnu2(valnu2);
			 cocow36DTO.setValalf(valalf);
			 cocow36DTO.setResval(resval);
			 lista.add(cocow36DTO);
			 return lista;
			 
		 }
		 public List<Cocow36DTO> getAgregaListaMD5(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf, String resval){
			 Cocow36DTO cocow36DTO = new Cocow36DTO();
			 cocow36DTO.setTiping(tiping);
			 cocow36DTO.setNomcam(nomcam);
			 cocow36DTO.setValnu1(valnu1);
			 cocow36DTO.setValnu2(valnu2);
			 cocow36DTO.setValalf(valalf);
			 cocow36DTO.setResval(resval);
			 lista.add(cocow36DTO);
			 return lista;
			 
		 }	
		 
		 public boolean saveAutoriza(int numdoc, int rutUsuario, String digide, String nomTipo, String tipCue){
				PreswBean preswbean = new PreswBean(nomTipo, 0,0,0,0, rutUsuario);
				preswbean.setDigide(digide);
				preswbean.setNumdoc(numdoc);
				preswbean.setTipcue(tipCue);
				
			return preswbean.ingreso_presw19();
		}
			public boolean saveRechaza(int numdoc, int rutUsuario, String digide, String tipo, String glosa, String tipCue){
				PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
				preswbean.setDigide(digide);
				preswbean.setNumdoc(numdoc);
				//preswbean.ingreso_presw19();
				
				List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
				Presw25DTO presw25DTO = new Presw25DTO();
				presw25DTO.setTippro(tipo);
				presw25DTO.setPres01(numdoc);
				presw25DTO.setComen1(glosa);
				presw25DTO.setRutide(rutUsuario);
				presw25DTO.setDigide(digide);
				presw25DTO.setIndexi(tipCue);
				lista.add(presw25DTO);
			return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
			}
			
		 
	public RecuperacionGastoDao getRecuperacionGastoDao() {
		return recuperacionGastoDao;
	}

	public void setRecuperacionGastoDao(RecuperacionGastoDao recuperacionGastoDao) {
		this.recuperacionGastoDao = recuperacionGastoDao;
	}

	public List<ItemDTO> getItemCocof139()throws SQLException{
		/*String query = 	" SELECT codpag, despag" +
						" FROM view_cocof139@badin" ;*/
		String query = 	" SELECT codpag, despag" +
						" FROM DBO_DB_UTFSM.view_cocof139" ;
		


		
	//System.out.println("query: "+ query);
	List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
	try {
		ConexionBD con = new ConexionBD();
		Statement sent = con.conexion().createStatement();
		ResultSet res = sent.executeQuery(query);
		while (res.next()) {
			ItemDTO item = new ItemDTO();
		    item.setItepre(res.getBigDecimal(1));
		    item.setNomite(res.getString(2));
		    listaItem.add(item);
	}
	res.close();
	sent.close();
	con.close();
	}
	catch (SQLException e) {
	System.out.println("Error en moduloRecuperacionGasto.getItemCocof139:" + e.toString());
	}
	return listaItem;					
	}

}
