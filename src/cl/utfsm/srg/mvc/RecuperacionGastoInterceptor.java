package cl.utfsm.srg.mvc;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.tools.generic.DateTool;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.siv.modulo.ModuloValePago;
import cl.utfsm.siv.mvc.ValePagoInterceptor;
import cl.utfsm.srg.modulo.ModuloRecuperacionGasto;
import descad.cliente.CocowBean;
import descad.cliente.Item;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.ItemDTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;
import descad.presupuesto.Presw25DTO;

public class RecuperacionGastoInterceptor extends HandlerInterceptorAdapter{
	private ModuloRecuperacionGasto moduloRecuperacionGasto;
	private ModuloValePago moduloValePago;
	
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		
	}
	
	public void cargarRecuperacion(AccionWeb accionweb) throws Exception {
		int control = Util.validaParametro(accionweb.getParameter("control"), 0);
      	accionweb.agregarObjeto("control", control);
      	if(accionweb.getSesion().getAttribute("listaSolicitudDistribucion") != null)
      		accionweb.getSesion().setAttribute("listaSolicitudDistribucion", null);
  		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
   		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
   		int rutOrigen = Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen")+ "",0);
   	  	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
          		accionweb.getSesion().removeAttribute("autorizaProyecto");
          	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
          		accionweb.getSesion().removeAttribute("autorizaUCP");
          	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
          		accionweb.getSesion().removeAttribute("autorizaDIRPRE");
          	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
    			accionweb.getSesion().removeAttribute("autorizaFinanzas");
    	/* se debe buscar segun el usuario si tiene permiso para autorizar*/
    	List<Presw18DTO> listaAutorizaciones = new ArrayList<Presw18DTO>();
    	listaAutorizaciones = moduloValePago.getConsultaAutoriza(rutUsuario, dv);
    	if(listaAutorizaciones != null && listaAutorizaciones.size() > 0){
    		 for (Presw18DTO ss : listaAutorizaciones){
    			if(ss.getIndprc().trim().equals("Y")){
    				accionweb.agregarObjeto("autorizaProyecto", 1);
    				accionweb.getSesion().setAttribute("autorizaProyecto", 1);
    			}
    			if(ss.getIndprc().trim().equals("X")){
    				accionweb.agregarObjeto("autorizaUCP", 1);
    				accionweb.getSesion().setAttribute("autorizaUCP", 1);
    			}
    			if(ss.getIndprc().trim().equals("Z")){
    				accionweb.agregarObjeto("autorizaDIRPRE", 1);
    				accionweb.getSesion().setAttribute("autorizaDIRPRE", 1);
    			}
    			if(ss.getIndprc().trim().equals("F")){
    				accionweb.agregarObjeto("autorizaFinanzas", 1);
    				accionweb.getSesion().setAttribute("autorizaFinanzas", 1);
    			}
    		 }
    	}
    		if(rutOrigen > 0){
    			this.limpiaSimulador(accionweb);
    		}
    	 		    			
    	accionweb.agregarObjeto("listaAutorizaciones", listaAutorizaciones);
    	accionweb.agregarObjeto("esRecuperacionGasto", 1); // es para mostrar el menu  de Recuperacion
	}
	
	public void verificaRut(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		int identificador = Util.validaParametro(accionweb.getParameter("identificador"), 0);
		if(accionweb.getSesion().getAttribute("listaSolicitudDistribucion") != null)
      		accionweb.getSesion().setAttribute("listaSolicitudDistribucion", null);
		if(accionweb.getSesion().getAttribute("listaSolicitudDocumento") != null)
			accionweb.getSesion().setAttribute("listaSolicitudDocumento", null);
		if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
			accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		
		if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
			accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));

		
		String nombre = "";
		if(rutnum > 0 && String.valueOf(rutnum).length() <= 8){
			Collection<Presw18DTO> lista = moduloRecuperacionGasto.getVerificaRut(rutnum, dvrut,"VRF");
			if(lista != null && lista.size() > 0){
				 for (Presw18DTO ss : lista){
					 nombre = ss.getDesuni();
				   }
			}
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
			
		}  
			
	   	if(!nombre.trim().equals("")){
	    		accionweb.agregarObjeto("hayDatoslista", 1);
	    		accionweb.agregarObjeto("nomIdentificador", nombre);
	    } else{
	    	if(String.valueOf(rutnum).length() > 8)
	    		accionweb.agregarObjeto("mensaje", "Este RUT est� inv�lido.");
	    	else
	    		accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado. Debe agregarlo.");
	    }
	    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
	    listSede = moduloValePago.getListaSede(accionweb.getReq());
	    String comboSede = "<select name=\"sede\" id=\"sede\" class=\"estilo_Azul\" onChange=\"\" >"+
	                       "<option value=\"-1\" selected=\"selected\">Seleccione</option> ";
	  //  if(identificador == 1)
	    for (Presw18DTO ss : listSede){
	    	comboSede += "<option value=" + ss.getNummes() + ">" + ss.getDesuni() + "</option>";
	    	
	    }
	    comboSede += "</select>";
	   
	    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
	    moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
	    accionweb.agregarObjeto("listaUnidades", listaUnidades);
	    
	    List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
	    listaTipoDocumento = moduloRecuperacionGasto.getTipoDocumento();
	    accionweb.agregarObjeto("listaTipoDocumento", listaTipoDocumento);
	    
		/*Item items = new Item();
		Collection<ItemDTO> lista = (Collection<ItemDTO>) items.todos_items();
		List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
		for(ItemDTO pp: lista){
			//System.out.println("incren: "+ pp.getIncren()+" "+ pp.getNomite()+"  "+ pp.getItepre());
			if(pp.getIncren().trim().equals("S"))
				listaItem.add(pp);
			
		}*/
	//	este acceso cambia a partir de 27/01/2015 pr instrucciones de correo PP accediendo a partir de ahora a la tabla cocof139 en lugar de cocof23
		List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
		listaItem = moduloRecuperacionGasto.getItemCocof139();
		accionweb.agregarObjeto("listaItem", listaItem);
	  
		DateTool fechaActual = new DateTool();
		String fecha = "";
		String dia = fechaActual.getDay()+"";
		if(dia.length() == 1)
			dia = "0" + dia;
		String mes = (fechaActual.getMonth()+1)+"";
		if(mes.length()== 1)
			mes = "0" + mes;
		fecha = dia + "/" + mes + "/" + String.valueOf(fechaActual.getYear());
  
	    accionweb.agregarObjeto("fecha", fecha);
	    accionweb.agregarObjeto("anioActual", String.valueOf(fechaActual.getYear()));
	    accionweb.agregarObjeto("comboSede", comboSede);
	    accionweb.agregarObjeto("listSede", listSede);
		accionweb.agregarObjeto("identificador", identificador);		  		  
		accionweb.agregarObjeto("esRecuperacionGasto", 1);   
		
	}
	public void verificaRutDocumento(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		String tipoDocumento = Util.validaParametro(accionweb.getParameter("tipoDocumento"),"");
		
		String nombre = "";
		String indproc = "";
		String indHay = "";
		if(rutnum > 0 && String.valueOf(rutnum).length() <= 8){
			Collection<Presw18DTO> lista = moduloRecuperacionGasto.getVerificaRut(rutnum, dvrut, "VRD");
			if(lista != null && lista.size() > 0){
				 for (Presw18DTO ss : lista){
					 nombre = ss.getDesuni();
					 indproc = ss.getIndprc();
					 indHay = ss.getIndhay();
				   }
			}
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
			
		}
		if(indproc.trim().equals("1") || String.valueOf(rutnum).length() > 8)
			accionweb.agregarObjeto("mensaje", "Este RUT es inv�lido.");
		else 
			if(!nombre.trim().equals("")){	    		
	    		accionweb.agregarObjeto("nomRut", nombre);
	    		if(indHay.trim().equals("A")) // datos OK
	    			accionweb.agregarObjeto("hayDatoslista", 1);
	    		else
	    			accionweb.agregarObjeto("mensaje", nombre);
	    		if(indHay.trim().equals("P") && tipoDocumento.trim().equals("H"))
	    			accionweb.agregarObjeto("mensaje", "Este RUT No existe en archivo maestro de proveedores.");
	    		else
	    			accionweb.agregarObjeto("mensaje", nombre);
	    		if(indHay.trim().equals("H") &&  !tipoDocumento.trim().equals("BHE") &&  tipoDocumento.trim().equals("BOH"))
	    			accionweb.agregarObjeto("mensaje", "Este RUT No existe en archivo maestro de prestadores de servicio.");
	    		else
	    			accionweb.agregarObjeto("mensaje", nombre);
	    			
		    } else
		    	accionweb.agregarObjeto("mensaje", "Este RUT no se encuentra registrado.");
		
	    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
	    listSede = moduloValePago.getListaSede(accionweb.getReq());
	    String comboSede = "<select name=\"sede\" id=\"sede\" class=\"estilo_Azul\" onChange=\"\" >"+
	                       "<option value=\"-1\" selected=\"selected\">Seleccione</option> ";
	  //  if(identificador == 1)
	    for (Presw18DTO ss : listSede){
	    	comboSede += "<option value=" + ss.getNummes() + ">" + ss.getDesuni() + "</option>";
	    	
	    }
	    comboSede += "</select>";
	   
	    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
	    moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
	    
	    List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
	    listaTipoDocumento = moduloRecuperacionGasto.getTipoDocumento();
	    accionweb.agregarObjeto("listaTipoDocumento", listaTipoDocumento);
	    
		/*Item items = new Item();
		Collection<ItemDTO> lista = (Collection<ItemDTO>) items.todos_items();
		List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
		for(ItemDTO pp: lista){
			//System.out.println("incren: "+ pp.getIncren()+" "+ pp.getNomite()+"  "+ pp.getItepre());
			if(pp.getIncren().trim().equals("S"))
				listaItem.add(pp);
			
		}*/
		//	este acceso cambia a partir de 27/01/2015 pr instrucciones de correo PP accediendo a partir de ahora a la tabla cocof139 en lugar de cocof23
		List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
		listaItem = moduloRecuperacionGasto.getItemCocof139();
		
		accionweb.agregarObjeto("listaItem", listaItem);
	  
		DateTool fechaActual = new DateTool();
		String fecha = "";
		String dia = fechaActual.getDay()+"";
		if(dia.length() == 1)
			dia = "0" + dia;
		String mes = (fechaActual.getMonth()+1)+"";
		if(mes.length()== 1)
			mes = "0" + mes;
		fecha = dia + "/" + mes + "/" + String.valueOf(fechaActual.getYear());
  
	    accionweb.agregarObjeto("fecha", fecha);
	    accionweb.agregarObjeto("comboSede", comboSede);
	    accionweb.agregarObjeto("esRecuperacionGasto", 1);   
	}
	public void verificaRutPago(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
		String rut_aux = Util.validaParametro(accionweb.getParameter("rut_aux"),"");
		int identificador = Util.validaParametro(accionweb.getParameter("identificador"), 0);
			
		String nombre = "";
		if(rutnum > 0 && String.valueOf(rutnum).length() <= 8){
			Collection<Presw18DTO> lista = moduloValePago.getVerificaRut(rutnum, dvrut);
			if(lista != null && lista.size() > 0){
				 for (Presw18DTO ss : lista){
					 nombre = ss.getDesuni();
				   }
			}
			accionweb.agregarObjeto("rut_aux", rut_aux);  
			accionweb.agregarObjeto("rutnum", rutnum); 
			accionweb.agregarObjeto("dvrut", dvrut); 
			
		}
	   	if(!nombre.trim().equals("")){
	    		accionweb.agregarObjeto("hayDatoslista", 1);
	    		accionweb.agregarObjeto("nomIdentificador", nombre);
	    } else{
	    	if(String.valueOf(rutnum).length() > 8)
	    		accionweb.agregarObjeto("mensaje", "Este RUT es inv�lido.");
	    	else
	    		accionweb.agregarObjeto("mensaje", "Este RUT no est� registrado. Debe agregarlo.");
	    }
	    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
	    listSede = moduloValePago.getListaSede(accionweb.getReq());
	    String comboSede = "<select name=\"sede\" id=\"sede\" class=\"estilo_Azul\" onChange=\"\" >"+
	                       "<option value=\"-1\" selected=\"selected\">Seleccione</option> ";
	  //  if(identificador == 1)
	    for (Presw18DTO ss : listSede){
	    	comboSede += "<option value=" + ss.getNummes() + ">" + ss.getDesuni() + "</option>";
	    	
	    }
	    comboSede += "</select>";
	   
	    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
	    moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
	    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
	    
	    List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
	    listaTipoDocumento = moduloRecuperacionGasto.getTipoDocumento();
	    accionweb.agregarObjeto("listaTipoDocumento", listaTipoDocumento);
	    
		/*Item items = new Item();
		Collection<ItemDTO> lista = (Collection<ItemDTO>) items.todos_items();
		List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
		for(ItemDTO pp: lista){
			//System.out.println("incren: "+ pp.getIncren()+" "+ pp.getNomite()+"  "+ pp.getItepre());
			if(pp.getIncren().trim().equals("S"))
				listaItem.add(pp);
			
		}
		accionweb.agregarObjeto("listaItem", listaItem);*/
		
		//	este acceso cambia a partir de 27/01/2015 pr instrucciones de correo PP accediendo a partir de ahora a la tabla cocof139 en lugar de cocof23
		List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
		listaItem = moduloRecuperacionGasto.getItemCocof139();
		accionweb.agregarObjeto("listaItem", listaItem);
	  
		DateTool fechaActual = new DateTool();
		String fecha = "";
		String dia = fechaActual.getDay()+"";
		if(dia.length() == 1)
			dia = "0" + dia;
		String mes = (fechaActual.getMonth()+1)+"";
		if(mes.length()== 1)
			mes = "0" + mes;
		fecha = dia + "/" + mes + "/" + String.valueOf(fechaActual.getYear());
  
	    accionweb.agregarObjeto("fecha", fecha);
	    accionweb.agregarObjeto("comboSede", comboSede);
	    accionweb.agregarObjeto("listSede", listSede);
		accionweb.agregarObjeto("identificador", identificador);		  		  
		accionweb.agregarObjeto("esRecuperacionGasto", 1);   
		
	}
	
	public void cargaResultadoSolicitudDocumento(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int numeroDocumento = Util.validaParametro(accionweb.getParameter("numeroDocumento"),0);
   	  	String tipoDocumento = Util.validaParametro(accionweb.getParameter("tipoDocumento"),"");
   	  	int rutnumDoc = Util.validaParametro(accionweb.getParameter("rutnumDoc"), 0);
   	  	String dvrutDoc = Util.validaParametro(accionweb.getParameter("dvrutDoc"),"");
   	  	long iva = Util.validaParametro(accionweb.getParameter("iva"), 0);
   	  	long totalDocumento = Util.validaParametro(accionweb.getParameter("totalDocumento"), 0);
   	  	String glosa = Util.validaParametro(accionweb.getParameter("glosa"),"");
   	  	long rubroGasto = Util.validaParametro(accionweb.getParameter("rubroGasto"),0);
   		List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumento");
   		List<Cocow36DTO>  lista = new ArrayList<Cocow36DTO>();
   		String validaDocumento = "";
   		long total = 0;
   	//	System.out.println("String.valueOf(numeroDocumento).length() "+String.valueOf(numeroDocumento).length());
		if(String.valueOf(rutnumDoc).length() <= 8 && String.valueOf(numeroDocumento).length() <= 8)
			validaDocumento = moduloRecuperacionGasto.getValidaDocumento("VDR", rutnumDoc, dvrutDoc, numeroDocumento, tipoDocumento);
	    if(!validaDocumento.trim().equals("") || String.valueOf(rutnumDoc).length() > 8 || String.valueOf(numeroDocumento).length() > 8) 
	     	{
	    	if(String.valueOf(rutnumDoc).length() > 8)
	    		accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > RUT Inv�lido.</font> ");
		    else {
		    	   if(String.valueOf(numeroDocumento).length() > 8)
		    		   accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > N�mero de Documento Inv�lido.</font>");
		    	   else {
	    			if(validaDocumento.trim().equals("1"))
	    				accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > Documento existe en cuentas Corrientes.</font>");
	    			if(validaDocumento.trim().equals("2"))
	    				accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > Documento existe en otra rendici�n.</font>");
	    			if(validaDocumento.trim().equals("3"))
	    				accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > Documento en proceso en otros sistemas.</font>");
		    	   }
	        	}
	    	lista = (List<Cocow36DTO>) listaSolicitudDocumento;
	    	if(lista != null && lista.size() > 0){
	    		for (Cocow36DTO ss : listaSolicitudDocumento){
	    			total = total + ss.getValnu2();	
	    		}
	    	}
	    } else {
			boolean agrega = true;
			DecimalFormat formato = new DecimalFormat("00000000");
			String rutFormateado = formato.format(rutnumDoc);
			formato = new DecimalFormat("00000000");
			String numeroFormateado = formato.format(numeroDocumento);
			formato = new DecimalFormat("000");
			String rubroFormateado = formato.format(rubroGasto);
			String dvrutDocFormato = dvrutDoc.trim();
			if(dvrutDocFormato.trim().equals(""))
				dvrutDocFormato = " ";
			
			if (listaSolicitudDocumento != null && listaSolicitudDocumento.size() > 0){
				for (Cocow36DTO ss : listaSolicitudDocumento){
					if(ss.getResval().trim().equals(rutFormateado.trim() + dvrutDocFormato + numeroFormateado.trim() ) && ss.getValalf().substring(0, 3).equals(tipoDocumento.trim()))
						agrega = false;
					total = total + ss.getValnu2();
			        
				}
				lista = (List<Cocow36DTO>) listaSolicitudDocumento;
			} else lista = new ArrayList<Cocow36DTO>();
			
			if(agrega){
				Cocow36DTO cocow36DTO = new Cocow36DTO();
				cocow36DTO.setNomcam("DOCASO");
				cocow36DTO.setValnu1(iva);
				cocow36DTO.setValnu2(totalDocumento);
				cocow36DTO.setResval(rutFormateado + dvrutDocFormato + numeroFormateado);
				cocow36DTO.setValalf(tipoDocumento + rubroFormateado + glosa);
				lista.add(cocow36DTO);
				total = total + totalDocumento;
		        
				
			} else 	{
					accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > El Documento N&deg; "+numeroFormateado+" ya se encuentra agregado.</font> .");
			}
			
			
	 
	    }
	    accionweb.agregarObjeto("totalSolDocumento", total);
        accionweb.agregarObjeto("listaSolicitudDocumento", lista);
		accionweb.getSesion().setAttribute("listaSolicitudDocumento", lista);
		List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
	    listaTipoDocumento = moduloRecuperacionGasto.getTipoDocumento();
	    accionweb.agregarObjeto("listaTipoDocumento", listaTipoDocumento);
	    
		/*Item items = new Item();
		Collection<ItemDTO> listaI = (Collection<ItemDTO>) items.todos_items();
		List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
		for(ItemDTO pp: listaI){
			//System.out.println("incren: "+ pp.getIncren()+" "+ pp.getNomite()+"  "+ pp.getItepre());
			if(pp.getIncren().trim().equals("S"))
				listaItem.add(pp);
			
		}
		accionweb.agregarObjeto("listaItem", listaItem);*/

		//	este acceso cambia a partir de 27/01/2015 pr instrucciones de correo PP accediendo a partir de ahora a la tabla cocof139 en lugar de cocof23
		List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
		listaItem = moduloRecuperacionGasto.getItemCocof139();
		accionweb.agregarObjeto("listaItem", listaItem);
		
	}
	public void cargaResultadoSolicitudDistribucion(AccionWeb accionweb) throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
		int cuentaPresup = Util.validaParametro(accionweb.getParameter("cuentaPresup"),0);
   	  	String desuni = Util.validaParametro(accionweb.getParameter("desuni"),"");
   	    long totalSolDocumento = Util.validaParametro(accionweb.getParameter("totalSolDocumento"), 0); 
   	    long valor = Util.validaParametro(accionweb.getParameter("valor"), 0);
		
	  
			List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
			boolean agrega = true;
			long total = 0;
			if (listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
				for (Cocow36DTO ss : listaSolicitudDistribucion){
					if(ss.getValnu1() == cuentaPresup)
						agrega = false;
					total = total + ss.getValnu2();
			        
				}
			} else listaSolicitudDistribucion = new ArrayList<Cocow36DTO>();
			
			if(agrega){
				if(String.valueOf(valor).length() > 8)
					accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > El valor ingresado es muy grande.</font> .");
				else {
				if(totalSolDocumento <= (total + valor))
					{
						Cocow36DTO cocow36DTO = new Cocow36DTO();
						cocow36DTO.setNomcam("CUENTA");
						cocow36DTO.setValnu1(cuentaPresup);
						cocow36DTO.setValnu2(valor);
						cocow36DTO.setValalf(desuni);
						listaSolicitudDistribucion.add(cocow36DTO);
						total = total + valor;
				        
				} else 	accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > El Valor excede el valor total de la Solicitud.</font> .");
				}
			} else 	accionweb.agregarObjeto("mensajeSolicitud", "<font class=\"Estilo_Rojo\" > Esta cuenta ya se encuentra agregada.</font> .");
			
			
            accionweb.agregarObjeto("listaSolicitudDistribucion", listaSolicitudDistribucion);
			accionweb.getSesion().setAttribute("listaSolicitudDistribucion", listaSolicitudDistribucion);
		    accionweb.agregarObjeto("totalSolDistribucion", total);
	 
	    		
	}
	 synchronized public void eliminaResultadoSolicitudDocumento(AccionWeb accionweb) throws Exception {
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			
			   moduloRecuperacionGasto.getEliminaListaSolicitaDocumento(accionweb.getReq());
			   	accionweb.agregarObjeto("mensaje", "Se elimin� el Documento.");
			    	
				List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumento");
		  
				long total = 0;
				if (listaSolicitudDocumento != null && listaSolicitudDocumento.size() > 0){
					accionweb.agregarObjeto("registra", 1);
				    accionweb.agregarObjeto("listaSolicitudDocumento", listaSolicitudDocumento); 
				    for (Cocow36DTO ss : listaSolicitudDocumento)
						total = total + ss.getValnu2();
		            accionweb.agregarObjeto("totalSolDocumento", total);
				}
				/*Item items = new Item();
				Collection<ItemDTO> lista = (Collection<ItemDTO>) items.todos_items();
				List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
				for(ItemDTO pp: lista){
					//System.out.println("incren: "+ pp.getIncren()+" "+ pp.getNomite()+"  "+ pp.getItepre());
					if(pp.getIncren().trim().equals("S"))
						listaItem.add(pp);
					
				}*/
				//	este acceso cambia a partir de 27/01/2015 pr instrucciones de correo PP accediendo a partir de ahora a la tabla cocof139 en lugar de cocof23
				List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
				listaItem = moduloRecuperacionGasto.getItemCocof139();
				accionweb.agregarObjeto("listaItem", listaItem);
				
				List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
			    listaTipoDocumento = moduloRecuperacionGasto.getTipoDocumento();
			    accionweb.agregarObjeto("listaTipoDocumento", listaTipoDocumento);
				if(tipo > 0)
				 accionweb.agregarObjeto("opcion", tipo);
				
			}
	 synchronized public void eliminaResultadoSolicitudDistribucion(AccionWeb accionweb) throws Exception {
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			
			   moduloRecuperacionGasto.getEliminaListaSolicitaDistribucion(accionweb.getReq());
			   	accionweb.agregarObjeto("mensaje", "Se elimin� la Unidad de a Distribuci�n.");
			    	
				List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
		  
				long total = 0;
				if (listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
					accionweb.agregarObjeto("registra", 1);
				    accionweb.agregarObjeto("listaSolicitudDistribucion", listaSolicitudDistribucion); 
				    for (Cocow36DTO ss : listaSolicitudDistribucion)
						total = total + ss.getValnu2();
		            accionweb.agregarObjeto("totalSolDistribucion", total);
				}
				
				if(tipo > 0)
				 accionweb.agregarObjeto("opcion", tipo);
				
			}
	 
	 synchronized public void registraRecuperacionGasto(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
			List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumento");

		    
			long rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
			String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
			String identificadorInterno = Util.validaParametro(accionweb.getParameter("identificadorInterno"),"");	
	
			long sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
			if(String.valueOf(rutnum).length() > 8 || identificadorInterno.trim().length() > 40)
				accionweb.agregarObjeto("mensaje", "Datos Inv�lidos");
			else {
			
			int numSolicitud = 0;
		    try {
		    numSolicitud = moduloRecuperacionGasto.getRegistraRecuperacion(accionweb.getReq(),rutUsuario,dv);
		    if(numSolicitud > 0){
		   	    accionweb.agregarObjeto("mensaje", "Se registr� en forma exitosa la solicitud de Recuperacion de gasto. Debe enviar documentos de respaldo a la Direcci�n de Finanzas haciendo referencia al n�mero de solicitud: "+ numSolicitud);
		   	    if (listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
					accionweb.agregarObjeto("registra", 1);
				    accionweb.getSesion().removeAttribute("listaSolicitudDistribucion");
				    if(listaSolicitudDocumento != null)
				    	accionweb.getSesion().removeAttribute("listaSolicitudDocumento");
				}
		   	 
		    } else
		    	accionweb.agregarObjeto("mensaje", "No se registr� la solicitud de recuperaci�n de gasto.");	
			}
		    catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado getRegistraRecuperacion.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
			   this.cargarRecuperacion(accionweb);
			}
		} 
	 synchronized public void actualizaRecuperacionGasto(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			List<Cocow36DTO>  listaSolicitudDistribucion = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDistribucion");
			List<Cocow36DTO>  listaSolicitudDocumento = (List<Cocow36DTO>) accionweb.getSesion().getAttribute("listaSolicitudDocumento");
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);

			boolean registra = false;
		    
		    try {
			registra = moduloRecuperacionGasto.getActualizaRecuperacion(accionweb.getReq(),rutUsuario,dv, numDoc);
		    if(registra){
		   	    accionweb.agregarObjeto("mensaje", "Se actualiz� en forma exitosa la solicitud de Recuperacion de gasto.");
		   	    if (listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
					accionweb.agregarObjeto("registra", 1);
				    accionweb.getSesion().removeAttribute("listaSolicitudDistribucion");
				    if(listaSolicitudDocumento != null)
				    	accionweb.getSesion().removeAttribute("listaSolicitudDocumento");
				}
		   	 Thread.sleep(500);
		   	 this.consultaRecuperacion(accionweb);
		    } else
		    	accionweb.agregarObjeto("mensaje", "No se modific� la solicitud de recuperaci�n de gasto.");	
		    } catch (Exception e) {
				accionweb.agregarObjeto("mensaje", "No guardado actualizaRecuperacionGasto.");
				accionweb.agregarObjeto("exception", e);
				e.printStackTrace();
			}
		} 
	 public void cargaAutorizaRecuperacion(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		
			
			PreswBean preswbean = null;
			Collection<Presw18DTO> listaRecuperacion = null;
			String titulo = "";
			String tituloDetalle1 = "";
			String tituloDetalle2 = "";
			String tipcue = "";
			//System.out.println("rutUsuario: "+rutUsuario);
			//System.out.println("dv: "+dv);
			switch (tipo) {
			case  2:// lista de vales para modificar
				{preswbean = new PreswBean("LMR",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				titulo = "Modificaci�n";
				break;
			}
			// case 3: case 6: case 8: case 9: case 10: // lista de vales para autorizar
			case 3: case 18: case 19: case 20: // lista de SOLICITUDES para autorizar
				{String nomtipo = "";
				if(tipo == 3 ){
					titulo = "Autorizaci�n Presupuestaria de Recuperaci�n de Gastos";
					nomtipo = "LRA";
					tituloDetalle1 = "Autorizar";
					tituloDetalle2 = "Rechazar";
					accionweb.agregarObjeto("opcion2", 8); // rechazar responsable de cuenta
					accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
					accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
				
				}
				if(tipo == 18){
					   nomtipo = "LRE";
					   titulo = "Autorizaci�n MECESUP VRA";
					   tipcue = "Y";
					   accionweb.agregarObjeto("opcion2", 21);
					}
				if(tipo == 19){
						nomtipo = "LRE";
						titulo = "Autorizaci�n UCP";
						tipcue = "X";
						accionweb.agregarObjeto("opcion2", 22);
					}
				if(tipo == 20){
						nomtipo = "LRE";
						titulo = "Autorizaci�n MECESUP DIPRES";
						tipcue = "Z";
						accionweb.agregarObjeto("opcion2", 23);
					}
				preswbean = new PreswBean(nomtipo,0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				preswbean.setTipcue(tipcue);
				
				tituloDetalle1 = "Autorizar";
				tituloDetalle2 = "Rechazar";
				accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
				accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
				break;
				}
				
			case 4: // lista de consulta de solicitudes de recuperacion de gastos
				{		
				int fechaInicio = Util.validaParametro(accionweb.getParameter("fechaInicio"),0);
				int fechaTermino = Util.validaParametro(accionweb.getParameter("fechaTermino"),0);
				String fechaIni = Util.validaParametro(accionweb.getParameter("fechaIni"),"");
				String fechaTer = Util.validaParametro(accionweb.getParameter("fechaTer"),"");
				int unidad = Util.validaParametro(accionweb.getParameter("unidad"),0);
				int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
				String dvrut = Util.validaParametro(accionweb.getParameter("dvrut"),"");
				
				if(fechaInicio > 0 && fechaTermino == 0 ){
					DateTool fechaActual = new DateTool();
					String dia = fechaActual.getDay()+"";
					if(dia.length() == 1)
						dia = "0" + dia;
					String mes = (fechaActual.getMonth()+1)+"";
					if(mes.length()== 1)
						mes = "0" + mes;
					fechaTermino = Integer.parseInt(String.valueOf(fechaActual.getYear()) + mes + dia);
					fechaTer = dia + "/" + mes + "/" + String.valueOf(fechaActual.getYear());
					
				}
				if(unidad >= 0){
				preswbean = new PreswBean("CRG",0,0,0,0, rutUsuario,0,"",0,dv, "",0,0,0,0,"","");
				preswbean.setCoduni(unidad);
				preswbean.setFecmov(fechaInicio);
				preswbean.setNumche(fechaTermino);
				preswbean.setRutide(rutnum);
				preswbean.setDigide(dvrut);
				preswbean.setNumcom(rutUsuario);
				preswbean.setTipcue(dv);
				
				listaRecuperacion = (Collection<Presw18DTO>) preswbean.consulta_presw18();
				}
				titulo = "Consulta";	
				List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
				moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
				listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
				accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
			   	accionweb.agregarObjeto("unidad", unidad);
			   	accionweb.agregarObjeto("fechaIni", fechaIni);
			   	accionweb.agregarObjeto("fechaTer", fechaTer);
			   	accionweb.agregarObjeto("rutnum", rutnum);
			   	accionweb.agregarObjeto("dvrut", dvrut);
	
				break;
				}
			case 5: // lista de solicitudes para revisi�n de documentaci�n
				{
			    int sede = Util.validaParametro(accionweb.getParameter("sede"), -1);
			    if(sede >= 0){
			    	preswbean = new PreswBean("FLR",0,0,0,0, rutUsuario,0,"",sede,dv, "",0,0,0,0,"","");
			    	listaRecuperacion = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			    }
				titulo = "Revisi�n de Documentaci�n en Finanzas";
				tipo = 5;// para que cambie opcion en caso de que sea 7
				List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
				listSede = moduloValePago.getListaSede(accionweb.getReq());
				accionweb.agregarObjeto("listSede", listSede);
				accionweb.agregarObjeto("sedeOpc", sede);
				break;
				}
			case 6: case 9: // lista de solicitudes para recepci�n en finanzas
			{
		    int sede = Util.validaParametro(accionweb.getParameter("sede"), -1);
		    if(sede >= 0){
		    	preswbean = new PreswBean("LDR",0,0,0,0, rutUsuario,0,"",sede,dv, "",0,0,0,0,"","");
		    	listaRecuperacion = (Collection<Presw18DTO>) preswbean.consulta_presw18();
		    }
			titulo = "Recepci�n en Finanzas";
			tituloDetalle1 = "Autorizar";
			tituloDetalle2 = "Rechazar";
			accionweb.agregarObjeto("opcion2", 9); // rechazar en DF
			accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
			accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);
            tipo = 6;
            List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
			listSede = moduloValePago.getListaSede(accionweb.getReq());
			accionweb.agregarObjeto("listSede", listSede);
			accionweb.agregarObjeto("sedeOpc", sede);
			break;
			}
			
		
			}
			if(tipo != 4 && tipo != 12 && tipo != 5 && tipo != 6 && tipo != 9)
				listaRecuperacion = (Collection<Presw18DTO>) preswbean.consulta_presw18();
			
			
			//System.out.println("listaValePago.size(): "+listaValePago.size());
		    if(listaRecuperacion != null && listaRecuperacion.size() > 0)
	        	accionweb.agregarObjeto("hayDatoslista", "1");
		    
		    
	        accionweb.agregarObjeto("listaRecuperacion", listaRecuperacion);
	      	accionweb.agregarObjeto("esRecuperacionGasto", "1");
	      	accionweb.agregarObjeto("opcion", String.valueOf(tipo));
	    	accionweb.agregarObjeto("opcionMenu", tipo);
	      	accionweb.agregarObjeto("titulo", titulo);
	      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
	      	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
		}
	 
	 public void consultaRecuperacion(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			int unidad = Util.validaParametro(accionweb.getParameter("unidad"), 0);
			String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
			long sede = Util.validaParametro(accionweb.getParameter("sede"), 0);
			//String estadoVales = Util.validaParametro(accionweb.getParameter("estadoVales"),"");
			String nomIdentificador = "";
			long rutnum = 0;
			String dvrut = "";
			//System.out.println("numDoc: "+numDoc);
			if(sede > 0)
				accionweb.agregarObjeto("sedeSolicitud", sede);
			//if(!estadoVales.trim().equals(""))
			//	accionweb.agregarObjeto("estadoVales", estadoVales);
			Presw19DTO preswbean19DTO = new Presw19DTO();
			String titulo = "";
			String tituloDetalle1 = "";
			preswbean19DTO.setTippro("ODR");
			preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
			preswbean19DTO.setDigide(dv);
			preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
		//	preswbean19DTO.setIdsoli("9999999");
		
			String readonly = "readonly";
			

			
	/*capturar los datos */
			switch (tipo) {
			case  2: // lista de solicitudes de recuperaci�n para consultar
			{	titulo = "Modifica ";
				tituloDetalle1 = "Modificar";
				readonly = "";
				break;
				}
			case 3: // lista de solicitudes de recuperaci�n para autorizar
				{
				titulo = "Autorizaci�n Responsable de la Cuenta";
				tituloDetalle1 = "Autorizar";	
				break;
				}
			case 4: // lista de solicitudes de recuperaci�n para consulta
				{
				titulo = "Consulta ";
				tituloDetalle1 = "Consultar";
				break;
				}
			case 5: // lista de solicitudes de recuperaci�n para Recepci�n de solicitudes de recuperaci�n en Finanzas
				{
				titulo = "Revisi�n de Documentaci�n";
				tituloDetalle1 = "Recepci�n";
				break;
				}
			case 6: // lista de solicitudes recepci�n en Finanzas
			{
			titulo = "Recepci�n en Finanzas";
			break;
			}
			case 7: // Documento Rechazado
			{
			titulo = "Documento Rechazado";
			break;
			}
			
			}
			List<Cocow36DTO>  listaSolicitudDocumento = new ArrayList<Cocow36DTO>();
			List<Cocow36DTO>  listaSolicitudDistribucion = new ArrayList<Cocow36DTO>();
			List<Cocow36DTO>  listaHistorial = new ArrayList<Cocow36DTO>();
			long valorAPagar = 0;
			String identificadorInterno = "";	
			sede = 0;
			int identificador = 0; 
			
		    String nomcam = "";
		    long valnu1 = 0;
		    long valnu2 = 0;
		    String valalf = "";
		    String glosa1 = "";
		    String glosa2 = "";
		    String glosa3 = "";
		    String glosa4 = "";
		    String glosa5 = "";
		    String glosa6 = "";
		    String tipoPago = "";
		    String nombrePago = "";
		    String nomSede = "";
		    int ind = 0;
		    String estadoFinal = "";
		    long fecha = 0;
		    long totalSolDistribucion = 0;
		    long totalSolDocumento = 0;
		  	List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
			CocowBean cocowBean = new CocowBean();
			//System.out.println("preswbean19DTO : "+ preswbean19DTO.getRutide()+"  - "+preswbean19DTO.getNumdoc()+"--- "+preswbean19DTO.getTippro()+"  --- "+preswbean19DTO.getFecmov());
			listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
			//System.out.println("listCocow36DTO : "+ listCocow36DTO.size() );
		    if(listCocow36DTO != null && listCocow36DTO.size() >0 ){
		    
		    for(Cocow36DTO ss: listCocow36DTO){
		   	
		    	if(ss.getNomcam().trim().equals("IDMEMO"))
	            	identificadorInterno = ss.getValalf();
	         	if(ss.getNomcam().trim().equals("CODSUC")){
	            	sede = ss.getValnu1();
	            	nomSede = ss.getValalf();
	            }
	            if(ss.getNomcam().trim().equals("RUTIDE") && tipo != 11)
	             	   rutnum = ss.getValnu1();
	            if(ss.getNomcam().trim().equals("DIGIDE") && tipo != 11)
	            	dvrut = ss.getValalf();
	            if(ss.getNomcam().trim().equals("NOMPAG") && tipo != 11)
	                nombrePago = ss.getValalf();
	 		   
		    	if(ss.getNomcam().trim().equals("DESCR1"))
		    		glosa1 = ss.getValalf();
		    	if(ss.getNomcam().trim().equals("DESCR2"))
		    		glosa2 = ss.getValalf();
		     	if(ss.getNomcam().trim().equals("DESCR3"))
		    		glosa3 = ss.getValalf();
		     	if(ss.getNomcam().trim().equals("DESCR4"))
		    		glosa4 = ss.getValalf();
		     	if(ss.getNomcam().trim().equals("DESCR5"))
		    		glosa5 = ss.getValalf();   
		     	if(ss.getNomcam().trim().equals("DESCR6"))
		    		glosa6 = ss.getValalf();
	            
	            if(ss.getNomcam().trim().equals("VALREC"))
	            	valorAPagar = ss.getValnu1();
	            if(ss.getNomcam().trim().equals("ESTADO"))
	            	estadoFinal = ss.getValalf();
	            
	            if(ss.getNomcam().trim().equals("DOCASO")){
	            	Cocow36DTO cocow36DTO = new Cocow36DTO();
	            	cocow36DTO.setNomcam(ss.getNomcam());
	            	cocow36DTO.setValnu1(ss.getValnu1());
	            	cocow36DTO.setValnu2(ss.getValnu2());
	            	cocow36DTO.setResval(ss.getResval()); // rut + dig + nombre
	            	cocow36DTO.setValalf(ss.getValalf());  // tipo + rubro + glosa
	            	listaSolicitudDocumento.add(cocow36DTO);
	            	totalSolDocumento += ss.getValnu2();
	           
	            }
	            
	            if(ss.getNomcam().trim().equals("CUENTA")){
	            	Cocow36DTO cocow36DTO = new Cocow36DTO();
	            	cocow36DTO.setNomcam(ss.getNomcam());
	            	cocow36DTO.setValnu1(ss.getValnu1());
	            	cocow36DTO.setValnu2(ss.getValnu2());
	            	cocow36DTO.setResval(ss.getResval());
	            	cocow36DTO.setValalf(ss.getValalf());
	            	listaSolicitudDistribucion.add(cocow36DTO);
	            	totalSolDistribucion +=  ss.getValnu2();
	            }
	         int i = 0;
	            if(ss.getNomcam().trim().equals("HISTORIAL")){
	            	i++;
	            	if(i==1)
	            		fecha = ss.getValnu1();
	            	Cocow36DTO cocow36DTO = new Cocow36DTO();
	            	cocow36DTO.setValnu1(ss.getValnu1()); // es la fecha, 
	            	cocow36DTO.setValalf(ss.getValalf());//nombre del funcionario
	            	cocow36DTO.setResval(ss.getResval()); // comentario
	              	listaHistorial.add(cocow36DTO);
	            }
	       
		    //	 }
		    }
		    if(tipo == 7 && !estadoFinal.trim().equals("REC.REV.FINANZA") && !estadoFinal.trim().equals("RECHAZADO UCP"))
				   accionweb.agregarObjeto("mensaje", "Esta Solicitud no ha sido Rechazada en la Revisi�n de Documentos." );
		    else {
				    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
				    listSede = moduloValePago.getListaSede(accionweb.getReq());
				    String comboSede = "<select name=\"sede\" id=\"sede\" class=\"estilo_Azul\" onChange=\"\" >"+
				                       "<option value=\"-1\" selected=\"selected\">Seleccione</option> ";
				  //  if(identificador == 1)
				    for (Presw18DTO ss : listSede){
				    	comboSede += "<option value=" + ss.getNummes() + ">" + ss.getDesuni() + "</option>";
				    	
				    }
				    comboSede += "</select>";
				   
				    List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
				    moduloValePago.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);
				    listaUnidades = (List<Presw18DTO> )accionweb.getSesion().getAttribute("cuentasPresupuestarias");
				    accionweb.agregarObjeto("listaUnidades", listaUnidades);
				    
				    List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
				    listaTipoDocumento = moduloRecuperacionGasto.getTipoDocumento();
				    accionweb.agregarObjeto("listaTipoDocumento", listaTipoDocumento);
				    
					/*Item items = new Item();
					Collection<ItemDTO> lista = (Collection<ItemDTO>) items.todos_items();
					List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
					for(ItemDTO pp: lista){
						//System.out.println("incren: "+ pp.getIncren()+" "+ pp.getNomite()+"  "+ pp.getItepre());
						if(pp.getIncren().trim().equals("S"))
							listaItem.add(pp);
						
					}
					accionweb.agregarObjeto("listaItem", listaItem);*/
					//	este acceso cambia a partir de 27/01/2015 pr instrucciones de correo PP accediendo a partir de ahora a la tabla cocof139 en lugar de cocof23
					List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
					listaItem = moduloRecuperacionGasto.getItemCocof139();
					accionweb.agregarObjeto("listaItem", listaItem);
							
				    accionweb.agregarObjeto("listSede", listSede);   
				 
				  
				    accionweb.agregarObjeto("estado",estado);
				    accionweb.agregarObjeto("unidad",unidad);
				     
				    //accionweb.agregarObjeto("numVale",String.valueOf(numDoc));
				    if(rutnum == 0)
				    	 accionweb.agregarObjeto("identificador","1");
				    else
				    	accionweb.agregarObjeto("identificador","2");
				    accionweb.agregarObjeto("identificadorInterno", identificadorInterno);
				    accionweb.agregarObjeto("nomIdentificador",nomIdentificador);
				    accionweb.agregarObjeto("sede", sede);
				    accionweb.agregarObjeto("nomSede", nomSede);
				    accionweb.agregarObjeto("rutnum",rutnum);// rut a pago
				    accionweb.agregarObjeto("dvrut",dvrut);
				    accionweb.agregarObjeto("nombrePago", nombrePago);
				    accionweb.agregarObjeto("glosa1",glosa1);
			    	accionweb.agregarObjeto("glosa2",glosa2);
			    	accionweb.agregarObjeto("glosa3",glosa3);
			    	accionweb.agregarObjeto("glosa4",glosa4);
			    	accionweb.agregarObjeto("glosa5",glosa5);
			    	accionweb.agregarObjeto("glosa6",glosa6);	   
				    accionweb.agregarObjeto("valorAPagar", valorAPagar);
				    accionweb.agregarObjeto("estadoFinal",estadoFinal);	
				    accionweb.agregarObjeto("fechaSolicitud", fecha);
				    accionweb.agregarObjeto("totalSolDistribucion", totalSolDistribucion);
				    accionweb.agregarObjeto("totalSolDocumento", totalSolDocumento);
				    accionweb.agregarObjeto("numDoc", numDoc);
					//System.out.println("listaUnidades: "+listaUnidades);
				    if(listaSolicitudDocumento != null && listaSolicitudDocumento.size() > 0){
				    	accionweb.agregarObjeto("listaSolicitudDocumento", listaSolicitudDocumento);
				    	accionweb.getSesion().setAttribute("listaSolicitudDocumento", listaSolicitudDocumento);
				    }
				    if(listaSolicitudDistribucion != null && listaSolicitudDistribucion.size() > 0){
				    	accionweb.agregarObjeto("listaSolicitudDistribucion", listaSolicitudDistribucion);
				       	accionweb.getSesion().setAttribute("listaSolicitudDistribucion", listaSolicitudDistribucion);
				    }
				    if(listaHistorial != null && listaHistorial.size() > 0){
				    	accionweb.agregarObjeto("listaHistorial", listaHistorial);
				       	accionweb.getSesion().setAttribute("listaHistorial", listaHistorial);
				    }
				   	accionweb.agregarObjeto("hayDatoslista", "1");     
			      	if(tipo != 7)
			      		accionweb.agregarObjeto("read",readonly);
		    }
		
		    } else { 
		    	accionweb.agregarObjeto("mensaje", "No existe solicitud "+titulo+"con el n�mero " + numDoc);
		    	if(tipo == 4){
		        accionweb.agregarObjeto("pagina", "recMuestraLista.vm");   
		       // tipo = 4;
		        this.cargaAutorizaRecuperacion(accionweb);
		    	}
		    }
				    	
		   	    
		    accionweb.agregarObjeto("esRecuperacionGasto", "1");
	      	accionweb.agregarObjeto("opcion", tipo);
	      	accionweb.agregarObjeto("titulo", titulo);
	      	accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
	
	    	if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
	      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
	      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
	      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
	      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
	      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
	      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
				accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
	     // 	System.out.println("termina consulta ");	
		}
	 
	 
	 
	 
	 public void consultaRechazada(AccionWeb accionweb) throws Exception {
		 this.consultaRecuperacion(accionweb);
		 //if(accionweb.getParameter("estadoFinal"))
		     
		}
	 public void imprimeConsulta(AccionWeb accionweb) throws Exception {
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
			int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		
			long valorAPagar = 0;
			long rutnum = 0;
			String dvrut = "";
			String identificadorInterno = "";	
			long sede = 0;
			int identificador = 0; 
			
		    String nomcam = "";
		    long valnu1 = 0;
		    long valnu2 = 0;
		    String valalf = "";
		    String glosa1 = "";
		    String glosa2 = "";
		    String glosa3 = "";
		    String glosa4 = "";
		    String glosa5 = "";
		    String glosa6 = "";
		    String estadoFinal = "";
		    String nomSede = "";
		    String nombrePago = "";
		    String tipoDocumento = "";
		    String nomRubro = "";
		    
		    List<Presw18DTO> listaTipoDocumento = new ArrayList<Presw18DTO>();
		    listaTipoDocumento = moduloRecuperacionGasto.getTipoDocumento();
		    
			/*Item items = new Item();
			Collection<ItemDTO> lista = (Collection<ItemDTO>) items.todos_items();
			List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
			for(ItemDTO pp: lista){
				//System.out.println("incren: "+ pp.getIncren()+" "+ pp.getNomite()+"  "+ pp.getItepre());
				if(pp.getIncren().trim().equals("S"))
					listaItem.add(pp);
				
			}*/
			//	este acceso cambia a partir de 27/01/2015 pr instrucciones de correo PP accediendo a partir de ahora a la tabla cocof139 en lugar de cocof23
			List<ItemDTO> listaItem = new ArrayList<ItemDTO>();
			listaItem = moduloRecuperacionGasto.getItemCocof139();
			accionweb.agregarObjeto("listaItem", listaItem);
		    
		    
			
			Presw19DTO preswbean19DTO = new Presw19DTO();
			String titulo = "";
			String tituloDetalle1 = "";
			preswbean19DTO.setTippro("ODR");
			preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
			preswbean19DTO.setDigide(dv);
			preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
			
			Vector vecSolicitudDocumento = new Vector();	
			Vector vecSolicitudDistribucion = new Vector();	
			Vector vecHistorial = new Vector();	
			List<Cocow36DTO>  listCocow36DTO = new ArrayList<Cocow36DTO>();
			CocowBean cocowBean = new CocowBean();
			listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
			 if(listCocow36DTO != null && listCocow36DTO.size() >0 ) {
				   for(Cocow36DTO ss: listCocow36DTO){
					    if(ss.getNomcam().trim().equals("IDMEMO"))
			            	identificadorInterno = ss.getValalf();
			         	if(ss.getNomcam().trim().equals("CODSUC")){
			            	sede = ss.getValnu1();
			            	nomSede = ss.getValalf();
			            }
			            if(ss.getNomcam().trim().equals("RUTIDE") )
			             	   rutnum = ss.getValnu1();
			            if(ss.getNomcam().trim().equals("DIGIDE") )
			            	dvrut = ss.getValalf();
			            if(ss.getNomcam().trim().equals("NOMPAG") )
			                nombrePago = ss.getValalf();
			 		   
				    	if(ss.getNomcam().trim().equals("DESCR1"))
				    		glosa1 = ss.getValalf();
				    	if(ss.getNomcam().trim().equals("DESCR2"))
				    		glosa2 = ss.getValalf();
				     	if(ss.getNomcam().trim().equals("DESCR3"))
				    		glosa3 = ss.getValalf();
				     	if(ss.getNomcam().trim().equals("DESCR4"))
				    		glosa4 = ss.getValalf();
				     	if(ss.getNomcam().trim().equals("DESCR5"))
				    		glosa5 = ss.getValalf();   
				     	if(ss.getNomcam().trim().equals("DESCR6"))
				    		glosa6 = ss.getValalf();
			            
			            if(ss.getNomcam().trim().equals("VALREC"))
			            	valorAPagar = ss.getValnu1();
			            if(ss.getNomcam().trim().equals("ESTADO"))
			            	estadoFinal = ss.getValalf();
			            
			            if(ss.getNomcam().trim().equals("DOCASO")){
			            	for(Presw18DTO pp: listaTipoDocumento){
							    if(ss.getValalf().substring(0,3).equals(pp.getTipmov().trim()))	
							    	tipoDocumento = pp.getDesuni();
			            	}
			            	for(ItemDTO pp: listaItem){
			            		//System.out.println("ss.getValalf().substring(3,6): "+ss.getValalf().substring(3,6) +"   pp.getItepre(): "+ pp.getItepre());
							    if( Integer.parseInt(ss.getValalf().substring(3,6)) == Integer.parseInt(pp.getItepre()+""))	
							    	nomRubro = pp.getNomite();
			            	}
			            	Vector vec = new Vector();
			            	vec.addElement(ss.getResval().substring(9).trim()); // numero de solicitud
			            	vec.addElement(tipoDocumento); // tipoDocumento
			               	vec.addElement(moduloValePago.formateoNumeroSinDecimales(Double.parseDouble(ss.getResval().substring(0, 8))) + "-" + ss.getResval().substring(8,9)); // rut del documento
			               	vec.addElement(ss.getValnu1()); //iva
			               	vec.addElement(ss.getValnu2()); // total documento
			               	vec.addElement(nomRubro);  // nomRubro
			               	vec.addElement(ss.getValalf().substring(6).trim()); // glosa
			               	vecSolicitudDocumento.addElement(vec);
			            	
			            }
			            
			         		  
			            if(ss.getNomcam().trim().equals("CUENTA")){
			            	Vector vec = new Vector();
			            	vec.addElement(ss.getValnu1() + "-"+ ss.getValalf()); // cuenta
			            	vec.addElement(ss.getValnu2()); // valor
			               	vecSolicitudDistribucion.addElement(vec);
			            	
			            }
			 
				        
			            if(ss.getNomcam().trim().equals("HISTORIAL")){
			            	String fecha = "";
			            	String diaFecha = "";
			            	String mesFecha = "";
			            	String annoFecha = "";
			            	String valor = "";
			            	String valor2 = "";
			            	if(ss.getValnu1()> 0 ) {
			    				fecha = ss.getValnu1() + "";
			    				diaFecha = fecha.substring(6);
			    				mesFecha = fecha.substring(4,6);
			    				annoFecha = fecha.substring(0,4);
			    				valor = diaFecha+"/"+mesFecha+"/"+annoFecha;   
			            	} else {
			    				valor = ss.getValnu1()+"";
			            	} 
			            	if (ss.getResval().trim().equals("AUTORIZACION"))// se ocupa de la 1-15 el resto se ocupa la observacion del rechazo
			            	   valor +=  " por " + ss.getValalf() + " Cuenta " + ss.getValnu2() ;
			            	else
			            		valor += " por " + ss.getValalf() + " " + ss.getResval().substring(15);
			            	
			            	Vector vec3 = new Vector();
			            	vec3.addElement(ss.getResval().substring(0, 15)); // es la accion, INGRESO, MODIFICACION, AUTORIZACION, PAGO, AUTORIZA FINANZA				        
			            	vec3.addElement(valor); // es la fecha, 
			            	vecHistorial.addElement(vec3);
			            }
			            if(ss.getNomcam().trim().equals("ESTADO"))
			            	estadoFinal = ss.getValalf();
				    // }
				    }	
				    List<Presw18DTO> listSede = new ArrayList<Presw18DTO>();
				    listSede = moduloValePago.getListaSede(accionweb.getReq());
				    for( Presw18DTO ss:listSede){
				    	if(ss.getNummes() == sede)
				    		nomSede = ss.getDesuni();
				    }
				 
	               accionweb.getSesion().setAttribute("vecSolicitudDocumento", vecSolicitudDocumento);
	               accionweb.getSesion().setAttribute("vecSolicitudDistribucion", vecSolicitudDistribucion);
	               accionweb.getSesion().setAttribute("vecHistorial", vecHistorial);
	               accionweb.getSesion().setAttribute("nombrePago", nombrePago);
	               accionweb.getSesion().setAttribute("glosa1", glosa1);
	               accionweb.getSesion().setAttribute("glosa2", glosa2);
	               accionweb.getSesion().setAttribute("glosa3", glosa3);
	               accionweb.getSesion().setAttribute("glosa4", glosa4);
	               accionweb.getSesion().setAttribute("glosa5", glosa5);
	               accionweb.getSesion().setAttribute("glosa6", glosa6);
	               if(rutnum > 0)
	            	   accionweb.getSesion().setAttribute("rutnum", moduloValePago.formateoNumeroSinDecimales(Double.parseDouble(rutnum+"")) + "-" + dvrut);
	               accionweb.getSesion().setAttribute("nomSede", nomSede);
	               accionweb.getSesion().setAttribute("valorAPagar", moduloValePago.formateoNumeroSinDecimales(Double.parseDouble(valorAPagar+"")));
	               accionweb.getSesion().setAttribute("identificadorInterno", identificadorInterno);
	               accionweb.getSesion().setAttribute("estadoFinal", estadoFinal);
	               accionweb.getSesion().setAttribute("numDoc", String.valueOf(numDoc));
			 }
		}
		public void limpiaSimulador(AccionWeb accionweb) throws Exception {
			if (accionweb.getSesion().getAttribute("listaUnidad") != null)
				accionweb.getSesion().setAttribute("listaUnidad", null);
			int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
			int rutOrigen  = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutOrigen") +"","0"));
			String nomSimulacion = "";
			if(accionweb.getSesion().getAttribute("opcionMenu") != null)
				accionweb.getSesion().removeAttribute("opcionMenu");
			
			/*hacer este rut rutUsuario*/
			if(rutOrigen != 0) // lo vuelve a lo original
			{
				rutUsuario = rutOrigen ;
				rutOrigen = 0;
			}
			 
			
			accionweb.getSesion().setAttribute("rutUsuario", rutUsuario);
			accionweb.getSesion().setAttribute("rutOrigen", rutOrigen);
			accionweb.getSesion().setAttribute("nomSimulacion", nomSimulacion);
			accionweb.agregarObjeto("rutOrigen", rutOrigen);
			accionweb.agregarObjeto("nomSimulacion", nomSimulacion);
			
		}
		
		 synchronized public void ejecutaAccion(AccionWeb accionweb) throws Exception {
				int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
				int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
				String estadoVales = Util.validaParametro(accionweb.getParameter("estadoVales"),"");
				String estado = Util.validaParametro(accionweb.getParameter("estado"),"");
				
				switch (tipo){
				case 2: {// actualiza
					this.actualizaRecuperacionGasto(accionweb);
					break;
				}
				case 3: case 5: case 6: case 18: case 19: case 20:// autoriza responsable de cuenta, revision de documento, UCP, DIPRES, MECESUP
					{
					this.autorizaRecuperacion(accionweb);
					break;
				}
				case 8: case 9: case 21: case 22: case 23:{// rechaza Recepci�n
					this.rechazaAutSolicitud(accionweb);
				break;
					
				}
			
				}
				if(!estadoVales.trim().equals(""))
					accionweb.agregarObjeto("estadoVales", estadoVales);
				if(!estado.trim().equals(""))
					accionweb.agregarObjeto("estado", estado);
				if(tipo != 11)
				         this.cargarRecuperacion(accionweb);
				if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
		      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
		      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
		      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
		      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
		      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
		      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
					accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
			}	
		 synchronized public void rechazaAutSolicitud(AccionWeb accionweb) throws Exception {
				int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
				int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);	
				String glosa = Util.validaParametro(accionweb.getParameter("motivoRechazo")+ "","");
				int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
				String texto = "";
				String tipCue = "";
				
				String nomTipo = "";
				switch(tipo){
				case 3: case 8: {// rechazo responsable de cuenta
						nomTipo = "RRR";
						texto = "de la Autorizaci�n";
						break;
						}
				case 5: { // rechazo revisi�n documentaci�n
					nomTipo = "RSR";
					texto = "de la Revisi�n de Documentaci�n";
					break;
					}
				case 6: case 9: {// rechazo recepcion finanzas
					nomTipo = "RSS";
					texto = "de la Recepci�n";
					break;
					}
				case 18: case 21: { // rechazo mecesup
					nomTipo = "RER";
					tipCue = "Y";
					texto = "de la Autorizaci�n de MECESUP ";
					break;
					}
				case 19: case 22: { // rechazo  UCP
					nomTipo = "RER";
					texto = "de la Autorizaci�n de UCP";
					tipCue = "X";
					break;
					}
				case 20: case 23: { // rechazo DIPRES
					nomTipo = "RER";
					texto = "de la Autorizaci�n de DIPRES";
					tipCue = "Z";
					break;
					}
				
				

				}
				try { 
				boolean graba = moduloRecuperacionGasto.saveRechaza(numdoc, rutUsuario,dv,nomTipo, glosa, tipCue);
				
			    if(graba)
			    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa el rechazo " +  texto);
			    else
			    	accionweb.agregarObjeto("mensaje", "No Grab� el Rechazo " + texto);
			    
				Thread.sleep(500);
				if(tipo != 22)
			          this.cargaAutorizaRecuperacion(accionweb);
			    
			    
				} catch (Exception e) {
					accionweb.agregarObjeto("mensaje", "No guardado rechazaAutSolicitud.");
					accionweb.agregarObjeto("exception", e);
					e.printStackTrace();
				}
				if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
		      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
		      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
		      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
		      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
		      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
		      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
					accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
				
			}
		 synchronized public void autorizaRecuperacion(AccionWeb accionweb) throws Exception {
				int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
				int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
				int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
				String nomTipo = "";
				String tipCue = "";
				String nomAut = "";
				String texto = "";
				switch(tipo){
				case 3: {   // autorizaci�n responsable de cuenta
					nomTipo = "ARR";
					nomAut = " de Responsable de Cuenta. ";
					texto = " la autorizaci�n ";
					break;
				}
				case 5: {// autoricaci�n revisi�n documento
					nomTipo = "ASF";
					nomAut = " Revisi�n de Documento. ";
					break;
				}
				case 6: {
					nomTipo = "ASR";
					nomAut = " Recepci�n en Finanzas. ";
					break;
				}
				case 18: { // autoriza MECESUP
					
					nomTipo = "AER";
					tipCue = "Y";
					nomAut = " de MECESUP VRA. ";
					texto = " la autorizaci�n ";
					break;
				}
				case 19: {// AUTORIZA UCP
					nomTipo = "AER";
					tipCue = "X";
					nomAut = " de UCP. ";
					texto = " la autorizaci�n ";
					break;
				}
				case 20: { // AUTORIZA DIPRES
					nomTipo = "AER";
					tipCue = "Z";
					nomAut = " de MECESUP DIPRES. ";
					texto = " la autorizaci�n ";
					break;
				}
				}
				try {	 
				boolean graba = moduloRecuperacionGasto.saveAutoriza(numdoc, rutUsuario,dv,nomTipo, tipCue);
			    if(graba)
			    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa " + texto + nomAut);
			    else
			    	accionweb.agregarObjeto("mensaje", "Problemas en la grabaci�n de autorizaci�n" + nomAut);
			    
				Thread.sleep(500);
			    this.cargaAutorizaRecuperacion(accionweb);
				} catch (Exception e) {
					accionweb.agregarObjeto("mensaje", "No guardado autoriza Solicitud de recuperaci�n de gasto.");
					accionweb.agregarObjeto("exception", e);
					e.printStackTrace();
				}
		
			    
				if(accionweb.getSesion().getAttribute("autorizaProyecto") != null)
		      		accionweb.agregarObjeto("autorizaProyecto", accionweb.getSesion().getAttribute("autorizaProyecto"));
		      	if(accionweb.getSesion().getAttribute("autorizaUCP") != null)
		      		accionweb.agregarObjeto("autorizaUCP", accionweb.getSesion().getAttribute("autorizaUCP"));
		      	if(accionweb.getSesion().getAttribute("autorizaDIRPRE") != null)
		      		accionweb.agregarObjeto("autorizaDIRPRE", accionweb.getSesion().getAttribute("autorizaDIRPRE"));
		      	if(accionweb.getSesion().getAttribute("autorizaFinanzas") != null)
					accionweb.agregarObjeto("autorizaFinanzas", accionweb.getSesion().getAttribute("autorizaFinanzas"));
			}	 
		 synchronized public void recepcionaSolicitud(AccionWeb accionweb) throws Exception {
				int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
				int numdoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
			

				try { 
				boolean graba = moduloValePago.saveRecepciona(numdoc, rutUsuario,dv,"REV");
			    if(graba)
			    	accionweb.agregarObjeto("mensaje", "Se grab� en forma exitosa la Recepci�n.");
			    else
			    	accionweb.agregarObjeto("mensaje", "No se Grab� la Recepci�n.");
				Thread.sleep(500);
			    this.cargaAutorizaRecuperacion(accionweb);
				} catch (Exception e) {
					accionweb.agregarObjeto("mensaje", "No guardado recepcionaSolicitud.");
					accionweb.agregarObjeto("exception", e);
					e.printStackTrace();
				}
			
				
			}
		 
		 
		 
		 synchronized public void recepcionaMasivo(AccionWeb accionweb) throws Exception {
				int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb.getSesion().getAttribute("rutUsuario")+ "","0"));
				String dv = Util.validaParametro(accionweb.getSesion().getAttribute("dv")+ "","");
				String linea = Util.validaParametro(accionweb.getParameter("linea"),"");
				int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
				int numdoc = 0;
				String nomTipo = "";
				String texto = "";
				switch(tipo){
				case 3:{// autoriza responsable de cuenta
					nomTipo = "ARR";
					texto = "Autorizaci&oacute;n";
					break;
				}
				case 6:{// recepci�n en finanzas
					nomTipo = "ASR";
					texto = "Recepci&oacute;n";
					break;
				}
				}
				try { 
					int index = 1;
					boolean graba = false;
					String mensaje = "";
					while(linea.length() > 0){
						index = linea.indexOf("@");
						if(index > 0)
							numdoc = Integer.parseInt(linea.substring(0,index));
						else{
							numdoc = Integer.parseInt(linea);
							linea = "";
						}
							graba = moduloRecuperacionGasto.saveAutoriza(numdoc, rutUsuario,dv,nomTipo, "");
						    if(graba)
						    	mensaje += "Se grab&oacute; en forma exitosa la "+texto+" del Vale N " + numdoc + "<br>";
						    else
						    	mensaje += "No se Grab� la " + texto + " del Vale N " + numdoc;
						    linea = linea.substring(index+1);
						
					}
					Thread.sleep(500);
				   	accionweb.agregarObjeto("mensaje", mensaje);
					this.cargaAutorizaRecuperacion(accionweb);
					} catch (Exception e) {
					accionweb.agregarObjeto("mensaje", "No guardado recepcionaMasivo.");
					accionweb.agregarObjeto("exception", e);
					e.printStackTrace();
				}
				
				
			} 
		 
		
		 
		 
	public ModuloRecuperacionGasto getModuloRecuperacionGasto() {
		return moduloRecuperacionGasto;
	}

	public void setModuloRecuperacionGasto(
			ModuloRecuperacionGasto moduloRecuperacionGasto) {
		this.moduloRecuperacionGasto = moduloRecuperacionGasto;
	}
	
		public ModuloValePago getModuloValePago() {
		return moduloValePago;
	}

	public void setModuloValePago(ModuloValePago moduloValePago) {
		this.moduloValePago = moduloValePago;
	}



}
