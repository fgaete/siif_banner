package cl.utfsm.sriB.POJO;



import java.io.Serializable;
import java.math.BigDecimal;







public class movimientoPOJO
  implements Serializable
{
  private String tipo;
  private String cuenta;
  private String detalle_cuenta;
  private String orgn;
  private long cargo;
  private long abono;
	private long saldo;
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getOrgn() {
		return orgn;
	}
	public void setOrgn(String orgn) {
		this.orgn = orgn;
	}
	public long getCargo() {
		return cargo;
	}
	public void setCargo(long cargo) {
		this.cargo = cargo;
	}
	public long getAbono() {
		return abono;
	}
	public void setAbono(long abono) {
		this.abono = abono;
	}
	public long getSaldo() {
		return saldo;
	}
	public void setSaldo(long saldo) {
		this.saldo = saldo;
	}
	public String getDetalle_cuenta() {
		return detalle_cuenta;
	}
	public void setDetalle_cuenta(String detalle_cuenta) {
		this.detalle_cuenta = detalle_cuenta;
	}
	
 
  }
  
 