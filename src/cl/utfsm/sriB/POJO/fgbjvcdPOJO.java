package cl.utfsm.sriB.POJO;

import java.io.Serializable;

public class fgbjvcdPOJO implements Serializable{
	
	private int FGBJVCD_SEQ_NUM;
	private String FGBJVCD_RUCL_CODE;
	private String FGBJVCD_COAS_CODE;
	private String FGBJVCD_ACCI_CODE;
	private String FGBJVCD_FUND_CODE;
	private String FGBJVCD_ORGN_CODE;
	private String FGBJVCD_ACCT_CODE;
	private String FGBJVCD_PROG_CODE;
	private long FGBJVCD_TRANS_AMT;
	private String FGBJVCD_DR_CR_IND;
	private String FGBJVCD_TRANS_DESC;
	private String FGBJVCD_DOC_REF_NUM;
	private String  FGBJVCD_BUDGET_PERIOD;
	public int getFGBJVCD_SEQ_NUM() {
		return FGBJVCD_SEQ_NUM;
	}
	public void setFGBJVCD_SEQ_NUM(int fgbjvcd_seq_num) {
		FGBJVCD_SEQ_NUM = fgbjvcd_seq_num;
	}
	public String getFGBJVCD_RUCL_CODE() {
		return FGBJVCD_RUCL_CODE;
	}
	public void setFGBJVCD_RUCL_CODE(String fgbjvcd_rucl_code) {
		FGBJVCD_RUCL_CODE = fgbjvcd_rucl_code;
	}
	public String getFGBJVCD_COAS_CODE() {
		return FGBJVCD_COAS_CODE;
	}
	public void setFGBJVCD_COAS_CODE(String fgbjvcd_coas_code) {
		FGBJVCD_COAS_CODE = fgbjvcd_coas_code;
	}
	public String getFGBJVCD_ACCI_CODE() {
		return FGBJVCD_ACCI_CODE;
	}
	public void setFGBJVCD_ACCI_CODE(String fgbjvcd_acci_code) {
		FGBJVCD_ACCI_CODE = fgbjvcd_acci_code;
	}
	public String getFGBJVCD_FUND_CODE() {
		return FGBJVCD_FUND_CODE;
	}
	public void setFGBJVCD_FUND_CODE(String fgbjvcd_fund_code) {
		FGBJVCD_FUND_CODE = fgbjvcd_fund_code;
	}
	public String getFGBJVCD_ORGN_CODE() {
		return FGBJVCD_ORGN_CODE;
	}
	public void setFGBJVCD_ORGN_CODE(String fgbjvcd_orgn_code) {
		FGBJVCD_ORGN_CODE = fgbjvcd_orgn_code;
	}
	public String getFGBJVCD_ACCT_CODE() {
		return FGBJVCD_ACCT_CODE;
	}
	public void setFGBJVCD_ACCT_CODE(String fgbjvcd_acct_code) {
		FGBJVCD_ACCT_CODE = fgbjvcd_acct_code;
	}
	public String getFGBJVCD_PROG_CODE() {
		return FGBJVCD_PROG_CODE;
	}
	public void setFGBJVCD_PROG_CODE(String fgbjvcd_prog_code) {
		FGBJVCD_PROG_CODE = fgbjvcd_prog_code;
	}
	public long getFGBJVCD_TRANS_AMT() {
		return FGBJVCD_TRANS_AMT;
	}
	public void setFGBJVCD_TRANS_AMT(long fgbjvcd_trans_amt) {
		FGBJVCD_TRANS_AMT = fgbjvcd_trans_amt;
	}
	public String getFGBJVCD_DR_CR_IND() {
		return FGBJVCD_DR_CR_IND;
	}
	public void setFGBJVCD_DR_CR_IND(String fgbjvcd_dr_cr_ind) {
		FGBJVCD_DR_CR_IND = fgbjvcd_dr_cr_ind;
	}
	public String getFGBJVCD_TRANS_DESC() {
		return FGBJVCD_TRANS_DESC;
	}
	public void setFGBJVCD_TRANS_DESC(String fgbjvcd_trans_desc) {
		FGBJVCD_TRANS_DESC = fgbjvcd_trans_desc;
	}
	public String getFGBJVCD_DOC_REF_NUM() {
		return FGBJVCD_DOC_REF_NUM;
	}
	public void setFGBJVCD_DOC_REF_NUM(String fgbjvcd_doc_ref_num) {
		FGBJVCD_DOC_REF_NUM = fgbjvcd_doc_ref_num;
	}
	public String getFGBJVCD_BUDGET_PERIOD() {
		return FGBJVCD_BUDGET_PERIOD;
	}
	public void setFGBJVCD_BUDGET_PERIOD(String fgbjvcd_budget_period) {
		FGBJVCD_BUDGET_PERIOD = fgbjvcd_budget_period;
	}
	
	
}
