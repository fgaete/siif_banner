package cl.utfsm.sriB.mvc;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import cl.utfsm.sipB.POJO.ItemPOJO;
import cl.utfsm.base.AccionWeb;
import cl.utfsm.base.util.Util;
import cl.utfsm.sipB.POJO.Pres18POJO;
import cl.utfsm.sipB.modulo.ModuloPresupuestoB;
import cl.utfsm.sivB.modulo.ModuloValePagoB;
import cl.utfsm.sriB.POJO.fgbjvcdPOJO;
import cl.utfsm.sriB.POJO.movimientoPOJO;
import cl.utfsm.sriB.modulo.ModuloReitemizacionB;
import descad.cliente.CocowBean;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw19DTO;


public class ReitemizacionInterceptorB extends HandlerInterceptorAdapter {
	private ModuloValePagoB moduloValePagoB;
	private ModuloReitemizacionB moduloReitemizacionB;

	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

	}
	
	public void limpiaSession(AccionWeb accionweb) throws Exception {
		if (accionweb.getSesion().getAttribute("listaOrganizacion") != null) {
			accionweb.getSesion().setAttribute("listaOrganizacion", null);
			accionweb.getSesion().removeAttribute("listaOrganizacion");
		}
		if (accionweb.getSesion().getAttribute("cuentasPresupuestarias") != null)
			accionweb.getSesion().removeAttribute("cuentasPresupuestarias");
		if (accionweb.getSesion().getAttribute("listaActualizadaSaldos") != null)
			accionweb.getSesion().removeAttribute("listaActualizadaSaldos");
		if (accionweb.getSesion().getAttribute("listaActualizada") != null)
			accionweb.getSesion().removeAttribute("listaActualizada");
		if (accionweb.getSesion().getAttribute("totales_cargo") != null)
			accionweb.getSesion().removeAttribute("totales_cargo");
		if (accionweb.getSesion().getAttribute("totales_abono") != null)
			accionweb.getSesion().removeAttribute("totales_abono");
	}

	public void cargarMenu(AccionWeb accionweb) throws Exception {

		limpiaSession(accionweb);

		String rutUsuarioaux = ""
				+ accionweb.getSesion().getAttribute("rutUsuario");
		int rutUsuario = Integer.parseInt(rutUsuarioaux);
		String dvrut = accionweb.getSesion().getAttribute("dv") + "";
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		accionweb.agregarObjeto("esReitemizacionB", 1);
		accionweb.agregarObjeto("opcionMenu", tipo);
		accionweb.agregarObjeto("titulo", "Ingreso");
		int rutnum = Util.validaParametro(accionweb.getParameter("rutnum"), 0);
		int rutOrigen = Integer.parseInt(Util.validaParametro(accionweb
				.getSesion().getAttribute("rutOrigen")
				+ "", "0"));

		// List<Presw18DTO> lista = new ArrayList<Presw18DTO>();

		moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutUsuario);

		List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
		listaUnidades = (List<Presw18DTO>) accionweb.getSesion().getAttribute(
				"cuentasPresupuestarias");
		accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
		accionweb.getSesion().setAttribute("listaReitemizacion_session", listaUnidades);
		accionweb.agregarObjeto("tipo", tipo);

		// Carga lista de item ingresados previamente
		PreswBean preswbean = null;
		Collection<Presw18DTO> listaReitemizacion = null;
		preswbean = new PreswBean("LSP", 0, 0, 0, 0, rutUsuario, 0, "", 0,
				dvrut, "", 0, 0, 0, 0, "", "");
		listaReitemizacion = (Collection<Presw18DTO>) preswbean
				.consulta_presw18();
		//System.out.println("tama�o lista " + listaReitemizacion.size());
		accionweb.agregarObjeto("listaReitemizacion", listaReitemizacion);

	}

	public void cargaAutorizaReitemizacion(AccionWeb accionweb)
			throws Exception {
		int rutUsuario = Integer.parseInt(Util.validaParametro(accionweb
				.getSesion().getAttribute("rutUsuario")
				+ "", "0"));
		String dv = Util.validaParametro(accionweb.getSesion().getAttribute(
				"dv")
				+ "", "");
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);

		PreswBean preswbean = null;
		Collection<Presw18DTO> listaReitemizacion = null;
		String titulo = "";
		String tituloDetalle1 = "";
		String tituloDetalle2 = "";
		String tipcue = "";
		String nombre = moduloValePagoB.getVerificaRut(rutUsuario + "" + dv);
		switch (tipo) {
		case 2:// lista de Reitemizacion para modificar
		{
			preswbean = new PreswBean("MSI", 0, 0, 0, 0, rutUsuario, 0, "", 0,
					dv, "", 0, 0, 0, 0, "", "");
			titulo = "Modificaci�n";
			accionweb.agregarObjeto("rut", rutUsuario + "-" + dv);
			accionweb.agregarObjeto("nomIdentificador", nombre);
			break;
		}

		case 3: // lista de Reitemizacion para consulta
		{

			preswbean = new PreswBean("LSP", 0, 0, 0, 0, rutUsuario, 0, "", 0,
					dv, "", 0, 0, 0, 0, "", "");
			// preswbean.setCoduni(unidad);

			listaReitemizacion = (Collection<Presw18DTO>) preswbean
					.consulta_presw18();

			titulo = "Consulta solicitudes reitemizacion";
			List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
			moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(),
					rutUsuario);
			listaUnidades = (List<Presw18DTO>) accionweb.getSesion()
					.getAttribute("cuentasPresupuestarias");
			accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
			accionweb.agregarObjeto("rut", rutUsuario + "-" + dv);

			accionweb.agregarObjeto("nomIdentificador", nombre);
			break;
		}
		case 4: // lista de Reitemizacion para autorizar responsable
		{
			String nomtipo = "";
			titulo = "Autorizaci�n Responsable de la Organizaci�n";
			nomtipo = "OLA";
			accionweb.agregarObjeto("opcion2", 6); // rechazar

			preswbean = new PreswBean(nomtipo, 0, 0, 0, 0, rutUsuario, 0, "",
					0, dv, "", 0, 0, 0, 0, "", "");

			listaReitemizacion = (Collection<Presw18DTO>) preswbean
					.consulta_presw18();

			tituloDetalle1 = "Autorizar";
			tituloDetalle2 = "Rechazar";
			break;
		}
		case 5: // lista de Reitemizacion para preaprobaci�n DIPRES
		{
			String nomtipo = "";
			titulo = "Preaprobaci�n Solicitudes Reitemizaci�n DIPRES";
			nomtipo = "LIA";
			preswbean = new PreswBean(nomtipo, 0, 0, 0, 0, rutUsuario, 0, "",
					0, dv, "", 0, 0, 0, 0, "", "");
			listaReitemizacion = (Collection<Presw18DTO>) preswbean
					.consulta_presw18();
			tituloDetalle1 = "Autorizar";
			// System.out.println("tama�o lista " + listaReitemizacion.size());
			break;
		}
		case 6: // lista de Reitemizacion para Aprobaci�n DIPRES
		{
			String nomtipo = "";
			titulo = "Aprobaci�n Solicitudes Reitemizaci�n DIPRES";
			nomtipo = "AIA";
			preswbean = new PreswBean(nomtipo, 0, 0, 0, 0, rutUsuario, 0, "",
					0, dv, "", 0, 0, 0, 0, "", "");
			tituloDetalle1 = "Autorizar";
			listaReitemizacion = (Collection<Presw18DTO>) preswbean
					.consulta_presw18();
			break;
		}
		}
		if (tipo == 2)
			listaReitemizacion = (Collection<Presw18DTO>) preswbean
					.consulta_presw18();

		// System.out.println("listaValePago.size(): "+listaValePago.size());
		if (listaReitemizacion != null && listaReitemizacion.size() > 0)
			accionweb.agregarObjeto("hayDatoslista", "1");

		accionweb.agregarObjeto("listaReitemizacion", listaReitemizacion);
		accionweb.agregarObjeto("esReitemizacionB", 1);
		accionweb.agregarObjeto("opcion", String.valueOf(tipo));
		accionweb.agregarObjeto("opcionMenu", tipo);
		accionweb.agregarObjeto("titulo", titulo);
		accionweb.agregarObjeto("tituloDetalle1", tituloDetalle1);
		accionweb.agregarObjeto("tituloDetalle2", tituloDetalle2);

	}
 /*
	public void copiaIngreso(AccionWeb accionweb) throws Exception {
		accionweb.agregarObjeto("esReitemizacionB", 1);
		String copia_seleccionada = "" + accionweb.getParameter("listaItem");
		int pos =  copia_seleccionada.indexOf("@");
		String id = copia_seleccionada.substring(0,pos).trim();
		String rutUsuarioaux = ""
				+ accionweb.getSesion().getAttribute("rutUsuario");
		int rutnum = Integer.parseInt(rutUsuarioaux);
		String dvrut = accionweb.getSesion().getAttribute("dv") + "";
		String nombre = "";
		if (rutnum > 0) {
			nombre = moduloValePagoB.getVerificaRut(rutnum + "" + dvrut);
			accionweb.agregarObjeto("rutnum", rutnum);
			accionweb.agregarObjeto("dvrut", dvrut);

		}
		if (!nombre.trim().equals("")) {
			accionweb.agregarObjeto("hayDatoslista", 1);
			accionweb.agregarObjeto("nomIdentificador", nombre);
			accionweb.getSesion().setAttribute("nomIdentificador", nombre);
		} else
			accionweb
					.agregarObjeto("mensaje",
							"Este RUT no est� registrado. Debe solicitar a FINANZAS su creaci�n.");
	try {
		

		String cuenta = "";
		String muestraSaldo = "";
		String glosa1 = "";
		String glosa2 = "";
		String glosa3 = "";
		String glosa4 = "";
		String glosa5 = "";
		String glosa6 = "";
		long total_saldo = 0;
		String estadoFinal = "";
		String codEstadoFinal = "";
		long totalSolicitudVale = 0;
		long totalSolicitudPago = 0;

		long saldoCuenta = 0;
		int permiteAut = 1;
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		CocowBean cocowBean = new CocowBean();
		String glosafinal = "";
		long fecha = 0;
		ModuloReitemizacionB moduloReitemizacionB = new ModuloReitemizacionB();

		if (!id.isEmpty()) {//iseries

			String organizacion = "";
			String nomIdentificador = "";
			Presw19DTO preswbean19DTO = new Presw19DTO();
			

			List<Presw18DTO> listaSolicitudesPago = new ArrayList<Presw18DTO>();
			List<movimientoPOJO> listaSolicitudes = new ArrayList<movimientoPOJO>();
			List<Presw18DTO> listaHistorial = new ArrayList<Presw18DTO>();
			preswbean19DTO.setTippro("GDI");
			preswbean19DTO.setRutide(new BigDecimal(rutnum));
			preswbean19DTO.setDigide(dvrut);
			preswbean19DTO.setNumdoc(new BigDecimal(id));
			listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
			List<ItemPOJO> listasaldos = new ArrayList<ItemPOJO>();
			String orgn = "" + accionweb.getParameter("cuenta");

			System.out.println("Tama�o  lista " + listCocow36DTO.size());
			if (listCocow36DTO != null && listCocow36DTO.size() > 0)

				for (Cocow36DTO ss : listCocow36DTO) {
					System.out.println("Nombre Iseries "
							+ ss.getNomcam().trim());
					if (ss.getNomcam().trim().equals("CODORG")) {
						organizacion = ss.getValalf().trim();

					}
					// Motivo Reitemizacion 
					
					if (ss.getNomcam().trim().equals("GLOSA1")
							|| ss.getNomcam().trim().equals("GLOSA2")
							|| ss.getNomcam().trim().equals("GLOSA3")
							|| ss.getNomcam().trim().equals("GLOSA4")
							|| ss.getNomcam().trim().equals("GLOSA5")
							|| ss.getNomcam().trim().equals("GLOSA6"))
						glosafinal = glosafinal + ss.getValalf();

					if (ss.getNomcam().trim().equals("TOTAL"))
						total_saldo = ss.getValnu1();

					if (ss.getNomcam().trim().equals("CUENTA")) { // detalle
						// de los
						// cargos-abono
						// while
						movimientoPOJO presw18DTO = new movimientoPOJO();
						//Listado cuenta Movimientos
						presw18DTO.setCuenta(ss.getValalf()); // $NOMBRE
						cuenta = ss.getValalf(); // CUENTA
						presw18DTO.setCargo(ss.getValnu1()); // $CARGO
						presw18DTO.setAbono(ss.getValnu2()); // $Abono
						if (presw18DTO.getCargo() > 0)
							presw18DTO.setTipo("Cargo");
						else if (presw18DTO.getAbono() > 0)
							presw18DTO.setTipo("Abono");

						if (ss.getValnu2() > saldoCuenta) {
							muestraSaldo = "<font class=\"Estilo_Rojo\" >"
									+ saldoCuenta + "</font>";
							permiteAut = 2;
						} else
							muestraSaldo = saldoCuenta + "";

						accionweb.agregarObjeto("permiteAut", permiteAut);
						long saldo = getBuscaSaldo(cuenta, listasaldos);
						Boolean permiteAuto = true;			
						accionweb.agregarObjeto("permiteAuto", permiteAuto);
						presw18DTO.setSaldo(saldo);

						listaSolicitudes.add(presw18DTO);
						totalSolicitudVale += ss.getValnu2();
					}
					if (ss.getNomcam().trim().equals("NOMBEN")) {
						Presw18DTO presw18DTO = new Presw18DTO();
						presw18DTO.setUsadom(ss.getValnu1());
						presw18DTO.setDesuni(ss.getValalf());
						presw18DTO.setRutide(Integer.parseInt(ss.getValnu2()
								+ "")); // rut
						presw18DTO.setIddigi(ss.getAccion()); // dv
						listaSolicitudesPago.add(presw18DTO);
						totalSolicitudPago += ss.getValnu1();
					}
					if (ss.getNomcam().trim().equals("HISTORIAL")) {
						Presw18DTO presw18DTO = new Presw18DTO();
						presw18DTO.setUsadom(ss.getValnu1()); // es la fecha,

						if (fecha == 0)
							fecha = ss.getValnu1();
						presw18DTO.setNompro(ss.getResval().substring(26));// en
						// caso
						// de
						// Resval="Autoriza"
						// es
						// la
						// unidad

						presw18DTO.setIndpro(ss.getResval().substring(15, 25));// comentario
						presw18DTO.setDesite(ss.getValalf()); // es el
						// responsable

						presw18DTO.setDesuni(ss.getResval().substring(0, 15)); // es
						// la
						// accion,
						// INGRESO,
						// MODIFICACION,
						// AUTORIZACION,
						// PAGO,
						// AUTORIZA
						// FINANZAS
						// //
						// Tipo
						// Movimeinto
						listaHistorial.add(presw18DTO);
					}
					if (ss.getNomcam().trim().equals("ESTADO")) {
						estadoFinal = ss.getValalf();
						codEstadoFinal = ss.getResval();
					}

				}

			List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
			moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(),
					(int) rutnum);
			listaUnidades = (List<Presw18DTO>) accionweb.getSesion()
					.getAttribute("cuentasPresupuestarias");
			// consulta borrar
			if (listaSolicitudes != null && listaSolicitudes.size() > 0) {
				accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes);
				accionweb.getSesion().setAttribute("listaActualizada",
						listaSolicitudes);
			}
			if (listaSolicitudesPago != null && listaSolicitudesPago.size() > 0) {
				accionweb.agregarObjeto("listaSolicitudesPago",
						listaSolicitudesPago);
				accionweb.getSesion().setAttribute("listaSolicitudesPago",
						listaSolicitudesPago);
			}
			if (listaHistorial != null && listaHistorial.size() > 0) {
				accionweb.agregarObjeto("listaHistorial", listaHistorial);
				accionweb.getSesion().setAttribute("listaHistorial",
						listaHistorial);
			}
			long totales_cargo = getTotales(listaSolicitudes, "cargo");
			long totales_abono = getTotales(listaSolicitudes, "abono");
			accionweb.agregarObjeto("totales_cargo", totales_cargo);
			accionweb.agregarObjeto("totales_abono", totales_abono);

			accionweb.agregarObjeto("rutnum", rutnum);
			accionweb.agregarObjeto("dvrut", dvrut);
			// accionweb.agregarObjeto("estado",estado);
			accionweb.agregarObjeto("estadoFinal", estadoFinal);

		//	accionweb.agregarObjeto("numVale", String.valueOf(numDoc));
						
			
			ConexionBanner con = new ConexionBanner();
			String orga_completa = moduloReitemizacionB.getNombreOrgn(organizacion.trim(), con.getConexion());
			con.CerrarConexion();
			
			
			accionweb.agregarObjeto("copia", true);	
			accionweb.agregarObjeto("orgnseleccionada", organizacion.trim()); 
			accionweb.agregarObjeto("orgnnombre", orga_completa.trim());
		
		//	accionweb.agregarObjeto("read", readonly);
			accionweb.agregarObjeto("fecha", fecha);
			accionweb.agregarObjeto("cuenta", cuenta);
			accionweb.agregarObjeto("muestradatos", 1);
			accionweb.agregarObjeto("motiv_selecc", glosafinal);
			accionweb.agregarObjeto("nomIdentificador", nomIdentificador);
			Collection<ItemPOJO> listaUnidadesCTA = new ArrayList<ItemPOJO>();
			ModuloReitemizacionB presuB = new ModuloReitemizacionB();
			listaUnidadesCTA = (Collection<ItemPOJO>) presuB.getCuentasorganizacion(organizacion);
			accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidadesCTA);
			if (listaSolicitudes != null && listaSolicitudes.size() > 0) {
				accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes);
				accionweb.getSesion().setAttribute("listaActualizada",
						listaSolicitudes);
			}
		}
	
	}catch (Exception ex) {
		
	}
	
	}
	*/
	public void verificaRut(AccionWeb accionweb) throws Exception {
		String orgn = "" + accionweb.getParameter("cuenta");		
		int ind = orgn.indexOf("@");
		String orgn_completo = orgn.substring(ind + 1,orgn.length());
		orgn = orgn.substring(0, ind).trim();
		if (orgn != null && !orgn.isEmpty())
			accionweb.getSesion().removeAttribute("organizacion");

		accionweb.getSesion().setAttribute("organizacion", orgn);
		String rutUsuarioaux = ""
				+ accionweb.getSesion().getAttribute("rutUsuario");
		int rutnum = Integer.parseInt(rutUsuarioaux);
		String dvrut = accionweb.getSesion().getAttribute("dv") + "";

		rutUsuarioaux = rutUsuarioaux + dvrut;
		int identificador = Util.validaParametro(accionweb
				.getParameter("identificador"), 0);
		List<Presw18DTO> listaSolicitudes = (List<Presw18DTO>) accionweb
				.getSesion().getAttribute("listaSolicitudes");
		if (listaSolicitudes != null && listaSolicitudes.size() > 0)
			accionweb.getSesion().removeAttribute("listaSolicitudes");
		List<Presw18DTO> listaSolicitudesPago = (List<Presw18DTO>) accionweb
				.getSesion().getAttribute("listaSolicitudesPago");
		if (listaSolicitudesPago != null && listaSolicitudesPago.size() > 0)
			accionweb.getSesion().removeAttribute("listaSolicitudesPago");
		String fechaActual = new SimpleDateFormat("dd/MM/yyyy")
				.format(new Date());
		String nombre = "";
		if (rutnum > 0) {
			nombre = moduloValePagoB.getVerificaRut(rutnum + "" + dvrut);
			accionweb.agregarObjeto("rutnum", rutnum);
			accionweb.agregarObjeto("dvrut", dvrut);
			accionweb.agregarObjeto("fechaActual", fechaActual);

		}
		if (!nombre.trim().equals("")) {
			accionweb.agregarObjeto("hayDatoslista", 1);
			accionweb.agregarObjeto("nomIdentificador", nombre);
			accionweb.getSesion().setAttribute("nomIdentificador", nombre);
		} else
			accionweb
					.agregarObjeto("mensaje",
							"Este RUT no est� registrado. Debe solicitar a FINANZAS su creaci�n.");

		Collection<ItemPOJO> listaUnidades = new ArrayList<ItemPOJO>();
		moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(), rutnum);
		accionweb.agregarObjeto("identificador", identificador);
		accionweb.agregarObjeto("orgnseleccionada", orgn.trim()); 
		accionweb.agregarObjeto("orgnnombre", orgn_completo.trim());
		ModuloReitemizacionB presuB = new ModuloReitemizacionB();
		int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);
		if (listaUnidades != null && listaUnidades.size() > 0)
			accionweb.getSesion().removeAttribute("listaActualizadaSaldos");

		listaUnidades = (Collection<ItemPOJO>) presuB
				.getCuentasorganizacion(orgn);

		accionweb.agregarObjeto("cuentasPresupuestarias", listaUnidades);
		accionweb.agregarObjeto("esReitemizacionB", 1);
	//	accionweb.agregarObjeto("motiv_selecc", "");
		if (accionweb.getSesion().getAttribute("listaActualizadaSaldos") != null)
			accionweb.getSesion().removeAttribute("listaActualizadaSaldos");
		accionweb.getSesion().setAttribute("listaActualizadaSaldos",
				listaUnidades);
		
		
		
	}

	public long calculaSaldoCuenta(String cuenta, String orgn) throws Exception {

		// String cuenta = accionweb.getParameter("identificadorInterno");
		// String orgn = accionweb.getParameter("organizacionPage");
		System.out.println("calculaSaldoCuenta cuenta  " + cuenta + " ORGN "
				+ orgn);
		// int anno = Util.validaParametro(accionweb.getParameter("anno"), 0);

		long saldo = 0;
		ModuloValePagoB mod = new ModuloValePagoB();
		saldo = mod.getBuscaSaldo(orgn.trim(), cuenta.trim());

		// accionweb.agregarObjeto("saldos", saldo);
		return saldo;

	}

	public ModuloValePagoB getModuloValePagoB() {
		return moduloValePagoB;
	}

	public ModuloReitemizacionB getModuloReitemizacionB() {
		return moduloReitemizacionB;
	}

	public void setModuloReitemizacionB(
			ModuloReitemizacionB moduloReitemizacionB) {
		this.moduloReitemizacionB = moduloReitemizacionB;
	}

	public void setModuloValePagoB(ModuloValePagoB moduloValePagoB) {
		this.moduloValePagoB = moduloValePagoB;
	}

	public void grabarReitemizacion(AccionWeb accionweb) throws Exception {

	}

	public void verificaCuenta(AccionWeb accionweb) throws Exception {
		String cuenta = accionweb.getParameter("cuenta");
		String movimiento = accionweb.getParameter("movimiento");
		// true , existe en la lista y es el mismo movimiento
		// false, no existe en la lista
		String resp = "";
		List<movimientoPOJO> lista = (List<movimientoPOJO>) accionweb
				.getSesion().getAttribute("listaActualizada");
		if (lista == null) {
			// no existe lista
			resp = "no";
		} else {
			for (movimientoPOJO str : lista) {
				if ((str.getTipo().equals(movimiento))
						&& (str.getCuenta().equals(cuenta)))
					resp = "si";
				else
					resp = "no";

			}
		}
		System.out.println("existe cuenta " + resp);
		accionweb.agregarObjeto("resultado", resp);
	}

	public void actualizaLista(AccionWeb accionweb) throws Exception {
		String cuenta = accionweb.getParameter("cuenta").trim();
		String movimiento = accionweb.getParameter("movimiento");
		String cargo_aux = (accionweb.getParameter("cargo").replace(".", ""));
		String bono_aux = (accionweb.getParameter("abono").replace(".", ""));
		long cargo = Long.parseLong(cargo_aux.trim());
		// long abono = Long.parseLong(accionweb.getParameter("abono").trim() +
		// "");
		long abono = Long.parseLong(bono_aux.trim());
		String orgn = accionweb.getParameter("organizacion");
		List<ItemPOJO> listaSaldo = (List<ItemPOJO>) accionweb.getSesion()
				.getAttribute("listaActualizadaSaldos");
		List<movimientoPOJO> lista = (List<movimientoPOJO>) accionweb
				.getSesion().getAttribute("listaActualizada");
		long totales_cargo = Long.parseLong(Util.validaParametro(accionweb
				.getSesion().getAttribute("totales_cargo")
				+ "", "0"));
		long totales_abono = Long.parseLong(Util.validaParametro(accionweb
				.getSesion().getAttribute("totales_abono")
				+ "", "0"));
		int existe_posic = -1;
		long buscacargo = 0;
		long buscaabono = 0;
		Boolean modifica = true;
		long saldo = getBuscaSaldo(cuenta, listaSaldo);
		Boolean saldoinsuficiente = false;
		if (((saldo < cargo) && (movimiento.equals("cargo")))
		// || ((saldo < abono) && (movimiento.equals("abono"))) MB debido a que
		// se est� abonando en la cuenta
		) {

			saldoinsuficiente = true;
			if (lista == null) {
				totales_cargo = 0;
				totales_abono = 0;
			} else {
				totales_cargo = getTotales(lista, "cargo");
				totales_abono = getTotales(lista, "abono");
			}
			accionweb.agregarObjeto("totales_cargo", totales_cargo);
			accionweb.agregarObjeto("totales_abono", totales_abono);
			accionweb.agregarObjeto("modifico", modifica);
			accionweb.agregarObjeto("saldoinsuficiente", saldoinsuficiente);
		} else {
			try {
				if (lista == null) {
					lista = new ArrayList<movimientoPOJO>();
					existe_posic = -1;
				} else
					existe_posic = getBuscaCuenta(cuenta, lista);

				buscacargo = getBuscaContabilidad(cuenta, lista, "cargo");
				buscaabono = getBuscaContabilidad(cuenta, lista, "abono");
				cargo = cargo + buscacargo;
				abono = abono + buscaabono;

				movimientoPOJO ss = new movimientoPOJO();

				if (movimiento.equals("cargo")) {
					ss.setAbono(0);
					ss.setCargo(cargo);
					/*
					 * if (buscacargo == 0) creanueva = true; else if
					 * (buscacargo > 0) creanueva = false ;
					 */
				} else if (movimiento.equals("abono")) {
					ss.setCargo(0);
					ss.setAbono(abono);
					/*
					 * if (buscaabono == 0) creanueva = true; else if
					 * (buscaabono > 0) creanueva = false ;
					 */
				}

				if (movimiento.equals("borrar")) {
					lista.remove(existe_posic);
				}
				/*
				 * else if ((existe_posic > -1) && (creanueva == false)) {
				 * lista.set(existe_posic, ss);// deberia mandar mensaje q no se
				 * puede }
				 */else if (existe_posic == -1) {
					ss.setTipo(movimiento);
					ss.setCuenta(cuenta);
					ss.setOrgn(orgn);
					if (movimiento.equals("cargo"))
						saldo = saldo - cargo;
					else saldo = saldo + abono;
					ss.setSaldo(saldo);
					lista.add(ss);
				} else {
					modifica = false;
				}
				if (accionweb.getSesion().getAttribute("listaActualizada") != null)
					accionweb.getSesion().removeAttribute("listaActualizada");

				accionweb.getSesion().setAttribute("listaActualizada", lista);
				totales_cargo = getTotales(lista, "cargo");
				totales_abono = getTotales(lista, "abono");

				if (accionweb.getSesion().getAttribute("totales_cargo") != null)
					accionweb.getSesion().removeAttribute("totales_cargo");
				if (accionweb.getSesion().getAttribute("totales_abono") != null)
					accionweb.getSesion().removeAttribute("totales_abono");

				accionweb.getSesion().setAttribute("totales_cargo",
						totales_cargo);
				accionweb.getSesion().setAttribute("totales_abono",
						totales_abono);
				accionweb.agregarObjeto("totales_cargo", totales_cargo);
				accionweb.agregarObjeto("totales_abono", totales_abono);
				accionweb.agregarObjeto("modifico", modifica);
				accionweb.agregarObjeto("muestradatos", 0);
			} catch (Exception ex) {

			}
		}

		accionweb.agregarObjeto("listaActualizada", lista);
	}

	
	
	
	public void actualizaListaMes(AccionWeb accionweb) throws Exception {
		String cuenta = accionweb.getParameter("cuenta").trim();
		List<String> trasMeses = new ArrayList<String>();
		 trasMeses.add(accionweb.getParameter("traspaso_Enero"));
		 trasMeses.add(accionweb.getParameter("traspaso_Febrero"));
		 trasMeses.add(accionweb.getParameter("traspaso_Marzo"));
		 trasMeses.add(accionweb.getParameter("traspaso_Abril"));
		 trasMeses.add(accionweb.getParameter("traspaso_Mayo"));
		 trasMeses.add(accionweb.getParameter("traspaso_Junio"));
		 trasMeses.add(accionweb.getParameter("traspaso_Julio"));
		 trasMeses.add(accionweb.getParameter("traspaso_Agosto"));
		 trasMeses.add(accionweb.getParameter("traspaso_Septiembre"));
		 trasMeses.add(accionweb.getParameter("traspaso_Octubre"));
		 trasMeses.add(accionweb.getParameter("traspaso_Noviembre"));
		 trasMeses.add(accionweb.getParameter("traspaso_Diciembre"));
		 
		    for(int x=0;x<trasMeses.size();x++) {
		        System.out.println(trasMeses.get(x));
		      }
		 
		 
		/*
		String cuenta = accionweb.getParameter("cuenta").trim();
		String movimiento = accionweb.getParameter("movimiento");
		String cargo_aux = (accionweb.getParameter("cargo").replace(".", ""));
		String bono_aux = (accionweb.getParameter("abono").replace(".", ""));
		long cargo = Long.parseLong(cargo_aux.trim());		
		long abono = Long.parseLong(bono_aux.trim());
		String orgn = accionweb.getParameter("organizacion");
		List<ItemPOJO> listaSaldo = (List<ItemPOJO>) accionweb.getSesion()
				.getAttribute("listaActualizadaSaldos");
		List<movimientoPOJO> lista = (List<movimientoPOJO>) accionweb
				.getSesion().getAttribute("listaActualizada");
		long totales_cargo = Long.parseLong(Util.validaParametro(accionweb
				.getSesion().getAttribute("totales_cargo")
				+ "", "0"));
		long totales_abono = Long.parseLong(Util.validaParametro(accionweb
				.getSesion().getAttribute("totales_abono")
				+ "", "0"));
		int existe_posic = -1;
		long buscacargo = 0;
		long buscaabono = 0;
		Boolean modifica = true;
		long saldo = getBuscaSaldo(cuenta, listaSaldo);
		Boolean saldoinsuficiente = false;
		if (((saldo < cargo) && (movimiento.equals("cargo")))
		// se est� abonando en la cuenta
		) {
			saldoinsuficiente = true;
			if (lista == null) {
				totales_cargo = 0;
				totales_abono = 0;
			} else {
				totales_cargo = getTotales(lista, "cargo");
				totales_abono = getTotales(lista, "abono");
			}
			accionweb.agregarObjeto("totales_cargo", totales_cargo);
			accionweb.agregarObjeto("totales_abono", totales_abono);
			accionweb.agregarObjeto("modifico", modifica);
			accionweb.agregarObjeto("saldoinsuficiente", saldoinsuficiente);
		} else {
			try {
				if (lista == null) {
					lista = new ArrayList<movimientoPOJO>();
					existe_posic = -1;
				} else
					existe_posic = getBuscaCuenta(cuenta, lista);

				buscacargo = getBuscaContabilidad(cuenta, lista, "cargo");
				buscaabono = getBuscaContabilidad(cuenta, lista, "abono");
				cargo = cargo + buscacargo;
				abono = abono + buscaabono;

				movimientoPOJO ss = new movimientoPOJO();

				if (movimiento.equals("cargo")) {
					ss.setAbono(0);
					ss.setCargo(cargo);			
				} else if (movimiento.equals("abono")) {
					ss.setCargo(0);
					ss.setAbono(abono);
				}

				if (movimiento.equals("borrar")) {
					lista.remove(existe_posic);
				}
				 else if (existe_posic == -1) {
					ss.setTipo(movimiento);
					ss.setCuenta(cuenta);
					ss.setOrgn(orgn);
					if (movimiento.equals("cargo"))
						saldo = saldo - cargo;
					else saldo = saldo + abono;
					ss.setSaldo(saldo);
					lista.add(ss);
				} else {
					modifica = false;
				}
				if (accionweb.getSesion().getAttribute("listaActualizada") != null)
					accionweb.getSesion().removeAttribute("listaActualizada");
				accionweb.getSesion().setAttribute("listaActualizada", lista);
				totales_cargo = getTotales(lista, "cargo");
				totales_abono = getTotales(lista, "abono");

				if (accionweb.getSesion().getAttribute("totales_cargo") != null)
					accionweb.getSesion().removeAttribute("totales_cargo");
				if (accionweb.getSesion().getAttribute("totales_abono") != null)
					accionweb.getSesion().removeAttribute("totales_abono");

				accionweb.getSesion().setAttribute("totales_cargo",
						totales_cargo);
				accionweb.getSesion().setAttribute("totales_abono",
						totales_abono);
				accionweb.agregarObjeto("totales_cargo", totales_cargo);
				accionweb.agregarObjeto("totales_abono", totales_abono);
				accionweb.agregarObjeto("modifico", modifica);
				accionweb.agregarObjeto("muestradatos", 0);
			} catch (Exception ex) {

			}
		}

		accionweb.agregarObjeto("listaActualizada", lista); */
	}

	
	
	
	synchronized public void actualizaReitemizacion(AccionWeb accionweb)
			throws Exception {
		String accion = accionweb.getParameter("accion");
		String cuenta = accionweb.getParameter("cuenta");
		String orgn = accionweb.getParameter("organizacion");
		String doc = accionweb.getParameter("numdocumento").trim();

		String rut = accionweb.getParameter("rut");
		String dv = accionweb.getParameter("dv");
		System.out.println("rut " + rut);
		System.out.println("rut " + rut + dv);

		Calendar fecha = new GregorianCalendar();
		int a�o = fecha.get(Calendar.YEAR);
		int mes = fecha.get(Calendar.MONTH) + 1;
		int dia = fecha.get(Calendar.DAY_OF_MONTH);
		int hora = fecha.get(Calendar.HOUR_OF_DAY);
		int minuto = fecha.get(Calendar.MINUTE);
		int segundo = fecha.get(Calendar.SECOND);
		String mestexto = (mes > 9) ? String.valueOf(mes) : "0"
				+ String.valueOf(mes);
		String diatexto = (dia > 9) ? String.valueOf(dia) : "0"
				+ String.valueOf(dia);
		String mintexto = (minuto > 9) ? String.valueOf(minuto) : "0"
				+ String.valueOf(minuto);
		String segtexto = (segundo > 9) ? String.valueOf(segundo) : "0"
				+ String.valueOf(segundo);
		int fechaAc = Integer.parseInt(String.valueOf(a�o) + mestexto
				+ diatexto);
		int horaAc = Integer.parseInt(String.valueOf(hora) + mintexto
				+ segtexto);

		int resultado = moduloReitemizacionB.saveActualizaReitemizacion(accion,
				Integer.parseInt(doc.trim()), fechaAc, horaAc, Integer
						.parseInt(rut.trim()), dv);

		// int resultado = 0;

		accionweb.agregarObjeto("resultado", resultado);
		accionweb.agregarObjeto("permiteAuto", true);
		accionweb.agregarObjeto("totales_cargo", 0);
		accionweb.agregarObjeto("totales_abono", 0);
		// -1 no actualizo
		// -2 no inserta
		// -3 error actualiza
		// -4 error inserta
		// 0 transacci�n correcta
		if ((resultado == 0) && (accion.equals("P")))
		// llama a actualizar banner
		{
			String motivo = accionweb.getParameter("motivo").trim();
			String mesFecha = accionweb.getParameter("mesFecha").trim();
			ModuloReitemizacionB moduloReitemizacionB = new ModuloReitemizacionB();
			List<fgbjvcdPOJO> listaFinal = new ArrayList<fgbjvcdPOJO>();
			List<movimientoPOJO> listagetDatos = (List<movimientoPOJO>) accionweb
					.getSesion().getAttribute("listaActualizada");
			int cont = 1;
			List<fgbjvcdPOJO> lista = moduloReitemizacionB.getDatosOrgn(orgn);

			for (movimientoPOJO ss : listagetDatos) {
				fgbjvcdPOJO objeto = new fgbjvcdPOJO();
				objeto.setFGBJVCD_SEQ_NUM(cont); // SECUENCIA
				objeto.setFGBJVCD_RUCL_CODE("BD06"); // TIPO POLIZA
				objeto.setFGBJVCD_COAS_CODE("S"); // CDC
				objeto.setFGBJVCD_ACCI_CODE(orgn); // INDICE
				objeto
						.setFGBJVCD_FUND_CODE(lista.get(0)
								.getFGBJVCD_FUND_CODE()); // FONDO
				objeto.setFGBJVCD_ORGN_CODE(orgn); // ORGANIZACION
				objeto
						.setFGBJVCD_ACCT_CODE(lista.get(0)
								.getFGBJVCD_ACCT_CODE()); // CUENTA
				objeto
						.setFGBJVCD_PROG_CODE(lista.get(0)
								.getFGBJVCD_PROG_CODE()); // PROGRAMA
				if (ss.getCargo() > 0) {
					objeto.setFGBJVCD_TRANS_AMT(ss.getCargo()); // MONTO
					objeto.setFGBJVCD_DR_CR_IND("-"); // CREDITO
				} else if (ss.getAbono() > 0) {
					objeto.setFGBJVCD_TRANS_AMT(ss.getAbono()); // MONTO
					objeto.setFGBJVCD_DR_CR_IND("+"); // DEBITO
				}
				objeto.setFGBJVCD_TRANS_DESC(motivo); // MOTIVO
				objeto.setFGBJVCD_DOC_REF_NUM(doc); // DOCUMENTO
				objeto.setFGBJVCD_BUDGET_PERIOD(mesFecha); // PERIODO
				listaFinal.add(objeto);
				cont = cont + 1;
			}
			moduloReitemizacionB.saveReitemizacionBanner(listaFinal);
		}
	}

	
	

	synchronized public void eliminaReitemizacion(AccionWeb accionweb)
			throws Exception {
		String accion = accionweb.getParameter("accion");
		String cuenta = accionweb.getParameter("cuenta");
		String orgn = accionweb.getParameter("organizacion");
		String doc = accionweb.getParameter("numdocumento").trim();

		String rut = accionweb.getParameter("rut");
		String dv = accionweb.getParameter("dv");
		

		Calendar fecha = new GregorianCalendar();
		int a�o = fecha.get(Calendar.YEAR);
		int mes = fecha.get(Calendar.MONTH) + 1;
		int dia = fecha.get(Calendar.DAY_OF_MONTH);
		int hora = fecha.get(Calendar.HOUR_OF_DAY);
		int minuto = fecha.get(Calendar.MINUTE);
		int segundo = fecha.get(Calendar.SECOND);
		String mestexto = (mes > 9) ? String.valueOf(mes) : "0"
				+ String.valueOf(mes);
		String diatexto = (dia > 9) ? String.valueOf(dia) : "0"
				+ String.valueOf(dia);
		String mintexto = (minuto > 9) ? String.valueOf(minuto) : "0"
				+ String.valueOf(minuto);
		String segtexto = (segundo > 9) ? String.valueOf(segundo) : "0"
				+ String.valueOf(segundo);
		int fechaAc = Integer.parseInt(String.valueOf(a�o) + mestexto
				+ diatexto);
		int horaAc = Integer.parseInt(String.valueOf(hora) + mintexto
				+ segtexto);

		int resultado = moduloReitemizacionB.saveActualizaReitemizacion(accion,
				Integer.parseInt(doc.trim()), fechaAc, horaAc, Integer
						.parseInt(rut.trim()), dv);

		// int resultado = 0;

		accionweb.agregarObjeto("resultado", resultado);
		
		accionweb.agregarObjeto("totales_cargo", 0);
		accionweb.agregarObjeto("totales_abono", 0);
		// -1 no actualizo
		// -2 no inserta
		// -3 error actualiza
		// -4 error inserta
		// 0 transacci�n correcta
	
	}

	public void getListaPresupuestoMensual(AccionWeb accionweb) throws Exception {
		try{			
			String codUnidad = accionweb.getParameter("orgn");
			String cuenta = accionweb.getParameter("cuenta");
			Calendar fecha = Calendar.getInstance();
			int a�o = fecha.get(Calendar.YEAR);
			
			int mes = fecha.get(Calendar.MONTH) + 1;
			List<Pres18POJO> listaPresw18 = null;
			
			
			String cuentas_excepciones = "";
			ModuloPresupuestoB mod = new ModuloPresupuestoB();
			cuentas_excepciones = mod.getCuentasExcepcionesAS400();
			
			
			
			  listaPresw18 = (List<Pres18POJO>) mod.getCuentasDetalles(codUnidad,a�o,mes,cuenta,cuentas_excepciones,true);
			System.out.println("tama�o lista cuentas detalles " + listaPresw18.size()); 
			accionweb.agregarObjeto("detalleDisponible", listaPresw18);
			accionweb.agregarObjeto("mes_actual", mes);
		}catch (Exception ex){
			System.out.println("Error ReitemizacionInterceptorB.getListaPresupuestoMensual " + ex.getMessage());	
		}
		
	}
	
	synchronized public void modificaReitemizacion(AccionWeb accionweb)
			throws Exception {
		Boolean modifico = false;
		String orgn = accionweb.getParameter("orgn");
		String doc = accionweb.getParameter("numdocumento").trim();
		int documento = Integer.valueOf(doc);
		String rutUsuarioaux = ""
				+ accionweb.getSesion().getAttribute("rutUsuario");
		int rutnum = Integer.parseInt(rutUsuarioaux);
		String dvrut = accionweb.getSesion().getAttribute("dv") + "";
		List<movimientoPOJO> listaMovimientos = (List<movimientoPOJO>) accionweb
				.getSesion().getAttribute("listaActualizada");
		Boolean correcto = getValoresCuadrados(listaMovimientos);
		if (correcto) {
			int numVale = 0;

			String glosa1 = accionweb.getParameter("motivo1");
			String glosa2 = accionweb.getParameter("motivo2");
			String glosa3 = accionweb.getParameter("motivo3");
			String glosa4 = accionweb.getParameter("motivo4");
			String glosa5 = accionweb.getParameter("motivo5");
			String glosa6 = accionweb.getParameter("motivo6");

			modifico = moduloReitemizacionB.getActualizaReitemizacion(accionweb
					.getReq(), rutnum, dvrut, documento, orgn, glosa1, glosa2,
					glosa3, glosa4, glosa5, glosa6);
			// accionweb.getSesion().removeAttribute("listaActualizada");
			accionweb.agregarObjeto("totales_cargo", 0);
			accionweb.agregarObjeto("totales_abono", 0);
		} else {
			long totales_cargo = Long.parseLong(Util.validaParametro(accionweb
					.getSesion().getAttribute("totales_cargo")
					+ "", "0"));
			long totales_abono = Long.parseLong(Util.validaParametro(accionweb
					.getSesion().getAttribute("totales_abono")
					+ "", "0"));
			totales_cargo = getTotales(listaMovimientos, "cargo");
			totales_abono = getTotales(listaMovimientos, "abono");
			accionweb.agregarObjeto("totales_cargo", totales_cargo);
			accionweb.agregarObjeto("totales_abono", totales_abono);
			accionweb.agregarObjeto("listaActualizada", listaMovimientos);
		}

		accionweb.agregarObjeto("correcto", correcto);

	}

	synchronized public void registraReitemizacion(AccionWeb accionweb)
			throws Exception {

		String orgn = "" + accionweb.getSesion().getAttribute("organizacion");
		String rutUsuarioaux = ""
				+ accionweb.getSesion().getAttribute("rutUsuario");
		int rutnum = Integer.parseInt(rutUsuarioaux);
		String dvrut = accionweb.getSesion().getAttribute("dv") + "";
		List<movimientoPOJO> listaMovimientos = (List<movimientoPOJO>) accionweb
				.getSesion().getAttribute("listaActualizada");
		Boolean correcto = getValoresCuadrados(listaMovimientos);
		if (correcto) {
			int numVale = 0;

			String glosa1 = accionweb.getParameter("motivo1");
			String glosa2 = accionweb.getParameter("motivo2");
			String glosa3 = accionweb.getParameter("motivo3");
			String glosa4 = accionweb.getParameter("motivo4");
			String glosa5 = accionweb.getParameter("motivo5");
			String glosa6 = accionweb.getParameter("motivo6");

			numVale = moduloReitemizacionB.getRegistraReitemizacion(accionweb
					.getReq(), rutnum, dvrut, orgn, glosa1, glosa2, glosa3,
					glosa4, glosa5, glosa6);
			// accionweb.getSesion().removeAttribute("listaActualizada");
			accionweb.agregarObjeto("totales_cargo", 0);
			accionweb.agregarObjeto("totales_abono", 0);
		} else {
			long totales_cargo = Long.parseLong(Util.validaParametro(accionweb
					.getSesion().getAttribute("totales_cargo")
					+ "", "0"));
			long totales_abono = Long.parseLong(Util.validaParametro(accionweb
					.getSesion().getAttribute("totales_abono")
					+ "", "0"));
			totales_cargo = getTotales(listaMovimientos, "cargo");
			totales_abono = getTotales(listaMovimientos, "abono");
			accionweb.agregarObjeto("totales_cargo", totales_cargo);
			accionweb.agregarObjeto("totales_abono", totales_abono);
			accionweb.agregarObjeto("listaActualizada", listaMovimientos);
		}

		accionweb.agregarObjeto("correcto", correcto);
		// borra datos session proceso
		/*
		 * accionweb.getSesion().removeAttribute("listaActualizadaSaldos");
		 * accionweb.getSesion().removeAttribute("listaActualizada");
		 * accionweb.getSesion().removeAttribute("totales_cargo");
		 * accionweb.getSesion().removeAttribute("totales_abono");
		 * accionweb.getSesion().removeAttribute("listaSolicitudesPago");
		 * accionweb.getSesion().removeAttribute("organizacion");
		 * accionweb.getSesion().removeAttribute("cuentasPresupuestarias");
		 * accionweb.getSesion().removeAttribute("listaOrganizacion");
		 */
		if (correcto)
			limpiaSession(accionweb);

	}

	private Boolean getValoresCuadrados(List<movimientoPOJO> lista) {
		Boolean resp = false;
		try {
			long total_cargos = getTotales(lista, "cargo");
			long total_abonos = getTotales(lista, "abono");

			if (total_cargos == total_abonos)
				resp = true;
			else
				resp = false;

		} catch (Exception ex) {
			resp = false;
		}
		return resp;

	}

	private long getTotales(List<movimientoPOJO> lista, String movimiento) {
		long total = 0;
		long totalCargo = 0;
		long totalAbono = 0;
		for (movimientoPOJO str : lista) {
			totalCargo = totalCargo + str.getCargo();
			totalAbono = totalAbono + str.getAbono();
		}

		if (movimiento.equals("cargo"))
			total = totalCargo;
		else
			total = totalAbono;

		return total;
	}

	private long getBuscaContabilidad(String cuenta,
			List<movimientoPOJO> listaSaldos, String movimiento) {
		long cargo = 0;
		long abono = 0;
		long valor = 0;
		try {
			if (movimiento.equals("cargo")) {
				for (movimientoPOJO str : listaSaldos) {
					if ((str.getCuenta().equals(cuenta.trim()))
							&& (str.getAbono() == 0)) {
						cargo = str.getCargo();
						abono = str.getAbono();
						break;
					}
				}
			} else {
				for (movimientoPOJO str : listaSaldos) {
					if ((str.getCuenta().equals(cuenta.trim()))
							&& (str.getCargo() == 0)) {
						cargo = str.getCargo();
						abono = str.getAbono();
						break;
					}
				}
			}
		} catch (Exception ex) {
			System.out.println("Error getBuscaContabilidad" + ex.getMessage());
		}
		if (movimiento.equals("cargo"))
			valor = cargo;
		else
			valor = abono;

		return valor;
	}

	private long getBuscaSaldo(String cuenta, List<ItemPOJO> listaSaldos) {
		long saldo = 0;
		long cargo = 0;
		long abono = 0;
		try {
			for (ItemPOJO str : listaSaldos) {
				if (str.getCoditem().equals(cuenta.trim())) {
					saldo = str.getSaldo();
					break;
				}
			}
		} catch (Exception ex) {
			System.out.println("Error getBuscaSaldo" + ex.getMessage());
			saldo = 0;
		}
		return saldo;
	}

	public int getBuscaCuenta(String cuenta, List<movimientoPOJO> lista) {
		Iterator<movimientoPOJO> iterator = lista.iterator();
		int posicion = -1;
		int contador = 0;
		try {
			System.out.println("Cuenta 2 " + cuenta.trim());
			for (movimientoPOJO customer : lista) {
				System.out.println("Cuenta 1 " + customer.getCuenta().trim());
				if (customer.getCuenta().trim().equals(cuenta.trim())) {
					posicion = contador;
					break;
				} else
					contador = contador + 1;
			}

		} catch (Exception ex) {
			posicion = -1;
		}

		return posicion;
	}

	public void consultaReitemizacion(AccionWeb accionweb) throws Exception {
		accionweb.agregarObjeto("esReitemizacionB", 1);

		String cuenta = "";
		String muestraSaldo = "";
		String glosa1 = "";
		String glosa2 = "";
		String glosa3 = "";
		String glosa4 = "";
		String glosa5 = "";
		String glosa6 = "";
		long total_saldo = 0;
		String estadoFinal = "";
		String codEstadoFinal = "";
		long totalSolicitudVale = 0;
		long totalSolicitudPago = 0;

		long saldoCuenta = 0;
		int permiteAut = 1;
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		CocowBean cocowBean = new CocowBean();
		String glosafinal = "";
		long fecha = 0;
		ModuloReitemizacionB moduloReitemizacionB = new ModuloReitemizacionB();
		String rutUsuario = ""
				+ accionweb.getSesion().getAttribute("rutUsuario");
		long rutnum = Integer.parseInt(rutUsuario);
		String dv = accionweb.getSesion().getAttribute("dv") + "";
		String dvrut = dv;
		int tipo = Util.validaParametro(accionweb.getParameter("tipo"), 0);
		int numDoc = Util.validaParametro(accionweb.getParameter("numDoc"), 0);
		if (numDoc == 0)
			numDoc = Util.validaParametro(accionweb.getSesion().getAttribute(
					"numDoc"), 0);
		
		/*if ((numDoc == 0)&& (tipo == 7)) {
			String copia_seleccionada = "" + accionweb.getParameter("listaItem");
			int pos =  copia_seleccionada.indexOf("@");
			String id = copia_seleccionada.substring(0,pos).trim();
			numDoc = Integer.parseInt(id);
		}*/
		if (numDoc > 0 && numDoc < 99999999) {

			// String organizacion = "" +
			// accionweb.getSesion().getAttribute("organizacion");
			String organizacion = "";

			String nomIdentificador = "";

			Presw19DTO preswbean19DTO = new Presw19DTO();
			String titulo = "";
			String tituloDetalle1 = "";
			String tituloDetalle2 = "";
			String readonly = "readonly";

			// capturar los datos

			if (tipo == 2) // lista de vales para consultar
			{
				titulo = "Modifica ";
				tituloDetalle1 = "Modificar";
				readonly = "";
			}
			if (tipo == 3) // lista de vales para consulta
			{
				titulo = "Consulta ";
				tituloDetalle1 = "Consultar";
			}
			if (tipo == 4) // lista de vales para Autorizacion responsable
			{
				titulo = "Autorizaci�n ";
				tituloDetalle1 = "Autorizaci�n";
				tituloDetalle2 = "Rechazar";
			}
			if (tipo == 5) // lista de vales para preaprobar
			{
				titulo = "Preaprobaci�n";
				tituloDetalle1 = "Preaprobar";
			}
			if (tipo == 6) // lista de vales para autorizar DIPRES
			{
				titulo = "Autorizaci�n DIPRES";
				tituloDetalle1 = "Autorizar";
			}

			List<Presw18DTO> listaSolicitudesPago = new ArrayList<Presw18DTO>();
			List<movimientoPOJO> listaSolicitudes = new ArrayList<movimientoPOJO>();
			List<Presw18DTO> listaHistorial = new ArrayList<Presw18DTO>();
			preswbean19DTO.setTippro("GDI");
			preswbean19DTO.setRutide(new BigDecimal(rutUsuario));
			preswbean19DTO.setDigide(dv);
			preswbean19DTO.setNumdoc(new BigDecimal(numDoc));
			listCocow36DTO = cocowBean.buscar_cocow36(preswbean19DTO);
			List<ItemPOJO> listasaldos = new ArrayList<ItemPOJO>();
			String orgn = "" + accionweb.getParameter("cuenta");

			System.out.println("Tama�o  lista " + listCocow36DTO.size());
			if (listCocow36DTO != null && listCocow36DTO.size() > 0)

				for (Cocow36DTO ss : listCocow36DTO) {
					System.out.println("Nombre Iseries "
							+ ss.getNomcam().trim());
					if (ss.getNomcam().trim().equals("CODORG")) {
						organizacion = ss.getValalf().trim();

						listasaldos = (List<ItemPOJO>) moduloReitemizacionB
								.getCuentasorganizacion(organizacion);
						if (accionweb.getSesion().getAttribute(
								"listaActualizadaSaldos") != null)
							accionweb.getSesion().removeAttribute(
									"listaActualizadaSaldos");
						accionweb.getSesion().setAttribute(
								"listaActualizadaSaldos", listasaldos);
					}
					if (ss.getNomcam().trim().equals("GLOSA1"))
						glosa1 = ss.getValalf().trim();
					if (ss.getNomcam().trim().equals("GLOSA2"))
						glosa2 = ss.getValalf().trim();
					if (ss.getNomcam().trim().equals("GLOSA3"))
						glosa3 = ss.getValalf().trim();
					if (ss.getNomcam().trim().equals("GLOSA4"))
						glosa4 = ss.getValalf().trim();
					if (ss.getNomcam().trim().equals("GLOSA5"))
						glosa5 = ss.getValalf().trim();
					if (ss.getNomcam().trim().equals("GLOSA6"))
						glosa6 = ss.getValalf().trim();
					if (ss.getNomcam().trim().equals("GLOSA1")
							|| ss.getNomcam().trim().equals("GLOSA2")
							|| ss.getNomcam().trim().equals("GLOSA3")
							|| ss.getNomcam().trim().equals("GLOSA4")
							|| ss.getNomcam().trim().equals("GLOSA5")
							|| ss.getNomcam().trim().equals("GLOSA6"))
						glosafinal = glosafinal + ss.getValalf();

					if (ss.getNomcam().trim().equals("TOTAL"))
						total_saldo = ss.getValnu1();

					if (ss.getNomcam().trim().equals("CUENTA")) { // detalle
						// de los
						// cargos-abono
						// while
						movimientoPOJO presw18DTO = new movimientoPOJO();

						presw18DTO.setCuenta(ss.getValalf()); // $NOMBRE
						cuenta = ss.getValalf(); // CUENTA
						presw18DTO.setCargo(ss.getValnu1()); // $CARGO
						presw18DTO.setAbono(ss.getValnu2()); // $Abono
						if (presw18DTO.getCargo() > 0)
							presw18DTO.setTipo("Cargo");
						else if (presw18DTO.getAbono() > 0)
							presw18DTO.setTipo("Abono");

						if (ss.getValnu2() > saldoCuenta) {
							muestraSaldo = "<font class=\"Estilo_Rojo\" >"
									+ saldoCuenta + "</font>";
							permiteAut = 2;
						} else
							muestraSaldo = saldoCuenta + "";

						accionweb.agregarObjeto("permiteAut", permiteAut);
						long saldo = getBuscaSaldo(cuenta, listasaldos);
						Boolean permiteAuto = true;
						if ((tipo == 4)
								&& (presw18DTO.getTipo().equals("Cargo"))) {
							// valida q los saldos sean mayor a 0 cuando es
							// cargo
							if (saldo < 1)
								permiteAuto = false;
						}
						accionweb.agregarObjeto("permiteAuto", permiteAuto);
						presw18DTO.setSaldo(saldo);

						listaSolicitudes.add(presw18DTO);
						totalSolicitudVale += ss.getValnu2();
					}
					if (ss.getNomcam().trim().equals("NOMBEN")) {
						Presw18DTO presw18DTO = new Presw18DTO();
						presw18DTO.setUsadom(ss.getValnu1());
						presw18DTO.setDesuni(ss.getValalf());
						presw18DTO.setRutide(Integer.parseInt(ss.getValnu2()
								+ "")); // rut
						presw18DTO.setIddigi(ss.getAccion()); // dv
						listaSolicitudesPago.add(presw18DTO);
						totalSolicitudPago += ss.getValnu1();
					}
					if (ss.getNomcam().trim().equals("HISTORIAL")) {
						Presw18DTO presw18DTO = new Presw18DTO();
						presw18DTO.setUsadom(ss.getValnu1()); // es la fecha,

						if (fecha == 0)
							fecha = ss.getValnu1();
						presw18DTO.setNompro(ss.getResval().substring(26));// en
						// caso
						// de
						// Resval="Autoriza"
						// es
						// la
						// unidad

						presw18DTO.setIndpro(ss.getResval().substring(15, 25));// comentario
						presw18DTO.setDesite(ss.getValalf()); // es el
						// responsable

						presw18DTO.setDesuni(ss.getResval().substring(0, 15)); // es
						// la
						// accion,
						// INGRESO,
						// MODIFICACION,
						// AUTORIZACION,
						// PAGO,
						// AUTORIZA
						// FINANZAS
						// //
						// Tipo
						// Movimeinto
						listaHistorial.add(presw18DTO);
					}
					if (ss.getNomcam().trim().equals("ESTADO")) {
						estadoFinal = ss.getValalf();
						codEstadoFinal = ss.getResval();
					}

				}

			List<Presw18DTO> listaUnidades = new ArrayList<Presw18DTO>();
			moduloValePagoB.getCuentasAutorizadas(accionweb.getReq(),
					(int) rutnum);
			listaUnidades = (List<Presw18DTO>) accionweb.getSesion()
					.getAttribute("cuentasPresupuestarias");
			// consulta borrar
			if (listaSolicitudes != null && listaSolicitudes.size() > 0) {
				accionweb.agregarObjeto("listaSolicitudes", listaSolicitudes);
				accionweb.getSesion().setAttribute("listaActualizada",
						listaSolicitudes);
			}
			if (listaSolicitudesPago != null && listaSolicitudesPago.size() > 0) {
				accionweb.agregarObjeto("listaSolicitudesPago",
						listaSolicitudesPago);
				accionweb.getSesion().setAttribute("listaSolicitudesPago",
						listaSolicitudesPago);
			}
			if (listaHistorial != null && listaHistorial.size() > 0) {
				accionweb.agregarObjeto("listaHistorial", listaHistorial);
				accionweb.getSesion().setAttribute("listaHistorial",
						listaHistorial);
			}
			long totales_cargo = getTotales(listaSolicitudes, "cargo");
			long totales_abono = getTotales(listaSolicitudes, "abono");
			accionweb.agregarObjeto("totales_cargo", totales_cargo);
			accionweb.agregarObjeto("totales_abono", totales_abono);

			accionweb.agregarObjeto("rutnum", rutnum);
			accionweb.agregarObjeto("dvrut", dvrut);
			// accionweb.agregarObjeto("estado",estado);
			accionweb.agregarObjeto("estadoFinal", estadoFinal);

			accionweb.agregarObjeto("numVale", String.valueOf(numDoc));
			nomIdentificador = ""
					+ accionweb.getSesion().getAttribute("nomIdentificador");

			if (!nomIdentificador.equals("null"))
				accionweb.agregarObjeto(" nomIdentificador", nomIdentificador);
			else {
				nomIdentificador = moduloValePagoB.getVerificaRut(rutnum + ""
						+ dvrut);
				accionweb.agregarObjeto(" nomIdentificador", nomIdentificador);
			}

			accionweb.agregarObjeto("cuentasPresupuestarias", listasaldos);
			accionweb.agregarObjeto("opcion", tipo);
			accionweb.agregarObjeto("orgnseleccionada", organizacion);
			accionweb.agregarObjeto("glosa1", glosa1);
			accionweb.agregarObjeto("glosa2", glosa2);
			accionweb.agregarObjeto("glosa3", glosa3);
			accionweb.agregarObjeto("glosa4", glosa4);
			accionweb.agregarObjeto("glosa5", glosa5);
			accionweb.agregarObjeto("glosa6", glosa6);
			accionweb.agregarObjeto("read", readonly);
			accionweb.agregarObjeto("fecha", fecha);
			accionweb.agregarObjeto("cuenta", cuenta);
			accionweb.agregarObjeto("muestradatos", 1);
			accionweb.agregarObjeto("motivodetalle", glosafinal);
			accionweb.agregarObjeto("nomIdentificador", nomIdentificador);
			accionweb.agregarObjeto("titulo", titulo);

		}
	}
}
