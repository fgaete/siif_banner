package cl.utfsm.sriB.modulo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import cl.utfsm.base.util.ConexionBanner;
import cl.utfsm.base.util.Util;
import cl.utfsm.conexion.ConexionAs400ValePago;
import cl.utfsm.sipB.POJO.ItemPOJO;
import cl.utfsm.sipB.POJO.Pres18POJO;
import cl.utfsm.sivB.modulo.ModuloValePagoB;
import cl.utfsm.sriB.POJO.fgbjvcdPOJO;
import cl.utfsm.sriB.POJO.movimientoPOJO;
import cl.utfsm.sriB.datos.ReitemizacionDao;
import descad.cliente.Ingreso_Documento;
import descad.cliente.MD5;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Presw18DTO;
import cl.utfsm.sriB.POJO.queryPOJO;

public class ModuloReitemizacionB {
	ReitemizacionDao reitemizacionDao;

	public ReitemizacionDao getReitemizacionDao() {
		return reitemizacionDao;
	}

	public void setReitemizacionDao(ReitemizacionDao reitemizacionDao) {
		this.reitemizacionDao = reitemizacionDao;
	}

	public List<String> getCuentas(String orgn, Connection cone) {
		List<String> lista = new ArrayList<String>();
		try {
			PreparedStatement sent = null;
			ResultSet res = null;
			String sql = "select DISTINCT FGBTRND_ACCT_CODE  "
					+ " from FGBTRND " + " where  FGBTRND_ORGN_CODE = ? ";
			sent = cone.prepareStatement(sql);
			sent.setString(1, orgn);
			res = sent.executeQuery();
			while (res.next()) {
				lista.add(res.getString(1));
			}
		} catch (Exception ex) {
			lista.add("error");
		}
		return lista;
	}

	public String getNombreOrgn(String orgn, Connection cone) {
		String titulo = "";
		try {
			PreparedStatement sent = null;
			ResultSet res = null;
			String sql = "SELECT FTVORGN_TITLE   " + "FROM FTVORGN "
					+ "WHERE FTVORGN_ORGN_CODE = ? ";
			sent = cone.prepareStatement(sql);
			sent.setString(1, orgn);
			res = sent.executeQuery();
			while (res.next()) {
				titulo = res.getString(1);
			}
		} catch (Exception ex) {
			titulo = "error";
		}
		return titulo.trim();
	}

	/*
	 * public Collection<Pres18POJO> getCuentasSaldos(String codOrg, String
	 * codCuenta) { List<Pres18POJO> lista = new ArrayList<Pres18POJO>(); List<String>
	 * listaCuentas = new ArrayList<String>(); try { String codFondo = ""; long
	 * saldoCuenta = 0; ConexionBanner con = new ConexionBanner();
	 * ModuloValePagoB valesP = new ModuloValePagoB(); listaCuentas =
	 * getCuentas(codOrg,con.getConexion()); codFondo =
	 * valesP.getFondoSaldo(codOrg); String sql = "{?=call
	 * PKG_VALIDACIONES.F_CALCULO_SALDO(?,?,?)}"; CallableStatement cstmt =
	 * con.getConexion().prepareCall(sql); for(String ctas : listaCuentas) {
	 * Pres18POJO ss = new Pres18POJO(); cstmt.registerOutParameter(1,
	 * Types.INTEGER); cstmt.setString(2,codOrg); cstmt.setString(3, ctas);
	 * cstmt.setInt(4,(codFondo.trim().equals("")?0:1)); cstmt.executeQuery();
	 * saldoCuenta = cstmt.getLong(1);
	 *  }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * cstmt.close(); con.close();
	 * 
	 * }catch (Exception ex){ } return lista; }
	 */
	public List<ItemPOJO> getSaldos(int a�oIN, String cuenta, String orgn,
			Connection cone) {

		List<ItemPOJO> saldos = new ArrayList<ItemPOJO>();
		if (a�oIN == 0) {
			Calendar fecha = Calendar.getInstance();
			a�oIN = fecha.get(Calendar.YEAR);
		}
		if (a�oIN != 0) {
			Calendar fecha = Calendar.getInstance();
			int a�o = a�oIN;
			String a�osql = String.valueOf(a�o);
			a�osql = a�osql.substring(2);
			a�o = 0;
			a�o = Integer.parseInt(a�osql);
			a�oIN = a�o;
		}
		Pres18POJO pres18POJO = new Pres18POJO();
		PreparedStatement sent = null;
		ResultSet res = null;
		List<Pres18POJO> listaSaldo = new ArrayList<Pres18POJO>();
		Calendar c = new GregorianCalendar();
		int mes = (c.get(Calendar.MONTH) + 1);

		System.out.println("getSaldos cuenta " + cuenta);
		try {
			String sql = "";

			sql = ""
					+ "select "
					+ " FGBTRND_ACCT_CODE,"
					+ " FTVACCT_TITLE,"
					+ "  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,?,FGBTRND.FGBTRND_TRANS_AMT,0),0))+ "
					+ "  SUM(DECODE(FGBTRND_FIELD_CODE,1,DECODE(FGBTRND_RUCL_CODE,?,FGBTRND.FGBTRND_TRANS_AMT,0),0))+ "
					+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,?,FGBTRND.FGBTRND_TRANS_AMT,0),0))+ "
					+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,?,FGBTRND.FGBTRND_TRANS_AMT,0),0))+  "
					+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,?,FGBTRND.FGBTRND_TRANS_AMT,0),0))+ "
					+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,?,FGBTRND.FGBTRND_TRANS_AMT,0),0))+ "
					+ "  SUM(DECODE(FGBTRND_FIELD_CODE,2,DECODE(FGBTRND_RUCL_CODE,?,FGBTRND.FGBTRND_TRANS_AMT,0),0)) +"
					+ " SUM(DECODE(fgbtrnd_field_code,1,DECODE(fgbtrnd_rucl_code,?,fgbtrnd.fgbtrnd_trans_amt,0),0) ) +"
					+ "  SUM(DECODE(fgbtrnd_field_code,2,DECODE(fgbtrnd_rucl_code,?,fgbtrnd.fgbtrnd_trans_amt,0),0) ) "
					+ " -"
					+ "  SUM(DECODE(FGBTRND_FIELD_CODE,3,FGBTRND.FGBTRND_TRANS_AMT,0))- "
					+ "  SUM(DECODE(FGBTRND_FIELD_CODE,4,FGBTRND.FGBTRND_TRANS_AMT,5,FGBTRND.FGBTRND_TRANS_AMT,0)) saldo "
					+ "  ,FTVFUND_TITLE fondo,"
					+ " FGBTRND_ACCT_CODE"
					+ " FROM FIMSMGR.FGBTRND,"
					+ "  FIMSMGR.FTVFUND,"
					+ "  FIMSMGR.FTVORGN,"
					+ "  FIMSMGR.FTVACCT,"
					+ "  FIMSMGR.FTVPROG"
					+ " WHERE FGBTRND_FUND_CODE      =FTVFUND_FUND_CODE"
					+ " AND FGBTRND_ORGN_CODE        =FTVORGN_ORGN_CODE"
					+ " AND FGBTRND_PROG_CODE        =FTVPROG_PROG_CODE"
					+ " AND FGBTRND_ACCT_CODE        =FTVACCT_ACCT_CODE"
					+ " AND FGBTRND_ACTIVITY_DATE   >=FTVFUND_EFF_DATE"
					+ " AND FGBTRND_ACTIVITY_DATE    <FTVFUND_NCHG_DATE"
					+ " AND FGBTRND_ACTIVITY_DATE   >=FTVORGN_EFF_DATE"
					+ " AND FGBTRND_ACTIVITY_DATE    <FTVORGN_NCHG_DATE"
					+ " AND FGBTRND_ACTIVITY_DATE   >=FTVPROG_EFF_DATE"
					+ " AND FGBTRND_ACTIVITY_DATE    <FTVPROG_NCHG_DATE"
					+ " AND FGBTRND_ACTIVITY_DATE   >=FTVACCT_EFF_DATE"
					+ " AND FGBTRND_ACTIVITY_DATE    <FTVACCT_NCHG_DATE"
					+ " AND FTVFUND_FTYP_CODE       IN(?,?,?,?,?)"
					+ " AND NOT ( FGBTRND_RUCL_CODE IN (?,?,?,?,?,?,?,?) AND "
					+ "       FTVACCT_ATYP_CODE IN "
					+ "         (SELECT FTVATYP_ATYP_CODE "
					+ "          FROM FTVATYP"
					+ "          WHERE FTVATYP_INTERNAL_ATYP_CODE = ?) )"
					+ " AND FTVFUND_DATA_ENTRY_IND   = ? "
					+ " AND FTVORGN_DATA_ENTRY_IND   =?"
					+ " AND FTVACCT_DATA_ENTRY_IND   =?"
					+ " AND FTVPROG_DATA_ENTRY_IND   =?"
					+ " AND FTVORGN_STATUS_IND = ?"
					+ " AND FGBTRND.FGBTRND_LEDC_CODE= ? "
					+ " AND FGBTRND_LEDGER_IND       =? "
					+ " AND FGBTRND_FSYR_CODE        =? "
					+ " AND  FGBTRND_POSTING_PERIOD BETWEEN ? AND ? "
					+ " AND FGBTRND_ORGN_CODE       = '"
					+ orgn
					+ "' "
					+ " AND FGBTRND_ACCT_CODE       in ( "
					+ cuenta
					+ " )"
					+ " GROUP BY FGBTRND_ACCT_CODE, FTVFUND_TITLE, FTVACCT_TITLE ORDER BY FGBTRND_ORGN_CODE ASC";
			System.out.println("Saldo " + sql);
			sent = cone.prepareStatement(sql);

			sent.setString(1, "BD01");
			sent.setString(2, "BD05");
			sent.setString(3, "BD07");
			sent.setString(4, "BD06");
			sent.setString(5, "BD02");
			sent.setString(6, "BD08");
			sent.setString(7, "BD09");
			sent.setString(8, "BD11");
			sent.setString(9, "BD12");
			sent.setString(10, "CA");
			sent.setString(11, "CD");
			sent.setString(12, "CN");
			sent.setString(13, "FI");
			sent.setString(14, "FR");
			sent.setString(15, "APS1");
			sent.setString(16, "APS2");
			sent.setString(17, "APS3");
			sent.setString(18, "APS4");
			sent.setString(19, "CHS1");
			sent.setString(20, "CSS1");
			sent.setString(21, "JE15");
			sent.setString(22, "CR05");
			sent.setString(23, "50");
			sent.setString(24, "Y");
			sent.setString(25, "Y");
			sent.setString(26, "Y");
			sent.setString(27, "Y");
			sent.setString(28, "A");
			sent.setString(29, "FINANC");
			sent.setString(30, "O");
			sent.setInt(31, a�oIN);
			sent.setInt(32, 0);
			sent.setInt(33, mes);

			res = sent.executeQuery();
			long acumulado = 0;
			long gastos = 0;
			while (res.next()) {
				// String aux = res.getString(1);
				ItemPOJO ss = new ItemPOJO();
				ss.setCoditem(res.getString(1)); // 
				ss.setItepre(res.getString(4));
				ss.setSaldo(res.getLong(3) + ss.getSaldo());
				// saldos = saldos + ss.getAcumum();
				ss.setNomite(res.getString(2));
				saldos.add(ss);
			}

			/*
			 * sent = cone.prepareStatement("TRUNCATE TABLE
			 * TMP_ORGANIZACIONES"); // sent.setString(1,rutnumdv);
			 * sent.executeUpdate();
			 */

			res.close();
			sent.close();
			// cone.close();
		} catch (SQLException e) {
			System.out.println("Error sipB : " + e.getMessage());
		}

		return saldos;
	}

	public Collection<ItemPOJO> getCuentasorganizacion(String org) {

		ConexionAs400ValePago con = new ConexionAs400ValePago();
		List<ItemPOJO> lista = new ArrayList<ItemPOJO>();
		List<movimientoPOJO> lista_tempo = new ArrayList<movimientoPOJO>();

		try {
			String sql = ""
					+ " SELECT CUEBAN,DESCUE"
					+ " FROM USMMBP.PRESF202"
					+ " WHERE CODGRU = ? and NOT CUEBAN IN ('1','2','3','4','5','6','7') ";

			PreparedStatement sent = con.getConnection().prepareStatement(sql);
			sent.setString(1, "2");
			ResultSet res = sent.executeQuery();
			String cuentas = "";
			Boolean primeravez = true;
			while (res.next()) {
				movimientoPOJO ss = new movimientoPOJO();
				ss.setCuenta(res.getString(1).trim());
				ss.setDetalle_cuenta(res.getString(2).trim()); //										
				lista_tempo.add(ss);
			}

			// lista = getSaldos(anno, cuentas,
			// org.trim(),con.getConexion());
			// con un for recorrer lista_tempo y agregar los datos + el
			// saldo de busca saldo
			ModuloValePagoB mod = new ModuloValePagoB();
			for (movimientoPOJO str : lista_tempo) {

				ItemPOJO ss = new ItemPOJO();
				ss.setCoditem(str.getCuenta());
				ss.setNomite(str.getDetalle_cuenta());
				long saldo = mod.getBuscaSaldo(org, str.getCuenta());
				ss.setSaldo(saldo);
				lista.add(ss);
			}
			// lista = getBuscaSaldo() ;

			// res.close();
			res.close();
			sent.close();
			con.getConnection().close();
		} catch (SQLException e) {
			System.out
					.println("Error ModuloReitemizacionB.getCuentasorganizacion: "
							+ e.getMessage());
		}

		return lista;
	}

	public Collection<ItemPOJO> getCuentasorganizacion_Respaldo(String org,
			int anno) {

		ConexionBanner con = new ConexionBanner();
		PreparedStatement sent = null;
		ResultSet res = null;

		List<ItemPOJO> lista = new ArrayList<ItemPOJO>();
		List<movimientoPOJO> lista_tempo = new ArrayList<movimientoPOJO>();

		try {
			String sql = ""
					+ " select distinct fg.FGBOPAL_ACCT_CODE , ft.FTVACCT_TITLE"
					+ " from    FGBOPAL fg"
					+ " INNER JOIN FTVACCT ft ON FT.FTVACCT_ACCT_CODE= FG.FGBOPAL_ACCT_CODE "
					+ "where fg.FGBOPAL_ORGN_CODE = '" + org
					+ "' order by FGBOPAL_ACCT_CODE ";
			System.out.println("Cuentas lista " + sql);
			sent = con.conexion().prepareStatement(sql);
			// sent.setString(1,rutnumdv);
			res = sent.executeQuery();
			String cuentas = "";
			Boolean primeravez = true;
			while (res.next()) {
				movimientoPOJO ss = new movimientoPOJO();
				// String aux = res.getString(1);
				/*
				 * ss.setCoditem(res.getString(1));
				 * ss.setItepre(res.getString(1));
				 * ss.setNomite(res.getString(2)); //
				 */
				ss.setCuenta(res.getString(1));
				ss.setCuenta(res.getString(1));
				ss.setDetalle_cuenta(res.getString(2)); //
				if (primeravez) {
					cuentas = "'" + ss.getCuenta() + "'";
					primeravez = false;
				} else
					cuentas = cuentas + "," + "'" + ss.getCuenta() + "'";

				lista_tempo.add(ss);
			}

			// lista = getSaldos(anno, cuentas, org.trim(),con.getConexion());

			// esta mal, devuelve un solo dato y deberia devolver la lista de
			// las cuentas. Devuelve la orgn

			// res.close();
			sent.close();
			con.close();
		} catch (SQLException e) {
			System.out
					.println("Error ModuloReitemizacionB.getCuentasorganizacion: "
							+ e.getMessage());
		}

		return lista;
	}

	public boolean getActualizaReitemizacion(HttpServletRequest req,
			int rutide, String digide, int numDoc, String orgn, String glosa1,
			String glosa2, String glosa3, String glosa4, String glosa5,
			String glosa6) {
		boolean registra = false;
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		listCocow36DTO = getCreaCocow36(req, rutide, digide, orgn, glosa1,
				glosa2, glosa3, glosa4, glosa5, glosa6);
		Ingreso_Documento ingresoDocumento = new Ingreso_Documento("AMI",
				rutide, digide);
		ingresoDocumento.setNumdoc(numDoc);

		registra = ingresoDocumento.Actualizar_Documento(listCocow36DTO);

		return registra;
	}

	synchronized public int getRegistraReitemizacion(HttpServletRequest req,
			int rutide, String digide, String orgn, String glosa1,
			String glosa2, String glosa3, String glosa4, String glosa5,
			String glosa6) {
		int numVale = 0;
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		listCocow36DTO = getCreaCocow36(req, rutide, digide, orgn, glosa1,
				glosa2, glosa3, glosa4, glosa5, glosa6);
		Ingreso_Documento ingresoDocumento = new Ingreso_Documento("GSI",
				rutide, digide);

		numVale = ingresoDocumento.Ingresar_Documento(listCocow36DTO);

		return numVale;
	}

	public List<Cocow36DTO> getCreaCocow36(HttpServletRequest req, int rutide,
			String digide, String orgn, String glosa1, String glosa2,
			String glosa3, String glosa4, String glosa5, String glosa6) {
		List<movimientoPOJO> listaMovimientos = (List<movimientoPOJO>) req
				.getSession().getAttribute("listaActualizada");

		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();

		String tiping = "GSI";
		String nomcam = "";
		long valnu1 = 0;
		long valnu2 = 0;
		String valalf = "";

		/* prepara archivo para grabar, llenar listCocow36DTO */
		nomcam = "CODORG";
		valnu1 = 0;
		valnu2 = 0;
		valalf = orgn;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tiping, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "GLOSA1";
		valnu1 = 0;
		valnu2 = 0;
		valalf = glosa1;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tiping, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "GLOSA2";
		valnu1 = 0;
		valnu2 = 0;
		valalf = glosa2;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tiping, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "GLOSA3";
		valnu1 = 0;
		valnu2 = 0;
		valalf = glosa3;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tiping, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "GLOSA4";
		valnu1 = 0;
		valnu2 = 0;
		valalf = glosa4;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tiping, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "GLOSA5";
		valnu1 = 0;
		valnu2 = 0;
		valalf = glosa5;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tiping, nomcam, valnu1,
				valnu2, valalf);

		nomcam = "GLOSA6";
		valnu1 = 0;
		valnu2 = 0;
		valalf = glosa6;
		listCocow36DTO = getAgregaLista(listCocow36DTO, tiping, nomcam, valnu1,
				valnu2, valalf);

		// MAA cambio en valalf
		for (movimientoPOJO ss : listaMovimientos) {
			nomcam = "CUENTA";
			valnu1 = ss.getCargo();
			valnu2 = ss.getAbono();
			valalf = ss.getCuenta();
			listCocow36DTO = getAgregaLista(listCocow36DTO, tiping, nomcam,
					valnu1, valnu2, valalf);
		}

		// Se agrega RUTUSU N�mero del mes
		Calendar fecha = new GregorianCalendar();
		int mes = fecha.get(Calendar.MONTH) + 1;
		nomcam = "RUTUSU";
		valnu1 = 0;
		valnu2 = 0;
		valalf = Integer.toString(mes);
		listCocow36DTO = getAgregaLista(listCocow36DTO, tiping, nomcam, valnu1,
				valnu2, valalf);

		/*
		 * if(listaMovimientos != null && listaMovimientos.size() > 0){ for
		 * (Presw18DTO ss : listaSolicitudesPago){ md5 = new MD5(); texto =
		 * md5.getMD5("0" + "" + ss.getDesuni().trim() + ss.getUsadom()) ;
		 * 
		 * nomcam = "NOMBEN"; valnu1 = ss.getUsadom(); valnu2 = ss.getRutide();
		 * valalf = ss.getDesuni(); // agregado 10/05/2013 para indicar el rut
		 * del beneficiario, adem�s del nombre Cocow36DTO cocow36DTO = new
		 * Cocow36DTO(); cocow36DTO.setTiping(tiping);
		 * cocow36DTO.setNomcam(nomcam); cocow36DTO.setValnu1(valnu1);
		 * cocow36DTO.setValnu2(valnu2); cocow36DTO.setValalf(valalf);
		 * cocow36DTO.setAccion(ss.getIddigi()); cocow36DTO.setResval(texto);
		 * listCocow36DTO.add(cocow36DTO); // listCocow36DTO =
		 * getAgregaListaMD5(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2
		 * ,valalf,texto ); } }
		 */
		return listCocow36DTO;
	}

	public List<fgbjvcdPOJO> getDatosOrgn(String orgn) {
		List<fgbjvcdPOJO> datos = new ArrayList<fgbjvcdPOJO>();
		try {

			ConexionBanner con = new ConexionBanner();
			PreparedStatement sent = null;
			ResultSet res = null;
			queryPOJO sql = new queryPOJO();
			String query = sql.getQueryFTVACCI();
			System.out.println("Query " + query);
			sent = con.conexion().prepareStatement(query);
			sent.setString(1, orgn);
			res = sent.executeQuery();
			Boolean tiene_datos = false;
			while (res.next()) {
				tiene_datos = true;
				fgbjvcdPOJO ss = new fgbjvcdPOJO();
				ss.setFGBJVCD_FUND_CODE(res.getString(1));
				ss.setFGBJVCD_ACCI_CODE(res.getString(2));
				ss.setFGBJVCD_ACCT_CODE(res.getString(3));
				ss.setFGBJVCD_PROG_CODE(res.getString(4));
				datos.add(ss);
			}

			if (tiene_datos == false) {
				fgbjvcdPOJO ss = new fgbjvcdPOJO();
				ss.setFGBJVCD_FUND_CODE("");
				ss.setFGBJVCD_ACCI_CODE("");
				ss.setFGBJVCD_ACCT_CODE("");
				ss.setFGBJVCD_PROG_CODE("");
				datos.add(ss);
			}
			res.close();
			sent.close();
			con.close();
		} catch (Exception ex) {
			System.out
					.println("Error en moduloReitemizacion.saveReitemizacionBanner  : "
							+ ex.getMessage());

		}
		return datos;
	}

	public Boolean saveReitemizacionBanner(List<fgbjvcdPOJO> lista) {
		Boolean resultado = false;
		try {
			ConexionBanner con = new ConexionBanner();
			PreparedStatement sent = null;
			int secuencia = 1;
			queryPOJO sql = new queryPOJO();
			String query = sql.getQueryBanner();
			con.getConexion().setAutoCommit(false);
			// for
			Boolean correcto = true;
			if (lista != null && lista.size() > 0) {

				for (fgbjvcdPOJO ss : lista) {
					String test = ss.getFGBJVCD_ACCI_CODE()
							.replace("^\\s*", "");
					System.out.println(ss.getFGBJVCD_SEQ_NUM());
					System.out.println(ss.getFGBJVCD_RUCL_CODE().trim());
					System.out.println(ss.getFGBJVCD_COAS_CODE().trim());
					System.out.println(ss.getFGBJVCD_ACCI_CODE().trim());
					System.out.println(ss.getFGBJVCD_FUND_CODE().trim());
					System.out.println(ss.getFGBJVCD_ORGN_CODE().trim());
					System.out.println(ss.getFGBJVCD_ACCT_CODE().trim());
					System.out.println(ss.getFGBJVCD_PROG_CODE().trim());
					System.out.println(ss.getFGBJVCD_TRANS_AMT());
					System.out.println(ss.getFGBJVCD_DR_CR_IND().trim());
					System.out.println(ss.getFGBJVCD_TRANS_DESC().trim());
					System.out.println(ss.getFGBJVCD_DOC_REF_NUM().trim());
					System.out.println(ss.getFGBJVCD_BUDGET_PERIOD().trim());

					sent = con.getConexion().prepareStatement(query);
					sent.setInt(1, ss.getFGBJVCD_SEQ_NUM());
					sent.setString(2, ss.getFGBJVCD_RUCL_CODE().trim());
					sent.setString(3, ss.getFGBJVCD_COAS_CODE().trim());
					sent.setString(4, ss.getFGBJVCD_ACCI_CODE().trim());
					sent.setString(5, ss.getFGBJVCD_FUND_CODE().trim());
					sent.setString(6, ss.getFGBJVCD_ORGN_CODE().trim());
					sent.setString(7, ss.getFGBJVCD_ACCT_CODE().trim());
					sent.setString(8, ss.getFGBJVCD_PROG_CODE().trim());
					sent.setLong(9, ss.getFGBJVCD_TRANS_AMT());
					sent.setString(10, ss.getFGBJVCD_DR_CR_IND().trim());
					sent.setString(11, ss.getFGBJVCD_TRANS_DESC().trim());
					sent.setString(12, ss.getFGBJVCD_DOC_REF_NUM().trim());
					sent.setString(13, ss.getFGBJVCD_BUDGET_PERIOD().trim());

					int res = sent.executeUpdate();
					if (res < 0) {
						correcto = false;
						break;
					}
				}
			} else {
				correcto = false;
			}
			// fin for
			if (correcto)
				con.getConexion().commit();

			sent.close();
			con.close();
		} catch (Exception ex) {
			System.out
					.println("Error en moduloReitemizacion.saveReitemizacionBanner  : "
							+ ex.getMessage());
			resultado = false;
		}
		return resultado;
	}

	synchronized public int saveActualizaReitemizacion(String accion, int doc,
			int fecha, int hora, int rut, String dv) {
		int error = 0;
		ConexionAs400ValePago con2 = new ConexionAs400ValePago();
		// en caso de aprobaci�n, se debe actualizar PRESF113B
		int res2 = 0;
		try {
			PreparedStatement sent2 = con2.getConnection().prepareStatement(
					" UPDATE USMMBP.PRESF113B SET ESTREI = ? "
							+ " WHERE NUMREI = ? ");
			sent2.setString(1, accion);
			sent2.setInt(2, doc);

			res2 = sent2.executeUpdate();
			if (res2 == 0) {
				error = -2;
			} else if (res2 < 0) {
				error = -1;
			}
			sent2.close();
			// con.close();
			con2.getConnection().close();
		} catch (SQLException ex) {
			System.out
					.println("Error en moduloReitemizacion.saveActualizaReitemizacion  : "
							+ ex.getMessage());
			error = -4;
		}

		if ((error == 0) && (res2 > 0)) {
			try {
				PreparedStatement sent2 = con2.getConnection()
						.prepareStatement(
								" INSERT INTO USMMBP.PRESF115B (" + " NUMREI, "
										+ " FECOPE ," + " HOROPE ,"
										+ " TIPOPE ," + " RUTIDE ,"
										+ " DIGIDE ," + " COMENT )"
										+ " VALUES(?,?,?,?,?,?,?)");

				sent2.setInt(1, doc);
				sent2.setInt(2, fecha);
				sent2.setInt(3, hora);
				sent2.setString(4, accion);
				// sent2.setInt(5,
				// Integer.parseInt(rutDvDAF.substring(0,rutDvDAF.length()-1)));
				sent2.setInt(5, rut);
				// sent2.setString(6, rutDvDAF.substring(rutDvDAF.length()-1));
				sent2.setString(6, dv);
				sent2.setString(7, " ");

				int res = sent2.executeUpdate();
				if (res < 0) {
					error = -2;
				}
			} catch (SQLException e) {
				System.out
						.println("Error en moduloValePagoB.saveCuentasPorPagar COCOF102B graba estado de sobregiro : "
								+ e);
				error = -3;

			}
		}

		return error;
	}

	public List<Cocow36DTO> getAgregaLista(List<Cocow36DTO> lista,
			String tiping, String nomcam, long valnu1, long valnu2,
			String valalf) {
		Cocow36DTO cocow36DTO = new Cocow36DTO();
		cocow36DTO.setTiping(tiping);
		cocow36DTO.setNomcam(nomcam);
		cocow36DTO.setValnu1(valnu1);
		cocow36DTO.setValnu2(valnu2);
		cocow36DTO.setValalf(valalf);
		lista.add(cocow36DTO);
		return lista;

	}
}
