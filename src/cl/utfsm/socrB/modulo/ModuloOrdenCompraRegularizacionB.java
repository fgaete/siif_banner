package cl.utfsm.socrB.modulo;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.MathTool;

import cl.utfsm.POJO.Sede;
import cl.utfsm.POJO.Producto;
import cl.utfsm.base.util.ConexionBanner;
import cl.utfsm.base.util.Util;
import cl.utfsm.conexion.ConexionAs400OrdPago;
import cl.utfsm.socrB.datos.OrdenCompraRegularizacionDao;
import descad.cliente.Ingreso_Documento;
import descad.cliente.Ingreso_Documentos;
import descad.cliente.MD5;
import descad.cliente.PreswBean;
import descad.documentos.Cocow36DTO;
import descad.presupuesto.Presw18DTO;
import descad.presupuesto.Presw25DTO;

public class ModuloOrdenCompraRegularizacionB {
     OrdenCompraRegularizacionDao ordenCompraRegularizacionDao;

	public OrdenCompraRegularizacionDao getOrdenCompraRegularizacionDao() {
		return ordenCompraRegularizacionDao;
	}

	public void setOrdenCompraRegularizacionDao(
			OrdenCompraRegularizacionDao ordenCompraRegularizacionDao) {
		this.ordenCompraRegularizacionDao = ordenCompraRegularizacionDao;
	}
	
	public String formateoNumeroConDecimales(Double numero){
		/*formatear n�mero */
	     Locale l = new Locale("es","CL");
	     DecimalFormat formatter2 = (DecimalFormat)DecimalFormat.getInstance(l);
	     formatter2.applyPattern("###,###.###");

	   //  formatter2.format(rs.getDouble("UTMSUS"))   /*con decimales*/
	     return formatter2.format(numero);
	}
 public List<String> getCampus(){
		 ConexionBanner con = new ConexionBanner();	
		 List<String> listacampus = new ArrayList<String>();
		 String query = "";
		
		 try{
			 query = " SELECT DISTINCT FTVSHIP_ADDR_LINE2" +
			 		 " FROM FTVSHIP " +
			         " WHERE FTVSHIP_ADDR_LINE2 IS NOT NULL " +
                     " ORDER BY FTVSHIP_ADDR_LINE2";  
			 
		// System.out.println("query " + query);
			 Statement sent = con.conexion().createStatement();
			 ResultSet res  = sent.executeQuery(query);
			 
	         while (res.next()){
	        	 listacampus.add(res.getString(1)); // campus
			 }
			 res.close();
			 sent.close(); 
			 con.close();
		 }
		catch (SQLException e){
		   	System.out.println("Error moduloOrdenCompraCentralizadaB.getCampus: " + e);
		} 
    	 return listacampus;
	 } 
	
	 public List<String> getEdificio(String campus){
		 ConexionBanner con = new ConexionBanner();	
		 List<String> lista = new ArrayList<String>();
		 String query = "";
		
		 try{
			 query = " SELECT DISTINCT(FTVSHIP_BUILDING)" +
			 		 " FROM FTVSHIP" +
			 		 " WHERE FTVSHIP_ADDR_LINE2 like '" + campus + "'" +
			 		 " ORDER BY FTVSHIP_BUILDING"; 
			 
			// System.out.println("query " + query);
			 Statement sent = con.conexion().createStatement();
			 ResultSet res  = sent.executeQuery(query);
			 
	         while (res.next()){
			   	lista.add(res.getString(1)); // EDIFICIO
			 }
			 res.close();
			 sent.close(); 
			 con.close();
		 }
		catch (SQLException e){
		   	System.out.println("Error moduloOrdenCompraCentralizadaB.getEdificio: " + e);
		} 
    	 return lista;
	 } 
	 public List<String> getPiso(String campus, String edificio){
		 ConexionBanner con = new ConexionBanner();	
		 List<String> lista = new ArrayList<String>();
		 String query = "";
		
		 try{
			 query = " SELECT DISTINCT(FTVSHIP_FLOOR)" +
			 		 " FROM FTVSHIP" +
			 		 " WHERE FTVSHIP_ADDR_LINE2 like '" + campus + 
			 		 "' AND FTVSHIP_BUILDING like '" + edificio +"'" +
			 		 " ORDER BY FTVSHIP_FLOOR";  //piso
			 
			// System.out.println("query " + query);
			 Statement sent = con.conexion().createStatement();
			 ResultSet res  = sent.executeQuery(query);
			 
	         while (res.next()){
			   	lista.add(res.getString(1)); // piso
			 }
			 res.close();
			 sent.close(); 
			 con.close();
		 }
		catch (SQLException e){
		   	System.out.println("Error moduloOrdenCompraCentralizadaB.getPiso: " + e);
		} 
    	 return lista;
	 } 
	 public List<String> getOficina(String campus, String edificio, String piso){
		 ConexionBanner con = new ConexionBanner();	
		 List<String> lista = new ArrayList<String>();
		 String query = "";
		
		 try{
			 query = " SELECT DISTINCT(FTVSHIP_CONTACT)" +
			 		 " FROM FTVSHIP" +
			 		 " WHERE FTVSHIP_ADDR_LINE2 like '" + campus + "'" +
			 		 " AND FTVSHIP_BUILDING like '" + edificio +"'" +
			 		 " AND FTVSHIP_FLOOR like '" + piso +"'" +
			 		 " ORDER BY FTVSHIP_CONTACT";   

			 
			// System.out.println("query " + query);
			 Statement sent = con.conexion().createStatement();
			 ResultSet res  = sent.executeQuery(query);
			 
	         while (res.next()){
			   	lista.add(res.getString(1)); // getOficina
			 }
			 res.close();
			 sent.close(); 
			 con.close();
		 }
		catch (SQLException e){
		   	System.out.println("Error moduloOrdenCompraCentralizadaB.getOficina: " + e);
		} 
    	 return lista;
	 } 
	 
	 public List<String> getCodigoEnvio(String campus, String edificio, String piso, String oficina){
		 ConexionBanner con = new ConexionBanner();	
		 List<String> lista = new ArrayList<String>();
		 String query = "";
		
		 try{
			 query = " SELECT FTVSHIP_CODE, FTVSHIP_CONTACT " +
			 		 " FROM FTVSHIP" +
			 		 " WHERE FTVSHIP_ADDR_LINE2 like '" + campus + "'" +
			 		 " AND FTVSHIP_BUILDING like '" + edificio +"'" +
			 		 " AND FTVSHIP_FLOOR like '" + piso +"'" +  
			 		 " AND FTVSHIP_CONTACT like '" + oficina +"'" +
			 		 " AND FTVSHIP_TERM_DATE is null";
			 
			// System.out.println("query " + query);
			 Statement sent = con.conexion().createStatement();
			 ResultSet res  = sent.executeQuery(query);
			 
	         while (res.next()){
			   	lista.add(res.getString(1)); // codigoEnvio
			   	lista.add(res.getString(2)); // detalle
			 }
			 res.close();
			 sent.close(); 
			 con.close();
		 }
		catch (SQLException e){
		   	System.out.println("Error moduloOrdenCompraCentralizadaB.getOficina: " + e);
		} 
    	 return lista;
	 } 
	 
	 public Map<String, String> getDivisas(){  
		    ConexionBanner con = new ConexionBanner();
		    PreparedStatement sent = null;
		    ResultSet res = null;
		    Map<String, String> lista = new HashMap<String, String>();
		    try{
		      
		      sent = con.conexion().prepareStatement("SELECT gtvcurr_curr_code, gtvcurr_title FROM GTVCURR");
		      res = sent.executeQuery();

		      while (res.next()){
		    	lista.put(res.getString(1), res.getString(2));  
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error Error moduloOrdenCompraRegularizacionB.getDivisas: " + e);
			}
		 return lista;
} 
	 
	 public List<Producto> getProductos(){  
		    ConexionBanner con = new ConexionBanner();
		    PreparedStatement sent = null;
		    ResultSet res = null;
		    List<Producto> lista = new ArrayList<Producto>();
		    String query = "SELECT ftvcomm_code, ftvcomm_desc FROM FTVCOMM WHERE SUBSTR(ftvcomm_code,1,1) =  'C'  and ftvcomm_desc <> '*' ORDER BY ftvcomm_desc";
			
		    try{
		      
		    	 sent = con.conexion().prepareStatement(query);
			     res = sent.executeQuery();

		      while (res.next()){
		    	  Producto list = new Producto();
		    	  list.setCodigo(res.getString(1));
		    	  list.setDescripcion(res.getString(2));
		    	  lista.add(list);
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error Error moduloOrdenCompraRegularizacionB.getProductos: " + e);
			}
		 return lista;
  }  
	 
	 public List<String> getDetalleProducto(String producto, String obsequio){
		 ConexionBanner con = new ConexionBanner();	
		 List<String> lista = new ArrayList<String>();
		 try{
		      		      
		      PreparedStatement sent = con.conexion().prepareStatement(" SELECT ftvcomm_desc, NVL(ftvcomm_uoms_code,'Sin informaci�n'), " +
		    		                                                   " NVL(ftvcomm_tgrp_code,' '), NVL(ftrcomm_acct_code,'Bien sin cuenta asociada'), ftvacct_title  " + 
                                                                       " FROM FTVCOMM LEFT JOIN FTRCOMM ON ftvcomm_code = ftrcomm_comm_code" +
                                                                       " AND ftrcomm_coas_code = 'S'  " +
                                                                       " LEFT JOIN FTVACCT ON ftrcomm_acct_code = ftvacct_acct_code " +
                                                                       " AND ftvacct_coas_code = 'S' " +
                                                                       " WHERE ftvcomm_code = ? ");
		      sent.setString(1,producto.trim());
		      ResultSet res = sent.executeQuery();

		      if (res.next()){
		        lista.add(res.getString(1)); // detalle
		        lista.add(res.getString(2)); // unidad de medidad
		        lista.add(res.getString(3)); // impuesto
		        if(obsequio.trim().equals("S")){
		        	lista.add("6BF001"); // cuenta asociada al bien
			        lista.add("PREMIOS-REGALOS"); // Nombre cuenta asociada al bien
		        } else {
			        lista.add(res.getString(4)); // cuenta asociada al bien
			        lista.add(res.getString(5)); // Nombre cuenta asociada al bien
		        }
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloOrdenCompraRegularizacionB.getDetalleProducto: " + e);
			}
    	 return lista;
	 } 
	 
	 public String getDetalleCodigoEntrega(String codigo){
		 ConexionBanner con = new ConexionBanner();	
		 String lug = "";
		 try{
		      		      
		      PreparedStatement sent = con.conexion().prepareStatement(" SELECT DISTINCT FTVSHIP_ADDR_LINE2 || '-' || " +
		    		                                                   " FTVSHIP_BUILDING || '-' || FTVSHIP_FLOOR || '-' || " +
		    		                                                   " FTVSHIP_CONTACT " + 
                                                                       " FROM FTVSHIP " +
			 		                                                   " WHERE FTVSHIP_CODE = ? ");
		      sent.setString(1,codigo.trim());
		      ResultSet res = sent.executeQuery();

		      if (res.next()){
		        lug = res.getString(1); // detalle de entrega
		       }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloOrdenCompraRegularizacionB.DetalleCodigoEntrega: " + e);
			}
    	 return lug;
	 } 
	 
	 public String getVerificaRut(String rutnumdv){
		   // System.out.println(rutnumdv);
		    ConexionBanner con = new ConexionBanner();
		    String nombre ="";
		    
		    String rut = rutnumdv.substring(0,rutnumdv.length()-1);
		    String dig = rutnumdv.substring(rutnumdv.length()-1,rutnumdv.length());
		    if (dig.equals("k")) rutnumdv = rut+"K";
		    
		    try{
		      
		      PreparedStatement sent = con.conexion().prepareStatement("SELECT spbpers_legal_name FROM spriden, spbpers " +
		    		                                                   "WHERE spriden_id = ? " +
		    		                                                   "AND spriden_pidm = spbpers_pidm " +
		    		                                                   "AND spriden_change_ind is null");
		      sent.setString(1,rutnumdv);
		      ResultSet res = sent.executeQuery();

		      if (res.next()){
		        nombre = res.getString(1); // nombre mas apellidos
		      }
		      res.close();
		      sent.close();
		      con.close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloOrdenCompraRegularizacionB.getVerificaRut: " + e);
			}
		 return nombre;
	 }
	 
	 public List<Cocow36DTO> getCreaCocow36(HttpServletRequest req, int rutide, String digide, String tipoProceso){
		List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>) req.getSession().getAttribute("totalListaOrganizaciones");
		List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) req.getSession().getAttribute("totalListaProductos");
		long fechaEntrega = Util.validaParametro(req.getParameter("fechaEntrega"),0);
		String codigoEntrega = Util.validaParametro(req.getParameter("codigoEntrega"),"");
		String importacion = Util.validaParametro(req.getParameter("importacion"),"");
		String divisa = Util.validaParametro(req.getParameter("divisa"),"");
		int rutProveedor = Util.validaParametro(req.getParameter("rutProv"),0);
		String dvProveedor = Util.validaParametro(req.getParameter("dvProv"),"");
		int numeroDoc = Util.validaParametro(req.getParameter("numeroDoc"),0);
		String tipoDoc = Util.validaParametro(req.getParameter("tipoDoc"),"");
		long fechaDoc = Util.validaParametro(req.getParameter("fechaDoc"),0);
		String motivoReg = Util.validaParametro(req.getParameter("motivoReg"),"");
		String otroMotReg = Util.validaParametro(req.getParameter("otroMotReg"),"");
		
		List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
		
		String tiping = tipoProceso;
		String nomcam = "";
		long valnu1 = 0;
		long valnu2 = 0;
		long valorExento = 0;
		long totalOrden  = 0;
		long totalImpuestos = 0;
		String valalf = "";
		
		for (Presw25DTO ss : totalListaProductos){                              // Impuestos
			totalOrden = totalOrden + (ss.getPres02() * ss.getPres01()) + ss.getPres03();
			totalImpuestos = totalImpuestos + ss.getPres03();
		}

	    /* prepara archivo para grabar, llenar listCocow36DTO*/
		nomcam = "FECENT";
	    valnu1 = fechaEntrega;
	    valnu2 = 0;
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "LUGENT";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = codigoEntrega;
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "INDIMP";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = importacion;
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "CODDIV";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = divisa;
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "VALIMP";
	    valnu1 = totalImpuestos;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "VALTOT";
	    valnu1 = totalOrden;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "RUTIDE";
	    valnu1 = rutProveedor;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "DIGIDE";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = dvProveedor;
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "NUMDOC";
	    valnu1 = numeroDoc;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

	    nomcam = "TIPDOC";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = tipoDoc;
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );

	    nomcam = "FECDOC";
	    valnu1 = fechaDoc;
	    valnu2 = 0;
	    valalf = "";
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    nomcam = "MOTCOM";
	    valnu1 = 0;
	    valnu2 = 0;
	    valalf = (motivoReg.equals("")?otroMotReg.trim():motivoReg.trim());
	    listCocow36DTO = this.getAgregaLista(listCocow36DTO,tiping ,nomcam ,valnu1 ,valnu2 ,valalf );
	    
	    return listCocow36DTO;
		}
	 
	 
	 public List<Cocow36DTO> getAgregaLista(List<Cocow36DTO> lista, String tiping, String nomcam, long valnu1, long valnu2, String valalf){
		 Cocow36DTO cocow36DTO = new Cocow36DTO();
		 cocow36DTO.setTiping(tiping);
		 cocow36DTO.setNomcam(nomcam);
		 cocow36DTO.setValnu1(valnu1);
		 cocow36DTO.setValnu2(valnu2);
		 cocow36DTO.setValalf(valalf);
		 lista.add(cocow36DTO);
		 return lista;
		 
	 }
	 
	  public boolean saveEstadoCuenta(int numdoc, String codOrg, int rutUsuario, String digide, String estado){
			PreswBean preswbean = new PreswBean("AU2",0,0,0,0, rutUsuario);
			preswbean.setDigide(digide);
			preswbean.setNumdoc(numdoc);
			preswbean.setCajero(codOrg);
			preswbean.setTipcue(estado);
		return preswbean.ingreso_presw19();
		}
     
	  public long getBuscaSaldo(String codOrg, String codCuenta){
		  ConexionBanner con = new ConexionBanner();
		  long saldoCuenta = 0;
		  try{
		   /* PreparedStatement sent = con.conexion().prepareStatement("SELECT (fgbbavl_sum_adopt_bud+(fgbbavl_sum_bud_adjt-fgbbavl_sum_ytd_actv-fgbbavl_sum_encumb-fgbbavl_sum_bud_rsrv)) " +
		    		                                                 "FROM fgbbavl t1 " +
	                                                                 "WHERE t1.fgbbavl_coas_code = 'S' " + 
	                                                                 "AND t1.fgbbavl_orgn_code =  ? " +
	                                                                 "AND t1.fgbbavl_acct_code = ? " +
	                                                                 "AND t1.fgbbavl_activity_date = (SELECT MAX(t2.fgbbavl_activity_date) " + 
	                                                                 "FROM fgbbavl t2 " +
	                                                                 "WHERE t1.fgbbavl_coas_code = t2.fgbbavl_coas_code " +
	                                                                 "AND t1.fgbbavl_orgn_code = t2.fgbbavl_orgn_code " +
	                                                                 "AND t1.fgbbavl_acct_code = t2.fgbbavl_acct_code)");*/
			  
			  String mesActual = this.getMesActual();
			  String a�oActual = this.getAnioActual();
			  String query =   "SELECT sum(fgbbavl_sum_adopt_bud+(fgbbavl_sum_bud_adjt-fgbbavl_sum_ytd_actv-fgbbavl_sum_encumb-fgbbavl_sum_bud_rsrv)) " +
              " FROM fgbbavl t1 " +
              " WHERE t1.fgbbavl_coas_code = 'S'" + 
              " AND t1.fgbbavl_orgn_code =  ? " +
              " AND t1.fgbbavl_acct_code = ? " +
              " AND FGBBAVL_PERIOD <= '" + mesActual+ "'" +
              " AND FGBBAVL_FSYR_CODE = '" + a�oActual+ "'" +
              " ORDER BY FGBBAVL_PERIOD";
			PreparedStatement sent = con.conexion().prepareStatement(query);  
		    sent.setString(1,codOrg);
		    sent.setString(2,codCuenta);
		    ResultSet res = sent.executeQuery();

		     while (res.next()){
		    	saldoCuenta = res.getLong(1); // Saldo
		     }
		     res.close();
		     sent.close(); 
		     con.close();
		     }
		    catch (SQLException e){
		    	System.out.println("Error moduloOrdenCompraRegularizacionB.getBuscaSaldo: " + e);
			} 
		 // para la prueba 
		   // saldoCuenta = 5;
		 return saldoCuenta;
	}
	  
	  public boolean saveRecepciona(int numdoc, int rutUsuario, String digide, String tipo){
			PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
			preswbean.setDigide(digide);
			preswbean.setNumdoc(numdoc);
			
		return preswbean.ingreso_presw19();
		}
	  
	  
	  public boolean saveEstadoOrdenCompra(int numdoc,int rutUsuario,String digide){
			PreswBean preswbean = new PreswBean("CER",0,0,0,0, rutUsuario);
			preswbean.setDigide(digide);
			preswbean.setNumdoc(numdoc);

		return preswbean.ingreso_presw19();
		}
	  public Vector getDatosCOCOF156B(int numero){
		    ConexionAs400OrdPago con = new ConexionAs400OrdPago();
		    Vector vec = new Vector();
		    try{
	
		     PreparedStatement sent = con.getConnection().prepareStatement(" SELECT v1.FECENT, v1.LUGENT, v1.CODDIV, v1.RUTIDE,  v1.DIGIDE, v2.CODORG " +
                                                                           " FROM USMMBP.COCOF156B v1, USMMBP.COCOF159B v2 " + 
                                                                           " WHERE v1.numreg = ? " + 
                                                                           " AND v1.estord = 'A' " + 
                                                                           " AND v1.numreg = v2.numreg " + 
                                                                           " AND v2.numseq = 1");	
		     sent.setInt(1,numero);
  	         ResultSet res = sent.executeQuery();

		     while (res.next()) { 
		       vec.add(res.getString(1).trim()); // fecha de entrega del producto
		       vec.add(res.getString(2).trim()); // lugar de entrega del producto;
			   vec.add(res.getString(3).trim()); // codigo de divisa del producto
			   vec.add(res.getString(4).trim()); // rut del proveedor del producto
			   vec.add(res.getString(5).trim()); // digito del proveedor producto 
			   vec.add(res.getString(6).trim()); // codigo de organizacion 
			 }
		     res.close();
		     sent.close();
		     // con.close();
		     con.getConnection().close();

		    }
		    catch (SQLException e ) {
		      System.out.println("Error en moduloOrdenCompraDecentralizadaB.getDatosCOCOF156B : " + e );
		    }
		    return vec;
   }

		  
	  public Vector getDatosProductos(int numero){
		    // ConexionBD con = new ConexionBD();
			 ConexionAs400OrdPago con = new ConexionAs400OrdPago();
		     Vector vec = new Vector();
		     Vector detalle = null;
		     
		     float porcentaje  = 0;
		     float valorFocpau = 0;
		     float valorNeto   = 0;
		     float valorTotal  = 0;
		     float valorImp    = 0;
		     float impFocpau   = 0;
			 try {
		    	PreparedStatement sent = con.getConnection().prepareStatement(" SELECT v1.NUMSEQ, v1.CODPRO, v1.CANPRO, v1.PREREG, " +
		    			                                                      " v1.IMPPRO, v2.codorg, v2.cueban, v3.codfon, v3.codpro, v2.valfin " +
                                                                            " FROM USMMBP.COCOF158B v1, USMMBP.COCOF159B v2, " +
                                                                            " USMMBP.PRESF200 v3 " +
                                                                            " WHERE v1.numreg = ? " +
                                                                            " AND v1.numreg = v2.numreg " +
                                                                            " AND v1.numseq = v2.numseq " +
                                                                            " AND v2.codorg = v3.codorg"); 
		        sent.setInt(1,numero);
			    ResultSet res = sent.executeQuery();

			    while (res.next()) {
			       valorNeto = res.getInt(3)*res.getFloat(4);
				   valorImp  = res.getFloat(5);
				   valorTotal = valorNeto + valorImp;
				   porcentaje = Math.round((100* res.getFloat(10))/valorTotal); // porcentaje ya asignado	
				   valorFocpau = (valorNeto*Math.round(porcentaje))/100;
                   impFocpau   = (valorImp*Math.round(porcentaje))/100;
			    	
			       detalle = new Vector();
			       detalle.add(res.getString(1).trim()); // numero de secuencia dep producto;
				   detalle.add(res.getString(2).trim()); // codigo del producto;
				   detalle.add(res.getString(3).trim()); // cantidad del producto
				   detalle.add(res.getString(4).trim()); // precio unitario del producto
				   detalle.add(res.getString(5).trim()); // impuesto pagado del producto 
				   detalle.add(res.getString(6).trim()); // codigo de organizacion 
				   detalle.add(res.getString(7).trim()); // codigo de cuenta
				   detalle.add(res.getString(8).trim()); // codigo de fondo;
				   detalle.add(res.getString(9).trim()); // codigo de programa
				   detalle.add(formateoNumeroConDecimales(Double.parseDouble(porcentaje+""))); // Porcentaje asignado
				   detalle.add(valorFocpau+""); // valor del focapau antes de aplicar impuesto
				   detalle.add(impFocpau+""); // valor del focapau antes de aplicar impuesto
				   vec.add(detalle);
			     }
			     res.close();
			     sent.close();
			     // con.close();
			     con.getConnection().close();

			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloOrdenCompraRegularizacion.getDatosProductos : " + e );
			    }
			    return vec;
		}
	  
	  public String getRutOperacion(int numero, String tipOpe){
		    //ConexionBD con = new ConexionBD();
		    ConexionAs400OrdPago con = new ConexionAs400OrdPago(); 
		    String rutDv = "";
		    try{
		      
		      PreparedStatement sent = con.getConnection().prepareStatement(" SELECT RUTIDE,DIGIDE FROM USMMBP.COCOF157B " +
	                 									    				" WHERE NUMREG = ? " +
	                 										    			" AND TIPOPE = ? "); 	
		    	
		      sent.setInt(1,numero);
		      sent.setString(2,tipOpe);
		      ResultSet res = sent.executeQuery();

		      while (res.next()){
		        rutDv = res.getInt(1)+""+res.getString(2); // rut operaci�n
		      }
		      res.close();
		      sent.close();
		     // con.close();
		      con.getConnection().close();
		    }
		    catch (SQLException e){
		    	System.out.println("Error moduloOrdenCompraRegularizacionB.getRutOperacion: " + e);
			}
		 return rutDv;
	}
	 
	  public int saveOrdenCompra(int rutUsuario, String dvUsuario,String numorden,String fechaEnt,String lugarEnt,String divisa, String proveedor, String organizacion,String datos,
			                     String ingresador, String autorizador){
		  ConexionBanner con = new ConexionBanner();
		  CallableStatement sent;
		  int error = -1;
		  String codigoBaner = "";
		  String rutdv = String.valueOf(rutUsuario)+dvUsuario;
		  System.out.println(rutdv+"-"+numorden+"-"+fechaEnt+"-"+lugarEnt+"-"+divisa+"-"+proveedor+"-"+organizacion+"-"+datos);
		  try{ 
		       sent = con.conexion().prepareCall("{call P_USM_OC_SIIF(?,?,?,?,?,?,?,?,?,?,?,?)}");
		       sent.setString(1,rutdv);
		       sent.setString(2,numorden);
		       sent.setString(3,fechaEnt);
		       sent.setString(4,lugarEnt);
		       sent.setString(5,divisa);
		       sent.setString(6,proveedor);
		       sent.setString(7,organizacion);
		       sent.setString(8,datos);
		       sent.registerOutParameter(9, Types.INTEGER);
		       sent.registerOutParameter(10, Types.VARCHAR);
		       sent.setString(11,ingresador);
		       sent.setString(12,autorizador);
		         
		       sent.executeQuery();
		       error = sent.getInt(9);
		       codigoBaner = sent.getString(10);
		      
		       sent.close(); 
		       con.close();
		     }
		    catch (SQLException e){
		    	System.out.println("Error moduloOrdenCompraRegularizacionB.saveOrdenCompra: " + e);
			} 
		    
		    if(error == 0) {
		         Calendar fecha = new GregorianCalendar();
			     int a�o = fecha.get(Calendar.YEAR);
			     int mes = fecha.get(Calendar.MONTH) + 1;
			     int dia = fecha.get(Calendar.DAY_OF_MONTH);
			     int hora = fecha.get(Calendar.HOUR_OF_DAY);
			     int minuto = fecha.get(Calendar.MINUTE);
			     int segundo = fecha.get(Calendar.SECOND);
			     String mestexto = (mes > 9)?String.valueOf(mes):"0"+String.valueOf(mes);
			     String diatexto = (dia > 9)?String.valueOf(dia):"0"+String.valueOf(dia);
			     String mintexto = (minuto > 9)?String.valueOf(minuto):"0"+String.valueOf(minuto);
			     String segtexto = (segundo > 9)?String.valueOf(segundo):"0"+String.valueOf(segundo); 
			     int fechaAc = Integer.parseInt(String.valueOf(a�o) + mestexto + diatexto);
			     int horaAc = Integer.parseInt(String.valueOf(hora) + mintexto + segtexto);
		         

		         ConexionAs400OrdPago con2 = new ConexionAs400OrdPago();
		         	         
		         try {
		              PreparedStatement sent2 = con2.getConnection().prepareStatement(" INSERT INTO USMMBP.COCOF157B (" +                                           
		            		                                                          " NUMREG, " +
		            		                                                          " FECOPE ," + 
		            		                                                          " HOROPE ," + 
		            		                                                          " TIPOPE ," + 
		            		                                                          " RUTIDE ," + 
		            		                                                          " DIGIDE ," + 
		            		                                                          " CODORG ," + 
		            		                                                          " COMENT )" +           
		              																  " VALUES(?,?,?,?,?,?,?,?)"); 
		            sent2.setString(1,numorden);
		            sent2.setInt(2, fechaAc);
		            sent2.setInt(3, horaAc);
		            sent2.setString(4, "G");
		            sent2.setInt(5, rutUsuario);
		            sent2.setString(6, dvUsuario);
		            sent2.setString(7, " ");
			        sent2.setString(8, codigoBaner);
		            
		    	    int res = sent2.executeUpdate();
	                if(res < 0)
	                 	error = -12;  
	                else {
	                	try {	
	                		sent2 = con2.getConnection().prepareStatement(" UPDATE USMMBP.COCOF156B SET ESTORD = ? "+
	                				                                      " WHERE NUMREG = ? " );
	                		sent2.setString(1,"G");
	                		sent2.setString(2,numorden);
	                		int res2 = sent2.executeUpdate();
	                		if(res2 < 0){
	                			error = -13;    	  
	                		}
	                      }
	                    catch (SQLException e ) {
	      	    	      System.out.println("Error en moduloOrdenCompraRegularizacionB.saveOrdenComprar COCOF156B : " + e );
	                      error = -6;  
	                        
	      	    	    } 
	                 }
	                
		    	     sent2.close();
		    	     // con.close();
		    	     con2.getConnection().close();

		    	    }
		    	    catch (SQLException e ) {
		    	      System.out.println("Error en moduloOrdenCompraRegularizacionB.saveOrdenCompra COCOF157B : " + e );
		    	    } 
		       }
		 return error;
	}
	  
	  public boolean getActualizaOrdenCompra(HttpServletRequest req, int rutide, String digide, int numDoc){
			boolean registra = false;
			List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>) req.getSession().getAttribute("totalListaOrganizaciones");
			List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) req.getSession().getAttribute("totalListaProductos");
			List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
			listCocow36DTO = this.getCreaCocow36(req, rutide, digide,"MOR");
			Ingreso_Documentos ingresoDocumentos = new Ingreso_Documentos("MOR",rutide,digide);
			ingresoDocumentos.setNumdoc(numDoc);
			
			for (Presw18DTO ss: totalListaOrganizaciones){
				ss.setTippro("MOR");
			}
			for (Presw25DTO ss: totalListaProductos) {
				ss.setTippro("MOR");
			}
				
			registra = ingresoDocumentos.Actualizar_Documentos(listCocow36DTO,totalListaProductos, totalListaOrganizaciones);
			
		
			return registra;
		}  
	  
	  public int getRegistraOrdenCompra(HttpServletRequest req, int rutide, String digide){
		    List<Presw18DTO> totalListaOrganizaciones = (List<Presw18DTO>) req.getSession().getAttribute("totalListaOrganizaciones");
			List<Presw25DTO> totalListaProductos = (List<Presw25DTO>) req.getSession().getAttribute("totalListaProductos");
		 	int numOrden = 0;
			List<Cocow36DTO> listCocow36DTO = new ArrayList<Cocow36DTO>();
			listCocow36DTO = this.getCreaCocow36(req, rutide, digide, "GRC");
			Ingreso_Documentos ingresoDocumentos =  new Ingreso_Documentos("GRC",rutide,digide);
			
			for (Presw18DTO ss: totalListaOrganizaciones){
				ss.setIddigi("");
				ss.setTippro("GRC");
			}
			for (Presw25DTO ss: totalListaProductos) {
				ss.setTippro("GRC");
			}
			
			
		    numOrden = ingresoDocumentos.Ingresar_Documento(listCocow36DTO, totalListaProductos, totalListaOrganizaciones);
			return numOrden;
	}
	  
	  public boolean saveRechaza(int numdoc, int rutUsuario, String digide, String tipo, String glosa, String tipCue, String codOrg){
			PreswBean preswbean = new PreswBean(tipo, 0,0,0,0, rutUsuario);
			preswbean.setNumdoc(numdoc);
			preswbean.setRutide(rutUsuario);
			preswbean.setDigide(digide);
		    preswbean.setCajero(codOrg);
		  //  preswbean.ingreso_presw19();
			
			List<Presw25DTO> lista = new ArrayList<Presw25DTO>();
			Presw25DTO presw25DTO = new Presw25DTO();
			presw25DTO.setPres01(numdoc);
			presw25DTO.setComen1(glosa);
			presw25DTO.setRutide(rutUsuario);
			presw25DTO.setDigide(digide);
			presw25DTO.setIndexi(tipCue);
			lista.add(presw25DTO);
		    return preswbean.ingreso_presw25((List<Presw25DTO> )lista);
		}  
	  
		public String formateoRut(long rut){
			String rutFormateado = "";
			if(rut > 0){
				rutFormateado = String.valueOf(rut);
				if(rutFormateado.length() == 7)
					rutFormateado = rutFormateado.substring(0,1) + "." + rutFormateado.substring(1, 4) + "." + rutFormateado.substring(4);
				else
					rutFormateado = rutFormateado.substring(0,2) + "." + rutFormateado.substring(2, 5) + "." + rutFormateado.substring(5);
				
			} 
				
			return rutFormateado;
		}
		
		 public boolean getEliminaOrdenCompra( int rut, String dv, int numdoc, String nomTipo, String tipCue ){
			  	PreswBean preswbean = null;
				Collection<Presw18DTO> lista = null;
				long saldoCuenta = 0;
				preswbean = new PreswBean(nomTipo,0,0,0,0, 0,0,"",0,"", "",0,0,0,0,"","");
				preswbean.setRutide(rut);
				preswbean.setDigide(dv);
				preswbean.setNumdoc(numdoc);
				preswbean.setTipcue(tipCue);
				
				return preswbean.ingreso_presw19();	
				
		 } 
		 
		 public List<Presw25DTO> getListaMotReg(){
				List<Presw25DTO> listaMotReg = new ArrayList<Presw25DTO>();
				Presw25DTO presw25DTO = new Presw25DTO();
				presw25DTO.setComen1("No es agil servicio de area de compras");
				presw25DTO.setComen2("No es &aacute;gil servicio de &aacute;rea de compras");
				listaMotReg.add(presw25DTO);
				presw25DTO = new Presw25DTO();
				presw25DTO.setComen1("Requeria productos con urgencia");
				presw25DTO.setComen2("Requer&iacute;a productos con urgencia");
				listaMotReg.add(presw25DTO);	
				presw25DTO = new Presw25DTO();
				presw25DTO.setComen1("Me resulta mas comodo comprar asi");
				presw25DTO.setComen2("Me resulta m&aacute;s c&oacute;modo comprar as&iacute");
				listaMotReg.add(presw25DTO);	
				presw25DTO = new Presw25DTO();
				presw25DTO.setComen1("Me entregan exactamente lo que requiero");
				presw25DTO.setComen2("Me entregan exactamente lo que requiero");
				listaMotReg.add(presw25DTO);	
				return listaMotReg;
			}
		 public String getMesActual(){
			 DateTool fechaActual = new DateTool();
			 String mesActual = "";
				
			 String mes = (fechaActual.getMonth()+1)+"";
			 if(mes.length()== 1)
						mes = "0" + mes;
			 mesActual = mes;
			 return mesActual;
		}
		public String getAnioActual(){
			 DateTool fechaActual = new DateTool();
			 String a�oActual = String.valueOf(fechaActual.getYear());
				
			 
			 return a�oActual.substring(2,4);
		}
		
		public List<Sede> getListaSedeAutorizadas(int rutUsuario) throws Exception {
			List<Sede> lista = new ArrayList<Sede>();
			ConexionAs400OrdPago con = new ConexionAs400OrdPago();
		    Sede sede = new Sede();
			try{
				      
				 PreparedStatement sent = con.getConnection().prepareStatement(" SELECT v1.SUCBAN, v1.DESSUC " +
                         													   " FROM USMMBP.PRESF207 v1, USMMBP.PRESF206 v2" +
                         													   " WHERE v1.SUCBAN = v2.SUCBAN " +
						   													   " AND v2.RUTIDE = ? "); 	
					    	
				 sent.setInt(1,rutUsuario);
				 ResultSet res = sent.executeQuery();
		
				 while (res.next()){
					 sede = new Sede();
					 sede.setCodSuc(res.getInt(1));
					 sede.setNomSuc(res.getString(2).trim()); // 
					 lista.add(sede);
				 }
				 res.close();
				 sent.close();
				 con.getConnection().close();
			   }
			    catch (SQLException e){
				    	System.out.println("Error moduloOrdenCompraRegularizacionB.getListaSedeAutorizadas: " + e);
				}
			 return lista;
			}
		public String getNumeroOrden(int numero){
		    // ConexionBD con = new ConexionBD();
			 ConexionAs400OrdPago con = new ConexionAs400OrdPago();
		     String numeroBanner = "";
			 try {
		    	PreparedStatement sent = con.getConnection().prepareStatement(" SELECT COMENT" +
		    																  " FROM USMMBP.COCOF157B " + 
		    																  " WHERE numreg = ?" ); 
		        sent.setInt(1,numero);
		        ResultSet res = sent.executeQuery();

			    if (res.next()) {
			    	numeroBanner = res.getString(1);
			     }
			     res.close();
			     sent.close();
			     // con.close();
			     con.getConnection().close();

			    }
			    catch (SQLException e ) {
			      System.out.println("Error en moduloOrdenCompraRegularizacionB.getNumeroOrden : " + e );
			    }
			    return numeroBanner;
		}
}
