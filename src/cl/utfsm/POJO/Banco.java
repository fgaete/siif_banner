package cl.utfsm.POJO;

public class Banco {
	
	int codBan;
	String desBan;
	String indVig;
	public int getCodBan() {
		return codBan;
	}
	public void setCodBan(int codBan) {
		this.codBan = codBan;
	}
	public String getDesBan() {
		return desBan;
	}
	public void setDesBan(String desBan) {
		this.desBan = desBan;
	}
	public String getIndVig() {
		return indVig;
	}
	public void setIndVig(String indVig) {
		this.indVig = indVig;
	}
	
}
