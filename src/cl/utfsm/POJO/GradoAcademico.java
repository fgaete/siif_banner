package cl.utfsm.POJO;

public class GradoAcademico {
	int codGra;
	String desGra;
	
	public int getCodGra() {
		return codGra;
	}
	public void setCodGra(int codGra) {
		this.codGra = codGra;
	}
	public String getDesGra() {
		return desGra;
	}
	public void setDesGra(String desGra) {
		this.desGra = desGra;
	}
	
	
}
