package cl.utfsm.POJO;

public class APREF08 {
	
	int codPla;
	int codCar;
	String desCar;
	int nivGra;
	String famCarCod;
	String pueBan;
	
	public int getCodPla() {
		return codPla;
	}
	public void setCodPla(int codPla) {
		this.codPla = codPla;
	}
	public int getCodCar() {
		return codCar;
	}
	public void setCodCar(int codCar) {
		this.codCar = codCar;
	}
	public String getDesCar() {
		return desCar;
	}
	public void setDesCar(String desCar) {
		this.desCar = desCar;
	}
	public int getNivGra() {
		return nivGra;
	}
	public void setNivGra(int nivGra) {
		this.nivGra = nivGra;
	}
	public String getFamCarCod() {
		return famCarCod;
	}
	public void setFamCarCod(String famCarCod) {
		this.famCarCod = famCarCod;
	}
	public String getPueBan() {
		return pueBan;
	}
	public void setPueBan(String pueBan) {
		this.pueBan = pueBan;
	}

}
