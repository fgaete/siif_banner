package cl.utfsm.POJO;

public class Comuna {
	int codigoComuna;
	String nomComuna;
	public int getCodigoComuna() {
		return codigoComuna;
	}
	public void setCodigoComuna(int codigoComuna) {
		this.codigoComuna = codigoComuna;
	}
	public String getNomComuna() {
		return nomComuna;
	}
	public void setNomComuna(String nomComuna) {
		this.nomComuna = nomComuna;
	}
	

}
