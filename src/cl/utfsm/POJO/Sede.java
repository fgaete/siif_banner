package cl.utfsm.POJO;

public class Sede {
	int codSuc;
	String nomSuc;
	
	public int getCodSuc() {
		return codSuc;
	}
	public void setCodSuc(int codSuc) {
		this.codSuc = codSuc;
	}
	public String getNomSuc() {
		return nomSuc;
	}
	public void setNomSuc(String nomSuc) {
		this.nomSuc = nomSuc;
	}
	
	

}
