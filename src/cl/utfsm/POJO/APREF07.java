package cl.utfsm.POJO;

public class APREF07 {
	
	int codPla;
	String desPla;
	int codDesc;
	int nivCce;
	int nivSed;
	int antCce;
	int antSed;
	int nivOtr;
	String claBan;
	public int getCodPla() {
		return codPla;
	}
	public void setCodPla(int codPla) {
		this.codPla = codPla;
	}
	public String getDesPla() {
		return desPla;
	}
	public void setDesPla(String desPla) {
		this.desPla = desPla;
	}
	public int getCodDesc() {
		return codDesc;
	}
	public void setCodDesc(int codDesc) {
		this.codDesc = codDesc;
	}
	public int getNivCce() {
		return nivCce;
	}
	public void setNivCce(int nivCce) {
		this.nivCce = nivCce;
	}
	public int getNivSed() {
		return nivSed;
	}
	public void setNivSed(int nivSed) {
		this.nivSed = nivSed;
	}
	public int getAntCce() {
		return antCce;
	}
	public void setAntCce(int antCce) {
		this.antCce = antCce;
	}
	public int getAntSed() {
		return antSed;
	}
	public void setAntSed(int antSed) {
		this.antSed = antSed;
	}
	public int getNivOtr() {
		return nivOtr;
	}
	public void setNivOtr(int nivOtr) {
		this.nivOtr = nivOtr;
	}
	public String getClaBan() {
		return claBan;
	}
	public void setClaBan(String claBan) {
		this.claBan = claBan;
	}

}
