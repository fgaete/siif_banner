package cl.utfsm.POJO;

public class APREF23 {
	
	int sucur;
	String docAca;
	String tipSes;
	int valNom;
	int valMin;
	int valMax;
	int valNi4;
	int valNu5;
	public int getSucur() {
		return sucur;
	}
	public void setSucur(int sucur) {
		this.sucur = sucur;
	}
	public String getDocAca() {
		return docAca;
	}
	public void setDocAca(String docAca) {
		this.docAca = docAca;
	}
	public String getTipSes() {
		return tipSes;
	}
	public void setTipSes(String tipSes) {
		this.tipSes = tipSes;
	}
	public int getValNom() {
		return valNom;
	}
	public void setValNom(int valNom) {
		this.valNom = valNom;
	}
	public int getValMin() {
		return valMin;
	}
	public void setValMin(int valMin) {
		this.valMin = valMin;
	}
	public int getValMax() {
		return valMax;
	}
	public void setValMax(int valMax) {
		this.valMax = valMax;
	}
	public int getValNi4() {
		return valNi4;
	}
	public void setValNi4(int valNi4) {
		this.valNi4 = valNi4;
	}
	public int getValNu5() {
		return valNu5;
	}
	public void setValNu5(int valNu5) {
		this.valNu5 = valNu5;
	}

	
}
