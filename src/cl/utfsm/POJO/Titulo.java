package cl.utfsm.POJO;

public class Titulo {
	
	int codGra;
	int codTit;
	String desTit;
	
	public int getCodGra() {
		return codGra;
	}
	public void setCodGra(int codGra) {
		this.codGra = codGra;
	}
	public int getCodTit() {
		return codTit;
	}
	public void setCodTit(int codTit) {
		this.codTit = codTit;
	}
	public String getDesTit() {
		return desTit;
	}
	public void setDesTit(String desTit) {
		this.desTit = desTit;
	}

}
