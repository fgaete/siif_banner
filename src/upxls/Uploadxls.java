package upxls;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class Uploadxls extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html";

    public void init() throws ServletException {
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        Vector salida = new Vector();
        PrintWriter out = response.getWriter();

        FileItemFactory factory = new DiskFileItemFactory();

        ServletFileUpload upload = new ServletFileUpload(factory);

        try {
            List items = null;
            items = upload.parseRequest(request);

            Iterator iter = items.iterator();
            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();
                if (!item.isFormField()) {
                    String fieldName = item.getFieldName();
                    String fileName = item.getName();
                    String contentType = item.getContentType();
                    boolean isInMemory = item.isInMemory();
                    long sizeInBytes = item.getSize();
                    System.out.println("Nombre ********" + fieldName);
                    System.out.println("nombre " + fileName);
                    if(fileName.indexOf(".") > 0) {
                        System.out.println("extension " + fileName.substring(fileName.indexOf(".")+1,fileName.length()));
                    }

                    System.out.println("Type " + contentType);
                    System.out.println("en memoria " + isInMemory);
                    System.out.println("tama�o " + sizeInBytes);

                    Calendar fecha = Calendar.getInstance();
                    String mes = "";
                    String dia = "";
                    String a�o = String.valueOf(fecha.get(Calendar.YEAR));
                    if(fecha.get(Calendar.MONTH) < 8)
                        mes = "0" + String.valueOf(fecha.get(Calendar.MONTH) + 1);
                        else
                            mes = String.valueOf(fecha.get(Calendar.MONTH) + 1);
                    if(fecha.get(Calendar.DAY_OF_MONTH) < 10)
                        dia = "0" + String.valueOf(fecha.get(Calendar.DAY_OF_MONTH));
                    else
                        dia = String.valueOf(fecha.get(Calendar.DAY_OF_MONTH));
                    String dir = a�o + "/" + mes + "/" + dia + "/";

                    if(!(new File(dir).exists()))
                        (new File(dir)).mkdirs();

                    File uploadedFile = null;
                    if (fileName.substring(fileName.indexOf(".")+1, fileName.length()).
                                equals("xlsx")) {
                        uploadedFile = new File( dir + "prueba_upload.xlsx");
                    }
                    else {
                        uploadedFile = new File(dir + "prueba_upload.xls");
                    }

                    try {
                        item.write(uploadedFile);

                    } catch (Exception ex1) {
                        System.out.println("error escribir" + ex1.toString());
                    }



                    try {

                        if(fileName.indexOf(".") > 0) {
                            if (fileName.substring(fileName.indexOf(".")+1, fileName.length()).
                                equals("xlsx")) {

                                Process_xls procesarxls = new Process_xls(
                                         dir + "prueba_upload.xlsx",
                                        "Planilla_presupuesto_corriente.xlsx", 2, 1);

                                        salida = procesarxls.Source();
                                System.out.println("compara archivos " +
                                                   procesarxls.Equals());

                            }
                            else {
                                Process_xls procesarxls = new Process_xls(
                                        dir + "prueba_upload.xls",
                                        "Planilla_presupuesto_corriente.xls", 2, 1);
                                salida = procesarxls.Source();
                                System.out.println("compara archivos " +
                                                   procesarxls.Equals());


                            }
                        }

                    }catch (Exception ex1) {
                        System.out.println(ex1.toString());
                        out.println("<html>");
                        out.println("<head><title>uploadxls</title></head>");
                        out.println("<body bgcolor=\"#ffffff\">");
                        out.println(
                                "<p>Error.</p>");
                        out.println("</body>");
                        out.println("</html>");
                        out.close();


                    }

                }
                else {
                    String name = item.getFieldName();
                    String value = item.getString();

                }
            }

        } catch (FileUploadException ex) {
            System.out.println(ex.toString());
            }


        out.println("<html>");
        out.println("<head><title>uploadxls</title></head>");
        out.println("<body bgcolor=\"#ffffff\">");
        out.println(
                "<p>The servlet has received a POST. This is the reply.</p>");
        out.println(salida);
        out.println("</body>");
        out.println("</html>");
        out.close();

    }

    public void destroy() {
    }

}
