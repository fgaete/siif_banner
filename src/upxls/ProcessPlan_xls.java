package upxls;

import java.io.IOException;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.xml.sax.SAXException;

import descad.presupuesto.Presw25DTO;

public class ProcessPlan_xls {
	 private Vector salida_source = new Vector();
	    private Vector salida_pattern = new Vector();
	    private int column = 0 ;
	    private int row = 0;
	    private String ext = "";

	    public ProcessPlan_xls(String path_source, String path_pattern, int column, int row) {
	        this.column = column;
	        this.row = row;
	        if(path_source.indexOf(".") > 0) {
	            ext = path_source.substring(path_source.indexOf(".")+1,path_source.length());
	        }

	        if(ext.equals("xlsx")) {

	            OPCPackage p_source = null;
	            OPCPackage p_pattern = null;
	            try {
	                p_source = OPCPackage.open(path_source, PackageAccess.READ);
	                p_pattern = OPCPackage.open(path_pattern, PackageAccess.READ);
	            } catch (InvalidFormatException ex2) {
	            }
	            XLSX xlsx_source = new XLSX(p_source, System.out, -1);
	            XLSX xlsx_pattern = new XLSX(p_pattern, System.out, -1);
	            try {
	                salida_source = xlsx_source.process();
	                salida_pattern = xlsx_pattern.process();
	            } catch (SAXException ex3) {
	            } catch (ParserConfigurationException ex3) {
	            } catch (OpenXML4JException ex3) {
	            } catch (IOException ex3) {
	            }
	        }
	        else if (ext.equals("xls")) {

	            XLS xls_source = null;
	            XLS xls_pattern = null;
	            try {
	                xls_source = new XLS(path_source, -1);
	                xls_pattern = new XLS(path_pattern, -1);
	                salida_source = xls_source.process();
	                salida_pattern = xls_pattern.process();
	            } catch (IOException ex) {
	            }


	        }

	    }
	    public ProcessPlan_xls(String path_source) {

	        if (ext.equals("xlsx")) {

	            OPCPackage p_source = null;

	            try {
	                p_source = OPCPackage.open(path_source, PackageAccess.READ);

	            } catch (InvalidFormatException ex2) {
	            }
	            XLSX xlsx_source = new XLSX(p_source, System.out, -1);

	            try {
	                salida_source = xlsx_source.process();

	            } catch (SAXException ex3) {
	            } catch (ParserConfigurationException ex3) {
	            } catch (OpenXML4JException ex3) {
	            } catch (IOException ex3) {
	            }
	        }
	        else if (ext.equals("xls")) {

	            XLS xls_source = null;
	            try {
	                xls_source = new XLS(path_source, -1);

	                salida_source = xls_source.process();

	            } catch (IOException ex) {
	            }

	        }

	    }
	    /**
	     * Source_pattern
	     *
	     * @return boolean
	     */
	    public boolean Equals() {

	            for (int i = 0; i < salida_pattern.size(); i++) {
	                Vector vec1 = new Vector();
	                Vector vec2 = new Vector();

	                if (i < row) {
	                    if (!salida_pattern.get(i).equals(salida_source.get(i))) {
	                        return false;
	                    }

	                } else {

	                    if (salida_pattern.size() == salida_source.size()) {
	                        vec1 = (Vector) salida_pattern.get(i);
	                        vec2 = (Vector) salida_source.get(i);
	                        if (vec1.size() > 0 && vec2.size() > 0 &&
	                            vec1.size() <= vec2.size()) {
	                            for (int j = 0; j < column; j++) {
	                                if (!vec1.get(j).equals(vec2.get(j))) {
	                                      return false;
	                                }
	                            }
	                        } else {

	                            return false;
	                        }
	                    } else {
	                        return false;
	                    }
	                }
	            }
	            if(salida_pattern.size() ==0)
	                return false;
	            else
	                return true;
	    }

	    
	    /**
	     * getPresw25DTO
	     * banner
	     * @return Vector
	     */
	    public Vector getPresw25DTO(String tippro, String codOrganizacion, int anopre, int numrec, int rutide, String digide ) {
	        Vector vec1= new Vector();
	        Vector salida = new Vector();
	        int cont = 0;
	        if(salida_source.size()> 1) {
	            for (int i = 1; i < salida_source.size() - 1; i++) {
	                vec1 = (Vector) salida_source.get(i);
	                Presw25DTO presw = new Presw25DTO();
	                String item ="";
	               // if (vec1.get(0).toString().trim().length() >= 6)
	                        item = vec1.get(0).toString();

	                if(item.trim().equals(""))  {  // para que no tome en cuenta los que no tienen codigo de organizacion
	                	cont++;
	                	presw.setTippro(tippro);
	                    presw.setCodman(String.valueOf(cont));
	                	presw.setMotiv2("");
	                    presw.setMotiv3(String.valueOf(cont));
	                    presw.setAnopre(anopre);
	                    presw.setNumreq(numrec);
	                    presw.setRutide(rutide);
	                    presw.setDigide(digide);
	                    presw.setDesite(((String) vec1.get(1)).toUpperCase());
	                    presw.setIndexi("");
	                    presw.setPres01(0);
	                    presw.setPres02(0);
	                    presw.setPres03(0);
	                    presw.setPres04(0);
	                    presw.setPres05(0);
	                    presw.setPres06(0);
	                    presw.setPres07(0);
	                    presw.setPres08(0);
	                    presw.setPres09(0);
	                    presw.setPres10(0);
	                    presw.setPres11(0);
	                    presw.setPres12(0);
	                    presw.setComen1("");
	                    presw.setComen2("");
	                    presw.setComen3("");
	                    presw.setComen4("");
	                    presw.setComen5("");
	                    presw.setComen6("");
	                    
	                } else {
	                for (int j = 0; j < vec1.size(); j++) {

	                        presw.setTippro(tippro);
	                        presw.setCodman(((String) vec1.get(0)).toUpperCase());
	                        presw.setAnopre(anopre);
	                        presw.setNumreq(numrec);
	                        presw.setRutide(rutide);
	                        presw.setDigide(digide);
	                        presw.setIndexi("");

	                        switch (j) {
	                        case 0:
	                        	if (vec1.get(0).toString().trim().length() == 6) {
	                                presw.setCodman(vec1.get(0).toString().toUpperCase());
	                                presw.setMotiv3(String.valueOf(cont));
	                            }
	                            break;
	                        case 1:
	                            presw.setDesite(((String) vec1.get(1)).toUpperCase());
	                            break;
	                        case 2: 
	                            if (vec1.get(2).toString().length() > 0)
	                                presw.setTotpre(Long.parseLong(((String) vec1.get(2)).replace(
	                                        ".", "").replace(",", "")));

	                            break;
	                        case 3: 
	                            if (vec1.get(3).toString().length() > 0)
	                                presw.setPres01(Long.parseLong(((String) vec1.get(3)).replace(
	                                        ".", "").replace(",", "")));

	                            break;
	                        case 4: 
	                            if (vec1.get(4).toString().length() > 0)
	                                presw.setPres02(Long.parseLong(((String) vec1.get(4)).replace(
	                                        ".", "").replace(",", "")));
	                            break;
	                        case 5: 

	                            if (vec1.get(5).toString().length() > 0)
	                                presw.setPres03(Long.parseLong(((String) vec1.get(5)).replace(
	                                        ".", "").replace(",", "")));
	                            break;
	                        case 6:

	                            if (vec1.get(6).toString().length() > 0)
	                                presw.setPres04(Long.parseLong(((String) vec1.get(6)).replace(
	                                        ".", "").replace(",", "")));

	                            break;
	                        case 7: 

	                            if (vec1.get(7).toString().length() > 0)
	                                presw.setPres05(Long.parseLong(((String) vec1.get(7)).replace(
	                                        ".", "").replace(",", "")));

	                            break;
	                        case 8: 
	                            if (vec1.get(8).toString().length() > 0)
	                                presw.setPres06(Long.parseLong(((String) vec1.get(8)).replace(
	                                        ".", "").replace(",", "")));
	                            break;
	                        case 9: 
	                            if (vec1.get(9).toString().length() > 0)
	                                presw.setPres07(Long.parseLong(((String) vec1.get(9)).replace(
	                                        ".", "").replace(",", "")));
	                            break;
	                        case 10: 
	                            if (vec1.get(10).toString().length() > 0)
	                                presw.setPres08(Long.parseLong(((String) vec1.get(10)).replace(
	                                        ".", "").replace(",", "")));
	                            break;
	                        case 11: 
	                            if (vec1.get(11).toString().length() > 0)
	                                presw.setPres09(Long.parseLong(((String) vec1.get(11)).replace(
	                                        ".", "").replace(",", "")));
	                            break;
	                        case 12: 
	                            if (vec1.get(12).toString().length() > 0)
	                                presw.setPres10(Long.parseLong(((String) vec1.get(12)).replace(
	                                        ".", "").replace(",", "")));
	                            break;
	                        case 13: 
	                            if (vec1.get(13).toString().length() > 0)
	                                presw.setPres11(Long.parseLong(((String) vec1.get(13)).replace(
	                                        ".", "").replace(",", "")));
	                            break;
	                        case 14: 
	                            if (vec1.get(14).toString().length() > 0)
	                                presw.setPres12(Long.parseLong(((String) vec1.get(14)).replace(
	                                        ".", "").replace(",", "")));
	                            break;
	                        case 15: 
	                            int largo = ((String) vec1.get(15)).length();
	                            if (largo > 40) {
	                                if (largo > 80) {
	                                    if (largo > 120) {
	                                        if (largo > 160) {
	                                            if (largo > 200) {
	                                                presw.setComen1(vec1.get(15).
	                                                                toString().substring(0, 40));
	                                                presw.setComen2(vec1.get(15).
	                                                                toString().substring(40, 80));
	                                                presw.setComen3(vec1.get(15).
	                                                                toString().substring(80,
	                                                        120));
	                                                presw.setComen4(vec1.get(15).
	                                                                toString().substring(120,
	                                                        160));
	                                                presw.setComen5(vec1.get(15).
	                                                                toString().substring(160,
	                                                        200));
	                                                if(largo > 240)
	                                                    presw.setComen6(vec1.get(15).
	                                                            toString().substring(200,
	                                                            240));
	                                                else
	                                                    presw.setComen6(vec1.get(15).
	                                                            toString().substring(200,
	                                                            largo));


	                                            } else {
	                                                presw.setComen1(vec1.get(15).
	                                                                toString().substring(0, 40));
	                                                presw.setComen2(vec1.get(15).
	                                                                toString().substring(40, 80));
	                                                presw.setComen3(vec1.get(15).
	                                                                toString().substring(80,
	                                                        120));
	                                                presw.setComen4(vec1.get(15).
	                                                                toString().substring(120,
	                                                        160));
	                                                presw.setComen5(vec1.get(15).
	                                                                toString().substring(160,
	                                                        largo));
	                                            }
	                                        } else {
	                                            presw.setComen1(vec1.get(15).toString().
	                                                            substring(0, 40));
	                                            presw.setComen2(vec1.get(15).toString().
	                                                            substring(40, 80));
	                                            presw.setComen3(vec1.get(15).toString().
	                                                            substring(80, 120));
	                                            presw.setComen4(vec1.get(15).toString().
	                                                            substring(120, largo));
	                                        }

	                                    } else {
	                                        presw.setComen1(vec1.get(15).toString().
	                                                        substring(0, 40));
	                                        presw.setComen2(vec1.get(15).toString().
	                                                        substring(40, 80));
	                                        presw.setComen3(vec1.get(15).toString().
	                                                        substring(80, largo));
	                                    }

	                                } else {
	                                    presw.setComen1(vec1.get(15).toString().
	                                                    substring(0, 40));
	                                    presw.setComen2(vec1.get(15).toString().
	                                                    substring(40, largo));
	                                }

	                            } else
	                                presw.setComen1(vec1.get(15).toString());

	                            break;

	                        }
	                		}
	                    }
	                if(cont <= 5)
	                    salida.add(presw);
	             
	            }
	        }
	        return salida;
	    }
	    

	    /**
	     * Source
	     *
	     * @return Vector
	     */



	    public Vector Source() {
	        return salida_source;
	    }

}
