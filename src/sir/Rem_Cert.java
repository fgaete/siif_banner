package sir;

import java.util.List;
import java.util.Vector;


public class Rem_Cert {
    private boolean cert_sueldo = true;
    private boolean cert_honorarios = true;
    private boolean cert_antiguedad = true;
    private boolean cert_antiguedad_renta = true;
    private boolean cert_antiguedad_periodos = true;
    private boolean cert_proyecto_fondef = true;
    private String rut;
    private String dv;

     public Rem_Cert() {
    }
    /**
     *
     * @return boolean
     */
    public boolean getCert_Sueldo() {
        return cert_sueldo;
    }
    /**
     *
     * @return boolean
     */
    public boolean getCert_Honorarios() {
        return cert_honorarios;
    }
    /**
     *
     * @return boolean
     */
    public boolean getCert_Antiguedad() {
        return cert_antiguedad;
    }
    /**
     *
     * @return boolean
     */
    public boolean getCert_Antiguedad_Renta() {
        return cert_antiguedad_renta;
    }
    /**
     *
     * @return boolean
     */
    public boolean getCert_Antiguedad_Periodos() {
        return cert_antiguedad_periodos;

    }
    /**
     *
     * @return boolean
     */
    public boolean getCert_Proyecto_Fondef() {
       return cert_proyecto_fondef;
   }

    /**
     * getRem
     *
     * @return List
     */
    public List getRem() {
        Vector vec = new Vector();
        RemDTO rem = new RemDTO();
        rem.setAno("2010");
        rem.setContrato("1");
        rem.setLiquidacion("L");
        rem.setMes("09");
        rem.setReliquidacion("R");
        rem.setBonificacion("B");

        vec.add(rem);
        
        rem = new RemDTO();
        rem.setAno("2010");
        rem.setContrato("5");
        rem.setLiquidacion("L");
        rem.setMes("09");
        rem.setReliquidacion("");

        vec.add(rem);
        
        
        rem = new RemDTO();
        rem.setAno("2010");
        rem.setContrato("7");
        rem.setLiquidacion("L");
        rem.setMes("09");
        rem.setReliquidacion("");

        vec.add(rem);
        return vec;

    }

    /**
     * getRem_Hist
     *
     * @return List
     */
    public List getRem_Hist() {
        Vector vec = new Vector();
        RemDTO rem = new RemDTO();



       for (int x = 11; x <= 12; x++) {
    	   rem = new RemDTO();
           rem.setAno("2009");
           rem.setContrato("1");
           rem.setLiquidacion("L");
           rem.setReliquidacion("R");
           rem.setMes(String.valueOf(x));
           vec.add(rem);
       }
       

        for (int x = 1; x < 10; x++) {
        	rem = new RemDTO();
            rem.setAno("2010");
            rem.setContrato("1");
            rem.setLiquidacion("L");
            rem.setReliquidacion("R");
            rem.setMes("0"+ x);
            vec.add(rem);
        }
        
        for (int x = 1; x < 10; x++) {
        	rem = new RemDTO();
            rem.setAno("2010");
            rem.setContrato("7");
            rem.setLiquidacion("L");
            rem.setMes("0"+ x);
            vec.add(rem);
        }

        return vec;
    }

    /**
     * 
     * @param rut String
     */
    public void setRut(String rut) {
        this.rut = rut;
    }
    /**
     * 
     * @param dv String
     */
    public void setDv(String dv) {
        this.dv = dv;
    }

}

