package sir;


public class RemDTO {
    private String contrato;
    private String liquidacion;
    private String reliquidacion;
    private String bonificacion;
    private String ano;
    private String mes;
    private String nommes;
    private String nomContrato;
    private String nomLiquidacion;
    private String nomReliquidacion;
    private String nomBonificacion;

    /**
     *
     */
    public RemDTO() {
    }
    /**
     *
     * @param contrato String
     */

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }
    /**
     *
     * @param liquidacion String
     */
    public void setLiquidacion(String liquidacion) {
        this.liquidacion = liquidacion;
    }
    /**
     *
     * @param reliquidacion String
     */
    public void setReliquidacion(String reliquidacion) {
        this.reliquidacion = reliquidacion;
    }
    /**
     *
     * @param a�o String
     */
    public void setAno(String ano) {
        this.ano = ano;
    }
    /**
     *
     * @param mes String
     */
    public void setMes(String mes) {
        this.mes = mes;
    }

    public void setNommes(String nommes) {
        this.nommes = nommes;
    }
    /**
     *
     * @return String
     */
    public String getContrato() {
        return contrato;
    }
    /**
     *
     * @return String
     */
    public String getLiquidacion() {
        return liquidacion;
    }
    /**
     *
     * @return String
     */
    public String getReliquidacion() {
        return reliquidacion;
    }
    /**
     *
     * @return String
     */
    public String getAno() {
        return ano;
    }
    /**
     *
     * @return String
     */
    public String getMes() {
        return mes;
    }
    public String getNommes() {
        return nommes;
    }
	public String getNomContrato() {
		return nomContrato;
	}
	public void setNomContrato(String nomContrato) {
		this.nomContrato = nomContrato;
	}
	public String getNomLiquidacion() {
		return nomLiquidacion;
	}
	public void setNomLiquidacion(String nomLiquidacion) {
		this.nomLiquidacion = nomLiquidacion;
	}
	public String getNomReliquidacion() {
		return nomReliquidacion;
	}
	public void setNomReliquidacion(String nomReliquidacion) {
		this.nomReliquidacion = nomReliquidacion;
	}
	public String getBonificacion() {
		return bonificacion;
	}
	public void setBonificacion(String bonificacion) {
		this.bonificacion = bonificacion;
	}
	public String getNomBonificacion() {
		return nomBonificacion;
	}
	public void setNomBonificacion(String nomBonificacion) {
		this.nomBonificacion = nomBonificacion;
	}
}
